INTRN_ABSCH	,
INTRN_ABSCL	,
INTRN_ABSCW	,
INTRN_ABSH	,
INTRN_ABSL	,
INTRN_ABSW	,
INTRN_ADDCH	,
INTRN_ADDCL	,
INTRN_ADDCW	,
INTRN_ADDD	,
INTRN_ADDL	,
INTRN_ADDS	,
INTRN_ADDUL	,
INTRN_BITCLRH	,
INTRN_BITCLRW	,
INTRN_BITCNTH	,
INTRN_BITCNTW	,
INTRN_BITNOTH	,
INTRN_BITNOTW	,
INTRN_BITREVW	,
INTRN_BITSETH	,
INTRN_BITSETW	,
INTRN_BITVALH	,
INTRN_BITVALW	,
INTRN_BUILTIN__DIVUW	,
INTRN_BUILTIN__DIVW	,
INTRN_BUILTIN__MODUW	,
INTRN_BUILTIN__MODW	,
INTRN_CLAMPLW	,
INTRN_CLAMPWH	,
INTRN_DISTH	,
INTRN_DISTUH	,
INTRN_DISTUW	,
INTRN_DISTW	,
INTRN_DIVD	,
INTRN_DIVFCH	,
INTRN_DIVFCM	,
INTRN_DIVFCW	,
INTRN_DIVH	,
INTRN_DIVL	,
INTRN_DIVM	,
INTRN_DIVS	,
INTRN_DIVUH	,
INTRN_DIVUL	,
INTRN_DIVUM	,
INTRN_DIVUW	,
INTRN_DIVW	,
INTRN_DTOL	,
INTRN_DTOS	,
INTRN_DTOUL	,
INTRN_DTOUW	,
INTRN_DTOW	,
INTRN_EDGESH	,
INTRN_EDGESW	,
INTRN_EQD	,
INTRN_EQL	,
INTRN_EQS	,
INTRN_EQUL	,
INTRN_GED	,
INTRN_GEL	,
INTRN_GES	,
INTRN_GETHH	,
INTRN_GETHW	,
INTRN_GETLH	,
INTRN_GETLW	,
INTRN_GEUL	,
INTRN_GTD	,
INTRN_GTL	,
INTRN_GTS	,
INTRN_GTUL	,
INTRN_INSEQUW	,
INTRN_INSEQW	,
INTRN_INSGEUW	,
INTRN_INSGEW	,
INTRN_INSGTUW	,
INTRN_INSGTW	,
INTRN_INSLEUW	,
INTRN_INSLEW	,
INTRN_INSLTUW	,
INTRN_INSLTW	,
INTRN_INSNEUW	,
INTRN_INSNEW	,
INTRN_LED	,
INTRN_LEL	,
INTRN_LES	,
INTRN_LEUL	,
INTRN_LTD	,
INTRN_LTL	,
INTRN_LTOD	,
INTRN_LTOS	,
INTRN_LTS	,
INTRN_LTUL	,
INTRN_LZCNTH	,
INTRN_LZCNTL	,
INTRN_LZCNTW	,
INTRN_MADDS	,
INTRN_MAFCW	,
INTRN_MAXD	,
INTRN_MAXH	,
INTRN_MAXL	,
INTRN_MAXS	,
INTRN_MAXUH	,
INTRN_MAXUL	,
INTRN_MAXUW	,
INTRN_MAXW	,
INTRN_MIND	,
INTRN_MINH	,
INTRN_MINL	,
INTRN_MINS	,
INTRN_MINUH	,
INTRN_MINUL	,
INTRN_MINUW	,
INTRN_MINW	,
INTRN_MODFCH	,
INTRN_MODFCM	,
INTRN_MODFCW	,
INTRN_MODH	,
INTRN_MODL	,
INTRN_MODM	,
INTRN_MODUH	,
INTRN_MODUL	,
INTRN_MODUM	,
INTRN_MODUW	,
INTRN_MODW	,
INTRN_MPFCW	,
INTRN_MPFCWL	,
INTRN_MPFML	,
INTRN_MPFRCH	,
INTRN_MPML	,
INTRN_MPUML	,
INTRN_MSUBS	,
INTRN_MULD	,
INTRN_MULFCH	,
INTRN_MULFCM	,
INTRN_MULFCW	,
INTRN_MULH	,
INTRN_MULHH	,
INTRN_MULHUH	,
INTRN_MULHUW	,
INTRN_MULHW	,
INTRN_MULL	,
INTRN_MULM	,
INTRN_MULN	,
INTRN_MULS	,
INTRN_MULUH	,
INTRN_MULUL	,
INTRN_MULUM	,
INTRN_MULUN	,
INTRN_MULUW	,
INTRN_MULW	,
INTRN_NEARCLW	,
INTRN_NEARCWH	,
INTRN_NEARLW	,
INTRN_NEARWH	,
INTRN_NED	,
INTRN_NEGCH	,
INTRN_NEGCL	,
INTRN_NEGCW	,
INTRN_NEGL	,
INTRN_NEGUL	,
INTRN_NEL	,
INTRN_NES	,
INTRN_NEUL	,
INTRN_NORMH	,
INTRN_NORML	,
INTRN_NORMW	,
INTRN_PRIORH	,
INTRN_PRIORL	,
INTRN_PRIORW	,
INTRN_PUTHL	,
INTRN_PUTHW	,
INTRN_PUTLL	,
INTRN_PUTLW	,
INTRN_ROTLH	,
INTRN_ROTLW	,
INTRN_ROUNDCLW	,
INTRN_ROUNDCWH	,
INTRN_ROUNDLW	,
INTRN_ROUNDWH	,
INTRN_SHLCH	,
INTRN_SHLCW	,
INTRN_SHLL	,
INTRN_SHLUL	,
INTRN_SHLW	,
INTRN_SHRL	,
INTRN_SHRRH	,
INTRN_SHRRW	,
INTRN_SHRUL	,
INTRN_SHRUW	,
INTRN_SHRW	,
INTRN_SQRTD	,
INTRN_SQRTS	,
INTRN_STOD	,
INTRN_STOL	,
INTRN_STOUL	,
INTRN_STOUW	,
INTRN_STOW	,
INTRN_SUBCH	,
INTRN_SUBCL	,
INTRN_SUBCW	,
INTRN_SUBD	,
INTRN_SUBL	,
INTRN_SUBS	,
INTRN_SUBUL	,
INTRN_SWAPBH	,
INTRN_SWAPBW	,
INTRN_SWAPHW	,
INTRN_ULTOD	,
INTRN_ULTOS	,
INTRN_UWTOD	,
INTRN_UWTOS	,
INTRN_WTOD	,
INTRN_WTOS	,
INTRN_XSHLH	,
INTRN_XSHLW	,
INTRN_XSHRH	,
INTRN_XSHRW	,
INTRN_NMADDS	,
INTRN_NMSUBS	,
INTRN_SQUARES	,
INTRN_RECIPS	,
INTRN_RSQRTS	,
INTRN_ASM_0	,
INTRN_ASM_1	,
INTRN_ASM_2	,
INTRN_ASM_3	,
INTRN_ASM_4	,
INTRN_ASM_5	,
INTRN_ASM_6	,
INTRN_ASM_7	,
INTRN_ASM_8	,
INTRN_ASM_9	,
INTRN_ASM_10	,
INTRN_ASM_11	,
INTRN_ASM_12	,
INTRN_ASM_13	,
INTRN_ASM_14	,
INTRN_ASM_15	,
INTRN_TRAP	,
INTRN_BUILTIN__DIVH	,
INTRN_BUILTIN__DIVUH	,
INTRN_BUILTIN__MODH	,
INTRN_BUILTIN__MODUH	,
INTRN_ISGREATER,
INTRN_ISGREATERF,
INTRN_ISGREATEREQUAL,
INTRN_ISGREATEREQUALF,
INTRN_ISLESS,
INTRN_ISLESSF,
INTRN_ISLESSEQUAL,
INTRN_ISLESSEQUALF,
INTRINSIC_GENERIC_LAST = INTRN_ISLESSEQUALF,
#define INTRN_DEFINES
#include "gen_intrinsics.inc"
#undef INTRN_DEFINES

