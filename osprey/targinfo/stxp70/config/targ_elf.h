/*
  Copyright (C) 2007, STMicroelectronics, All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.
*/


/* This file holds definitions specific to the STxP70 ELF ABI. */

#ifndef _TARG_ELF_H
#define _TARG_ELF_H

#include <elf.h>

/*--------------------------------------------------------------------------*/
/* Since in the IPA process WHIRL objects files are in fact ELF object files*/
/* it is mandatory to keep the same convention than in standard and         */ 
/* and executable files. This let binary utilities and in particular the    */
/* GNU linker read and extract information from WHIRL files. In particular: */
/*                                                                          */
/*    - the upper 16 bits are common to "normal" and WHIRL files            */
/*    - the lower 16 bits (see below) are used to transmit information      */
/*      that is only meaningful for the compiler.                           */
/*--------------------------------------------------------------------------*/

/* Upper 16 bits                                                            */
/* Extracted from GNU binutils repository (from file include/elf/stxp70.h)  */
#define EF_STxP70_FAMILY     0xffff0000
#define EF_STxP70            0x10000                /* STxP70 V3.           */
#define EF_STxP70_DOUBLE64   0x20000                /* 64 bits-double.      */
#define EF_STxP70v4          0x40000                /* STxP70v4             */

/* Lower 16 bits                                                            */
/* Defining the core "subset".                                              */
#define _ELF_STxP70_CORE_BIT	       (8)     /* 1st bit position in byte */
#define ELF_STxP70_CORE_MASK	       (0xFF<<_ELF_STxP70_CORE_BIT) /* mask */
#define ELF_STxP70_CORE_stxp70_v3      (0x1<<_ELF_STxP70_CORE_BIT)
/* #define ELF_STxP70_CORE_stxp70_v3_ext  (0x2<<_ELF_STxP70_CORE_BIT) */
#define ELF_STxP70_CORE_stxp70_v4      (0x3<<_ELF_STxP70_CORE_BIT)
#define ELF_STxP70_CORE_UNDEF          (0x4<<_ELF_STxP70_CORE_BIT)
#define _ELF_STxP70_CHECK_CORE(m)      ((m&ELF_STxP70_CORE_MASK)==m)

/* (pp) abi */
#define _ELF_STxP70_ABI_BIT	(0)             /* 1st bit position in byte */
#define ELF_STxP70_ABI_MASK	(0x7F<<_ELF_STxP70_ABI_BIT)         /* mask */
#define ELF_STxP70_ABI_NO	(0x0<<_ELF_STxP70_ABI_BIT)
#define ELF_STxP70_ABI_EMBED	(0x1<<_ELF_STxP70_ABI_BIT)
#define ELF_STxP70_ABI_FPX      (0x2<<_ELF_STxP70_ABI_BIT)
#define ELF_STxP70_ABI_UNDEF	(0x3<<_ELF_STxP70_ABI_BIT)
#define _ELF_STxP70_CHECK_ABI(m) ((m&ELF_STxP70_ABI_MASK)==m)


#define EM_STxP70 166    /* Registered ELF number for STxP70 */

#endif  /* _TARG_ELF_H */
