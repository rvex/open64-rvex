/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


/* ====================================================================
 * ====================================================================
 *
 * Module: config_elf_targ.cxx
 *
 * Description:
 *
 * ELF configuration specific to the target machine/system.
 *
 * ====================================================================
 * ====================================================================
 */

#include <string.h>                        // for strncmp
#include <elf.h>
#include <sys/elf_whirl.h>
#include "defs.h"
#include "erglob.h"
#include "tracing.h"
#include "config_elf_targ.h"

// [CL] include STxP70 specific values
#include "targ_elf.h"
#include "config_target.h"
#include "config_TARG.h"                   // for Enable_Fpx, Enable_X3
#include "config.h"

// [YJ] Information on extension subset.
#include "mempool.h"
#include "../gccfe/extension_include.h"
#include "dyn_dll_api_access.h"
#include "loader.h"
#include "isa_ext_limits.h"                // definition of EL_MAX_EXT


// Any modification in this structure is to be reported
// in the binary utilities, if the content of .WHIRL.subset
// is to be analyzed by binary utilities such as elfdump
// or objdump.
typedef struct {
   mUINT32  is_available;
   mUINT32  magic_key;
   mUINT32  base_mtypes;
   mUINT32  base_mmodes;
   mUINT32  base_intrinsics;
   mUINT32  base_builtins;
   mUINT32  is_fpx;
   char     extname[16];
} WHIRL_stxp70_reconf_info;


/* ====================================================================
 *   Config_Target_From_ELF
 *
 *   Based on the flags in the ELF header <ehdr>, return whether 
 *   is 64bit and the isa level.
 * ====================================================================
 */
void 
Config_Target_From_ELF (
  Elf64_Word e_flags, 
  BOOL *is_64bit,
  INT *isa
)
{
   static const char *errmsg;
 
   // In any cases, we've got a 32-bit target.
   // But for stxp70v4, we use ELF64.
  *is_64bit = FALSE;

  switch(e_flags & ELF_STxP70_CORE_MASK) {

  case ELF_STxP70_CORE_stxp70_v3:
    if(Target == TARGET_UNDEF) {
       Target = TARGET_stxp70_v3;
    }
    else if (Target != TARGET_stxp70_v3) {
      ErrMsg(EC_Conf_Targ, "processor", Targ_Name(Target), 
             Targ_Name(TARGET_stxp70_v3));
    }
    break;

  case ELF_STxP70_CORE_stxp70_v4:
    if ( Target == TARGET_UNDEF ) {
      Target = TARGET_stxp70_v4_single;
    }
    else if (! Generate_Code_For_v4 ) {
      ErrMsg(EC_Conf_Targ, "processor", Targ_Name(Target),
             Targ_Name(TARGET_stxp70_v4_single));
    }
    break;

  default:
    ErrMsg(EC_Conf_Targ, "processor", Abi_Name(Target_ABI), 
           "undefined/unsupported");
    Target = TARGET_UNDEF;
    break;
  }

  // Now, determine target isa. If it has already been set,
  // perform some coherency check.
  if (Target_ISA==TARGET_ISA_UNDEF) {
     if( Generate_Code_For_v3 ) {
         Target_ISA = TARGET_ISA_stxp70_v3; 
         Use_ELF_32 = TRUE;
      }
     else if( Generate_Code_For_v4 ) {
         Target_ISA = TARGET_ISA_stxp70_v4; 
         Use_ELF_32 = FALSE;
      }
  } else {
      errmsg =
      "Incompatibilite between ELF e_flag (0x%x) and Target Processor (%s)\n";

      if( Generate_Code_For_v3 ) {
          FmtAssert(Target_ISA == TARGET_ISA_stxp70_v3 && Use_ELF_32 == TRUE,
                    (errmsg,(INT32)e_flags,Targ_Name(Target)));
      }

     if( Generate_Code_For_v4 ) {
          FmtAssert(Target_ISA == TARGET_ISA_stxp70_v4 && Use_ELF_32 == FALSE,
                    (errmsg,(INT32)e_flags,Targ_Name(Target)));
      }
  }
 
  *isa = (INT)Target_ISA;

  // Handle ABI.
  switch(e_flags & ELF_STxP70_ABI_MASK) {
  case ELF_STxP70_ABI_EMBED:
    if (Target_ABI == ABI_UNDEF) { 
      Target_ABI = ABI_STxP70_embedded;
    } 
    else if (Target_ABI != ABI_STxP70_embedded) {
      ErrMsg(EC_Conf_Targ, "abi", Abi_Name(Target_ABI), 
             Abi_Name(ABI_STxP70_embedded));
    }
    break;

  case ELF_STxP70_ABI_FPX:
    if (Target_ABI == ABI_UNDEF) { 
      Target_ABI = ABI_STxP70_fpx;
    }
    else if (Target_ABI != ABI_STxP70_fpx) {
      ErrMsg(EC_Conf_Targ, "abi", Abi_Name(Target_ABI), 
             Abi_Name(ABI_STxP70_fpx));
    }
    break;

  default:
    ErrMsg(EC_Conf_Targ, "abi", Abi_Name(Target_ABI), "undefined/unsupported");
    Target_ABI = ABI_UNDEF;
    break;
  }

  // Short doubles: track coherency between ELF flags and
  // commmand line options.
  errmsg =
  "Coherency report: either -fshort-double is missing, or one file\n"
  "has been compiled with -fshort-double and shouldn't have been.\n";

  if(TRUE==Double_Is_Short) {
     FmtAssert(0==(e_flags & EF_STxP70_DOUBLE64),(errmsg));
  } else {
     FmtAssert(0!=(e_flags & EF_STxP70_DOUBLE64),(errmsg));
  }
   
  return;
}

/* ====================================================================
 *   Config_ELF_From_Target
 *
 *   Return the ELF specific flags for the current target ABI and 
 *   core ISA subset.
 * ====================================================================
 */
Elf32_Word
Config_ELF_From_Target (
  BOOL is_64bit,
  BOOL old_abi,
  INT isa
)
{
  Elf32_Word e_flags;

  // [CL] setup ELF flags according to user options

  if (isa != (INT)TARGET_ISA_stxp70_v3 &&
      isa != (INT)TARGET_ISA_stxp70_v4) {
      ErrMsg ( EC_Inv_TARG, "isa", Isa_Name((TARGET_ISA)isa) );
  }

  switch(isa) {
     case TARGET_ISA_stxp70_v3:
       e_flags  = EF_STxP70;
       e_flags |= ELF_STxP70_CORE_stxp70_v3;
       break;

     case TARGET_ISA_stxp70_v4:
       e_flags  = EF_STxP70v4;
       e_flags |= ELF_STxP70_CORE_stxp70_v4; 
       break;

     default:
       ErrMsg ( EC_Inv_TARG, "isa", Isa_Name((TARGET_ISA)isa) );
       e_flags  = ELF_STxP70_CORE_UNDEF;
       break;
  }

  // Handle ABI
  switch(Target_ABI) {
     case ABI_STxP70_embedded:
       e_flags |= ELF_STxP70_ABI_EMBED; 
       break;

     case ABI_STxP70_fpx:
       e_flags |= ELF_STxP70_ABI_FPX; 
       break;

     default:
       e_flags |= ELF_STxP70_ABI_UNDEF; 
       break;
  }


  // if -fshort-double hasn't been set on command line
  if(FALSE==Double_Is_Short)
    e_flags |= EF_STxP70_DOUBLE64;

  return e_flags;
}

/* ====================================================================
 *   Get_Elf_Target_Machine
 * ====================================================================
 */
Elf32_Half 
Get_Elf_Target_Machine (void)
{
  // [CL] this ID is to be sync'ed with binutils
  return EM_STxP70;
}

/* =====================================================================
 *   ELF_WHIRL_has_subset_section
 *
 *   Returns TRUE if WHIRL files generated in IPA process (ipl) contains
 *   a specific section (.WHIRL.subset) containing target specific
 *   information on (extension) subsets.
 *
 *   In current implementation, .WHIRL.subset section is not emitted
 *   in files *.B emitted by gccfe (see usage of BACK_END preprocessor 
 *   constant).
 * =====================================================================
 */
BOOL
ELF_WHIRL_has_subset_section (void)
{
#if defined (BACK_END) 
    return TRUE;
#else
    return FALSE;
#endif
}

/* ======================================================================
 *   ELF_WHIRL_subset_info_buf
 *
 *   Returns a buffer that contains subset information for ELF WHIRL
 *   file or NULL.
 *
 *   Input:  pool (MEM_POOL*)  memory pool where buffer is to be allocated.
 *   Output: buf  (char**)     buffer address
 *           size (Elf64_Word) buffer size.
 *           
 *   In current implementation, .WHIRL.subset section is not emitted
 *   in files *.B emitted by gccfe (see usage of BACK_END preprocessor 
 *   constant).
 * =====================================================================
 */
void
ELF_WHIRL_subset_info_buf (MEM_POOL *pool, char **buf, Elf64_Word *size)
{

   /* TODO: V3 only. Do V4 */
#if defined (BACK_END)

#define EMPTY_WHIRL_RECONF_TABLE_ENTRY(id)  \
   {(mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, \
    (mUINT32)0, (mUINT32){0} }

#define EMPTY_WHIRL_CORE_RECONF_TABLE_ENTRY(id) \
   {(mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, (mUINT32)0, \
    (mUINT32)0, {'C', 'o', 'r', 'e', 0} }

   WHIRL_stxp70_reconf_info tab_ext[1]={EMPTY_WHIRL_RECONF_TABLE_ENTRY(0)};
   WHIRL_stxp70_reconf_info tab_cor[1]={EMPTY_WHIRL_CORE_RECONF_TABLE_ENTRY(0)};

   mUINT32 sztab;                          // Nb of items in WHIRL_info_tab.
   WHIRL_stxp70_reconf_info *WHIRL_info_tab;
   Extension_dll_t          *tab_dll;      // Loader table of extensions.
   Extension_dll_t          *cur_dll;
   mUINT32                   nb_dll;       // Number of dll extensions.
   mUINT32                   i;
   mUINT32                   index_WHIRL_tab;
   const mUINT32             max_nb_extensions = EL_MAX_EXT;
   
    
   sztab          = max_nb_extensions+1U;
   WHIRL_info_tab = TYPE_MEM_POOL_ALLOC_N(WHIRL_stxp70_reconf_info,pool,sztab);

   // Fill in the table with default values.
   for(i=0;i<sztab-1U;++i)
     WHIRL_info_tab[i] = tab_ext[0] ;
   WHIRL_info_tab[sztab-1U] = tab_cor[0] ;

   index_WHIRL_tab = 0U;

   // First handle native extensions.
   // X3 extension.
   if(TRUE==Enable_X3) {
     strcpy(WHIRL_info_tab[index_WHIRL_tab].extname,"x3");
     WHIRL_info_tab[index_WHIRL_tab].magic_key   = 0x0;
     WHIRL_info_tab[index_WHIRL_tab].is_fpx      = FALSE;
     WHIRL_info_tab[index_WHIRL_tab].is_available= TRUE;
     ++index_WHIRL_tab;
   }

   // Fpx extension.
   if(TRUE==Enable_Fpx) {
     strcpy(WHIRL_info_tab[index_WHIRL_tab].extname,"fpx");
     WHIRL_info_tab[index_WHIRL_tab].magic_key   = 0x0;
     WHIRL_info_tab[index_WHIRL_tab].is_fpx      = TRUE;
     WHIRL_info_tab[index_WHIRL_tab].is_available= TRUE;
     ++index_WHIRL_tab;
   }

   // Get table of extension dlls as seen by the loader.
   Get_Loader_Extension_Table(&tab_dll,&nb_dll);

   if(NULL!=tab_dll) {
    for(i=0;i<nb_dll;++i) {
     FmtAssert(index_WHIRL_tab<max_nb_extensions,
               ("More than %d extensions", max_nb_extensions));

     cur_dll=&tab_dll[i];

     FmtAssert(cur_dll->handler->extname != NULL &&
               strlen(cur_dll->handler->extname)<16,
               ("Error in extension name"));

     // Fill in table.
     strcpy(WHIRL_info_tab[index_WHIRL_tab].extname,
            cur_dll->handler->extname);
     WHIRL_info_tab[index_WHIRL_tab].magic_key      = 0x0;
     WHIRL_info_tab[index_WHIRL_tab].base_mtypes    = cur_dll->base_mtypes;
     WHIRL_info_tab[index_WHIRL_tab].base_mmodes    = cur_dll->base_mmodes;
     WHIRL_info_tab[index_WHIRL_tab].base_intrinsics= cur_dll->base_intrinsics;
     WHIRL_info_tab[index_WHIRL_tab].base_builtins  = cur_dll->base_builtins;
     WHIRL_info_tab[index_WHIRL_tab].is_fpx         = FALSE;
     WHIRL_info_tab[index_WHIRL_tab].is_available   = TRUE;
     index_WHIRL_tab++;
     }
  }

   // Set corecfg in the last entry of the table.
   if(0x0!=TARG_Corecfg) {
     WHIRL_info_tab[max_nb_extensions].magic_key    = TARG_Corecfg;
     WHIRL_info_tab[max_nb_extensions].is_available = TRUE;
   } 

  *buf  = (char*)WHIRL_info_tab;
  *size = (Elf64_Word)sztab*sizeof(WHIRL_stxp70_reconf_info);

#else   // BACKEND
  *buf = NULL;
  *size= 0;
#endif  // BACKEND

   return;
}


/* ======================================================================
 *   ELF_WHIRL_check_subset
 *
 *   Check that a subset section in correct.
 *
 * =====================================================================
 */
BOOL
ELF_WHIRL_check_subset (char *base, Elf64_Word size)
{
    mUINT32        tt_size;
    static BOOL    already_tested = FALSE;
    int            i;

    static WHIRL_stxp70_reconf_info   ref[EL_MAX_EXT+1U]; 
    static WHIRL_stxp70_reconf_info *test; 

    tt_size = (Elf64_Word)(EL_MAX_EXT+1U)*sizeof(WHIRL_stxp70_reconf_info);
    if(NULL == base ||  size < tt_size )
       return FALSE;

    test = (WHIRL_stxp70_reconf_info*) base;

    if(!already_tested) {
       // First test

       already_tested = TRUE;

       // Copy the whole stuff.
       memcpy((char*)ref,base,tt_size);  

       // Core must be present
       if(ref[EL_MAX_EXT].is_available != TRUE)
         return FALSE;

       char *core_name   = "Core";
       int   core_length = strlen(core_name);
       if(strncmp(ref[EL_MAX_EXT].extname,core_name,core_length))
         return FALSE;

    } else {

       // Normal test.
       // We require all the objects to have exactly the
       // same subset section. Tests will have to be refined when
       // WHIRL libraries are activated. In such a case, we would
       // have to consider corecfgs having different but compatible
       // values.
       for(i=0;i<EL_MAX_EXT+1U;++i) {
         // Check if extension is available.
         if(ref[i].is_available!=test[i].is_available)
              return FALSE;
         if(test[i].is_available==FALSE)
              continue;

         if(ref[i].magic_key != test[i].magic_key)
              return FALSE;
         if(ref[i].base_mtypes != test[i].base_mtypes)
              return FALSE;
         if(ref[i].base_mmodes != test[i].base_mmodes)
              return FALSE;
         if(ref[i].base_intrinsics != test[i].base_intrinsics)
              return FALSE;
         if(ref[i].base_builtins != test[i].base_builtins)
              return FALSE;
         if(ref[i].is_fpx != test[i].is_fpx)
              return FALSE;
         if(strncmp(ref[i].extname,test[i].extname,strlen(ref[i].extname)))
              return FALSE;
       }
    }
    return TRUE;
}
