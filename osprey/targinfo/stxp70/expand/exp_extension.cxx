/*
 
  Copyright (C) 2006 ST Microelectronics, Inc.  All Rights Reserved.
 
  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.
  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 
  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement
  or the like.  Any license provided herein, whether implied or
  otherwise, applies only to this software file.  Patent licenses, if
  any, provided herein do not apply to combinations of this program with
  other software, or any other product whatsoever.
  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.
 
  Contact information:  ST Microelectronics, Inc.,
  , or:
 
  http://www.st.com
 
  For further information regarding this notice, see:
 
  http:
*/

/*
 * Date: 2006/08/01
 * Desc: These functions are used to expand extension specific operations,
 *       such as compose/extract/widemove/load/store.
 *
 * This code expansion depends on several aspects:
 * - Size of extension registers,
 * - Number of sub-level in the register hierarchy,
 * - Available predefined instructions (they might be disabled by the
 *   extension designer)
 *
 * Here is a synthetic description of the code generation for the different
 * macro operations (LOAD, STORE, MOVE, ...). Possible cases are ordered by
 * preference.
 *
 * ===================================================================
 * == LOAD EXT sequence                                             ==
 * ===================================================================
 * -------------------------------------------------------------------
 * -- Level X (with X=128bits, splittable into 2x64,and 4x32)       --
 * -------------------------------------------------------------------
 * -> 1x LDX  Xr, @
 * -> 2x LDP  Xr_Pn, @
 * -> 4x LDQ  Xr_Qn, @
 * -> 4x LD core; MOVE Core->Ext (see below)
 *
 * -------------------------------------------------------------------
 * -- Level X (with X=512bits, splittable into 2x256,and 4x128)     --
 * -------------------------------------------------------------------
 * ->  1x LDX  Xr, @
 * ->  2x LDP  Xr_Pn, @
 * ->  4x LDQ  Xr_Qn, @
 * -> 16x LD core; MOVE Core->Ext (see below)
 *
 * ===================================================================
 * == STORE EXT sequence                                            ==
 * ===================================================================
 * -------------------------------------------------------------------
 * -- Level X (with X=128bits, splittable into 2x64,and 4x32)       --
 * -------------------------------------------------------------------
 * -> 1x STX  @, Xr
 * -> 2x STP  @, Xr_Pn
 * -> 4x STQ  @, Xr_Qn
 * -> MOVE Ext->Core (see below) ; 4x ST core
 *
 * -------------------------------------------------------------------
 * -- Level X (with X=512bits, splittable into 2x256,and 4x128)     --
 * -------------------------------------------------------------------
 * -> 1x STX  @, Xr
 * -> 2x STP  @, Xr_Pn
 * -> 4x STQ  @, Xr_Qn
 * -> MOVE Ext->Core (see below) ; 16x ST core
 *
 * ===================================================================
 * == CLEAR EXT sequence : Level X                                  ==
 * ===================================================================
 * -> 1x  CLRX
 * -> 2x  CLRP
 * -> 4x  CLRQ 
 * -> make  Rn, #0 ; 1x R2X  Xm, Rn
 * -> make  Rn, #0 ; 2x R2P  Xm, Rn
 * -> make  Rn, #0 ; 4x R2Q  Xm, Rn
 *
 * ===================================================================
 * == EXPAND Immediate sequence (32bits imm -> 1x Ext reg size)     ==
 * ===================================================================
 * -------------------------------------------------------------------
 * -- Level X, Sign extended                                        --
 * -------------------------------------------------------------------
 * -> CLEAR X (if imm == 0)
 * -> MAKE gpr, #imm ; R2X   (X size <= 32)
 * -> MAKE gpr, #imm ; R2X ; ( SIGNEXT )
 * -> MAKE gpr, #imm ; R2P ; SIGNEXT (see below)
 * -> MAKE gpr, #imm ; R2Q ; SIGNEXT (see below)
 *
 * -------------------------------------------------------------------
 * -- Level X, Zero extended                                        --
 * -------------------------------------------------------------------
 * -> CLEAR X (if imm == 0)
 * -> MAKE gpr, #imm ; R2X   (X size <= 32)
 * -> MAKE gpr, #imm ; R2X
 * -> MAKE gpr, #imm ; R2P ; ZEROEXT (see below)
 * -> MAKE gpr, #imm ; R2Q ; ZEROEXT (see below)
 *
 * ===================================================================
 * == SIGNEXT sequence                                              ==
 * ===================================================================
 * -> SIGNX
 * -> P2SX
 * -> SIGNP; P2SX
 * -> Q2SP ; P2SX
 * -> SIGNQ; Q2SP; P2SX
 *
 * ===================================================================
 * == ZEROEXT sequence                                              ==
 * ===================================================================
 * -> ( ZEROX ) not used, as input data are already zero extended
 * -> P2UX
 * -> CLRX ; COMPOSE 
 * -> Q2UP ; P2UX
 *
 * ===================================================================
 * == MOVE sequence : Ext->Core                                     ==
 * ===================================================================
 * -------------------------------------------------------------------
 * -- Level X (with X=128bits, splittable into 2x64,and 4x32)       --
 * -------------------------------------------------------------------
 * -> 4x CSX2R
 * -> 2x ( 2x CSP2R )
 * -> 4x ( 1x CSQ2R )
 *
 * -> 1x STORE EXT ; Nx LOAD GPR (in tempo stack location)
 *
 * -------------------------------------------------------------------
 * -- Level X (with X=512bits, splittable into 2x256,and 4x128)     --
 * -------------------------------------------------------------------
 * -> 16x CSX2R
 * ->  2x ( 8x CSP2R )
 * ->  4x ( 4x CSQ2R )
 *
 * -> 1x STORE EXT ; Nx LOAD GPR (through tempo stack location)
 *
 * ===================================================================
 * == MOVE sequence : Core->Ext                                     ==
 * ===================================================================
 * -------------------------------------------------------------------
 * -- Level X (with X=128bits, splittable into 2x64,and 4x32)       --
 * -------------------------------------------------------------------
 * -> 1x RR2X ; 1x RR2CSX
 * -> 1x R2X  ; 3x R2CSX
 * -> 2x ( 1x RR2P )                
 * -> 2x ( 1x R2P  ; 1x  R2CSP  )   
 * -> 4x ( 1x R2Q  )                
 *
 * -> KILL X ; 2x RR2CSX            
 * -> KILL X ; 4x R2CSX             
 * -> KILL X ; 2x ( 1x RR2CSP )     
 * -> KILL X ; 2x ( 2x R2CSP )      
 * -> KILL X ; 4x ( 1x R2CSQ )      
 *
 * -> 1x STORE GPR ; Nx LOAD EXT (through tempo stack location)
 *
 * -------------------------------------------------------------------
 * -- Level X (with X=512bits, splittable into 2x256,and 4x128)     --
 * -------------------------------------------------------------------
 * -> 1x RR2X ;  7x RR2CSX
 * -> 1x R2X  ; 15x R2CSX
 * -> 2x ( 1x RR2P ; 3x  RR2CSP )   
 * -> 2x ( 1x R2P  ; 7x  R2CSP  )   
 * -> 4x ( 1x RR2Q ; 1x  RR2CSQ )   
 * -> 4x ( 1x R2Q  ; 3x  R2CSQ  )   
 *
 * -> KILL X ;  8x RR2CSX
 * -> KILL X ; 16x R2CSX
 * -> KILL X ;  2x ( 4x  RR2CSP )   
 * -> KILL X ;  2x ( 8x  R2CSP  )   
 * -> KILL X ;  4x ( 2x  RR2CSQ )   
 * -> KILL X ;  4x ( 4x  R2CSQ  )   
 *
 * -> 1x STORE GPR ; Nx LOAD EXT (through tempo stack location)
 *
 * Note 1: Some weird cases are not yet handled, for instance if we have
 *         RR2P and R2CSP, but not RR2CSP.
 * Note 2: KILL X is a dummy operation, used only for correct liveness
 *         analysis. It will be ignored when emitting assembly code.
 */

#include "exp_extension.h"
#include "exp_targ.h"
#include "cgexp.h"
#include "erglob.h"

#include "cg_spill.h"

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

// Maximum number of subparts for extension registers
#define MAX_SUB_TNS (REGISTER_COMPOSITE_SIZE_MAX)

/* [JV] Defined in exp_targ.cxx */
extern ISA_LIT_CLASS Get_Lit_Class_For_Memory_Access_Size( INT32 size, BOOL is_signed );

/* =============================================================================
 * Structure and enumeration used to describe code generation of
 * move between Extension registers and Core registers
 * =============================================================================
 */
enum {
  // ..Comments specify the number of acces per level,
  // ..level being X, P or Q
  EXTCG_FAILURE,                // No code generation found
  // ... Move GPR -> Ext ...
  EXTCG_INSERT_R2X,             // 1x insert                   (1xGPR input)
  EXTCG_INSERT_RR2X,            // 1x insert                   (2xGPR input)
  EXTCG_INSERT_R2X_R2CSX,       // 1x insert, Nx rotate_insert (1xGPRs input)
  EXTCG_INSERT_RR2X_RR2CSX,     // 1x insert, Nx rotate_insert (2xGPR  input)
  EXTCG_KILL_THEN_INSERT_R2CSX, // 1x KILL,   Nx rotate_insert (1xGPRs input)
  EXTCG_KILL_THEN_INSERT_RR2CSX,// 1x KILL,   Nx rotate_insert (2xGPRs input)
  EXTCG_STORE_GPR_LOAD_EXT,     // Nx store GPR, 1x load Ext
  // ... Move Ext -> GPR ...
  EXTCG_EXTRACT_X2R,            // 1x extract
  EXTCG_EXTRACT_CSX2R,          // Nx extract_rotate
  EXTCG_STORE_EXT_LOAD_GPR      // 1x store Ext, Nx load GPR
};

// If <nb_sub_TNs> > 1, it means that the selected TOPs operate on subpart
//                      of the initial Ext TN, that must then be  splitted.
typedef struct {
  INT type;                 // Selected code generation
  INT nb_sub_TNs;           // Subdivision required for initial Ext operand
  INT sub_TNs_size;         // Size of Ext operands for selected TOP
  INT gpr_per_op;           // Number of input GPRs taken by top_insert/top_rotate_insert
  union {
    TOP top_insert;         // R2X, RR2X or undef
    TOP top_extract;        // X2R or undef
  };
  union {
    TOP top_rotate_insert;  // R2CSX, RR2CSX or undef
    TOP top_extract_rotate; // CSX2R or undef
  };    
} extcg_move_corext_t;


/* =============================================================================
 * Functions for allocation handling
 * =============================================================================
 */

/**
 * Check whether given register set has nbRequired contiguous register from
 * reg.
 *
 * @param  reg Register, start point of the contiguous numbering check
 * @param  currSet Register set to be checked
 * @param  usedSet [out] Will contained found contiguous registers
 * @param  nbRequired number of contiguous register to be found to return true
 *
 * @pre    usedSet->isEmpty()
 * @post   if result then usedSet->size() = nbRequired and it exists an order
 *         in usedSet such as elements of usedSet are contiguous else
 *         currSet does not containt nbRequired contiguous register with start
 *         point reg.
 *
 * @return true if currSet has nbRequired contiguous register, false otherwise
 */
static bool
HasContiguousNumbering(REGISTER reg, const REGISTER_SET& currSet,
                       REGISTER_SET& usedSet, int nbRequired)
{
  REGISTER prevReg = reg;
  usedSet = REGISTER_SET_Union1(usedSet, reg);
  --nbRequired;
  while(nbRequired && (reg = REGISTER_SET_Choose_Next(currSet, reg))
	!= REGISTER_UNDEFINED) {
    if(reg == prevReg + 1) {
      --nbRequired;
      prevReg = reg;
      usedSet = REGISTER_SET_Union1(usedSet, reg);
    }
  }
  return nbRequired == 0;
}

/**
 * Seek for an available extension dedicated register defined by rclass and
 * byteSize.
 *
 * @param  availSet Array of available register set
 * @param  rclass register class for which we want a register
 * @param  byteSize Size in byte of the need register
 * @param  preferedReg [optionnal] Specify we want to have resource corresponding to
 *         this register
 *
 * @pre    availSet->size() >= ISA_REGISTER_CLASS_MAX + 1 and rclass is a valid
 *         ISA_REGISTER_CLASS index
 * @post   result != REGISTER_UNDEFINED implies result and its subpart have
 *         been removed from availSet[rclass] 
 *         
 *
 * @return Found dedicated register or REGISTER_UNDEFINED
 */
static REGISTER
SeekForExtensionRegister(REGISTER_SET* availSet,
                         ISA_REGISTER_CLASS rclass, INT byteSize,
                         REGISTER preferedReg = REGISTER_UNDEFINED) {
  REGISTER reg = REGISTER_UNDEFINED;
  REGISTER_SET& currSet = availSet[rclass];
  REGISTER_SET usedSet = REGISTER_SET_EMPTY_SET;
  REGISTER first = REGISTER_SET_Choose(currSet);
  int regAlignment = byteSize * CHAR_BIT /
    ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rclass));
  DevAssert(regAlignment > 0, ("size must be at least equal to the register "
			       "class wide, since it is the smallest for "
			       "this bed!"));
  while(first != REGISTER_UNDEFINED) {
    // Use hw numbering -> REGISTER_MIN offset
    if((preferedReg == REGISTER_UNDEFINED || preferedReg == first) &&
       ((first - REGISTER_MIN) % regAlignment == 0) &&
       HasContiguousNumbering(first, currSet, usedSet,
			      regAlignment)) {
      reg = first;
      break;
    }
    else {
      usedSet = REGISTER_SET_EMPTY_SET;
    }
    first = REGISTER_SET_Choose_Next(currSet, first);
  }
  currSet = REGISTER_SET_Difference(currSet, usedSet);
  return reg;
}

/**
 * @see exp_extension.h
 */
TN*
EXTENSION_Gen_Register_TN_From_Available_Set(REGISTER_SET* availSet,
                                             ISA_REGISTER_CLASS rclass,
                                             INT byteSize)
{
  TN* res;
  if(availSet) {
    REGISTER_SET& regs = availSet[rclass];
    const char* rcName =
      ISA_REGISTER_CLASS_INFO_Name(ISA_REGISTER_CLASS_Info(rclass));
    REGISTER reg;
    if(rclass > ISA_REGISTER_CLASS_STATIC_MAX) {
      reg = SeekForExtensionRegister(availSet, rclass, byteSize);
      FmtAssert(reg != REGISTER_UNDEFINED,
		("No extension register left for %s!!!", rcName));
    }
    else {
      reg = REGISTER_SET_Choose(regs);
      regs = REGISTER_SET_Difference1(regs, reg);
      FmtAssert(reg != REGISTER_UNDEFINED,
		("No register left for %s!!!", rcName));
    }
    res = Build_Dedicated_TN(rclass, reg, byteSize);
  }
  else {
    res = Gen_Register_TN(rclass, byteSize);
  }
  return res;
}

/**
 * Release resource used by given tn in availSet.
 * Used resource are register and related sub part if any
 *
 * @param  availSet Array of available set
 * @param  tn temporary name to be released
 *
 * @pre    tn is an extension register and
 *         availSet->size() >= ISA_REGISTER_CLASS_MAX + 1
 * @post   Resource used by tn has been added to availSet
 *
 */
static void
ReleaseExtensionRegister(REGISTER_SET* availSet, TN* tn)
{
  ISA_REGISTER_CLASS rclass = TN_register_class(tn);
  REGISTER reg = TN_register(tn);
  REGISTER_SET& currSet = availSet[rclass];
  int regAlignment = TN_size(tn) * CHAR_BIT /
    ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rclass));
  DevAssert(regAlignment > 0, ("size must be at least equal to the register "
			       "class wide, since it is the smallest for "
			       "this bed!"));
  int i;
  for(i = 0; i < regAlignment; ++i) {
    currSet = REGISTER_SET_Union1(currSet, reg + i);
  }
}

/**
 * @see exp_extension.h
 */
void
EXTENSION_Release_Register_TN(REGISTER_SET* availSet, TN* tn)
{
  if(tn && availSet) {
    ISA_REGISTER_CLASS regClass = TN_register_class(tn);
    if(TN_is_dedicated(tn)) {
      if(regClass > ISA_REGISTER_CLASS_STATIC_MAX) {
	ReleaseExtensionRegister(availSet, tn);
      }
      else {
	availSet[regClass] =
	  REGISTER_SET_Union1(availSet[regClass],
			      TN_register(tn));
      }
    }
  }
}

/**
 * Assign a list of sub register according to parameters.
 * The purpose of this function is to create dedicated sub part using given src
 * tn.
 *
 * @param  availSet Define available resources. This parameter must be not null
 *         to generate allocated code
 * @param  src_subs Array that will receive created sub parts
 * @param  src Source tn from which we will create the sub parts
 * @param  nb_sub Number of needed sub parts
 * @param  subClassByteSize Size in byte of a sub part
 * @param  isAllocated [out] Specify whether created sub parts are allocated
 *         or not
 *
 * @pre    src_subs and src_subs->size() = nb_sub
 * @post   *isAllocated implies src_subs->forAll(tn | TN_is_dedicated(tn))
 *
 */
static void
Create_Sub_Regs(REGISTER_SET* availSet, TN** src_subs, TN* src, INT nb_sub,
                INT subClassByteSize, bool* isAllocated)
{
  if(isAllocated) {
    *isAllocated = availSet && TN_is_dedicated(src);
  }
  if(availSet && TN_is_dedicated(src)) {
    int i;
    DevAssert(TN_size(src) / subClassByteSize <= nb_sub,
	      ("Ask more subpart than we are sure to have"));
    int regAlignment = subClassByteSize * CHAR_BIT /
      ISA_REGISTER_CLASS_INFO_Bit_Size
      (ISA_REGISTER_CLASS_Info(TN_register_class(src)));
    DevAssert((TN_register(src) - REGISTER_MIN) % regAlignment == 0,
	      ("It is assumed that registers' number for upper level"
	       " is the smallest number of  lower register that "
	       "composed it (e.g. hwX1 = P1, P2 => X1, hwX2 = P3, P4 =>"
	       " X4)"));
    for(i = 0; i < nb_sub; ++i) {
      src_subs[i] = Build_Dedicated_TN(TN_register_class(src),
				       TN_register(src) +
				       i * regAlignment,
				       subClassByteSize);
    }
  }
  else {
    int i;
    for(i = 0; i < nb_sub; ++i) {
      src_subs[i] = Gen_Register_TN(TN_register_class(src),
				    subClassByteSize);
    }
  }
}

void
EXTENSION_Take_Register_TN(REGISTER_SET* availSet, TN* tn)
{
  if(tn && availSet) {
    ISA_REGISTER_CLASS regClass = TN_register_class(tn);
    if(TN_is_dedicated(tn)) {
      if(regClass > ISA_REGISTER_CLASS_STATIC_MAX) {
	REGISTER reg =
	  SeekForExtensionRegister(availSet,
				   TN_register_class(tn),
				   TN_size(tn), TN_register(tn));
      }
      else {
	availSet[regClass] =
	  REGISTER_SET_Difference1(availSet[regClass],
				   TN_register(tn));
      }
    }
  }
}

static void
CopyRegisterSets(REGISTER_SET* dst, const REGISTER_SET* const src)
{
  if(dst && src) {
    ISA_REGISTER_CLASS i;
    FOR_ALL_ISA_REGISTER_CLASS(i) {
      dst[i] = src[i];
    }
  }
}

static BOOL
RestoreAvailableResource(TN* tn, bool isLoad, BOOL success, REGISTER_SET* availSet,
                         const REGISTER_SET* const saveSet)
{
  CopyRegisterSets(availSet, saveSet);
  if(success) {
    if(isLoad) {
      EXTENSION_Take_Register_TN(availSet, tn);
    }
    else {
      EXTENSION_Release_Register_TN(availSet, tn);
    }
  }
  return success;
}

static BOOL
RestoreAvailableResourceForLoad(TN* tn, BOOL success, REGISTER_SET* availSet,
                                const REGISTER_SET* const saveSet)
{
  return RestoreAvailableResource(tn, true, success, availSet, saveSet);
}

static BOOL
RestoreAvailableResourceForStore(TN* tn, BOOL success, REGISTER_SET* availSet,
                                const REGISTER_SET* const saveSet)
{
  return RestoreAvailableResource(tn, false, success, availSet, saveSet);
}


/* =============================================================================
 * Macro used to simplify search of available TOP in extension, by trying getting
 * a specific TOP for different register size, starting with the biggest one.
 * Parameters:
 * IN_function      : Function called to try retrieving a TOP
 *                    Expected prototype:  TOP IN_function(INT size)
 * IN_biggest_rsize : Preferred register width
 * IN_biggest_rsize : Last possible register width
 * OUT_top          : Resulting TOP, TOP_UNDEFINED if not found
 * OUT_rsize        : Register size used by returned TOP
 * =============================================================================
 */
#define GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(IN_function,			\
					   IN_biggest_rsize,		\
					   IN_smallest_rsize,		\
					   OUT_top,			\
					   OUT_rsize)			\
  {									\
    OUT_rsize = IN_biggest_rsize;					\
    OUT_top   = IN_function (OUT_rsize);				\
    while ((OUT_top == TOP_UNDEFINED) &&				\
	   (OUT_rsize > IN_smallest_rsize)) {				\
      OUT_rsize = OUT_rsize/2;						\
      OUT_top   = IN_function (OUT_rsize);				\
    }									\
  }

// [TTh] 2006/08/01: Predicated compose/extract are not yet fully supported by
//       the compiler, explaining why they are currently disabled.
//
// #define PREDICATED_COMPOSE_EXTRACT

/* =============================================================================
 * Build a simulated extract TOP, that extract 2 sub-TNs from a composite one.
 * =============================================================================
 */
void Build_OP_simulated_extract(TN *tgt0, TN *tgt1, TN *guard, TN *src, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *results[2];
  TN *operands[2];

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(src));
  top = rc_info->get_simulated_extract_TOP(TN_size(src), TN_size(tgt0));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated extract TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src), TN_size(tgt0)));

  // Build result array
  results[0]  = tgt0;
  results[1]  = tgt1;

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  operands[1] = src;
#else
  operands[0] = src;
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, 2, 2, results, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, 2, 1, results, operands));
#endif
}


/* =============================================================================
 * Build a simulated extract TOP, that extract 4 sub-TNs from a composite one.
 * =============================================================================
 */
void Build_OP_simulated_extract(TN *tgt0, TN *tgt1, TN *tgt2, TN *tgt3, TN *guard, TN *src, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *results[4];
  TN *operands[2];

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(src));
  top = rc_info->get_simulated_extract_TOP(TN_size(src), TN_size(tgt0));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated extract TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src), TN_size(tgt0)));

  // Build result array
  results[0]  = tgt0;
  results[1]  = tgt1;
  results[2]  = tgt2;
  results[3]  = tgt3;

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  operands[1] = src;
#else
  operands[0] = src;
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, 4, 2, results, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, 4, 1, results, operands));
#endif
}


/* =============================================================================
 * Build a simulated extract TOP, that extract <nb_tgt> sub-TNs from a
 * composite TN (<tgt_tab> elements in little-endian ordering).
 * =============================================================================
 */
void Build_OP_simulated_extract(INT nb_tgt, TN **tgt_tab, TN *guard, TN *src, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *operands[2];

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(src));
  top = rc_info->get_simulated_extract_TOP(TN_size(src), TN_size(tgt_tab[0]));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated extract TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src), TN_size(tgt_tab[0])));

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  operands[1] = src;
#else
  operands[0] = src;
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, nb_tgt, 2, tgt_tab, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, nb_tgt, 1, tgt_tab, operands));
#endif
}


/* =============================================================================
 * Build a simulated compose TOP, that produce a composite TN from 2 sub-TNs.
 * =============================================================================
 */
void Build_OP_simulated_compose(TN *tgt, TN *guard, TN *src0, TN *src1, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *results[1];
  TN *operands[3];

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(tgt));
  top = rc_info->get_simulated_compose_TOP(TN_size(src0), TN_size(tgt));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated compose TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src0), TN_size(tgt)));

  // Build result array
  results[0]  = tgt;

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  operands[1] = src0;
  operands[2] = src1;
#else
  operands[0] = src0;
  operands[1] = src1;
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, 1, 3, results, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, 1, 2, results, operands));
#endif
}


/* =============================================================================
 * Build a simulated compose TOP, that produce a composite TN from 4 sub-TNs.
 * =============================================================================
 */
void Build_OP_simulated_compose(TN *tgt, TN *guard, TN *src0, TN *src1, TN *src2, TN *src3, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *results[1];
  TN *operands[5];

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(tgt));
  top = rc_info->get_simulated_compose_TOP(TN_size(src0), TN_size(tgt));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated compose TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src0), TN_size(tgt)));

  // Build result array
  results[0]  = tgt;

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  operands[1] = src0;
  operands[2] = src1;
  operands[3] = src2;
  operands[4] = src3;
#else
  operands[0] = src0;
  operands[1] = src1;
  operands[2] = src2;
  operands[3] = src3;
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, 1, 5, results, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, 1, 4, results, operands));
#endif
}


/* =============================================================================
 * Build a simulated compose TOP, that produce a composite TN from <nb_src_TNs>
 * sub-TNs (<src_tab> elements in little-endian ordering).
 * =============================================================================
 */
void Build_OP_simulated_compose(TN *tgt, TN *guard, INT nb_src, TN **src_tab, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  TN *results[1];
#ifdef PREDICATED_COMPOSE_EXTRACT
  TN *operands[1+MAX_SUB_TNS];
#endif

  // Retrieve TOP
  rc_info = EXTENSION_get_REGISTER_CLASS_info(TN_register_class(tgt));
  top = rc_info->get_simulated_compose_TOP(TN_size(src_tab[0]), TN_size(tgt));
  FmtAssert((top != TOP_UNDEFINED),
	    ("Cannot find simulated compose TOP(src size:%d, tgt size:%d)\n",
	     TN_size(src_tab[0]), TN_size(tgt)));

  // Build result array
  results[0]  = tgt;

  // Build operand array
#ifdef PREDICATED_COMPOSE_EXTRACT
  operands[0] = guard;
  memcpy(&operands[1], src_tab, nb_src*sizeof(TN*));
#endif

  // Create op
#ifdef PREDICATED_COMPOSE_EXTRACT
  OPS_Append_Op(ops, Mk_VarOP(top, 1, nb_src+1, results, operands));
#else
  OPS_Append_Op(ops, Mk_VarOP(top, 1, nb_src, results, src_tab));
#endif
}


/* =============================================================================
 * Build a pseudo KILL TOP, to simulate the initialization of the specified TN.
 * =============================================================================
 */
static inline void Build_OP_KILL(TN *tn, OPS *ops) {
  OPS_Append_Op(ops, Mk_VarOP(TOP_KILL, 1, 0, &tn, NULL));
}


/* =============================================================================
 * Return true if the alignment constraint of <top> operation is smaller
 * or equal to data_align.
 * Return false otherwise.
 * =============================================================================
 */
static inline BOOL IsCompatibleAlignment(TOP top, UINT32 data_align) {
  UINT32 top_align;
  top_align = TOP_Mem_Alignment (top);
  return ((data_align == 0) || (top_align <= data_align));
}

/* =============================================================================
 * Check if it exists a memory access sequence between memory and the specified
 * extension register type, that use only extension load (or store) operations
 * (no move btwn extension and core registers required).
 * The function returns the compatible load (or store) TOP if found, 
 * TOP_UNDEFINED otherwise.
 * If only a misaligned load is found, TOP_UNDEFINED is returned and
 * <top_misalign> is assigned with this TOP.
 *
 * [IN]  reg_rc        : register class of the register to load/store
 * [IN]  reg_size      : size to load/store
 * [IN]  data_align    : alignment of the memory location
 * [IN]  is_load       : Is load (TRUE) or store (FALSE) operation wanted
 * [IN]  base_type     : Addressing mode base type
 * [IN]  offs_is_imm   : Addressing mode with immediate offset?
 * [IN]  offs_is_incr  : Addressing mode with positive offset?
 * [OUT] top_misalign  : Return the potential misalign TOP available, if the
 *                       ptr is not NULL
 * =============================================================================
 */
TOP EXTENSION_Get_Real_Load_Store(ISA_REGISTER_CLASS reg_rc,
					 INT                reg_size,
					 UINT32             data_align,
					 BOOL               is_load,
					 AM_Base_Reg_Type   base_type,
					 BOOL               offs_is_imm,
					 BOOL               offs_is_incr,
					 TOP               *top_misalign,
                                         UINT32             pixel_size) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top;
  INT current_access_size;
  INT reg_rc_size;

  reg_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(reg_rc)) / CHAR_BIT;
  rc_info     = EXTENSION_get_REGISTER_CLASS_info(reg_rc);

  // ##########################################
  // ## Retrieve the widest TOP              ##
  // ## for current TN and define the number ##
  // ## of access required (1, 2 or 4 parts) ##
  // ##########################################
  top = TOP_UNDEFINED;
  if (top_misalign) {
    *top_misalign = TOP_UNDEFINED;
  }
  current_access_size = reg_size*2;
  while (top == TOP_UNDEFINED && current_access_size >= reg_rc_size) {
    current_access_size /= 2;
    if (is_load) {
      top = rc_info->get_load_TOP(/* reg size     */current_access_size,
				  /* base_type    */base_type,
				  /* offs_is_imm  */offs_is_imm,
				  /* offs_is_incr */offs_is_incr,
                                  /* pixel_size   */pixel_size);
    } else {
      top = rc_info->get_store_TOP(/* reg size     */current_access_size,
				   /* base_type    */base_type,
				   /* offs_is_imm  */offs_is_imm,
				   /* offs_is_incr */offs_is_incr,
                                   /* pixel_size   */pixel_size);
    }

    if ((top != TOP_UNDEFINED) && (!IsCompatibleAlignment(top, data_align))) {
      // ...Found it but alignment constraint too strong.
      if (top_misalign &&
	  ((*top_misalign == TOP_UNDEFINED) ||
	   (TOP_Mem_Alignment(top) < TOP_Mem_Alignment(*top_misalign)))) {
	// ...Keep misalign TOP with the smaller alignment constraint
	*top_misalign = top;
      }
      top = TOP_UNDEFINED;
    }
  }

  return (top);
}


/* =============================================================================
 * Generate a temporary location on stack for register move between extension
  * registers and GPRs, and return a pointer to it.
 * For a given function, the same location is shared between all TNs of the same
 * type.
 * =============================================================================
 */
static ST *
Allocate_Stack_Location_For_Memory_Move(TN *ext_tn) {
  static PU_IDX pu_id = UINT_MAX;
  static ST  *mtyp_to_loc[MTYPE_MAX_LIMIT+1];
  const char *base_name = "extmov";

  // Initialize location table for new proc
  if (pu_id == UINT_MAX ||
      pu_id != ST_pu(Get_Current_PU_ST())) {
    pu_id = ST_pu(Get_Current_PU_ST());
    memset(mtyp_to_loc, 0, (MTYPE_MAX_LIMIT+1) * sizeof(ST*));
  }

  // Retrieve types from reg
  TY_IDX      mem_type  = CGTARG_Spill_Type [Spill_Type_Index(ext_tn)];
  CLASS_INDEX mem_mtype = CGTARG_Spill_Mtype[Spill_Type_Index(ext_tn)];
  FmtAssert(mem_mtype<=MTYPE_MAX_LIMIT, ("Unexpected type id"));
    
  if (mtyp_to_loc[mem_mtype] == NULL) {
    mtyp_to_loc[mem_mtype] = CGSPILL_Gen_Spill_Symbol (mem_type, base_name);
  }

  return mtyp_to_loc[mem_mtype];
}


/* =============================================================================
 * Return TRUE if predefined instrucions might be used to clear the content
 * of the extension register class <rc>, for register of size <size>.
 * Return FALSE otherwise.
 * =============================================================================
 */
static BOOL EXTENSION_Exist_Clr(ISA_REGISTER_CLASS rc, INT size) {
  const EXTENSION_Regclass_Info *rc_info;
  rc_info = EXTENSION_get_REGISTER_CLASS_info(rc);
  TOP top;
  INT top_rsize;
  INT rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rc)) / CHAR_BIT;

  // Try CLRX, CLRP or CLRQ
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_clear_TOP,
				     /* IN_biggest_rsize  */ size,
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top,
				     /* OUT_rsize         */ top_rsize);
  if (top != TOP_UNDEFINED) {
    return (TRUE);
  }

  // Try R2X, R2P or R2Q
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_insert_and_zeroext_R2X_TOP,
				     /* IN_biggest_rsize  */ size,
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top,
				     /* OUT_rsize         */ top_rsize);
  if (top != TOP_UNDEFINED) {
    return (TRUE);
  }

  // Try RR2X, RR2P or RR2Q
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_insert_and_zeroext_RR2X_TOP,
				     /* IN_biggest_rsize  */ size,
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top,
				     /* OUT_rsize         */ top_rsize);
  if (top != TOP_UNDEFINED) {
    return (TRUE);
  }

  return (FALSE);
}


/* =============================================================================
 * Generate code to clear the content of <tn> and return TRUE if successful,
 * FALSE otherwise.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Clr(TN *tn, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS rc;
  INT rc_size;
  TN *sub_TNs[MAX_SUB_TNS];
  INT sub_TNs_size;
  INT top_r2x_sub_TNs_size;
  INT top_rr2x_sub_TNs_size;
  INT nb_sub_TNs;
  TOP top_clr, top_rr2x, top_r2x, top;
  INT i;

  rc      = TN_register_class(tn);
  rc_info = EXTENSION_get_REGISTER_CLASS_info(rc);
  rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rc)) / CHAR_BIT;

  // First Try with simple 1x  CLRX, 2x  CLRP or  4x  CLRQ
  //
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_clear_TOP,
				     /* IN_biggest_rsize  */ TN_size(tn),
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top_clr,
				     /* OUT_rsize         */ sub_TNs_size);
  if (top_clr != TOP_UNDEFINED) {
    nb_sub_TNs = TN_size(tn) / sub_TNs_size;
    if (nb_sub_TNs == 1) {
      Build_OP(top_clr, tn, True_TN, ops);
    }
    else {  // nb_sub_TNs > 1
      for (i=0; i<nb_sub_TNs; i++) {
	// Create tempo sub-registers and clear them
	sub_TNs[i] = Gen_Register_TN(rc, sub_TNs_size);
	Build_OP(top_clr, sub_TNs[i], True_TN, ops);
      }
      // Compose the dest register
      Build_OP_simulated_compose(tn, True_TN, nb_sub_TNs, sub_TNs, ops);
    }
    return TRUE;
  }

  // Now Try with 1x  R2X,  2x  R2P,  4x  R2Q
  //           or 1x  RR2X, 2x  RR2P, 4x  RR2Q
  //  
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_insert_and_zeroext_R2X_TOP,
				     /* IN_biggest_rsize  */ TN_size(tn),
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top_r2x,
				     /* OUT_rsize         */ top_r2x_sub_TNs_size);

  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_insert_and_zeroext_RR2X_TOP,
				     /* IN_biggest_rsize  */ TN_size(tn),
				     /* IN_smallest_rsize */ rc_size,
				     /* OUT_top           */ top_rr2x,
				     /* OUT_rsize         */ top_rr2x_sub_TNs_size);
  
  if (top_r2x == TOP_UNDEFINED && top_rr2x == TOP_UNDEFINED) {
    return FALSE;
  }
  BOOL use_single_gpr = ((top_r2x != TOP_UNDEFINED) &&
			 ((top_rr2x == TOP_UNDEFINED) ||
			  top_r2x_sub_TNs_size >= top_rr2x_sub_TNs_size));
  if (use_single_gpr) {
    top = top_r2x;
    sub_TNs_size = top_r2x_sub_TNs_size;
  }
  else {
    top = top_rr2x;
    sub_TNs_size = top_rr2x_sub_TNs_size;
  }
  
  // Expand zero in GPR
  TN *gpr, *zero;
  gpr  = Gen_Register_TN(ISA_REGISTER_CLASS_gpr, 4);
  zero = Gen_Literal_TN(0, 4);
  Expand_Immediate(gpr, zero, FALSE, ops);

  nb_sub_TNs = TN_size(tn) / sub_TNs_size;
  if (nb_sub_TNs == 1) {
    if (use_single_gpr) {
      Build_OP(top, tn, True_TN, gpr, ops);
    } else {
      Build_OP(top, tn, True_TN, gpr, gpr, ops);
    }
  }
  else {  // nb_sub_TNs > 1
    for (i=0; i<nb_sub_TNs; i++) {
      // Create tempo sub-registers and clear them
      sub_TNs[i] = Gen_Register_TN(rc, sub_TNs_size);
      if (use_single_gpr) {
	Build_OP(top, sub_TNs[i], True_TN, gpr, ops);
      } else {
	Build_OP(top, sub_TNs[i], True_TN, gpr, gpr, ops);
      }
    }
    // Compose the dest register
    Build_OP_simulated_compose(tn, True_TN, nb_sub_TNs, sub_TNs, ops);
  }
  return TRUE;
}


/* =============================================================================
 * Return TRUE if it exists a single non-simulated instruction to copy
 * a composite register of class <rc> made of <nhardregs> atomic regs.
 * =============================================================================
 */
BOOL EXTENSION_Exist_Single_OP_Copy(ISA_REGISTER_CLASS rc, INT nhardregs) {
  const EXTENSION_Regclass_Info *rc_info = EXTENSION_get_REGISTER_CLASS_info(rc);
  INT size = (ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rc)) / CHAR_BIT) * nhardregs;

  return (rc_info->get_move_X2X_TOP(size) != TOP_UNDEFINED);
}


/* =============================================================================
 * Expand a copy operation between 2 TNs, more likely to be extension ones.
 * - It can produce one or more move operations, depending on the TOP 
 *   availables for the corresponding register class.
 * - If the TNs are not yet allocated and there is no single move TOP 
 *   available for this register size, a simulated widemove TOP is
 *   generated, to help the register allocator.
 * pre-condition: at least one of the tn register belongs to an extension
 * =============================================================================
 */
BOOL EXTENSION_Expand_Copy(TN *tgt_tn, TN *guard, TN *src_tn, OPS *ops) {
  const EXTENSION_Regclass_Info *rc_info;
  INT top_move_rsize;
  TOP top_move = TOP_UNDEFINED;
  INT nb_access;
  INT i;

  ISA_REGISTER_CLASS tgt_rc = TN_register_class(tgt_tn);
  ISA_REGISTER_CLASS src_rc = TN_register_class(src_tn);

  INT tgt_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(tgt_rc)) / CHAR_BIT;

  INT tgt_size = TN_size(tgt_tn);
  INT src_size = TN_size(src_tn);

  if (tgt_rc == src_rc) {

    FmtAssert((tgt_size == src_size), ("Cannot expand extension copy, because of incompatible size"));

    rc_info = EXTENSION_get_REGISTER_CLASS_info(src_rc);

    // Retrieve the widest move TOP for current TN and
    // define the number of access required (1, 2 or 4 parts)
    GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_move_X2X_TOP,
				       /* IN_biggest_rsize  */ tgt_size,
				       /* IN_smallest_rsize */ tgt_rc_size,
				       /* OUT_top           */ top_move,
				       /* OUT_rsize         */ top_move_rsize);
    if (top_move == TOP_UNDEFINED) {
      const char* rcName =
        ISA_REGISTER_CLASS_INFO_Name(ISA_REGISTER_CLASS_Info(tgt_rc));
      FmtAssert(0, ("Cannot generate Copy between registers of regclass %s, no instruction (TOP) found", rcName));
    }
    nb_access = tgt_size / top_move_rsize;

    // ###############################
    // ## Effective code generation ##
    // ###############################
    switch (nb_access) {

    case 1:
      // Found a TOP to perform the move in a single operation: do it.
      Build_OP(top_move, tgt_tn, guard, src_tn, ops);
      if (TN_is_true_pred(guard)) Set_OP_copy (OPS_last(ops)); // Mark true copy
      return TRUE;
      break;
      
    case 2:
    case 4:
    default:
      // No TOP found to perform the move in a single operation.
      // Different process depending on TN status:
      // Non-allocated TNs: 
      // --> Generate a single simulated TOP widemove
      //     (Facilitate register allocation work)
      // Allocated TNs:
      // --> Generate a set of move operating on subparts of the TNs
      if (TN_register(src_tn) == REGISTER_UNDEFINED ||
	  TN_register(tgt_tn) == REGISTER_UNDEFINED) {
	// Non-allocated TNs
	// -----------------
	// Retrieve simulated TOP
	top_move = rc_info->get_simulated_widemove_TOP(src_size);
	FmtAssert((top_move != TOP_UNDEFINED), ("Cannot get simulated widemove for current size(%d)\n", src_size));
	Build_OP(top_move, tgt_tn, src_tn, ops);
	return TRUE;
      }
      else {
	// Allocated TNs
	// -------------
	TN *src_sub_tn, *tgt_sub_tn;
	REGISTER src_reg = TN_register(src_tn);
	REGISTER tgt_reg = TN_register(tgt_tn);

	for (i=0; i<nb_access; i++) {
	  src_sub_tn = Gen_Register_TN(src_rc, top_move_rsize);
	  Set_TN_register(src_sub_tn, src_reg+i);

	  tgt_sub_tn = Gen_Register_TN(tgt_rc, top_move_rsize);
	  Set_TN_register(tgt_sub_tn, tgt_reg+i);

	  Build_OP(top_move, tgt_sub_tn, guard, src_sub_tn, ops);
	}
	return TRUE;
      }
      break;
    }
  }
  else {  // != reg classes
    // Note: only 32bits moves are handled here.
    // Indeed, 64 bits move needs 2 input TNs. These moves should be handled at higher level
    // of the code generation, if required.
    if (src_rc == ISA_REGISTER_CLASS_gpr) {
      rc_info = EXTENSION_get_REGISTER_CLASS_info(tgt_rc);
      if (tgt_size <= 4) {
	top_move = rc_info->get_move_R2X_TOP(tgt_size);
      }
      else {
	top_move = rc_info->get_insert_and_zeroext_R2X_TOP(tgt_size);
      }
      if (top_move == TOP_UNDEFINED) {
	FmtAssert(0, ("Cannot generate Copy from gpr, no TOP found"));
      }
      Build_OP(top_move, tgt_tn, guard, src_tn, ops);
      return TRUE;
    }
    else if (tgt_rc == ISA_REGISTER_CLASS_gpr) {
      rc_info = EXTENSION_get_REGISTER_CLASS_info(src_rc);
      if (src_size <= 4) {
	top_move = rc_info->get_move_X2R_TOP(src_size);
      }
      if (top_move == TOP_UNDEFINED) {
	FmtAssert(0, ("Cannot generate Copy to gpr, no TOP found"));
      }
      Build_OP(top_move, tgt_tn, guard, src_tn, ops);
      return TRUE;
    }
    else {  // Move between extension register classes
      FmtAssert(0, ("Copy between extension register classes not implemented"));
    }
  }
  return FALSE;
}


/* =============================================================================
 * Check wether or not it is possible to generate a move from extension 
 * register to a set of core registers. Return TRUE if possible, FALSE otherwise.
 * If <cginfo> is not NULL, it is filled with all information required to
 * generate the move sequence.
 * =============================================================================
 */
static BOOL EXTENSION_Exist_Copy_From_Core(ISA_REGISTER_CLASS    tgt_rc,
					   INT                   tgt_size,
					   extcg_move_corext_t  *cginfo) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top_insert;
  TOP top_rotate_insert;
  TOP top_rotate_insert_biggest = TOP_UNDEFINED;
  INT nb_sub_TNs;
  INT sub_TNs_size;
  INT sub_TNs_size_biggest;
  INT gpr_per_op;

  INT tgt_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(tgt_rc)) / CHAR_BIT;
  rc_info = EXTENSION_get_REGISTER_CLASS_info(tgt_rc);

  // Search code generation path
  // ---------------------------
  // Try first TOPs with 2xGPRs input, then ones with 1xGPR input
  for (gpr_per_op = (tgt_size>=8)?2:1; gpr_per_op >= 1; --gpr_per_op) {
    nb_sub_TNs   = 1;
    sub_TNs_size = tgt_size;
    do {
      if (gpr_per_op == 1) {
	top_insert        = rc_info->get_insert_and_zeroext_R2X_TOP(sub_TNs_size);
	top_rotate_insert = rc_info->get_rotate_and_insert_R2X_TOP (sub_TNs_size);
      } else { // (gpr_per_op == 2)
	top_insert        = rc_info->get_insert_and_zeroext_RR2X_TOP(sub_TNs_size);
	top_rotate_insert = rc_info->get_rotate_and_insert_RR2X_TOP (sub_TNs_size);
      }
      if ((top_insert != TOP_UNDEFINED) && (top_rotate_insert != TOP_UNDEFINED)) {
	// Selected codegen : 1x insert, Nx rotate_insert (level X, P or Q)
	// ----------------
	// ->   RFnR2X   dest, Rn0         (also catch RR2X)
	// ->   RFnR2CSX dest, dest, Rn1    "
	// ->   ...                         "
	// ->   RFnR2CSX dest, dest, Rnm    "
	if (cginfo) {
	  cginfo->type              = (gpr_per_op==1) ? EXTCG_INSERT_R2X_R2CSX : EXTCG_INSERT_RR2X_RR2CSX;
	  cginfo->nb_sub_TNs        = nb_sub_TNs;
	  cginfo->sub_TNs_size      = sub_TNs_size;
	  cginfo->gpr_per_op        = gpr_per_op;
	  cginfo->top_insert        = top_insert;
	  cginfo->top_rotate_insert = top_rotate_insert;
	}
	return TRUE;
      }
      else if ((top_insert != TOP_UNDEFINED) && ((sub_TNs_size == gpr_per_op*4) || 
						 (gpr_per_op==1 && sub_TNs_size < 4))) {
	// Selected codegen : Nx insert (level X, P or Q)
	// ----------------
	// ->   RFnR2X  dest_subpart0, Rn0   (also catch RR2X)
	// ->   ...                           "
	// ->   RFnR2X  dest_subpartm, Rnm    "
	if (cginfo) {
	  cginfo->type              = (gpr_per_op==1) ? EXTCG_INSERT_R2X : EXTCG_INSERT_RR2X;
	  cginfo->nb_sub_TNs        = nb_sub_TNs;
	  cginfo->sub_TNs_size      = sub_TNs_size;
	  cginfo->gpr_per_op        = gpr_per_op;
	  cginfo->top_insert        = top_insert;
	}
	return TRUE;
      }
      else {
	if (top_rotate_insert_biggest == TOP_UNDEFINED) {
	  top_rotate_insert_biggest = top_rotate_insert;
	  sub_TNs_size_biggest = sub_TNs_size;
	}
	nb_sub_TNs   *= 2;
	sub_TNs_size /= 2;
      }
    } while (sub_TNs_size >= tgt_rc_size);
    
    if (top_rotate_insert_biggest != TOP_UNDEFINED) {
      // Selected codegen : 1x KILL, Nx rotate_insert (level X, P or Q)
      // ----------------
      // ->   KILLX    dest            
      // ->   RFnR2CSX dest, dest, Rn0   (also catch RR2CSX)
      // ->   ...                         "
      // ->   RFnR2CSX dest, dest, Rnm    "
      // NOTE: KILL is a pseudo op, required for correct liveness analysis
      if (cginfo) {
	cginfo->type              = (gpr_per_op==1) ? EXTCG_KILL_THEN_INSERT_R2CSX : EXTCG_KILL_THEN_INSERT_RR2CSX;
	cginfo->nb_sub_TNs        = tgt_size / sub_TNs_size_biggest;
	cginfo->sub_TNs_size      = sub_TNs_size_biggest;
	cginfo->gpr_per_op        = gpr_per_op;
	cginfo->top_rotate_insert = top_rotate_insert_biggest;
      }
      return TRUE;
    }
  }

  // Try using STORE GPR + LOAD EXTREG through a tempo stack location
  TOP top_load = EXTENSION_Get_Real_Load_Store(/* reg_rc       */ tgt_rc,
					       /* reg_size     */ tgt_size,
					       /* data_align   */ PU_aligned_stack(Get_Current_PU()),
					       /* is_load      */ TRUE,
					       /* base_type    */ AM_BASE_DEFAULT,
					       /* offs_is_imm  */ TRUE,
					       /* offs_is_incr */ TRUE,
					       /* top_misalign */ NULL,
                                                                  NOT_A_PIXEL);
  if (top_load != TOP_UNDEFINED) {
    cginfo->sub_TNs_size = TOP_Mem_Bytes(top_load);
    cginfo->type       = EXTCG_STORE_GPR_LOAD_EXT;
    cginfo->nb_sub_TNs = tgt_size / cginfo->sub_TNs_size;
    cginfo->gpr_per_op = 1;
    return TRUE;
  }

  // No solution found..
  if (cginfo) {
    cginfo->type = EXTCG_FAILURE;
  }
  return FALSE;
}


/* =============================================================================
 * Expand a copy from a set of GPR tns to an extension register.
 * Return TRUE if a code sequence has been generated, FALSE otherwise.
 * Note: <src_tab> elements in little-endian ordering.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Copy_From_Core(TN   *tgt,
				     TN   *guard,
				     INT   nb_src,
				     TN  **src_tab,
				     OPS  *ops) {
  INT i;
  INT nb_sub_TNs, sub_TNs_size;
  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS tgt_rc;
  INT tgt_rc_size;
  INT tgt_size;
  TN *tgt_sub_TNs[MAX_SUB_TNS];
  
  tgt_rc      = TN_register_class(tgt);
  tgt_size    = TN_size(tgt);
  tgt_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(tgt_rc)) / CHAR_BIT;
  rc_info     = EXTENSION_get_REGISTER_CLASS_info(tgt_rc);

  extcg_move_corext_t cginfo;
  BOOL exist_move = EXTENSION_Exist_Copy_From_Core(tgt_rc, tgt_size, &cginfo);
  if (!exist_move) {
    return FALSE;
  }

  sub_TNs_size = cginfo.sub_TNs_size;
  nb_sub_TNs   = cginfo.nb_sub_TNs;
  
  // Create sub TNs, if required
  // ----------------------------
  // If R2X/R2CSX not available for result size, it is required to 
  // load data on result sub-TNs, and compose them later.
  if (nb_sub_TNs > 1) {
    for (i=0; i<nb_sub_TNs; i++) {
      tgt_sub_TNs[i] = Gen_Register_TN(tgt_rc, sub_TNs_size);
    }
  }

  // ----
  if (cginfo.type == EXTCG_STORE_GPR_LOAD_EXT) {
    // Generate STORE GPR + LOAD EXTREG through a tempo stack location

    ST *spill_loc = Allocate_Stack_Location_For_Memory_Move(tgt);
    FmtAssert(spill_loc != NULL,
	      ("EXTENSION_Expand_Copy_From_Core(): Unable to allocate tempo location on stack"));

    // ...Expand STORE GPR to stack first
    TYPE_ID mtype = (tgt_size==1)?MTYPE_U1:((tgt_size==2)?MTYPE_U2:MTYPE_U4);
    for (i=0; i<nb_src; i++) {
      Exp_Store (mtype, src_tab[i], spill_loc, /*offset*/i*4, ops, V_NONE);
    }

    // ...Then expand LOAD EXTREG from stack
    UINT32  data_align = PU_aligned_stack(Get_Current_PU());
    VARIANT variant    = V_NONE;
    if (data_align < (UINT32)tgt_size) {
      Set_V_alignment(variant, data_align);
      Set_V_align_offset_unknown(variant);
    }
    mtype = TY_mtype(ST_type(spill_loc));
    Exp_Load(mtype, mtype, tgt, spill_loc, 0, ops, variant);
    return TRUE;
  }

  // ----
  INT nb_access = sub_TNs_size/(cginfo.gpr_per_op*4); // count of 32b or 64b chunks per sub TNs

  if (cginfo.type == EXTCG_KILL_THEN_INSERT_R2CSX ||
      cginfo.type == EXTCG_KILL_THEN_INSERT_RR2CSX) {
    Build_OP_KILL(tgt, ops);
    if (nb_sub_TNs > 1) {
      Build_OP_simulated_extract(nb_sub_TNs, tgt_sub_TNs, True_TN, tgt, ops);
    }
  }
  
  INT src_TNs_idx = nb_src - cginfo.gpr_per_op;
  INT subid;
  for (subid=nb_sub_TNs-1; subid>=0; subid--) {
    TN *tmp_res      = NULL;
    TN *tmp_res_prev = NULL;
    TN *sub_tgt   = (nb_sub_TNs==1) ? tgt : tgt_sub_TNs[subid];
    if (cginfo.type == EXTCG_KILL_THEN_INSERT_R2CSX ||
	cginfo.type == EXTCG_KILL_THEN_INSERT_RR2CSX) {
      tmp_res = sub_tgt; // required to have a valid input for first R2CSX/RR2CSX
    }
    for (i=0; i<nb_access; i++) {
      // ... move chunks to extension reg
      // ... we build progressively the final extension
      // ... register. The final TN is "sub_tgt".
      // ... Other ones are built by duplicating "sub_tgt" TN.
      tmp_res_prev = tmp_res;
      tmp_res = (i!=nb_access-1) ? Gen_Register_TN(tgt_rc, sub_TNs_size) : sub_tgt;
      
      if (cginfo.gpr_per_op == 1) {
	if (( cginfo.type == EXTCG_INSERT_R2X) ||
	    ((cginfo.type == EXTCG_INSERT_R2X_R2CSX) && (i == 0))) {
	  Build_OP(cginfo.top_insert, tmp_res, True_TN, src_tab[src_TNs_idx], ops);
	} else {
	  Build_OP(cginfo.top_rotate_insert, tmp_res, True_TN, tmp_res_prev, src_tab[src_TNs_idx], ops);
	}
	src_TNs_idx--;
      }
      else { // cginfo.gpr_per_op == 2
	if (( cginfo.type == EXTCG_INSERT_RR2X) ||
	    ((cginfo.type == EXTCG_INSERT_RR2X_RR2CSX) && (i == 0))) {
	  Build_OP(cginfo.top_insert, tmp_res, True_TN, /*high*/src_tab[src_TNs_idx+1], /*low*/src_tab[src_TNs_idx], ops);
	} else {
	  Build_OP(cginfo.top_rotate_insert, tmp_res, True_TN, tmp_res_prev, /*high*/src_tab[src_TNs_idx+1], /*low*/src_tab[src_TNs_idx], ops);
	}
	src_TNs_idx-=2;
      }
    }
  }
  
  if (nb_sub_TNs > 1) {
    // Compose the final result register
    Build_OP_simulated_compose(tgt, True_TN, nb_sub_TNs, tgt_sub_TNs, ops);
  }
  return TRUE;
}


/* =============================================================================
 * Check wether or not it is possible to generate a move from a set of core
 * register to an extension register. Return TRUE if possible, FALS otherwise.
 * If <cginfo> is not NULL, it is filled with all information required to
 * generate the move sequence.
 * =============================================================================
 */
static BOOL EXTENSION_Exist_Copy_To_Core(ISA_REGISTER_CLASS    src_rc,
					 INT                   src_size,
					 extcg_move_corext_t  *cginfo) {
  const EXTENSION_Regclass_Info *rc_info;
  TOP top_extract;
  TOP top_extract_rotate;
  INT sub_TNs_size;

  INT src_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(src_rc)) / CHAR_BIT;
  rc_info = EXTENSION_get_REGISTER_CLASS_info(src_rc);

  // Try extract instructions
  // ------------------------
  if (src_size <= 4) {
    top_extract = rc_info->get_extract_X2R_TOP(src_size);
    if (top_extract != TOP_UNDEFINED) {
      // Selected codegen : 1x extract
      // ----------------
      // ->   RFnX2R   Rn0, src
      if (cginfo) {
	cginfo->type          = EXTCG_EXTRACT_X2R;
	cginfo->nb_sub_TNs    = 1;
	cginfo->sub_TNs_size  = src_size;
	cginfo->gpr_per_op    = 1;
	cginfo->top_extract   = top_extract;
      }
      return TRUE;
    }
  }
  else {
    sub_TNs_size = src_size;
    do {
      GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_extract_X2R_TOP,
					 /* IN_biggest_rsize  */ sub_TNs_size,
					 /* IN_smallest_rsize */ src_rc_size,
					 /* OUT_top           */ top_extract,
					 /* OUT_rsize         */ sub_TNs_size);
      if ((top_extract != TOP_UNDEFINED) && (sub_TNs_size == 4)) {
	// Selected codegen : Nx extract
	// ----------------
	// ->   RFnX2R   Rn0, src_subpart0
	// ->   ...
	// ->   RFnX2R   Rnm, src_subpartm
	if (cginfo) {
	  cginfo->type          = EXTCG_EXTRACT_X2R;
	  cginfo->nb_sub_TNs    = src_size / sub_TNs_size;
	  cginfo->sub_TNs_size  = sub_TNs_size;
	  cginfo->gpr_per_op    = 1;
	  cginfo->top_extract   = top_extract;
	}
	return TRUE;
      }
      else {
	sub_TNs_size /= 2;
      }
    } while ((top_extract != TOP_UNDEFINED) && (sub_TNs_size >= src_rc_size));
  }

  // Try extract_rotate instructions
  // -------------------------------
  GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_extract_and_rotate_X2R_TOP,
				     /* IN_biggest_rsize  */ src_size,
				     /* IN_smallest_rsize */ src_rc_size,
				     /* OUT_top           */ top_extract_rotate,
				     /* OUT_rsize         */ sub_TNs_size);
  if (top_extract_rotate != TOP_UNDEFINED) {
    // Selected codegen : Nx extract_rotate, either at level X, P or Q
    // ----------------
    // ->   RFnCSX2R  Rn0, src, src
    // ->   ...
    // ->   RFnCSX2R  Rnm, src, src
    if (cginfo) {
      cginfo->type               = EXTCG_EXTRACT_CSX2R;
      cginfo->nb_sub_TNs         = src_size / sub_TNs_size;
      cginfo->sub_TNs_size       = sub_TNs_size;
      cginfo->gpr_per_op         = 1;
      cginfo->top_extract_rotate = top_extract_rotate;
    }
    return TRUE;
  }

  // Try using STORE EXTREG + LOAD GPR through a tempo stack location
  TOP top_store = EXTENSION_Get_Real_Load_Store(/* reg_rc       */ src_rc,
						/* reg_size     */ src_size,
						/* data_align   */ PU_aligned_stack(Get_Current_PU()),
						/* is_load      */ FALSE,
						/* base_type    */ AM_BASE_DEFAULT,
						/* offs_is_imm  */ TRUE,
						/* offs_is_incr */ TRUE,
						/* top_misalign */ NULL,
                                                NOT_A_PIXEL );
  if (top_store != TOP_UNDEFINED) {
    cginfo->sub_TNs_size = TOP_Mem_Bytes(top_store);
    cginfo->type       = EXTCG_STORE_EXT_LOAD_GPR;
    cginfo->nb_sub_TNs = src_size / cginfo->sub_TNs_size;
    cginfo->gpr_per_op = 1;
    return TRUE;
  }

  // No solution found..
  if (cginfo) {
    cginfo->type = EXTCG_FAILURE;
  }
  return FALSE;
}


/* =============================================================================
 * Expand a copy from an extension register to a set of GPR tns.
 * Return TRUE if a code sequence has been generated, FALSE otherwise.
 * Note: <tgt_tab> elements in little-endian ordering.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Copy_To_Core(INT   nb_tgt,
				   TN  **tgt_tab,
				   TN   *guard,
				   TN   *src,
				   OPS  *ops) {
  ISA_REGISTER_CLASS src_rc;
  INT src_rc_size;
  INT src_size;
  TN *src_sub_TNs[MAX_SUB_TNS];
  INT i;

  src_rc   = TN_register_class(src);
  src_size = TN_size(src);
  src_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(src_rc)) / CHAR_BIT;

  extcg_move_corext_t cginfo;
  BOOL exist_move = EXTENSION_Exist_Copy_To_Core(src_rc, src_size, &cginfo);
  if (!exist_move) {
    return FALSE;
  }

  INT nb_sub_TNs, nb_chunk, subid;
  INT sub_TNs_size;
  sub_TNs_size = cginfo.sub_TNs_size;
  nb_sub_TNs   = cginfo.nb_sub_TNs;
  nb_chunk     = sub_TNs_size / 4; // count of 32 bits chunks per sub TNs
  if (nb_chunk == 0) { // Extension reg < 32 bits
    nb_chunk = 1;
  }
  
  // Extract sub TNs, if required
  if (nb_sub_TNs > 1) {
    for (i=0; i<nb_sub_TNs; i++) {
      src_sub_TNs[i] = Gen_Register_TN(src_rc, sub_TNs_size);
    }
    Build_OP_simulated_extract(nb_sub_TNs, src_sub_TNs, True_TN, src, ops);
  }

  // ----
  if (cginfo.type == EXTCG_STORE_EXT_LOAD_GPR) {
    // Generate STORE EXTREG + LOAD GPR through a tempo stack location

    ST *spill_loc = Allocate_Stack_Location_For_Memory_Move(src);
    FmtAssert(spill_loc != NULL,
	      ("EXTENSION_Expand_Copy_To_Core(): Unable to allocate tempo location on stack"));

    // ...Expand STORE EXTREG to stack first
    UINT32  data_align = PU_aligned_stack(Get_Current_PU());
    VARIANT variant    = V_NONE;
    if (data_align < (UINT32)src_size) {
      Set_V_alignment(variant, data_align);
      Set_V_align_offset_unknown(variant);
    }
    TYPE_ID mtype = TY_mtype(ST_type(spill_loc));
    Exp_Store(mtype, src, spill_loc, 0, ops, variant);

    // ...Then expand LOAD GPR from stack
    for (i=0; i<nb_tgt; i++) {
      TYPE_ID mtype = (src_size==1)?MTYPE_U1:((src_size==2)?MTYPE_U2:MTYPE_U4);
      Exp_Load (mtype, mtype, tgt_tab[i], spill_loc, /*offset*/i*4, ops, V_NONE);
    }

    return TRUE;
  }
  
  INT tgt_TNs_idx = 0;
  // Save 32 bits chunks (no temporary used for src, has value is preserved)
  for (subid=0; subid<nb_sub_TNs; subid++) {
    TN *sub_src = (nb_sub_TNs == 1) ? src : src_sub_TNs[subid];
    for (i=0; i<nb_chunk; i++) {
      if (cginfo.type == EXTCG_EXTRACT_X2R) {
	Build_OP(cginfo.top_extract, tgt_tab[tgt_TNs_idx], True_TN, sub_src, ops);
      }
      else { // cginfo.type == EXTCG_EXTRACT_CSX2R
	Build_OP(cginfo.top_extract_rotate, tgt_tab[tgt_TNs_idx], sub_src, True_TN, sub_src, ops);
      }
      tgt_TNs_idx++;
    }
  }
  return TRUE;
}


/* =============================================================================
 * Perform sign extension of source TN to dest TN, both belonging to same
 * register class. Source size should be smaller or equal to Dest size.
 * If from_32bits is true, only the lowest 32 bits of Source register
 * are correctly sign extended, otherwise the source register is already
 * sign extended on its full size.
 * =============================================================================
 */
static BOOL EXTENSION_Expand_SignExt(TN *dest, TN *src, BOOL from_32bits, OPS *ops) {

  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS rc;
  int src_rsize;
  int dest_rsize;
  int rc_size;
  TN *sub_TNs[MAX_SUB_TNS];

  rc         = TN_register_class(src);
  rc_size    = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(rc)) / CHAR_BIT;
  rc_info    = EXTENSION_get_REGISTER_CLASS_info(rc);
  src_rsize  = TN_size(src);
  dest_rsize = TN_size(dest);

  FmtAssert((src_rsize <= dest_rsize), ("Unexpected register size for sign extension"));

  // Note: if SIGNX is available, it implicitly means that Dest is greater than 32 bits
  TOP top_SIGNX = rc_info->get_signext_X_TOP(dest_rsize);
  TOP top_Q2SP  = rc_info->get_signext_P2X_TOP(dest_rsize/2);
  TOP top_P2SX  = rc_info->get_signext_P2X_TOP(dest_rsize);
  TOP top_CLRX  = rc_info->get_clear_TOP(dest_rsize);

  // -----------------------------------------------------------------
  // Various code selection will be tried, order by preference.
  // Choice will be made based on available predefined instructions
  // and Source and Dest sizes.
  // -----------------------------------------------------------------
  if (src_rsize == dest_rsize) {
    if (!from_32bits) {
      //
      // No sign extension required, just copy TNs if needed
      //
      if (src != dest) {
	return (EXTENSION_Expand_Copy(dest, True_TN, src, ops));
      }
      return TRUE;
    }
    else if (top_SIGNX != TOP_UNDEFINED) {
      //
      // Source and dest TNs have same size but only lowest 32 bits are 
      // correctly sign extended in source
      // ->   RFnSIGNX   dest, src, #1
      //
      Build_OP(top_SIGNX, dest, True_TN, src, Gen_Literal_TN(1, 4), ops);
      return TRUE;
    }
  }

  if ((from_32bits) && (src_rsize > 4) && (src_rsize > rc_size) && (top_P2SX != TOP_UNDEFINED)) {
    if (((src_rsize/2 == 4) || ((top_SIGNX == TOP_UNDEFINED || top_CLRX == TOP_UNDEFINED) &&
				(rc_info->get_signext_X_TOP(src_rsize/2) != TOP_UNDEFINED))) &&
	(( src_rsize == dest_rsize) ||
	 ((src_rsize == dest_rsize/2) && (top_Q2SP != TOP_UNDEFINED)))) {
      //
      // Source is bigger than 32 bits and only lowest 32 bits are sign extended:
      // Extract the lowest half of Source and insure it is correclty sign extended
      // ->   Extract   sub_0, sub_1, src
      // -> ( RFnSIGNx  sub_0, sub_0, #1 )  (only if Source is > 64 bits)
      //    ( Sign Extension from sub_0 to Dest done afterwards)
      //
      src_rsize  = src_rsize/2;
      sub_TNs[0] = Gen_Register_TN(rc, src_rsize);
      sub_TNs[1] = Gen_Register_TN(rc, src_rsize);
      Build_OP_simulated_extract(2, sub_TNs, True_TN, src, ops);
      src        = sub_TNs[0];
      if (src_rsize > 4) {
	TOP top_SIGNP = rc_info->get_signext_X_TOP(src_rsize);
	Build_OP(top_SIGNP, src, True_TN, src, Gen_Literal_TN(1, 4), ops);
	from_32bits = FALSE;
      }
    }
    else if (((src_rsize/4 == 4) || ((top_SIGNX == TOP_UNDEFINED || top_CLRX == TOP_UNDEFINED) &&
				     (rc_info->get_signext_X_TOP(src_rsize/4) != TOP_UNDEFINED))) &&
	     (( src_rsize == dest_rsize) && (top_Q2SP != TOP_UNDEFINED))) {
      //
      // Source is bigger than 32 bits and only lowest 32 bits are sign extended:
      // Extract the lowest quarter of Source and insure it is correclty sign extended
      // ->   Extract   sub_0, sub_1, sub_2, sub_3, src
      // -> ( RFnSIGNQ  sub_0, sub_0, #1 )    (only if Source is > 64 bits)
      //    ( Sign Extension from sub_0 to Dest done afterwards)
      //
      src_rsize = src_rsize/4;
      for (int i=0; i<4; i++) {
	sub_TNs[i] = Gen_Register_TN(rc, src_rsize);
      }
      Build_OP_simulated_extract(4, sub_TNs, True_TN, src, ops);
      src = sub_TNs[0];
      if (src_rsize > 4) {
	TOP top_SIGNQ = rc_info->get_signext_X_TOP(src_rsize);
	Build_OP(top_SIGNQ, src, True_TN, src, Gen_Literal_TN(1, 4), ops);
	from_32bits = FALSE;
      }
    }
  }
  
  if ((!from_32bits) || (src_rsize == 4)) {
    if ((dest_rsize == src_rsize*2) && (top_P2SX != TOP_UNDEFINED)) {
      //
      // Source is correctly sign extended but is smaller than dest
      // ->   RFnP2SX   dest, src
      //
      Build_OP(top_P2SX, dest, True_TN, src, ops);
      return TRUE;
    }
    
    if ((dest_rsize == src_rsize*4) && (top_Q2SP != TOP_UNDEFINED) && (top_P2SX != TOP_UNDEFINED)) {
      //
      // Source is correctly sign extended but is smaller than dest
      // ->   RFnQ2SP   tmp64, src
      // ->   RFnP2SX   dest, tmp64
      //
      TN *tmp_tn = Gen_Register_TN(rc, dest_rsize/2);
      Build_OP(top_Q2SP, tmp_tn, True_TN, src, ops);
      Build_OP(top_P2SX, dest, True_TN, tmp_tn, ops);
      return TRUE;
    }
  }

  if ((src_rsize >= 4) && (top_SIGNX != TOP_UNDEFINED) && (top_CLRX != TOP_UNDEFINED)) {
    //
    // Source is smaller than dest but at least 32 bits wide.
    // Note: this is the less efficient code, as CLR is required for liveness analysis,
    //       (to initialize tmpx_sub1 (+_sub2 and _sub3 if defined) but is finally useless
    //       in the final code as the SIGNX will overwrite those register parts.
    // ->   RFnCLRx    tmpx
    // ->   Extract    tmpx_sub0, tmpx_sub1, tmpx
    // ->   Compose    tmpx, src, tmpx_sub1
    // ->   RFnSIGNX   dest, tmpx, #n
    //
    TN *tmp_tn = Gen_Register_TN(rc, dest_rsize);
    Build_OP(top_CLRX, tmp_tn, True_TN, ops);

    int nb_part = dest_rsize / src_rsize;

    // Create temporary sub-registers
    for (int i=0; i<nb_part; i++) {
      sub_TNs[i] = Gen_Register_TN(rc, src_rsize);
    }
    if (nb_part == 2) {
      Build_OP_simulated_extract(sub_TNs[0], sub_TNs[1], True_TN, tmp_tn, ops);
      Build_OP_simulated_compose(tmp_tn, True_TN, src, sub_TNs[1], ops);
    } else {  // nb_access == 4
      Build_OP_simulated_extract(sub_TNs[0], sub_TNs[1], sub_TNs[2], sub_TNs[3], True_TN, tmp_tn, ops);
      Build_OP_simulated_compose(tmp_tn, True_TN, src, sub_TNs[1], sub_TNs[2], sub_TNs[3], ops);
    }
    TN *start = Gen_Literal_TN(from_32bits?1:(src_rsize/4), 4);
    Build_OP(top_SIGNX, dest, True_TN, tmp_tn, start, ops);
    return TRUE;
  }
 
  char error_msg[128];
  sprintf(error_msg, "[%s] Failed to sign extend data on extension register "
	  "class, lack of predefined instructions.",
	  EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(rc));
  ErrMsg(EC_Ext_Expand, error_msg);
  return FALSE;
}


/* =============================================================================
 * Perform zero extension of source TN to dest TN, both belonging to same
 * register class. Source size should be smaller to Dest size.
 * =============================================================================
 */
static BOOL EXTENSION_Expand_ZeroExt(TN *dest, TN *src, OPS *ops) {

  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS rc;
  int src_rsize;
  int dest_rsize;
  TN *sub_TNs[MAX_SUB_TNS];

  rc         = TN_register_class(src);
  src_rsize  = TN_size(src);
  dest_rsize = TN_size(dest);
  rc_info    = EXTENSION_get_REGISTER_CLASS_info(rc);

  FmtAssert((src_rsize <= dest_rsize), ("Unexpected register size for zero extension"));

  // -----------------------------------------------------------------
  // Various code selection will be tried, order by preference.
  // Choice will be made based on available predefined instructions
  // and Source and Dest sizes.
  // -----------------------------------------------------------------
  if (src_rsize == dest_rsize) {
    // No zero extension required, just copy TNs if needed
    if (src != dest) {
      return (EXTENSION_Expand_Copy(dest, True_TN, src, ops));
    }
    return TRUE;
  }

  if (dest_rsize == src_rsize*2) {
    // Source is half the size of the Dest, 2 solutions:
    TOP top_P2UX = rc_info->get_zeroext_P2X_TOP(dest_rsize);
    if (top_P2UX != TOP_UNDEFINED) {
      // ->   RFnP2UX   dest, src
      Build_OP(top_P2UX, dest, True_TN, src, ops);
      return TRUE;
    }

    TOP top_CLR = rc_info->get_clear_TOP(src_rsize);
    if (top_CLR != TOP_UNDEFINED) {
      // ->   RFnCLRp   tmp
      // ->   Compose   dest, src, tmp
      TN *tmp_tn = Gen_Register_TN(rc, src_rsize);
      Build_OP(top_CLR, tmp_tn, True_TN, ops);
      Build_OP_simulated_compose(dest, True_TN, src, tmp_tn, ops);
      return TRUE;
    }
  }
  else if (dest_rsize == src_rsize*4) {
    // Source is the quarter of the size of the Dest
    TOP top_Q2UP = rc_info->get_zeroext_P2X_TOP(dest_rsize/2);
    TOP top_P2UX = rc_info->get_zeroext_P2X_TOP(dest_rsize);
    if ((top_Q2UP != TOP_UNDEFINED) && (top_P2UX != TOP_UNDEFINED)) {
      // ->   RFnQ2UP   tmp, src
      // ->   RFnP2UX   dest, tmp
      TN *tmp_tn = Gen_Register_TN(rc, dest_rsize/2);
      Build_OP(top_Q2UP, tmp_tn, True_TN, src, ops);
      Build_OP(top_P2UX, dest, True_TN, tmp_tn, ops);
      return TRUE;
    }

    TOP top = rc_info->get_clear_TOP(dest_rsize);
    if (top != TOP_UNDEFINED) {
      // ->   RFnCLRx    tmpx
      // ->   Extract    sub0, sub1, sub2, sub3, tmpx
      // ->   Compose    tmpx, src, sub1, sub2, sub3
      TN *tmp_tn = Gen_Register_TN(rc, dest_rsize);
      Build_OP(top, tmp_tn, True_TN, ops);
      Build_OP_simulated_extract(sub_TNs[0], sub_TNs[1], sub_TNs[2], sub_TNs[3], True_TN, tmp_tn, ops);
      Build_OP_simulated_compose(dest, True_TN, src, sub_TNs[1], sub_TNs[2], sub_TNs[3], ops);
      return TRUE;
    }
  }

  char error_msg[128];
  sprintf(error_msg, "[%s] Failed to zero extend data on extension register "
	  "class, lack of predefined instructions.",
	  EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(rc));
  ErrMsg(EC_Ext_Expand, error_msg);
  return FALSE;
}


/* =============================================================================
 * Expand a 32 bits immediate value within an extension register.
 * Various sequences of code might be generated, depending on the
 * immediate value and available predefined instructions.
 * If immediate is 0 and CLR instruction available, use it
 * Otherwise, generate the following instruction sequence:
 *   MAKE       Rn, #const    (copy value in gpr)
 *   XRFnR2X    Xn, Rn        (move and zero extend to ext register)
 *   XRFnSIGNX  Xn, Xn        (sign extend)
 *   Note: the later is not required if immediate value is known and positive,
 *         or if extension register is smaller or equal to 32 bits.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Immediate(TN   *dest,
				TN   *src,
				BOOL  is_signed,
				OPS  *ops) {
  char error_msg[128];
  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS dest_rc;
  INT dest_rc_size;
  INT dest_rsize;
  int nb_access;

  dest_rc      = TN_register_class(dest);
  dest_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(dest_rc)) / CHAR_BIT;
  dest_rsize   = TN_size(dest);
  rc_info      = EXTENSION_get_REGISTER_CLASS_info(dest_rc);

  // ---------------------------------------
  // --  Generate CLR if appropriate
  // ---------------------------------------
  if (TN_has_value(src) && TN_value(src)==0) {
    BOOL ok = EXTENSION_Expand_Clr(dest, ops);
    if (ok) {
      return TRUE;
    }
  }

  /* After this point, we know we cannot use clear instructions */

  // ---------------------------------------
  // --  Create constant in GPR register
  // ---------------------------------------
  TN *gpr;
  gpr = Gen_Register_TN(ISA_REGISTER_CLASS_gpr, 4);
  Expand_Immediate(gpr, src, is_signed, ops);


  // --------------------------------------
  // --  Move GPR to Extension register
  // --------------------------------------
  if (dest_rsize <= 4) {
    TOP top_r2x;
    top_r2x = rc_info->get_insert_and_zeroext_R2X_TOP(dest_rsize);
    if (top_r2x != TOP_UNDEFINED) {
      Build_OP(top_r2x, dest, True_TN, gpr, ops);
      return TRUE;
    }
  }
  else {
    TOP top_r2x;
    int top_r2x_rsize;
    GET_TOP_FOR_BIGGEST_AVAIL_REG_SIZE(/* IN_function       */ rc_info->get_insert_and_zeroext_R2X_TOP,
				       /* IN_biggest_rsize  */ dest_rsize,
				       /* IN_smallest_rsize */ dest_rc_size,
				       /* OUT_top           */ top_r2x,
				       /* OUT_rsize         */ top_r2x_rsize);
    if (top_r2x != TOP_UNDEFINED) {
      nb_access = dest_rsize / top_r2x_rsize;
      if (nb_access == 1) {
	Build_OP(top_r2x, dest, True_TN, gpr, ops);
	if (is_signed && (!TN_has_value(src) || TN_value(src)<0)) {
	  EXTENSION_Expand_SignExt(dest, dest, TRUE, ops);
	}
      }
      else {
	// Verify value can fit in move
	if (top_r2x_rsize < 4) {
	  if (TN_is_symbol(src)) {
	    DevWarn("Cannot insure that symbol value will be correctly copied into extension register");
	  }
	  else if ((!is_signed &&  TN_value(src) >= ( 1<< top_r2x_rsize)) ||
		   ( is_signed && (TN_value(src) >= ( 1<<(top_r2x_rsize-1)) ||
				   TN_value(src) <  (-1<<(top_r2x_rsize-1))))) {
	    sprintf(error_msg, "No way to generate value %d in extension register of class %s",
		    (int)TN_value(src),
		    ISA_REGISTER_CLASS_INFO_Name(ISA_REGISTER_CLASS_Info(dest_rc)));
	    ErrMsg(EC_Ext_Expand, error_msg);
	  }
	}

	// Create tempo sub reg
	TN *sub_TN = Gen_Register_TN(dest_rc, dest_rsize/nb_access);
	Build_OP(top_r2x, sub_TN, True_TN, gpr, ops);

	if (is_signed) {
	  EXTENSION_Expand_SignExt(dest, sub_TN, TRUE, ops);
	} else {
	  EXTENSION_Expand_ZeroExt(dest, sub_TN, ops);
	}
      }
      return TRUE;
    }
  }

  sprintf(error_msg, "[%s] Failed to expand immediate in extension register class",
	  EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(dest_rc));
  ErrMsg(EC_Ext_Expand, error_msg);
  return FALSE;
}

/* =============================================================================
 * Duplicate parameter TN <offset> and increment its value by <incr>.
 * =============================================================================
 */
static inline TN * Duplicate_Offset_TN_And_Increment(TN *offset, INT incr) {
  TN *res;
  if (TN_is_symbol(offset)) {
    res = Gen_Symbol_TN(TN_var(offset),
			TN_offset(offset) + incr,
			TN_relocs(offset));
  } else if (TN_has_value(offset)) {
    res = Gen_Literal_TN(TN_value(offset) + incr, 4);
  }
  else {
    FmtAssert(0, ("Duplicate_Offset_TN_And_Increment(): unexpected constant"));
  }
  return (res);
}


/* =============================================================================
 * Update addressing mode TNs <base> + <offset> so it can be safely incremented
 * later using the Duplicate_Offset_TN_And_Increment() function.
 * The offset  value is incremented by <incr>.
 * =============================================================================
 */
static void Transform_AM_For_Immediate_Offset(TN  **base,
					      TN  **offset,
					      BOOL  offs_is_imm,
					      INT   incr,
					      OPS  *ops,
					      REGISTER_SET* availTemps) {
  TN *tmp_base;
  if (!offs_is_imm) {
      // register used by base can be released, since tmp_base is used as write
      // parameter and base as read one and then base is replaced by tmp_base
      EXTENSION_Release_Register_TN(availTemps, *base);
      tmp_base = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(*base), TN_size(*base));
    Build_OP(TOP_addu_r, tmp_base, True_TN, *base, *offset, ops);
    Set_OP_carryisignored (OPS_last(ops));
    *base = tmp_base;
    *offset = Gen_Literal_TN(incr, 4);
  }
  else if (incr != 0) {
    *offset = Duplicate_Offset_TN_And_Increment(*offset, incr);
  }
}


/* =============================================================================
 * Try to generate a load to <result> TN using core loads and predefined
 * extension instructions.
 * Return TRUE if the code has been generated, FALSE otherwise.
 * =============================================================================
 */
static BOOL EXTENSION_Expand_Rescue_Load(BOOL  offs_is_imm,
					 TN   *result,
					 TN   *base,
					 TN   *offset,
					 OPS  *ops,
					 REGISTER_SET* availTemps) {
  INT i;
  const EXTENSION_Regclass_Info *rc_info;
  ISA_REGISTER_CLASS dest_rc;
  INT dest_rc_size;
  INT dest_size;
  TN *dest_sub_TNs[MAX_SUB_TNS];
  INT action;
  
  dest_rc      = TN_register_class(result);
  dest_size    = TN_size(result);
  dest_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(dest_rc)) / CHAR_BIT;
  rc_info      = EXTENSION_get_REGISTER_CLASS_info(dest_rc);

  // Check if data can copied from Core to Extension
  extcg_move_corext_t cginfo;
  BOOL exist_move = EXTENSION_Exist_Copy_From_Core(dest_rc, dest_size, &cginfo);
  if (!exist_move) {
    return FALSE;
  }

  action = cginfo.type;

  if (dest_size <= 4) {
    // ----------------------------------------------------------------------------
    // data <= 32 bits
    // ---------------
    // LOAD to Core reg a 8, 16 or 32 bits data chunks and MOVE it to extension reg
    // ----------------------------------------------------------------------------
    int mtype;
    if (dest_size == 4) {
      mtype = MTYPE_U4;
    } else if (dest_size == 2) {
      mtype = MTYPE_U2;
    } else {
      mtype = MTYPE_U1;
    }
    OPCODE opc = OPCODE_make_op(OPR_LDID, MTYPE_U4, mtype);
    TN *gpr = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps,
                                                 ISA_REGISTER_CLASS_gpr, 4);
    Expand_Load(opc, gpr, base, offset, ops);

    if (action == EXTCG_INSERT_R2X) {
      Build_OP(cginfo.top_insert, result, True_TN, gpr, ops);
    }
    else if (action == EXTCG_KILL_THEN_INSERT_R2CSX) {
      Build_OP_KILL(result, ops);
      Build_OP(cginfo.top_rotate_insert, result, True_TN, result, gpr, ops);
    }
    else {
      FmtAssert((0), ("Unexpected code generation"));
    }
    EXTENSION_Release_Register_TN(availTemps, gpr);
    return TRUE;
  }
  else {
    // ----------------------------------------------------------------------
    // data > 32 bits
    // --------------
    // LOAD data to several Core regs (GPRs) then MOVE them into an extension
    // reg, using predefined instructions
    // ----------------------------------------------------------------------
    OPCODE opc = OPCODE_make_op(OPR_LDID, MTYPE_U4, MTYPE_U4);
	
    INT sub_TNs_size = cginfo.sub_TNs_size;
    INT nb_sub_TNs   = cginfo.nb_sub_TNs;
    INT nb_access    = sub_TNs_size/(cginfo.gpr_per_op*4); // count of 32b or 64b chunks per sub TNs
	
    // Set offset to the last address to access (reverse accesses)
    Transform_AM_For_Immediate_Offset(&base, &offset, offs_is_imm, (dest_size - 4), ops, availTemps);

    // Create sub TNs, if required
    // ----------------------------
    // If R2X/RR2X not available for result size, it is required to 
    // load data on result sub-TNs, and compose them later.
    bool isAllocated = false;
    if (nb_sub_TNs > 1) {
        Create_Sub_Regs(availTemps, dest_sub_TNs, result, nb_sub_TNs,
                        sub_TNs_size, &isAllocated);
    }
    if (action == EXTCG_KILL_THEN_INSERT_R2CSX ||
	action == EXTCG_KILL_THEN_INSERT_RR2CSX) {
      Build_OP_KILL(result, ops);
      if (nb_sub_TNs > 1 && !isAllocated) {
	Build_OP_simulated_extract(nb_sub_TNs, dest_sub_TNs, True_TN, result, ops);
      }
    }

    INT subid;
    for (subid=nb_sub_TNs-1; subid>=0; subid--) {
      TN *tmp_res      = NULL;
      TN *tmp_res_prev = NULL;
      TN *sub_result   = (nb_sub_TNs==1) ? result : dest_sub_TNs[subid];
      if (cginfo.type == EXTCG_KILL_THEN_INSERT_R2CSX ||
	  cginfo.type == EXTCG_KILL_THEN_INSERT_RR2CSX) {
	tmp_res = sub_result; // required to have a valid input for first R2CSX/RR2CSX
      }
      for (i=0; i<nb_access; i++) {
	TN *high = NULL, *low = NULL;
	if (cginfo.gpr_per_op == 2) {
	  // ... load high 32 bits chunk
	  high = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps,
                                                ISA_REGISTER_CLASS_gpr, 4);
	  Expand_Load(opc, high, base, offset, ops);
	  offset = Duplicate_Offset_TN_And_Increment(offset, -4);
	}
	// ... load 32 bits chunk
	low = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, ISA_REGISTER_CLASS_gpr,
                                             4);
	Expand_Load(opc, low, base, offset, ops);
	offset = Duplicate_Offset_TN_And_Increment(offset, -4);
	// ... move chunks to extension reg
	// ... we build progressively the final extension
	// ... register. The final TN is "sub_result".
	// ... Other ones are built based on "sub_result" TN attributes.
	tmp_res_prev = tmp_res;
	tmp_res = (i!=nb_access-1) && !availTemps ? Gen_Register_TN(dest_rc, sub_TNs_size) : sub_result;

	if (cginfo.gpr_per_op == 1) {
	  if (( cginfo.type == EXTCG_INSERT_R2X) ||
	      ((action == EXTCG_INSERT_R2X_R2CSX) && (i == 0))) {
	    Build_OP(cginfo.top_insert, tmp_res, True_TN, low, ops);
	  } else {
	    Build_OP(cginfo.top_rotate_insert, tmp_res, True_TN, tmp_res_prev, low, ops);
	  }
	}
	else { // cginfo.gpr_per_op == 2
	  if (( cginfo.type == EXTCG_INSERT_RR2X) ||
	      ((action == EXTCG_INSERT_RR2X_RR2CSX) && (i == 0))) { 
	    Build_OP(cginfo.top_insert, tmp_res, True_TN, high, low, ops);
	  } else {
	    Build_OP(cginfo.top_rotate_insert, tmp_res, True_TN, tmp_res_prev, high, low, ops);
	  }
	}
	EXTENSION_Release_Register_TN(availTemps, high);
	EXTENSION_Release_Register_TN(availTemps, low);
      }
    }
    
    if (nb_sub_TNs > 1 && !isAllocated) {
      // Compose the final result register
      Build_OP_simulated_compose(result, True_TN, nb_sub_TNs, dest_sub_TNs, ops);
    }
    return TRUE;
  }
  return FALSE;
}



/* =============================================================================
 * Real load expansion function, used to produce both (base + immediate)
 * and (base + reg).
 * Note: Auto-modified addressing mode are not generated here.
 *       This is handle later in the compilation flow (in EBO).
 * =============================================================================
 */
static
BOOL EXTENSION_Expand_Load(AM_Base_Reg_Type base_type,
			   BOOL             offs_is_imm,
			   BOOL             offs_is_incr,
			   TN              *result,
			   TN              *base,
			   TN              *offset,
			   OPS             *ops,
			   UINT32           data_align,
			   REGISTER_SET* availTemps,
                           UINT32 pixel_size
                           ) {
  const EXTENSION_Regclass_Info *rc_info;
  TN *result_sub_tns[MAX_SUB_TNS];
  TN *tmp_offset;
  TN *tmp_base;
  INT64 tmp_val;
  TOP top_load, top_tmp;
  TOP top_misalign;
  INT nb_access;
  INT current_reg_access_size;
  INT current_mem_access_size;
  INT i;

  ISA_REGISTER_CLASS result_rc;
  INT result_rc_size;
  INT result_size;
  bool isAllocated = false;
  REGISTER_SET saveSet[ISA_REGISTER_CLASS_MAX + 1];

  CopyRegisterSets(saveSet, availTemps);

  result_rc   = TN_register_class(result);
  result_size = TN_size(result);
  
  result_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(result_rc)) / CHAR_BIT;


  rc_info = EXTENSION_get_REGISTER_CLASS_info(result_rc);

  // ##########################################
  // ## Retrieve the widest load TOP         ##
  // ## for current TN and define the number ##
  // ## of access required (1, 2 or 4 parts) ##
  // ##########################################
  top_load = EXTENSION_Get_Real_Load_Store(/* reg_rc       */ result_rc,
					   /* reg_size     */ result_size,
					   /* data_align   */ data_align,
					   /* is_load      */ TRUE,
					   /* base_type    */ base_type,
					   /* offs_is_imm  */ offs_is_imm,
					   /* offs_is_incr */ TRUE,
					   /* top_misalign */ &top_misalign,
                                           pixel_size );
  
  if (top_load == TOP_UNDEFINED) {
    // No load found for current extension register!!
    // (except a possibly misaligned one, handled later)
    // Looking for an alternative
    if (pixel_size>0) // core alternative is not available for pixel loads.
      {
        char error_msg[256];
        sprintf(error_msg,
                "[%s] pixel load not available for type of size %d,"
                "pixel size %d and alignment %d.",
                EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(result_rc),
                result_size, pixel_size, data_align);
        ErrMsg(EC_Ext_Expand, error_msg);
        return RestoreAvailableResourceForLoad(result, FALSE, availTemps, saveSet);
      }

      BOOL ok = EXTENSION_Expand_Rescue_Load(offs_is_imm, result, base, offset, ops, availTemps);
    if (ok) {
        return RestoreAvailableResourceForLoad(result, TRUE, availTemps, saveSet);
    }

    if (top_misalign != TOP_UNDEFINED) {
      // ------------------------------------
      // Available load is too constrained...
      // ------------------------------------
      char error_msg[256];
      sprintf(error_msg, "[%s] Failed to generate load on extension registers for "
	      "data aligned on %d bytes. Available one is aligned on %d bytes,"
	      " and lack of predefined instructions does not allow usage of core load.",
	      EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(result_rc),
	      data_align, TOP_Mem_Alignment(top_misalign));
      ErrMsg(EC_Ext_Expand, error_msg);
      return RestoreAvailableResourceForLoad(result, FALSE, availTemps, saveSet);
    }
    else {
      char error_msg[256];
      sprintf(error_msg, "[%s] No load found in extension for register of size %d bytes,"
	      "and lack of predefined instructions does not allow usage of core load.",
	      EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(result_rc),
	      result_size);
      ErrMsg(EC_Ext_Expand, error_msg);
      return RestoreAvailableResourceForLoad(result, FALSE, availTemps, saveSet);
    }
  }

  nb_access = result_size/TOP_Mem_Bytes(top_load);

  // [VCdV] for pixel mtypes, we only need to load half the size of the
  // target register. ( TOP_Mem_Bytes(LDXPIX) == TOP_Mem_Bytes(LDX) / 2 ).
  if (pixel_size>0) {
    nb_access/=2;
  }

  INT offset_idx = TOP_Find_Operand_Use(top_load, OU_offset);

  // ###############################
  // ## Effective code generation ##
  // ###############################
  switch (nb_access) {
  case 1:
    // ===============================================================
    // Composite register size:  1
    // ===============================================================
    if (offs_is_imm) {
      if (TN_has_value(offset) ) {
        top_load = 
          TOP_opnd_immediate_variant(top_load, offset_idx, TN_value(offset));
      }
    }
    
    Build_OP(top_load, result, True_TN, base, offset, ops);
    return RestoreAvailableResourceForLoad(result, TRUE, availTemps, saveSet);
    
  case 2:
  case 4:
    // ===============================================================
    // Composite register size:  2, 4
    // ===============================================================    
    current_reg_access_size = result_size/nb_access;

    // From the compiler point of view, a pixel data takes
    // half less bytes in memory than in register.
    if (pixel_size > 0) {
      current_mem_access_size = current_reg_access_size/2;
    } else {
      current_mem_access_size = current_reg_access_size;
    }

    // Create temporary sub-registers
    Create_Sub_Regs(availTemps, result_sub_tns, result, nb_access,
                    current_reg_access_size, &isAllocated);

    switch (base_type) {
    case AM_BASE_DEFAULT:
      if (offs_is_imm) {
        FmtAssert((offs_is_incr), ("offset is always incremental !"));
	  // -----------------------------------------------
	  // AddrMode:    (Rn + #imm_u8)
	  // -----------------------------------------------
	  //
	  // if (immediate value of all load addressing mode fits in u8)
	  //     load  low,  @( Rn + #u8 )
	  //     ...
	  //     load  high, @( Rn + #(u8 + (nb_access-1)*current_mem_access_size) )
	  // else
	  //     add   Rt, Rn, #(nb_access*current_mem_access_size)
	  //     load  low,  @( Rt + (#u8 - #(nb_access*current_mem_access_size) ) )
	  //     ...
	  //     load  high, @( Rt + (#u8 - #current_mem_access_size) )
	  //
	  FmtAssert(TN_is_constant(offset) &&
		    (TN_has_value(offset) || TN_is_symbol(offset)),
		    ("Unexpected offset type for current addressing mode"));

	  // Check offset range (only for value TN with positive offset)
	  if (TN_has_value(offset) && TN_value(offset)>0) {
	    tmp_val = TN_value(offset) + (INT64)(current_mem_access_size*(nb_access-1));
            if (TOP_opnd_immediate_variant(top_load, offset_idx, tmp_val) ==
                TOP_UNDEFINED) {
	      // Not all offsets will fit in current addressing mode
	      // -> Increment base and decrement offset
	      INT64 dec = (INT64)(current_mem_access_size*nb_access);

	      TN *tmp_offset = Dup_TN(offset);
	      Set_TN_value(tmp_offset, dec);

	      // register used by base can be released, since tmp_base is used as
	      // write parameter and base as read one and then base is replaced by
	      // tmp_base
	      EXTENSION_Release_Register_TN(availTemps, base);
	      tmp_base = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	      Build_OP(TOP_addu_i8, tmp_base, True_TN, base, tmp_offset, ops);
              Set_OP_carryisignored (OPS_last(ops));
	      base = tmp_base;
	      
	      offset = Dup_TN(offset);
	      Set_TN_value(offset, TN_value(offset) - dec);
	    }
	  }
	  
	  // Generate the move sequence
          TOP effective_top = top_load;
	  for (i=0; i<nb_access; i++) {
	    if (i!=0) {
	      offset = Dup_TN(offset);
	      if (TN_has_value(offset)) {
		Set_TN_value(offset, TN_value(offset)+current_mem_access_size);
                effective_top = TOP_opnd_immediate_variant(top_load, offset_idx,
                                                           TN_value(offset));
	      }
	      else { // TN_is_symbol
		Set_TN_offset(offset, TN_offset(offset)+current_mem_access_size);
                effective_top = top_load;
	      }
            } else {
              if (TN_has_value(offset)) {
                effective_top = TOP_opnd_immediate_variant(top_load, offset_idx,
                                                           TN_value(offset));
	      }
            }
            Build_OP(effective_top, result_sub_tns[i], True_TN, base, offset, ops);
          }
      }
      else {  // !offs_is_imm
	// -----------------------------------------------
	// AddrMode:    (Rn + Rp)
	// -----------------------------------------------
	//
	// add   Rt, Rn, Rp
	// load  low,  @( Rt + #0 )
	// ...
	// load  high, @( Rt + #(nb_access-1)*current_mem_access_size )
	//

	// register used by base can be released, since tmp_base is used as write
	// parameter and base as read one and then base is replaced by tmp_base
	EXTENSION_Release_Register_TN(availTemps, base);
	tmp_base = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	Build_OP(TOP_addu_r, tmp_base, True_TN, base, offset, ops);
        Set_OP_carryisignored (OPS_last(ops));
	base = tmp_base;
	
	top_tmp = rc_info->get_load_TOP(current_reg_access_size, AM_BASE_DEFAULT, 
					/*offs_is_imm*/true, /*offs_is_incr*/true, pixel_size);
	FmtAssert((top_tmp!=TOP_UNDEFINED), ("Cannot find expected load instruction!"));
	
	for (i=0; i<nb_access; i++) {
	  tmp_offset = Gen_Literal_TN(i*current_mem_access_size, 4, 1);
	  Build_OP(top_tmp, result_sub_tns[i], True_TN, base, tmp_offset, ops);
	}
      }
      break;

    case AM_BASE_GP:
    case AM_BASE_SP:
      // -----------------------------------------------
      // AddrMode:    (GP + #imm_u12)
      //              (SP + #imm_u12)
      // -----------------------------------------------
      // (GP + #imm_u12)
      // ---------------
      // if (all immediate offset remains in U12 range)
      //     load  low,  @( R13 + #u12 )
      //     ...
      //     load  high, @( R13 + #(u12 + (nb_access-1)*current_mem_access_size)) )
      // else
      //     make  Rt, #u12
      //     add  Rt, R13, Rt
      //     load  low,  @( Rt + #0 )
      //     ...
      //     load  high, @( Rt + (nb_access-1)*current_mem_access_size)
      //
      // (SP + #imm_u12)
      // ---------------
      // (Same handling except that R13 is replace by R15)
      //
      FmtAssert(TN_is_constant(offset) && 
		(TN_has_value(offset) || TN_is_symbol(offset)),
		("Unexpected offset type for current addressing mode"));

      if (TN_is_symbol(offset)) {
	for (i=0; i<nb_access; i++) {
	  if (i!=0) {
	    offset = Dup_TN(offset);
	    Set_TN_offset(offset, TN_offset(offset)+current_mem_access_size);
	  }
	  if ((base_type == AM_BASE_GP) && !TN_is_reloc_none(offset)) {
	    Set_GP_rel_reloc(offset, EXTENSION_REGISTER_CLASS_to_MTYPE(result_rc, current_reg_access_size));
	  }
	  Build_OP(top_load, result_sub_tns[i], True_TN, base, offset, ops);
	}
      }
      else {  // TN_has_value(offset)
	tmp_val = TN_value(offset) + (INT64)((nb_access-1)*current_mem_access_size);
	if ((tmp_val >= 0) &&
            (TOP_opnd_immediate_variant(top_load, offset_idx, tmp_val) !=
             TOP_UNDEFINED)) {
          TOP effective_top;
	  for (i=0; i<nb_access; i++) {
	    if (i!=0) {
	      offset = Dup_TN(offset);
	      Set_TN_value(offset, TN_value(offset)+current_mem_access_size);
	    }
	    if ((base_type == AM_BASE_GP) && !TN_is_reloc_none(offset)) {
	      Set_GP_rel_reloc(offset, EXTENSION_REGISTER_CLASS_to_MTYPE(result_rc, current_reg_access_size));
	    }
            effective_top = TOP_opnd_immediate_variant(top_load, offset_idx, TN_value(offset));
	    Build_OP(effective_top, result_sub_tns[i], True_TN, base, offset, ops);
	  }
	}
	else {
	  // Create a new base and update offset (to fit addressing mode offset range)
	  tmp_base   = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	  tmp_offset = Gen_Literal_TN(TN_value(offset), 4);
	  Expand_Immediate(tmp_base, tmp_offset, MTYPE_I4, ops);
	  Build_OP(TOP_addu_r, tmp_base, True_TN, tmp_base, base, ops);
          Set_OP_carryisignored (OPS_last(ops));
	  EXTENSION_Release_Register_TN(availTemps, base);
	  base = tmp_base;
	
	  // AM_DISPL_POS_U8
	  top_tmp = rc_info->get_load_TOP(current_reg_access_size, AM_BASE_DEFAULT,
					  /*offs_is_imm*/true, /*offs_is_incr*/true, pixel_size);
	  FmtAssert((top_tmp!=TOP_UNDEFINED), ("Cannot find expected load instruction!"));
	
	  for (i=0; i<nb_access; i++) {
	    tmp_offset = Gen_Literal_TN(i*current_mem_access_size, 4, 1);
	    Build_OP(top_tmp, result_sub_tns[i], True_TN, base, tmp_offset, ops);
	  }
	}
      }
      break;

    default:
      // -----------------------------------------------
      // AddrMode:    Unexpected base!!
      // -----------------------------------------------
      FmtAssert(0, ("Unexpected base register type"));
    }

    if(!isAllocated)
        {
            // Compose the result register
            Build_OP_simulated_compose(result, True_TN, nb_access,
                                       result_sub_tns, ops);
        }

    return RestoreAvailableResourceForLoad(result, TRUE, availTemps, saveSet);
    break;

  default:
    // ===============================================================
    // Composite register size:  Unexpected value!!
    // ===============================================================
    FmtAssert(0, ("EXTENSION_Expand_Load(): Unexpected composite register size: %d", nb_access));
  }
  return RestoreAvailableResourceForLoad(result, FALSE, availTemps, saveSet);
}


/* =============================================================================
 * Real store expansion function, used to produce both (base + immediate)
 * and (base + reg).
 * Note: Auto-modified addressing mode are not generated here.
 *       This is handle later in the compilation flow (in EBO).
 * =============================================================================
 */
static
BOOL EXTENSION_Expand_Store(AM_Base_Reg_Type base_type,
			    BOOL             offs_is_imm,
			    BOOL             offs_is_incr,
			    TN              *src,
			    TN              *base,
			    TN              *offset,
			    OPS             *ops,
			    UINT32           data_align,
			    REGISTER_SET* availTemps,
                            UINT32 pixel_size) {
  const EXTENSION_Regclass_Info *rc_info;
  TN *src_sub_tns[MAX_SUB_TNS];
  TN *tmp_offset;
  TN *tmp_base;
  INT64 tmp_val;
  TOP top_store, top_tmp;
  TOP top_misalign;
  INT nb_access;
  INT current_reg_access_size;
  INT current_mem_access_size;
  INT i;

  ISA_REGISTER_CLASS src_rc;
  INT src_rc_size;
  INT src_size;
  REGISTER_SET saveSet[ISA_REGISTER_CLASS_MAX + 1];

  CopyRegisterSets(saveSet, availTemps);

  src_rc   = TN_register_class(src);
  src_size = TN_size(src);
  src_rc_size = ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(src_rc)) / CHAR_BIT;

  rc_info = EXTENSION_get_REGISTER_CLASS_info(src_rc);

  // ##########################################
  // ## Retrieve the widest store TOP        ##
  // ## for current TN and define the number ##
  // ## of access required (1, 2 or 4 parts) ##
  // ##########################################
  top_store = EXTENSION_Get_Real_Load_Store(/* reg_rc       */ src_rc,
					    /* reg_size     */ src_size,
					    /* data_align   */ data_align,
					    /* is_load      */ FALSE,
					    /* base_type    */ base_type,
					    /* offs_is_imm  */ offs_is_imm,
					    /* offs_is_incr */ TRUE,
					    /* top_misalign */ &top_misalign,
                                            pixel_size );

  if (top_store == TOP_UNDEFINED) {
    // No store found for current extension register!!
    // (except a possibly misaligned one, handled later)
    // Looking for an alternative
    extcg_move_corext_t cginfo;
    if (pixel_size>0) // core alternative is not available for pixel loads.
      {
        char error_msg[256];
        sprintf(error_msg,
                "[%s] pixel store not available for type of size %d,"
                "pixel size %d and alignment %d.",
                EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(src_rc),
                src_size, pixel_size, data_align);
        ErrMsg(EC_Ext_Expand, error_msg);
        return RestoreAvailableResourceForStore(src, FALSE, availTemps, saveSet);
      }
    BOOL exist_move = EXTENSION_Exist_Copy_To_Core(src_rc, src_size, &cginfo);
    if (exist_move) {
      if (cginfo.type == EXTCG_EXTRACT_X2R) {
	// ---------------------------------------------------------------------
	// RESCUE CODE 1:                    (not optimal but nearly never used)
	// MOVE 16 or 32 bits from extension reg to Core reg and STORE it in mem
	// ---------------------------------------------------------------------
	TN *gpr = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, ISA_REGISTER_CLASS_gpr, 4);
	Build_OP(cginfo.top_extract, gpr, True_TN, src, ops);
	// ... store chunk in mem
	Expand_Store((src_size==1)?MTYPE_U1:((src_size==2)?MTYPE_U2:MTYPE_U4),
		     gpr, base, offset, ops);
	return RestoreAvailableResourceForStore(src, TRUE, availTemps, saveSet);
      }
      else if (cginfo.type == EXTCG_EXTRACT_CSX2R) {
	// -----------------------------------------------------------------------
	// RESCUE CODE 2:                      (not optimal but nearly never used)
	// POP 32 bits chunks from extension reg to Core reg and STORE them in mem
	// -----------------------------------------------------------------------
	INT nb_sub_TNs, nb_chunk, subid;
	INT sub_TNs_size;
	sub_TNs_size = cginfo.sub_TNs_size;
	nb_sub_TNs   = cginfo.nb_sub_TNs;
	nb_chunk     = sub_TNs_size / 4; // count of 32 bits chunks per sub TNs


	// Prepare AM for incrementation
	Transform_AM_For_Immediate_Offset(&base, &offset, offs_is_imm, 0, ops,
                                      availTemps);

	// Extract sub TNs, if required
        bool isAllocated = FALSE;
	if (nb_sub_TNs > 1) {
	  // Create temporary sub-registers
	  Create_Sub_Regs(availTemps, src_sub_tns, src, nb_sub_TNs, sub_TNs_size,
			  &isAllocated);
	  
	  if(!isAllocated) {
	    Build_OP_simulated_extract(nb_sub_TNs, src_sub_tns, True_TN,
				       src, ops);
	  }
	}
	else {
	  isAllocated = TN_is_dedicated(src);
	}
	
	// Save 32 bits chunks
	for (subid=0; subid<nb_sub_TNs; subid++) {
	  TN *sub_src = (nb_sub_TNs == 1) ? src : src_sub_tns[subid];
	  for (i=0; i<nb_chunk; i++) {
	    TN *gpr = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, ISA_REGISTER_CLASS_gpr, 4);
	    // ... create tempo TN for shifted reg. (each tempo reg. must be defined only once)
	    TN *next_sub_src = isAllocated ? sub_src : Gen_Register_TN(TN_register_class(sub_src), TN_size(sub_src));
	    // ... move the 32 bits to core reg
	    Build_OP(cginfo.top_extract_rotate, gpr, next_sub_src, True_TN, sub_src, ops);
	    sub_src = next_sub_src;
	    // ... store chunk in mem
	    Expand_Store(MTYPE_U4, gpr, base, offset, ops);
	    // ... increment offset
	    offset = Duplicate_Offset_TN_And_Increment(offset, 4);
	    EXTENSION_Release_Register_TN(availTemps, gpr);
	  }
	}
	return RestoreAvailableResourceForStore(src, TRUE, availTemps, saveSet);
      }
    }

    if (top_misalign != TOP_UNDEFINED) {
      // -------------------------------------
      // Available store is too constrained...
      // -------------------------------------
      char error_msg[256];
      sprintf(error_msg, "[%s] Failed to generate store from extension registers to "
	      "memory aligned on %d bytes. Available one is aligned on %d bytes,"
	      " and lack of predefined instructions does not allow usage of core store.",
	      EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(src_rc),
	      data_align, TOP_Mem_Alignment(top_misalign));
      ErrMsg(EC_Ext_Expand, error_msg);
      return RestoreAvailableResourceForStore(src, FALSE, availTemps, saveSet);
    }
    else {
      char error_msg[256];
      sprintf(error_msg, "[%s] No store found in extension for register of size %d bytes,"
	      "and lack of predefined instructions does not allow usage of core store.",
	      EXTENSION_Get_Extension_Name_From_REGISTER_CLASS(src_rc),
	      src_size);
      ErrMsg(EC_Ext_Expand, error_msg);
      return RestoreAvailableResourceForStore(src, FALSE, availTemps, saveSet);
    }
  }

  nb_access = src_size/TOP_Mem_Bytes(top_store);

  // [VCdV] for pixel mtypes, we only need to store half the size of the
  // src register. ( TOP_Mem_Bytes(STXPIX) == TOP_Mem_Bytes(STX) / 2 ).
  if (pixel_size>0) {
    nb_access/=2;
  }

  INT offset_idx = TOP_Find_Operand_Use(top_store, OU_offset);

  // ###############################
  // ## Effective code generation ##
  // ###############################
  switch (nb_access) {
  case 1:
    // ===============================================================
    // Composite register size:  1
    // ===============================================================
    if (offs_is_imm) {
      if (TN_has_value(offset) ) {
        top_store = 
          TOP_opnd_immediate_variant(top_store, offset_idx, TN_value(offset));
      }
    }

    Build_OP(top_store, True_TN, base, offset, src, ops);
    return RestoreAvailableResourceForStore(src, TRUE, availTemps, saveSet);

  case 2:
  case 4:
    // ===============================================================
    // Composite register size:  2, 4
    // ===============================================================    
    current_reg_access_size = src_size/nb_access;

    // From the compiler point of view, a pixel data takes
    // half less bytes in memory than in register.
    if (pixel_size > 0) {
      current_mem_access_size = current_reg_access_size/2;
    } else {
      current_mem_access_size = current_reg_access_size;
    }

    {
        bool isAllocated;
        // Create temporary sub-registers
        Create_Sub_Regs(availTemps, src_sub_tns, src, nb_access,
                        current_reg_access_size, &isAllocated);

        if(!isAllocated)
            {
                Build_OP_simulated_extract(nb_access, src_sub_tns, True_TN, src,
                                           ops);
            }
    }

    switch (base_type) {
    case AM_BASE_DEFAULT:
      if (offs_is_imm) {
        FmtAssert((offs_is_incr), ("offset is always incremental !"));
	  // -----------------------------------------------
	  // AddrMode:    (Rn + #imm_u8)
	  // -----------------------------------------------
	  //
	  // if (immediate value of all store addressing mode fits in u8)
	  //     store  low,  @( Rn + #u8 )
	  //     ...
	  //     store  high, @( Rn + #(u8 + (nb_access-1)*current_mem_access_size) )
	  // else
	  //     add   Rt, Rn, #(nb_access*current_mem_access_size)
	  //     store  low,  @( Rt + (#u8 - #(nb_access*current_mem_access_size) )
	  //     ...
	  //     store  high, @( Rt + (#u8 - #current_mem_access_size) )
	  FmtAssert(TN_is_constant(offset) && 
		    (TN_has_value(offset) || TN_is_symbol(offset)),
		    ("Unexpected offset type for current addressing mode"));

	  // Check offset range (only for value TN with positive offset)
	  if (TN_has_value(offset) && TN_value(offset)>0) {
	    tmp_val = TN_value(offset) + (INT64)(current_mem_access_size*(nb_access-1));
            if (TOP_opnd_immediate_variant(top_store, offset_idx, tmp_val) ==
                TOP_UNDEFINED) {
	      // Not all offsets will fit in current addressing mode
	      // -> Increment base and decrement offset
	      INT64 dec = (INT64)(current_mem_access_size*nb_access);

	      TN *tmp_offset = Dup_TN(offset);
	      Set_TN_value(tmp_offset, dec);

          // register used by base can be released, since tmp_base is used as
          // write parameter and base as read one and then base is replaced by
          // tmp_base
          EXTENSION_Release_Register_TN(availTemps, base);
	      tmp_base = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	      Build_OP(TOP_addu_i8, tmp_base, True_TN, base, tmp_offset, ops);
              Set_OP_carryisignored (OPS_last(ops));
	      base = tmp_base;
	      
	      offset = Dup_TN(offset);
	      Set_TN_value(offset, TN_value(offset) - dec);
	    }
	  }

	  // Generate the move sequence
          TOP effective_top = top_store;
	  for (i=0; i<nb_access; i++) {
	    if (i!=0) {
	      offset = Dup_TN(offset);
	      if (TN_has_value(offset)) {
		Set_TN_value(offset, TN_value(offset)+current_mem_access_size);
                effective_top = TOP_opnd_immediate_variant(top_store,
                                                           offset_idx,
                                                           TN_value(offset));
	      }
	      else { // TN_is_symbol
		Set_TN_offset(offset, TN_offset(offset)+current_mem_access_size);
                effective_top = top_store;
	      }
	    } else {
              if (TN_has_value(offset)) {
                effective_top = TOP_opnd_immediate_variant(top_store,
                                                           offset_idx,
                                                           TN_value(offset));
              }
            }
	    Build_OP(effective_top, True_TN, base, offset, src_sub_tns[i], ops);
	  }
      }
      else {  // !offs_is_imm
	// -----------------------------------------------
	// AddrMode:    (Rn + Rp)
	// -----------------------------------------------
	//
	// add   Rt, Rn, Rp
	// store  low,  @( Rt + #0 )
	// ...
	// store  high, @( Rt + #(nb_access-1)*current_mem_access_size )
	//

	// register used by base can be released, since tmp_base is used as write
	// parameter and base as read one and then base is replaced by tmp_base
	EXTENSION_Release_Register_TN(availTemps, base);
	tmp_base = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	Build_OP(TOP_addu_r, tmp_base, True_TN, base, offset, ops);
        Set_OP_carryisignored (OPS_last(ops));
	base = tmp_base;
	
	top_tmp = rc_info->get_store_TOP(current_reg_access_size, AM_BASE_DEFAULT, 
					/*offs_is_imm*/true, /*offs_is_incr*/true, pixel_size);
	FmtAssert((top_tmp!=TOP_UNDEFINED), ("Cannot find expected store instruction!"));
	
	for (i=0; i<nb_access; i++) {
	  tmp_offset = Gen_Literal_TN(i*current_mem_access_size, 4, 1);
	  Build_OP(top_tmp, True_TN, base, tmp_offset, src_sub_tns[i], ops);
	}
      }
      break;

    case AM_BASE_GP:
    case AM_BASE_SP:
      // -----------------------------------------------
      // AddrMode:    (GP + #imm_u12)
      //              (SP + #imm_u12)
      // -----------------------------------------------
      // (GP + #imm_u12)
      // ---------------
      // if (all immediate offset remains in U12 range)
      //     store  low,  @( R13 + #u12 )
      //     ...
      //     store  high, @( R13 + #(u12 + (nb_access-1)*current_mem_access_size)) )
      // else
      //     make  Rt, #u12
      //     add  Rt, R13, Rt
      //     store  low,  @( Rt + #0 )
      //     ...
      //     store  high, @( Rt + (nb_access-1)*current_mem_access_size)
      //
      // (SP + #imm_u12)
      // ---------------
      // (Same handling except that R13 is replace by R15)
      //
      FmtAssert(TN_is_constant(offset) && 
		(TN_has_value(offset) || TN_is_symbol(offset)),
		("Unexpected offset type for current addressing mode"));

      if (TN_is_symbol(offset)) {
	for (i=0; i<nb_access; i++) {
	  if (i!=0) {
	    offset = Dup_TN(offset);
	    Set_TN_offset(offset, TN_offset(offset)+current_mem_access_size);
	  }
	  if ((base_type == AM_BASE_GP) && !TN_is_reloc_none(offset)) {
	    Set_GP_rel_reloc(offset, EXTENSION_REGISTER_CLASS_to_MTYPE(src_rc, current_reg_access_size));
	  }
	  Build_OP(top_store, True_TN, base, offset, src_sub_tns[i], ops);
	}
      }
      else {  // TN_has_value(offset)
	tmp_val = TN_value(offset) + (INT64)((nb_access-1)*current_mem_access_size);
	if ((tmp_val >= 0) &&
            (TOP_opnd_immediate_variant(top_store, offset_idx, tmp_val) !=
             TOP_UNDEFINED)) {
          TOP effective_top;
	  for (i=0; i<nb_access; i++) {
	    if (i!=0) {
	      offset = Dup_TN(offset);
	      Set_TN_value(offset, TN_value(offset)+current_mem_access_size);
	    }
	    if ((base_type == AM_BASE_GP) && !TN_is_reloc_none(offset)) {
	      Set_GP_rel_reloc(offset, EXTENSION_REGISTER_CLASS_to_MTYPE(src_rc, current_reg_access_size));
	    }
            effective_top = TOP_opnd_immediate_variant(top_store, offset_idx, TN_value(offset));
	    Build_OP(effective_top, True_TN, base, offset, src_sub_tns[i], ops);
	  }
	}
	else {
	  // Create a new base and update offset (to fit addressing mode offset range)
	  tmp_base   = EXTENSION_Gen_Register_TN_From_Available_Set(availTemps, TN_register_class(base), TN_size(base));
	  tmp_offset = Gen_Literal_TN(TN_value(offset), 4);
	  Expand_Immediate(tmp_base, tmp_offset, MTYPE_I4, ops);
	  Build_OP(TOP_addu_r, tmp_base, True_TN, tmp_base, base, ops);
          Set_OP_carryisignored (OPS_last(ops));
	  EXTENSION_Release_Register_TN(availTemps, base);
	  base = tmp_base;
	  
	  // AM_DISPL_POS_U8
	  top_tmp = rc_info->get_store_TOP(current_reg_access_size, AM_BASE_DEFAULT,
					   /*offs_is_imm*/true, /*offs_is_incr*/true, pixel_size);
	  FmtAssert((top_tmp!=TOP_UNDEFINED), ("Cannot find expected load instruction!"));

	  for (i=0; i<nb_access; i++) {
	    tmp_offset = Gen_Literal_TN(i*current_mem_access_size, 4, 1);
	    Build_OP(top_tmp, True_TN, base, tmp_offset, src_sub_tns[i], ops);
	  }
	}
      }
      break;

    default:
      // -----------------------------------------------
      // AddrMode:    Unexpected base!!
      // -----------------------------------------------
      FmtAssert(0, ("Unexpected base register type"));
    }
	
    return RestoreAvailableResourceForStore(src, TRUE, availTemps, saveSet);
    break;
	
  default:
    // ===============================================================
    // Composite register size:  Unexpected value!!
    // ===============================================================
    FmtAssert(0, ("EXTENSION_Expand_Store(): Unexpected composite register size: %d", nb_access));
  }
  return RestoreAvailableResourceForStore(src, FALSE, availTemps, saveSet);
}


/* =============================================================================
 * Expand load with immediate. Return FALSE if fail.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Load_Imm(AM_Base_Reg_Type base_type, TN *result, TN *base,
			       TN *ofst, OPS *ops, UINT32 data_align,
                               UINT32 pixel_size) {
  BOOL done;
  // Note: offs_is_incr (3rd argument) always set to true
  //       Hack to handle addressing modes as signed 9 bits.
  //       The target description defines the ld/st_i8 as signed 9 bits.
  //       This allows us to always use the inc form even for negative
  //       immediates.
  //       The rewriting to _inc or _dec form is done later in the code generator.
  //       This is a temporary scheme until we have a property for specifying
  //       that a load/store offset is substracted to the base and the code 
  //       generator is fixed to handle this.
  done = EXTENSION_Expand_Load(base_type, true, true, result, base, ofst, ops, data_align, NULL, pixel_size);
  return (done);
}

/*
 * Expand load with immediate. Return FALSE if fail.
 */
BOOL EXTENSION_Expand_Load_Imm_With_Allocation(AM_Base_Reg_Type base_type, TN *result, TN *base,
                                               TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps,
                                               UINT32 pixel_size) {
  BOOL done;
  // Note: offs_is_incr (3rd argument) always set to true
  //       Hack to handle addressing modes as signed 9 bits.
  //       The target description defines the ld/st_i8 as signed 9 bits.
  //       This allows us to always use the inc form even for negative
  //       immediates.
  //       The rewriting to _inc or _dec form is done later in the code generator.
  //       This is a temporary scheme until we have a property for specifying
  //       that a load/store offset is substracted to the base and the code 
  //       generator is fixed to handle this.
  done = availTemps && EXTENSION_Expand_Load(base_type, true, true, result, base, ofst, ops, data_align, availTemps, pixel_size);
  return (done);
}


/* =============================================================================
 * Expand load with register. Return FALSE if fail.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Load_Reg(AM_Base_Reg_Type base_type, TN *result, TN *base,
			       TN *ofst, OPS *ops, UINT32 data_align,
                               UINT32 pixel_size) {
  BOOL done;
  done = EXTENSION_Expand_Load(base_type, false, true, result, base, ofst, ops, data_align, NULL, pixel_size);
  return (done);
}

/*
 * Expand load with register. Return FALSE if fail.
 */
BOOL EXTENSION_Expand_Load_Reg_With_Allocation(AM_Base_Reg_Type base_type, TN *result, TN *base,
                                               TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps,
                                               UINT32 pixel_size) {
  BOOL done;
  done = availTemps && EXTENSION_Expand_Load(base_type, false, true, result, base, ofst, ops, data_align, availTemps, pixel_size);
  return (done);
}

/* =============================================================================
 * Expand store with immediate. Return FALSE if fail.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Store_Imm(AM_Base_Reg_Type base_type, TN *src, TN *base,
				TN *ofst, OPS *ops, UINT32 data_align,
                                UINT32 pixel_size) {
  BOOL done;
  // Note: offs_is_incr (3rd argument) always set to true
  //       Hack to handle addressing modes as signed 9 bits.
  //       The target description defines the ld/st_i8 as signed 9 bits.
  //       This allows us to always use the inc form even for negative
  //       immediates.
  //       The rewriting to _inc or _dec form is done later in the code generator.
  //       This is a temporary scheme until we have a property for specifying
  //       that a load/store offset is substracted to the base and the code 
  //       generator is fixed to handle this.
  done = EXTENSION_Expand_Store(base_type, true, true, src, base, ofst, ops, data_align, NULL, pixel_size);
  return (done);
}

/*
 * Expand store with immediate. Return FALSE if fail.
 */
BOOL EXTENSION_Expand_Store_Imm_With_Allocation(AM_Base_Reg_Type base_type, TN *src, TN *base,
                                                TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps,
                                                UINT32 pixel_size) {
  BOOL done;
  // Note: offs_is_incr (3rd argument) always set to true
  //       Hack to handle addressing modes as signed 9 bits.
  //       The target description defines the ld/st_i8 as signed 9 bits.
  //       This allows us to always use the inc form even for negative
  //       immediates.
  //       The rewriting to _inc or _dec form is done later in the code generator.
  //       This is a temporary scheme until we have a property for specifying
  //       that a load/store offset is substracted to the base and the code 
  //       generator is fixed to handle this.
  done = availTemps && EXTENSION_Expand_Store(base_type, true, true, src, base, ofst, ops, data_align, availTemps, pixel_size);
  return (done);
}

/* =============================================================================
 * Expand store with register. Return FALSE if fail.
 * =============================================================================
 */
BOOL EXTENSION_Expand_Store_Reg(AM_Base_Reg_Type base_type, TN *src, TN *base,
				TN *ofst, OPS *ops, UINT32 data_align,
                                UINT32 pixel_size) {
  BOOL done;
  done = EXTENSION_Expand_Store(base_type, false, true, src, base, ofst, ops, data_align, NULL, pixel_size);
  return (done);
}

/*
 * Expand store with register. Return FALSE if fail.
 */
BOOL EXTENSION_Expand_Store_Reg_With_Allocation(AM_Base_Reg_Type base_type, TN *src, TN *base,
                                                TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps,
                                                UINT32 pixel_size) {
  BOOL done;
  done = availTemps && EXTENSION_Expand_Store(base_type, false, true, src, base, ofst, ops, data_align, availTemps, pixel_size);
  return (done);
}
