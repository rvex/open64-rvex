/*
 
  Copyright (C) 2007 ST Microelectronics, Inc.  All Rights Reserved.
 
  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.
  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 
  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement
  or the like.  Any license provided herein, whether implied or
  otherwise, applies only to this software file.  Patent licenses, if
  any, provided herein do not apply to combinations of this program with
  other software, or any other product whatsoever.
  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.
 
  Contact information:  ST Microelectronics, Inc.,
  , or:
 
  http://www.st.com
 
  For further information regarding this notice, see:
 
  http:
*/

/*
 * Date: 2006/08/01
 * Desc: These functions are used to expand extension specific operations,
 *       such as compose/extract/widemove/load/store.
 */

#ifndef EXP_EXTENSION_H_INCLUDED
#define EXP_EXTENSION_H_INCLUDED

#include "defs.h"
#include "config.h"

#include "intrn_info.h"
#include "cgir.h"

#include "dyn_isa_api.h" /* for reconfigurability */
#include "isa_loader_api.h"
#include "lai_loader_api.h"

/* pixel_size value for EXTENSION_load/store() is zero for non pixel types.  */
#define NOT_A_PIXEL 0

// ===================================================================
// ==
// ==  Code selection API for extension register class
// ==
// ===================================================================

/* Generate simulated compose/extract */
void Build_OP_simulated_extract(TN *tgt0, TN *tgt1, TN *guard, TN *src, OPS *ops);
void Build_OP_simulated_extract(TN *tgt0, TN *tgt1, TN *tgt2, TN *tgt3, TN *guard, TN *src, OPS *ops);
void Build_OP_simulated_extract(INT nb_tgt, TN **tgt_tab, TN *guard, TN *src, OPS *ops);
void Build_OP_simulated_compose(TN *tgt, TN *guard, TN *src0, TN *src1, OPS *ops);
void Build_OP_simulated_compose(TN *tgt, TN *guard, TN *src0, TN *src1, TN *src2, TN *src3, OPS *ops);
void Build_OP_simulated_compose(TN *tgt, TN *guard, INT nb_src, TN **src_tab, OPS *ops);

/*
 * The following set of functions return TRUE if code generation was successfull,
 * FALSE otherwise (Load/Store might also produce an error message).
 */
/* Expand Copy between 2 extension regs or between 1 extension reg and 1 GPR */
BOOL EXTENSION_Expand_Copy(TN *tgt_tn, TN *guard, TN *src_tn, OPS *ops);

/* Expand Copy From a list of GPR to an extension register */
BOOL EXTENSION_Expand_Copy_From_Core(TN *tgt, TN *guard, INT nb_src, TN **src_tab, OPS *ops);

/* Expand Copy From an extension register to a list of GPR */
BOOL EXTENSION_Expand_Copy_To_Core(INT nb_tgt, TN **tgt_tab, TN *guard, TN *src, OPS *ops);

/* Expand Clear for  given extension register */
BOOL EXTENSION_Expand_Clr(TN *tn, OPS *ops);

/* Expand immediate 32bits to extension registers */
BOOL EXTENSION_Expand_Immediate(TN *result, TN *src, BOOL is_signed, OPS *ops);

/* Expand load with immediate. Return FALSE if fail. */
BOOL EXTENSION_Expand_Load_Imm(AM_Base_Reg_Type base_type, TN *result, TN *base, TN *ofst, OPS *ops, UINT32 data_align, UINT32 pixel_size);

/* Expand load with immediate. Return FALSE if fail. */
BOOL EXTENSION_Expand_Load_Imm_With_Allocation(AM_Base_Reg_Type base_type, TN *result, TN *base, TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps, UINT32 pixel_size);

/* Expand load with register. Return FALSE if fail. */
BOOL EXTENSION_Expand_Load_Reg(AM_Base_Reg_Type base_type, TN *result, TN *base, TN *ofst, OPS *ops, UINT32 data_align, UINT32 pixel_size);

/* Expand load with register. Return FALSE if fail. */
BOOL EXTENSION_Expand_Load_Reg_With_Allocation(AM_Base_Reg_Type base_type, TN *result, TN *base, TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps, UINT32 pixel_size);

/* Expand store with immediate. Return FALSE if fail. */
BOOL EXTENSION_Expand_Store_Imm(AM_Base_Reg_Type base_type, TN *src, TN *base, TN *ofst, OPS *ops, UINT32 data_align, UINT32 pixel_size);

/* Expand store with immediate. Return FALSE if fail. */
BOOL EXTENSION_Expand_Store_Imm_With_Allocation(AM_Base_Reg_Type base_type, TN *src, TN *base, TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps, UINT32 pixel_size);

/* Expand store with register. Return FALSE if fail. */
BOOL EXTENSION_Expand_Store_Reg(AM_Base_Reg_Type base_type, TN *src, TN *base, TN *ofst, OPS *ops, UINT32 data_align, UINT32 pixel_size);

/* Expand store with register. Return FALSE if fail. */
BOOL EXTENSION_Expand_Store_Reg_With_Allocation(AM_Base_Reg_Type base_type, TN *src, TN *base, TN *ofst, OPS *ops, UINT32 data_align, REGISTER_SET* availTemps, UINT32 pixel_size);

/* Return TRUE if it exists a single non-simulated instruction to copy
 * a composite register of class <rc> made of <nhardregs> atomic regs. */
BOOL EXTENSION_Exist_Single_OP_Copy(ISA_REGISTER_CLASS rc, INT nhardregs);

/**
 * Generate a TN register for given rclass and byteSize from given availSet.
 *
 * @param  availSet Array of available register set
 * @param  rclass register class
 * @param  byteSize byte size
 *
 * @pre    availSet implies availSet->size() >= ISA_REGISTER_CLASS_MAX + 1
 * @post   availSet has been updated to reflect newly used resource
 *
 * @return Generate register set
 */
TN*
EXTENSION_Gen_Register_TN_From_Available_Set(REGISTER_SET* availSet,
                                             ISA_REGISTER_CLASS rclass,
                                             INT byteSize);
/**
 * Release resource used by given tn in availSet.
 *
 * @param  availSet Array of available set
 * @param  tn temporary name to be released
 *
 * @pre    availSet implies availSet->size() >= ISA_REGISTER_CLASS_MAX + 1
 * @post   tn and availSet@pre and TN_is_dedicated(tn) implies resource used
 *         by tn has been added to availSet
 *
 */
void
EXTENSION_Release_Register_TN(REGISTER_SET* availSet, TN* tn);

/**
 * Take resource used by given tn in availSet.
 *
 * @param  availSet Array of available set
 * @param  tn temporary name to be taken
 *
 * @pre    availSet implies availSet->size() >= ISA_REGISTER_CLASS_MAX + 1
 * @post   tn and availSet@pre and TN_is_dedicated(tn) implies resource used
 *         by tn has been removed from availSet
 *
 */
void
EXTENSION_Take_Register_TN(REGISTER_SET* availSet, TN* tn);

TOP EXTENSION_Get_Real_Load_Store(ISA_REGISTER_CLASS reg_rc,
					 INT                reg_size,
					 UINT32             data_align,
					 BOOL               is_load,
					 AM_Base_Reg_Type   base_type,
					 BOOL               offs_is_imm,
					 BOOL               offs_is_incr,
					 TOP               *top_misalign,
                                  UINT32             pixel_size);
#endif
