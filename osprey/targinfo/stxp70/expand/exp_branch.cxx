/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


/* EXP routines for expanding branches */

#include "defs.h"
#include "erglob.h"
#include "ercg.h"
#include "tracing.h"
#include "config.h"
#include "config_TARG.h"
#include "topcode.h"
#include "tn.h"
#include "targ_isa_lits.h"
#include "cg_flags.h"
#include "op.h"
#include "cgexp.h"
#include "targ_sim.h"

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

/* ====================================================================
 *   Initialize_Branch_Variants
 * ====================================================================
 */
void
Initialize_Branch_Variants(void)
{
	// nothing to do
}

/* Import from exp_targ.cxx */
extern TN *Expand_Or_Inline_Immediate( OPERATION_WITH_INLINED_IMM operation, TN *src, TYPE_ID mtype, OPS *ops);


ISA_ENUM_CLASS_VALUE
Pick_Compare_Kind (VARIANT *variant, TN **src1, TN **src2, BOOL *is_integer,
                   // should the result be generated into a int reg
                   OPS *ops)
{
  ISA_ENUM_CLASS_VALUE cmp = ISA_ECV_UNDEFINED;
  ISA_REGISTER_CLASS src1_rc = ISA_REGISTER_CLASS_UNDEFINED;
  ISA_REGISTER_CLASS src2_rc = ISA_REGISTER_CLASS_UNDEFINED;

  /* 
   * If first operand is an immediate value, swap it with the
   * second:
   */
  if (*src1 != NULL && TN_has_value(*src1)) {
    // swap operands and change variant
    TN *tmp = *src1;
   *src1 = *src2;
    *src2 = tmp;
    *variant = Invert_BR_Variant(*variant);
  }

  if (*src1 != NULL && TN_is_constant(*src1)) {
    *src1 = Expand_Immediate_Into_Register (MTYPE_I4, *src1, ops);
  }
  if (*src2 != NULL && TN_is_constant(*src2)) {
    *src2 = Expand_Or_Inline_Immediate(INL_COMPAR,*src2, MTYPE_I4, ops);
  }

  src1_rc = *src1 != NULL ? TN_register_class(*src1) : ISA_REGISTER_CLASS_UNDEFINED;
  src2_rc = *src2 != NULL && TN_is_register(*src2) ? TN_register_class(*src2) : ISA_REGISTER_CLASS_UNDEFINED;


  // Whether the variant is float or int, we must copy operands to the right register class before the
  // comparison.
  // In the case of a float comparison, we get the result into an integer register.
  // In the case of a int comparison, we get the result in a predicate register.
  switch (*variant) {
    // Float Variant
    case V_BR_FGE:
    case V_BR_FGT:
    case V_BR_FLE:
    case V_BR_FLT:
    case V_BR_FEQ:
    case V_BR_FNE:
      if(src1_rc != ISA_REGISTER_CLASS_UNDEFINED && src1_rc != ISA_REGISTER_CLASS_fpr) {
	TN *tmp = Build_RCLASS_TN(ISA_REGISTER_CLASS_fpr);
	Expand_Copy(tmp, True_TN, *src1, ops);
	*src1 = tmp;
      }
      if(src2_rc != ISA_REGISTER_CLASS_UNDEFINED && src2_rc != ISA_REGISTER_CLASS_fpr) {
	TN *tmp = Build_RCLASS_TN(ISA_REGISTER_CLASS_fpr);
	Expand_Copy(tmp, True_TN, *src2, ops);
	*src2 = tmp;
      }
      *is_integer = TRUE;
      break;
  default:
    // Int variant
      if(src1_rc != ISA_REGISTER_CLASS_UNDEFINED && src1_rc != ISA_REGISTER_CLASS_gpr) {
	TN *tmp = Build_RCLASS_TN(ISA_REGISTER_CLASS_gpr);
	Expand_Copy(tmp, True_TN, *src1, ops);
	*src1 = tmp;
      }
      if(src2_rc != ISA_REGISTER_CLASS_UNDEFINED && src2_rc != ISA_REGISTER_CLASS_gpr) {
	TN *tmp = Build_RCLASS_TN(ISA_REGISTER_CLASS_gpr);
	Expand_Copy(tmp, True_TN, *src2, ops);
	*src2 = tmp;
      }
      *is_integer = FALSE;
    break;
  }
  
  // pick tops
  switch (*variant) {
  case V_BR_I8GE:
  case V_BR_U8GE:
  case V_BR_I4GE:
  case V_BR_U4GE:
    cmp = ISA_ECV_cmp_ge;
    break;
  case V_BR_FGE:
  case V_BR_DGE:
    cmp = ISA_ECV_cp_ge;
    break;
  case V_BR_I8GT:
  case V_BR_U8GT:
  case V_BR_I4GT:
  case V_BR_U4GT:
    cmp = ISA_ECV_cmp_gt;
    break;
  case V_BR_FGT:
  case V_BR_DGT:
    cmp = ISA_ECV_cp_gt;
    break;
  case V_BR_I8LE:
  case V_BR_U8LE:
  case V_BR_I4LE:
  case V_BR_U4LE:
    cmp = ISA_ECV_cmp_le;
    break;
  case V_BR_FLE:
  case V_BR_DLE:
    cmp = ISA_ECV_cp_le;
    break;
  case V_BR_I8LT:
  case V_BR_U8LT:
  case V_BR_I4LT:
  case V_BR_U4LT:
    cmp = ISA_ECV_cmp_lt;
    break;
  case V_BR_FLT:
  case V_BR_DLT:
    cmp = ISA_ECV_cp_lt;
    break;
  case V_BR_I8EQ:
  case V_BR_U8EQ:
  case V_BR_I4EQ:
  case V_BR_U4EQ:
    cmp = ISA_ECV_cmp_eq;
    break;
  case V_BR_FEQ:
  case V_BR_DEQ:
    cmp = ISA_ECV_cp_eq;
    break;
  case V_BR_I8NE:
  case V_BR_U8NE:
  case V_BR_I4NE:
  case V_BR_U4NE:
    cmp = ISA_ECV_cmp_ne;
    break;
  case V_BR_FNE:
  case V_BR_DNE:
    cmp = ISA_ECV_cp_ne;
    break;
  default:
    break;
  }
  return cmp;
}

TOP
Pick_Comparison_TOP(VARIANT *variant, TN **src2, OPS *ops)
{
  TYPE_ID mtype;
  TOP cmp = TOP_UNDEFINED;
  switch (*variant) {
  case V_BR_I8GE:
  case V_BR_I8GT:
  case V_BR_I8LE:
  case V_BR_I8LT:
  case V_BR_I8EQ:
  case V_BR_I8NE:
    mtype= MTYPE_I8;
    break;
  case V_BR_U8GE:
  case V_BR_U8GT:
  case V_BR_U8LE:
  case V_BR_U8LT:
  case V_BR_U8EQ:
  case V_BR_U8NE:
    mtype= MTYPE_U8;
    break;
  case V_BR_I4GE:
  case V_BR_I4GT:
  case V_BR_I4LE:
  case V_BR_I4LT:
  case V_BR_I4EQ:
  case V_BR_I4NE:
    mtype= MTYPE_I4;
    cmp = TOP_cmp_r_cmp_g;
    break;
  case V_BR_U4GE:
  case V_BR_U4GT:
  case V_BR_U4LE:
  case V_BR_U4LT:
  case V_BR_U4EQ:
  case V_BR_U4NE:
    mtype= MTYPE_U4;
    cmp = TOP_cmp_r_u_cmp_g;
    break;
  case V_BR_FGE:
  case V_BR_FGT:
  case V_BR_FLE:
  case V_BR_FLT:
  case V_BR_FEQ:
  case V_BR_FNE:
    mtype= MTYPE_F4;
    cmp = TOP_fpx_fcmp_cp_g;
    break;
  case V_BR_DGE:
  case V_BR_DGT:
  case V_BR_DLE:
  case V_BR_DLT:
  case V_BR_DEQ:
  case V_BR_DNE:
    mtype= MTYPE_F8;
    cmp= TOP_UNDEFINED;
    break;
  default:
    break;
  }
  if (cmp != TOP_UNDEFINED) {
    // if src2 is immediate, get the immediate form
    if (*src2 != NULL && TN_has_value(*src2)) {
      TOP cmp_i;
      cmp_i = TOP_opnd_immediate_variant(cmp, TOP_Find_Operand_Use(cmp,OU_opnd2), TN_value(*src2));
      if (cmp_i != TOP_UNDEFINED) {
        return cmp_i;
      } else {
	*src2 = Expand_Immediate_Into_Register(mtype, *src2, ops);
	return cmp;
      }
    }
  }
  return cmp;
}





/* ====================================================================
 *   Pick_Compare_TOP
 *
 *   Check that compare is of proper form, and return TOP to use for 
 *   the compare.
 *   May modify the variant and src tns.
 *
 *   Modify the is_integer parameter to reflect if the top result is
 *   a branch or an int.
 * ====================================================================
 */
TOP
Pick_Compare_TOP (
  VARIANT *variant, 
  TN **src1, 
  TN **src2, 
  BOOL *is_integer,      // should the result be generated into a int
                         // reg
  ISA_ENUM_CLASS_VALUE *relop,
  OPS *ops
)
{
  *relop = Pick_Compare_Kind(variant, src1, src2, is_integer, ops);
  return Pick_Comparison_TOP(variant, src2, ops);
}

/* ====================================================================
 *   Expand_Branch
 * ====================================================================
 */
void
Expand_Branch ( 
  TN *targ, 
  TN *src1, 
  TN *src2, 
  VARIANT variant, 
  OPS *ops
)
{
  TOP   cmp;
  ISA_ENUM_CLASS_VALUE relop = ISA_ECV_UNDEFINED;


  // If the branch is conditional FALSEBR, we must invert the condition 
  // because ST200 branches are active on TRUE.
  //  if (V_br_condition(variant) != V_BR_ALWAYS &&
  //      V_br_condition(variant) != V_BR_NEVER &&
  //      V_false_br(variant) ) 
  //    variant = Negate_BR_Variant(variant);

  BOOL  false_br = V_false_br(variant);
  VARIANT cond = V_br_condition(variant);
  BOOL is_integer = FALSE;

  /* Trace if required: */
  if (Trace_Exp) {
    fprintf ( TFile, "<cgexp> Translating %s branch:\n",
                                    (false_br ? "false" : "true") );
  }

  /* default branch instruction on: */
  if (cond == V_BR_NONE) {
    FmtAssert(FALSE,("Expand_Branch: default branch ??"));
    return;
  }

  FmtAssert( cond <= V_BR_LAST, ("unexpected variant in Expand_Branch"));
  FmtAssert( cond != V_BR_NONE, ("BR_NONE variant in Expand_Branch"));

  if (Trace_Exp) {
    fprintf (TFile, "<cgexp> branch cond = %s\n", BR_Variant_Name(cond));
  }

  // check for special case of second arg being zero.
  if (src2 != NULL && TN_is_zero(src2)) {
    switch (cond) {
      case V_BR_U8LT:	
      case V_BR_U4LT:	
      case V_BR_FLT:	
	cond = V_BR_NEVER; break;
      case V_BR_U8GE:
      case V_BR_U4GE:
      case V_BR_FGE:
	cond = V_BR_ALWAYS; break;
    }
  }

  if(cond == V_BR_P_TRUE) {
    Build_OP (TOP_jr, src1, targ, ops); 
    if (false_br) {
      OP *op = OPS_last(ops);
      Set_OP_Pred_False(op, OP_find_opnd_use(op, OU_condition));
    }
    return;
  }

  if(false_br) {
    switch(cond) {
    case V_BR_NEVER: cond = V_BR_ALWAYS; break;
    case V_BR_ALWAYS: cond = V_BR_NEVER; break;
    case V_BR_I4EQ: cond = V_BR_I4NE; break;
    case V_BR_I4NE: cond = V_BR_I4EQ; break;
    case V_BR_I4GT: cond = V_BR_I4LE; break;
    case V_BR_I4GE: cond = V_BR_I4LT; break;
    case V_BR_I4LT: cond = V_BR_I4GE; break;
    case V_BR_I4LE: cond = V_BR_I4GT; break;
    case V_BR_U4EQ: cond = V_BR_U4NE; break;
    case V_BR_U4NE: cond = V_BR_U4EQ; break;
    case V_BR_U4GT: cond = V_BR_U4LE; break;
    case V_BR_U4GE: cond = V_BR_U4LT; break;
    case V_BR_U4LT: cond = V_BR_U4GE; break;
    case V_BR_U4LE: cond = V_BR_U4GT; break;
    case V_BR_FEQ: cond = V_BR_FNE; break;
    case V_BR_FNE: cond = V_BR_FEQ; break;
    case V_BR_FGT: cond = V_BR_FLE; break;
    case V_BR_FGE: cond = V_BR_FLT; break;
    case V_BR_FLT: cond = V_BR_FGE; break;
    case V_BR_FLE: cond = V_BR_FGT; break;
    case V_BR_DNE: cond = V_BR_DEQ; break;
    case V_BR_DEQ: cond = V_BR_DNE; break;
    default:
      FmtAssert(FALSE,("Unimplemented condition"));
    }
  }


  // compare should calculate a guard reg result, gpr result 
  // supported only for floating point comparisons.

  cmp = Pick_Compare_TOP (&cond, &src1, &src2, &is_integer, &relop, ops);

  if (Trace_Exp) {
    fprintf (TFile, "<cgexp> transformed branch cond = %s\n",BR_Variant_Name(cond));
  }

  switch (cond) {
  case V_BR_ALWAYS:
  case V_BR_NEVER:
    Is_True(cmp == TOP_UNDEFINED, 
	    ("unexpected compare op for %s", BR_Variant_Name(cond)));
    if (cond == V_BR_ALWAYS) {
      // Unconditional branch for ALWAYS/!false_br and NEVER/false_br
      FmtAssert(!TN_is_register(targ),("targ is a register"));
      Build_OP (TOP_jr, True_TN, targ, ops);
    }
    break;
  default: {
		TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
		FmtAssert(cmp != TOP_UNDEFINED,	("Expand_Branch: unexpected comparison"));
		if (is_integer) {
			TN *gpr = Build_RCLASS_TN(ISA_REGISTER_CLASS_gpr);
			TN *fsr = Build_Dedicated_TN(ISA_REGISTER_CLASS_fsr, 1, 0);
			Build_OP (cmp, fsr, gpr, True_TN, Gen_Enum_TN(relop), src1, src2, ops);
			Expand_Copy(guard, True_TN, gpr, ops);
		} else {
			Build_OP (cmp, guard, True_TN, Gen_Enum_TN(relop), src1, src2, ops);
		}
		FmtAssert(TN_is_label(targ), ("Expand_Branch: expected a label"));
		/*
		 * For now I just trust it that it fits into 23 bits
		 */
		Build_OP (TOP_jr, guard, targ, ops);
	}
	break;
	}
}

/* ====================================================================
 *   Exp_Indirect_Branch
 * ====================================================================
 */
void 
Exp_Indirect_Branch (
  TN *targ_reg, 
  OPS *ops
)
{
  Build_OP (TOP_ja, True_TN, targ_reg, ops);
  return;
}

/* ====================================================================
 *   Exp_Local_Branch
 * ====================================================================
 */
void 
Exp_Local_Jump (
  BB *bb, 
  INT64 offset, 
  OPS *ops
)
{
#ifdef TARG_ST  
  TN *targ;
  ANNOTATION *ant;
  LABEL_IDX lab;

  FmtAssert(offset == 0, ("Offset is non null in Exp_Local_Jump"));
  /* first get a label attached to this BB */
  ant = ANNOT_First (BB_annotations(bb), ANNOT_LABEL);
  lab = ANNOT_label(ant);

  targ = Gen_Label_TN (lab, offset);
  Build_OP (TOP_jr, True_TN, targ, ops);
#else
   FmtAssert(FALSE, ("NYI: Exp_Local_Jump"));
#endif
}

/* ====================================================================
 *   Exp_Return
 * ====================================================================
 */
void 
Exp_Return (
  TN *return_address, 
  OPS *ops
)
{
  Build_OP (TOP_rts,  RA_TN, True_TN, ops);
  return;
}

/* ====================================================================
 *   Exp_Return_Interrupt
 * ====================================================================
 */
void 
Exp_Return_Interrupt (
  TN *return_address, 
  OPS *ops
)
{
  Build_OP (TOP_rte,  SP_TN, SP_TN, ops);
  return;
}

/* ====================================================================
 *   Exp_Call
 *
 *   There are three types of calls:
 *     OPR_CALL    -- direct call;
 *     OPR_ICALL   -- indirect call;
 *     OPR_PICCALL -- pic call.
 * ====================================================================
 */
void 
Exp_Call (
  OPERATOR opr, 
  TN *return_address, 
  TN *target, 
  OPS *ops
)
{
  TOP top;
  TN *br_tmp;

  if (Trace_Exp) {
    fprintf(TFile, "exp_call %s :- ", OPERATOR_name(opr));
    Print_TN (target, FALSE);
    fprintf(TFile, "\n");
  }

  FmtAssert(!TN_relocs(target),("target TN has relocation"));

  switch (opr) {
  case OPR_CALL:
    Build_OP (TOP_callr, RA_TN, True_TN, target, ops);
      break;

    case OPR_ICALL:
      if (Trace_Exp) {
	fprintf(TFile,"exp_call ICALL into ");
	Print_OP (OPS_last(ops));
      }

      FmtAssert(TN_register_class(target) == ISA_REGISTER_CLASS_gpr,("not a gpr register"));
      Build_OP(TOP_calla, RA_TN, True_TN, target, ops);
      break;

    case OPR_PICCALL:

      FmtAssert(FALSE, ("Not Implemented"));
      top = TOP_noop;
      break;

    default:
      FmtAssert(FALSE, ("unexpected opr in Exp_Call"));
      /*NOTREACHED*/
  }

  if (Trace_Exp) {
    fprintf(TFile,"exp_call into ");
    Print_OP (OPS_last(ops));
  }

  return;
}
