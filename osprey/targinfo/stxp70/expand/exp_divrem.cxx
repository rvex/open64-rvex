/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/

/* EXP routines for expanding divide and rem */

#include <signal.h>
#include "defs.h"
#include "errors.h"
#include "erglob.h"
#include "ercg.h"
#include "tracing.h"
#include "config.h"
#include "config_TARG.h"
#include "config_debug.h"
#include "mtypes.h"
#include "topcode.h"
#include "tn.h"
#include "op.h"
#include "targ_isa_lits.h"
#include "targ_trap.h"
#include "cg_flags.h"
#include "cgexp.h"

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

/* Import from exp_targ.cxx */
extern TN *Expand_Or_Inline_Immediate( OPERATION_WITH_INLINED_IMM operation, TN *src, TYPE_ID mtype, OPS *ops);

#define RESET_COND_DEF_LAST(ops) Set_OP_cond_def_kind(OPS_last(ops),OP_ALWAYS_UNC_DEF)

/* Define the exponent parameters for the various float types.
 */
enum { E32min = -126,   E32max = 127,   E32bias = 127   }; // single
enum { E64min = -1022,  E64max = 1023,  E64bias = 1023  }; // double
enum { E80min = -16382, E80max = 16383, E80bias = 16383 }; // long double
enum { E82min = -65534, E82max = 65535, E82bias = 65535 }; // register-format

/* ====================================================================
 *   Exp_Float_Divide
 * ====================================================================
 */
void
Expand_Float_Divide(
  TN *result, 
  TN *src1, 
  TN *src2, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert(FALSE, ("Not implemented"));
}

/* ====================================================================
 *   Exp_Float_Recip
 * ====================================================================
 */ 
void
Expand_Float_Recip (
  TN *result, 
  TN *src, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert(FALSE,("Not Implemented"));

  return;
}

/*****************************************************************************
 *
 * Integer division internal support routines
 *
 *****************************************************************************/

/* Return values for Check_Divide:
 */
typedef enum {
  DIVCHK_RUNTIME,	// unknown except at run-time
  DIVCHK_BYZERO,	// unconditional div-by-zero
  DIVCHK_OVERFLOW	// unconditional overflow
} DIVCHK_STATUS;

static TOP
Pick_Compare_For_Mtype (
  TYPE_ID mtype,
  BOOL imm
)
{
  switch (mtype) {
  case MTYPE_I4:
    return (imm) ? TOP_cmp_r_cmp_g: TOP_cmp_i8_cmp_g;
  case MTYPE_U4:
    return (imm) ? TOP_cmp_r_u_cmp_g: TOP_cmp_i8_u_cmp_g;
    break;
  default:
    FmtAssert(FALSE,("Pick_Compare_For_Mtype: unsupported mtype"));
  }
}

/* ====================================================================
 *   Check_Divide
 * 
 *   Check a divide for undefined operations. The operands are examined
 *   to determine if it is known at compile-time that a fault will
 *   occur or if it cannot be known until run-time. The return value
 *   indicates the situation. In addition, if divide checking is
 *   enabled, code is generated to cause an exception.
 * ====================================================================
 */ 
static DIVCHK_STATUS
Check_Divide (
  TN *numerator, 
  TN *divisor, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  // TODO: don't want to generate checks while using simulator, so reset
  // div-by-zero checking which is on by default.
  BOOL div_zero_check = FALSE; // DEBUG_Div_Zero_Check

  INT64	divisor_val;
  BOOL const_divisor = TN_Value_At_Op (divisor, NULL, &divisor_val);

  /* Check for divide-by-zero.
   */
  if (const_divisor) {
    if (divisor_val == 0) {
      if (div_zero_check) {
	TOP top = TOP_UNDEFINED;
	if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, TOP_trap)) {
	  top = TOP_trap;
	}
	Build_OP (top, Gen_Literal_TN(FPE_INTDIV_trap, 1), ops);
      }
      return DIVCHK_BYZERO;
    }
  } 
  else if (div_zero_check) {
    // Generate a compare and a trap:
    TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
    TOP opc = Pick_Compare_For_Mtype (mtype, FALSE);
#if 0
    // Cannot do it now because trap is not predicated, so ...
    Build_OP (opc, guard, Gen_Enum_TN(ISA_ECV_cmp_eq), True_TN, divisor, Gen_Literal_TN (0, MTYPE_byte_size(mtype)), ops);
    Build_OP (TOP_trap, guard, Gen_Literal_TN(FPE_INTDIV_trap, 1), ops);
#else
    FmtAssert(FALSE,("Cannot perform div zero check"));
#endif
  }

  /* Check for overflow.
   */
  if (MTYPE_is_signed(mtype)) {
    INT64 numer_val;
    BOOL const_numer = TN_Value_At_Op (numerator, NULL, &numer_val);
    const INT64 minint_val = MTYPE_size_min (mtype);
    const INT min_tn_size = MTYPE_byte_size (mtype);
    if (const_divisor && const_numer) {
      if (numer_val == minint_val && divisor_val == -1) {
	if (DEBUG_Div_Oflow_Check) {
	  TOP top = TOP_UNDEFINED;
	  if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, TOP_trap)) {
	    top = TOP_trap;
	  }
	  Build_OP (top, Gen_Literal_TN(FPE_INTOVF_trap, 1), ops);
	}
	return DIVCHK_OVERFLOW;
      }
    } 
    else if (DEBUG_Div_Oflow_Check) {

      /* Generate code to test for most-negative-integer divided by -1
       */
      TOP opc;
      TN *p1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
      if (const_numer) {
	opc = Pick_Compare_For_Mtype (mtype, FALSE);
	Build_OP (opc, p1, True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), Gen_Literal_TN(-1, 4), divisor, ops);
      } 
      else if (const_divisor) {
	TN *minint = Build_TN_Of_Mtype (mtype);
	Expand_Immediate (minint, Gen_Literal_TN(minint_val, min_tn_size), 
			  TRUE, ops);
	opc = Pick_Compare_For_Mtype (mtype, TRUE);
	Build_OP (opc, p1, True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), numerator, minint, ops);
      } 
#if 0
      else {
	//	opc = is_double ? TOP_noop : TOP_noop;
	Build_OP (opc, p1, True_TN, Gen_Literal_TN(-1, 4), divisor, ops);

	TN *minint = Build_TN_Of_Mtype (mtype);
	Expand_Immediate (minint, Gen_Literal_TN(minint_val, min_tn_size), 
			  TRUE, ops);
	//	opc = is_double ? TOP_noop : TOP_noop;
	Build_OP (opc, p1, True_TN, numerator, minint, ops);
      }
      Build_OP (TOP_GP32_TRAP_GT_U4, p1, 
                               Gen_Literal_TN(FPE_INTOVF_trap, 1), ops);
#else
      FmtAssert(FALSE,("Check_Divide: no can do I said"));
#endif
    }
  }

  return DIVCHK_RUNTIME;
}

/* ====================================================================
 *   Is_Power_OF_2
 *
 *   return TRUE if the val is a power of 2
 * ====================================================================
 */
#define IS_POWER_OF_2(val)	((val != 0) && ((val & (val-1)) == 0))

static BOOL 
Is_Power_Of_2 (
  INT64 val, 
  TYPE_ID mtype
)
{
  if (MTYPE_is_signed(mtype) && val < 0) val = -val;

  if (mtype == MTYPE_U4) val &= 0xffffffffull;

  return IS_POWER_OF_2(val);
}

/* ====================================================================
 *   Get_Power_OF_2
 * ====================================================================
 */
static INT
Get_Power_Of_2 (
  INT64 val, 
  TYPE_ID mtype
)
{
  INT i;
  INT64 pow2mask;

  if (MTYPE_is_signed(mtype) && val < 0) val = -val;

  if (mtype == MTYPE_U4) val &= 0xffffffffull;

  pow2mask = 1;
  for ( i = 0; i < MTYPE_size_reg(mtype); ++i ) {
    if (val == pow2mask) return i;
    pow2mask <<= 1;
  }

  FmtAssert(FALSE, ("Get_Power_Of_2 unexpected value"));
  /* NOTREACHED */
}

/* ====================================================================
 * Expand the sequence for division and/or remainder by a variable.
 * ====================================================================
 */
static void
Expand_NonConst_DivRem (TN *quot, TN *rem, TN *x, TN *y, TYPE_ID mtype, OPS *ops)
{
  FmtAssert(Enable_Fpx, ("Fpx extension mandatory for div/mod instructions."));

  BOOL is_signed = MTYPE_is_signed(mtype);

  TN *fsr = Build_Dedicated_TN(ISA_REGISTER_CLASS_fsr,1,0);
  TN *res = Build_RCLASS_TN (ISA_REGISTER_CLASS_fpr);

  // Process immediate operand
//   if (TN_is_constant(x)) {
//     x = Expand_Or_Inline_Immediate
//       (INL_UNKNOWN, x, ISA_REGISTER_CLASS_gpr, ops);
//   }
  if (TN_is_constant(y)) {
    y = Expand_Or_Inline_Immediate
      (INL_UNKNOWN, y, MTYPE_I4, ops);
  }

  x = check_opd_rclass( x, ISA_REGISTER_CLASS_gpr, ops);
  y = check_opd_rclass( y, ISA_REGISTER_CLASS_gpr, ops);

  if (quot) {
    TOP divop = is_signed ? TOP_fpx_div : TOP_fpx_divu ;
    Build_OP(divop, fsr, res, True_TN, x, y, ops) ;
    Expand_Copy(quot,True_TN,res,ops);
  } else if (rem) {
    TOP remop = is_signed ? TOP_fpx_mod : TOP_fpx_modu ;
    Build_OP(remop, fsr, res, True_TN, x, y, ops) ;
    Expand_Copy(rem, True_TN, res, ops);
  } else {
    FmtAssert(FALSE, ("Expand_NonConst_DivRem unexpected expansion"));
  }
}

/* ====================================================================
 *   Expand_Divide_By_Constant
 * ====================================================================
 */
static BOOL
Expand_Divide_By_Constant (
  TN *result, 
  TN *src1,
  TN *src2,
  INT64 src2_val,
  TYPE_ID mtype, 
  OPS *ops
)
{
  BOOL is_signed = MTYPE_is_signed(mtype);
  FmtAssert(mtype == MTYPE_I4 || mtype == MTYPE_U4, ("Expand_Divide_By_Constant: mtype not handled"));

  /* Handle the trivial ones:
   */
  if (src2_val == 1) {
    Exp_COPY(result, src1, ops);
    return TRUE;
  } else if (is_signed && src2_val == -1) {
    Expand_Neg(result, src1, mtype, ops);
    return TRUE;
  }

  /* Look for simple shift optimizations:
   */
  if (Is_Power_Of_2(src2_val, mtype)) {
        TN *numer = src1;
        INT64 dvsr = src2_val;
        INT pow2 = Get_Power_Of_2(src2_val, mtype);

        if (MTYPE_is_unsigned(mtype)) {
            TN *tmp = Gen_Literal_TN(pow2, MTYPE_byte_size(mtype));
            TN *tmp2= NULL;
            tmp2 = Expand_Or_Inline_Immediate(INL_SHIFT, tmp, mtype, ops);
            if (tmp2 == tmp) {
                // Fit in immediate
                Build_OP(TOP_shru_i5, result, True_TN, src1, tmp2, ops);
            } else {
                Build_OP(TOP_shru_r, result, True_TN, src1, tmp2, ops);
            }
        } else {
            // When fpx is activated, better to use it 
            if (Enable_Fpx) return FALSE;
            INT64 absdvsr = abs(dvsr);
            TN *tmp_res = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
            TN *tmp = Gen_Literal_TN(pow2, MTYPE_byte_size(mtype));
            TN *tmp2= NULL;
            TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
            TN *tmp3=Gen_Literal_TN(((int)absdvsr-1),4);
            TN *tmp4=Expand_Or_Inline_Immediate(INL_SHIFT, tmp3, mtype, ops);
            tmp2 = Expand_Or_Inline_Immediate(INL_SHIFT, tmp, mtype, ops);
            Build_OP (TOP_or_i8, tmp_res, True_TN, src1, Gen_Literal_TN(0,4), ops);                                   // or      tmp_res, src1, 0
            Build_OP(TOP_cmp_i8_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), src1, Gen_Literal_TN(0, 4), ops); // cmplt   guard, src1, 0
            if (tmp3 == tmp4) {
              Build_OP (TOP_addu_i8, tmp_res, guard, src1, tmp3, ops);          
              Set_OP_carryisignored (OPS_last(ops));
           // addu    tmp_res, tmp_res, absdvsr-1
            } else {
                Build_OP (TOP_addu_r, tmp_res, guard, src1, tmp4, ops);
                Set_OP_carryisignored (OPS_last(ops));
                  // addu    tmp_res, tmp_res, absdvsr-1
            }
            if (tmp2 == tmp) {
                // Fit in immediate
                Build_OP(TOP_shr_i5, tmp_res, True_TN, tmp_res, tmp2, ops);
            } else {
                Build_OP(TOP_shr_r, tmp_res, True_TN, tmp_res, tmp2, ops);
            }
            if (dvsr < 0) {
                // must negate the tmp_res
                Build_OP(TOP_negu, tmp_res, True_TN, tmp_res, ops);
            }
			Build_OP (TOP_or_i8,  result, True_TN, tmp_res, Gen_Literal_TN(0,4), ops);       // or      tmp1, src1, 0

        }
        return TRUE;
    } else {
      // When fpx is activated, better to use it 
      if (Enable_Fpx) return FALSE;
      //TDR - Code for div by 3 5 6 7 9 10 11 12 13
      if ((mtype == MTYPE_I4 || mtype == MTYPE_U4) && src2_val > 0 && src2_val <= 13) {
          TN *tmp_res = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
          TN *tmp1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
          TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
          TN *tmp3;
          TN *tmp4;
          if (mtype == MTYPE_I4) {
              TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
              Build_OP (TOP_or_i8,  tmp1, True_TN, src1, Gen_Literal_TN(0,4), ops);                                     // or      tmp1, src1, 0
              Build_OP(TOP_cmp_i8_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), src1, Gen_Literal_TN(0, 4), ops); // cmplt   guard, tmp1, 0 
              Build_OP (TOP_addu_i8, tmp1, guard, tmp1, Gen_Literal_TN(((int)src2_val-1),4), ops);                      // addu    tmp1, tmp1, src2_val-1 
              Set_OP_carryisignored (OPS_last(ops));
          } else {
              Build_OP (TOP_or_i8,  tmp1, True_TN, src1, Gen_Literal_TN(0,4), ops);                 // or      tmp1, src1, 0
          }
          switch  (src2_val) {
              case 3:
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(2,4), ops);     // shr     tmp_res, tmp1, 2     
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr     tmp2, tmp1, 4     
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(4,4), ops);     // shr     tmp2, tmp_res, 4   
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(8,4), ops);     // shr     tmp2, tmp_res, 8   
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(16,4), ops);    // shr     tmp2, tmp_res, 16  
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(1,4), ops);    // shlu    tmp2, tmp_res, 1   
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp_res, tmp2, ops);                    // addu    tmp2, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu    tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(2,4), ops);       // shlu    tmp2, tmp1, 2   
                  Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // addu    tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(5,4), ops);       // addu    tmp1, tmp1, 5   
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr     tmp1, tmp1, 4   
                  break;
              case 5:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(1,4), ops);     // shr     tmp_res, tmp1, 1
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(2,4), ops);        // shr     tmp2, tmp1, 2   
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(4,4), ops);     // shr     tmp2, tmp_res, 4   
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(8,4), ops);     // shr     tmp2, tmp_res, 8   
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(16,4), ops);    // shr     tmp2, tmp_res, 16  
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu    tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);  // shr     tmp_res, tmp_res, 2  
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(1,4), ops);    // shlu    tmp2, tmp_res, 1 
                  Build_OP (TOP_shlu_i5, tmp3, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu    tmp3, tmp_res, 2  
                  Build_OP (TOP_addu_r, tmp3, True_TN, tmp_res, tmp3, ops);                    // addu    tmp3, tmp_res, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp3, ops);                       // subu    tmp1, tmp1, tmp3   
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(3,4), ops);       // shlu    tmp2, tmp1, 3    
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp1, ops);                       // subu    tmp2, tmp2, tmp1 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp2, Gen_Literal_TN(5,4), ops);        // shr     tmp1, tmp2, 5 
                  break;
              case 6:
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(1,4), ops);     // shr    tmp_res, tmp1, 1  
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(3,4), ops);        // shr    tmp2, tmp1, 3  
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(4,4), ops);     // shr    tmp2, tmp_res, 4  
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(8,4), ops);     // shr    tmp2, tmp_res, 8  
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(16,4), ops);    // shr    tmp2, tmp_res, 16 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);  // shr    tmp_res, tmp_res, 2  
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu   tmp2, tmp_res, 2  
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp2, Gen_Literal_TN(1,4), ops);       // shlu   tmp2, tmp2, 1  
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(2,4), ops);       // addu   tmp1, tmp1, 2  
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(3,4), ops);        // shr    tmp1, tmp1, 3  
                  break;
              case 7:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(1,4), ops);        // shr    tmp2, tmp1, 1  
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp3, tmp1, 4  
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp2, Gen_Literal_TN(6,4), ops);        // shr    tmp3, tmp2, 6  
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp2, Gen_Literal_TN(12,4), ops);       // shr    tmp3, tmp2, 12 
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp2, Gen_Literal_TN(24,4), ops);    // shr    tmp_res, tmp2, 24 
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);  // shr    tmp_res, tmp_res, 2  
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);    // shlu   tmp2, tmp_res, 3  
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2 
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(1,4), ops);       // addu   tmp1, tmp1, 1  
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(3,4), ops);        // shr    tmp1, tmp1, 3  
                  break;
              case 9:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  tmp4 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp1, Gen_Literal_TN(1,4), ops);        // shr    tmp3, tmp1, 1 
                  Build_OP (TOP_shr_i5, tmp4, True_TN, tmp1, Gen_Literal_TN(2,4), ops);        // shr    tmp4, tmp1, 2 
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(3,4), ops);        // shr    tmp2, tmp1, 3 
                  Build_OP (TOP_addu_r, tmp3, True_TN, tmp3, tmp4, ops);                       // addu   tmp3, tmp3, tmp4
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp2, Gen_Literal_TN(6,4), ops);        // shr    tmp3, tmp2, 6 
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp2, Gen_Literal_TN(12,4), ops);       // shr    tmp3, tmp2, 12
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp2, Gen_Literal_TN(24,4), ops);    // shr    tmp_res, tmp2, 24
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);  // shr    tmp_res, tmp_res, 3 
                  Build_OP (TOP_shlu_i5,tmp2, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);     // shlu   tmp2, tmp_res, 3
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp_res, tmp2, ops);                    // addu   tmp2, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8,tmp1, True_TN, tmp1, Gen_Literal_TN(7,4), ops);        // addu   tmp1, tmp1, 7
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp1, tmp1, 4 
                break;
              case 10:
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(1,4), ops);     // shr    tmp_res, tmp1, 1 
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(2,4), ops);        // shr    tmp2, tmp1, 2 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(4,4), ops);     // shr    tmp2, tmp_res, 4 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(8,4), ops);     // shr    tmp2, tmp_res, 8 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(16,4), ops);    // shr    tmp2, tmp_res, 16
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);  // shr    tmp_res, tmp_res, 3 
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu   tmp2, tmp_res, 2
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp_res, tmp2, ops);                    // addu   tmp2, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp2, Gen_Literal_TN(1,4), ops);       // shlu   tmp2, tmp2, 1
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(6,4), ops);       // addu   tmp1, tmp1, 6
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp1, tmp1, 4 
                  break;
              case 11:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(1,4), ops);        // shr    tmp2, tmp1, 1 
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp1, Gen_Literal_TN(2,4), ops);        // shr    tmp3, tmp1, 2 
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp1, Gen_Literal_TN(5,4), ops);        // shr    tmp3, tmp1, 5 
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(7,4), ops);     // shr    tmp_res, tmp1, 7 
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // subu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(10,4), ops);    // shr    tmp2, tmp_res, 10
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(20,4), ops);    // shr    tmp2, tmp_res, 20
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);  // shr    tmp_res, tmp_res, 3 
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu   tmp2, tmp_res, 2
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp2, Gen_Literal_TN(2,4), ops);       // shlu   tmp2, tmp2, 2
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(5,4), ops);       // addu   tmp1, tmp1, 5
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp1, tmp1, 4 
                  break;
              case 12:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(1,4), ops);     // shr    tmp_res, tmp1, 1 
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(3,4), ops);        // shr    tmp2, tmp1, 3 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(4,4), ops);     // shr    tmp2, tmp_res, 4 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(8,4), ops);     // shr    tmp2, tmp_res, 8 
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(16,4), ops);    // shr    tmp2, tmp_res, 16
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);  // shr    tmp_res, tmp_res, 3 
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu   tmp2, tmp_res, 2
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp2, Gen_Literal_TN(2,4), ops);       // shlu   tmp2, tmp2, 2
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);       // addu   tmp1, tmp1, 4
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp1, tmp1, 4 
                  break;
              case 13:
                  tmp3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp1, Gen_Literal_TN(1,4), ops);        // shr    tmp3, tmp1, 1 
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp1, Gen_Literal_TN(4,4), ops);     // shr    tmp_res, tmp1, 4 
                  Build_OP (TOP_addu_r, tmp3, True_TN, tmp3, tmp_res, ops);                    // addu   tmp3, tmp3, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp3, Gen_Literal_TN(4,4), ops);     // shr    tmp_res, tmp3, 4 
                  Build_OP (TOP_shr_i5, tmp2, True_TN, tmp3, Gen_Literal_TN(5,4), ops);        // shr    tmp2, tmp3, 5 
                  Build_OP (TOP_addu_r, tmp3, True_TN, tmp3, tmp_res, ops);                    // addu   tmp3, tmp3, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp3, True_TN, tmp2, Gen_Literal_TN(12,4), ops);       // shr    tmp3, tmp2, 12
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp2, Gen_Literal_TN(24,4), ops);    // shr    tmp_res, tmp2, 24
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp2, tmp3, ops);                       // addu   tmp2, tmp2, tmp3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_r, tmp_res, True_TN, tmp_res, tmp2, ops);                 // addu   tmp_res, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp_res, True_TN, tmp_res, Gen_Literal_TN(3,4), ops);  // shr    tmp_res, tmp_res, 3 
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp_res, Gen_Literal_TN(2,4), ops);    // shlu   tmp2, tmp_res, 2
                  Build_OP (TOP_subu_r, tmp2, True_TN, tmp2, tmp_res, ops);                    // subu   tmp2, tmp2, tmp_res
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp2, Gen_Literal_TN(2,4), ops);       // shlu   tmp2, tmp2, 2
                  Build_OP (TOP_addu_r, tmp2, True_TN, tmp_res, tmp2, ops);                    // addu   tmp2, tmp_res, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_subu_r, tmp1, True_TN, tmp1, tmp2, ops);                       // subu   tmp1, tmp1, tmp2
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_addu_i8, tmp1, True_TN, tmp1, Gen_Literal_TN(3,4), ops);       // addu   tmp1, tmp1, 3
                  Set_OP_carryisignored (OPS_last(ops));
                  Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(4,4), ops);        // shr    tmp1, tmp1, 4
                  break;
              default:
                  FmtAssert(FALSE, ("Expand_Divide_By_Constant unexpected expansion"));
          }
          Build_OP (TOP_addu_r, result, True_TN, tmp_res, tmp1, ops);                     // addu    result, tmp_res, tmp1
          Set_OP_carryisignored (OPS_last(ops));
          return TRUE;
      }
  }
  return FALSE;
}

/* ====================================================================
 *   Expand_Divide
 *
 *   Should be called only if Lai_Code = TRUE or 
 *   OPT_inline_divide = TRUE.
 * ====================================================================
 */
TN *
Expand_Divide (
  TN *result, 
  TN *src1, 
  TN *src2, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert (MTYPE_is_integral(mtype) && MTYPE_byte_size(mtype) == 4,
	     ("Unexpected MTYPE: %s", MTYPE_name(mtype)));

  /* Check for undefined operations we can detect at compile-time
   * and when enabled, generate run-time checks.
   */
  switch (Check_Divide(src1, src2, mtype, ops)) {
  case DIVCHK_BYZERO:
  case DIVCHK_OVERFLOW:
//    Build_OP(TOP_ifixup, result, ops);
    OPS_Append_Op(ops, Mk_VarOP(TOP_ifixup,1,0,&result,NULL));
    return NULL;
  }

  /* Look for simple shift optimizations and multiply_hi optimizations:
   */
  INT64 src2_val;
  BOOL const_src2 = TN_Value_At_Op (src2, NULL, &src2_val);

  if (const_src2) {
    if (Expand_Divide_By_Constant (result, src1, src2, src2_val, mtype, ops)) {
      return NULL;
    }
  }

  Expand_NonConst_DivRem(result, NULL, src1, src2, mtype, ops);
  return NULL;
}

/* ====================================================================
 *   Expand_Power_Of_2_Rem
 *
 *   Expand the sequence for remainder of a power of two. It is the
 *   caller's job to verify that divisor is a non-zero power of two.
 *
 *   Expand rem(x, [+-]2^n) as follows:
 *
 *	Using the identities
 *		rem(x,y) =  rem( x,-y)
 *		rem(x,y) = -rem(-x, y)
 *
 *	unsigned
 *	f=	x & MASK(n)
 *
 *	signed
 *	f=	x & MASK(n)		x>=0
 *	f=	-(-x & MASK(n))		x<0
 * ====================================================================
 */
static void 
Expand_Power_Of_2_Rem (
  TN *result, 
  TN *src1, 
  INT64 src2_val, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  Is_True(MTYPE_is_class_integer(mtype),("wrong mtype"));
  Is_True(MTYPE_byte_size(mtype) <= 4,("longlong ?"));

  INT n = Get_Power_Of_2(src2_val, mtype);
  INT64 nMask = (1LL << n) - 1;
  TN *con = Gen_Literal_TN(nMask, 4);

  if (MTYPE_signed(mtype)) {
    TN *tmp1, *tmp2, *tmp3, *tmp4;
    TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr);
    Build_OP(TOP_cmp_i8_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), src1, Gen_Literal_TN(0, 4), ops);

    //
    // Get absolute value of src1
    //
    tmp1 = Build_TN_Of_Mtype(mtype);
    tmp2 = Build_TN_Of_Mtype(mtype);
    Build_OP(TOP_negu, tmp1, True_TN, src1, ops);
    Expand_Select(tmp2, guard, tmp1, src1,MTYPE_byte_size(mtype),FALSE, ops);

    //
    // Perform the AND
    //
    tmp3 = Build_TN_Of_Mtype(mtype);
    Expand_Binary_And(tmp3, tmp2, con, mtype, ops);

    //
    // Negate the result if src1 was negative
    //
    tmp4 = Build_TN_Of_Mtype(mtype);
    Build_OP(TOP_negu, tmp4, True_TN, tmp3, ops);
    Expand_Select(result, guard, tmp4, tmp3,MTYPE_byte_size(mtype),FALSE, ops);
  } else {
    Expand_Binary_And(result, src1, con, mtype, ops);
  }
}

/* ====================================================================
 *   Expand_Small_Values_Rem
 *
 *   Expand the sequence for remainder when dividing by 3, 5 or 7
 * ====================================================================
 */
static void 
Expand_Small_Values_Rem (
  TN *result, 
  TN *src1, 
  INT64 src2_val, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  Is_True(MTYPE_is_class_integer(mtype),("wrong mtype"));
  Is_True(MTYPE_byte_size(mtype) <= 4,("longlong ?"));

  FmtAssert ((src2_val == 3) || (src2_val == 5) || (src2_val == 7),
	     ("Unexpected calculation of remainder - divider should be 3, 5 or 7"));

  TN *tmp0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *tmp1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  
  switch  (src2_val) {
  case 3:
    // or 	tmp0, src1, 0 
    Build_OP (TOP_or_i8,  tmp0, True_TN, src1, Gen_Literal_TN(0,4), ops);
    // shlu 	tmp1, tmp0, 2
    Build_OP (TOP_shlu_i5, tmp1, True_TN, tmp0, Gen_Literal_TN(2,4), ops);
    // addu 	tmp1, tmp0, tmp1
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp0, tmp1, ops);
    // shlu 	tmp2, tmp1, 4
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(4,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shlu 	tmp2, tmp1, 8
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(8,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shlu 	tmp2, tmp1, 16
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(16,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shru 	tmp2, tmp0, 1  
    Build_OP (TOP_shru_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(1,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shru 	tmp0, tmp0, 3
    Build_OP (TOP_shru_i5, tmp0, True_TN, tmp0, Gen_Literal_TN(3,4), ops);
    // subu 	tmp0, tmp1, tmp0
    Build_OP (TOP_subu_r, tmp0, True_TN, tmp1, tmp0, ops);
    // shru 	result, tmp0, 30
    Build_OP (TOP_shru_i5, result, True_TN, tmp0, Gen_Literal_TN(30,4), ops);
    break;
  case 5:
    // or 	tmp0, src1, 0 
    Build_OP (TOP_or_i8,  tmp0, True_TN, src1, Gen_Literal_TN(0,4), ops);
    // shlu 	tmp1, tmp0, 1 
    Build_OP (TOP_shlu_i5, tmp1, True_TN, tmp0, Gen_Literal_TN(1,4), ops);
    // addu 	tmp1, tmp0, tmp1
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp0, tmp1, ops);
    // shlu 	tmp2, tmp1, 4 
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(4,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shlu 	tmp2, tmp1, 8 
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(8,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shlu 	tmp2, tmp1, 16
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp1, Gen_Literal_TN(16,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shru 	tmp2, tmp0, 3 
    Build_OP (TOP_shru_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(3,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shru 	tmp1, tmp1, 29
    Build_OP (TOP_shru_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(29,4), ops);
    // make 	tmp0, 1091  
    Build_OP (TOP_make, tmp0, True_TN, Gen_Literal_TN(1091,4), ops);
    // shlu 	tmp1, tmp1, 2 
    Build_OP (TOP_shlu_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(2,4), ops);
    // more 	tmp0, 8720  
    Build_OP (TOP_more, tmp0, True_TN, tmp0, Gen_Literal_TN(8720,4), ops);
    // shr 	tmp0, tmp0, tmp1 
    Build_OP (TOP_shr_r, tmp0, True_TN, tmp0, tmp1, ops);
    // and 	result, tmp0, 7  
    Build_OP(TOP_and_i8, result, True_TN, tmp0, Gen_Literal_TN(7,4), ops);
    break;
  case 7:
    // or 	tmp0, src1, 0 
    Build_OP (TOP_or_i8,  tmp0, True_TN, src1, Gen_Literal_TN(0,4), ops);
    // shru 	tmp2, tmp0, 4 
    Build_OP (TOP_shru_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(4,4), ops);
    // shru 	tmp1, tmp0, 1 
    Build_OP (TOP_shru_i5, tmp1, True_TN, tmp0, Gen_Literal_TN(1,4), ops);
    // addu 	tmp1, tmp1, tmp2
    Build_OP (TOP_addu_r, tmp1, True_TN, tmp1, tmp2, ops);
    // shlu 	tmp2, tmp0, 5 
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(5,4), ops);
    // shlu 	tmp0, tmp0, 2 
    Build_OP (TOP_shlu_i5, tmp0, True_TN, tmp0, Gen_Literal_TN(2,4), ops);
    // addu 	tmp0, tmp0, tmp2
    Build_OP (TOP_addu_r, tmp0, True_TN, tmp0, tmp2, ops);
    // shlu 	tmp2, tmp0, 6 
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(6,4), ops);
    // addu 	tmp0, tmp0, tmp2
    Build_OP (TOP_addu_r, tmp0, True_TN, tmp0, tmp2, ops);
    // shlu 	tmp2, tmp0, 12
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(12,4), ops);
    // addu 	tmp0, tmp0, tmp2
    Build_OP (TOP_addu_r, tmp0, True_TN, tmp0, tmp2, ops);
    // shlu 	tmp2, tmp0, 24
    Build_OP (TOP_shlu_i5, tmp2, True_TN, tmp0, Gen_Literal_TN(24,4), ops);
    // addu 	tmp0, tmp0, tmp2
    Build_OP (TOP_addu_r, tmp0, True_TN, tmp0, tmp2, ops);
    // addu 	tmp0, tmp1, tmp0
    Build_OP (TOP_addu_r, tmp0, True_TN, tmp1, tmp0, ops);
    // shru 	tmp0, tmp0, 29
    Build_OP (TOP_shru_i5, tmp0, True_TN, tmp0, Gen_Literal_TN(29,4), ops);
    // subu 	tmp1, tmp0, 7 
    Build_OP (TOP_subu_i8, tmp1, True_TN, tmp0, Gen_Literal_TN(7,4), ops);
    // shr 	tmp1, tmp1, 31 
    Build_OP (TOP_shr_i5, tmp1, True_TN, tmp1, Gen_Literal_TN(31,4), ops);
    // and 	result, tmp0, tmp1 
    Build_OP(TOP_and_r, result, True_TN, tmp0, tmp1, ops);
    break;
  default:
    FmtAssert(FALSE, ("Expand_Small_Values_Rem unexpected expansion"));
  }
}

/* ====================================================================
 *   Expand_Rem
 * ====================================================================
 */
void
Expand_Rem (
  TN *result, 
  TN *src1, 
  TN *src2, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert (MTYPE_is_integral(mtype) && MTYPE_byte_size(mtype) == 4,
	     ("Unexpected MTYPE: %s", MTYPE_name(mtype)));

  //
  // Check for undefined operations we can detect at compile-time
  // and when enabled, generate run-time checks.
  //
  switch (Check_Divide(src1, src2, mtype, ops)) {
  case DIVCHK_BYZERO:
  case DIVCHK_OVERFLOW:
//    Build_OP(TOP_ifixup, result, ops);
    OPS_Append_Op(ops, Mk_VarOP(TOP_ifixup,1,0,&result,NULL));
   return;
  }

  //
  // Try to optimize when constant divisor.
  //
  INT64 src2_val;
  BOOL const_src2 = TN_Value_At_Op (src2, NULL, &src2_val);
  BOOL is_signed = MTYPE_is_signed(mtype);


  if (const_src2) {

    // Handle trivial cases
    if (src2_val == 1 ||
	(is_signed && src2_val == -1)) {
      Expand_Immediate (result, Gen_Literal_TN(0, MTYPE_byte_size(mtype)),
			mtype, ops);
      return;
    }


    //
    // Handle powers of 2 specially.
    //
    if (Is_Power_Of_2(src2_val, mtype)) {
      Expand_Power_Of_2_Rem(result, src1, src2_val, mtype, ops);
      return;
    }

    // Handle efficient remainder when dividing by 3, 5 or 7
    if (!is_signed &&((src2_val == 3) || (src2_val == 5) || (src2_val == 7))) {
      Expand_Small_Values_Rem(result, src1, src2_val, mtype, ops);
      return;
    }

    // Handle remainders when divisions are generated in terms of multiplications
    if (CGEXP_cvrt_int_div_to_mult && Enable_Mx) {
      TN *div_tn = Build_TN_Like (result);

      if (Expand_Divide_By_Constant(div_tn, src1, src2,
				    src2_val, mtype, ops)) {
	TN *mult_tn;

	/* Generate a multiply:
	 */
	mult_tn = Build_TN_Like (result);
	Expand_Multiply(mult_tn, mtype, div_tn, mtype, src2, mtype, ops);

	/* Subtract the result of the multiply from the original value.
	 */
	Build_OP(TOP_subu_r, result, True_TN, src1, mult_tn, ops);
        Set_OP_carryisignored (OPS_last(ops));
	return;
      }
    }
  }

  Expand_NonConst_DivRem(NULL, result, src1, src2, mtype, ops);
}

/* ====================================================================
 *   Expand_Mod
 *
 *	Expand mod(x,y) as follows:
 *		t1=	rem(x,y)
 *		t2=	xor(t1,y)
 *		t3,t4=	cmp.lt(t2,0)
 *	  if t3 r=	t1+y
 *	  if t4 r=	t1
 *
 * ====================================================================
 */
void 
Expand_Mod (
  TN *result, 
  TN *src1, 
  TN *src2, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
#if 0
  TN *tmp1;
  TN *tmp2;
  TN *p1;
  TN *p2;
  TOP opc;
  BOOL is_double = MTYPE_is_size_double(mtype);

  FmtAssert(FALSE,("Not Implemented"));

  /* Check for undefined operations we can detect at compile-time
   * and when enabled, generate run-time checks.
   */
  switch (Check_Divide(src1, src2, mtype, ops)) {
  case DIVCHK_BYZERO:
  case DIVCHK_OVERFLOW:
//    Build_OP(TOP_ifixup, result, ops);
    OPS_Append_Op(ops, Mk_VarOP(TOP_ifixup,1,0,&result,NULL));
    return;
  }

  /* Handle mod by power of 2 specially
   */
  INT64 src2_val;
  BOOL const_src2 = TN_Value_At_Op (src2, NULL, &src2_val);
  if (const_src2 && Is_Power_Of_2(src2_val, mtype)) {

    return;
  }
#if 0
  /* Calculate remainder 
   */
  tmp1 = Build_TN_Like(result);
  Expand_Rem(tmp1, src1, src2, mtype, ops);

  /* Are signs different? 
   */
  tmp2 = Build_TN_Like(result);
  Build_OP(TOP_noop, tmp2, True_TN, tmp1, src2, ops);

  p1 = Build_RCLASS_TN(ISA_REGISTER_CLASS_guard);
  p2 = Build_RCLASS_TN(ISA_REGISTER_CLASS_guard);
  opc = is_double ? TOP_noop : TOP_noop;
  Build_OP(opc, p1, p2, True_TN, tmp2, Zero_TN, ops);

  /* result = divisor + remainder if p1
   * result = remainder if p2
   */
  Build_OP(TOP_noop, result, p1, src2, tmp1, ops);
  Build_OP(TOP_noop, result, p2, tmp1, ops);
#endif
#endif
}

/* ====================================================================
 *   Expand_DivRem
 * ====================================================================
 */
void 
Expand_DivRem (
  TN *result, 
  TN *result2, 
  TN *src1, 
  TN *src2, 
  TYPE_ID mtype, 
  OPS *ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
#if 0
  FmtAssert(FALSE,("Not Implemented"));

  /* Check for undefined operations we can detect at compile-time
   * and when enabled, generate run-time checks.
   */
  switch (Check_Divide(src1, src2, mtype, ops)) {
  case DIVCHK_BYZERO:
  case DIVCHK_OVERFLOW:
//    Build_OP(TOP_ifixup, result, ops);
//    Build_OP(TOP_ifixup, result2, ops);
    OPS_Append_Op(ops, Mk_VarOP(TOP_ifixup,1,0,&result,NULL));
    OPS_Append_Op(ops, Mk_VarOP(TOP_ifixup,1,0,&result2,NULL));
    return;
  }

  /* Usually we expect whirl operators to be folded where possible.
   * But divrem is an odd beast in that the result is a special
   * "handle" rather than a value. There is no way to produce constants.
   * Therefore in some odd instances we can get constant operands,
   * so fold them here, avoiding nasty trapping issues.
   */
  INT64 src1_val;
  BOOL const_src1 = TN_Value_At_Op (src1, NULL, &src1_val);
  INT64 src2_val;
  BOOL const_src2 = TN_Value_At_Op (src2, NULL, &src2_val);
  if (const_src1 && const_src2) {
    INT64 quot_val, rem_val;
    switch (mtype) {
    case MTYPE_I8:
      quot_val = (INT64)src1_val / (INT64)src2_val;
      rem_val = (INT64)src1_val % (INT64)src2_val;
      break;
    case MTYPE_U8:
      quot_val = (UINT64)src1_val / (UINT64)src2_val;
      rem_val = (UINT64)src1_val % (UINT64)src2_val;
      break;
    case MTYPE_U4:
      quot_val = (UINT32)src1_val / (UINT32)src2_val;
      rem_val = (UINT32)src1_val % (UINT32)src2_val;
      break;
    case MTYPE_I4:
      quot_val = (INT32)src1_val / (INT32)src2_val;
      rem_val = (INT32)src1_val % (INT32)src2_val;
      break;
    }
    BOOL is_signed = MTYPE_is_signed(mtype);
    INT tn_size = MTYPE_is_size_double(mtype) ? 8 : 4;
    Exp_Immediate(result, Gen_Literal_TN(quot_val, tn_size), is_signed, ops);
    Exp_Immediate(result2, Gen_Literal_TN(rem_val, tn_size), is_signed, ops);
    return;
  }

  /* Look for simple shift optimizations and multiply_hi optimizations:
   */
  if (const_src2) {
    ;
  }

#endif
  return;
}
