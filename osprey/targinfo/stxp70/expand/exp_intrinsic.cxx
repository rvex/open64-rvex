/*
  Copyright (C) 2002-2009, STMicroelectronics, All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.
*/

#include <map>

#include <ctype.h>
#include "defs.h"
#include "config.h"
#include "erglob.h"
#include "ercg.h"
#include "glob.h"
#include "tracing.h"
#include "util.h"

#include "symtab.h"
#include "opcode.h"
#include "intrn_info.h"
#include "const.h" /* needed to manipulate target/host consts */
#include "targ_const.h" /* needed to manipulate target/host consts */
#include "cgir.h"
#include "whirl2ops.h"

#include "label_util.h"

#include "topcode.h"
#include "targ_isa_lits.h"
#include "targ_isa_properties.h"
#include "cgexp.h"
#include "config_TARG.h"

#include "dyn_isa_api.h"
#include "lai_loader_api.h"
#include "exp_extension.h"

#include "insn-config.h" /* for MAX_RECOG_OPERANDS */

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

static void
Expand_Unimplemented_Intrinsic (TOP opcode,
				OPS *ops)
{
  Build_OP ( TOP_trap, Gen_Literal_TN(0, 4), ops) ;
}

/*
 * Expansion of KILL intrinsic into simulated TOP_KILL
*/
static void
Expand__KILL(
 TN** o0,
 INT num_results,
 OPS* ops
)
{
  OPS_Append_Op(ops, Mk_VarOP(TOP_KILL, num_results, 0, o0, NULL));
} /* Expand__KILL */

/*
 * Expansion of builtin__divuw based on validated and scheduled basic assembly source.
*/
static void
Expand__builtin__divuw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__builtin__divuw */

/*
@@@  case INTRN_BUILTIN__DIVUW:
@@@    Expand__builtin__divuw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of builtin__divw based on validated and scheduled basic assembly source.
*/
static void
Expand__builtin__divw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__builtin__divw */

/*
@@@  case INTRN_BUILTIN__DIVW:
@@@    Expand__builtin__divw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of builtin__moduw based on validated and scheduled basic assembly source.
*/
static void
Expand__builtin__moduw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__builtin__moduw */

/*
@@@  case INTRN_BUILTIN__MODUW:
@@@    Expand__builtin__moduw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of builtin__modw based on validated and scheduled basic assembly source.
*/
static void
Expand__builtin__modw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__builtin__modw */

/*
@@@  case INTRN_BUILTIN__MODW:
@@@    Expand__builtin__modw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of absch based on validated and scheduled basic assembly source.
*/
static void
Expand__absch(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__absch */

/*
@@@  case INTRN_ABSCH:
@@@    Expand__absch(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of abscl based on validated and scheduled basic assembly source.
*/
static void
Expand__abscl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__abscl */

/*
@@@  case INTRN_ABSCL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__abscl(result[1],result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__abscl(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of abscw based on validated and scheduled basic assembly source.
*/
static void
Expand__abscw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__abscw */

/*
@@@  case INTRN_ABSCW:
@@@    Expand__abscw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of absh based on validated and scheduled basic assembly source.
*/
static void
Expand__absh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__absh */

/*
@@@  case INTRN_ABSH:
@@@    Expand__absh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of absl based on validated and scheduled basic assembly source.
*/
static void
Expand__absl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  TN *zero = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_il0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_ih0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_il1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_ih1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *cond = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;

  
  Build_OP (    TOP_addu_i8,      ol0,      True_TN, il0, Gen_Literal_TN(0,4), ops);
  Set_OP_carryisignored (OPS_last(ops));

  Build_OP (    TOP_addu_i8,      oh0,      True_TN, ih0, Gen_Literal_TN(0,4), ops);
  Set_OP_carryisignored (OPS_last(ops));
  Build_OP (    TOP_cmp_i8_cmp_g,     guard,    True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), ih0, Gen_Literal_TN(0,4), ops);
  Build_OP (	TOP_not,	n_il0,   guard, il0,	ops) ;
  Build_OP (	TOP_not,	n_ih0,   guard, ih0,	ops) ;
  Build_OP (    TOP_MAKE,       zero,   guard, Gen_Literal_TN(0,2), ops);

  Build_OP (	TOP_addu_i8,	ol0,   guard, n_il0,	Gen_Literal_TN(1,4), ops) ;
  Build_OP (	TOP_addcu,	oh0,   guard, n_ih0,	zero,	ops) ;
} /* Expand__absl */

/*
@@@  case INTRN_ABSL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__absl(result[1],result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__absl(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of absw based on validated and scheduled basic assembly source.
*/
static void
Expand__absw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  Build_OP (	TOP_absu,	o0,	True_TN,	i0,	ops) ;
} /* Expand__absw */

/*
@@@  case INTRN_ABSW:
@@@    Expand__absw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of addch based on validated and scheduled basic assembly source.
*/
static void
Expand__addch(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__addch */

/*
@@@  case INTRN_ADDCH:
@@@    Expand__addch(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of addcl based on validated and scheduled basic assembly source.
*/
static void
Expand__addcl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__addcl */

/*
@@@  case INTRN_ADDCL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__addcl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__addcl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of addcw based on validated and scheduled basic assembly source.
*/
static void
Expand__addcw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__addcw */

/*
@@@  case INTRN_ADDCW:
@@@    Expand__addcw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of addl based on validated and scheduled basic assembly source.
*/
static void
Expand__addl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  Build_OP (	TOP_addu_r,	ol0,	True_TN, il0,	il1,	ops) ;
  Build_OP (	TOP_addcu,	oh0,	True_TN, ih0,    ih1,	ops) ; 
} /* Expand__addl */

/*
@@@  case INTRN_ADDL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__addl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__addl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of addul based on validated and scheduled basic assembly source.
*/
static void
Expand__addul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  Build_OP (	TOP_addu_r,	ol0,	True_TN, il0,	il1,	ops) ;
  Build_OP (	TOP_addcu,	oh0,	True_TN, ih0,    ih1,	ops) ;
} /* Expand__addul */

/*
@@@  case INTRN_ADDUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__addul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__addul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of bitclrh based on validated and scheduled basic assembly source.
*/
static void
Expand__bitclrh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitclrh */

/*
@@@  case INTRN_BITCLRH:
@@@    Expand__bitclrh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitclrw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitclrw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
    TOP opcode = TOP_b_r;
    TN *index = i1;
    if(TN_has_value(i1))
        {
            if(ISA_LC_Value_In_Class (TN_value(i1), LC_imm_u5))
                {
                    opcode = TOP_b_i5;
                }
            else
                {
                    index = Expand_Immediate_Into_Register(MTYPE_I4, i1, ops);
                }
        }
    Build_OP(opcode, o0, True_TN, Gen_Enum_TN(ISA_ECV_bitop_clr), i0, index, ops);
} /* Expand__bitclrw */

/*
@@@  case INTRN_BITCLRW:
@@@    Expand__bitclrw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitcnth based on validated and scheduled basic assembly source.
*/
static void
Expand__bitcnth(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitcnth */

/*
@@@  case INTRN_BITCNTH:
@@@    Expand__bitcnth(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitcntw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitcntw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitcntw */

/*
@@@  case INTRN_BITCNTW:
@@@    Expand__bitcntw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitnoth based on validated and scheduled basic assembly source.
*/
static void
Expand__bitnoth(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitnoth */

/*
@@@  case INTRN_BITNOTH:
@@@    Expand__bitnoth(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitnotw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitnotw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitnotw */

/*
@@@  case INTRN_BITNOTW:
@@@    Expand__bitnotw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitrevw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitrevw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitrevw */

/*
@@@  case INTRN_BITREVW:
@@@    Expand__bitrevw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitseth based on validated and scheduled basic assembly source.
*/
static void
Expand__bitseth(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitseth */

/*
@@@  case INTRN_BITSETH:
@@@    Expand__bitseth(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitsetw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitsetw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitsetw */

/*
@@@  case INTRN_BITSETW:
@@@    Expand__bitsetw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitvalh based on validated and scheduled basic assembly source.
*/
static void
Expand__bitvalh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitvalh */

/*
@@@  case INTRN_BITVALH:
@@@    Expand__bitvalh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of bitvalw based on validated and scheduled basic assembly source.
*/
static void
Expand__bitvalw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__bitvalw */

/*
@@@  case INTRN_BITVALW:
@@@    Expand__bitvalw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of clamplw based on validated and scheduled basic assembly source.
*/
static void
Expand__clamplw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__clamplw */

/*
@@@  case INTRN_CLAMPLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__clamplw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__clamplw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of clampwh based on validated and scheduled basic assembly source.
*/
static void
Expand__clampwh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__clampwh */

/*
@@@  case INTRN_CLAMPWH:
@@@    Expand__clampwh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of disth based on validated and scheduled basic assembly source.
*/
static void
Expand__disth(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__disth */

/*
@@@  case INTRN_DISTH:
@@@    Expand__disth(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of distuh based on validated and scheduled basic assembly source.
*/
static void
Expand__distuh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__distuh */

/*
@@@  case INTRN_DISTUH:
@@@    Expand__distuh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of distuw based on validated and scheduled basic assembly source.
*/
static void
Expand__distuw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__distuw */

/*
@@@  case INTRN_DISTUW:
@@@    Expand__distuw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of distw based on validated and scheduled basic assembly source.
*/
static void
Expand__distw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__distw */

/*
@@@  case INTRN_DISTW:
@@@    Expand__distw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of edgesh based on validated and scheduled basic assembly source.
*/
static void
Expand__edgesh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__edgesh */

/*
@@@  case INTRN_EDGESH:
@@@    Expand__edgesh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of edgesw based on validated and scheduled basic assembly source.
*/
static void
Expand__edgesw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__edgesw */

/*
@@@  case INTRN_EDGESW:
@@@    Expand__edgesw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of eql based on validated and scheduled basic assembly source.
*/
static void
Expand__eql(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), il0,	il1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),   ih0,	ih1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN, guard,	guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,	guard,	ops) ;
} /* Expand__eql */

/*
@@@  case INTRN_EQL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__eql(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__eql(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of equl based on validated and scheduled basic assembly source.
*/
static void
Expand__equl(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  il0,	il1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0,	ih1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN, guard,	guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,	guard,	ops) ;
} /* Expand__equl */

/*
@@@  case INTRN_EQUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__equl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__equl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of gel based on validated and scheduled basic assembly source.
*/
static void
Expand__gel(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g ,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_ge),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__gel */

/*
@@@  case INTRN_GEL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__gel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__gel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of gethh based on validated and scheduled basic assembly source.
*/
static void
Expand__gethh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__gethh */

/*
@@@  case INTRN_GETHH:
@@@    Expand__gethh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of gethw based on validated and scheduled basic assembly source.
*/
static void
Expand__gethw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__gethw */

/*
@@@  case INTRN_GETHW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__gethw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__gethw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of getlh based on validated and scheduled basic assembly source.
*/
static void
Expand__getlh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__getlh */

/*
@@@  case INTRN_GETLH:
@@@    Expand__getlh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of getlw based on validated and scheduled basic assembly source.
*/
static void
Expand__getlw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__getlw */

/*
@@@  case INTRN_GETLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__getlw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__getlw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of geul based on validated and scheduled basic assembly source.
*/
static void
Expand__geul(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_ge),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN,Gen_Enum_TN(ISA_ECV_cmp_gt),   ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__geul */

/*
@@@  case INTRN_GEUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__geul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__geul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of gtl based on validated and scheduled basic assembly source.
*/
static void
Expand__gtl(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__gtl */

/*
@@@  case INTRN_GTL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__gtl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__gtl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of gtul based on validated and scheduled basic assembly source.
*/
static void
Expand__gtul(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__gtul */

/*
@@@  case INTRN_GTUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__gtul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__gtul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of insequw based on validated and scheduled basic assembly source.
*/
static void
Expand__insequw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insequw */

/*
@@@  case INTRN_INSEQUW:
@@@    Expand__insequw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of inseqw based on validated and scheduled basic assembly source.
*/
static void
Expand__inseqw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__inseqw */

/*
@@@  case INTRN_INSEQW:
@@@    Expand__inseqw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insgeuw based on validated and scheduled basic assembly source.
*/
static void
Expand__insgeuw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insgeuw */

/*
@@@  case INTRN_INSGEUW:
@@@    Expand__insgeuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insgew based on validated and scheduled basic assembly source.
*/
static void
Expand__insgew(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insgew */

/*
@@@  case INTRN_INSGEW:
@@@    Expand__insgew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insgtuw based on validated and scheduled basic assembly source.
*/
static void
Expand__insgtuw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insgtuw */

/*
@@@  case INTRN_INSGTUW:
@@@    Expand__insgtuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insgtw based on validated and scheduled basic assembly source.
*/
static void
Expand__insgtw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insgtw */

/*
@@@  case INTRN_INSGTW:
@@@    Expand__insgtw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insleuw based on validated and scheduled basic assembly source.
*/
static void
Expand__insleuw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insleuw */

/*
@@@  case INTRN_INSLEUW:
@@@    Expand__insleuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of inslew based on validated and scheduled basic assembly source.
*/
static void
Expand__inslew(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__inslew */

/*
@@@  case INTRN_INSLEW:
@@@    Expand__inslew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insltuw based on validated and scheduled basic assembly source.
*/
static void
Expand__insltuw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insltuw */

/*
@@@  case INTRN_INSLTUW:
@@@    Expand__insltuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insltw based on validated and scheduled basic assembly source.
*/
static void
Expand__insltw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insltw */

/*
@@@  case INTRN_INSLTW:
@@@    Expand__insltw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insneuw based on validated and scheduled basic assembly source.
*/
static void
Expand__insneuw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insneuw */

/*
@@@  case INTRN_INSNEUW:
@@@    Expand__insneuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of insnew based on validated and scheduled basic assembly source.
*/
static void
Expand__insnew(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__insnew */

/*
@@@  case INTRN_INSNEW:
@@@    Expand__insnew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of lel based on validated and scheduled basic assembly source.
*/
static void
Expand__lel(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_le),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__lel */

/*
@@@  case INTRN_LEL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__lel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__lel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of leul based on validated and scheduled basic assembly source.
*/
static void
Expand__leul(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_le),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__leul */

/*
@@@  case INTRN_LEUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__leul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__leul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of ltl based on validated and scheduled basic assembly source.
*/
static void
Expand__ltl(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__ltl */

/*
@@@  case INTRN_LTL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__ltl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__ltl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of ltul based on validated and scheduled basic assembly source.
*/
static void
Expand__ltul(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq),  ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  il0,	il1,	ops) ;
  Build_OP (	TOP_andg,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_cmp_r_u_cmp_g,	guard_2,True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt),  ih0, ih1,	ops) ;
  Build_OP (	TOP_org,	guard,  True_TN,  guard, guard_2,ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN,  guard,   ops) ;
} /* Expand__ltul */

/*
@@@  case INTRN_LTUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__ltul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__ltul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of lzcnth based on validated and scheduled basic assembly source.
*/
static void
Expand__lzcnth(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  TN *tmp = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *c0 = Gen_Literal_TN(0, 4) ;
  TN *c16 = Gen_Literal_TN(16, 4) ;

  Build_OP (    TOP_extuh,      tmp, True_TN,   i0,     ops);
  Build_OP (	TOP_lzc,	o0, True_TN,	tmp,	ops) ;
  Build_OP (    TOP_cmp_i8_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), i0, c0, ops);
  Expand_Sub(tmp2, tmp, c16, MTYPE_I4, ops);
  // This piece of code is sub optimal, since we currently do not know how to
  // get the complementary guard (need predication support)
  Expand_Copy(o0, True_TN, tmp2, ops);

  // Expand_Copy does not perform the proper job (bug #28693)!
  // So we have to write our own sequence of code
  Build_OP(TOP_MAKE, o0, guard, c0, ops);

} /* Expand__lzcnth */

/*
@@@  case INTRN_LZCNTH:
@@@    Expand__lzcnth(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of lzcntl based on validated and scheduled basic assembly source.
*/
static void
Expand__lzcntl(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  TN *c0 = Gen_Literal_TN(0, 4) ;
  TN *c32 = Gen_Literal_TN(32, 4) ;
  TN *r0_20_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *r0_21_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;

  Build_OP (   TOP_lzc,	r0_20_0, True_TN,	il0,	ops) ;
  Build_OP (   TOP_lzc,	r0_21_0, True_TN,	ih0,	ops) ;
  Build_OP (   TOP_cmp_i8_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0, c0, ops);
  Build_OP (   TOP_addu_i8, o0, guard, r0_20_0, c32, ops);
  Set_OP_carryisignored (OPS_last(ops));
  Build_OP (   TOP_addu_i8,   o0, guard, r0_21_0, c0, ops);
  Set_OP_carryisignored (OPS_last(ops));
  Set_OP_Pred_False(OPS_last(ops),OP_find_opnd_use(OPS_last(ops), OU_predicate));

} /* Expand__lzcntl */

/*
@@@  case INTRN_LZCNTL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__lzcntl(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__lzcntl(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of lzcntw based on validated and scheduled basic assembly source.
*/
static void
Expand__lzcntw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  Build_OP (	TOP_lzc,	o0, True_TN,	i0,	ops) ;
} /* Expand__lzcntw */

/*
@@@  case INTRN_LZCNTW:
@@@    Expand__lzcntw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of mafcw based on validated and scheduled basic assembly source.
*/
static void
Expand__mafcw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mafcw */

/*
@@@  case INTRN_MAFCW:
@@@    Expand__mafcw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of maxh based on validated and scheduled basic assembly source.
*/
static void
Expand__maxh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_max_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__maxh */

/*
@@@  case INTRN_MAXH:
@@@    Expand__maxh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of maxl based on validated and scheduled basic assembly source.
*/
static void
Expand__maxl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *r0_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *r0_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;

  Build_OP (	TOP_cmp_r_cmp_g,   guard,	 True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt), ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g, guard_1,    True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0,	ih1,	ops) ;
  Build_OP (	TOP_maxu_r,	r0_0,	 True_TN, il0,	il1,	ops) ;
  Build_OP (    TOP_or_i8,       oh0,    True_TN, ih1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       oh0,      guard, ih0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    True_TN, il1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,      guard, il0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    guard_1,r0_0, Gen_Literal_TN(0,4), ops);
} /* Expand__maxl */

/*
@@@  case INTRN_MAXL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__maxl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__maxl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of maxuh based on validated and scheduled basic assembly source.
*/
static void
Expand__maxuh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_maxu_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__maxuh */

/*
@@@  case INTRN_MAXUH:
@@@    Expand__maxuh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of maxul based on validated and scheduled basic assembly source.
*/
static void
Expand__maxul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *r0_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *r0_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;

  Build_OP (	TOP_cmp_r_u_cmp_g,  guard,	 True_TN, Gen_Enum_TN(ISA_ECV_cmp_gt), ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g, guard_1,    True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0,	ih1,	ops) ;
  Build_OP (	TOP_maxu_r,	r0_0,	 True_TN, il0,	il1,	ops) ;
  Build_OP (    TOP_or_i8,       oh0,    True_TN, ih1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       oh0,      guard, ih0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    True_TN, il1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,      guard, il0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    guard_1,r0_0, Gen_Literal_TN(0,4), ops);
} /* Expand__maxul */

/*
@@@  case INTRN_MAXUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__maxul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__maxul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of maxuw based on validated and scheduled basic assembly source.
*/
static void
Expand__maxuw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_maxu_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__maxuw */

/*
@@@  case INTRN_MAXUW:
@@@    Expand__maxuw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of maxw based on validated and scheduled basic assembly source.
*/
static void
Expand__maxw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_max_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__maxw */

/*
@@@  case INTRN_MAXW:
@@@    Expand__maxw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of minh based on validated and scheduled basic assembly source.
*/
static void
Expand__minh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_min_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__minh */

/*
@@@  case INTRN_MINH:
@@@    Expand__minh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of minl based on validated and scheduled basic assembly source.
*/
static void
Expand__minl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *r0_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *r0_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;

  Build_OP (	TOP_cmp_r_cmp_g,   guard,	 True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g, guard_1,    True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0,	ih1,	ops) ;
  Build_OP (	TOP_minu_r,	r0_0,	 True_TN, il0,	il1,	ops) ;
  Build_OP (    TOP_or_i8,       oh0,    True_TN, ih1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       oh0,      guard, ih0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    True_TN, il1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,      guard, il0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    guard_1,r0_0, Gen_Literal_TN(0,4), ops);
} /* Expand__minl */

/*
@@@  case INTRN_MINL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__minl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__minl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of minuh based on validated and scheduled basic assembly source.
*/
static void
Expand__minuh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_minu_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__minuh */

/*
@@@  case INTRN_MINUH:
@@@    Expand__minuh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of minul based on validated and scheduled basic assembly source.
*/
static void
Expand__minul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *r0_0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *r0_1 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;

  Build_OP (	TOP_cmp_r_u_cmp_g,  guard,	 True_TN, Gen_Enum_TN(ISA_ECV_cmp_lt), ih0,	ih1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g, guard_1,    True_TN, Gen_Enum_TN(ISA_ECV_cmp_eq), ih0,	ih1,	ops) ;
  Build_OP (	TOP_minu_r,	r0_0,	 True_TN, il0,	il1,	ops) ;
  Build_OP (    TOP_or_i8,       oh0,    True_TN, ih1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       oh0,      guard, ih0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    True_TN, il1, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,      guard, il0, Gen_Literal_TN(0,4), ops);
  Build_OP (    TOP_or_i8,       ol0,    guard_1,r0_0, Gen_Literal_TN(0,4), ops);

} /* Expand__minul */

/*
@@@  case INTRN_MINUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__minul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__minul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of minuw based on validated and scheduled basic assembly source.
*/
static void
Expand__minuw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_minu_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__minuw */

/*
@@@  case INTRN_MINUW:
@@@    Expand__minuw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of minw based on validated and scheduled basic assembly source.
*/
static void
Expand__minw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  Build_OP (	TOP_min_r,	o0,	True_TN, i0,	i1,	ops) ;
} /* Expand__minw */

/*
@@@  case INTRN_MINW:
@@@    Expand__minw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mpfcw based on validated and scheduled basic assembly source.
*/
static void
Expand__mpfcw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpfcw */

/*
@@@  case INTRN_MPFCW:
@@@    Expand__mpfcw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mpfcwl based on validated and scheduled basic assembly source.
*/
static void
Expand__mpfcwl(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpfcwl */

/*
@@@  case INTRN_MPFCWL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mpfcwl(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__mpfcwl(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mpfml based on validated and scheduled basic assembly source.
*/
static void
Expand__mpfml(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpfml */

/*
@@@  case INTRN_MPFML:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mpfml(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__mpfml(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mpfrch based on validated and scheduled basic assembly source.
*/
static void
Expand__mpfrch(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpfrch */

/*
@@@  case INTRN_MPFRCH:
@@@    Expand__mpfrch(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mpml based on validated and scheduled basic assembly source.
*/
static void
Expand__mpml(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpml */

/*
@@@  case INTRN_MPML:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mpml(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__mpml(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mpuml based on validated and scheduled basic assembly source.
*/
static void
Expand__mpuml(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mpuml */

/*
@@@  case INTRN_MPUML:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mpuml(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__mpuml(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mulfch based on validated and scheduled basic assembly source.
*/
static void
Expand__mulfch(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulfch */

/*
@@@  case INTRN_MULFCH:
@@@    Expand__mulfch(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulfcm based on validated and scheduled basic assembly source.
*/
static void
Expand__mulfcm(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulfcm */

/*
@@@  case INTRN_MULFCM:
@@@    Expand__mulfcm(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulfcw based on validated and scheduled basic assembly source.
*/
static void
Expand__mulfcw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulfcw */

/*
@@@  case INTRN_MULFCW:
@@@    Expand__mulfcw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulh based on validated and scheduled basic assembly source.
*/
static void
Expand__mulh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulh */

/*
@@@  case INTRN_MULH:
@@@    Expand__mulh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulhh based on validated and scheduled basic assembly source.
*/
static void
Expand__mulhh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulhh */

/*
@@@  case INTRN_MULHH:
@@@    Expand__mulhh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulhuh based on validated and scheduled basic assembly source.
*/
static void
Expand__mulhuh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulhuh */

/*
@@@  case INTRN_MULHUH:
@@@    Expand__mulhuh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulhuw based on validated and scheduled basic assembly source.
*/
static void
Expand__mulhuw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulhuw */

/*
@@@  case INTRN_MULHUW:
@@@    Expand__mulhuw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulhw based on validated and scheduled basic assembly source.
*/
static void
Expand__mulhw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulhw */

/*
@@@  case INTRN_MULHW:
@@@    Expand__mulhw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mull based on validated and scheduled basic assembly source.
*/
static void
Expand__mull(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mull */

/*
@@@  case INTRN_MULL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mull(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__mull(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mulm based on validated and scheduled basic assembly source.
*/
static void
Expand__mulm(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulm */

/*
@@@  case INTRN_MULM:
@@@    Expand__mulm(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of muln based on validated and scheduled basic assembly source.
*/
static void
Expand__muln(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__muln */

/*
@@@  case INTRN_MULN:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__muln(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__muln(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of muluh based on validated and scheduled basic assembly source.
*/
static void
Expand__muluh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__muluh */

/*
@@@  case INTRN_MULUH:
@@@    Expand__muluh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulul based on validated and scheduled basic assembly source.
*/
static void
Expand__mulul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulul */

/*
@@@  case INTRN_MULUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mulul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__mulul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of mulum based on validated and scheduled basic assembly source.
*/
static void
Expand__mulum(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulum */

/*
@@@  case INTRN_MULUM:
@@@    Expand__mulum(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulun based on validated and scheduled basic assembly source.
*/
static void
Expand__mulun(
 TN* ol0,
 TN* oh0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulun */

/*
@@@  case INTRN_MULUN:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__mulun(result[1],result[0],opnd[0],opnd[1],ops) ;
@@@  } else { 
@@@    Expand__mulun(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of muluw based on validated and scheduled basic assembly source.
*/
static void
Expand__muluw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__muluw */

/*
@@@  case INTRN_MULUW:
@@@    Expand__muluw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of mulw based on validated and scheduled basic assembly source.
*/
static void
Expand__mulw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__mulw */

/*
@@@  case INTRN_MULW:
@@@    Expand__mulw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of nearclw based on validated and scheduled basic assembly source.
*/
static void
Expand__nearclw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__nearclw */

/*
@@@  case INTRN_NEARCLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__nearclw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__nearclw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of nearcwh based on validated and scheduled basic assembly source.
*/
static void
Expand__nearcwh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__nearcwh */

/*
@@@  case INTRN_NEARCWH:
@@@    Expand__nearcwh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of nearlw based on validated and scheduled basic assembly source.
*/
static void
Expand__nearlw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__nearlw */

/*
@@@  case INTRN_NEARLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__nearlw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__nearlw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of nearwh based on validated and scheduled basic assembly source.
*/
static void
Expand__nearwh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__nearwh */

/*
@@@  case INTRN_NEARWH:
@@@    Expand__nearwh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of negch based on validated and scheduled basic assembly source.
*/
static void
Expand__negch(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__negch */

/*
@@@  case INTRN_NEGCH:
@@@    Expand__negch(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of negcl based on validated and scheduled basic assembly source.
*/
static void
Expand__negcl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__negcl */

/*
@@@  case INTRN_NEGCL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__negcl(result[1],result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__negcl(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of negcw based on validated and scheduled basic assembly source.
*/
static void
Expand__negcw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__negcw */

/*
@@@  case INTRN_NEGCW:
@@@    Expand__negcw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of negl based on validated and scheduled basic assembly source.
*/
static void
Expand__negl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  TN *n_il0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_ih0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *zero = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  Build_OP (    TOP_MAKE,       zero,   True_TN, Gen_Literal_TN(0,2), ops);

  Build_OP (	TOP_not,	n_il0,   True_TN, il0,	ops) ;
  Build_OP (	TOP_not,	n_ih0,   True_TN, ih0,	ops) ;

  Build_OP (	TOP_addu_i8,	ol0,   True_TN, n_il0,	Gen_Literal_TN(1,2),ops) ;
  Build_OP (	TOP_addcu,	oh0,   True_TN, n_ih0,	zero, ops) ;
} /* Expand__negl */

/*
@@@  case INTRN_NEGL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__negl(result[1],result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__negl(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of negul based on validated and scheduled basic assembly source.
*/
static void
Expand__negul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  TN *n_il0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *n_ih0 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *zero = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  Build_OP (    TOP_MAKE,       zero,   True_TN, Gen_Literal_TN(0,2), ops);

  Build_OP (	TOP_not,	n_il0,   True_TN, il0,	ops) ;
  Build_OP (	TOP_not,	n_ih0,   True_TN, ih0,	ops) ;

  Build_OP (	TOP_addu_i8,	ol0,   True_TN, n_il0,	Gen_Literal_TN(1,2), ops) ;
  Build_OP (	TOP_addcu,	oh0,   True_TN, n_ih0,	zero,	ops) ;
} /* Expand__negul */

/*
@@@  case INTRN_NEGUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__negul(result[1],result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__negul(result[0],result[1],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of nel based on validated and scheduled basic assembly source.
*/
static void
Expand__nel(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_ne),   il0,	il1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard2,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_ne),   ih0,	ih1,	ops) ;
  Build_OP (    TOP_org,    	guard,  True_TN, guard, guard2, ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN, guard,	ops) ;
} /* Expand__nel */

/*
@@@  case INTRN_NEL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__nel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__nel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of neul based on validated and scheduled basic assembly source.
*/
static void
Expand__neul(
 TN* o0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_ne),   il0,	il1,	ops) ;
  Build_OP (	TOP_cmp_r_cmp_g,	guard2,	True_TN, Gen_Enum_TN(ISA_ECV_cmp_ne),   ih0,	ih1,	ops) ;
  Build_OP (    TOP_org,        guard,  True_TN, guard, guard2, ops) ;
  Build_OP (	TOP_bool,	o0,	True_TN, guard,	ops) ;
} /* Expand__neul */

/*
@@@  case INTRN_NEUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__neul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__neul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of normh based on validated and scheduled basic assembly source.
*/
static void
Expand__normh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__normh */

/*
@@@  case INTRN_NORMH:
@@@    Expand__normh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of norml based on validated and scheduled basic assembly source.
*/
static void
Expand__norml(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__norml */

/*
@@@  case INTRN_NORML:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__norml(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__norml(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of normw based on validated and scheduled basic assembly source.
*/
static void
Expand__normw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__normw */

/*
@@@  case INTRN_NORMW:
@@@    Expand__normw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of priorh based on validated and scheduled basic assembly source.
*/
static void
Expand__priorh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__priorh */

/*
@@@  case INTRN_PRIORH:
@@@    Expand__priorh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of priorl based on validated and scheduled basic assembly source.
*/
static void
Expand__priorl(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__priorl */

/*
@@@  case INTRN_PRIORL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__priorl(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__priorl(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of priorw based on validated and scheduled basic assembly source.
*/
static void
Expand__priorw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__priorw */

/*
@@@  case INTRN_PRIORW:
@@@    Expand__priorw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of puthl based on validated and scheduled basic assembly source.
*/
static void
Expand__puthl(
 TN* ol0,
 TN* oh0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__puthl */

/*
@@@  case INTRN_PUTHL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__puthl(result[1],result[0],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__puthl(result[0],result[1],opnd[0],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of puthw based on validated and scheduled basic assembly source.
*/
static void
Expand__puthw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__puthw */

/*
@@@  case INTRN_PUTHW:
@@@    Expand__puthw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of putll based on validated and scheduled basic assembly source.
*/
static void
Expand__putll(
 TN* ol0,
 TN* oh0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__putll */

/*
@@@  case INTRN_PUTLL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__putll(result[1],result[0],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__putll(result[0],result[1],opnd[0],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of putlw based on validated and scheduled basic assembly source.
*/
static void
Expand__putlw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__putlw */

/*
@@@  case INTRN_PUTLW:
@@@    Expand__putlw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of rotlh based on validated and scheduled basic assembly source.
*/
static void
Expand__rotlh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__rotlh */

/*
@@@  case INTRN_ROTLH:
@@@    Expand__rotlh(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of rotlw based on validated and scheduled basic assembly source.
*/
static void
Expand__rotlw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__rotlw */

/*
@@@  case INTRN_ROTLW:
@@@    Expand__rotlw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of roundclw based on validated and scheduled basic assembly source.
*/
static void
Expand__roundclw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__roundclw */

/*
@@@  case INTRN_ROUNDCLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__roundclw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__roundclw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of roundcwh based on validated and scheduled basic assembly source.
*/
static void
Expand__roundcwh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__roundcwh */

/*
@@@  case INTRN_ROUNDCWH:
@@@    Expand__roundcwh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of roundlw based on validated and scheduled basic assembly source.
*/
static void
Expand__roundlw(
 TN* o0,
 TN* il0,
 TN* ih0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__roundlw */

/*
@@@  case INTRN_ROUNDLW:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__roundlw(result[0],opnd[1],opnd[0],ops) ;
@@@  } else { 
@@@    Expand__roundlw(result[0],opnd[0],opnd[1],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of roundwh based on validated and scheduled basic assembly source.
*/
static void
Expand__roundwh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__roundwh */

/*
@@@  case INTRN_ROUNDWH:
@@@    Expand__roundwh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of shlch based on validated and scheduled basic assembly source.
*/
static void
Expand__shlch(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shlch */

/*
@@@  case INTRN_SHLCH:
@@@    Expand__shlch(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of shlcw based on validated and scheduled basic assembly source.
*/
static void
Expand__shlcw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shlcw */

/*
@@@  case INTRN_SHLCW:
@@@    Expand__shlcw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of shll based on validated and scheduled basic assembly source.
*/
static void
Expand__shll(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* i1,
 OPS* ops
)
{
  INT64 value;

    TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
    TN *guard2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
    TN *guard3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;

    TN *val64 = Gen_Literal_TN(64, 4, TRUE);
    TN *val32 = Gen_Literal_TN(32, 4, TRUE);
    TN *val0 = Gen_Literal_TN(0, 4, TRUE);
    TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
    TN *tmp = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;

    //        0:  0xe2189018    G7&   CMPGEU  G0,R2,0x40
    Build_OP(TOP_cmp_i8_u_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_ge),
             i1, val64, ops);
    //     Expand__KILL(&ol0, 1, ops);
    Expand__KILL(&oh0, 1, ops);
    //        4:  0x81400015    G0?   MAKE    R0,0x0
    Build_OP(TOP_MAKE, ol0, True_TN, val0, ops);
    //        8:  0x81400015    G0?   MAKE    R1,0x0
    Build_OP (TOP_MAKE, oh0, guard, val0, ops);
    
    //        c:  0x02188818    G4&   CMPLTU  G0,R2,0x20
    Build_OP(TOP_cmp_i8_u_cmp_g, guard2, guard, Gen_Enum_TN(ISA_ECV_cmp_ge),
             i1, val32, ops);
    Set_OP_Pred_False(OPS_last(ops), 0);

    Expand__KILL(&tmp2, 1, ops);
    //       10:  0x82188810    G0?   SUBU    R2,R2,0x20
    Build_OP(TOP_subu_i8, tmp2, guard2, i1, val32, ops);
    Set_OP_carryisignored (OPS_last(ops));

    //       14:  0x81082092    G0?   SHLU    R1,R0,R2
    Build_OP(TOP_shlu_r, oh0, guard2, il0, tmp2, ops);
    //       18:  0x80400015    G0?   MAKE    R0,0x0
    //    Build_OP(TOP_MAKE, ol0, guard2, val0, ops);
    // redundant with instruction 4:
    
    //       28:  0x00082092    G4?   SHLU    R0,R0,R2
     Build_OP(TOP_shlu_r, oh0, guard2, ih0, i1, ops);
    Set_OP_Pred_False(OPS_last(ops), 0);
    //       2c:  0x01086092    G4?   SHLU    R1,R1,R2
    Build_OP(TOP_shlu_r, ol0, guard2, il0, i1, ops);
    Set_OP_Pred_False(OPS_last(ops), 0);
    
    //       1c:  0x00488018    G4&   CMPNE   G0,R2,0x0
    Build_OP(TOP_cmp_i8_cmp_g, guard3, guard2, Gen_Enum_TN(ISA_ECV_cmp_ne),
             i1, val0, ops);
    Set_OP_Pred_False(OPS_last(ops), 0);
    Expand__KILL(&tmp, 1, ops);
    //       20:  0x23302092    G0?   SHRNU   R3,R0,R2
    Build_OP(TOP_shrnu, tmp, guard3, il0, i1, ops);
    //    30:  0x01b840d1    G0?   OR      R1,R1,R3
    Build_OP(TOP_or_r, oh0, guard3, oh0, tmp, ops);
} /* Expand__shll */

/*
 * Expansion of shlw based on validated and scheduled basic assembly source.
*/
static void
Expand__shlw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shlw */

/*
@@@  case INTRN_SHLW:
@@@    Expand__shlw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of shrl based on validated and scheduled basic assembly source.
*/
static void
Expand__shrl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* i1,
 OPS* ops
)
{
  INT64 value;
  //   if (TN_Value_At_Op(i1, NULL, &value)) {
  //   }
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  
  TN *val64 = Gen_Literal_TN(64, 4, TRUE);
  TN *val32 = Gen_Literal_TN(32, 4, TRUE);
  TN *val0 = Gen_Literal_TN(0, 4, TRUE);
  TN *val31 = Gen_Literal_TN(31, 4, TRUE);
  TN *tmp = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
    
//         0:  0xe2189018    G7&   CMPGEU  G0,<i1>,0x40
  Build_OP(TOP_cmp_i8_u_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_ge),
           i1, val64, ops);
  //       4:  0x811047d2    <guard>?   SHR     <oh0>,<ih0>,0x1F
  //  Expand__KILL(&oh0, 1, ops);
  Expand__KILL(&ol0, 1, ops);
  Build_OP(TOP_shr_i5, oh0, True_TN, ih0, val31, ops);
  //       8:  0x80b84010    <guard>?   OR      <ih0>,<oh0>,0x0
  Build_OP(TOP_or_i8, ol0, guard, oh0, val0, ops);
  
  //       c:  0x02188818    <!guard>&   CMPGEU  G0,<i1>,0x20
  Build_OP(TOP_cmp_i8_u_cmp_g, guard2, guard, Gen_Enum_TN(ISA_ECV_cmp_ge),
           i1, val32, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  
  //        10:  0x82188810    <guard2>?   SUBU    <tmp2>,<i1>,0x20
  Expand__KILL(&tmp2, 1, ops);
  Build_OP(TOP_subu_i8, tmp2, guard2, i1, val32, ops);
  Set_OP_carryisignored (OPS_last(ops));
  //        14:  0x80106092    <guard2>?   SHR     <ol0>,<ih0>,<tmp2>
  Build_OP(TOP_shr_r, ol0, guard2, ih0, tmp2, ops);
  //        18:  0x811047d2    <guard2>?   SHR     <oh0>,<ih0>,0x1F
  //  Build_OP(TOP_shr_i5, oh0, guard2, ih0, val31, ops);
  // redundant with isntruction 4

  //        28:  0x00182092    <!guard2>?   SHRU    <ol0>,<il0>,<i1>
  Build_OP(TOP_shru_r, ol0, guard2, il0, i1, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  //        2c:  0x01106092    <!guard2>?   SHR     <oh0>,<ih0>,<i1>
  Build_OP(TOP_shr_r, oh0, guard2, ih0, i1, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  
//        1c:  0x00488018    <!guar2>&   CMPNE   <guard3>,<i1>,0x0
  Build_OP(TOP_cmp_i8_u_cmp_g, guard3, guard2, Gen_Enum_TN(ISA_ECV_cmp_ne),
           i1, val0, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  //        20:  0x23206092    <guard3>?   SHLNU   <tmp>,<ih0>,<i1>
  Expand__KILL(&tmp, 1, ops);
  Build_OP(TOP_shlnu, tmp, guard3, ih0, i1, ops);
  //        30:  0x00b800d1    <guard3>?   OR      <ol0>,<ol0>,<tmp>
  Build_OP(TOP_or_r, ol0, guard3, ol0, tmp, ops);
  
} /* Expand__shrl */

/*
 * Expansion of shrrh based on validated and scheduled basic assembly source.
*/
static void
Expand__shrrh(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shrrh */


/*
 * Expansion of shrrw based on validated and scheduled basic assembly source.
*/
static void
Expand__shrrw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shrrw */

/*
 * Expansion of shrul based on validated and scheduled basic assembly source.
*/
static void
Expand__shrul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* i1,
 OPS* ops
)
{
  INT64 value;
  //   if (TN_Value_At_Op(i1, NULL, &value)) {
  //   }
  TN *guard = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  TN *guard3 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gr) ;
  
  TN *val64 = Gen_Literal_TN(64, 4, TRUE);
  TN *val32 = Gen_Literal_TN(32, 4, TRUE);
  TN *val0 = Gen_Literal_TN(0, 4, TRUE);
  TN *val31 = Gen_Literal_TN(31, 4, TRUE);
  TN *tmp = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
  TN *tmp2 = Build_RCLASS_TN (ISA_REGISTER_CLASS_gpr) ;
    
//         0:  0xe2189018    G7&   CMPGEU  G0,<i1>,0x40
  Build_OP(TOP_cmp_i8_u_cmp_g, guard, True_TN, Gen_Enum_TN(ISA_ECV_cmp_ge),
           i1, val64, ops);
  //         4:  0x811047d2    <guard>?   MAKE     <oh0>,0
  //   Expand__KILL(&oh0, 1, ops);
  Expand__KILL(&ol0, 1, ops);
  Build_OP(TOP_MAKE, oh0, True_TN, val0, ops);
  //         8:  0x80b84010    <guard>?   MAKE      <ih0>,0
  Build_OP(TOP_MAKE, ol0, guard, val0, ops);
  
  //         c:  0x02188818    <!guard>&   CMPGEU  G0,<i1>,0x20
  Build_OP(TOP_cmp_i8_u_cmp_g, guard2, guard, Gen_Enum_TN(ISA_ECV_cmp_ge),
           i1, val32, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  //        10:  0x82188810    <guard2>?   SUBU    <tmp2>,<i1>,0x20
  Expand__KILL(&tmp2, 1, ops);
  Build_OP(TOP_subu_i8, tmp2, guard2, i1, val32, ops);
  Set_OP_carryisignored (OPS_last(ops));
  //        14:  0x80106092    <guard2>?   SHR     <ol0>,<ih0>,<tmp2>
  Build_OP(TOP_shru_r, ol0, guard2, ih0, tmp2, ops);
  //        18:  0x811047d2    <guard2>?   MAKE     <oh0>,0
  //redundant with instruction 4
  //  Build_OP(TOP_MAKE, oh0, guard2, val0, ops);
  
  //        28:  0x00182092    <!guard2>?   SHRU    <ol0>,<il0>,<i1>
  Build_OP(TOP_shru_r, ol0, guard2, il0, i1, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  //        2c:  0x01106092    <!guard2>?   SHRU     <oh0>,<ih0>,<i1>
  Build_OP(TOP_shru_r, oh0, guard2, ih0, i1, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  
  //        1c:  0x00488018    <!guar2>&   CMPNE   <guard3>,<i1>,0x0
  Build_OP(TOP_cmp_i8_u_cmp_g, guard3, guard2, Gen_Enum_TN(ISA_ECV_cmp_ne),
           i1, val0, ops);
  Set_OP_Pred_False(OPS_last(ops), 0);
  //        20:  0x23206092    <guard3>?   SHLNU   <tmp>,<ih0>,<i1>
  Expand__KILL(&tmp, 1, ops);
  Build_OP(TOP_shlnu, tmp, guard3, ih0, i1, ops);
  //        30:  0x00b800d1    <guard3>?   OR      <ol0>,<ol0>,<tmp>
  Build_OP(TOP_or_r, ol0, guard3, ol0, tmp, ops);
  
} /* Expand__shrul */


/*
 * Expansion of shruw based on validated and scheduled basic assembly source.
*/
static void
Expand__shruw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shruw */

/*
@@@  case INTRN_SHRUW:
@@@    Expand__shruw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of shrw based on validated and scheduled basic assembly source.
*/
static void
Expand__shrw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__shrw */

/*
@@@  case INTRN_SHRW:
@@@    Expand__shrw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of subch based on validated and scheduled basic assembly source.
*/
static void
Expand__subch(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__subch */

/*
@@@  case INTRN_SUBCH:
@@@    Expand__subch(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of subcl based on validated and scheduled basic assembly source.
*/
static void
Expand__subcl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__subcl */

/*
@@@  case INTRN_SUBCL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__subcl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__subcl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of subcw based on validated and scheduled basic assembly source.
*/
static void
Expand__subcw(
 TN* o0,
 TN* i0,
 TN* i1,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__subcw */

/*
@@@  case INTRN_SUBCW:
@@@    Expand__subcw(result[0],opnd[0],opnd[1],ops) ;
@@@  break ;
*/

/*
 * Expansion of subl based on validated and scheduled basic assembly source.
*/
static void
Expand__subl(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{
  Build_OP (	TOP_subu_r,	ol0,	True_TN, il0,	il1,	ops) ;
  Build_OP (	TOP_subcu,	oh0,	True_TN, ih0,    ih1,	ops) ;
} /* Expand__subl */

/*
@@@  case INTRN_SUBL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__subl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__subl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of subul based on validated and scheduled basic assembly source.
*/
static void
Expand__subul(
 TN* ol0,
 TN* oh0,
 TN* il0,
 TN* ih0,
 TN* il1,
 TN* ih1,
 OPS* ops
)
{

  Build_OP (	TOP_subu_r,	ol0,	True_TN, il0,	il1,	ops) ;
  Build_OP (	TOP_subcu,	oh0,	True_TN, ih0,    ih1,	ops) ;
} /* Expand__subul */

/*
@@@  case INTRN_SUBUL:
@@@  if (Target_Byte_Sex == BIG_ENDIAN) {
@@@    Expand__subul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
@@@  } else { 
@@@    Expand__subul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
@@@  }  
@@@  break ;
*/

/*
 * Expansion of swapbh based on validated and scheduled basic assembly source.
*/
static void
Expand__swapbh(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__swapbh */

/*
@@@  case INTRN_SWAPBH:
@@@    Expand__swapbh(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of swapbw based on validated and scheduled basic assembly source.
*/
static void
Expand__swapbw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__swapbw */

/*
@@@  case INTRN_SWAPBW:
@@@    Expand__swapbw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of swaphw based on validated and scheduled basic assembly source.
*/
static void
Expand__swaphw(
 TN* o0,
 TN* i0,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__swaphw */

/*
@@@  case INTRN_SWAPHW:
@@@    Expand__swaphw(result[0],opnd[0],ops) ;
@@@  break ;
*/

/*
 * Expansion of xshlh based on validated and scheduled basic assembly source.
*/
static void
Expand__xshlh(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__xshlh */

/*
@@@  case INTRN_XSHLH:
@@@    Expand__xshlh(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of xshlw based on validated and scheduled basic assembly source.
*/
static void
Expand__xshlw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__xshlw */

/*
@@@  case INTRN_XSHLW:
@@@    Expand__xshlw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of xshrh based on validated and scheduled basic assembly source.
*/
static void
Expand__xshrh(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__xshrh */

/*
@@@  case INTRN_XSHRH:
@@@    Expand__xshrh(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

/*
 * Expansion of xshrw based on validated and scheduled basic assembly source.
*/
static void
Expand__xshrw(
 TN* o0,
 TN* i0,
 TN* i1,
 TN* i2,
 OPS* ops
)
{
  FmtAssert (FALSE, ("not yet implemented"));
} /* Expand__xshrw */

/* =============================================================================
 * =
 * =  Handling of dynamically added (extension) specific intrinsics
 * =
 * =============================================================================
 */
/*
 * Expansion of a full compose intrinsic, used to initialize a SIMD TN from
 * sub TNs
 * Note: Operand ordering is reversed here. Indeed, C level compose builtins
 *       take subparts in big endian ordering, whereas compose TOP assumes
 *       operands to be in little endian ordering.
 */
static void
Expand_Extension_Compose(
  INTRINSIC id,
  INT num_results,
  INT num_opnds,
  TN *result[],
  TN *opnd[],
  OPS *ops
  ) {
  TN *macroreg, *subreg;
  FmtAssert((num_opnds==2 || num_opnds==4) && num_results==1,
	    ("Incorrect number of parameters for Compose intrinsic"));
    
  macroreg = result[0];
  subreg = opnd[0];
    
  FmtAssert(TN_register_class(subreg)==TN_register_class(macroreg),
	    ("Incompatible register class for compose operation"));
    
  int nb_subreg = TN_size(macroreg) / TN_size(subreg);
  
  if (nb_subreg==2) {
    Build_OP_simulated_compose(result[0],
			       True_TN,
			       opnd[1], opnd[0], ops);
  }
  else if (nb_subreg==4) {
    Build_OP_simulated_compose(result[0],
			       True_TN,
			       opnd[3], opnd[2],
			       opnd[1], opnd[0], ops);
  }
  else {
    FmtAssert(0, ("Unexpected composite TN size"));
  }

  return;
}

/*
 * Expansion of a partial compose intrinsic, used to update only a subpart of a TN
 */
static void
Expand_Extension_Partial_Compose(
  INTRINSIC id,
  INT num_results,
  INT num_opnds,
  TN *result[],
  TN *opnd[],
  OPS *ops
  ) {
  int i;
  TN *new_opnd[ISA_OPERAND_max_operands];
  TN *new_result[ISA_OPERAND_max_results];
  TN *macroreg, *subreg;
  ISA_REGISTER_CLASS rclass;
  FmtAssert(num_opnds==2 && num_results==1,
	    ("Incorrect number of parameters for Partial Compose intrinsic"));
    
  macroreg = opnd[0];
  subreg   = opnd[1];
    
  rclass   = TN_register_class(macroreg);
  FmtAssert(TN_register_class(subreg)==rclass,
	    ("Incompatible register class for compose operation"));
    
  int nb_subreg = TN_size(macroreg) / TN_size(subreg);
  
  int rank_to_update = EXTENSION_Get_ComposeExtract_Index(id);
    
  FmtAssert(rank_to_update>=0 && rank_to_update<nb_subreg,
	    ("Compose: Access out of bound to composite register"));
    
  // Partial compose is currently divided in 2 steps (2 pseudo TOPs):
  // - First extract all sub-TNs of the macro TN to update
  // - Then  compose the updated TN with a combination of its own sub-TNs and
  //   the subTN to add
  
  // 1) EXTRACT
  // operands: guard, COMPOSITE_REG
  // results : [new sub TNs]*
  for (i=0; i<nb_subreg; i++) {
    new_result[i] = Gen_Register_TN(rclass, TN_size(subreg));
  }
  if (nb_subreg==2) {
    Build_OP_simulated_extract(new_result[0], new_result[1],
			       True_TN, macroreg, ops);
  }
  else if (nb_subreg==4) {
    Build_OP_simulated_extract(new_result[0], new_result[1],
			       new_result[2], new_result[3],
			       True_TN, macroreg, ops);
  }
  else {
    FmtAssert(0, ("Unexpected composite TN size"));
  }

  // 2) COMPOSE 
  // operands: guard, [new sub TNS + subreg]*
  // results : macroreg
  for (i=0; i<nb_subreg; i++) {
    if (i != rank_to_update) {
      new_opnd[i] = new_result[i];
    }
    else {
      new_opnd[i] = subreg;
    }
  }

  if (nb_subreg==2) {
    Build_OP_simulated_compose(result[0],
			       True_TN,
			       new_opnd[0], new_opnd[1], ops);
  }
  else if (nb_subreg==4) {
    Build_OP_simulated_compose(result[0],
			       True_TN,
			       new_opnd[0], new_opnd[1],
			       new_opnd[2], new_opnd[3], ops);
  }
  else {
    FmtAssert(0, ("Unexpected composite TN size"));
  }

  return;
}

/*
 * Expansion of a partial extract intrinsic, used to retrieve only a subpart of a TN
 */
static void
Expand_Extension_Partial_Extract(
  INTRINSIC id,
  INT num_results,
  INT num_opnds,
  TN *result[],
  TN *opnd[],
  OPS *ops
  ) {
  int i;
  TN *new_result[ISA_OPERAND_max_results];
  TN *macroreg, *subreg;
  ISA_REGISTER_CLASS rclass;
  FmtAssert((num_opnds==1 && num_results==1),
	    ("Incorrect number of parameter for Partial Extract intrinsic"));

  macroreg = opnd[0];
  subreg   = result[0];

  rclass   = TN_register_class(macroreg);
  FmtAssert(TN_register_class(subreg)==rclass,
	    ("Incompatible register class for compose operation"));

  int nb_subreg = TN_size(macroreg) / TN_size(subreg);

  int rank_to_get = EXTENSION_Get_ComposeExtract_Index(id);

  // EXTRACT
  // operands: guard, COMPOSITE_REG
  // results : dead TNs, ...subreg, ... dead TNs
  for (i=0; i<nb_subreg; i++) {
    if (i != rank_to_get) {
      // Create tempo TNs that will not be used
      new_result[i] = Gen_Register_TN(rclass, TN_size(subreg));
    }
    else {
      new_result[i] = subreg;
    }
  }
	
  if (nb_subreg==2) {
    Build_OP_simulated_extract(new_result[0], new_result[1],
			       True_TN,
			       macroreg, ops);
  }
  else if (nb_subreg==4) {
    Build_OP_simulated_extract(new_result[0], new_result[1],
			       new_result[2], new_result[3],
			       True_TN,
			       macroreg, ops);
  }
  else {
    FmtAssert(0, ("Unexpected composite TN size"));
  }
}

/** 
 * Expand the intrinsic_op corresponding to a meta instrinsic into
 * the corresponding asm-stmt node.
 * 
 * @param id  intrinsic_id
 * @param num_results 
 * @param num_opnds 
 * @param result 
 * @param opnd 
 * @param ops 
 * @param srcpos 
 * @param curbb
 */
typedef std::map <TN*, TN*> TNCorrespMap;

static void
EXTENSION_Expand_Meta_Intrinsic (
  INTRINSIC id,
  INT wn_num_results,
  INT wn_num_opnds,
  TN *wn_result[],
  TN *wn_opnd[],
  OPS *ops,
  SRCPOS srcpos,
  BB* curbb
  ) {

  // 'result' and 'opnd' below have a fixed size as well as
  // the arrays in ASM_OP_ANNOT. Define here so we can sanity check.
  // [TTh] Max number of operands and results retrieved from gccfe constant
  // Note that arrays in ASM_OP_ANNOT are now dynamically allocated
  // when calling Create_Empty_ASM_OP_ANNOT(nb_res, nb_opnd)
  enum { MAX_OPNDS = MAX_RECOG_OPERANDS, MAX_RESULTS = MAX_RECOG_OPERANDS };

  OPS New_OPs;
  OPS_Init(&New_OPs);

  // First get rid of any dedicated register used before the meta
  // intrinsic. (first EBO pass deadcodes any dedicated tn write.)
  // note that this pass introduces extra copies that are likely to be
  // suppressed in coalescing/regalloc.
  TNCorrespMap tnMap;

  OP* op;
  FOR_ALL_OPS_OPs(ops, op) {
    int r;
    for (r=0; r<OP_results(op); r++) {
      TN* res =  OP_result(op, r);
      FmtAssert((res!=NULL&&TN_is_register(res)),
                ("Tn res is necessarily a register"));
      INT sameres = OP_same_res(op, r);
      /* res is sameres and already copied into another tn.
       * -> just propagate the copy to the new TN.
       */
      if (sameres>=0 && tnMap[res]!=NULL) {
        Set_OP_result(op, r, tnMap[res]);
        Set_OP_opnd(op, sameres, tnMap[res]);
      } else {
        /* tn is dedicated thus we need to create a new tn and
           propagate it */
	ISA_REGISTER_CLASS rc = TN_register_class(res);
        if (TN_is_dedicated(res) &&
	    (ABI_PROPERTY_Is_func_arg(rc, REGISTER_machine_id(rc, TN_register(res))) ||
	     ABI_PROPERTY_Is_func_val(rc, REGISTER_machine_id(rc, TN_register(res))))) {
          TN* new_tn = Build_TN_Like(res);
          /* store the tn correspondence */
          tnMap[res]= new_tn;
          Set_OP_result(op, r, new_tn);
          /* New_OPs is the next BB. */
          Exp_COPY(res, new_tn, &New_OPs);
          
          /* handles the unlikely sameres case on dedicated tn case */
          if (sameres>=0) {
            Set_OP_opnd(op, sameres, new_tn);
            /* we must also insert a copy from the dedicated tn to the
               new_tn before current op */
            OPS Insert_OPs;
            OPS_Init(&Insert_OPs);
            Exp_COPY(new_tn, res, &Insert_OPs);
            OPS_Insert_Ops(ops, op, &Insert_OPs, true);
            OPS_Remove_All(&Insert_OPs);
          }
        }
      }
    }
    int o;
    for (o=0; o<OP_opnds(op); o++) {
      TN* opn =  OP_opnd(op, o);
      if (tnMap[opn] != NULL) {
        Set_OP_opnd(op, o, tnMap[opn]);
      }
    }
  }

  // these two arrays may have to be reallocatable
  TN* result[MAX_RESULTS];
  TN* opnd[MAX_OPNDS]; 

  INT num_results = 0;
  INT num_opnds = 0;

  int same_res[MAX_RESULTS];


  ISA_REGISTER_SUBCLASS opnd_sc[OP_MAX_FIXED_OPNDS];
  BZERO(opnd_sc, sizeof(opnd_sc));

  char* asm_template = EXTENSION_Get_Meta_INTRINSIC_Asm_Behavior(id);
  char** asm_results = EXTENSION_Get_Meta_INTRINSIC_Asm_Results(id);
  char** asm_operands = EXTENSION_Get_Meta_INTRINSIC_Asm_Operands(id);
  char** asm_clobbers = NULL;
  const char* full_asm_results[MAX_RESULTS];

  FmtAssert((asm_template!=NULL && asm_results!=NULL && asm_operands!=NULL),
            ("Meta Intrinsic does not have an asm behavior !"));
  
  //  PU_Has_Asm = TRUE;

  CGTARG_Init_Asm_Constraints();

  // count nb of result to allocate the ASM_OP_ANNOT
  num_results=0;
  while (asm_results!=NULL && asm_results[num_results] != NULL) {
    full_asm_results[num_results] = asm_results[num_results];
    same_res[num_results]=-1;
    num_results++;
  }
  num_opnds=0;
  while (asm_operands!=NULL && asm_operands[num_opnds] != NULL) {
    const char* constraint = asm_operands[num_opnds];
    /* clobbered params must be added to res list with same_res
       positionned */
    if (constraint[0]=='&') {
      TN* new_tn = Build_TN_Like(wn_opnd[num_opnds]);
      Exp_COPY(new_tn, wn_opnd[num_opnds], ops);
      wn_opnd[num_opnds] = new_tn;

      //      constraint[0]='+';
      fprintf(stderr, "clobbered operand %d %s detected\n", num_opnds,
              constraint);
      FmtAssert((false), ("clobbered operand not supported yet !"));
      full_asm_results[num_results] = "=+&MP1x_VX"; //constraint;
       same_res[num_results]=num_opnds;
       num_results++;
    }
    num_opnds++;
  }
  full_asm_results[num_results]=NULL;

  ASM_OP_ANNOT* asm_info = Create_Empty_ASM_OP_ANNOT(num_results, wn_num_opnds);

  ASM_OP_wn(asm_info) = WN_CreateAsm_Stmt (2, asm_template);

  // process ASM clobber list
  // [vcdv] implementation of clobber list is costly. we need to
  // recognize a register via its name. This is done, for normal
  // asm-stmts, in the front-end and the corresponding tables are not
  // available in the be. Since we have no motivating case to
  // implement it. this is delayed.
  // using "dummy" results is the preferred way (it is also more
  // efficient since regalloc is done by the compiler).
  // note though that it implies in particular not using calls in the asm-stmt !
  FmtAssert((asm_clobbers==NULL),
            ("clobber list is not handled for asm-stmt in meta-instruction"));
  

  // process asm input parameters
  num_opnds = 0;
  while (asm_operands!=NULL && asm_operands[num_opnds] != NULL) {
    const char* constraint = asm_operands[num_opnds];
    TN* pref_tn = wn_opnd[num_opnds];
    ISA_REGISTER_SUBCLASS subclass = ISA_REGISTER_SUBCLASS_UNDEFINED;
    TN* tn = CGTARG_TN_For_Asm_Operand(constraint, NULL, pref_tn, &subclass);
    
    ASM_OP_opnd_constraint(asm_info)[num_opnds] = constraint+1;
    ASM_OP_opnd_subclass(asm_info)[num_opnds] = subclass;
    ASM_OP_opnd_position(asm_info)[num_opnds] = num_opnds+num_results;
    ASM_OP_opnd_memory(asm_info)[num_opnds] = 
      (strchr(constraint, 'm') != NULL);
    opnd[num_opnds] = tn;
    num_opnds++;
  }

  // process ASM output parameters:
  num_results = 0;
  while (full_asm_results!=NULL && full_asm_results[num_results] != NULL) {
    FmtAssert(num_results < MAX_RESULTS,
	      ("too may asm results in Handle_ASM"));
    const char* constraint = full_asm_results[num_results];


    FmtAssert((constraint!=NULL),
              ("asm_result[%d] cannot be NULL", num_results));
    TN* pref_tn = NULL;
    
    if (num_results<wn_num_results) {
      pref_tn = wn_result[num_results];
    }
    
    ISA_REGISTER_SUBCLASS subclass = ISA_REGISTER_SUBCLASS_UNDEFINED;
    
    TN* tn = NULL;

    /* api CGTARG_TN_For_Asm_Operand() does not work for boolean type
     * because pref_tn is of type MTYPE_I4 instead of required type
     * MTYPE_B.
     */
    if (strstr(constraint, "b") && pref_tn !=NULL &&
        TN_register_class(pref_tn)!=Register_Class_For_Mtype(MTYPE_B)) {
      tn = Build_TN_Of_Mtype(MTYPE_B);
      subclass = Register_Subclass_For_Mtype(MTYPE_B);
    } else {
      if (same_res[num_results] != -1) {
        tn = opnd[same_res[num_results]];
      } else {
        if (pref_tn == NULL || ! TN_is_dedicated(pref_tn))
          tn = CGTARG_TN_For_Asm_Operand(constraint, NULL, pref_tn, &subclass);
        else
          tn = Build_TN_Like(pref_tn);
      }
      /* for dedicated tn a new tn is created to avoid issues in
         EBO. it is not optimal though :-(. Note that similar behavior
         occurs on "normal" asm stmts. */
    }

    /* num_results>=wn_num_result means tmp variable that will be
       allocated using mtype in CGTARG_TN_For_Asm_Operand() */

    ASM_OP_result_constraint(asm_info)[num_results] = constraint;
    ASM_OP_result_subclass(asm_info)[num_results] = subclass;
    ASM_OP_result_position(asm_info)[num_results] = num_results;
    ASM_OP_result_clobber(asm_info)[num_results] = 
      (strchr(constraint, '&') != NULL);
    ASM_OP_result_memory(asm_info)[num_results] = 
      (strchr(constraint, 'm') != NULL);
    ASM_OP_result_same_opnd(asm_info)[num_results] = same_res[num_results];
    
    if (tn!=NULL && pref_tn!=NULL &&
        tn!=pref_tn) {
      /* [vcdv] for booleans (for v4 in fact), we need to import bug #47469
         fix */
      Exp_COPY(pref_tn, tn, &New_OPs);
    }

    result[num_results] = tn;
    num_results++;
  }
  // now create ASM op
  OP* asm_op = Mk_VarOP(TOP_asm, num_results, num_opnds, result, opnd);
  
  OPS_Append_Op(ops, asm_op);
  OP_MAP_Set(OP_Asm_Map, asm_op, asm_info);
  

  // TODO: Determine what ASM_livein/out are ?
  ASMINFO* info = TYPE_PU_ALLOC (ASMINFO);
  ISA_REGISTER_CLASS rc;
  FOR_ALL_ISA_REGISTER_CLASS(rc) {
    ASMINFO_livein(info)[rc] = REGISTER_SET_EMPTY_SET;
    ASMINFO_liveout(info)[rc] = REGISTER_SET_EMPTY_SET;
    ASMINFO_kill(info)[rc] = ASM_OP_clobber_set(asm_info)[rc];
  }
  FmtAssert((curbb!=NULL),
            ("No cur bb available for meta intrinsic expansion"));
  BB_Add_Annotation(curbb, ANNOT_ASMINFO, info);
  
  // forces new BB after asm stmt.
  Add_Label (Gen_Temp_Label());
  
  OPS_Append_Ops(ops, &New_OPs);
}

/*
 * Expansion of extension intrinsics.
 * - In case of pseudo intrinsic (compose, extract), generate generic COMPOSE/EXTRACT operation
 * Else
 * - Retrieve TOP associated with the intrinsic
 * - Add True predicate register to the list of operands
 * - Insure that immediate operands are stored in literal TN
 */
void
static Expand_Extension_intrinsic (
  INTRINSIC id,
  INT num_results,
  INT num_opnds,
  TN *result[],
  TN *opnd[],
  OPS *ops,
  SRCPOS srcpos,
  BB* curbb
  ) {
  int i;
  char error_msg[160];
  const ISA_OPERAND_INFO   *operand_info;
  TN *new_opnd[ISA_OPERAND_max_operands];
  TN *new_result[ISA_OPERAND_max_results];

  // Handle special intrinsics (compose, extract)
  if (EXTENSION_Is_Compose_INTRINSIC(id)) {
    Expand_Extension_Compose(id, num_results, num_opnds, result, opnd, ops);
    return;
  }
  else if (EXTENSION_Is_Partial_Compose_INTRINSIC(id)) {
    Expand_Extension_Partial_Compose(id, num_results, num_opnds, result, opnd, ops);
    return;
  }
  else if (EXTENSION_Is_Partial_Extract_INTRINSIC(id)) {
    Expand_Extension_Partial_Extract(id, num_results, num_opnds, result, opnd, ops);
    return;
  }
  else if (EXTENSION_Is_Convert_To_Pixel_INTRINSIC(id) ||
           EXTENSION_Is_Convert_From_Pixel_INTRINSIC(id)) {
    FmtAssert((num_results==1 && num_opnds==1),
              ("Pixel Convert Intrinsic must be of the form 1 result and 1 operands"));
    Expand_Copy(result[0], True_TN, opnd[0], ops);
    return;
  }
  else if (EXTENSION_Is_Convert_From_CType_INTRINSIC(id)) {
    EXTENSION_Expand_Copy_From_Core(result[0], True_TN, num_opnds, opnd, ops);
    return;
  }
  else if (EXTENSION_Is_Convert_To_CType_INTRINSIC(id)) {
    EXTENSION_Expand_Copy_To_Core(num_results, result, True_TN, opnd[0], ops);
    return;
  }
  else if (EXTENSION_Is_Meta_INTRINSIC(id)) {
    EXTENSION_Expand_Meta_Intrinsic(id,
                                    num_results, num_opnds,
                                    result, opnd, ops, srcpos, curbb);
    return;
  }

  // This point is reached only for standard TOP INTRINSICs

  // Add predicate operand
  memcpy(&new_opnd[1], opnd, num_opnds*sizeof(TN*));
  new_opnd[0] = True_TN;
  num_opnds++;

  // Retrieve TOP
  TOP top = EXTENSION_INTRINSIC_to_TOP(id);
  if (top == TOP_UNDEFINED) {
    DevWarn("No TOP found corresponding to INTRINSIC id '%d'\n", (int)id);
  }

  /*
    Insure that in/out operands references the same TN in both
    result and operand list
   */
  for (int idx=0; idx<num_results; idx++) {
    int same_opnd = TOP_Same_Res_Operand(top, idx);
    if ((same_opnd >= 0) &&
	(!TNs_Are_Equivalent(result[idx], new_opnd[same_opnd]))) {
      BOOL ok;
      ok = EXTENSION_Expand_Copy(result[idx], True_TN, new_opnd[same_opnd], ops);
      FmtAssert(ok, ("Failed to expand copy on extension register"));
      new_opnd[same_opnd] = result[idx];
    }
  }


  /*
    For immediate operands, it might be required to retrieve the value from
    rematerialization information.
    Range are automatically checked by Mk_VarOP.
    Additional surprises include that 0 is a special case that actually happens.
    The case value is written but does not seem to be triggered
  */
  operand_info = ISA_OPERAND_Info(top);
  for (i=0; i<num_opnds; i++) {
    const ISA_OPERAND_VALTYP *valtyp = ISA_OPERAND_INFO_Operand(operand_info, i);
    if (!ISA_OPERAND_VALTYP_Is_Literal(valtyp)) {
      continue;
    }
    
    TN *current_operand = new_opnd[i];    
    if (TN_is_rematerializable(current_operand)) {
      WN *wn = TN_remat(current_operand);
      if (WN_operator_is(wn, OPR_INTCONST)) {
	TN *cunknown = Gen_Literal_TN(WN_const_val(wn), 4);
	new_opnd[i] = cunknown;
      } else {
	DevWarn("%s::Expand_Extension_intrinsic:"
		" TN_is_rematerializable BUT WN_operator_is *not* OPR_INTCONST.", __FILE__);
      }
    }
    else if (TN_is_zero(current_operand)) {
      new_opnd[i] = Zero_TN;
    }
    else if (TN_has_value(current_operand)) {
      TN *cunknown = Gen_Literal_TN(TN_value(current_operand), 4);
      new_opnd[i] = cunknown;
    }
    else {
      sprintf(error_msg, "Immediate value expected for arg %d of intrinsic call '%s'",
	      i, TOP_Name(top));

      if (SRCPOS_linenum(srcpos)>0) {
	ErrMsgLine(EC_CG_Generic_Fatal, SRCPOS_linenum(srcpos), error_msg);
      }
      else {
	ErrMsg(EC_CG_Generic_Fatal, error_msg);
      }
    }

    // Check immediate value is in range
    ISA_LIT_CLASS lc = ISA_OPERAND_VALTYP_Literal_Class(valtyp);
    INT64 imm = TN_value(new_opnd[i]);
    if (!ISA_LC_Value_In_Class(imm, lc)) {
      sprintf(error_msg, "Immediate value '%lld' of arg %d out of bounds"
	      " for intrinsic call '%s'.",
	      imm, i, TOP_Name(top));
      if (SRCPOS_linenum(srcpos)>0) {
	ErrMsgLine(EC_CG_Generic_Fatal, SRCPOS_linenum(srcpos), error_msg);
      }
      else {
	ErrMsg(EC_CG_Generic_Fatal, error_msg);
      }
    }
  }
    
  OPS_Append_Op(ops, Mk_VarOP(top, num_results, num_opnds, result, new_opnd));
}
/* =============================================================================
 * =
 * =  END of dynamically added (extension) specific intrinsics handling
 * =
 * =============================================================================
 */

/*
@@@  case INTRN_XSHRW:
@@@    Expand__xshrw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
@@@  break ;
*/

#define INTRN_EXPAND
#include "gen_intrinsics.inc"
#undef INTRN_EXPAND

void
Exp_Intrinsic_Op (
  INTRINSIC id,
  INT num_results,
  INT num_opnds,
  TN *result[],
  TN *opnd[],
  OPS *ops,
  SRCPOS srcpos,
  BB* curbb
)
{
  switch (id) {
    case INTRN_BUILTIN__DIVUW:
      Expand__builtin__divuw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BUILTIN__DIVW:
      Expand__builtin__divw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BUILTIN__MODUW:
      Expand__builtin__moduw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BUILTIN__MODW:
      Expand__builtin__modw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_ABSCH:
      Expand__absch(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ABSCL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__abscl(result[1],result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__abscl(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_ABSCW:
      Expand__abscw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ABSH:
      Expand__absh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ABSL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__absl(result[1],result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__absl(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_ABSW:
      Expand__absw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ADDCH:
      Expand__addch(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_ADDCL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__addcl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__addcl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_ADDCW:
      Expand__addcw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_ADDL:
      FmtAssert(num_opnds == 4,("bad operand number"));
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__addl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__addl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_ADDUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__addul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__addul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_BITCLRH:
      Expand__bitclrh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITCLRW:
      Expand__bitclrw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITCNTH:
      Expand__bitcnth(result[0],opnd[0],ops) ;
    break ;
    case INTRN_BITCNTW:
      Expand__bitcntw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_BITNOTH:
      Expand__bitnoth(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITNOTW:
      Expand__bitnotw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITREVW:
      Expand__bitrevw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITSETH:
      Expand__bitseth(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITSETW:
      Expand__bitsetw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITVALH:
      Expand__bitvalh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_BITVALW:
      Expand__bitvalw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_CLAMPLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__clamplw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__clamplw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_CLAMPWH:
      Expand__clampwh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_DISTH:
      Expand__disth(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_DISTUH:
      Expand__distuh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_DISTUW:
      Expand__distuw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_DISTW:
      Expand__distw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_EDGESH:
      Expand__edgesh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_EDGESW:
      Expand__edgesw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_EQL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__eql(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__eql(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_EQUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__equl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__equl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_GEL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__gel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__gel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_GETHH:
      Expand__gethh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_GETHW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__gethw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__gethw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_GETLH:
      Expand__getlh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_GETLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__getlw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__getlw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_GEUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__geul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__geul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_GTL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__gtl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__gtl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_GTUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__gtul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__gtul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_INSEQUW:
      Expand__insequw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSEQW:
      Expand__inseqw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSGEUW:
      Expand__insgeuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSGEW:
      Expand__insgew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSGTUW:
      Expand__insgtuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSGTW:
      Expand__insgtw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSLEUW:
      Expand__insleuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSLEW:
      Expand__inslew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSLTUW:
      Expand__insltuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSLTW:
      Expand__insltw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSNEUW:
      Expand__insneuw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_INSNEW:
      Expand__insnew(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_LEL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__lel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__lel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_LEUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__leul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__leul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_LTL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__ltl(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__ltl(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_LTUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__ltul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__ltul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_LZCNTH:
      Expand__lzcnth(result[0],opnd[0],ops) ;
    break ;
    case INTRN_LZCNTL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__lzcntl(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__lzcntl(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_LZCNTW:
      Expand__lzcntw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_MAFCW:
      Expand__mafcw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_MAXH:
      Expand__maxh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MAXL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__maxl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__maxl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MAXUH:
      Expand__maxuh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MAXUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__maxul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__maxul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MAXUW:
      Expand__maxuw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MAXW:
      Expand__maxw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MINH:
      Expand__minh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MINL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__minl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__minl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MINUH:
      Expand__minuh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MINUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__minul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__minul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MINUW:
      Expand__minuw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MINW:
      Expand__minw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MPFCW:
      Expand__mpfcw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MPFCWL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mpfcwl(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__mpfcwl(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MPFML:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mpfml(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__mpfml(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MPFRCH:
      Expand__mpfrch(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MPML:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mpml(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__mpml(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MPUML:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mpuml(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__mpuml(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MULFCH:
      Expand__mulfch(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULFCM:
      Expand__mulfcm(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULFCW:
      Expand__mulfcw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULH:
      Expand__mulh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULHH:
      Expand__mulhh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULHUH:
      Expand__mulhuh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULHUW:
      Expand__mulhuw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULHW:
      Expand__mulhw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mull(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__mull(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MULM:
      Expand__mulm(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULN:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__muln(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__muln(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MULUH:
      Expand__muluh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mulul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__mulul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_MULUM:
      Expand__mulum(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULUN:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__mulun(result[1],result[0],opnd[0],opnd[1],ops) ;
    } else { 
      Expand__mulun(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_MULUW:
      Expand__muluw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_MULW:
      Expand__mulw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_NEARCLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__nearclw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__nearclw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NEARCWH:
      Expand__nearcwh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_NEARLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__nearlw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__nearlw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NEARWH:
      Expand__nearwh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_NEGCH:
      Expand__negch(result[0],opnd[0],ops) ;
    break ;
    case INTRN_NEGCL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__negcl(result[1],result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__negcl(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NEGCW:
      Expand__negcw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_NEGL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__negl(result[1],result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__negl(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NEGUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__negul(result[1],result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__negul(result[0],result[1],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NEL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__nel(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__nel(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_NEUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__neul(result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__neul(result[0],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_NORMH:
      Expand__normh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_NORML:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__norml(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__norml(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_NORMW:
      Expand__normw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_PRIORH:
      Expand__priorh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_PRIORL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__priorl(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__priorl(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_PRIORW:
      Expand__priorw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_PUTHL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__puthl(result[1],result[0],opnd[0],ops) ;
    } else { 
      Expand__puthl(result[0],result[1],opnd[0],ops) ;
    }  
    break ;
    case INTRN_PUTHW:
      Expand__puthw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_PUTLL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__putll(result[1],result[0],opnd[0],ops) ;
    } else { 
      Expand__putll(result[0],result[1],opnd[0],ops) ;
    }  
    break ;
    case INTRN_PUTLW:
      Expand__putlw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ROTLH:
      Expand__rotlh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_ROTLW:
      Expand__rotlw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_ROUNDCLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__roundclw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__roundclw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_ROUNDCWH:
      Expand__roundcwh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_ROUNDLW:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__roundlw(result[0],opnd[1],opnd[0],ops) ;
    } else { 
      Expand__roundlw(result[0],opnd[0],opnd[1],ops) ;
    }  
    break ;
    case INTRN_ROUNDWH:
      Expand__roundwh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_SHLCH:
      Expand__shlch(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHLCW:
      Expand__shlcw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHLL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__shll(result[1],result[0],opnd[1],opnd[0],opnd[2],ops) ;
    } else { 
      Expand__shll(result[0],result[1],opnd[0],opnd[1],opnd[2],ops) ;
    }  
    break ;
    case INTRN_SHLW:
      Expand__shlw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHRL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__shrl(result[1],result[0],opnd[1],opnd[0],opnd[2],ops) ;
    } else { 
      Expand__shrl(result[0],result[1],opnd[0],opnd[1],opnd[2],ops) ;
    }  
    break ;
    case INTRN_SHRRH:
      Expand__shrrh(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHRRW:
      Expand__shrrw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHRUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__shrul(result[1],result[0],opnd[1],opnd[0],opnd[2],ops) ;
    } else { 
      Expand__shrul(result[0],result[1],opnd[0],opnd[1],opnd[2],ops) ;
    }  
    break ;
    case INTRN_SHRUW:
      Expand__shruw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SHRW:
      Expand__shrw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SUBCH:
      Expand__subch(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SUBCL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__subcl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__subcl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_SUBCW:
      Expand__subcw(result[0],opnd[0],opnd[1],ops) ;
    break ;
    case INTRN_SUBL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__subl(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__subl(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_SUBUL:
    if (Target_Byte_Sex == BIG_ENDIAN) {
      Expand__subul(result[1],result[0],opnd[1],opnd[0],opnd[3],opnd[2],ops) ;
    } else { 
      Expand__subul(result[0],result[1],opnd[0],opnd[1],opnd[2],opnd[3],ops) ;
    }  
    break ;
    case INTRN_SWAPBH:
      Expand__swapbh(result[0],opnd[0],ops) ;
    break ;
    case INTRN_SWAPBW:
      Expand__swapbw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_SWAPHW:
      Expand__swaphw(result[0],opnd[0],ops) ;
    break ;
    case INTRN_XSHLH:
      Expand__xshlh(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_XSHLW:
      Expand__xshlw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_XSHRH:
      Expand__xshrh(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_XSHRW:
      Expand__xshrw(result[0],opnd[0],opnd[1],opnd[2],ops) ;
    break ;
    case INTRN_KILL:
      /* Note: KILL operand is ignored. It is no more required in CGIR */
      Expand__KILL(result, num_results, ops);
    break;
#define INTRN_SWITCH
#include "gen_intrinsics.inc"
#undef INTRN_SWITCH
    default:
      if (EXTENSION_Is_Extension_INTRINSIC(id)) {
	Expand_Extension_intrinsic(id, num_results, num_opnds, result, opnd, ops, srcpos, curbb);
      }
      else {
	FmtAssert (FALSE, ("Exp_Intrinsic_Op: unknown intrinsic op %s", INTRN_c_name(id)));
      }
  } /* switch*/
}
