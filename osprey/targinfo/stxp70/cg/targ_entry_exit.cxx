/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/

#include <set>

#include "defs.h"
#include "erglob.h"
#include "erbe.h"
#include "glob.h"
#include "tracing.h"
#include "config_target.h"
#include "config.h"
#include "config_TARG.h"

#include "symtab.h"
#include "strtab.h"
#include "be_symtab.h"
#include "targ_isa_lits.h"
#include "topcode.h"
#include "cgir.h"
#include "tn_set.h"
#include "tn_map.h"

#include "ttype.h"
#include "const.h"
#include "data_layout.h"
#include "targ_sim.h"
#include "whirl2ops.h"
#include "entry_exit_targ.h"
#include "targ_abi_properties.h"

#include "cg_flags.h"
#include "cg.h"
#include "calls.h"
#include "register.h"
#include "cgexp.h"
#include "cg_spill.h"
#include "cgtarget.h"
#include "mexpand.h"
#include "reg_live.h"
#include "bb.h"
#include "config_TARG.h"
#include "ercg.h"

#include "lai_loader_api.h"
#include "exp_extension.h"

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

#define IS_POWER_OF_2(val)      ((val != 0) && ((val & (val-1)) == 0))

// We cannot use the value defined by DEFAULT_STACK_ALIGNMENT, since it can be
// changed on the command line
#define DEFAULT_SUPPORTED_STACK_ALIGNMENT 8
#define SIZEOF_CORE_REG_IN_BYTE 4

#define GPR_FOR_SP_ADJUST   (REGISTER_MIN + 6)  // R6
#define GPR_FOR_STACKALIGN1 (REGISTER_MIN + 6)  // R6
#define GPR_FOR_STACKALIGN2 (REGISTER_MIN + 7)  // R7

typedef std::vector<CLASS_REG_PAIR> ClassRegPairVector;
typedef ClassRegPairVector::const_iterator CItClassRegPairVector;

/**
 * Class reg pair and info.
 * This class holds the information on alignment and current padding of a class
 * reg pair.
 */
struct ClassRegPairAndInfo
{
    /**
     * Class reg pair member
     */
    CLASS_REG_PAIR regPair;

    /**
     * Padding number. Represents the number of bytes used to align the stack
     * before pushing regPair
     */
    UINT padding;

    /**
     * Date alignment constraint. Data alignment contraint in bytes used to
     * push regPair
     */
    UINT alignment;

    /**
     * Constructor.
     * Initialize all data members
     */
    ClassRegPairAndInfo(const CLASS_REG_PAIR& a_regPair, UINT a_padding,
                           UINT a_alignment)
    {
        regPair = a_regPair;
        padding = a_padding;
        alignment = a_alignment;
    }

    /**
     * Default constructor
     */
    ClassRegPairAndInfo()
    {
        Set_CLASS_REG_PAIR_rclass(regPair, ISA_REGISTER_CLASS_UNDEFINED);
        Set_CLASS_REG_PAIR_reg(regPair, REGISTER_UNDEFINED);
        padding = 0;
        alignment = 0;
    }
};

typedef std::list<ClassRegPairAndInfo> ClassRegPairAndInfoList;
typedef ClassRegPairAndInfoList::const_iterator CItClassRegPairAndInfoList;

/**
 * Final adjustment need to have save reg area aligned on
 * PU_aligned_stack(Get_Current_PU())
 */
static UINT finalAdjustment = 0;


static ClassRegPairVector callee_saved_vector;
static ClassRegPairAndInfoList not_pushed_callee_saved_vector;
static ClassRegPairVector caller_saved_vector;
static ClassRegPairAndInfoList not_pushed_caller_saved_vector;
static INT push_pop_rh_mask;
static INT push_pop_rl_mask;
static INT push_pop_sh_mask;
static ST *first_st_save, *last_st_save;
static INT64 push_area_size, upper_size, lower_size, frame_size;
static BOOL has_fp_adjust;

// [JV] Flag to mark if GP is restored
static BOOL GP_is_restored;

// [VL] Flag to mark if GP is set for PIC/PID
static BOOL GP_is_set_for_pic;

/**
 * Structure used to order classes reg pair.
 */
struct CompareRegClass
{
    /**
     * Two objects x and y are equivalent if both f(x, y) and f(y, x) are
     * false. Note that an object is always (by the irreflexivity invariant)
     * equivalent to itself.
     */
    bool operator()(const CLASS_REG_PAIR& a, const CLASS_REG_PAIR& b) const
    {
      return (!(CLASS_REG_PAIR_rclass(a) == ISA_REGISTER_CLASS_gr &&
                CLASS_REG_PAIR_rclass(a) == CLASS_REG_PAIR_rclass(b))) &&
        (CLASS_REG_PAIR_class_n_reg(a) < CLASS_REG_PAIR_class_n_reg(b));
    }
};

typedef std::set<CLASS_REG_PAIR, CompareRegClass> SetOfRegClass;
typedef SetOfRegClass::const_iterator CItSetOfRegClass;

/**
 * Get space in bytes used by rclass when push on stack.
 *
 * @param  rclass 
 *
 * @pre    rclass is a valid ISA_REGISTER_CLASS
 * @post   true
 *
 * @return The maximum size (takes the greatest size for extension register)
 *         used by rclass when pushed on stack
 */
static INT
Get_Reg_Class_Max_Bytes_Size_For_Sp(ISA_REGISTER_CLASS rclass)
{
    INT res = EXTENSION_get_REGISTER_CLASS_max_bit_size(rclass) / CHAR_BIT;

    return res? res: 4; 
}

/* ====================================================================
 *   EETARG_Build_Jump_Instead_Of_Call (call_op)
 * ====================================================================
 */
OP *
EETARG_Build_Jump_Instead_Of_Call (
  OP *call_op
)
{
  OP *jump_op;
  TOP jump_top;
  TOP call_top = OP_code(call_op);
  if( is_TOP_callr(call_top)) {
    jump_op = Mk_OP(TOP_jr, OP_opnd(call_op, 0), OP_opnd(call_op, 1));
  }
  else if(is_TOP_calla(call_top)) {
    jump_op = Mk_OP(TOP_ja, OP_opnd(call_op, 0), OP_opnd(call_op, 1));
  }
  else {
    FmtAssert(FALSE, ("don't know how to generate tail call for %s",
		     TOP_Name(call_top)));
    /*NOTREACHED*/
  }

  return jump_op;
}


/* ====================================================================
 * EETARG_restore_gp_from_symbol
 * Restore GP TN from global symbol __GP_BASE
 * ====================================================================
 */
static void EETARG_restore_gp_from_symbol( BB *bb ) {

  FmtAssert(BB_entry(bb),("BB not entry block"));

  // [JV] Restore only once GP.
  if(GP_is_restored) return;

  OPS ops_gp = OPS_EMPTY;
  OP *point = NULL;
  TN *tmp_tn, *tmp_tn1, *tmp_tn2;
  ST *st = New_ST (GLOBAL_SYMTAB);
  TY_IDX ty = MTYPE_To_TY(Pointer_Mtype);
  ST_Init (st, Save_Str("__GP_BASE"), CLASS_VAR
	   , SCLASS_EXTERN, EXPORT_PREEMPTIBLE, ty);
  tmp_tn = Gen_Symbol_TN(st, 0, 0);
  Build_OP(TOP_MAKE, GP_TN, True_TN, tmp_tn, &ops_gp);
  point = BB_first_op(bb);
  if (point == NULL) {
    BB_Append_Ops (bb, &ops_gp);
  }
  else {
    if(BB_entry_sp_adj_op(bb)) {
      // [JV] GP must not be restored before spadjust.
      point = OP_next(BB_entry_sp_adj_op(bb));
    }
    BB_Insert_Ops_Before(bb, point, &ops_gp);
  }

  // [JV] Mark GP restored.
  GP_is_restored = TRUE;
}

/* ====================================================================
 * EETARG_set_gp_for_pic
 * Dynamic loading. Set GP for PIC/PID using an addur instruction
 * ====================================================================
 */
static void EETARG_set_gp_for_pic( BB *bb ) {

  FmtAssert(BB_entry(bb),("BB not entry block"));

  // [VL] Set GP only once in PIC mode.
  if(GP_is_set_for_pic) return;

  OPS ops_gp   = OPS_EMPTY;
  OP *point    = NULL;
  OP *op       = NULL;

  static INT Temp_Index = 0;
  TN     *gpinit_label_tn;

  // [VL] We create a symbol to be used for the initialization of GP. 
  // The choixe of a symbol rather than a label is to ease emission 
  // with relocation, and make code closer to the one used on ST200.
  ANNOTATION *ant     = ANNOT_Get (BB_annotations(bb), ANNOT_ENTRYINFO);
  ENTRYINFO *ent_info = ANNOT_entryinfo(ant);
  ST *fn_st           = ENTRYINFO_name(ent_info);  

  ST *st              = New_ST (CURRENT_SYMTAB);
  STR_IDX str_idx     = Save_Str2i (".L_", "gpinit_", Temp_Index++);
  ST_Init (st, str_idx, CLASS_NAME, SCLASS_UNKNOWN, EXPORT_LOCAL, ST_pu (fn_st));
  gpinit_label_tn     = Gen_Symbol_TN (st, 0, 0);

  // [VL] The TOP_ADDUR_LABEL is a simulated op, which will be later
  // expanded later on as pseudo label (symbol in fact) + addur. This 
  // solution is derived from the one used for ST200, on which a call 
  // is also needed.
  // See Exp_Simulated_Op in exp_targ.cxx for the expansion of this
  // simulated op.
  Build_OP(TOP_ADDUR_LABEL, GP_TN, True_TN, gpinit_label_tn, &ops_gp);

  // The insertion is made afer sp_adjust if needed
  point = BB_first_op(bb);
  if (point == NULL) {
    BB_Append_Ops (bb, &ops_gp);
  } else {
    if(BB_entry_sp_adj_op(bb)) {
      // [VL] GP must not be restored before spadjust.
      point = OP_next(BB_entry_sp_adj_op(bb));
    }
    BB_Insert_Ops_Before(bb, point, &ops_gp);
  }

  // [VL] Mark GP set for PIC.
  GP_is_set_for_pic = TRUE;
}

/* ====================================================================
 * init_callee_saved
 * Build the list of callee-saved registers to be saved/restored
 * ====================================================================
 */
static void
init_callee_saved(SetOfRegClass& alreadySaved)
{
  callee_saved_vector.clear();
  not_pushed_callee_saved_vector.clear();
  INT callee_num;

  for (callee_num = 0; callee_num < Callee_Saved_Regs_Count; callee_num++) {
    TN *tn = CALLEE_tn(callee_num);
    ISA_REGISTER_CLASS cl = TN_save_rclass(tn);
    REGISTER reg = TN_save_reg(tn);
    CLASS_REG_PAIR rp = TN_save_creg(tn);

    if (cl > ISA_REGISTER_CLASS_STATIC_MAX) {
      // Use the hw numbering to know the alignment. Since we save only the
      // uppermost level, we put in the vector only aligned register
      // bug #51407
      int regAlignment = EXTENSION_get_REGISTER_CLASS_max_bit_size(cl) /
        ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(cl));
      if ((reg - REGISTER_MIN) % regAlignment != 0) {
        reg = reg - ((reg - REGISTER_MIN) % regAlignment);
        Set_CLASS_REG_PAIR_reg(rp, reg);
      }
    }

    if (EETARG_Save_With_Regmask (cl, reg) &&
	REGISTER_SET_MemberP(Callee_Saved_Regs_Mask[cl], reg) &&
        alreadySaved.find(TN_save_creg(tn)) == alreadySaved.end()) {

      callee_saved_vector.push_back(rp);
      alreadySaved.insert(rp);
    }

    // [VL] Dynamic loading - If we generate code for PIC/PID, then 
    // GP needs to be pushed/poped in case the PU uses GP only
    // [VCDV] in case of call, GP may be used implicitely.
    if (GOT_Model!=got_none &&
        cl == TN_register_class(GP_TN) &&
        (PU_References_GP || PU_Has_Calls) &&
        alreadySaved.find(TN_class_reg(GP_TN)) == alreadySaved.end()) {
      callee_saved_vector.push_back(TN_class_reg(GP_TN));
      alreadySaved.insert(TN_class_reg(GP_TN));
    }
  }
  if (RA_TN != NULL &&
      EETARG_Save_With_Regmask (REGISTER_CLASS_ra, REGISTER_ra) &&
      REGISTER_SET_MemberP(Callee_Saved_Regs_Mask[REGISTER_CLASS_ra], 
                           REGISTER_ra) &&
      alreadySaved.find(TN_class_reg(RA_TN)) == alreadySaved.end()) {
    callee_saved_vector.push_back(TN_class_reg(RA_TN));
    alreadySaved.insert(TN_class_reg(RA_TN));
  }

  if (saveHWloopRegisterLR0) { //hwloop LR0 used
      DevAssert(Core_Has_HWLoop, ("If LR0 is used, core cfg must have hardware"
                                  "loop"));
    int i;
    // add 4 sfr registers corresponding to LR0 macro register.
    for (i=0; i<4; i++)
      {
	if (! EETARG_Save_With_Regmask (ISA_REGISTER_CLASS_sfr,
					i+REGISTER_MIN+HWloopRegisterLR0SFRRank))
	    continue;
        CLASS_REG_PAIR pair;
        Set_CLASS_REG_PAIR_reg(pair, i+REGISTER_MIN+HWloopRegisterLR0SFRRank);
        Set_CLASS_REG_PAIR_rclass(pair, ISA_REGISTER_CLASS_sfr);
        if(alreadySaved.find(pair) == alreadySaved.end())
            {
                callee_saved_vector.push_back(pair);
                alreadySaved.insert(pair);
            }
      }
  }

}

/* ====================================================================
 * init_caller_saved
 * Build the list of caller-saved registers to be saved/restored
 * Notice that in case we are not in an IT routine, nothing is done
 * ====================================================================
 */
static void
init_caller_saved(SetOfRegClass& alreadySaved)
{
  caller_saved_vector.clear();
  not_pushed_caller_saved_vector.clear();

  // If not an interruption routine: nothing to do!
  if (!PU_is_interrupt(Get_Current_PU()) &&
      !PU_is_interrupt_nostkaln(Get_Current_PU())) {
    return;
  }

  ISA_REGISTER_CLASS cl;
  REGISTER_SET scratchs_used[ISA_REGISTER_CLASS_MAX+1];
  REGISTER_SET avails_scratchs[ISA_REGISTER_CLASS_MAX+1];
    
  FOR_ALL_ISA_REGISTER_CLASS(cl) {
    if(!((cl == ISA_REGISTER_CLASS_fpr && !Enable_Fpx) ||
          cl == ISA_REGISTER_CLASS_fsr ||
          cl == ISA_REGISTER_CLASS_gecr ||
          cl == ISA_REGISTER_CLASS_itcr ||
          cl == ISA_REGISTER_CLASS_sfr)) {
      scratchs_used[cl] = REGISTER_CLASS_caller_saves(cl);
      REGISTER member;

      // [CR] if no calls, just used scratchs have to be saved
      //      otherwise, all scratchs have to be saved.
      // [VL] GP is no longer saved since R6 is now used in the
      //      stack adjustement sequences instead.

      if (!PU_Has_Calls) {
        BB * bb;
        REG_LIVE_Analyze_Region();
        scratchs_used[cl] = REGISTER_SET_EMPTY_SET;
        for (bb = REGION_First_BB; bb != NULL; bb = BB_next(bb)) {
          REG_LIVE_Prolog_Temps(bb, BB_first_op(bb), BB_last_op(bb), avails_scratchs);
          scratchs_used[cl] = REGISTER_SET_Union(scratchs_used[cl],
                                  REGISTER_SET_Difference(REGISTER_CLASS_caller_saves(cl),
                                                          avails_scratchs[cl]));
        }
        REG_LIVE_Finish();
      }

      int regAlignment = 1;
      if(cl > ISA_REGISTER_CLASS_STATIC_MAX) {
        regAlignment = EXTENSION_get_REGISTER_CLASS_max_bit_size(cl) /
          ISA_REGISTER_CLASS_INFO_Bit_Size(ISA_REGISTER_CLASS_Info(cl));
      }
    
      /* VL: on STxP70, the so called "link register" LK/R14 is used to store the    */
      /* return address. ABI states that it is reserved. In Open64, it is declared   */
      /* as both callee and caller saved, and has the ret_addr ABI property.         */
      /* Thus on a general basis, it is part of register to be pushed / poped in IT  */
      /* Though; if IT has no call, it is not necessary and it can be removed  from  */
      /* the set. We use its ret_addr property to detect and remove it               */
      if (!PU_Has_Calls && ABI_PROPERTY_Has_ret_addr_Registers(cl)) {
        REGISTER ret_addr_reg = REGISTER_UNDEFINED;

        for (member = REGISTER_SET_Choose(scratchs_used[cl]);
             member != REGISTER_UNDEFINED;
             member = REGISTER_SET_Choose_Next(scratchs_used[cl],member)) {
          if (ABI_PROPERTY_Is_ret_addr(cl, member-REGISTER_MIN)) {
              ret_addr_reg = member;
              break;
          }
        }

        if (ret_addr_reg!=REGISTER_UNDEFINED) {
          scratchs_used[cl] = REGISTER_SET_Difference1(scratchs_used[cl], ret_addr_reg);
        }
      }

      for (member = REGISTER_SET_Choose(scratchs_used[cl]);
           member != REGISTER_UNDEFINED;
           member = REGISTER_SET_Choose_Next(scratchs_used[cl],member)) {
        if (cl == ISA_REGISTER_CLASS_lc) {
          if(Core_Has_HWLoop) {
            // [CR] loop register L1
            int i ;
            for (i = 0 ; i < 4 ; i++) {
              CLASS_REG_PAIR pair;
              Set_CLASS_REG_PAIR_reg(pair, i+REGISTER_MIN+HWloopRegisterLR0SFRRank+4);
              Set_CLASS_REG_PAIR_rclass(pair, ISA_REGISTER_CLASS_sfr);
              if(alreadySaved.find(pair) == alreadySaved.end()) {
                caller_saved_vector.push_back(pair);
                alreadySaved.insert(pair);
              }
            }
          } // Core_Has_HWLoop
        } else {
          CLASS_REG_PAIR pair;
          Set_CLASS_REG_PAIR_reg(pair, member);
          Set_CLASS_REG_PAIR_rclass(pair, cl);

          // Use the hw numbering to know the alignment. Since we save only the
          // uppermost level, we put in the vector only aligned register
          // bug #51407
          if ((member - REGISTER_MIN) % regAlignment != 0) {
            Set_CLASS_REG_PAIR_reg(pair, member - ((member - REGISTER_MIN) % regAlignment));
          }
          if(alreadySaved.find(pair) == alreadySaved.end()) {
            caller_saved_vector.push_back(pair);
            alreadySaved.insert(pair);
          }
        }
      } // for (member... )
    } // if(!((cl ...
  } // FOR_ALL_ISA_REGISTER_CLASS(cl)
  
}

/**
 * Get the needed number of padding bytes for given regPair to match alignment
 * constraint according to current value of stackSize.
 * The alignment constraint is defined like this:
 * @li Default is the alignment of the stack
 * @li if rclass is an extension and its mtype alignment is
 *     DEFAULT_SUPPORTED_STACK_ALIGNMENT then takes the minimum between
 *     the max size in bytes of a register of that class when pushed on sp and
 *     the stack alignment
 * @li if rclass is an extension and its mtype alignment is not
 *     DEFAULT_SUPPORTED_STACK_ALIGNMENT then takes the minimum between
 *     that value and the stack alignment
 *
 * @param  stackSize Size of the stack before pushing given regPair
 * @param  regPair regPair for which we want the padding size to be placed
 *         before
 * @param  requireDataAlignment [out] Will contain the result of constraint
 *         alignment
 *
 * @pre    true
 * @post   (stackSize + result) % requireDataAlignment == 0
 *
 * @return The number of bytes to be added before pushing regPair to match data
 *         alignment constraint
 */
static UINT
GetPaddingOffset(UINT stackSize, const CLASS_REG_PAIR& regPair,
                 UINT& requireDataAlignment)
{
    ISA_REGISTER_CLASS rclass = CLASS_REG_PAIR_rclass(regPair);
    INT byteSize = Get_Reg_Class_Max_Bytes_Size_For_Sp(rclass);
    requireDataAlignment = PU_aligned_stack(Get_Current_PU());

    if(rclass > ISA_REGISTER_CLASS_STATIC_MAX)
        {
            TYPE_ID typeId = EXTENSION_REGISTER_CLASS_to_MTYPE(rclass,
                                                               byteSize);
            UINT mtypeAlignment = MTYPE_alignment(typeId);
            if(mtypeAlignment == DEFAULT_SUPPORTED_STACK_ALIGNMENT)
                {
                    requireDataAlignment =
                        MIN(requireDataAlignment, byteSize);
                }
            else
                {
                    requireDataAlignment =
                        MIN(requireDataAlignment, mtypeAlignment);
                }
        }
    else
        {
            // We know that statically known registers have loads that
            // work on their size
            requireDataAlignment = byteSize;
        }

    UINT offset = 0;
    if(stackSize % requireDataAlignment != 0)
        {
            offset = requireDataAlignment -
                (stackSize % requireDataAlignment);
        }
    return offset;
}

/**
 * Set push pop mask (rl, rh, sh) for given savedVector.
 * All registers not pushed via these masks, will be stored in notPushedVector
 *
 * bug #51134 : as savedVector is not sorted with core register first,
 * it is mandatory to parse the core registers in a first loop and
 * then the extension registers so as to guarantee proper padding
 * computations. 
 *
 *
 * @param  savedVector List of register to be pushed
 * @param  notPushedVector [in/out] List of register not pushed by the setting
 *         masks
 * @param  stackSize [in] Size of the stack before pushing registers
 *                   [out] Size of the stack after pushing registers.
 *                   Note: The out stack considered that all registers in
 *                   notPushedVector have been pushed
 *
 * @pre    true
 * @post   stackSize represents the size of the stack after applying all push
 *         mask followed by pushing all notPushedVector elements. The
 *         information contained in notPushedVector are correct only if you
 *         push them in the right order (begin to end). Same for stackSize
 *
 */
static void
SetPushPopMask(const ClassRegPairVector& savedVector,
               ClassRegPairAndInfoList& notPushedVector, UINT& stackSize)
{
    CItClassRegPairVector it;
    INT tmp_push_pop_sh_mask = push_pop_sh_mask;
    INT tmp_push_pop_rl_mask = push_pop_rl_mask;
    INT tmp_push_pop_rh_mask = push_pop_rh_mask;
    BOOL extensionRegisterPresent = false;

    /* core registers only */
    for (it = savedVector.begin(); it != savedVector.end(); ++it)
        {
            CLASS_REG_PAIR reg_pair = *it;
            ISA_REGISTER_CLASS rclass = CLASS_REG_PAIR_rclass(reg_pair);
            REGISTER           reg =    CLASS_REG_PAIR_reg(reg_pair);

            switch(rclass)
                {
                case ISA_REGISTER_CLASS_gpr:
                    if(reg > 16)
                        {
                            FmtAssert(reg <= 32,
                                      ("register gpr greater than 32"));
                            push_pop_rh_mask |= 1 << (reg-REGISTER_MIN-16);
                        }
                    else
                        {
                            FmtAssert(!ABI_PROPERTY_Is_stack_ptr(rclass, reg-REGISTER_MIN),("Cannot push sp"));
                            push_pop_rl_mask |= 1 << (reg-REGISTER_MIN);
                        }
                    break;
                case ISA_REGISTER_CLASS_gr:
                    push_pop_rl_mask |= 1 << 15;
                    break;
                case ISA_REGISTER_CLASS_sfr:
                    FmtAssert(reg <= 32,("register sfr greater than 32"));
                    push_pop_sh_mask |= 1 << (reg-REGISTER_MIN-16);
                    break;
                default:
                  // extension register
                  extensionRegisterPresent = true;
                }
	    if(tmp_push_pop_sh_mask != push_pop_sh_mask ||
	       tmp_push_pop_rl_mask != push_pop_rl_mask ||
	       tmp_push_pop_rh_mask != push_pop_rh_mask)
	      {
		// stackSize is updated only if current register
		// was not already pushed on stack
		tmp_push_pop_sh_mask = push_pop_sh_mask;
		tmp_push_pop_rl_mask = push_pop_rl_mask;
		tmp_push_pop_rh_mask = push_pop_rh_mask;
		stackSize += Get_Reg_Class_Max_Bytes_Size_For_Sp(rclass);
	      }
        }

    if (extensionRegisterPresent) {
     /* extension registers only */
     for (it = savedVector.begin(); it != savedVector.end(); ++it)
        {
          CLASS_REG_PAIR reg_pair = *it;
          ISA_REGISTER_CLASS rclass = CLASS_REG_PAIR_rclass(reg_pair);
          REGISTER           reg =    CLASS_REG_PAIR_reg(reg_pair);
          
          switch(rclass)
            {
            case ISA_REGISTER_CLASS_gpr:
            case ISA_REGISTER_CLASS_gr:
            case ISA_REGISTER_CLASS_sfr:
              break;
            default:
              {
                UINT requireDataAlignment;
                UINT padding = GetPaddingOffset(stackSize, reg_pair,
                                                requireDataAlignment);
                notPushedVector.push_back
                  (ClassRegPairAndInfo(reg_pair, padding,
                                       requireDataAlignment));
                stackSize += padding;
                stackSize += Get_Reg_Class_Max_Bytes_Size_For_Sp(rclass);
              }
            }
        }
    }
}

/**
 * Set all extensions registers as not available.
 *
 * @param  availsTemps Array of REGISTER_SET
 *
 * @pre    availsTemps && availsTemps->size() >= ISA_REGISTER_CLASS_MAX + 1
 * @post   all REGISTER_SET representing extension register class are empty
 *
 */
static void
InitAvailTemps(REGISTER_SET* availsTemps)
{
    int i = 0;
    for(i = ISA_REGISTER_CLASS_STATIC_MAX + 1; i < ISA_REGISTER_CLASS_MAX + 1;
        ++i)
        {
            availsTemps[i] = REGISTER_SET_EMPTY_SET;
        }
}

/**
 * For debug purpose. Print all available registers
 */
static void
DispAvailRegs(const REGISTER_SET*const availsTemps)
{
    int i;
    printf("-----------\n");
    for(i = ISA_REGISTER_CLASS_MIN; i < ISA_REGISTER_CLASS_MAX + 1; ++i)
        {
            REGISTER reg;
            printf("%s: ",
                   ISA_REGISTER_CLASS_INFO_Name(ISA_REGISTER_CLASS_Info(i)));
            for(reg = REGISTER_SET_Choose(availsTemps[i]);
                reg != REGISTER_UNDEFINED;
                reg = REGISTER_SET_Choose_Next(availsTemps[i], reg))
                {
                    printf("%d, ", reg);
                }
            printf("\n");
        }
}

/**
 * Generate a dedicated TN according to reg_pair, which size in the maximum
 * possible size for that register class.
 *
 * @param  reg_pair Class reg pair for which we want to generate a TN
 * @param  byteSize [out] Size of the generated TN
 *
 * @pre    reg_pair is a valid class register pair
 * @post   TN_is_dedicated(result) and TN_register(result) =
 *         CLASS_REG_PAIR_reg(reg_pair) and TN_register_class(result) =
 *         CLASS_REG_PAIR_rclass(reg_pair) and TN_size(result) = byteSize
 *
 * @return The generated dedicated TN
 */
static TN*
GenMaxTNForClassRegPair(const CLASS_REG_PAIR& reg_pair, INT& byteSize)
{
    ISA_REGISTER_CLASS rclass = CLASS_REG_PAIR_rclass(reg_pair);
    REGISTER reg = CLASS_REG_PAIR_reg(reg_pair);

    byteSize = Get_Reg_Class_Max_Bytes_Size_For_Sp(rclass);

    return Build_Dedicated_TN(rclass,reg,byteSize);
}

/**
 * Single push all elements of pushVector in ops_push using given availsTemps
 * as available register. Pushes sequence is emitted according begin to end
 * order of pushVector. Before each push the padding information of the current
 * element is applied to SP
 *
 * @param  pushVector Class reg pair to be pushed, with related useful
 *                    information
 * @param  ops_push List of operations
 * @param  availTemps Array of available registers
 *
 * @pre    availTemps && ops_push
 * @post   ops_push = ops_push@pre->append(all single pushes of pushVector
 *         with the related padding)
 *
 */
static void
ExpandSinglePushes(const ClassRegPairAndInfoList& pushVector, OPS* ops_push,
                   REGISTER_SET* availTemps)
{
    CItClassRegPairAndInfoList it;
    for(it = pushVector.begin(); it != pushVector.end(); ++it)
        {
            INT byteSize;
            TN* src = GenMaxTNForClassRegPair(it->regPair, byteSize);

            if(it->padding)
                {
                    Expand_Sub(SP_TN, SP_TN, Gen_Literal_TN(it->padding, 4),
                               MTYPE_I4, ops_push);
                }
            Expand_SinglePush(SP_TN, byteSize, src, TRUE,
                              it->alignment, availTemps, ops_push);
        }
}

/**
 * Single pop all elements of popVector in ops_pop using given availsTemps as
 * available register. Pops sequence is emitted according end to begin order
 * of popVector. After each pop the padding information of the current
 * element is applied to SP
 *
 * @param  popVector Class reg pair to be poped, with related useful
 *                   information
 * @param  ops_pop List of operations
 * @param  availTemps Array of available registers
 *
 * @pre    availTemps && ops_push
 * @post   ops_pop = ops_pop@pre->append(all single pops of popVector
 *         with the related padding)
 *
 */
static void
ExpandSinglePops(const ClassRegPairAndInfoList& popVector, OPS* ops_pop,
                 REGISTER_SET* availTemps)
{
    CItClassRegPairAndInfoList it = popVector.end();

    while(it-- != popVector.begin())
        {
            INT byteSize;
            TN* res = GenMaxTNForClassRegPair(it->regPair, byteSize);

            Expand_SinglePop(res, SP_TN, byteSize, TRUE, it->alignment,
                             availTemps, ops_pop);
            if(it->padding)
                {
                    Expand_Add(SP_TN, SP_TN, Gen_Literal_TN(it->padding, 4),
                               MTYPE_I4, ops_pop);
                }
        }
}

/**
 * Translate all extension simulated operations of given list of operations
 * and set all operations as volatile.
 *
 * @param  ops List of operations
 *
 * @pre    ops
 * @post   ops->forAll(op | OP_volatile(op))
 *
 */
static void
ConvertAndSetVolatile(OPS* ops)
{
  
    BB* tmpBb = Gen_BB();
    BB_Append_Ops(tmpBb, ops);
    Convert_BB_To_Multi_Ops(tmpBb);
    ops->first = BB_first_op(tmpBb);
    ops->last = BB_last_op(tmpBb);
    ops->length = BB_length(tmpBb);
    OP* op;
    int i = 0;
    for(op = OPS_first(ops); op; op = OP_next(op))
        {
            Set_OP_volatile(op);
            ++i;
        }
}


void
EETARG_Fixup_Stack_Frame (void)
{
  INT i;
  UINT size = 0;
  finalAdjustment = 0;

  GP_is_restored = FALSE;

// [VL] Dynamic loading - Also indicates that GP is not yet set
  GP_is_set_for_pic = FALSE;

  {
      SetOfRegClass savedPair;
      init_callee_saved(savedPair);
      init_caller_saved(savedPair);
  }


  push_pop_rh_mask = 0;
  push_pop_rl_mask = 0;
  push_pop_sh_mask = 0;

  if (Trace_EE) {
    fprintf(TFile,"Callee save registers for pu %s:\n", Cur_PU_Name);
  }

  SetPushPopMask(callee_saved_vector, not_pushed_callee_saved_vector, size);

  if (PU_is_interrupt(Get_Current_PU()) ||
      PU_is_interrupt_nostkaln(Get_Current_PU())) {
      SetPushPopMask(caller_saved_vector, not_pushed_caller_saved_vector, size);
  }

  if(size % PU_aligned_stack(Get_Current_PU()) != 0)
      {
          finalAdjustment = PU_aligned_stack(Get_Current_PU()) -
              (size % PU_aligned_stack(Get_Current_PU()));
          size += finalAdjustment;
      }

  // If push_area_size isn't  empty, allocate REGSPACE
  if (size > 0) {
    Allocate_Reg_Save_Area(size);
  }
  
  if (Trace_EE) {
    fprintf(TFile,"rh_mask = %#x\n",push_pop_rh_mask);
    fprintf(TFile,"rl_mask = %#x\n",push_pop_rl_mask);
    fprintf(TFile,"sh_mask = %#x\n",push_pop_sh_mask);
  }
}

/** 
 * This auxilirary function adds an extra register to the push_pop
 * <mask> given as input.
 * 
 * @param mask input register mask.
 * @param start lower-bound of addressed register range
 * @param end upper-bound (strict) of addressed register range
 * 
 * @return the updated mask
 *
 * @misc [vcdv] used for enhancements art #28120 and #34846
 * 
 */
static INT
add_random_register_save(INT mask, int start, int end) {
  int i=0;
  INT test_mask=0x1;
  while (i<end) {
    if (i>=start) {
      if ((mask & test_mask) != test_mask) {
        return mask|test_mask;
      }
    }
    i++;
    test_mask=test_mask<<1;
  }
  return mask;
}

/* ====================================================================
 *   EETARG_Set_Frame_Len
 *
 *   Setup the computed frame len for the specific code defined here.
 * ====================================================================
 */
void
EETARG_Set_Frame_Len(INT64 frame_len)
{
  // if push mask is not requested, don't change.
  // Fix for bug #26574: The condition used to set lower_size variable
  // must be the same as its usage in EETARG_Fixup_Exit_Code function.

  // If push_area_size is empty, don't change.
  if (callee_saved_vector.empty() &&
      caller_saved_vector.empty())
      return;

  ST *regsave_stb;
  regsave_stb = Get_Reg_Save_Area();

  INT64 ofst =  ST_ofst(regsave_stb);
  ST *base = ST_base(regsave_stb);
  INT64 size = STB_size(regsave_stb);

  if (base == SP_Sym) {
    /* If REGSAVE is SP based (SMALL and LARGE model). */
    lower_size = ofst;
    push_area_size = size;
    upper_size = frame_len - push_area_size - lower_size;
  } else /* FP_Sym. */ {
    /* If REGSAVE is FP based (DYNAMIC model), 
       in this case ofst is negative. */
    upper_size = -ofst - size;
    push_area_size = size;
    lower_size = frame_len - push_area_size - upper_size;
  }
  /* [vcdv] 
   * bug #28120 and bug #34846: try to integrate finalAdjustment
   * realignment in stack allocation if any
   * or push/pop under Os.
   */
  if (finalAdjustment!=0) {
    if (lower_size>0) { /* some stack memory is allocated in function */
      lower_size+=finalAdjustment;
      push_area_size-=finalAdjustment;
      finalAdjustment=0;
    }
    else {
      if (Opt_Prolog_For_Size && finalAdjustment == 4) {
        /* 
         * Under Os pushrl/h mask,subu SP, SP, 4 can be optimized
         * into pushrl/h mask (with an extra bit at true).
         * Replacing an add by an extra push only gains in code size
         */
        if (push_pop_rl_mask != 0) {
          /* R0-R5 are not permitted since they can be used for param
             passing (tailcall) or result(R0-R1) */
          INT newmask = add_random_register_save(push_pop_rl_mask, 6, 16);
          if (newmask != push_pop_rl_mask) {
            push_pop_rl_mask = newmask;
            finalAdjustment = 0;
          }
        }
        if (finalAdjustment!=0 && push_pop_rh_mask != 0) {
          INT newmask = add_random_register_save(push_pop_rh_mask, 0, 16);
          if (newmask != push_pop_rh_mask) {
            push_pop_rh_mask = newmask;
            finalAdjustment = 0;
          }
        }
        // no try on push_pop_sh_mask as use is reserved.
      }
    }
  }
  frame_size = frame_len;

  if (Trace_EE) {
    fprintf(TFile, "<lay> frame_len: %lld, regsave: %lld, lower: %lld, upper: %lld\n",
            frame_len, push_area_size, lower_size, upper_size);
  }
}

/* ====================================================================
 *   Generate_SPAdjust
 *
 *     Generate the code to perform an adjustment of the Stack Pointer.
 *     The adjustment can be either positive or negative.
 *
 *     <offset> : the value to add to the Stack Pointer
 *     <tmp_tn> : a register that can be used to store a temporary
 *                value during computation
 *     <force_tmp_usage>: when TRUE, inform the function to use the long
 *                        code sequence with a temporary TN to hold the
 *                        offset (avoid some useless computation).
 *                        When FALSE, it is first tried to generate a
 *                        single instruction sequence.
 *     <ops> : the operation list to fill
 *
 * ====================================================================
 */
static void
Generate_SPAdjust(INT offset, TN *tmp_tn, BOOL force_tmp_usage, OPS *ops) {
  OPS ops_tmp = OPS_EMPTY;

  if (offset > 0) {
    TN *offset_tn = Gen_Literal_TN(offset, 4);

    if (!force_tmp_usage) {
      Exp_ADD (Pointer_Mtype, SP_TN, SP_TN, offset_tn, &ops_tmp);
    }
    if (force_tmp_usage || OPS_length(&ops_tmp) > 1) {
      /* More than one instruction, we will do explicitly a 
         move rx / add sp, sp, rx sequence with an assigned register. */
      Exp_Immediate(tmp_tn, offset_tn, FALSE, ops);
      Exp_ADD(Pointer_Mtype, SP_TN, SP_TN, tmp_tn, ops);
    }
    else {
      OPS_Append_Ops(ops, &ops_tmp);
    }
  }
  else {
    TN *offset_tn = Gen_Literal_TN(-offset, 4);

    if (!force_tmp_usage) {
      Exp_SUB (Pointer_Mtype, SP_TN, SP_TN, offset_tn, &ops_tmp);
    }
    if (force_tmp_usage || OPS_length(&ops_tmp) > 1) {
      /* More than one instruction, we will do explicitly a 
         move rx / add sp, sp, rx sequence with an assigned register. */
      Exp_Immediate(tmp_tn, offset_tn, FALSE, ops);
      Exp_SUB(Pointer_Mtype, SP_TN, SP_TN, tmp_tn, ops);
    }
    else {
      OPS_Append_Ops(ops, &ops_tmp);
    }
  }
}

/* ====================================================================
 *   Is_Reg_Needed_For_Sub
 *
 *     Return TRUE if a temporary register is required to generate
 *     an SP adjustment of the specified offset (can be either positive
 *     or negative).
 *     Return FALSE otherwise.
 *
 * ====================================================================
 */
static BOOL
Is_Reg_Needed_For_SPAdjust(INT offset) {
  if (offset == 0) {
    return FALSE;
  }

  // If upper size is not zero, we assess if register is needed
  // for the related adjustement by simulating the expansion of
  // the subtraction/addition, depending on the sign of the
  // immediate
  OPS ops_adjust_dummy = OPS_EMPTY;
  if (offset < 0) {
    Exp_SUB (Pointer_Mtype, SP_TN, SP_TN, Gen_Literal_TN(-offset, 4), &ops_adjust_dummy);
  }
  else {
    Exp_ADD (Pointer_Mtype, SP_TN, SP_TN, Gen_Literal_TN(offset, 4), &ops_adjust_dummy);
  }
  
  // Register is needed if expanded as make/more + subu
  if (OPS_last(&ops_adjust_dummy) != OPS_first(&ops_adjust_dummy)) {
    return TRUE;
  }
  return FALSE;
}

/* ====================================================================
 *   Is_Reg_Needed_For_Fixup
 *
 *     If a register is available, just build the TN and return it.
 *     Otherwise, assess if a register is really needed, based on
 *     the actual values of the adjustements.
 *     If need is confirmed, then try GPR_FOR_SP_ADJUST.
 *
 *     bb* is the block where considered fixup code is to be added
 *     rc is the register class where register is taken
 *     reg is an available register, undefined if none available
 *     upper_size, lower_size, finaladj_size are the actual adjustments
 *
 * ====================================================================
 */

static TN*
Is_Reg_Needed_For_Fixup ( BB *bb , ISA_REGISTER_CLASS rc, REGISTER reg, 
                          INT upper_size, INT lower_size, 
                          INT finaladj_size,
                          BOOL* reg_up_needed, BOOL* reg_low_needed,
                          BOOL* reg_finaladj_needed)

{
  TN *assigned_tn;

  // At this stage, register MAY be required for the SP adjustement
  // code. This happens in case the value of the adjustment is too
  // large to use an ADDU/SUBU with litteral operand. We should then
  // use a MAKE/MORE + ADDU/SUBU with register operand. We now
  // determines if register is needed or not!
  // Do it for upper size
  *reg_up_needed = Is_Reg_Needed_For_SPAdjust(-upper_size);

  // Same for lower size
  *reg_low_needed = Is_Reg_Needed_For_SPAdjust(-lower_size);

  // Same for final push adjustement
  *reg_finaladj_needed = Is_Reg_Needed_For_SPAdjust(-finaladj_size);

  // If a regular register was found, just built the TN,  
  // and then set the booleans indicating if needed or not
  // In case upper / lower size are zero, reg_XXX_needed are FALSE
  if (reg != REGISTER_UNDEFINED) {
    assigned_tn = Build_Dedicated_TN(rc, reg, 4);
    return assigned_tn;
  }

  // No regular register found,  but no register really needed for SP 
  // adjustment: return the NULL TN
  if (*reg_up_needed==FALSE && *reg_low_needed==FALSE && *reg_finaladj_needed==FALSE) {
    return NULL;
  }

  // [VL]
  // Reaching this point means that no available register was found
  // and a register is needed. So, we must try to use GPR_FOR_SP_ADJUST
  // instead.
  assigned_tn = EETARG_get_temp_for_spadjust(bb);
  FmtAssert(assigned_tn != NULL, ("No available register for prologue sequence"));
  return assigned_tn;
}


/* ====================================================================
 *   Get_Final_Stack_Alignment
 *
 * Returns the stack alignment value required for current function.
 * This value might differ from the default target alignment if PU is:
 *
 *   - either an interruption (in this case option -Mitstackalign=<n>
 *        or -CG:it_stackalign_val=<n> as well as stack alignment and
 *        nostackalign attributes may apply)
 *
 *   - or a function with forced stack alignment (attribute/auto-alignment)
 *
 * This function relies on the following variables:
 * -----------------------------------------------
 *   - <Target_Stack_Alignment> : Default target stack alignment
 *
 *   - <PU_aligned_stack(PU)> : This value is built from
 *                      - <Target_Stack_Alignment>
 *                      - <aligned_stack> attribute of the function,
 *                      - potential auto-alignment for stack variables,
 *  - <CG_itstackalign_val>   and
 *    <CG_itstackalign_val_overridden> : interruption specific alignments
 *
 * ====================================================================
 */
static INT
Get_Final_Stack_Alignment()
{
  INT pu_stack_alignment;

  // Exit of no specific alignment
  if(!((PU_is_interrupt(Get_Current_PU())) ||
       (PU_aligned_stack(Get_Current_PU())!=Target_Stack_Alignment))) {
    return (Target_Stack_Alignment);
  }

  // [VL] Calculate the effective alignment.
  // The overall policy is as follows:
  // - In interruption:
  //   - If declared as interrupt_nostackalign (pragma), this takes
  //     precedence over both stack alignment attribute and IT stack
  //     alignment option
  //   - If an alignment attribute is set, it always takes precedence over
  //     the IT alignment option
  //   - Otherwise, if IT alignment option is set, then it is applied 
  //     whatever its effect (increasing/decresing alignment)
  //   - In any other we just apply the default (64b)
  // - Otherwise, for regular PU's, either default or alignment specified
  //   thanks to the stack alignment attribute or self alignment is applied.

  if (PU_is_interrupt(Get_Current_PU())) {
    // In IT, apply the stack alignment attribute if set, or default (8)
    pu_stack_alignment = (int) (PU_aligned_stack(Get_Current_PU()));

    if (CG_itstackalign_val_overridden) {
      // If an option is set to specify stack alignment in IT...

      if (PU_aligned_stack(Get_Current_PU())!=Target_Stack_Alignment) {
        // If an attribute specifies larger stack alignment, it takes prece-
        // dence over the option
        pu_stack_alignment = (int) (PU_aligned_stack(Get_Current_PU()));
      } else {
        // Otherwise we verify and apply the option if legal
        if(!IS_POWER_OF_2(CG_itstackalign_val) ||
           (CG_itstackalign_val<4 || CG_itstackalign_val>512)) {

          // If its value is not legal, emit a warning and ignore option
          ErrMsg(EC_Warn_ITStackAlignOpt, CG_itstackalign_val, CG_itstackalign_val);

          pu_stack_alignment = (int) (PU_aligned_stack(Get_Current_PU()));
        } else {
          // Macro emission is needed only if alignment >= 8 bytes
          if (CG_itstackalign_val>=Target_Stack_Alignment) {
            pu_stack_alignment = CG_itstackalign_val;
          } else {
            return (Target_Stack_Alignment);
          }
        }
      }
    }
  } else if (PU_aligned_stack(Get_Current_PU())!=Target_Stack_Alignment) {
    // In regular PU, just apply the alignment already set
    pu_stack_alignment = (int) (PU_aligned_stack(Get_Current_PU()));
  }

  // At this stage, we should have a legal value for the alignment
  FmtAssert((pu_stack_alignment!=0) && (IS_POWER_OF_2(pu_stack_alignment)), 
            ("Unexpected stack alignment specification <%d>", pu_stack_alignment));

  return pu_stack_alignment;
}


/* ====================================================================
 *   Generate_Stack_Align_Entry
 *
 *   If required, generate the code sequence to align the stack
 *   on the value requested by current PU at function entry, and
 *   append the operations into <ops> parameter.
 *   
 * ====================================================================
 */
static void
Generate_Stack_Align_Entry(OPS *ops) {

  INT pu_stack_alignment = Get_Final_Stack_Alignment();
  if (pu_stack_alignment <= Target_Stack_Alignment) {
    // No emision of stack alignment code required 
    return;
  }
  // In code below, the stack alignment is used through a mask
  INT stack_alignment_mask = pu_stack_alignment - 1;

  TN *TN_tmp = Build_Dedicated_TN(ISA_REGISTER_CLASS_gpr, GPR_FOR_STACKALIGN1, 4);

  if (stack_alignment_mask <= 255) {
    // The required alignment is small enough to use NORN instruction.
    // Only one temporary register is required:
    //
    //      sw     @(sp-!4), r6
    //      subu   r6, sp, 4
    //      norn   sp, r6, Imm
    //      addu   r6, r6, 4
    //      sw     @(sp+0), r6
    //
    Build_OP(TOP_sw_i5_pre_dec, SP_TN, True_TN, SP_TN, Gen_Literal_TN(-4, 4), TN_tmp, ops);
    Build_OP(TOP_subu_i8, TN_tmp, True_TN, SP_TN, Gen_Literal_TN(4, 4), ops);
    Build_OP(TOP_norn, SP_TN, True_TN, TN_tmp, Gen_Literal_TN(stack_alignment_mask, 4), ops);
    Build_OP(TOP_addu_i8, TN_tmp, True_TN, TN_tmp, Gen_Literal_TN(4, 4), ops);
    Build_OP(TOP_sw_i8_dec, True_TN, SP_TN, Gen_Literal_TN(0, 4), TN_tmp, ops);
  }
  else {
    // With alignment > 256 bits, it is required to use an extra temporary register
    // to store the value for masking the address:
    //
    //      sw     @(sp-!4), r6
    //      or     r6, sp, 0
    //      sw     @(r6-!4), r7
    //      make   r7, lowpart(Imm)
    //    ( more   r7, highpart(Imm) )  --> only if needed, should never happen
    //      andn   sp, r7, r6               (required if alignment > 2^15)
    //      lw     r7, @(r6!+4)
    //      sw     @(sp+0), r6
    //
    TN *TN_tmp2 = Build_Dedicated_TN(ISA_REGISTER_CLASS_gpr, GPR_FOR_STACKALIGN2, 4);
    Build_OP(TOP_sw_i5_pre_dec, SP_TN, True_TN, SP_TN, Gen_Literal_TN(-4, 4), TN_tmp, ops);
    Build_OP(TOP_or_i8, TN_tmp, True_TN, SP_TN, Gen_Literal_TN(0, 4), ops);
    Build_OP(TOP_sw_i5_pre_dec, TN_tmp, True_TN, TN_tmp, Gen_Literal_TN(-4, 4), TN_tmp2, ops);
    Build_OP(TOP_MAKE, TN_tmp2, True_TN, Gen_Literal_TN(stack_alignment_mask, 4), ops);
    Build_OP(TOP_andn, SP_TN, True_TN, TN_tmp2, TN_tmp, ops);
    Build_OP(TOP_lw_i5_post_inc, TN_tmp2, TN_tmp, True_TN, TN_tmp, Gen_Literal_TN(4, 4), ops);
    Build_OP(TOP_sw_i8_dec, True_TN, SP_TN, Gen_Literal_TN(0, 4), TN_tmp, ops);
  }
}


/* ====================================================================
 *   Generate_Stack_Align_Exit
 *
 *   If required, generate the code sequence to undo the stack
 *   alignment at the end of current PU, and append the
 *   operations into <ops> parameter.
 *   
 * ====================================================================
 */
static void
Generate_Stack_Align_Exit(OPS *ops) {
  INT pu_stack_alignment = Get_Final_Stack_Alignment();
  if (pu_stack_alignment <= Target_Stack_Alignment) {
    // No emision of stack alignment code required 
    return;
  }
  // In code below, the stack alignment is used through a mask
  INT stack_alignment_mask = pu_stack_alignment - 1;

  // This is the counterpart of code generated by Generate_Stack_Align_Entry()
  //
  //      lw     sp, @(sp+0)
  //      lw     r6, @(sp!+4)
  //
  TN *TN_tmp = Build_Dedicated_TN(ISA_REGISTER_CLASS_gpr, GPR_FOR_SP_ADJUST, 4);
  Build_OP(TOP_lw_i8_inc, SP_TN, True_TN, SP_TN, Gen_Literal_TN(0, 4), ops);
  Build_OP(TOP_lw_i5_post_inc, TN_tmp, SP_TN, True_TN, SP_TN, Gen_Literal_TN(4, 4), ops);
}


/* ====================================================================
 *   Fixup_Stack_Align_Entry
 *
 *   If required, insert code to perform stack alignment
 *   at start of specified <bb>.
 *   
 * ====================================================================
 */
static void
Fixup_Stack_Align_Entry(BB *bb) {
  OPS ops_stkalign = OPS_EMPTY;
  Generate_Stack_Align_Entry(&ops_stkalign);

  if (OPS_length(&ops_stkalign) > 0) {
    OP *op;
    FOR_ALL_OPS_OPs_FWD(&ops_stkalign, op) {
      Set_OP_prologue(op);
    }

    BB_Prepend_Ops (bb, &ops_stkalign);
  }
}

/* ====================================================================
 *   Fixup_Stack_Align_Exit
 *
 *   If required, insert code to undo stack alignment
 *   at end of specified <bb>, before the return instruction.
 *   
 * ====================================================================
 */
static void
Fixup_Stack_Align_Exit(BB *bb) {
  OPS ops_stkalign = OPS_EMPTY;
  Generate_Stack_Align_Exit(&ops_stkalign);

  if (OPS_length(&ops_stkalign) > 0) {
    OP *op;
    FOR_ALL_OPS_OPs_FWD(&ops_stkalign, op) {
      Set_OP_epilogue(op);
    }

    OP *xfer_op = BB_last_op(bb);
    FmtAssert(xfer_op != NULL && OP_xfer(xfer_op), ("Unexpected empty or non xfer op in bb exit"));

    BB_Insert_Ops_Before(bb, xfer_op, &ops_stkalign);
  }
}


/* ====================================================================
 *   EETARG_Fixup_Entry_Code
 *
 *   Generate push/pop sequence.
 * ====================================================================
 */
void
EETARG_Fixup_Entry_Code (
  BB* bb
)
{
  INT i;

  if (PU_is_interrupt(Get_Current_PU()) ||
      PU_is_interrupt_nostkaln(Get_Current_PU())) {

    // [CR] Interrupt handlers have to enable HWLOOP bit if necessary
    // [VL] To fix #70522, we generate those instructions only if the
    //      core configuration has HWloops AND
    //       - either we have a call in the PU, in which case we must 
    //         assume that it may contain HWloops to be conservative/
    //         safe OR
    //       - we have HW loops in the PU and compiler options enable 
    //         HW loop generation
    if (Core_Has_HWLoop &&
        (PU_Has_Calls || 
         (PU_Has_Hwloops && (Activate_Hwloop & ACTIVATE_HWLOOP_GENERATION)))) {

      OPS ops_hwloop = OPS_EMPTY;
      OP *point = NULL;

      TN *flag0hwl_tn = Build_Dedicated_TN(ISA_REGISTER_CLASS_lw, REGISTER_MIN, 0);
      TN *flag1hwl_tn = Build_Dedicated_TN(ISA_REGISTER_CLASS_lw, REGISTER_MIN+1, 0);

      Build_OP(TOP_loopena, flag0hwl_tn, &ops_hwloop);
      Build_OP(TOP_loopena, flag1hwl_tn, &ops_hwloop);
      Build_OP(TOP_barrier, &ops_hwloop);

      point = OPS_first(&ops_hwloop);
      point = BB_first_op(bb);
      if (point == NULL) {
	BB_Append_Ops (bb, &ops_hwloop);
      }
      else {
	BB_Insert_Ops_Before(bb, point, &ops_hwloop);
      }
      OP *op;
      FOR_ALL_OPS_OPs_FWD(&ops_hwloop, op) {
	Set_OP_prologue(op);
      }
    } // End of Core_Has_HWLoop...

  } // End of PU_is_interrupt...

  // VL Dyn_Load
  // [VL] If we generate code for PIC/PID (GOT_Model is set), then 
  // we must set GP using an addur. For the moment this is done in
  // a systematic way, but can be refined.
  // [VCDV] added PU_Has_Calls condition since a call is an implicit
  // reference to GP if called func is not static/private.
  if (GOT_Model!=got_none) {
    if (PU_Has_Calls || PU_References_GP) {
      EETARG_set_gp_for_pic(bb);
    }
  }

  if (callee_saved_vector.empty() &&
      caller_saved_vector.empty()) {
    Fixup_Stack_Align_Entry(bb);
    return;
  }

  ANNOTATION *ant = ANNOT_Get(BB_annotations(bb), ANNOT_ENTRYINFO);
  ENTRYINFO *entry_info = ANNOT_entryinfo(ant);
  OP *ent_adj = ENTRYINFO_sp_adj(entry_info);
  OP *sp_adj;
  OPS ops_adjust = OPS_EMPTY;
  OPS ops_adjust_lower = OPS_EMPTY;
  OPS ops_push = OPS_EMPTY;

  sp_adj = ent_adj;
  
  if (has_fp_adjust) {
    do {
      sp_adj = OP_prev(sp_adj);
    } while (sp_adj != NULL &&
	     !(OP_results(sp_adj) > 0 && TN_is_sp_reg(OP_result(sp_adj,0))));
  }

  if (Trace_EE) {
    fprintf(TFile, "\nFrame allocation before target dependent fixup:\n");
    if (sp_adj != NULL) Print_OP_No_SrcLine(sp_adj);
    if (ent_adj != NULL && sp_adj != ent_adj) Print_OP_No_SrcLine(ent_adj);
  }

  OP *point = NULL;

  /* Find available register for adjusts. */
  TN *sp_adjust_tn;
  ISA_REGISTER_CLASS rc = ISA_REGISTER_CLASS_gpr;
  REGISTER_SET avail_temps[ISA_REGISTER_CLASS_MAX+1];
  BOOL reg_needed_upper = FALSE;
  BOOL reg_needed_lower = FALSE;
  BOOL reg_needed_finalAdjust = FALSE;

  /* Get available registers at the start of the block. */
  REG_LIVE_Prolog_Temps(bb, NULL, NULL, avail_temps);
  REGISTER reg = REGISTER_SET_Choose(avail_temps[rc]);

  // [VL] This function Is_Reg_Needed_For_Fixup:
  // - checks if register is really needed in sp_adjust
  // - checks if one is available and build the related TN 
  // - if no register available, assign and set R6
  // sp_adjust_tn contains the TN to be used, NULL if no
  // register available but R6, and no register needed
  // for sp_adjust

  sp_adjust_tn = Is_Reg_Needed_For_Fixup (bb, rc, reg, upper_size, lower_size,
                                          finalAdjustment,
                                          &reg_needed_upper, &reg_needed_lower,
                                          &reg_needed_finalAdjust);
  
  if (upper_size != 0) {
    Generate_SPAdjust(-upper_size, sp_adjust_tn, reg_needed_upper, &ops_adjust);
  }

  if(push_pop_rh_mask != 0) {
    Build_OP(TOP_pushrh, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_rh_mask, 4 ), &ops_push );
  }
  if(push_pop_rl_mask != 0) {
    Build_OP(TOP_pushrl, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_rl_mask, 4 ), &ops_push );
  }
  if(push_pop_sh_mask != 0) {
    Build_OP(TOP_pushsh, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_sh_mask, 4 ), &ops_push );
  }

  // If a TN was assigned for sp_adjust, and it is really needed, 
  // then we remove it from the set of available ones
  if(sp_adjust_tn && (reg_needed_upper || reg_needed_lower || reg_needed_finalAdjust)) {
    avail_temps[TN_register_class(sp_adjust_tn)] =
        REGISTER_SET_Difference1(avail_temps[TN_register_class(sp_adjust_tn)],
                                 TN_register(sp_adjust_tn));
  }

  InitAvailTemps(avail_temps);

  ExpandSinglePushes(not_pushed_callee_saved_vector, &ops_push, avail_temps);
  ExpandSinglePushes(not_pushed_caller_saved_vector, &ops_push, avail_temps);
  if(finalAdjustment) {
    Generate_SPAdjust(-finalAdjustment, sp_adjust_tn, reg_needed_finalAdjust, &ops_push);
  }

  ConvertAndSetVolatile(&ops_push);

  if (lower_size != 0) {
    Generate_SPAdjust(-lower_size, sp_adjust_tn, reg_needed_lower, &ops_adjust_lower);
  }

  /* Adjust insertion point. */
  if (sp_adj != NULL) {
    OP *new_adj = OPS_last(&ops_adjust_lower);
    /* Point to the possibly new SP adjust 
       (it may be NULL if lower_size == 0). */
    ENTRYINFO_sp_adj(entry_info) = new_adj;
    BB_Remove_Op(bb, sp_adj);
  }   
  point = BB_first_op(bb);

  if (Trace_EE) {
    fprintf(TFile, "\nNew target dependent stack frame allocation:\n");
    Print_OPS(&ops_adjust);
    Print_OPS(&ops_push);
    Print_OPS(&ops_adjust_lower);
  }

  /* [TTh] Set the prologue field for all the entry OPs */
  OP *op;
  FOR_ALL_OPS_OPs_FWD(&ops_adjust, op) {
    Set_OP_prologue(op);
  }
  FOR_ALL_OPS_OPs_FWD(&ops_push, op) {
    Set_OP_prologue(op);
  }
  FOR_ALL_OPS_OPs_FWD(&ops_adjust_lower, op) {
    Set_OP_prologue(op);
  }

  if (point == NULL) {
    /* Effective operations order in block after this is:
       ops_adjust
       ops_push
       ops_adjust_lower
    */
    BB_Append_Ops (bb, &ops_adjust);
    BB_Append_Ops (bb, &ops_push);
    BB_Append_Ops (bb, &ops_adjust_lower);
  } else {
   /* Effective operations order in block after this is:
       ops_adjust
       ops_push
       ops_adjust_lower
    */
    BB_Insert_Ops_Before(bb, point, &ops_adjust);
    BB_Insert_Ops_Before(bb, point, &ops_push);
    BB_Insert_Ops_Before(bb, point, &ops_adjust_lower);
  }

  Fixup_Stack_Align_Entry(bb);

  return;
}

/* ====================================================================
 *   EETARG_Fixup_Exit_Code
 *
 *   Generate push/pop sequence.
 * ====================================================================
 */
void
EETARG_Fixup_Exit_Code (
  BB *bb
)
{
  INT i;

  /* check for need to generate restore code for callee-saved regs */
  if ((callee_saved_vector.size() == 0) &&
      (caller_saved_vector.size() == 0)) {
    Fixup_Stack_Align_Exit(bb);
    return;
  }

  ANNOTATION *ant = ANNOT_Get(BB_annotations(bb), ANNOT_EXITINFO);
  EXITINFO *exit_info = ANNOT_exitinfo(ant);
  OP *sp_adj = EXITINFO_sp_adj(exit_info);
  OP *xfer_op = BB_last_op(bb);
  BOOL has_dynamic_frame = Current_PU_Stack_Model == SMODEL_DYNAMIC;
  FmtAssert(xfer_op != NULL && OP_xfer(xfer_op), ("Unexpected empty or non xfer op in bb exit"));
  
  OPS ops_adjust = OPS_EMPTY;
  OPS ops_adjust_lower = OPS_EMPTY;
  OPS ops_pop = OPS_EMPTY;
  OPS ops_sp_fp_adjust = OPS_EMPTY;
  
  /* Find available register for adjusts. */
  TN *tmp_tn;
  ISA_REGISTER_CLASS rc = ISA_REGISTER_CLASS_gpr;
  REGISTER_SET avail_temps[ISA_REGISTER_CLASS_MAX+1];
  /* Get available registers at the sp adjust or xfer operation. */
  REG_LIVE_Epilog_Temps(Get_Current_PU_ST(), bb, sp_adj != NULL ? OP_next(sp_adj) : xfer_op, avail_temps);
  REGISTER reg = REGISTER_SET_Choose(avail_temps[rc]);
  /* This should not happen as some scratch registers are available. */
  FmtAssert(reg != REGISTER_UNDEFINED, ("No available register for epilogue sequence"));
  tmp_tn = Build_Dedicated_TN(rc, reg, 4);
  
  if (upper_size != 0) {
    Generate_SPAdjust(upper_size, tmp_tn, FALSE, &ops_adjust);
  }

  avail_temps[TN_register_class(tmp_tn)] =
      REGISTER_SET_Difference1(avail_temps[TN_register_class(tmp_tn)],
                               TN_register(tmp_tn));

  if(finalAdjustment) {
    Generate_SPAdjust(finalAdjustment, tmp_tn, FALSE, &ops_pop);
  }
  ExpandSinglePops(not_pushed_caller_saved_vector, &ops_pop, avail_temps);
  ExpandSinglePops(not_pushed_callee_saved_vector, &ops_pop, avail_temps);

  if(push_pop_sh_mask != 0) {
    Build_OP(TOP_popsh, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_sh_mask, 4 ), &ops_pop );
  }
  if(push_pop_rl_mask != 0) {
    Build_OP(TOP_poprl, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_rl_mask, 4 ), &ops_pop );
  }
  if(push_pop_rh_mask != 0) {
    Build_OP(TOP_poprh, SP_TN, SP_TN, Gen_Literal_TN ( push_pop_rh_mask, 4 ), &ops_pop );
  }

  ConvertAndSetVolatile(&ops_pop);
  
  if (has_dynamic_frame) {
    /* In the case of a dynamic frame we must compute sp before 
     * generating the pop and upper adjust with:
     * sp = fp - (upper_size + push_area_size)
     */
    TN *size_tn;
    FmtAssert(sp_adj != NULL, ("Expected sp adjust for dynamic model"));
    size_tn =  Gen_Literal_TN(upper_size+push_area_size, 4);
    Exp_SUB (Pointer_Mtype, SP_TN, FP_TN, size_tn, &ops_adjust_lower);
    if (OPS_length(&ops_adjust_lower) > 1) {
      /* More than one instruction, we will do explicitly a 
	 move rx / add sp, sp, rx sequence with an assigned register. */
      ops_adjust_lower = (OPS)OPS_EMPTY;
      Exp_Immediate(tmp_tn, size_tn, FALSE, &ops_adjust_lower);
      Exp_SUB(Pointer_Mtype, SP_TN, FP_TN, tmp_tn, &ops_adjust_lower);
    }
  } else if (lower_size != 0) {
    Generate_SPAdjust(lower_size, tmp_tn, FALSE, &ops_adjust_lower);
  }


  /* [TTh] Set the epilogue field for all the exit OPs */
  OP *op;
  FOR_ALL_OPS_OPs_FWD(&ops_adjust_lower, op) {
    Set_OP_epilogue(op);
  }
  FOR_ALL_OPS_OPs_FWD(&ops_pop, op) {
    Set_OP_epilogue(op);
  }
  FOR_ALL_OPS_OPs_FWD(&ops_adjust, op) {
    Set_OP_epilogue(op);
  }

  if (sp_adj == NULL) {
    /* Effective operations order in block after this is:
       ops_adjust_lower
       ops_pop
       ops_adjust
    */
    if (Trace_EE) {
      fprintf(TFile, "\nExit sequence:\n");
      Print_OPS(&ops_adjust_lower);
      Print_OPS(&ops_pop);
      Print_OPS(&ops_adjust);
    }

    BB_Insert_Ops_Before(bb, xfer_op, &ops_adjust_lower);
    BB_Insert_Ops_Before(bb, xfer_op, &ops_pop);
    BB_Insert_Ops_Before(bb, xfer_op, &ops_adjust); 
  } else {
    OP *new_adj = OPS_last(&ops_adjust_lower);
    /* Point to the possibly new SP adjust 
       (it may be NULL if lower_size == 0). */
    EXITINFO_sp_adj(exit_info) = new_adj;


    /* Effective operations order in block after this is:
       [ops_sp_fp_adjust]
       ops_adjust_lower
       [other initial ops]
       ops_pop
       ops_adjust

       Note that pop operation must be generated after the [other initial ops]
       between the initial sp adjust and the end of the block as these
       operations may refer to a callee saved register that the pop could
       destroy. The ops_pop and ops_adjust operations, though, are generated
       just before the xfer operation.
    */
    if (Trace_EE) {
      fprintf(TFile, "\nSP adjust before exit sequence:\n");
      Print_OP_No_SrcLine(sp_adj);
      fprintf(TFile, "\nExit sequence:\n");
      Print_OPS(&ops_adjust_lower);
      Print_OPS(&ops_pop);
      Print_OPS(&ops_adjust);
    }

    BB_Insert_Ops_After(bb, sp_adj, &ops_adjust_lower);
    BB_Remove_Op(bb, sp_adj);
    BB_Insert_Ops_Before(bb, xfer_op, &ops_pop);
    BB_Insert_Ops_Before(bb, xfer_op, &ops_adjust); 
  }

  Fixup_Stack_Align_Exit(bb);

  return;
}

/* =================================================================
 *   EETARG_Callee_Saved_Regs_Mask_Size
 *
 *   Calculate the size of the save area needed for the callee
 *   saved register mask.
 * =================================================================
 */
INT
EETARG_Callee_Saved_Regs_Mask_Size ()
{
  INT size = 0;

  /* check for need to generate restore code for callee-saved regs */
  return size;
}

/* ====================================================================
 *   EETARG_Init_Entry_Exit_Code ()
 * ====================================================================
 */
void
EETARG_Init_Entry_Exit_Code (
  WN *pu_wn, 
  BOOL need_frame_pointer
)
{
  has_fp_adjust = need_frame_pointer;
  return;
}

/* ====================================================================
 *   EETARG_Save_Pfs
 *
 *   nothing to do, no PFS
 * ====================================================================
 */
void
EETARG_Save_Pfs (TN *saved_pfs, OPS *ops)
{
  return;
}

/* ====================================================================
 *   EETARG_Restore_Pfs
 *
 *   nothing to do, no PFS
 * ====================================================================
 */
void
EETARG_Restore_Pfs (TN *saved_pfs, OPS *ops)
{
  return;
}

/* ====================================================================
 *   EETARG_Save_Extra_Callee_Tns
 *
 *   save predicates
 *   This is handled differently cause we need to
 *   save and restore the whole bank of predicates in one instruction.
 *   Note that we assume GRA will remove the predicate save/restore
 *   if no callee-save predicates are used.
 * ====================================================================
 */
void EETARG_Save_Extra_Callee_Tns (
  OPS *ops
)
{
  // 1. Save the guard registers:
#if 0
  TN *callee_tn = CALLEE_tn(Callee_Saved_Regs_Count);
  // save callee_tn in callee-saved-regs array;
  // this works cause originally allocated space for all regs,
  // yet only use space for callee-save (so available space).
  // could get broken if ever allocated minimal space originally.
  if (callee_tn == NULL) {
    callee_tn = Build_RCLASS_TN(ISA_REGISTER_CLASS_du);
    Set_TN_is_gra_cannot_split(callee_tn);
    CALLEE_tn(Callee_Saved_Regs_Count) = callee_tn;
  }
  Build_OP (TOP_cmove, callee_tn, True_TN, Pr_TN, ops);
  Set_OP_no_move_before_gra(OPS_last(ops));
#endif
}

/* ====================================================================
 *   EETARG_Restore_Extra_Callee_Tns
 * ====================================================================
 */
void EETARG_Restore_Extra_Callee_Tns (
  OPS *ops
)
{
#if 0
  // restore all predicates
  TN *callee_tn = CALLEE_tn(Callee_Saved_Regs_Count);
  Build_OP (TOP_cmove, Pr_TN, True_TN, callee_tn, ops);
  Set_OP_no_move_before_gra(OPS_last(ops));
#endif
}


/* ====================================================================
 * EETARG_Do_Not_Save_Callee_Reg_Class
 * ====================================================================
 */

BOOL EETARG_Do_Not_Save_Callee_Reg_Class ( ISA_REGISTER_CLASS cl ) {
  // Do not save ISA_REGISTER_CLASS_lr for now. Will have to fix this
  // when hw loops will be implemented.
  if(cl == ISA_REGISTER_CLASS_lc) return TRUE;
  return FALSE;
}

/* ======================================================================
 * EETARG_get_temp_for_spadjust
 * Should returns a super scratch (not allocatable) or defined register.
 * This register will be used to initialize stack pointer in entry block.
 * ======================================================================
 */

TN *EETARG_get_temp_for_spadjust( BB *bb) {

  INT newmask = push_pop_rl_mask | (1 << GPR_FOR_SP_ADJUST - REGISTER_MIN);
  if (newmask != push_pop_rl_mask) {
    push_pop_rl_mask = newmask;
    ST * st;
    int i;
    FOREACH_SYMBOL (CURRENT_SYMTAB, st, i) {
      if ((ST_class(st) == CLASS_VAR) &&
	  ((ST_sclass(st) == SCLASS_FORMAL) ||
	   (ST_sclass(st) == SCLASS_FORMAL_REF)) &&
	  (ST_base_idx(st) != ST_st_idx(st)) &&
	  (Base_Symbol(st) == SP_Sym)) {
	Set_ST_ofst (st, ST_ofst(st)+4) ;
      }
    }
  }

  return Build_Dedicated_TN(ISA_REGISTER_CLASS_gpr, (REGISTER)GPR_FOR_SP_ADJUST, 4);
}

/* ======================================================================
 * EETARG_Save_With_Regmask
 * Return TRUE if (cl,reg) should be saved using the regmask mechanism
 * rather than the save_reg mechanism.
 * 
 * ======================================================================
 */

BOOL EETARG_Save_With_Regmask (ISA_REGISTER_CLASS cl, REGISTER reg)
{
  return CG_gen_callee_saved_regs_mask;
}
