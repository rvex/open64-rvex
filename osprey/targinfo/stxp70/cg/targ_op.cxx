/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/

/* ====================================================================
 * ====================================================================
 *
 *       OP utility routines which include target dependencies.
 *
 * ====================================================================
 * ====================================================================
 */

#include "defs.h"
#include "config.h"
#include "erglob.h"
#include "xstats.h"
#include "tracing.h"

#include "cgir.h"
#include "whirl2ops.h"

#include "cg.h"
#include "opt_alias_mgr.h"

// Reconfigurability
#include "lai_loader_api.h"

#include "targ_isa_subset.h"
#include "targ_isa_selector.h"

#include "cgexp.h"

/* [JV] Defined in exp_targ.cxx */
extern ISA_LIT_CLASS Get_Lit_Clas_For_Memory_Access_Size( INT32 size, BOOL is_signed );

/* ====================================================================
 *   OP_is_ext_op
 * ====================================================================
 */
BOOL OP_is_ext_op(OP *op) 
{
  TOP top = OP_code(op);
  if(TOP_is_targ_ext_fpx(top)) return TRUE;
  if(TOP_is_targ_ext_x3(top)) return TRUE;
  return FALSE;
}

/* ====================================================================
 *   OP_Is_Barrier
 *
 * ====================================================================
 */
BOOL OP_Is_Barrier(OP *op) 
{
  TOP top = OP_code(op);

  if (top == TOP_asm) {
    ASM_OP_ANNOT* asm_info = (ASM_OP_ANNOT*) OP_MAP_Get(OP_Asm_Map, op);
    if (ASM_OP_wn(asm_info) && WN_Asm_Clobbers_Mem(ASM_OP_wn(asm_info)))
      return TRUE;
  }

  if (TOP_is_barrier(top)) {
    return TRUE;
  }

  return FALSE;
}

/* ====================================================================
 *   OP_is_counted_loop
 * ====================================================================
 */
BOOL 
OP_Is_Counted_Loop(OP *op) 
{
  return (OP_code(op) == TOP_HWBRANCH);
}

/* ====================================================================
 *   OP_Is_Copy_Immediate_Into_Register
 * ====================================================================
 */
BOOL 
OP_Is_Copy_Immediate_Into_Register(OP *op) 
{
  return (OP_code(op) == TOP_MAKE ||
	  OP_code(op) == TOP_v3_makehp ||
	  OP_code(op) == TOP_v3_make ||
	  OP_code(op) == TOP_v4_gp16_make32 ||
	  OP_code(op) == TOP_v4_gp32_make32 ||
	  OP_code(op) == TOP_v4_gp48_make32);
}

/* ====================================================================
 *   OP_Has_Latency
 * ====================================================================
 */
BOOL 
OP_Has_Latency(OP *op) 
{
  return (CGTARG_Max_OP_Latency (op)>1 ||
          /* HWLOOP programming  rule 9 */
          (is_TOP_setlc_r(OP_code(op))) ||
          (is_TOP_setlc_i16(OP_code(op))) ||
          /* HWLOOP programming rule 11 */
	  (is_TOP_setle(OP_code(op))));
}

/* ====================================================================
 *   OP_Is_Advanced_Load
 * ====================================================================
 */
BOOL
OP_Is_Advanced_Load( OP *memop )
{
  // no advanced loads for st200
  return FALSE;
}

/* ====================================================================
 *   OP_Is_Speculative_Load
 * ====================================================================
 */
BOOL
OP_Is_Speculative_Load ( 
  OP *memop 
)
{
  if (!OP_load(memop)) return FALSE;

  return FALSE;
}

/* ====================================================================
 *   OP_Is_Check_Load
 * ====================================================================
 */
BOOL
OP_Is_Check_Load ( 
  OP *memop 
)
{
  if (!OP_load(memop)) return FALSE;

  return FALSE;
}


/* ====================================================================
 *   OP_Is_Speculative
 * ====================================================================
 */
BOOL
OP_Is_Speculative (
  OP *op
)
{
  if (!OP_load(op)) return FALSE;

  // speculative and advanced loads are safe to speculate.
  if (OP_Is_Advanced_Load(op) || OP_Is_Speculative_Load(op))
    return TRUE;

  return FALSE;
}

/* ====================================================================
 *   OP_Can_Be_Speculative
 *
 *   determines if the TOP can be speculatively executed taking 
 *   into account eagerness level
 * ====================================================================
 */
BOOL OP_Can_Be_Speculative (
  OP *op
)
{
  WN *wn;
  TN *offset;
  TOP opcode = OP_code(op);

  // can never speculate a call.
  if (OP_call (op))
    return FALSE;

  // [CG] Not clear if we can speculate an asm
  if (OP_code(op) == TOP_asm) return FALSE;

  if (OP_Is_Barrier(op)) return FALSE;

  if (OP_side_effects (op)) return FALSE;

  switch (Eager_Level) {
   case EAGER_NONE:

     /* not allowed to speculate anything
     */
     return FALSE;
     break;

  case EAGER_SAFE:

    /* Only exception-safe speculative ops are allowed
     */
#if 0
    if (TOP_is_ftrap(opcode) || TOP_is_itrap(opcode)) return FALSE;
#endif
    /*FALLTHROUGH*/

  case EAGER_ARITH:

    /* Arithmetic exceptions allowed
     */
    if (OP_fdiv(op)) return FALSE;
    /*FALLTHROUGH*/

  case EAGER_DIVIDE:

    /* Divide by zero exceptions allowed 
     */
#if 0
    if (TOP_is_memtrap(opcode)) return FALSE;
#endif
    /*FALLTHROUGH*/

  case EAGER_MEMORY:

    /* Memory exceptions allowed / All speculative ops allowed
     */
    if (TOP_is_unsafe(opcode)) return FALSE;
    break;

  default:
    DevWarn("unhandled eagerness level: %d", Eager_Level);
    return FALSE;
  }

  if (!OP_memory (op)) return TRUE;

  /* prefetch are speculative. */
  if (OP_prefetch (op)) return TRUE;

  /* This is a memory reference */

  /* don't speculate volatile memory references. */
  if (OP_volatile(op)) return FALSE;

  if (!OP_load(op)) return FALSE;

  /* Try to identify simple scalar loads than can be safely speculated:
   *  a) read only loads (literals, GOT-loads, etc.)
   *  b) load of a fixed variable (directly referenced)
   *  c) load of a fixed variable (base address is constant or
   *     known to be in bounds)
   *  d) speculative, advanced and advanced-speculative loads are safe.
   */

  /*  a) read only loads (literals, GOT-loads, etc.)
   */
  if (OP_no_alias(op)) goto scalar_load;

  /*  b) load of a fixed variable (directly referenced); this
   *     includes spill-restores.
   *  b') exclude cases of direct loads of weak symbols (#622949).
   */
  if ((offset = OP_Offset(op))
      && TN_is_symbol(offset)
      && ! ST_is_weak_symbol(TN_var(offset))) goto scalar_load;

  /*  c) load of a fixed variable (base address is constant or
   *     known to be in bounds).
   */
  if ((wn = Get_WN_From_Memory_OP(op))
      && Alias_Manager
      && Safe_to_speculate(Alias_Manager, wn)) goto scalar_load;

  /* we can't speculate a load unless it is marked as dismissable */
  /* it is the client's responsability to do that. */
  if (OP_Is_Speculative(op)) return TRUE;
  
  /* If we got to here, we couldn't convince ourself that we have
   * a scalar load -- no speculation this time...
   */
  return FALSE;

  /* We now know we have a scalar load of some form. Determine if they
   * are allowed to be speculated.
   */
scalar_load:
  return TRUE; 

}

/* ====================================================================
 *   CGTARG_Predicate_OP
 * ====================================================================
 */
void
CGTARG_Predicate_OP (
  BB* bb, 
  OP* op, 
  TN* pred_tn,
  bool on_false
)
{
  /* warning bb is not necessarily available when calling
     CGTARG_Predicate_OP().
     in this case, bb= NULL !!!
  */

  if (OP_has_predicate(op)) {
    Set_OP_opnd(op, OP_find_opnd_use(op,OU_predicate), pred_tn);
    /* copy flag not allowed on predicated operations */
    if (OP_copy(op) && (! TN_is_true_pred(pred_tn))) {
      Reset_OP_copy(op);
    }
    if (on_false) {
      FmtAssert(pred_tn != True_TN,("CGTARG_Predicate_OP: invert True TN ?"));
      Set_OP_Pred_False(op, OP_find_opnd_use(op, OU_predicate)); 
    }
    else 
      Set_OP_Pred_True(op, OP_find_opnd_use(op, OU_predicate)); 

    if (!TN_is_true_pred(pred_tn))
      Set_OP_cond_def_kind(op, OP_PREDICATED_DEF);
  }
  else {
    FmtAssert(FALSE,("CGTARG_Predicate_OP: cannot predicate this op"));
  }
}

/* ====================================================================
 *   CGTARG_Dup_OP_Predicate
 *
 *     Duplicate OP and give the copy the predicate NEW_PRED.
 * ====================================================================
 */
OP *
CGTARG_Dup_OP_Predicate (OP* op, TN *new_pred)
{
  OP *result = Dup_OP (op);
  Set_OP_opnd (result, OP_find_opnd_use(result, OU_predicate), new_pred);
  return result;
}


/* ====================================================================
 *   OP_Copy_Operand
 *
 *   Test and return operand for copy operations.
 *   Return the operand index if the operation is a copy from an
 *   immediate value or a register to a register of the same class.
 *   Returns -1 if the operation is not a copy.
 *   This function returns -1 also if the copy is predicated.
 * ====================================================================
 */
INT 
OP_Copy_Operand (
  OP *op
)
{
  TOP opcode = OP_code(op);
  INT pred_idx = OP_find_opnd_use(op, OU_predicate);

  if (opcode == TOP_spadjust) {
    return -1;
  }

  if (OP_cond_def(op)) 
    return -1;

  if (OP_iadd(op) || OP_ior(op) || OP_ixor(op)) {
    INT opnd1_idx = OP_find_opnd_use(op, OU_opnd1);
    INT opnd2_idx = OP_find_opnd_use(op, OU_opnd2);
    
    if (TN_has_value(OP_opnd(op,opnd2_idx)) && TN_value(OP_opnd(op,opnd2_idx)) == 0) {
      return opnd1_idx;
    }
    if (TN_has_value(OP_opnd(op,opnd1_idx)) && TN_value(OP_opnd(op,opnd1_idx)) == 0) {
      return opnd2_idx;
    }
  }
    
  if (OP_iand(op)) {
    INT opnd1_idx = OP_find_opnd_use(op, OU_opnd1);
    INT opnd2_idx = OP_find_opnd_use(op, OU_opnd2);
    if ((TN_has_value(OP_opnd(op,opnd2_idx)) && TN_value(OP_opnd(op,opnd2_idx)) == ~0)) {
      return opnd1_idx;
    }
    if ((TN_has_value(OP_opnd(op,opnd1_idx)) && TN_value(OP_opnd(op,opnd1_idx)) == ~0)) {
      return opnd2_idx;
    }
    if (OP_opnd(op,opnd2_idx) == True_TN) return opnd1_idx;
    if (OP_opnd(op,opnd1_idx) == True_TN) return opnd2_idx;
  }
    
  if (TOP_is_move(opcode)) {
    return OP_find_opnd_use(op, OU_opnd1);
  }
  
  if (TOP_is_xmove(opcode)) {
    return OP_find_opnd_use(op, OU_opnd1);
  }
    
  if (is_TOP_make(opcode) || is_TOP_gp48_make32(opcode)) {
    /* Returns the immediate that is copied. */
    return 1;
  }

  if (opcode == TOP_MAKE) {
    /* Returns the immediate that is copied. */
    return 1;
  }

  /* Ensure that if OP_copy() is true we return a valid operand. Otherwise
     there is a mismatch. */
  FmtAssert(!OP_copy(op), ("operation marked copy, but operand not found on %s",TOP_Name(OP_code(op))));

  return -1;
}

/* ====================================================================
 *   OP_Copy_Result
 *
 *   Returns the result operand index for operations
 *   that have a defined OP_Copy_Operand.
 * ====================================================================
 */
INT 
OP_Copy_Result (
  OP *op
)
{
  return 0;
}

/* ====================================================================
 *   CGTARG_Noop_Top
 * ====================================================================
 */
TOP 
CGTARG_Noop_Top (ISA_EXEC_UNIT_PROPERTY unit) { return TOP_nop; } 

/* ====================================================================
 *   OP_save_predicates/OP_restore_predicates
 * ====================================================================
 */
BOOL OP_save_predicates(OP *op) {
  
 return FALSE; 
}

BOOL OP_restore_predicates(OP *op) {
  return FALSE;
}

/* ====================================================================
 *   OP_is_associative
 *
 *   The list of TOPs that will be handled by the reassociation algorithm.
 * ====================================================================
 */
BOOL
OP_is_associative(OP *op)
{
  TOP top = OP_code(op);

  if (is_TOP_add_r(top) || is_TOP_sub_r(top) ||
      is_TOP_addu_r(top) || is_TOP_subu_r(top)) {
    return true;
  }

  return false;
}

/* ====================================================================
 *   TOP_opposite
 *
 *   Give the opposite form, e.g,  - => +,  + => -.
 *
 *   TODO: belongs to the targ_info.
 * ====================================================================
 */
TOP 
TOP_opposite(TOP top)
{
  if(is_TOP_addu_r(top)) {
    return TOP_subu_r;
  }
  else if(is_TOP_subu_r(top)) {
    return TOP_addu_r;
  }
  else if(is_TOP_add_r(top)) {
    return TOP_sub_r;
  }
  else if(is_TOP_sub_r(top)) {
    return TOP_add_r;
  }
  
  return TOP_UNDEFINED;
}

/* ====================================================================
 *   TOP_immediate
 *
 *   Give the immediate form.
 *
 *   TODO: belongs to the targ_info.
 * ====================================================================
 */
TOP 
TOP_immediate(TOP top)
{
  FmtAssert(FALSE,("TOP_immediate() is deprecated. use TOP_opnd_immediate_variant() instead"));

  return TOP_UNDEFINED;
}


/* ====================================================================
 *   TOP_equiv_nonindex_memory
 *
 *   TODO: belongs to the targ_info.
 * ====================================================================
 */
TOP 
TOP_equiv_nonindex_memory(TOP top)
{
  return TOP_UNDEFINED;
}

/* ====================================================================
 *   CGTARG_Which_OP_Select
 * ====================================================================
 */
TOP
CGTARG_Which_OP_Select ( 
  UINT16 bit_size, 
  BOOL is_float, 
  BOOL is_fcc 
)
{
  return TOP_UNDEFINED;
}

/*
 * STxP70 instructions that read/write carry bit (SR.C).
 * The instructions that access it are:
 * add, addc, addu, addcu,sub,subc, subu, subcu
 * We restrict the code generator to never use add[u],sub[u] for
 * generating a useful flag.
 * In this case we only have to handle:
 * - output dependencies between addc <-> add
 * - flow dependencies between addc -> addc
 * - anti dependencies between addc -> add (redundant with output deps)
 * I.e. the output dependency between add or sub can be ignored.
 */

OP_Flag_Effects
CGTARG_OP_Get_Flag_Effects(const OP *op)
{
  TOP opc = OP_code(op);
  

  if(
     is_TOP_addu_r(opc) ||
     is_TOP_addu_i8(opc) ||
     is_TOP_add_r(opc) ||
     is_TOP_add_i8(opc) ||
     is_TOP_subu_r(opc) ||
     is_TOP_subu_i8(opc) ||
     is_TOP_sub_r(opc) ||
     is_TOP_sub_i8(opc)) {
    if (OP_carryisignored(op)) {
      return OP_FE_CLOBBER;
    } else {
      return OP_FE_WRITE;
    }
  }
  else if(
          is_TOP_addcu(opc) ||
	  is_TOP_addc(opc) ||
	  is_TOP_subc(opc) ||
	  is_TOP_subcu(opc)) {
    return (OP_Flag_Effects)(OP_FE_WRITE|OP_FE_READ);
  }
  return OP_FE_NONE;
}


/* ====================================================================
 *   OP_opnd_can_be_reassociated
 *
 *   Test whether the OPND can be reassociated with the OP.
 * ====================================================================
 */
BOOL
OP_opnd_can_be_reassociated (
  OP *op, 
  INT opnd
)
{
  TOP top = OP_code(op);

  if(is_TOP_add_r(top) || is_TOP_addu_r(top)) {
    return (opnd == 1 || opnd == 2);
  }
  else if(is_TOP_sub_r(top) || is_TOP_subu_r(top)) {
    return (opnd == 1);
  }
    
  return false;
}

/* ====================================================================
 *   OP_other_opnd
 *
 *   The other opnd involved in reassociation
 * ====================================================================
 */
INT 
OP_other_opnd(OP *op, INT this_opnd)
{
  TOP opcode = OP_code(op);

  if(is_TOP_add_r(opcode) ||
     is_TOP_sub_r(opcode) ||
     is_TOP_addu_r(opcode) ||
     is_TOP_subu_r(opcode)) {
    if (this_opnd == 2) return 1;
    if (this_opnd == 1) return 2;
  }

  Is_True(FALSE, ("Other_opnd: wrong opnd num"));
  return 0;
}

/* ====================================================================
 *   CGTARG_Init_OP_cond_def_kind
 * ====================================================================
 */
void 
CGTARG_Init_OP_cond_def_kind (
  OP *op
)
{
  TOP top = OP_code(op);
  if (is_TOP_jr(top)) {
    // (cbr) The following OPs unconditionally define the predicate results.
    Set_OP_cond_def_kind(op, OP_ALWAYS_UNC_DEF);
  } else {
    if (OP_has_predicate(op))
      Set_OP_cond_def_kind(op, OP_PREDICATED_DEF);
    else
      Set_OP_cond_def_kind(op, OP_ALWAYS_UNC_DEF);
  }
}

/* =====================================================================
 *   OP_Is_Unconditional_Compare
 * =====================================================================
 */
BOOL
OP_Is_Unconditional_Compare (
  OP *op
)
{
  return (FALSE);
}


/*
 * TOP_opnd_immediate_variant_default
 *
 * Default target independent implementation for
 * TOP_opnd_immediate_variant.
 * The implementation is driven by the targinfo variant 
 * description (see isa_variant.cxx).
 * VARATT_immediate gives for any register or immediate operator the first immediate form.
 * VARATT_next_immediate gives the next larger immediate form.
 * Note that this model works only when 1 operand varies for the operator.
 * The matching test is done on the literal class for the given operand.
 */
static TOP
TOP_opnd_immediate_variant_default(TOP regform, int opnd, INT64 imm)
{
  TOP immform;

  // VCDV - bug #63273. avoid immediate variant for var-arg TOPs like top_asm
  if (opnd >= TOP_fixed_opnds(regform)) {
    return TOP_UNDEFINED;
  }

  // Check if it is already an immediate form
  if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, regform)) {
    const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info (regform);
    const ISA_OPERAND_VALTYP *otype = ISA_OPERAND_INFO_Operand(oinfo, opnd);
    if (ISA_OPERAND_VALTYP_Is_Literal(otype)) {
      immform = regform;
      // Get to the first immediate variant.
      TOP prevform = TOP_get_variant(immform, VARATT_prev_immediate);
      while (prevform != TOP_UNDEFINED && ISA_SUBSET_LIST_Member (ISA_SUBSET_List, prevform)) {
	immform = prevform;
	prevform = TOP_get_variant(prevform, VARATT_prev_immediate);
      }
    } else {
      immform = TOP_get_variant(regform, VARATT_immediate);
    }
  }  
  
  // Get the first immediate form that fits the operand
  while (immform != TOP_UNDEFINED && ISA_SUBSET_LIST_Member (ISA_SUBSET_List, immform)) {
    const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info (immform);
    const ISA_OPERAND_VALTYP *otype = ISA_OPERAND_INFO_Operand(oinfo, opnd);
    if (ISA_OPERAND_VALTYP_Is_Literal(otype)) {
      ISA_LIT_CLASS lit_class = ISA_OPERAND_VALTYP_Literal_Class(otype);
      
      if (ISA_LC_Value_In_Class (imm, lit_class))
	break;
    }
    immform = TOP_get_variant(immform, VARATT_next_immediate);
    
  }
  return immform;
}

/*
 * TOP_opnd_immediate_variant
 *
 * Returns the TOP immediate variant, depending on the immediate value.
 * Target dependent.
 * The reg form may be a register or immediate form opcode.
 * opnd is the operand number that may be replaced (0..2).
 * imm is the immediate value that should be encoded.
 * Returns TOP_UNDEFINED, if no immediate variant is available.
 */
TOP
TOP_opnd_immediate_variant(TOP regform, int opnd, INT64 imm)
{
  // No target dependent specificities.
  return TOP_opnd_immediate_variant_default(regform, opnd, imm);
}


/*
 * TOP_opnd_register_variant_default
 *
 * Default target independent implementation for
 * TOP_opend_register_variant.
 * The implementation is driven by the targinfo variant 
 * description (see isa_variant.cxx).
 * VARATT_immediate gives for any register or immediate operator the first immediate form.
 * VARATT_next_immediate gives the next larger immediate form.
 * We use these two variants to generate the reverse mapping which
 * gives for each top immediate or immediate next the initial register variant.
 */
extern TOP CGTARG_TOP_Register_Variant(TOP immform_top); /* Defined in targ_cg.cxx. */
static TOP
TOP_opnd_register_variant_default(TOP immform, int opnd, ISA_REGISTER_CLASS regclass)
{
  TOP regform;

  // Extra arguments of 'var_opnds' TOP are not candidates
  if (opnd >= TOP_fixed_opnds(immform)) {
    return TOP_UNDEFINED;
  }
 
  // Check if already a register variant.
  if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, immform)) {
    const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info (immform);
    const ISA_OPERAND_VALTYP *otype = ISA_OPERAND_INFO_Operand(oinfo, opnd);
    if (ISA_OPERAND_VALTYP_Is_Register(otype) &&
	regclass == ISA_OPERAND_VALTYP_Register_Class(otype))
      return immform;
  }
  regform = CGTARG_TOP_Register_Variant(immform);
  
  if (regform != TOP_UNDEFINED && ISA_SUBSET_LIST_Member (ISA_SUBSET_List, regform)) {
    const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info (regform);
    const ISA_OPERAND_VALTYP *otype = ISA_OPERAND_INFO_Operand(oinfo, opnd);
    if (ISA_OPERAND_VALTYP_Is_Register(otype)) {
      if (regclass == ISA_OPERAND_VALTYP_Register_Class(otype))
	return regform;
    }
  }
  return TOP_UNDEFINED;
}

/*
 * TOP_opnd_register_variant
 *
 * Returns the TOP register variant, matching the given register class.
 * Target dependent.
 * opnd is the operand number that may be replaced (0..2).
 * regclass is the requested register class.
 * Returns TOP_UNDEFINED, if no register variant is available.
 */
TOP
TOP_opnd_register_variant(TOP regform, int opnd, ISA_REGISTER_CLASS regclass)
{
  // No target dependent specificities.
  return TOP_opnd_register_variant_default(regform, opnd, regclass);
}

/*
 * TOP_opnd_swapped_variant
 * Returns the TOP corresponding to an invertion of the 2 operands index.
 * For commutative tops on the index, return the same top.
 * For inversible tops, return the inversed top.
 * [DT] Warning : This function will not work on copare inst => will return TOP_UNDEFINED
 */
TOP
OP_opnd_swapped_variant(OP* op, int opnd1, int opnd2)
{
  TOP top = OP_code(op);
#define IF_TOP(opcode,top) if(opcode == TOP_##top##_r) {	\
    return TOP_##top##_r;						\
  }
#define IF_TOP_INV(opcode,top,newtop) if(opcode == TOP_##top##_r) {	\
    return TOP_##newtop##_r;						\
}

  if (opnd1 > opnd2) {
    int tmp = opnd1;
    opnd1 = opnd2;
    opnd2 = tmp;
  }
  
  if (opnd1 == 1 && opnd2 == 2) {
    IF_TOP(top,add)
    else IF_TOP(top,addu)
    else IF_TOP(top,and)
    else IF_TOP(top,or)
    else IF_TOP(top,xor)
    else IF_TOP(top,max)
    else IF_TOP(top,maxu)
    else IF_TOP(top,min)
    else IF_TOP(top,minu)
      }
  return TOP_UNDEFINED;
#undef IF_TOP
#undef IF_TOP_INV
}

/*
 * TOP_result_register_variant
 * Returns the TOP variant for generating a result in the given register
 * class with the same operand types.
 * The reg form may be a register or immediate opcode.
 * rslt is the result number that may be replaced (0..1).
 */
TOP
TOP_result_register_variant(TOP regform, int rslt, ISA_REGISTER_CLASS regclass)
{

  return TOP_UNDEFINED;
}

/*
 * TOP_opnd_use_bits
 * Return the effective bits used for the given operand.
 * In case of immediate operand, returns the used bits after the
 * optional sign extension.
 * Return -1 for undefined semantic
 */
INT
TOP_opnd_use_bits(TOP top, int opnd)
{
  int use_bits;
  const ISA_OPERAND_INFO *oinfo;
  const ISA_OPERAND_VALTYP *vtype;
  
  // No info on extra arguments of 'var_opnds' TOP
  if (opnd >= TOP_fixed_opnds(top)) {
    return -1;
  }

  // Default cases depend on register class.
  // ISA_REGISTER_CLASS_gpr defaults to 32 bits, signed
  // ISA_REGISTER_CLASS_gr default to 1 bit unsigned
  // Non registers default to 32 bit signed
  oinfo = ISA_OPERAND_Info(top);
  vtype = ISA_OPERAND_INFO_Operand(oinfo, opnd);

  use_bits = ISA_OPERAND_VALTYP_Size(vtype);
  if (TOP_is_store(top) && opnd == TOP_Find_Operand_Use(top,OU_storeval)) {
    use_bits = TOP_Mem_Bytes(top)*8;
  }

 // The cases below 
  if(is_TOP_movel2h(top) ||
     is_TOP_movel2l(top)) {
    if (opnd == 1) use_bits = 16;
  }
  else if(is_TOP_exth(top) ||
	  is_TOP_extuh(top)) {
    if(opnd == 1) use_bits = 16;
  }
  else if(is_TOP_extb(top) ||
	  is_TOP_extub(top)) {
    if (opnd == 1) use_bits = 8;
  }
  else if(is_TOP_shl_r(top) ||
	  is_TOP_shlu_r(top) ||
	  is_TOP_shr_r(top) ||
	  is_TOP_shru_r(top)) {
    // This is the size of the bits fetched by the shifts.
    if (opnd == 2) use_bits = 5;
  }
  
  return use_bits;
}


/*
 * TOP_opnd_use_signed
 * Returns true if the extension os the effective use bits is
 * signed for the semantic of the TOP.
 * For instance a 32x16->32 unsigned multiply should have the following 
 * properties:
 * TOP_opnd_use_bits(top, opnd1) == 32
 * TOP_opnd_use_signed(top, opnd1) == FALSE
 * TOP_opnd_use_bits(top, opnd2) == 16
 * TOP_opnd_use_signed(top, opnd2) == FALSE
 *
 * Default is to return -1 for undefined semantic.
 */
INT
TOP_opnd_use_signed(TOP top, int opnd)
{
  INT is_signed;
  const ISA_OPERAND_INFO *oinfo;
  const ISA_OPERAND_VALTYP *vtype;
  ISA_REGISTER_CLASS rc;
  
  // Undefined semantic for extra arguments of 'var_opnds' TOP
  if (opnd >= TOP_fixed_opnds(top)) {
    return -1;
  }

  // Default cases depend on operand value type.
  oinfo = ISA_OPERAND_Info(top);
  vtype = ISA_OPERAND_INFO_Operand(oinfo, opnd);

  is_signed =  ISA_OPERAND_VALTYP_Is_Signed(vtype);

  if (is_TOP_extuh(top) || is_TOP_extub(top)
      // hack for xp70-v4 for the moment [vcdv] TODO
      || top==TOP_v4_gp32_extub || top==TOP_v4_gp32_extuh
      ) {
    if (opnd == 1) is_signed = FALSE;
  }
  else if(is_TOP_shl_r(top) ||
          is_TOP_shlu_r(top) ||
          is_TOP_shr_r(top)) {
    // Shift amount is interpreted unsigned
    if (opnd == 2) is_signed = FALSE;
  }
  else if(is_TOP_shru_r(top) ||
          is_TOP_shru_i5(top)) { // Only for opnd1, opnd2 is already defined unsigned ..
    // Shift amount and shifted value are interpreted unsigned
    if (opnd == 2 || opnd == 1) is_signed = FALSE;
  }
  
  return is_signed;
}

TOP
TOP_AM_automod_variant(TOP top, BOOL post_mod, BOOL inc_mod, ISA_REGISTER_CLASS regclass) {

#define TOP_AM_i_r(top,x)				\
  if ((is_TOP_##x##_i8_dec(top) || is_TOP_##x##_i8_inc(top) || is_TOP_##x##_i12_inc(top)) && (regclass == ISA_REGISTER_CLASS_UNDEFINED)) {	\
    if (post_mod)	return inc_mod ?  TOP_##x##_i5_post_inc :  TOP_##x##_i5_post_dec; \
    else if (!inc_mod) return TOP_##x##_i5_pre_dec;				\
  } else { \
    if((is_TOP_##x##_i8_dec(top) || is_TOP_##x##_i8_inc(top) || is_TOP_##x##_i12_inc(top) || is_TOP_##x##_r_inc(top)) && post_mod && inc_mod && (regclass == ISA_REGISTER_CLASS_gpr)) \
	  return TOP_##x##_r_post_inc;			\
  } 

#define TOP_AM_i(top,x)				      \
	if (is_TOP_##x##_i8_dec(top) || is_TOP_##x##_i8_inc(top) || is_TOP_##x##_i12_inc(top) ) { \
	    if (regclass == ISA_REGISTER_CLASS_UNDEFINED) {   \
	      if (post_mod)	return inc_mod ?  TOP_##x##_i5_post_inc : TOP_##x##_i5_post_dec; \
	      else if (!inc_mod) return TOP_##x##_i5_pre_dec;				\
	    }	\
	}
  // Reconfigurability: Extension TOPs are not handled here
  if (top >= TOP_static_count) {
	  return EXTENSION_TOP_AM_automod_variant(top, post_mod, inc_mod, regclass);
  }

  TOP_AM_i_r(top,fpx_flw)
  TOP_AM_i_r(top,fpx_fsw)
  TOP_AM_i_r(top,fpx_fswf2i)
  TOP_AM_i_r(top,lb)
  TOP_AM_i_r(top,lgs)
  TOP_AM_i_r(top,lh)
  TOP_AM_i_r(top,lsetub)
  TOP_AM_i_r(top,lsfr)
  TOP_AM_i_r(top,lub)
  TOP_AM_i_r(top,luh)
  TOP_AM_i_r(top,lw)

  // FdF 20060612: Rn !+ Rp not supported for core store operation
  TOP_AM_i(top,sb)
  TOP_AM_i(top,sh)
  TOP_AM_i(top,ssfr)
  TOP_AM_i(top,sw)

  return TOP_UNDEFINED;
}

/*
 * TOP_evaluate_op
 * Return the evaluated expression corresponding to given TOP and
 * the given result number.
 * Return FALSE if not able to evaluate.
 */
BOOL
TOP_evaluate_top( OP *op, INT64 *opnd_values, INT64 *result_val, int result_idx ) {
  return FALSE;
}

/*
 * OP_get_unconditional_variant
 * Return the unpredicated variant if given op.
 * TODO: Make the opposite form to get a predicated variant (if-conversion).
 */
OP *
OP_get_unconditional_variant( OP *op ) {
  return NULL;
}

/*
 * OP_condition_is_true
 * In case of true/false predication model,
 * returns TRUE if predicate not equal to zero, else returns FALSE.
 * On ARM, we have to get the condvariant on the predicate to evaluate
 * if the condition.
 */
BOOL
OP_condition_is_true( OP *op, INT64 pred_val ) {
  return pred_val != 0;
}

void
targ_cg_init_targ_op ()
{
#if 0
  // For debugging purpose
  INT i;
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      printf ("TOP_opnd_swapped_variant(%s, 0, 1) = %s\n",
	      TOP_Name (top), TOP_Name(TOP_opnd_swapped_variant (top, 0, 1)));
    }
  }
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      printf ("TOP_opnd_swapped_variant(%s, 1, 2) = %s\n",
	      TOP_Name (top), TOP_Name(TOP_opnd_swapped_variant (top, 1, 2)));
    }
  }
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      printf ("TOP_result_register_variant(%s, 0, ISA_REGISTER_CLASS_integer) = %s\n",
	      TOP_Name (top), TOP_Name(TOP_result_register_variant (top, 0, ISA_REGISTER_CLASS_integer)));
    }
  }
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      printf ("TOP_result_register_variant(%s, 0, ISA_REGISTER_CLASS_branch) = %s\n",
	      TOP_Name (top), TOP_Name(TOP_result_register_variant (top, 0, ISA_REGISTER_CLASS_branch)));
    }
  }
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info(top);
      for (INT j = 0; j < ISA_OPERAND_INFO_Operands(oinfo); j++) {
	printf ("TOP_opnd_use_bits(%s, %d) = %d\n",
		TOP_Name (top), j, TOP_opnd_use_bits(top, j));
      }
    }
  }
  for (i = 0; i < TOP_count; i++) {
    TOP top = (TOP)i;
    if (ISA_SUBSET_LIST_Member (ISA_SUBSET_List, top)) {
      const ISA_OPERAND_INFO *oinfo = ISA_OPERAND_Info(top);
      for (INT j = 0; j < ISA_OPERAND_INFO_Operands(oinfo); j++) {
	printf ("TOP_opnd_immediate_variant(%s, %d, 0) = %s\n",
		TOP_Name (top), j, TOP_Name (TOP_opnd_immediate_variant(top, j, 0)));
	printf ("TOP_opnd_immediate_variant(%s, %d, 1000) = %s\n",
	      TOP_Name (top), j, TOP_Name (TOP_opnd_immediate_variant(top, j, 1000)));
      }
    }
  }
#endif
	    
}

/**
 * CGTARG_OP_Make_movc
 *
 * Generate a conditional move with predicate information.
 * If no real operation exists, generate a simulated one.
 */
void
CGTARG_OP_Make_movc( TN *guard,
                     TN *dst,
                     TN *src,
                     OPS *cmov_ops,
                     bool on_false
                     ) {

  if (TN_register_class(dst) == ISA_REGISTER_CLASS_gr) {
    // On xp70, there is no single operation to generate a
    // predicated copy of guard registers
    // -> Generate a simulated OP instead.
    Build_OP(TOP_MOVC_GR, dst, guard, src, cmov_ops);
  }
  else {
    Expand_Copy(dst, guard, src, cmov_ops);
  }

  if (on_false) {
    OP *mop = OPS_last(cmov_ops);
    Set_OP_Pred_False(mop, OP_find_opnd_use(mop, OU_predicate));
  }
}

/**
 * CGTARG_OP_Lower_movc
 *
 * If argument <op> is a simulated operation representing a conditional move,
 * generates the real target code to perform the move and return TRUE.
 * Otherwise return FALSE.
 */
BOOL
CGTARG_OP_Lower_movc( OP *op,
                      OPS *lowered_ops
                      ) {

  if (OP_code(op) != TOP_MOVC_GR) {
    return FALSE;
  }
  
  if (OP_cond_def_kind(op) == OP_ALWAYS_UNC_DEF) {
    Exp_COPY(OP_result(op, 0), OP_opnd(op, 1), lowered_ops);
  }
  else {
    if (OP_Pred_False(op, OP_find_opnd_use(op, OU_predicate))) {
      Expand_Copy_On_False_Predicate(OP_result(op, 0), OP_opnd(op, 0), OP_opnd(op, 1), lowered_ops);
    }
    else {
      Expand_Copy(OP_result(op, 0), OP_opnd(op, 0), OP_opnd(op, 1), lowered_ops);
    }
  }
  return TRUE;
}
