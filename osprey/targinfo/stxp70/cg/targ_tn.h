/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


/* ====================================================================
 * ====================================================================
 *
 *  TN utility routines which include target dependencies.
 *
 *  THIS FILE IS ONLY TO BE INCLUDE BY ../tn.h!!!!
 *
 * ====================================================================
 * ====================================================================
 */

/* ====================================================================
 *
 *   Description:
 *
 *   This module defines a number of functions used by the cg phase
 *   of the compiler. These are the target dependent interface 
 *   functions for handling the TNs.
 *
 *   Macros to check if a TN is a particular dedicated register:
 *
 *     TN_is_zero_reg(r)
 *     TN_is_sp_reg(r)
 *     TN_is_gp_reg(r)
 *     TN_is_fp_reg(r)
 *     TN_is_ra_reg(r)
 *     TN_is_lc_reg(r)
 *     TN_is_true_pred(r)
 *
 *   Exported Interface:
 *
 *   BOOL TN_is_const_reg(
 *                  const TN *tn)
 *
 *       Returns TRUE if the TN represents a hardwired registers.
 *
 *   BOOL TN_is_fcc_register (
 *                  const TN *tn)
 *
 *       Returns TRUE if the TN is a conditional code register.
 *
 *   TN *Gen_Predicate_TN ()
 *
 *       Generate TN to contain a general predicate value
 *
 * ====================================================================
 */

#ifndef tn_targ_INCLUDED
#define tn_targ_INCLUDED

#include "vstring.h"
#include "targ_isa_relocs.h"

/* Target-specific special registers: */


/* ====================================================================
 * ====================================================================
 *                  Define the TN_relocs values 
 * ====================================================================
 * ====================================================================
 */

// [JV] For compatibility.
#define  TN_RELOC_NONE	          ISA_RELOC_UNDEFINED

// ---------------------------------------------------------------------
inline BOOL TN_is_fcc_register (const TN *tn)
{
  return FALSE;
}

#endif /* tn_targ_INCLUDED */
