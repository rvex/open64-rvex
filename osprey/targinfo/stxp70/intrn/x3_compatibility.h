/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */

#ifdef __X3

/* V4 compatiblity layer mp[n]us intructions are not available on v4
   but may be replaced by mp[n]su intructions by exchanging their
   paremeters
*/
#ifdef __STxP70_V4__

#define x3_mpnushh_f(a, b) x3_mpnsuhh_f(b, a)
#define x3_mpnushh(a, b, c) x3_mpnsuhh(a, c, b) 

#define x3_mpnushl_f(a, b) x3_mpnsulh_f(b, a)
#define x3_mpnushl(a, b, c) x3_mpnsulh(a, c, b) 

#define x3_mpnuslh_f(a, b) x3_mpnsuhl_f(b, a)
#define x3_mpnuslh(a, b, c) x3_mpnsuhl(a, c, b) 

#define x3_mpnusll_f(a, b) x3_mpnsull_f(b, a)
#define x3_mpnusll(a, b, c) x3_mpnsull(a, c, b) 


#define x3_mpushh_f(a, b) x3_mpsuhh_f(b, a)
#define x3_mpushh(a, b, c) x3_mpsuhh(a, c, b) 

#define x3_mpushl_f(a, b) x3_mpsulh_f(b, a)
#define x3_mpushl(a, b, c) x3_mpsulh(a, c, b) 

#define x3_mpuslh_f(a, b) x3_mpsuhl_f(b, a)
#define x3_mpuslh(a, b, c) x3_mpsuhl(a, c, b) 

#define x3_mpusll_f(a, b) x3_mpsull_f(b, a)
#define x3_mpusll(a, b, c) x3_mpsull(a, c, b) 

#endif

#endif
