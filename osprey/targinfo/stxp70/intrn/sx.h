/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */



#ifndef _SX_H_
#define _SX_H_

#include "sx_compatibility.h"

#if defined(__SX) && !defined(__SX_C_MODEL)

/* Macros mapped on builtins for class sx */
#include <builtins_sx.h>

#ifdef __STxP70_V4__
/*  make32 gpr, <s32>
 * gpr make32( <s32>);
 */
#define sx_make32_f  __builtin_sx_make32
#define sx_make32(a, b) { \
  a = __builtin_sx_make32(b); \
}

/*  moveg2r gpr, ISA_REGISTER_SUBCLASS_gr_dst
 * gpr moveg2r( ISA_REGISTER_SUBCLASS_gr_dst);
 */
#define sx_moveg2r_f  __builtin_sx_moveg2r
#define sx_moveg2r(a, b) { \
  a = __builtin_sx_moveg2r(b); \
}

/*  mover2g ISA_REGISTER_SUBCLASS_gr_dst, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst mover2g( gpr);
 */
#define sx_mover2g_f  __builtin_sx_mover2g
#define sx_mover2g(a, b) { \
  a = __builtin_sx_mover2g(b); \
}

#endif

/*  absbp gpr, gpr
 * gpr absbp( gpr);
 */
#define sx_absbp_f  __builtin_sx_absbp
#define sx_absbp(a, b) { \
  a = __builtin_sx_absbp(b); \
}

/*  abshp gpr, gpr
 * gpr abshp( gpr);
 */
#define sx_abshp_f  __builtin_sx_abshp
#define sx_abshp(a, b) { \
  a = __builtin_sx_abshp(b); \
}

/*  absubp gpr, gpr
 * gpr absubp( gpr);
 */
#define sx_absubp_f  __builtin_sx_absubp
#define sx_absubp(a, b) { \
  a = __builtin_sx_absubp(b); \
}

/*  absuhp gpr, gpr
 * gpr absuhp( gpr);
 */
#define sx_absuhp_f  __builtin_sx_absuhp
#define sx_absuhp(a, b) { \
  a = __builtin_sx_absuhp(b); \
}

/*  absu gpr, gpr
 * gpr absu( gpr);
 */
#define sx_absu_f  __builtin_sx_absu
#define sx_absu(a, b) { \
  a = __builtin_sx_absu(b); \
}

/*  abs gpr, gpr
 * gpr abs( gpr);
 */
#define sx_abs_f  __builtin_sx_abs
#define sx_abs(a, b) { \
  a = __builtin_sx_abs(b); \
}

/*  addbp gpr, gpr, gpr
 * gpr addbp( gpr, gpr);
 */
#define sx_addbp_r_f  __builtin_sx_addbp_r
#define sx_addbp_r(a, b, c) { \
  a = __builtin_sx_addbp_r(b, c); \
}

/*  addbp gpr, gpr, <u8>
 * gpr addbp( gpr, <u8>);
 */
#define sx_addbp_i8_f  __builtin_sx_addbp_i8
#define sx_addbp_i8(a, b, c) { \
  a = __builtin_sx_addbp_i8(b, c); \
}

/*  addcu gpr, gpr, gpr
 * gpr addcu( gpr, gpr);
 */
#define sx_addcu_f  __builtin_sx_addcu
#define sx_addcu(a, b, c) { \
  a = __builtin_sx_addcu(b, c); \
}

/*  addc gpr, gpr, gpr
 * gpr addc( gpr, gpr);
 */
#define sx_addc_f  __builtin_sx_addc
#define sx_addc(a, b, c) { \
  a = __builtin_sx_addc(b, c); \
}

/*  addhp gpr, gpr, gpr
 * gpr addhp( gpr, gpr);
 */
#define sx_addhp_r_f  __builtin_sx_addhp_r
#define sx_addhp_r(a, b, c) { \
  a = __builtin_sx_addhp_r(b, c); \
}

/*  addhp gpr, gpr, <u8>
 * gpr addhp( gpr, <u8>);
 */
#define sx_addhp_i8_f  __builtin_sx_addhp_i8
#define sx_addhp_i8(a, b, c) { \
  a = __builtin_sx_addhp_i8(b, c); \
}

/*  addubp gpr, gpr, gpr
 * gpr addubp( gpr, gpr);
 */
#define sx_addubp_r_f  __builtin_sx_addubp_r
#define sx_addubp_r(a, b, c) { \
  a = __builtin_sx_addubp_r(b, c); \
}

/*  addubp gpr, gpr, <u8>
 * gpr addubp( gpr, <u8>);
 */
#define sx_addubp_i8_f  __builtin_sx_addubp_i8
#define sx_addubp_i8(a, b, c) { \
  a = __builtin_sx_addubp_i8(b, c); \
}

/*  addugp gpr, <da0to15_gprel0to15_gotoffs0to15_u16>
 * gpr addugp( <da0to15_gprel0to15_gotoffs0to15_u16>);
 */
#define sx_addugp_f  __builtin_sx_addugp
#define sx_addugp(a, b) { \
  a = __builtin_sx_addugp(b); \
}

/*  adduhp gpr, gpr, gpr
 * gpr adduhp( gpr, gpr);
 */
#define sx_adduhp_r_f  __builtin_sx_adduhp_r
#define sx_adduhp_r(a, b, c) { \
  a = __builtin_sx_adduhp_r(b, c); \
}

/*  adduhp gpr, gpr, <u8>
 * gpr adduhp( gpr, <u8>);
 */
#define sx_adduhp_i8_f  __builtin_sx_adduhp_i8
#define sx_adduhp_i8(a, b, c) { \
  a = __builtin_sx_adduhp_i8(b, c); \
}

/*  addur gpr, gpr
 * gpr addur( gpr);
 */
#define sx_addur_r_f  __builtin_sx_addur_r
#define sx_addur_r(a, b) { \
  a = __builtin_sx_addur_r(b); \
}

/*  addur gpr, <rel2to17_neggprel_scf2_s16>
 * gpr addur( <rel2to17_neggprel_scf2_s16>);
 */
#define sx_addur_i16_f  __builtin_sx_addur_i16
#define sx_addur_i16(a, b) { \
  a = __builtin_sx_addur_i16(b); \
}

/*  addu gpr, gpr, gpr
 * gpr addu( gpr, gpr);
 */
#define sx_addu_r_f  __builtin_sx_addu_r
#define sx_addu_r(a, b, c) { \
  a = __builtin_sx_addu_r(b, c); \
}

/*  addu gpr, gpr, <u8>
 * gpr addu( gpr, <u8>);
 */
#define sx_addu_i8_f  __builtin_sx_addu_i8
#define sx_addu_i8(a, b, c) { \
  a = __builtin_sx_addu_i8(b, c); \
}

/*  add gpr, gpr, gpr
 * gpr add( gpr, gpr);
 */
#define sx_add_r_f  __builtin_sx_add_r
#define sx_add_r(a, b, c) { \
  a = __builtin_sx_add_r(b, c); \
}

/*  add gpr, gpr, <u8>
 * gpr add( gpr, <u8>);
 */
#define sx_add_i8_f  __builtin_sx_add_i8
#define sx_add_i8(a, b, c) { \
  a = __builtin_sx_add_i8(b, c); \
}

/*  andg ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst andg( gr, gr);
 */
#define sx_andg_f  __builtin_sx_andg
#define sx_andg(a, b, c) { \
  a = __builtin_sx_andg(b, c); \
}

/*  andng ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst andng( gr, gr);
 */
#define sx_andng_f  __builtin_sx_andng
#define sx_andng(a, b, c) { \
  a = __builtin_sx_andng(b, c); \
}

/*  andn gpr, gpr, gpr
 * gpr andn( gpr, gpr);
 */
#define sx_andn_f  __builtin_sx_andn
#define sx_andn(a, b, c) { \
  a = __builtin_sx_andn(b, c); \
}

/*  and gpr, gpr, gpr
 * gpr and( gpr, gpr);
 */
#define sx_and_r_f  __builtin_sx_and_r
#define sx_and_r(a, b, c) { \
  a = __builtin_sx_and_r(b, c); \
}

/*  and gpr, gpr, <u8>
 * gpr and( gpr, <u8>);
 */
#define sx_and_i8_f  __builtin_sx_and_i8
#define sx_and_i8(a, b, c) { \
  a = __builtin_sx_and_i8(b, c); \
}

/* barrier
 * barrier( );
 */
#define sx_barrier_f  __builtin_sx_barrier
#define sx_barrier() { \
  __builtin_sx_barrier(); \
}

/* bkp
 * bkp( );
 */
#define sx_bkp_f  __builtin_sx_bkp
#define sx_bkp() { \
  __builtin_sx_bkp(); \
}

#ifdef __STxP70_V3__
/*  boolbp gpr, gr
 * gpr boolbp( gr);
 */
#define sx_boolbp_f  __builtin_sx_boolbp
#define sx_boolbp(a, b) { \
  a = __builtin_sx_boolbp(b); \
}

/*  boolhp gpr, gr
 * gpr boolhp( gr);
 */
#define sx_boolhp_f  __builtin_sx_boolhp
#define sx_boolhp(a, b) { \
  a = __builtin_sx_boolhp(b); \
}

#endif

/*  bool gpr, gr
 * gpr bool( gr);
 */
#define sx_bool_f  __builtin_sx_bool
#define sx_bool(a, b) { \
  a = __builtin_sx_bool(b); \
}

/*  bISA_EC_bitop gpr, gpr, gpr
 * gpr b( ISA_EC_bitop, gpr, gpr);
 */
#define sx_bclr_r_f  __builtin_sx_bclr_r
#define sx_bclr_r(a, c, d) { \
  a = __builtin_sx_b_r(intrn_bitop_clr, c, d); \
}

#define sx_bset_r_f  __builtin_sx_bset_r
#define sx_bset_r(a, c, d) { \
  a = __builtin_sx_b_r(intrn_bitop_set, c, d); \
}

#define sx_bnot_r_f  __builtin_sx_bnot_r
#define sx_bnot_r(a, c, d) { \
  a = __builtin_sx_b_r(intrn_bitop_not, c, d); \
}

/*  bISA_EC_bitop gpr, gpr, <u5>
 * gpr b( ISA_EC_bitop, gpr, <u5>);
 */
#define sx_bclr_i5_f  __builtin_sx_bclr_i5
#define sx_bclr_i5(a, c, d) { \
  a = __builtin_sx_b_i5(intrn_bitop_clr, c, d); \
}

#define sx_bset_i5_f  __builtin_sx_bset_i5
#define sx_bset_i5(a, c, d) { \
  a = __builtin_sx_b_i5(intrn_bitop_set, c, d); \
}

#define sx_bnot_i5_f  __builtin_sx_bnot_i5
#define sx_bnot_i5(a, c, d) { \
  a = __builtin_sx_b_i5(intrn_bitop_not, c, d); \
}

/*  clamph gpr, gpr
 * gpr clamph( gpr);
 */
#define sx_clamph_f  __builtin_sx_clamph
#define sx_clamph(a, b) { \
  a = __builtin_sx_clamph(b); \
}

/*  cmpISA_EC_cmpU ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_u_eq_f  __builtin_sx_cmp_r_u_eq
#define sx_cmp_r_u_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_u_ne_f  __builtin_sx_cmp_r_u_ne
#define sx_cmp_r_u_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_u_ge_f  __builtin_sx_cmp_r_u_ge
#define sx_cmp_r_u_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_u_lt_f  __builtin_sx_cmp_r_u_lt
#define sx_cmp_r_u_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_u_le_f  __builtin_sx_cmp_r_u_le
#define sx_cmp_r_u_le(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_u_gt_f  __builtin_sx_cmp_r_u_gt
#define sx_cmp_r_u_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_u_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpU ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_u_eq_f  __builtin_sx_cmp_i8_u_eq
#define sx_cmp_i8_u_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_u_ne_f  __builtin_sx_cmp_i8_u_ne
#define sx_cmp_i8_u_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_u_ge_f  __builtin_sx_cmp_i8_u_ge
#define sx_cmp_i8_u_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_u_lt_f  __builtin_sx_cmp_i8_u_lt
#define sx_cmp_i8_u_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_u_le_f  __builtin_sx_cmp_i8_u_le
#define sx_cmp_i8_u_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_u_gt_f  __builtin_sx_cmp_i8_u_gt
#define sx_cmp_i8_u_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_u_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUH ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_uh_eq_f  __builtin_sx_cmp_r_uh_eq
#define sx_cmp_r_uh_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_uh_ne_f  __builtin_sx_cmp_r_uh_ne
#define sx_cmp_r_uh_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_uh_ge_f  __builtin_sx_cmp_r_uh_ge
#define sx_cmp_r_uh_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_uh_lt_f  __builtin_sx_cmp_r_uh_lt
#define sx_cmp_r_uh_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_uh_le_f  __builtin_sx_cmp_r_uh_le
#define sx_cmp_r_uh_le(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_uh_gt_f  __builtin_sx_cmp_r_uh_gt
#define sx_cmp_r_uh_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_uh_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUH ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_uh_eq_f  __builtin_sx_cmp_i8_uh_eq
#define sx_cmp_i8_uh_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_uh_ne_f  __builtin_sx_cmp_i8_uh_ne
#define sx_cmp_i8_uh_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_uh_ge_f  __builtin_sx_cmp_i8_uh_ge
#define sx_cmp_i8_uh_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_uh_lt_f  __builtin_sx_cmp_i8_uh_lt
#define sx_cmp_i8_uh_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_uh_le_f  __builtin_sx_cmp_i8_uh_le
#define sx_cmp_i8_uh_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_uh_gt_f  __builtin_sx_cmp_i8_uh_gt
#define sx_cmp_i8_uh_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_uh_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUB ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_ub_eq_f  __builtin_sx_cmp_r_ub_eq
#define sx_cmp_r_ub_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_ub_ne_f  __builtin_sx_cmp_r_ub_ne
#define sx_cmp_r_ub_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_ub_ge_f  __builtin_sx_cmp_r_ub_ge
#define sx_cmp_r_ub_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_ub_lt_f  __builtin_sx_cmp_r_ub_lt
#define sx_cmp_r_ub_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_ub_le_f  __builtin_sx_cmp_r_ub_le
#define sx_cmp_r_ub_le(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_ub_gt_f  __builtin_sx_cmp_r_ub_gt
#define sx_cmp_r_ub_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_ub_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUB ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_ub_eq_f  __builtin_sx_cmp_i8_ub_eq
#define sx_cmp_i8_ub_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_ub_ne_f  __builtin_sx_cmp_i8_ub_ne
#define sx_cmp_i8_ub_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_ub_ge_f  __builtin_sx_cmp_i8_ub_ge
#define sx_cmp_i8_ub_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_ub_lt_f  __builtin_sx_cmp_i8_ub_lt
#define sx_cmp_i8_ub_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_ub_le_f  __builtin_sx_cmp_i8_ub_le
#define sx_cmp_i8_ub_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_ub_gt_f  __builtin_sx_cmp_i8_ub_gt
#define sx_cmp_i8_ub_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_ub_cmp(intrn_cmp_gt, c, d); \
}

#ifdef __STxP70_V3__
/*  cmpISA_EC_cmpUHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_uhp_eq_f  __builtin_sx_cmp_r_uhp_eq
#define sx_cmp_r_uhp_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_uhp_ne_f  __builtin_sx_cmp_r_uhp_ne
#define sx_cmp_r_uhp_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_uhp_ge_f  __builtin_sx_cmp_r_uhp_ge
#define sx_cmp_r_uhp_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_uhp_lt_f  __builtin_sx_cmp_r_uhp_lt
#define sx_cmp_r_uhp_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_uhp_le_f  __builtin_sx_cmp_r_uhp_le
#define sx_cmp_r_uhp_le(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_uhp_gt_f  __builtin_sx_cmp_r_uhp_gt
#define sx_cmp_r_uhp_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_uhp_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_uhp_eq_f  __builtin_sx_cmp_i8_uhp_eq
#define sx_cmp_i8_uhp_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_uhp_ne_f  __builtin_sx_cmp_i8_uhp_ne
#define sx_cmp_i8_uhp_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_uhp_ge_f  __builtin_sx_cmp_i8_uhp_ge
#define sx_cmp_i8_uhp_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_uhp_lt_f  __builtin_sx_cmp_i8_uhp_lt
#define sx_cmp_i8_uhp_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_uhp_le_f  __builtin_sx_cmp_i8_uhp_le
#define sx_cmp_i8_uhp_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_uhp_gt_f  __builtin_sx_cmp_i8_uhp_gt
#define sx_cmp_i8_uhp_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_uhp_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_ubp_eq_f  __builtin_sx_cmp_r_ubp_eq
#define sx_cmp_r_ubp_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_ubp_ne_f  __builtin_sx_cmp_r_ubp_ne
#define sx_cmp_r_ubp_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_ubp_ge_f  __builtin_sx_cmp_r_ubp_ge
#define sx_cmp_r_ubp_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_ubp_lt_f  __builtin_sx_cmp_r_ubp_lt
#define sx_cmp_r_ubp_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_ubp_le_f  __builtin_sx_cmp_r_ubp_le
#define sx_cmp_r_ubp_le(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_ubp_gt_f  __builtin_sx_cmp_r_ubp_gt
#define sx_cmp_r_ubp_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_ubp_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpUBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_ubp_eq_f  __builtin_sx_cmp_i8_ubp_eq
#define sx_cmp_i8_ubp_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_ubp_ne_f  __builtin_sx_cmp_i8_ubp_ne
#define sx_cmp_i8_ubp_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_ubp_ge_f  __builtin_sx_cmp_i8_ubp_ge
#define sx_cmp_i8_ubp_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_ubp_lt_f  __builtin_sx_cmp_i8_ubp_lt
#define sx_cmp_i8_ubp_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_ubp_le_f  __builtin_sx_cmp_i8_ubp_le
#define sx_cmp_i8_ubp_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_ubp_gt_f  __builtin_sx_cmp_i8_ubp_gt
#define sx_cmp_i8_ubp_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_ubp_cmp(intrn_cmp_gt, c, d); \
}

#endif

/*  cmpISA_EC_cmp ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_eq_f  __builtin_sx_cmp_r_eq
#define sx_cmp_r_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_ne_f  __builtin_sx_cmp_r_ne
#define sx_cmp_r_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_ge_f  __builtin_sx_cmp_r_ge
#define sx_cmp_r_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_lt_f  __builtin_sx_cmp_r_lt
#define sx_cmp_r_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_le_f  __builtin_sx_cmp_r_le
#define sx_cmp_r_le(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_gt_f  __builtin_sx_cmp_r_gt
#define sx_cmp_r_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmp ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_eq_f  __builtin_sx_cmp_i8_eq
#define sx_cmp_i8_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_ne_f  __builtin_sx_cmp_i8_ne
#define sx_cmp_i8_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_ge_f  __builtin_sx_cmp_i8_ge
#define sx_cmp_i8_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_lt_f  __builtin_sx_cmp_i8_lt
#define sx_cmp_i8_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_le_f  __builtin_sx_cmp_i8_le
#define sx_cmp_i8_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_gt_f  __builtin_sx_cmp_i8_gt
#define sx_cmp_i8_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpH ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_h_eq_f  __builtin_sx_cmp_r_h_eq
#define sx_cmp_r_h_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_h_ne_f  __builtin_sx_cmp_r_h_ne
#define sx_cmp_r_h_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_h_ge_f  __builtin_sx_cmp_r_h_ge
#define sx_cmp_r_h_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_h_lt_f  __builtin_sx_cmp_r_h_lt
#define sx_cmp_r_h_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_h_le_f  __builtin_sx_cmp_r_h_le
#define sx_cmp_r_h_le(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_h_gt_f  __builtin_sx_cmp_r_h_gt
#define sx_cmp_r_h_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_h_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpH ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_h_eq_f  __builtin_sx_cmp_i8_h_eq
#define sx_cmp_i8_h_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_h_ne_f  __builtin_sx_cmp_i8_h_ne
#define sx_cmp_i8_h_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_h_ge_f  __builtin_sx_cmp_i8_h_ge
#define sx_cmp_i8_h_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_h_lt_f  __builtin_sx_cmp_i8_h_lt
#define sx_cmp_i8_h_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_h_le_f  __builtin_sx_cmp_i8_h_le
#define sx_cmp_i8_h_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_h_gt_f  __builtin_sx_cmp_i8_h_gt
#define sx_cmp_i8_h_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_h_cmp(intrn_cmp_gt, c, d); \
}

#ifdef __STxP70_V3__
/*  cmpISA_EC_cmpHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_hp_eq_f  __builtin_sx_cmp_r_hp_eq
#define sx_cmp_r_hp_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_hp_ne_f  __builtin_sx_cmp_r_hp_ne
#define sx_cmp_r_hp_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_hp_ge_f  __builtin_sx_cmp_r_hp_ge
#define sx_cmp_r_hp_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_hp_lt_f  __builtin_sx_cmp_r_hp_lt
#define sx_cmp_r_hp_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_hp_le_f  __builtin_sx_cmp_r_hp_le
#define sx_cmp_r_hp_le(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_hp_gt_f  __builtin_sx_cmp_r_hp_gt
#define sx_cmp_r_hp_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_hp_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_hp_eq_f  __builtin_sx_cmp_i8_hp_eq
#define sx_cmp_i8_hp_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_hp_ne_f  __builtin_sx_cmp_i8_hp_ne
#define sx_cmp_i8_hp_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_hp_ge_f  __builtin_sx_cmp_i8_hp_ge
#define sx_cmp_i8_hp_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_hp_lt_f  __builtin_sx_cmp_i8_hp_lt
#define sx_cmp_i8_hp_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_hp_le_f  __builtin_sx_cmp_i8_hp_le
#define sx_cmp_i8_hp_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_hp_gt_f  __builtin_sx_cmp_i8_hp_gt
#define sx_cmp_i8_hp_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_hp_cmp(intrn_cmp_gt, c, d); \
}

#endif

/*  cmpISA_EC_cmpB ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_b_eq_f  __builtin_sx_cmp_r_b_eq
#define sx_cmp_r_b_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_b_ne_f  __builtin_sx_cmp_r_b_ne
#define sx_cmp_r_b_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_b_ge_f  __builtin_sx_cmp_r_b_ge
#define sx_cmp_r_b_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_b_lt_f  __builtin_sx_cmp_r_b_lt
#define sx_cmp_r_b_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_b_le_f  __builtin_sx_cmp_r_b_le
#define sx_cmp_r_b_le(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_b_gt_f  __builtin_sx_cmp_r_b_gt
#define sx_cmp_r_b_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_b_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpB ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_b_eq_f  __builtin_sx_cmp_i8_b_eq
#define sx_cmp_i8_b_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_b_ne_f  __builtin_sx_cmp_i8_b_ne
#define sx_cmp_i8_b_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_b_ge_f  __builtin_sx_cmp_i8_b_ge
#define sx_cmp_i8_b_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_b_lt_f  __builtin_sx_cmp_i8_b_lt
#define sx_cmp_i8_b_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_b_le_f  __builtin_sx_cmp_i8_b_le
#define sx_cmp_i8_b_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_b_gt_f  __builtin_sx_cmp_i8_b_gt
#define sx_cmp_i8_b_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_b_cmp(intrn_cmp_gt, c, d); \
}

#ifdef __STxP70_V3__
/*  cmpISA_EC_cmpBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_bp_eq_f  __builtin_sx_cmp_r_bp_eq
#define sx_cmp_r_bp_eq(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_r_bp_ne_f  __builtin_sx_cmp_r_bp_ne
#define sx_cmp_r_bp_ne(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_r_bp_ge_f  __builtin_sx_cmp_r_bp_ge
#define sx_cmp_r_bp_ge(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_r_bp_lt_f  __builtin_sx_cmp_r_bp_lt
#define sx_cmp_r_bp_lt(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_r_bp_le_f  __builtin_sx_cmp_r_bp_le
#define sx_cmp_r_bp_le(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_r_bp_gt_f  __builtin_sx_cmp_r_bp_gt
#define sx_cmp_r_bp_gt(a, c, d) { \
  a = __builtin_sx_cmp_r_bp_cmp(intrn_cmp_gt, c, d); \
}

/*  cmpISA_EC_cmpBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_bp_eq_f  __builtin_sx_cmp_i8_bp_eq
#define sx_cmp_i8_bp_eq(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_eq, c, d); \
}

#define sx_cmp_i8_bp_ne_f  __builtin_sx_cmp_i8_bp_ne
#define sx_cmp_i8_bp_ne(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_ne, c, d); \
}

#define sx_cmp_i8_bp_ge_f  __builtin_sx_cmp_i8_bp_ge
#define sx_cmp_i8_bp_ge(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_ge, c, d); \
}

#define sx_cmp_i8_bp_lt_f  __builtin_sx_cmp_i8_bp_lt
#define sx_cmp_i8_bp_lt(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_lt, c, d); \
}

#define sx_cmp_i8_bp_le_f  __builtin_sx_cmp_i8_bp_le
#define sx_cmp_i8_bp_le(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_le, c, d); \
}

#define sx_cmp_i8_bp_gt_f  __builtin_sx_cmp_i8_bp_gt
#define sx_cmp_i8_bp_gt(a, c, d) { \
  a = __builtin_sx_cmp_i8_bp_cmp(intrn_cmp_gt, c, d); \
}

#endif

/*  extb gpr, gpr
 * gpr extb( gpr);
 */
#define sx_extb_f  __builtin_sx_extb
#define sx_extb(a, b) { \
  a = __builtin_sx_extb(b); \
}

/*  exth gpr, gpr
 * gpr exth( gpr);
 */
#define sx_exth_f  __builtin_sx_exth
#define sx_exth(a, b) { \
  a = __builtin_sx_exth(b); \
}

/*  extub gpr, gpr
 * gpr extub( gpr);
 */
#define sx_extub_f  __builtin_sx_extub
#define sx_extub(a, b) { \
  a = __builtin_sx_extub(b); \
}

/*  extuh gpr, gpr
 * gpr extuh( gpr);
 */
#define sx_extuh_f  __builtin_sx_extuh
#define sx_extuh(a, b) { \
  a = __builtin_sx_extuh(b); \
}

/*  idle <u2>
 * idle( <u2>);
 */
#define sx_idle_f  __builtin_sx_idle
#define sx_idle(a) { \
  __builtin_sx_idle(a); \
}

/*  lzc gpr, gpr
 * gpr lzc( gpr);
 */
#define sx_lzc_f  __builtin_sx_lzc
#define sx_lzc(a, b) { \
  a = __builtin_sx_lzc(b); \
}

#ifdef __STxP70_V3__
/*  makehp gpr, <s16>
 * gpr makehp( <s16>);
 */
#define sx_makehp_f  __builtin_sx_makehp
#define sx_makehp(a, b) { \
  a = __builtin_sx_makehp(b); \
}

/*  make gpr, <abs0to14_abs0to15_abs16to31_da0to14_gprel16to31_gotoffs16to31_s16>
 * gpr make( <abs0to14_abs0to15_abs16to31_da0to14_gprel16to31_gotoffs16to31_s16>);
 */
#define sx_make_f  __builtin_sx_make
#define sx_make(a, b) { \
  a = __builtin_sx_make(b); \
}

#endif

/*  maxbp gpr, gpr, gpr
 * gpr maxbp( gpr, gpr);
 */
#define sx_maxbp_r_f  __builtin_sx_maxbp_r
#define sx_maxbp_r(a, b, c) { \
  a = __builtin_sx_maxbp_r(b, c); \
}

/*  maxbp gpr, gpr, <u8>
 * gpr maxbp( gpr, <u8>);
 */
#define sx_maxbp_i8_f  __builtin_sx_maxbp_i8
#define sx_maxbp_i8(a, b, c) { \
  a = __builtin_sx_maxbp_i8(b, c); \
}

/*  maxhp gpr, gpr, gpr
 * gpr maxhp( gpr, gpr);
 */
#define sx_maxhp_r_f  __builtin_sx_maxhp_r
#define sx_maxhp_r(a, b, c) { \
  a = __builtin_sx_maxhp_r(b, c); \
}

/*  maxhp gpr, gpr, <u8>
 * gpr maxhp( gpr, <u8>);
 */
#define sx_maxhp_i8_f  __builtin_sx_maxhp_i8
#define sx_maxhp_i8(a, b, c) { \
  a = __builtin_sx_maxhp_i8(b, c); \
}

/*  maxubp gpr, gpr, gpr
 * gpr maxubp( gpr, gpr);
 */
#define sx_maxubp_r_f  __builtin_sx_maxubp_r
#define sx_maxubp_r(a, b, c) { \
  a = __builtin_sx_maxubp_r(b, c); \
}

/*  maxubp gpr, gpr, <u8>
 * gpr maxubp( gpr, <u8>);
 */
#define sx_maxubp_i8_f  __builtin_sx_maxubp_i8
#define sx_maxubp_i8(a, b, c) { \
  a = __builtin_sx_maxubp_i8(b, c); \
}

/*  maxuhp gpr, gpr, gpr
 * gpr maxuhp( gpr, gpr);
 */
#define sx_maxuhp_r_f  __builtin_sx_maxuhp_r
#define sx_maxuhp_r(a, b, c) { \
  a = __builtin_sx_maxuhp_r(b, c); \
}

/*  maxuhp gpr, gpr, <u8>
 * gpr maxuhp( gpr, <u8>);
 */
#define sx_maxuhp_i8_f  __builtin_sx_maxuhp_i8
#define sx_maxuhp_i8(a, b, c) { \
  a = __builtin_sx_maxuhp_i8(b, c); \
}

/*  maxu gpr, gpr, gpr
 * gpr maxu( gpr, gpr);
 */
#define sx_maxu_r_f  __builtin_sx_maxu_r
#define sx_maxu_r(a, b, c) { \
  a = __builtin_sx_maxu_r(b, c); \
}

/*  maxu gpr, gpr, <u8>
 * gpr maxu( gpr, <u8>);
 */
#define sx_maxu_i8_f  __builtin_sx_maxu_i8
#define sx_maxu_i8(a, b, c) { \
  a = __builtin_sx_maxu_i8(b, c); \
}

/*  max gpr, gpr, gpr
 * gpr max( gpr, gpr);
 */
#define sx_max_r_f  __builtin_sx_max_r
#define sx_max_r(a, b, c) { \
  a = __builtin_sx_max_r(b, c); \
}

/*  max gpr, gpr, <u8>
 * gpr max( gpr, <u8>);
 */
#define sx_max_i8_f  __builtin_sx_max_i8
#define sx_max_i8(a, b, c) { \
  a = __builtin_sx_max_i8(b, c); \
}

/*  minbp gpr, gpr, gpr
 * gpr minbp( gpr, gpr);
 */
#define sx_minbp_r_f  __builtin_sx_minbp_r
#define sx_minbp_r(a, b, c) { \
  a = __builtin_sx_minbp_r(b, c); \
}

/*  minbp gpr, gpr, <u8>
 * gpr minbp( gpr, <u8>);
 */
#define sx_minbp_i8_f  __builtin_sx_minbp_i8
#define sx_minbp_i8(a, b, c) { \
  a = __builtin_sx_minbp_i8(b, c); \
}

/*  minhp gpr, gpr, gpr
 * gpr minhp( gpr, gpr);
 */
#define sx_minhp_r_f  __builtin_sx_minhp_r
#define sx_minhp_r(a, b, c) { \
  a = __builtin_sx_minhp_r(b, c); \
}

/*  minhp gpr, gpr, <u8>
 * gpr minhp( gpr, <u8>);
 */
#define sx_minhp_i8_f  __builtin_sx_minhp_i8
#define sx_minhp_i8(a, b, c) { \
  a = __builtin_sx_minhp_i8(b, c); \
}

/*  minubp gpr, gpr, gpr
 * gpr minubp( gpr, gpr);
 */
#define sx_minubp_r_f  __builtin_sx_minubp_r
#define sx_minubp_r(a, b, c) { \
  a = __builtin_sx_minubp_r(b, c); \
}

/*  minubp gpr, gpr, <u8>
 * gpr minubp( gpr, <u8>);
 */
#define sx_minubp_i8_f  __builtin_sx_minubp_i8
#define sx_minubp_i8(a, b, c) { \
  a = __builtin_sx_minubp_i8(b, c); \
}

/*  minuhp gpr, gpr, gpr
 * gpr minuhp( gpr, gpr);
 */
#define sx_minuhp_r_f  __builtin_sx_minuhp_r
#define sx_minuhp_r(a, b, c) { \
  a = __builtin_sx_minuhp_r(b, c); \
}

/*  minuhp gpr, gpr, <u8>
 * gpr minuhp( gpr, <u8>);
 */
#define sx_minuhp_i8_f  __builtin_sx_minuhp_i8
#define sx_minuhp_i8(a, b, c) { \
  a = __builtin_sx_minuhp_i8(b, c); \
}

/*  minu gpr, gpr, gpr
 * gpr minu( gpr, gpr);
 */
#define sx_minu_r_f  __builtin_sx_minu_r
#define sx_minu_r(a, b, c) { \
  a = __builtin_sx_minu_r(b, c); \
}

/*  minu gpr, gpr, <u8>
 * gpr minu( gpr, <u8>);
 */
#define sx_minu_i8_f  __builtin_sx_minu_i8
#define sx_minu_i8(a, b, c) { \
  a = __builtin_sx_minu_i8(b, c); \
}

/*  min gpr, gpr, gpr
 * gpr min( gpr, gpr);
 */
#define sx_min_r_f  __builtin_sx_min_r
#define sx_min_r(a, b, c) { \
  a = __builtin_sx_min_r(b, c); \
}

/*  min gpr, gpr, <u8>
 * gpr min( gpr, <u8>);
 */
#define sx_min_i8_f  __builtin_sx_min_i8
#define sx_min_i8(a, b, c) { \
  a = __builtin_sx_min_i8(b, c); \
}

#ifdef __STxP70_V3__
/*  more gpr, <abs0to15_gprel0to15_gotoffs0to15_u16>
 * gpr more( gpr, <abs0to15_gprel0to15_gotoffs0to15_u16>);
 */
#define sx_more_f  __builtin_sx_more
#define sx_more(a, b) { \
  a = __builtin_sx_more(a, b); \
}

#endif

/*  moveh2h gpr, gpr
 * gpr moveh2h( gpr, gpr);
 */
#define sx_moveh2h_f  __builtin_sx_moveh2h
#define sx_moveh2h(a, b) { \
  a = __builtin_sx_moveh2h(a, b); \
}

/*  moveh2l gpr, gpr
 * gpr moveh2l( gpr, gpr);
 */
#define sx_moveh2l_f  __builtin_sx_moveh2l
#define sx_moveh2l(a, b) { \
  a = __builtin_sx_moveh2l(a, b); \
}

/*  movel2h gpr, gpr
 * gpr movel2h( gpr, gpr);
 */
#define sx_movel2h_f  __builtin_sx_movel2h
#define sx_movel2h(a, b) { \
  a = __builtin_sx_movel2h(a, b); \
}

/*  movel2l gpr, gpr
 * gpr movel2l( gpr, gpr);
 */
#define sx_movel2l_f  __builtin_sx_movel2l
#define sx_movel2l(a, b) { \
  a = __builtin_sx_movel2l(a, b); \
}

/*  negbp gpr, gpr
 * gpr negbp( gpr);
 */
#define sx_negbp_f  __builtin_sx_negbp
#define sx_negbp(a, b) { \
  a = __builtin_sx_negbp(b); \
}

/*  neghp gpr, gpr
 * gpr neghp( gpr);
 */
#define sx_neghp_f  __builtin_sx_neghp
#define sx_neghp(a, b) { \
  a = __builtin_sx_neghp(b); \
}

/*  negubp gpr, gpr
 * gpr negubp( gpr);
 */
#define sx_negubp_f  __builtin_sx_negubp
#define sx_negubp(a, b) { \
  a = __builtin_sx_negubp(b); \
}

/*  neguhp gpr, gpr
 * gpr neguhp( gpr);
 */
#define sx_neguhp_f  __builtin_sx_neguhp
#define sx_neguhp(a, b) { \
  a = __builtin_sx_neguhp(b); \
}

/*  negu gpr, gpr
 * gpr negu( gpr);
 */
#define sx_negu_f  __builtin_sx_negu
#define sx_negu(a, b) { \
  a = __builtin_sx_negu(b); \
}

/*  neg gpr, gpr
 * gpr neg( gpr);
 */
#define sx_neg_f  __builtin_sx_neg
#define sx_neg(a, b) { \
  a = __builtin_sx_neg(b); \
}

/* nop
 * nop( );
 */
#define sx_nop_f  __builtin_sx_nop
#define sx_nop() { \
  __builtin_sx_nop(); \
}

/*  norn gpr, gpr, <u8>
 * gpr norn( gpr, <u8>);
 */
#define sx_norn_f  __builtin_sx_norn
#define sx_norn(a, b, c) { \
  a = __builtin_sx_norn(b, c); \
}

/*  not gpr, gpr
 * gpr not( gpr);
 */
#define sx_not_f  __builtin_sx_not
#define sx_not(a, b) { \
  a = __builtin_sx_not(b); \
}

/*  org ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst org( gr, gr);
 */
#define sx_org_f  __builtin_sx_org
#define sx_org(a, b, c) { \
  a = __builtin_sx_org(b, c); \
}

/*  or gpr, gpr, gpr
 * gpr or( gpr, gpr);
 */
#define sx_or_r_f  __builtin_sx_or_r
#define sx_or_r(a, b, c) { \
  a = __builtin_sx_or_r(b, c); \
}

/*  or gpr, gpr, <u8>
 * gpr or( gpr, <u8>);
 */
#define sx_or_i8_f  __builtin_sx_or_i8
#define sx_or_i8(a, b, c) { \
  a = __builtin_sx_or_i8(b, c); \
}

/*  prior gpr, gpr
 * gpr prior( gpr);
 */
#define sx_prior_f  __builtin_sx_prior
#define sx_prior(a, b) { \
  a = __builtin_sx_prior(b); \
}

/*  rotl gpr, gpr, gpr
 * gpr rotl( gpr, gpr);
 */
#define sx_rotl_r_f  __builtin_sx_rotl_r
#define sx_rotl_r(a, b, c) { \
  a = __builtin_sx_rotl_r(b, c); \
}

/*  rotl gpr, gpr, <u5>
 * gpr rotl( gpr, <u5>);
 */
#define sx_rotl_i5_f  __builtin_sx_rotl_i5
#define sx_rotl_i5(a, b, c) { \
  a = __builtin_sx_rotl_i5(b, c); \
}

/* rte
 * rte( );
 */
#define sx_rte_f  __builtin_sx_rte
#define sx_rte() { \
  __builtin_sx_rte(); \
}

/*  shlmu gpr, gpr
 * gpr shlmu( gpr);
 */
#define sx_shlmu_r_f  __builtin_sx_shlmu_r
#define sx_shlmu_r(a, b) { \
  a = __builtin_sx_shlmu_r(b); \
}

/*  shlmu gpr, <u5>
 * gpr shlmu( <u5>);
 */
#define sx_shlmu_i5_f  __builtin_sx_shlmu_i5
#define sx_shlmu_i5(a, b) { \
  a = __builtin_sx_shlmu_i5(b); \
}

/*  shlnu gpr, gpr, gpr
 * gpr shlnu( gpr, gpr);
 */
#define sx_shlnu_f  __builtin_sx_shlnu
#define sx_shlnu(a, b, c) { \
  a = __builtin_sx_shlnu(b, c); \
}

/*  shlu gpr, gpr, gpr
 * gpr shlu( gpr, gpr);
 */
#define sx_shlu_r_f  __builtin_sx_shlu_r
#define sx_shlu_r(a, b, c) { \
  a = __builtin_sx_shlu_r(b, c); \
}

/*  shlu gpr, gpr, <u5>
 * gpr shlu( gpr, <u5>);
 */
#define sx_shlu_i5_f  __builtin_sx_shlu_i5
#define sx_shlu_i5(a, b, c) { \
  a = __builtin_sx_shlu_i5(b, c); \
}

/*  shl gpr, gpr, gpr
 * gpr shl( gpr, gpr);
 */
#define sx_shl_r_f  __builtin_sx_shl_r
#define sx_shl_r(a, b, c) { \
  a = __builtin_sx_shl_r(b, c); \
}

/*  shl gpr, gpr, <u5>
 * gpr shl( gpr, <u5>);
 */
#define sx_shl_i5_f  __builtin_sx_shl_i5
#define sx_shl_i5(a, b, c) { \
  a = __builtin_sx_shl_i5(b, c); \
}

/*  shrmu gpr, gpr
 * gpr shrmu( gpr);
 */
#define sx_shrmu_r_f  __builtin_sx_shrmu_r
#define sx_shrmu_r(a, b) { \
  a = __builtin_sx_shrmu_r(b); \
}

/*  shrmu gpr, <u5>
 * gpr shrmu( <u5>);
 */
#define sx_shrmu_i5_f  __builtin_sx_shrmu_i5
#define sx_shrmu_i5(a, b) { \
  a = __builtin_sx_shrmu_i5(b); \
}

/*  shrnu gpr, gpr, gpr
 * gpr shrnu( gpr, gpr);
 */
#define sx_shrnu_f  __builtin_sx_shrnu
#define sx_shrnu(a, b, c) { \
  a = __builtin_sx_shrnu(b, c); \
}

/*  shru gpr, gpr, gpr
 * gpr shru( gpr, gpr);
 */
#define sx_shru_r_f  __builtin_sx_shru_r
#define sx_shru_r(a, b, c) { \
  a = __builtin_sx_shru_r(b, c); \
}

/*  shru gpr, gpr, <u5>
 * gpr shru( gpr, <u5>);
 */
#define sx_shru_i5_f  __builtin_sx_shru_i5
#define sx_shru_i5(a, b, c) { \
  a = __builtin_sx_shru_i5(b, c); \
}

/*  shr gpr, gpr, gpr
 * gpr shr( gpr, gpr);
 */
#define sx_shr_r_f  __builtin_sx_shr_r
#define sx_shr_r(a, b, c) { \
  a = __builtin_sx_shr_r(b, c); \
}

/*  shr gpr, gpr, <u5>
 * gpr shr( gpr, <u5>);
 */
#define sx_shr_i5_f  __builtin_sx_shr_i5
#define sx_shr_i5(a, b, c) { \
  a = __builtin_sx_shr_i5(b, c); \
}

/*  subbp gpr, gpr, gpr
 * gpr subbp( gpr, gpr);
 */
#define sx_subbp_r_f  __builtin_sx_subbp_r
#define sx_subbp_r(a, b, c) { \
  a = __builtin_sx_subbp_r(b, c); \
}

/*  subbp gpr, gpr, <u8>
 * gpr subbp( gpr, <u8>);
 */
#define sx_subbp_i8_f  __builtin_sx_subbp_i8
#define sx_subbp_i8(a, b, c) { \
  a = __builtin_sx_subbp_i8(b, c); \
}

/*  subcu gpr, gpr, gpr
 * gpr subcu( gpr, gpr);
 */
#define sx_subcu_f  __builtin_sx_subcu
#define sx_subcu(a, b, c) { \
  a = __builtin_sx_subcu(b, c); \
}

/*  subc gpr, gpr, gpr
 * gpr subc( gpr, gpr);
 */
#define sx_subc_f  __builtin_sx_subc
#define sx_subc(a, b, c) { \
  a = __builtin_sx_subc(b, c); \
}

/*  subhp gpr, gpr, gpr
 * gpr subhp( gpr, gpr);
 */
#define sx_subhp_r_f  __builtin_sx_subhp_r
#define sx_subhp_r(a, b, c) { \
  a = __builtin_sx_subhp_r(b, c); \
}

/*  subhp gpr, gpr, <u8>
 * gpr subhp( gpr, <u8>);
 */
#define sx_subhp_i8_f  __builtin_sx_subhp_i8
#define sx_subhp_i8(a, b, c) { \
  a = __builtin_sx_subhp_i8(b, c); \
}

/*  sububp gpr, gpr, gpr
 * gpr sububp( gpr, gpr);
 */
#define sx_sububp_r_f  __builtin_sx_sububp_r
#define sx_sububp_r(a, b, c) { \
  a = __builtin_sx_sububp_r(b, c); \
}

/*  sububp gpr, gpr, <u8>
 * gpr sububp( gpr, <u8>);
 */
#define sx_sububp_i8_f  __builtin_sx_sububp_i8
#define sx_sububp_i8(a, b, c) { \
  a = __builtin_sx_sububp_i8(b, c); \
}

/*  subuhp gpr, gpr, gpr
 * gpr subuhp( gpr, gpr);
 */
#define sx_subuhp_r_f  __builtin_sx_subuhp_r
#define sx_subuhp_r(a, b, c) { \
  a = __builtin_sx_subuhp_r(b, c); \
}

/*  subuhp gpr, gpr, <u8>
 * gpr subuhp( gpr, <u8>);
 */
#define sx_subuhp_i8_f  __builtin_sx_subuhp_i8
#define sx_subuhp_i8(a, b, c) { \
  a = __builtin_sx_subuhp_i8(b, c); \
}

/*  subu gpr, gpr, gpr
 * gpr subu( gpr, gpr);
 */
#define sx_subu_r_f  __builtin_sx_subu_r
#define sx_subu_r(a, b, c) { \
  a = __builtin_sx_subu_r(b, c); \
}

/*  subu gpr, gpr, <u8>
 * gpr subu( gpr, <u8>);
 */
#define sx_subu_i8_f  __builtin_sx_subu_i8
#define sx_subu_i8(a, b, c) { \
  a = __builtin_sx_subu_i8(b, c); \
}

/*  sub gpr, gpr, gpr
 * gpr sub( gpr, gpr);
 */
#define sx_sub_r_f  __builtin_sx_sub_r
#define sx_sub_r(a, b, c) { \
  a = __builtin_sx_sub_r(b, c); \
}

/*  sub gpr, gpr, <u8>
 * gpr sub( gpr, <u8>);
 */
#define sx_sub_i8_f  __builtin_sx_sub_i8
#define sx_sub_i8(a, b, c) { \
  a = __builtin_sx_sub_i8(b, c); \
}

/* trap <u2>
 * trap( <u2>);
 */
#define sx_trap_f  __builtin_sx_trap
#define sx_trap(a) { \
  __builtin_sx_trap(a); \
}

/*  tISA_EC_btestBIT ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, gpr);
 */
#define sx_tfbit_r_f  __builtin_sx_tfbit_r
#define sx_tfbit_r(a, c, d) { \
  a = __builtin_sx_tbit_r(intrn_btest_f, c, d); \
}

#define sx_ttbit_r_f  __builtin_sx_ttbit_r
#define sx_ttbit_r(a, c, d) { \
  a = __builtin_sx_tbit_r(intrn_btest_t, c, d); \
}

/*  tISA_EC_btestBIT ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u5>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u5>);
 */
#define sx_tfbit_i5_f  __builtin_sx_tfbit_i5
#define sx_tfbit_i5(a, c, d) { \
  a = __builtin_sx_tbit_i5(intrn_btest_f, c, d); \
}

#define sx_ttbit_i5_f  __builtin_sx_ttbit_i5
#define sx_ttbit_i5(a, c, d) { \
  a = __builtin_sx_tbit_i5(intrn_btest_t, c, d); \
}

/*  tISA_EC_btestAND ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u8>);
 */
#define sx_tfand_f  __builtin_sx_tfand
#define sx_tfand(a, c, d) { \
  a = __builtin_sx_tand(intrn_btest_f, c, d); \
}

#define sx_ttand_f  __builtin_sx_ttand
#define sx_ttand(a, c, d) { \
  a = __builtin_sx_tand(intrn_btest_t, c, d); \
}

/*  tISA_EC_btestANDN ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u8>);
 */
#define sx_tfandn_f  __builtin_sx_tfandn
#define sx_tfandn(a, c, d) { \
  a = __builtin_sx_tandn(intrn_btest_f, c, d); \
}

#define sx_ttandn_f  __builtin_sx_ttandn
#define sx_ttandn(a, c, d) { \
  a = __builtin_sx_tandn(intrn_btest_t, c, d); \
}

/*  xorg ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst xorg( gr, gr);
 */
#define sx_xorg_f  __builtin_sx_xorg
#define sx_xorg(a, b, c) { \
  a = __builtin_sx_xorg(b, c); \
}

/*  xor gpr, gpr, gpr
 * gpr xor( gpr, gpr);
 */
#define sx_xor_r_f  __builtin_sx_xor_r
#define sx_xor_r(a, b, c) { \
  a = __builtin_sx_xor_r(b, c); \
}

/*  xor gpr, gpr, <u8>
 * gpr xor( gpr, <u8>);
 */
#define sx_xor_i8_f  __builtin_sx_xor_i8
#define sx_xor_i8(a, b, c) { \
  a = __builtin_sx_xor_i8(b, c); \
}

#else /* !defined(__SX) || defined(__SX_C_MODEL) */

/* Macros mapped on C models for class sx */
#include <builtins_model_sx.h>

/*  make32 gpr, <s32>
 * gpr make32( <s32>);
 */
#define sx_make32_f __cmodel_sx_make32
#define sx_make32(a, b) { \
  a = __cmodel_sx_make32(b); \
}

/*  moveg2r gpr, ISA_REGISTER_SUBCLASS_gr_dst
 * gpr moveg2r( ISA_REGISTER_SUBCLASS_gr_dst);
 */
#define sx_moveg2r_f __cmodel_sx_moveg2r
#define sx_moveg2r(a, b) { \
  a = __cmodel_sx_moveg2r(b); \
}

/*  mover2g ISA_REGISTER_SUBCLASS_gr_dst, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst mover2g( gpr);
 */
#define sx_mover2g_f __cmodel_sx_mover2g
#define sx_mover2g(a, b) { \
  a = __cmodel_sx_mover2g(b); \
}

/*  absbp gpr, gpr
 * gpr absbp( gpr);
 */
#define sx_absbp_f __cmodel_sx_absbp
#define sx_absbp(a, b) { \
  a = __cmodel_sx_absbp(b); \
}

/*  abshp gpr, gpr
 * gpr abshp( gpr);
 */
#define sx_abshp_f __cmodel_sx_abshp
#define sx_abshp(a, b) { \
  a = __cmodel_sx_abshp(b); \
}

/*  absubp gpr, gpr
 * gpr absubp( gpr);
 */
#define sx_absubp_f __cmodel_sx_absubp
#define sx_absubp(a, b) { \
  a = __cmodel_sx_absubp(b); \
}

/*  absuhp gpr, gpr
 * gpr absuhp( gpr);
 */
#define sx_absuhp_f __cmodel_sx_absuhp
#define sx_absuhp(a, b) { \
  a = __cmodel_sx_absuhp(b); \
}

/*  absu gpr, gpr
 * gpr absu( gpr);
 */
#define sx_absu_f __cmodel_sx_absu
#define sx_absu(a, b) { \
  a = __cmodel_sx_absu(b); \
}

/*  abs gpr, gpr
 * gpr abs( gpr);
 */
#define sx_abs_f __cmodel_sx_abs
#define sx_abs(a, b) { \
  a = __cmodel_sx_abs(b); \
}

/*  addbp gpr, gpr, gpr
 * gpr addbp( gpr, gpr);
 */
#define sx_addbp_r_f __cmodel_sx_addbp_r
#define sx_addbp_r(a, b, c) { \
  a = __cmodel_sx_addbp_r(b, c); \
}

/*  addbp gpr, gpr, <u8>
 * gpr addbp( gpr, <u8>);
 */
#define sx_addbp_i8_f __cmodel_sx_addbp_i8
#define sx_addbp_i8(a, b, c) { \
  a = __cmodel_sx_addbp_i8(b, c); \
}

/*  addcu gpr, gpr, gpr
 * gpr addcu( gpr, gpr);
 */
#define sx_addcu_f __cmodel_sx_addcu
#define sx_addcu(a, b, c) { \
  a = __cmodel_sx_addcu(b, c); \
}

/*  addc gpr, gpr, gpr
 * gpr addc( gpr, gpr);
 */
#define sx_addc_f __cmodel_sx_addc
#define sx_addc(a, b, c) { \
  a = __cmodel_sx_addc(b, c); \
}

/*  addhp gpr, gpr, gpr
 * gpr addhp( gpr, gpr);
 */
#define sx_addhp_r_f __cmodel_sx_addhp_r
#define sx_addhp_r(a, b, c) { \
  a = __cmodel_sx_addhp_r(b, c); \
}

/*  addhp gpr, gpr, <u8>
 * gpr addhp( gpr, <u8>);
 */
#define sx_addhp_i8_f __cmodel_sx_addhp_i8
#define sx_addhp_i8(a, b, c) { \
  a = __cmodel_sx_addhp_i8(b, c); \
}

/*  addubp gpr, gpr, gpr
 * gpr addubp( gpr, gpr);
 */
#define sx_addubp_r_f __cmodel_sx_addubp_r
#define sx_addubp_r(a, b, c) { \
  a = __cmodel_sx_addubp_r(b, c); \
}

/*  addubp gpr, gpr, <u8>
 * gpr addubp( gpr, <u8>);
 */
#define sx_addubp_i8_f __cmodel_sx_addubp_i8
#define sx_addubp_i8(a, b, c) { \
  a = __cmodel_sx_addubp_i8(b, c); \
}

/*  addugp gpr, <da0to15_gprel0to15_gotoffs0to15_u16>
 * gpr addugp( <da0to15_gprel0to15_gotoffs0to15_u16>);
 */
#define sx_addugp_f __cmodel_sx_addugp
#define sx_addugp(a, b) { \
  a = __cmodel_sx_addugp(b); \
}

/*  adduhp gpr, gpr, gpr
 * gpr adduhp( gpr, gpr);
 */
#define sx_adduhp_r_f __cmodel_sx_adduhp_r
#define sx_adduhp_r(a, b, c) { \
  a = __cmodel_sx_adduhp_r(b, c); \
}

/*  adduhp gpr, gpr, <u8>
 * gpr adduhp( gpr, <u8>);
 */
#define sx_adduhp_i8_f __cmodel_sx_adduhp_i8
#define sx_adduhp_i8(a, b, c) { \
  a = __cmodel_sx_adduhp_i8(b, c); \
}

/*  addur gpr, gpr
 * gpr addur( gpr);
 */
#define sx_addur_r_f __cmodel_sx_addur_r
#define sx_addur_r(a, b) { \
  a = __cmodel_sx_addur_r(b); \
}

/*  addur gpr, <rel2to17_neggprel_scf2_s16>
 * gpr addur( <rel2to17_neggprel_scf2_s16>);
 */
#define sx_addur_i16_f __cmodel_sx_addur_i16
#define sx_addur_i16(a, b) { \
  a = __cmodel_sx_addur_i16(b); \
}

/*  addu gpr, gpr, gpr
 * gpr addu( gpr, gpr);
 */
#define sx_addu_r_f __cmodel_sx_addu_r
#define sx_addu_r(a, b, c) { \
  a = __cmodel_sx_addu_r(b, c); \
}

/*  addu gpr, gpr, <u8>
 * gpr addu( gpr, <u8>);
 */
#define sx_addu_i8_f __cmodel_sx_addu_i8
#define sx_addu_i8(a, b, c) { \
  a = __cmodel_sx_addu_i8(b, c); \
}

/*  add gpr, gpr, gpr
 * gpr add( gpr, gpr);
 */
#define sx_add_r_f __cmodel_sx_add_r
#define sx_add_r(a, b, c) { \
  a = __cmodel_sx_add_r(b, c); \
}

/*  add gpr, gpr, <u8>
 * gpr add( gpr, <u8>);
 */
#define sx_add_i8_f __cmodel_sx_add_i8
#define sx_add_i8(a, b, c) { \
  a = __cmodel_sx_add_i8(b, c); \
}

/*  andg ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst andg( gr, gr);
 */
#define sx_andg_f __cmodel_sx_andg
#define sx_andg(a, b, c) { \
  a = __cmodel_sx_andg(b, c); \
}

/*  andng ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst andng( gr, gr);
 */
#define sx_andng_f __cmodel_sx_andng
#define sx_andng(a, b, c) { \
  a = __cmodel_sx_andng(b, c); \
}

/*  andn gpr, gpr, gpr
 * gpr andn( gpr, gpr);
 */
#define sx_andn_f __cmodel_sx_andn
#define sx_andn(a, b, c) { \
  a = __cmodel_sx_andn(b, c); \
}

/*  and gpr, gpr, gpr
 * gpr and( gpr, gpr);
 */
#define sx_and_r_f __cmodel_sx_and_r
#define sx_and_r(a, b, c) { \
  a = __cmodel_sx_and_r(b, c); \
}

/*  and gpr, gpr, <u8>
 * gpr and( gpr, <u8>);
 */
#define sx_and_i8_f __cmodel_sx_and_i8
#define sx_and_i8(a, b, c) { \
  a = __cmodel_sx_and_i8(b, c); \
}

/* barrier
 * barrier( );
 */
#define sx_barrier_f __cmodel_sx_barrier
#define sx_barrier() { \
  __cmodel_sx_barrier(); \
}

/* bkp
 * bkp( );
 */
#define sx_bkp_f __cmodel_sx_bkp
#define sx_bkp() { \
  __cmodel_sx_bkp(); \
}

/*  boolbp gpr, gr
 * gpr boolbp( gr);
 */
#define sx_boolbp_f __cmodel_sx_boolbp
#define sx_boolbp(a, b) { \
  a = __cmodel_sx_boolbp(b); \
}

/*  boolhp gpr, gr
 * gpr boolhp( gr);
 */
#define sx_boolhp_f __cmodel_sx_boolhp
#define sx_boolhp(a, b) { \
  a = __cmodel_sx_boolhp(b); \
}

/*  bool gpr, gr
 * gpr bool( gr);
 */
#define sx_bool_f __cmodel_sx_bool
#define sx_bool(a, b) { \
  a = __cmodel_sx_bool(b); \
}

/*  bISA_EC_bitop gpr, gpr, gpr
 * gpr b( ISA_EC_bitop, gpr, gpr);
 */
#define sx_bclr_r_f __cmodel_sx_bclr_r
#define sx_bclr_r(a, c, d) { \
  a = __cmodel_sx_bclr_r(c, d); \
}

#define sx_bset_r_f __cmodel_sx_bset_r
#define sx_bset_r(a, c, d) { \
  a = __cmodel_sx_bset_r(c, d); \
}

#define sx_bnot_r_f __cmodel_sx_bnot_r
#define sx_bnot_r(a, c, d) { \
  a = __cmodel_sx_bnot_r(c, d); \
}

/*  bISA_EC_bitop gpr, gpr, <u5>
 * gpr b( ISA_EC_bitop, gpr, <u5>);
 */
#define sx_bclr_i5_f __cmodel_sx_bclr_i5
#define sx_bclr_i5(a, c, d) { \
  a = __cmodel_sx_bclr_i5(c, d); \
}

#define sx_bset_i5_f __cmodel_sx_bset_i5
#define sx_bset_i5(a, c, d) { \
  a = __cmodel_sx_bset_i5(c, d); \
}

#define sx_bnot_i5_f __cmodel_sx_bnot_i5
#define sx_bnot_i5(a, c, d) { \
  a = __cmodel_sx_bnot_i5(c, d); \
}

/*  clamph gpr, gpr
 * gpr clamph( gpr);
 */
#define sx_clamph_f __cmodel_sx_clamph
#define sx_clamph(a, b) { \
  a = __cmodel_sx_clamph(b); \
}

/*  cmpISA_EC_cmpU ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_u_eq_f __cmodel_sx_cmp_r_u_eq
#define sx_cmp_r_u_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_eq(c, d); \
}

#define sx_cmp_r_u_ne_f __cmodel_sx_cmp_r_u_ne
#define sx_cmp_r_u_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_ne(c, d); \
}

#define sx_cmp_r_u_ge_f __cmodel_sx_cmp_r_u_ge
#define sx_cmp_r_u_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_ge(c, d); \
}

#define sx_cmp_r_u_lt_f __cmodel_sx_cmp_r_u_lt
#define sx_cmp_r_u_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_lt(c, d); \
}

#define sx_cmp_r_u_le_f __cmodel_sx_cmp_r_u_le
#define sx_cmp_r_u_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_le(c, d); \
}

#define sx_cmp_r_u_gt_f __cmodel_sx_cmp_r_u_gt
#define sx_cmp_r_u_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_u_gt(c, d); \
}

/*  cmpISA_EC_cmpU ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_u_eq_f __cmodel_sx_cmp_i8_u_eq
#define sx_cmp_i8_u_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_eq(c, d); \
}

#define sx_cmp_i8_u_ne_f __cmodel_sx_cmp_i8_u_ne
#define sx_cmp_i8_u_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_ne(c, d); \
}

#define sx_cmp_i8_u_ge_f __cmodel_sx_cmp_i8_u_ge
#define sx_cmp_i8_u_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_ge(c, d); \
}

#define sx_cmp_i8_u_lt_f __cmodel_sx_cmp_i8_u_lt
#define sx_cmp_i8_u_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_lt(c, d); \
}

#define sx_cmp_i8_u_le_f __cmodel_sx_cmp_i8_u_le
#define sx_cmp_i8_u_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_le(c, d); \
}

#define sx_cmp_i8_u_gt_f __cmodel_sx_cmp_i8_u_gt
#define sx_cmp_i8_u_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_u_gt(c, d); \
}

/*  cmpISA_EC_cmpUH ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_uh_eq_f __cmodel_sx_cmp_r_uh_eq
#define sx_cmp_r_uh_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_eq(c, d); \
}

#define sx_cmp_r_uh_ne_f __cmodel_sx_cmp_r_uh_ne
#define sx_cmp_r_uh_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_ne(c, d); \
}

#define sx_cmp_r_uh_ge_f __cmodel_sx_cmp_r_uh_ge
#define sx_cmp_r_uh_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_ge(c, d); \
}

#define sx_cmp_r_uh_lt_f __cmodel_sx_cmp_r_uh_lt
#define sx_cmp_r_uh_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_lt(c, d); \
}

#define sx_cmp_r_uh_le_f __cmodel_sx_cmp_r_uh_le
#define sx_cmp_r_uh_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_le(c, d); \
}

#define sx_cmp_r_uh_gt_f __cmodel_sx_cmp_r_uh_gt
#define sx_cmp_r_uh_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_uh_gt(c, d); \
}

/*  cmpISA_EC_cmpUH ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_uh_eq_f __cmodel_sx_cmp_i8_uh_eq
#define sx_cmp_i8_uh_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_eq(c, d); \
}

#define sx_cmp_i8_uh_ne_f __cmodel_sx_cmp_i8_uh_ne
#define sx_cmp_i8_uh_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_ne(c, d); \
}

#define sx_cmp_i8_uh_ge_f __cmodel_sx_cmp_i8_uh_ge
#define sx_cmp_i8_uh_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_ge(c, d); \
}

#define sx_cmp_i8_uh_lt_f __cmodel_sx_cmp_i8_uh_lt
#define sx_cmp_i8_uh_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_lt(c, d); \
}

#define sx_cmp_i8_uh_le_f __cmodel_sx_cmp_i8_uh_le
#define sx_cmp_i8_uh_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_le(c, d); \
}

#define sx_cmp_i8_uh_gt_f __cmodel_sx_cmp_i8_uh_gt
#define sx_cmp_i8_uh_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uh_gt(c, d); \
}

/*  cmpISA_EC_cmpUB ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_ub_eq_f __cmodel_sx_cmp_r_ub_eq
#define sx_cmp_r_ub_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_eq(c, d); \
}

#define sx_cmp_r_ub_ne_f __cmodel_sx_cmp_r_ub_ne
#define sx_cmp_r_ub_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_ne(c, d); \
}

#define sx_cmp_r_ub_ge_f __cmodel_sx_cmp_r_ub_ge
#define sx_cmp_r_ub_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_ge(c, d); \
}

#define sx_cmp_r_ub_lt_f __cmodel_sx_cmp_r_ub_lt
#define sx_cmp_r_ub_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_lt(c, d); \
}

#define sx_cmp_r_ub_le_f __cmodel_sx_cmp_r_ub_le
#define sx_cmp_r_ub_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_le(c, d); \
}

#define sx_cmp_r_ub_gt_f __cmodel_sx_cmp_r_ub_gt
#define sx_cmp_r_ub_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_ub_gt(c, d); \
}

/*  cmpISA_EC_cmpUB ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_ub_eq_f __cmodel_sx_cmp_i8_ub_eq
#define sx_cmp_i8_ub_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_eq(c, d); \
}

#define sx_cmp_i8_ub_ne_f __cmodel_sx_cmp_i8_ub_ne
#define sx_cmp_i8_ub_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_ne(c, d); \
}

#define sx_cmp_i8_ub_ge_f __cmodel_sx_cmp_i8_ub_ge
#define sx_cmp_i8_ub_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_ge(c, d); \
}

#define sx_cmp_i8_ub_lt_f __cmodel_sx_cmp_i8_ub_lt
#define sx_cmp_i8_ub_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_lt(c, d); \
}

#define sx_cmp_i8_ub_le_f __cmodel_sx_cmp_i8_ub_le
#define sx_cmp_i8_ub_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_le(c, d); \
}

#define sx_cmp_i8_ub_gt_f __cmodel_sx_cmp_i8_ub_gt
#define sx_cmp_i8_ub_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ub_gt(c, d); \
}

/*  cmpISA_EC_cmpUHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_uhp_eq_f __cmodel_sx_cmp_r_uhp_eq
#define sx_cmp_r_uhp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_eq(c, d); \
}

#define sx_cmp_r_uhp_ne_f __cmodel_sx_cmp_r_uhp_ne
#define sx_cmp_r_uhp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_ne(c, d); \
}

#define sx_cmp_r_uhp_ge_f __cmodel_sx_cmp_r_uhp_ge
#define sx_cmp_r_uhp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_ge(c, d); \
}

#define sx_cmp_r_uhp_lt_f __cmodel_sx_cmp_r_uhp_lt
#define sx_cmp_r_uhp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_lt(c, d); \
}

#define sx_cmp_r_uhp_le_f __cmodel_sx_cmp_r_uhp_le
#define sx_cmp_r_uhp_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_le(c, d); \
}

#define sx_cmp_r_uhp_gt_f __cmodel_sx_cmp_r_uhp_gt
#define sx_cmp_r_uhp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_uhp_gt(c, d); \
}

/*  cmpISA_EC_cmpUHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_uhp_eq_f __cmodel_sx_cmp_i8_uhp_eq
#define sx_cmp_i8_uhp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_eq(c, d); \
}

#define sx_cmp_i8_uhp_ne_f __cmodel_sx_cmp_i8_uhp_ne
#define sx_cmp_i8_uhp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_ne(c, d); \
}

#define sx_cmp_i8_uhp_ge_f __cmodel_sx_cmp_i8_uhp_ge
#define sx_cmp_i8_uhp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_ge(c, d); \
}

#define sx_cmp_i8_uhp_lt_f __cmodel_sx_cmp_i8_uhp_lt
#define sx_cmp_i8_uhp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_lt(c, d); \
}

#define sx_cmp_i8_uhp_le_f __cmodel_sx_cmp_i8_uhp_le
#define sx_cmp_i8_uhp_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_le(c, d); \
}

#define sx_cmp_i8_uhp_gt_f __cmodel_sx_cmp_i8_uhp_gt
#define sx_cmp_i8_uhp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_uhp_gt(c, d); \
}

/*  cmpISA_EC_cmpUBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_ubp_eq_f __cmodel_sx_cmp_r_ubp_eq
#define sx_cmp_r_ubp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_eq(c, d); \
}

#define sx_cmp_r_ubp_ne_f __cmodel_sx_cmp_r_ubp_ne
#define sx_cmp_r_ubp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_ne(c, d); \
}

#define sx_cmp_r_ubp_ge_f __cmodel_sx_cmp_r_ubp_ge
#define sx_cmp_r_ubp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_ge(c, d); \
}

#define sx_cmp_r_ubp_lt_f __cmodel_sx_cmp_r_ubp_lt
#define sx_cmp_r_ubp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_lt(c, d); \
}

#define sx_cmp_r_ubp_le_f __cmodel_sx_cmp_r_ubp_le
#define sx_cmp_r_ubp_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_le(c, d); \
}

#define sx_cmp_r_ubp_gt_f __cmodel_sx_cmp_r_ubp_gt
#define sx_cmp_r_ubp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_ubp_gt(c, d); \
}

/*  cmpISA_EC_cmpUBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_ubp_eq_f __cmodel_sx_cmp_i8_ubp_eq
#define sx_cmp_i8_ubp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_eq(c, d); \
}

#define sx_cmp_i8_ubp_ne_f __cmodel_sx_cmp_i8_ubp_ne
#define sx_cmp_i8_ubp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_ne(c, d); \
}

#define sx_cmp_i8_ubp_ge_f __cmodel_sx_cmp_i8_ubp_ge
#define sx_cmp_i8_ubp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_ge(c, d); \
}

#define sx_cmp_i8_ubp_lt_f __cmodel_sx_cmp_i8_ubp_lt
#define sx_cmp_i8_ubp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_lt(c, d); \
}

#define sx_cmp_i8_ubp_le_f __cmodel_sx_cmp_i8_ubp_le
#define sx_cmp_i8_ubp_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_le(c, d); \
}

#define sx_cmp_i8_ubp_gt_f __cmodel_sx_cmp_i8_ubp_gt
#define sx_cmp_i8_ubp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ubp_gt(c, d); \
}

/*  cmpISA_EC_cmp ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_eq_f __cmodel_sx_cmp_r_eq
#define sx_cmp_r_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_eq(c, d); \
}

#define sx_cmp_r_ne_f __cmodel_sx_cmp_r_ne
#define sx_cmp_r_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_ne(c, d); \
}

#define sx_cmp_r_ge_f __cmodel_sx_cmp_r_ge
#define sx_cmp_r_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_ge(c, d); \
}

#define sx_cmp_r_lt_f __cmodel_sx_cmp_r_lt
#define sx_cmp_r_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_lt(c, d); \
}

#define sx_cmp_r_le_f __cmodel_sx_cmp_r_le
#define sx_cmp_r_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_le(c, d); \
}

#define sx_cmp_r_gt_f __cmodel_sx_cmp_r_gt
#define sx_cmp_r_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_gt(c, d); \
}

/*  cmpISA_EC_cmp ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_eq_f __cmodel_sx_cmp_i8_eq
#define sx_cmp_i8_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_eq(c, d); \
}

#define sx_cmp_i8_ne_f __cmodel_sx_cmp_i8_ne
#define sx_cmp_i8_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ne(c, d); \
}

#define sx_cmp_i8_ge_f __cmodel_sx_cmp_i8_ge
#define sx_cmp_i8_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_ge(c, d); \
}

#define sx_cmp_i8_lt_f __cmodel_sx_cmp_i8_lt
#define sx_cmp_i8_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_lt(c, d); \
}

#define sx_cmp_i8_le_f __cmodel_sx_cmp_i8_le
#define sx_cmp_i8_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_le(c, d); \
}

#define sx_cmp_i8_gt_f __cmodel_sx_cmp_i8_gt
#define sx_cmp_i8_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_gt(c, d); \
}

/*  cmpISA_EC_cmpH ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_h_eq_f __cmodel_sx_cmp_r_h_eq
#define sx_cmp_r_h_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_eq(c, d); \
}

#define sx_cmp_r_h_ne_f __cmodel_sx_cmp_r_h_ne
#define sx_cmp_r_h_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_ne(c, d); \
}

#define sx_cmp_r_h_ge_f __cmodel_sx_cmp_r_h_ge
#define sx_cmp_r_h_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_ge(c, d); \
}

#define sx_cmp_r_h_lt_f __cmodel_sx_cmp_r_h_lt
#define sx_cmp_r_h_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_lt(c, d); \
}

#define sx_cmp_r_h_le_f __cmodel_sx_cmp_r_h_le
#define sx_cmp_r_h_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_le(c, d); \
}

#define sx_cmp_r_h_gt_f __cmodel_sx_cmp_r_h_gt
#define sx_cmp_r_h_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_h_gt(c, d); \
}

/*  cmpISA_EC_cmpH ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_h_eq_f __cmodel_sx_cmp_i8_h_eq
#define sx_cmp_i8_h_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_eq(c, d); \
}

#define sx_cmp_i8_h_ne_f __cmodel_sx_cmp_i8_h_ne
#define sx_cmp_i8_h_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_ne(c, d); \
}

#define sx_cmp_i8_h_ge_f __cmodel_sx_cmp_i8_h_ge
#define sx_cmp_i8_h_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_ge(c, d); \
}

#define sx_cmp_i8_h_lt_f __cmodel_sx_cmp_i8_h_lt
#define sx_cmp_i8_h_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_lt(c, d); \
}

#define sx_cmp_i8_h_le_f __cmodel_sx_cmp_i8_h_le
#define sx_cmp_i8_h_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_le(c, d); \
}

#define sx_cmp_i8_h_gt_f __cmodel_sx_cmp_i8_h_gt
#define sx_cmp_i8_h_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_h_gt(c, d); \
}

/*  cmpISA_EC_cmpHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_hp_eq_f __cmodel_sx_cmp_r_hp_eq
#define sx_cmp_r_hp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_eq(c, d); \
}

#define sx_cmp_r_hp_ne_f __cmodel_sx_cmp_r_hp_ne
#define sx_cmp_r_hp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_ne(c, d); \
}

#define sx_cmp_r_hp_ge_f __cmodel_sx_cmp_r_hp_ge
#define sx_cmp_r_hp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_ge(c, d); \
}

#define sx_cmp_r_hp_lt_f __cmodel_sx_cmp_r_hp_lt
#define sx_cmp_r_hp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_lt(c, d); \
}

#define sx_cmp_r_hp_le_f __cmodel_sx_cmp_r_hp_le
#define sx_cmp_r_hp_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_le(c, d); \
}

#define sx_cmp_r_hp_gt_f __cmodel_sx_cmp_r_hp_gt
#define sx_cmp_r_hp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_hp_gt(c, d); \
}

/*  cmpISA_EC_cmpHP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_hp_eq_f __cmodel_sx_cmp_i8_hp_eq
#define sx_cmp_i8_hp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_eq(c, d); \
}

#define sx_cmp_i8_hp_ne_f __cmodel_sx_cmp_i8_hp_ne
#define sx_cmp_i8_hp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_ne(c, d); \
}

#define sx_cmp_i8_hp_ge_f __cmodel_sx_cmp_i8_hp_ge
#define sx_cmp_i8_hp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_ge(c, d); \
}

#define sx_cmp_i8_hp_lt_f __cmodel_sx_cmp_i8_hp_lt
#define sx_cmp_i8_hp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_lt(c, d); \
}

#define sx_cmp_i8_hp_le_f __cmodel_sx_cmp_i8_hp_le
#define sx_cmp_i8_hp_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_le(c, d); \
}

#define sx_cmp_i8_hp_gt_f __cmodel_sx_cmp_i8_hp_gt
#define sx_cmp_i8_hp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_hp_gt(c, d); \
}

/*  cmpISA_EC_cmpB ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_b_eq_f __cmodel_sx_cmp_r_b_eq
#define sx_cmp_r_b_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_eq(c, d); \
}

#define sx_cmp_r_b_ne_f __cmodel_sx_cmp_r_b_ne
#define sx_cmp_r_b_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_ne(c, d); \
}

#define sx_cmp_r_b_ge_f __cmodel_sx_cmp_r_b_ge
#define sx_cmp_r_b_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_ge(c, d); \
}

#define sx_cmp_r_b_lt_f __cmodel_sx_cmp_r_b_lt
#define sx_cmp_r_b_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_lt(c, d); \
}

#define sx_cmp_r_b_le_f __cmodel_sx_cmp_r_b_le
#define sx_cmp_r_b_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_le(c, d); \
}

#define sx_cmp_r_b_gt_f __cmodel_sx_cmp_r_b_gt
#define sx_cmp_r_b_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_b_gt(c, d); \
}

/*  cmpISA_EC_cmpB ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_b_eq_f __cmodel_sx_cmp_i8_b_eq
#define sx_cmp_i8_b_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_eq(c, d); \
}

#define sx_cmp_i8_b_ne_f __cmodel_sx_cmp_i8_b_ne
#define sx_cmp_i8_b_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_ne(c, d); \
}

#define sx_cmp_i8_b_ge_f __cmodel_sx_cmp_i8_b_ge
#define sx_cmp_i8_b_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_ge(c, d); \
}

#define sx_cmp_i8_b_lt_f __cmodel_sx_cmp_i8_b_lt
#define sx_cmp_i8_b_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_lt(c, d); \
}

#define sx_cmp_i8_b_le_f __cmodel_sx_cmp_i8_b_le
#define sx_cmp_i8_b_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_le(c, d); \
}

#define sx_cmp_i8_b_gt_f __cmodel_sx_cmp_i8_b_gt
#define sx_cmp_i8_b_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_b_gt(c, d); \
}

/*  cmpISA_EC_cmpBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, gpr);
 */
#define sx_cmp_r_bp_eq_f __cmodel_sx_cmp_r_bp_eq
#define sx_cmp_r_bp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_eq(c, d); \
}

#define sx_cmp_r_bp_ne_f __cmodel_sx_cmp_r_bp_ne
#define sx_cmp_r_bp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_ne(c, d); \
}

#define sx_cmp_r_bp_ge_f __cmodel_sx_cmp_r_bp_ge
#define sx_cmp_r_bp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_ge(c, d); \
}

#define sx_cmp_r_bp_lt_f __cmodel_sx_cmp_r_bp_lt
#define sx_cmp_r_bp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_lt(c, d); \
}

#define sx_cmp_r_bp_le_f __cmodel_sx_cmp_r_bp_le
#define sx_cmp_r_bp_le(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_le(c, d); \
}

#define sx_cmp_r_bp_gt_f __cmodel_sx_cmp_r_bp_gt
#define sx_cmp_r_bp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_r_bp_gt(c, d); \
}

/*  cmpISA_EC_cmpBP ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst cmp( ISA_EC_cmp, gpr, <u8>);
 */
#define sx_cmp_i8_bp_eq_f __cmodel_sx_cmp_i8_bp_eq
#define sx_cmp_i8_bp_eq(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_eq(c, d); \
}

#define sx_cmp_i8_bp_ne_f __cmodel_sx_cmp_i8_bp_ne
#define sx_cmp_i8_bp_ne(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_ne(c, d); \
}

#define sx_cmp_i8_bp_ge_f __cmodel_sx_cmp_i8_bp_ge
#define sx_cmp_i8_bp_ge(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_ge(c, d); \
}

#define sx_cmp_i8_bp_lt_f __cmodel_sx_cmp_i8_bp_lt
#define sx_cmp_i8_bp_lt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_lt(c, d); \
}

#define sx_cmp_i8_bp_le_f __cmodel_sx_cmp_i8_bp_le
#define sx_cmp_i8_bp_le(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_le(c, d); \
}

#define sx_cmp_i8_bp_gt_f __cmodel_sx_cmp_i8_bp_gt
#define sx_cmp_i8_bp_gt(a, c, d) { \
  a = __cmodel_sx_cmp_i8_bp_gt(c, d); \
}

/*  extb gpr, gpr
 * gpr extb( gpr);
 */
#define sx_extb_f __cmodel_sx_extb
#define sx_extb(a, b) { \
  a = __cmodel_sx_extb(b); \
}

/*  exth gpr, gpr
 * gpr exth( gpr);
 */
#define sx_exth_f __cmodel_sx_exth
#define sx_exth(a, b) { \
  a = __cmodel_sx_exth(b); \
}

/*  extub gpr, gpr
 * gpr extub( gpr);
 */
#define sx_extub_f __cmodel_sx_extub
#define sx_extub(a, b) { \
  a = __cmodel_sx_extub(b); \
}

/*  extuh gpr, gpr
 * gpr extuh( gpr);
 */
#define sx_extuh_f __cmodel_sx_extuh
#define sx_extuh(a, b) { \
  a = __cmodel_sx_extuh(b); \
}

/*  idle <u2>
 * idle( <u2>);
 */
#define sx_idle_f __cmodel_sx_idle
#define sx_idle(a) { \
  __cmodel_sx_idle(a); \
}

/*  lzc gpr, gpr
 * gpr lzc( gpr);
 */
#define sx_lzc_f __cmodel_sx_lzc
#define sx_lzc(a, b) { \
  a = __cmodel_sx_lzc(b); \
}

/*  makehp gpr, <s16>
 * gpr makehp( <s16>);
 */
#define sx_makehp_f __cmodel_sx_makehp
#define sx_makehp(a, b) { \
  a = __cmodel_sx_makehp(b); \
}

/*  make gpr, <abs0to14_abs0to15_abs16to31_da0to14_gprel16to31_gotoffs16to31_s16>
 * gpr make( <abs0to14_abs0to15_abs16to31_da0to14_gprel16to31_gotoffs16to31_s16>);
 */
#define sx_make_f __cmodel_sx_make
#define sx_make(a, b) { \
  a = __cmodel_sx_make(b); \
}

/*  maxbp gpr, gpr, gpr
 * gpr maxbp( gpr, gpr);
 */
#define sx_maxbp_r_f __cmodel_sx_maxbp_r
#define sx_maxbp_r(a, b, c) { \
  a = __cmodel_sx_maxbp_r(b, c); \
}

/*  maxbp gpr, gpr, <u8>
 * gpr maxbp( gpr, <u8>);
 */
#define sx_maxbp_i8_f __cmodel_sx_maxbp_i8
#define sx_maxbp_i8(a, b, c) { \
  a = __cmodel_sx_maxbp_i8(b, c); \
}

/*  maxhp gpr, gpr, gpr
 * gpr maxhp( gpr, gpr);
 */
#define sx_maxhp_r_f __cmodel_sx_maxhp_r
#define sx_maxhp_r(a, b, c) { \
  a = __cmodel_sx_maxhp_r(b, c); \
}

/*  maxhp gpr, gpr, <u8>
 * gpr maxhp( gpr, <u8>);
 */
#define sx_maxhp_i8_f __cmodel_sx_maxhp_i8
#define sx_maxhp_i8(a, b, c) { \
  a = __cmodel_sx_maxhp_i8(b, c); \
}

/*  maxubp gpr, gpr, gpr
 * gpr maxubp( gpr, gpr);
 */
#define sx_maxubp_r_f __cmodel_sx_maxubp_r
#define sx_maxubp_r(a, b, c) { \
  a = __cmodel_sx_maxubp_r(b, c); \
}

/*  maxubp gpr, gpr, <u8>
 * gpr maxubp( gpr, <u8>);
 */
#define sx_maxubp_i8_f __cmodel_sx_maxubp_i8
#define sx_maxubp_i8(a, b, c) { \
  a = __cmodel_sx_maxubp_i8(b, c); \
}

/*  maxuhp gpr, gpr, gpr
 * gpr maxuhp( gpr, gpr);
 */
#define sx_maxuhp_r_f __cmodel_sx_maxuhp_r
#define sx_maxuhp_r(a, b, c) { \
  a = __cmodel_sx_maxuhp_r(b, c); \
}

/*  maxuhp gpr, gpr, <u8>
 * gpr maxuhp( gpr, <u8>);
 */
#define sx_maxuhp_i8_f __cmodel_sx_maxuhp_i8
#define sx_maxuhp_i8(a, b, c) { \
  a = __cmodel_sx_maxuhp_i8(b, c); \
}

/*  maxu gpr, gpr, gpr
 * gpr maxu( gpr, gpr);
 */
#define sx_maxu_r_f __cmodel_sx_maxu_r
#define sx_maxu_r(a, b, c) { \
  a = __cmodel_sx_maxu_r(b, c); \
}

/*  maxu gpr, gpr, <u8>
 * gpr maxu( gpr, <u8>);
 */
#define sx_maxu_i8_f __cmodel_sx_maxu_i8
#define sx_maxu_i8(a, b, c) { \
  a = __cmodel_sx_maxu_i8(b, c); \
}

/*  max gpr, gpr, gpr
 * gpr max( gpr, gpr);
 */
#define sx_max_r_f __cmodel_sx_max_r
#define sx_max_r(a, b, c) { \
  a = __cmodel_sx_max_r(b, c); \
}

/*  max gpr, gpr, <u8>
 * gpr max( gpr, <u8>);
 */
#define sx_max_i8_f __cmodel_sx_max_i8
#define sx_max_i8(a, b, c) { \
  a = __cmodel_sx_max_i8(b, c); \
}

/*  minbp gpr, gpr, gpr
 * gpr minbp( gpr, gpr);
 */
#define sx_minbp_r_f __cmodel_sx_minbp_r
#define sx_minbp_r(a, b, c) { \
  a = __cmodel_sx_minbp_r(b, c); \
}

/*  minbp gpr, gpr, <u8>
 * gpr minbp( gpr, <u8>);
 */
#define sx_minbp_i8_f __cmodel_sx_minbp_i8
#define sx_minbp_i8(a, b, c) { \
  a = __cmodel_sx_minbp_i8(b, c); \
}

/*  minhp gpr, gpr, gpr
 * gpr minhp( gpr, gpr);
 */
#define sx_minhp_r_f __cmodel_sx_minhp_r
#define sx_minhp_r(a, b, c) { \
  a = __cmodel_sx_minhp_r(b, c); \
}

/*  minhp gpr, gpr, <u8>
 * gpr minhp( gpr, <u8>);
 */
#define sx_minhp_i8_f __cmodel_sx_minhp_i8
#define sx_minhp_i8(a, b, c) { \
  a = __cmodel_sx_minhp_i8(b, c); \
}

/*  minubp gpr, gpr, gpr
 * gpr minubp( gpr, gpr);
 */
#define sx_minubp_r_f __cmodel_sx_minubp_r
#define sx_minubp_r(a, b, c) { \
  a = __cmodel_sx_minubp_r(b, c); \
}

/*  minubp gpr, gpr, <u8>
 * gpr minubp( gpr, <u8>);
 */
#define sx_minubp_i8_f __cmodel_sx_minubp_i8
#define sx_minubp_i8(a, b, c) { \
  a = __cmodel_sx_minubp_i8(b, c); \
}

/*  minuhp gpr, gpr, gpr
 * gpr minuhp( gpr, gpr);
 */
#define sx_minuhp_r_f __cmodel_sx_minuhp_r
#define sx_minuhp_r(a, b, c) { \
  a = __cmodel_sx_minuhp_r(b, c); \
}

/*  minuhp gpr, gpr, <u8>
 * gpr minuhp( gpr, <u8>);
 */
#define sx_minuhp_i8_f __cmodel_sx_minuhp_i8
#define sx_minuhp_i8(a, b, c) { \
  a = __cmodel_sx_minuhp_i8(b, c); \
}

/*  minu gpr, gpr, gpr
 * gpr minu( gpr, gpr);
 */
#define sx_minu_r_f __cmodel_sx_minu_r
#define sx_minu_r(a, b, c) { \
  a = __cmodel_sx_minu_r(b, c); \
}

/*  minu gpr, gpr, <u8>
 * gpr minu( gpr, <u8>);
 */
#define sx_minu_i8_f __cmodel_sx_minu_i8
#define sx_minu_i8(a, b, c) { \
  a = __cmodel_sx_minu_i8(b, c); \
}

/*  min gpr, gpr, gpr
 * gpr min( gpr, gpr);
 */
#define sx_min_r_f __cmodel_sx_min_r
#define sx_min_r(a, b, c) { \
  a = __cmodel_sx_min_r(b, c); \
}

/*  min gpr, gpr, <u8>
 * gpr min( gpr, <u8>);
 */
#define sx_min_i8_f __cmodel_sx_min_i8
#define sx_min_i8(a, b, c) { \
  a = __cmodel_sx_min_i8(b, c); \
}

/*  more gpr, <abs0to15_gprel0to15_gotoffs0to15_u16>
 * gpr more( gpr, <abs0to15_gprel0to15_gotoffs0to15_u16>);
 */
#define sx_more_f __cmodel_sx_more
#define sx_more(a, b) { \
  a = __cmodel_sx_more(a, b); \
}

/*  moveh2h gpr, gpr
 * gpr moveh2h( gpr, gpr);
 */
#define sx_moveh2h_f __cmodel_sx_moveh2h
#define sx_moveh2h(a, b) { \
  a = __cmodel_sx_moveh2h(a, b); \
}

/*  moveh2l gpr, gpr
 * gpr moveh2l( gpr, gpr);
 */
#define sx_moveh2l_f __cmodel_sx_moveh2l
#define sx_moveh2l(a, b) { \
  a = __cmodel_sx_moveh2l(a, b); \
}

/*  movel2h gpr, gpr
 * gpr movel2h( gpr, gpr);
 */
#define sx_movel2h_f __cmodel_sx_movel2h
#define sx_movel2h(a, b) { \
  a = __cmodel_sx_movel2h(a, b); \
}

/*  movel2l gpr, gpr
 * gpr movel2l( gpr, gpr);
 */
#define sx_movel2l_f __cmodel_sx_movel2l
#define sx_movel2l(a, b) { \
  a = __cmodel_sx_movel2l(a, b); \
}

/*  negbp gpr, gpr
 * gpr negbp( gpr);
 */
#define sx_negbp_f __cmodel_sx_negbp
#define sx_negbp(a, b) { \
  a = __cmodel_sx_negbp(b); \
}

/*  neghp gpr, gpr
 * gpr neghp( gpr);
 */
#define sx_neghp_f __cmodel_sx_neghp
#define sx_neghp(a, b) { \
  a = __cmodel_sx_neghp(b); \
}

/*  negubp gpr, gpr
 * gpr negubp( gpr);
 */
#define sx_negubp_f __cmodel_sx_negubp
#define sx_negubp(a, b) { \
  a = __cmodel_sx_negubp(b); \
}

/*  neguhp gpr, gpr
 * gpr neguhp( gpr);
 */
#define sx_neguhp_f __cmodel_sx_neguhp
#define sx_neguhp(a, b) { \
  a = __cmodel_sx_neguhp(b); \
}

/*  negu gpr, gpr
 * gpr negu( gpr);
 */
#define sx_negu_f __cmodel_sx_negu
#define sx_negu(a, b) { \
  a = __cmodel_sx_negu(b); \
}

/*  neg gpr, gpr
 * gpr neg( gpr);
 */
#define sx_neg_f __cmodel_sx_neg
#define sx_neg(a, b) { \
  a = __cmodel_sx_neg(b); \
}

/* nop
 * nop( );
 */
#define sx_nop_f __cmodel_sx_nop
#define sx_nop() { \
  __cmodel_sx_nop(); \
}

/*  norn gpr, gpr, <u8>
 * gpr norn( gpr, <u8>);
 */
#define sx_norn_f __cmodel_sx_norn
#define sx_norn(a, b, c) { \
  a = __cmodel_sx_norn(b, c); \
}

/*  not gpr, gpr
 * gpr not( gpr);
 */
#define sx_not_f __cmodel_sx_not
#define sx_not(a, b) { \
  a = __cmodel_sx_not(b); \
}

/*  org ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst org( gr, gr);
 */
#define sx_org_f __cmodel_sx_org
#define sx_org(a, b, c) { \
  a = __cmodel_sx_org(b, c); \
}

/*  or gpr, gpr, gpr
 * gpr or( gpr, gpr);
 */
#define sx_or_r_f __cmodel_sx_or_r
#define sx_or_r(a, b, c) { \
  a = __cmodel_sx_or_r(b, c); \
}

/*  or gpr, gpr, <u8>
 * gpr or( gpr, <u8>);
 */
#define sx_or_i8_f __cmodel_sx_or_i8
#define sx_or_i8(a, b, c) { \
  a = __cmodel_sx_or_i8(b, c); \
}

/*  prior gpr, gpr
 * gpr prior( gpr);
 */
#define sx_prior_f __cmodel_sx_prior
#define sx_prior(a, b) { \
  a = __cmodel_sx_prior(b); \
}

/*  rotl gpr, gpr, gpr
 * gpr rotl( gpr, gpr);
 */
#define sx_rotl_r_f __cmodel_sx_rotl_r
#define sx_rotl_r(a, b, c) { \
  a = __cmodel_sx_rotl_r(b, c); \
}

/*  rotl gpr, gpr, <u5>
 * gpr rotl( gpr, <u5>);
 */
#define sx_rotl_i5_f __cmodel_sx_rotl_i5
#define sx_rotl_i5(a, b, c) { \
  a = __cmodel_sx_rotl_i5(b, c); \
}

/* rte
 * rte( );
 */
#define sx_rte_f __cmodel_sx_rte
#define sx_rte() { \
  __cmodel_sx_rte(); \
}

/*  shlmu gpr, gpr
 * gpr shlmu( gpr);
 */
#define sx_shlmu_r_f __cmodel_sx_shlmu_r
#define sx_shlmu_r(a, b) { \
  a = __cmodel_sx_shlmu_r(b); \
}

/*  shlmu gpr, <u5>
 * gpr shlmu( <u5>);
 */
#define sx_shlmu_i5_f __cmodel_sx_shlmu_i5
#define sx_shlmu_i5(a, b) { \
  a = __cmodel_sx_shlmu_i5(b); \
}

/*  shlnu gpr, gpr, gpr
 * gpr shlnu( gpr, gpr);
 */
#define sx_shlnu_f __cmodel_sx_shlnu
#define sx_shlnu(a, b, c) { \
  a = __cmodel_sx_shlnu(b, c); \
}

/*  shlu gpr, gpr, gpr
 * gpr shlu( gpr, gpr);
 */
#define sx_shlu_r_f __cmodel_sx_shlu_r
#define sx_shlu_r(a, b, c) { \
  a = __cmodel_sx_shlu_r(b, c); \
}

/*  shlu gpr, gpr, <u5>
 * gpr shlu( gpr, <u5>);
 */
#define sx_shlu_i5_f __cmodel_sx_shlu_i5
#define sx_shlu_i5(a, b, c) { \
  a = __cmodel_sx_shlu_i5(b, c); \
}

/*  shl gpr, gpr, gpr
 * gpr shl( gpr, gpr);
 */
#define sx_shl_r_f __cmodel_sx_shl_r
#define sx_shl_r(a, b, c) { \
  a = __cmodel_sx_shl_r(b, c); \
}

/*  shl gpr, gpr, <u5>
 * gpr shl( gpr, <u5>);
 */
#define sx_shl_i5_f __cmodel_sx_shl_i5
#define sx_shl_i5(a, b, c) { \
  a = __cmodel_sx_shl_i5(b, c); \
}

/*  shrmu gpr, gpr
 * gpr shrmu( gpr);
 */
#define sx_shrmu_r_f __cmodel_sx_shrmu_r
#define sx_shrmu_r(a, b) { \
  a = __cmodel_sx_shrmu_r(b); \
}

/*  shrmu gpr, <u5>
 * gpr shrmu( <u5>);
 */
#define sx_shrmu_i5_f __cmodel_sx_shrmu_i5
#define sx_shrmu_i5(a, b) { \
  a = __cmodel_sx_shrmu_i5(b); \
}

/*  shrnu gpr, gpr, gpr
 * gpr shrnu( gpr, gpr);
 */
#define sx_shrnu_f __cmodel_sx_shrnu
#define sx_shrnu(a, b, c) { \
  a = __cmodel_sx_shrnu(b, c); \
}

/*  shru gpr, gpr, gpr
 * gpr shru( gpr, gpr);
 */
#define sx_shru_r_f __cmodel_sx_shru_r
#define sx_shru_r(a, b, c) { \
  a = __cmodel_sx_shru_r(b, c); \
}

/*  shru gpr, gpr, <u5>
 * gpr shru( gpr, <u5>);
 */
#define sx_shru_i5_f __cmodel_sx_shru_i5
#define sx_shru_i5(a, b, c) { \
  a = __cmodel_sx_shru_i5(b, c); \
}

/*  shr gpr, gpr, gpr
 * gpr shr( gpr, gpr);
 */
#define sx_shr_r_f __cmodel_sx_shr_r
#define sx_shr_r(a, b, c) { \
  a = __cmodel_sx_shr_r(b, c); \
}

/*  shr gpr, gpr, <u5>
 * gpr shr( gpr, <u5>);
 */
#define sx_shr_i5_f __cmodel_sx_shr_i5
#define sx_shr_i5(a, b, c) { \
  a = __cmodel_sx_shr_i5(b, c); \
}

/*  subbp gpr, gpr, gpr
 * gpr subbp( gpr, gpr);
 */
#define sx_subbp_r_f __cmodel_sx_subbp_r
#define sx_subbp_r(a, b, c) { \
  a = __cmodel_sx_subbp_r(b, c); \
}

/*  subbp gpr, gpr, <u8>
 * gpr subbp( gpr, <u8>);
 */
#define sx_subbp_i8_f __cmodel_sx_subbp_i8
#define sx_subbp_i8(a, b, c) { \
  a = __cmodel_sx_subbp_i8(b, c); \
}

/*  subcu gpr, gpr, gpr
 * gpr subcu( gpr, gpr);
 */
#define sx_subcu_f __cmodel_sx_subcu
#define sx_subcu(a, b, c) { \
  a = __cmodel_sx_subcu(b, c); \
}

/*  subc gpr, gpr, gpr
 * gpr subc( gpr, gpr);
 */
#define sx_subc_f __cmodel_sx_subc
#define sx_subc(a, b, c) { \
  a = __cmodel_sx_subc(b, c); \
}

/*  subhp gpr, gpr, gpr
 * gpr subhp( gpr, gpr);
 */
#define sx_subhp_r_f __cmodel_sx_subhp_r
#define sx_subhp_r(a, b, c) { \
  a = __cmodel_sx_subhp_r(b, c); \
}

/*  subhp gpr, gpr, <u8>
 * gpr subhp( gpr, <u8>);
 */
#define sx_subhp_i8_f __cmodel_sx_subhp_i8
#define sx_subhp_i8(a, b, c) { \
  a = __cmodel_sx_subhp_i8(b, c); \
}

/*  sububp gpr, gpr, gpr
 * gpr sububp( gpr, gpr);
 */
#define sx_sububp_r_f __cmodel_sx_sububp_r
#define sx_sububp_r(a, b, c) { \
  a = __cmodel_sx_sububp_r(b, c); \
}

/*  sububp gpr, gpr, <u8>
 * gpr sububp( gpr, <u8>);
 */
#define sx_sububp_i8_f __cmodel_sx_sububp_i8
#define sx_sububp_i8(a, b, c) { \
  a = __cmodel_sx_sububp_i8(b, c); \
}

/*  subuhp gpr, gpr, gpr
 * gpr subuhp( gpr, gpr);
 */
#define sx_subuhp_r_f __cmodel_sx_subuhp_r
#define sx_subuhp_r(a, b, c) { \
  a = __cmodel_sx_subuhp_r(b, c); \
}

/*  subuhp gpr, gpr, <u8>
 * gpr subuhp( gpr, <u8>);
 */
#define sx_subuhp_i8_f __cmodel_sx_subuhp_i8
#define sx_subuhp_i8(a, b, c) { \
  a = __cmodel_sx_subuhp_i8(b, c); \
}

/*  subu gpr, gpr, gpr
 * gpr subu( gpr, gpr);
 */
#define sx_subu_r_f __cmodel_sx_subu_r
#define sx_subu_r(a, b, c) { \
  a = __cmodel_sx_subu_r(b, c); \
}

/*  subu gpr, gpr, <u8>
 * gpr subu( gpr, <u8>);
 */
#define sx_subu_i8_f __cmodel_sx_subu_i8
#define sx_subu_i8(a, b, c) { \
  a = __cmodel_sx_subu_i8(b, c); \
}

/*  sub gpr, gpr, gpr
 * gpr sub( gpr, gpr);
 */
#define sx_sub_r_f __cmodel_sx_sub_r
#define sx_sub_r(a, b, c) { \
  a = __cmodel_sx_sub_r(b, c); \
}

/*  sub gpr, gpr, <u8>
 * gpr sub( gpr, <u8>);
 */
#define sx_sub_i8_f __cmodel_sx_sub_i8
#define sx_sub_i8(a, b, c) { \
  a = __cmodel_sx_sub_i8(b, c); \
}

/* trap <u2>
 * trap( <u2>);
 */
#define sx_trap_f __cmodel_sx_trap
#define sx_trap(a) { \
  __cmodel_sx_trap(a); \
}

/*  tISA_EC_btestBIT ISA_REGISTER_SUBCLASS_gr_dst, gpr, gpr
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, gpr);
 */
#define sx_tfbit_r_f __cmodel_sx_tfbit_r
#define sx_tfbit_r(a, c, d) { \
  a = __cmodel_sx_tfbit_r(c, d); \
}

#define sx_ttbit_r_f __cmodel_sx_ttbit_r
#define sx_ttbit_r(a, c, d) { \
  a = __cmodel_sx_ttbit_r(c, d); \
}

/*  tISA_EC_btestBIT ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u5>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u5>);
 */
#define sx_tfbit_i5_f __cmodel_sx_tfbit_i5
#define sx_tfbit_i5(a, c, d) { \
  a = __cmodel_sx_tfbit_i5(c, d); \
}

#define sx_ttbit_i5_f __cmodel_sx_ttbit_i5
#define sx_ttbit_i5(a, c, d) { \
  a = __cmodel_sx_ttbit_i5(c, d); \
}

/*  tISA_EC_btestAND ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u8>);
 */
#define sx_tfand_f __cmodel_sx_tfand
#define sx_tfand(a, c, d) { \
  a = __cmodel_sx_tfand(c, d); \
}

#define sx_ttand_f __cmodel_sx_ttand
#define sx_ttand(a, c, d) { \
  a = __cmodel_sx_ttand(c, d); \
}

/*  tISA_EC_btestANDN ISA_REGISTER_SUBCLASS_gr_dst, gpr, <u8>
 * ISA_REGISTER_SUBCLASS_gr_dst t( ISA_EC_btest, gpr, <u8>);
 */
#define sx_tfandn_f __cmodel_sx_tfandn
#define sx_tfandn(a, c, d) { \
  a = __cmodel_sx_tfandn(c, d); \
}

#define sx_ttandn_f __cmodel_sx_ttandn
#define sx_ttandn(a, c, d) { \
  a = __cmodel_sx_ttandn(c, d); \
}

/*  xorg ISA_REGISTER_SUBCLASS_gr_dst, gr, gr
 * ISA_REGISTER_SUBCLASS_gr_dst xorg( gr, gr);
 */
#define sx_xorg_f __cmodel_sx_xorg
#define sx_xorg(a, b, c) { \
  a = __cmodel_sx_xorg(b, c); \
}

/*  xor gpr, gpr, gpr
 * gpr xor( gpr, gpr);
 */
#define sx_xor_r_f __cmodel_sx_xor_r
#define sx_xor_r(a, b, c) { \
  a = __cmodel_sx_xor_r(b, c); \
}

/*  xor gpr, gpr, <u8>
 * gpr xor( gpr, <u8>);
 */
#define sx_xor_i8_f __cmodel_sx_xor_i8
#define sx_xor_i8(a, b, c) { \
  a = __cmodel_sx_xor_i8(b, c); \
}


#endif /* defined(__SX) && !defined(__SX_C_MODEL) */
#endif /* _SX_H_ */
