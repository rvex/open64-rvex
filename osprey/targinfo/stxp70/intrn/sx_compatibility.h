/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */


#ifndef _SX_COMPATIBILITY_H_
#define _SX_COMPATIBILITY_H_

#if defined(__SX) && !defined(__SX_C_MODEL)

/* Macros mapped on builtins for class sx */
#include <builtins_sx_compatibility.h>


/* loopena lw
 * lw loopena( u[0..1]);
 */
#define sx_loopena(a) { \
  __builtin_sx_loopena(a); \
}

/* loopdis lw
 * lw loopdis( u[0..1]);
 */
#define sx_loopdis(a) { \
  __builtin_sx_loopdis(a); \
}

/*  mover2sfr sfr, gpr
 * sfr mover2sfr( u[0..31], gpr);
 */
#define sx_mover2sfr(a, b) { \
  __builtin_sx_mover2sfr(a, b); \
}

/*  movesfr2r gpr, sfr
 * gpr movesfr2r( u[0..31]);
 */
#define sx_movesfr2r(a, b) { \
  a = __builtin_sx_movesfr2r(b); \
}

#else /* !defined(__SX) || defined(__SX_C_MODEL) */

/* Macros mapped on C models for class sx */

/* loopdis lw
 * lw loopdis( u[0..1]);
 */
#define sx_loopdis_f __cmodel_sx_loopdis
#define sx_loopdis(a) { \
  __cmodel_sx_loopdis(a); \
}

/* loopena lw
 * lw loopena( u[0..1]);
 */
#define sx_loopena_f __cmodel_sx_loopena
#define sx_loopena(a) { \
  __cmodel_sx_loopena(a); \
}

/*  mover2sfr sfr, gpr
 * sfr mover2sfr( u[0..31], gpr);
 */
#define sx_mover2sfr(a, b) { \
  __cmodel_sx_mover2sfr(a, b); \
}

/*  movesfr2r gpr, sfr
 * gpr movesfr2r( u[0..31]);
 */
#define sx_movesfr2r(a, b) { \
  a = __cmodel_sx_movesfr2r(b); \
}

#endif /* defined(__SX) && !defined(__SX_C_MODEL) */
#endif /* _SX_COMPATIBILITY_H_ */
