/*
 *      Copyright 2006, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 * Version   ST/HPC/STS (2006/Nov/23)
 */

#ifdef __X3

#error "builtins_model_x3 must not be used on X3 extension"

#else

#include "builtins_model_common.h"

//*****************************************************
//************** BUILTIN EMPTY *************************
//*****************************************************

#ifndef EXTERNAL_TIMING_BUILTINS_IMPLEMENTATION
void __cmodel_x3_clrcc(void)	 {}	
void __cmodel_x3_startcc(void)	 {}	
void __cmodel_x3_stopcc(void)	 {}	
void __cmodel_x3_clrcc1(void)	 {} 
void __cmodel_x3_startcc1(void) {}  
void __cmodel_x3_stopcc1(void)	 {}	
void __cmodel_x3_clrcca(void)	 {} 
void __cmodel_x3_startcca(void) {} 
void __cmodel_x3_stopcca(void)	 {}	
#endif	
						 
void __cmodel_x3_terminate(void){}  	
void __cmodel_x3_ptouch(int x)  {}	
void __cmodel_x3_pinval(int x)  {}	
void __cmodel_x3_pinvalr(int x) {}	
void __cmodel_x3_plock(int x)	 {} 	
void __cmodel_x3_plockr(int x)	 {}	
void __cmodel_x3_pflushw(void)  {}	
void __cmodel_x3_dflush(int x)  {}	
void __cmodel_x3_dflushw(void)	 {}	
void __cmodel_x3_dtouch(int x)  {}	
void __cmodel_x3_dlock(int x )	 {}	
void __cmodel_x3_dinval(int x)  {}	
void __cmodel_x3_pstata(int x)  {}	
void __cmodel_x3_pstatwi(int x) {}	
void __cmodel_x3_dstata(int x)	 {}	
void __cmodel_x3_dstatwi(int x) {}	

void __cmodel_x3_cancelg_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_cancelg_r_g(int x)	{}
void __cmodel_x3_cancell_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_cancell_r_g(int x)	{}
void __cmodel_x3_clric(unsigned x, int y)	{}
int  __cmodel_x3_clrie(void)		{}
void __cmodel_x3_fork_g(int x, int y, unsigned short z)	{}
void __cmodel_x3_fork_i3_g(unsigned short w, int x, int y, unsigned short z)	{}
int  __cmodel_x3_movee2r(unsigned x)	{}
int  __cmodel_x3_moveie2r(void)	{}
int  __cmodel_x3_moveic2r(unsigned x)	{}
int  __cmodel_x3_moveic2ri(int x)		{}
void __cmodel_x3_mover2e(unsigned x, int y)	{}
void __cmodel_x3_mover2ic(unsigned x, int y)	{}
void __cmodel_x3_mover2ici(int x ,int y)	{}
void __cmodel_x3_mover2ie(int x)	{}
void __cmodel_x3_notifyg_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_notifyg_r_g(int x)	{}
void __cmodel_x3_notifyl_i8_i2_i3_g(unsigned short x, unsigned short y, unsigned short z)	{}
void __cmodel_x3_notifyl_r_i3_g(unsigned short x, int y)	{}
void __cmodel_x3_notifyla_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_notifyla_r_g(int x)	{}
void __cmodel_x3_setic(unsigned x, int y)	{}
void __cmodel_x3_setie(int x)			{}
void __cmodel_x3_terminate_i3(unsigned short x)	{}
void __cmodel_x3_wait(int x, int y)		{}
void __cmodel_x3_waitg_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_waitg_r_g(int x)		{}
void __cmodel_x3_waitl_i8_i2_g(unsigned short x, unsigned short y)	{}
void __cmodel_x3_waitl_r_g(int x)		{}
void __cmodel_x3_waitnb(int x, int y)	{}
void __cmodel_x3_waitnbg_i8_i2_g(unsigned short x, unsigned short y){}
void __cmodel_x3_waitnbg_r_g(int x)		{}
void __cmodel_x3_waitnbl_i8_i2_g(unsigned short x, unsigned short y){}
void __cmodel_x3_waitnbl_r_g(int x)		{}

//*****************************************************
//************** BUILTIN      *************************
//*****************************************************


/* MULTIPLICATION */

int __cmodel_x3_mp(int s1, int s2)
{
	int t0 = s1*s2 ;
    return t0;	 
}

int __cmodel_x3_mpn(int s1, int s2)
{
	int t0 = -(s1*s2) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_x3_mpssll(int s1, int s2)
{
    int t0 = EXT32(s1,16);
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_x3_mpsslh(int s1, int s2)
{
    int t0 = EXT32(s1,16);
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_x3_mpsshl(int s1, int s2)
{
    int t0 = s1>>16;
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_x3_mpsshh(int s1, int s2)
{
    int t0 = s1>>16;
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return t2;	 
}
//------------------------------------------------------

int __cmodel_x3_mpuull(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    unsigned int t1 = EXTU32(s2,16);
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpuulh(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    unsigned int t1 = s2>>16;
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpuuhl(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = s1>>16;
    unsigned int t1 = EXTU32(s2,16);
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpuuhh(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = s1>>16;
    unsigned int t1 = s2>>16;
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
//------------------------------------------------------
int __cmodel_x3_mpusll(unsigned int s1, int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpuslh(unsigned int s1, int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpushl(unsigned int s1, int s2)
{
    unsigned int t0 = s1>>16;
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpushh(unsigned int s1, int s2)
{
    unsigned int t0 = s1>>16;
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}

//------------------------------------------------------
int __cmodel_x3_mpsull(int s1, unsigned int s2)
{
    int t0 = EXT32(s1,16);
    unsigned int t1 = EXTU32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpsulh(int s1, unsigned int s2)
{
    int t0 = EXT32(s1,16);
    unsigned int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpsuhl(int s1, unsigned int s2)
{
    int t0 = s1>>16;
    unsigned int t1 = EXTU32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_x3_mpsuhh(int s1, unsigned int s2)
{
    int t0 = s1>>16;
    unsigned int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
//------------------------------------------------------

int __cmodel_x3_mpnsshh(int s1, int s2)
{
	int t0 = -(__cmodel_x3_mpsshh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnsshl(int s1, int s2)
{
	int t0 = -(__cmodel_x3_mpsshl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnsslh(int s1, int s2)
{
	int t0 = -(__cmodel_x3_mpsslh( s1,  s2)) ;
    return t0;	 
}						  

int __cmodel_x3_mpnssll(int s1, int s2)
{
	int t0 = -(__cmodel_x3_mpssll( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_x3_mpnsuhh(int s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpsuhh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnsuhl(int s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpsuhl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnsulh(int s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpsulh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnsull(int s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpsull( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_x3_mpnushh(unsigned s1, int s2)
{
	int t0 = -(__cmodel_x3_mpushh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnushl(unsigned s1, int s2)
{
	int t0 = -(__cmodel_x3_mpushl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnuslh(unsigned s1, int s2)
{
	int t0 = -(__cmodel_x3_mpuslh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_x3_mpnusll(unsigned s1, int s2)
{
	int t0 = -(__cmodel_x3_mpusll( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_x3_mpnuuhh(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpuuhh( s1,  s2)) ;
    return t0;	 
}
int __cmodel_x3_mpnuuhl(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpuuhl( s1,  s2)) ;
    return t0;	 
}
int __cmodel_x3_mpnuulh(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpuulh( s1,  s2)) ;
    return t0;	 
}
int __cmodel_x3_mpnuull(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_x3_mpuull( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------


#endif	/* __X3	*/
