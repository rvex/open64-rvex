
//============================================================================
//
//         Copyright 2000-2006, STMicroelectronics, Incorporated.
//                          All rights reserved.
//         STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
//
//  This software is supplied under the terms of a license agreement or
//  nondis closure agreement with STMicroelectronics and may not be copied or
//  disclosed except in accordance with the terms of that agreement.
//
//----------------------------------------------------------------------------
//
//  System           : STxP70 Toolset
//  Project Component: STxP70 Reconfiguration Toolkit
//  File Name        : builtins_model_vx.c
//  Purpose          : C Models
//
//----------------------------------------------------------------------------
//
//  Manually generated on Tue Nov 28 14:19:32 CET 2006
//
//============================================================================

#if !defined( __Vx ) || defined( __Vx_C_MODEL )

/*==================================================================*
 *
 * Real C models
 *
 *==================================================================*/

#include "Vx/model_vx.c"

#endif  // __Vx

//============================================================================
//
// END
//
//============================================================================
