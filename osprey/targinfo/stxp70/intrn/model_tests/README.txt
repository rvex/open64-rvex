To run the validation, go into 'testing' directory and in order launch:

--> To generate data files for all builtins before testing
make data

--> To test sx core builtins  stxp70v3 by default
make gensrc	          SILICON_TARGET=[stxp70v3|stxp70v4]
make [-k] reference	  SILICON_TARGET=[stxp70v3|stxp70v4]	 > ref.log
make [-k] cbuilt	  SILICON_TARGET=[stxp70v3|stxp70v4]	 > model.log
make check	          SILICON_TARGET=[stxp70v3|stxp70v4]

--> To test x3 builtins
make gensrc         EXTENSION=x3   SILICON_TARGET=[stxp70v3|stxp70v4]   		
make [-k] reference EXTENSION=x3   SILICON_TARGET=[stxp70v3|stxp70v4]	> refx3.log
make [-k] cbuilt    EXTENSION=x3   SILICON_TARGET=[stxp70v3|stxp70v4]	> modelx3.log
make check          EXTENSION=x3   SILICON_TARGET=[stxp70v3|stxp70v4]

