/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 * Version ST/HPC/STS (2006/Nov/23)
 */


/* Builtins models for class __FPX */
#ifdef __FPX

#error "builtins_model_fpx must not be used on SX+FPX Core"

#else
#include "builtins_model_common.h"

/*************************************************************************************/
/* A union which permits to transport bit to bit between a float and a 32 bit int.  */
typedef union
{
  float 	   f_value  ;
  unsigned int ui_word	;
} ieee_float_shape_type	;

/********************************************************************/
/* transport bit to bit between a extension register and core register */

int __cmodel_fpx_fmvf2r(float f1)
{
  ieee_float_shape_type t1;	   	
  t1.f_value = (float) f1;			   	
  return (int) t1.ui_word ;	 
}

float __cmodel_fpx_fmvr2f(int s1)
{
  ieee_float_shape_type t1;	   
  t1.ui_word = (int) s1;			   
  return  t1.f_value ;	 
}

void __cmodel_fpx_fmvr2st(int s1){} /*no Floating-point status register in MODEL*/ 
void __cmodel_fpx_fmvr2s(int s1){}  /*no Floating-point status register in MODEL*/
int __cmodel_fpx_fmvs2r(void){}     /*no Floating-point status register in MODEL*/
void  __cmodel_fpx_fclrs(int){}		/*no Floating-point status register in MODEL*/

/********************************************************************/

float __cmodel_fpx_fmvf2f(float f1)
{
	 return f1 ;
}

/********************************************************************/
/* FLOATING CALCULATION div, mul, fabs , ... */

float __cmodel_fpx_fdiv(float f1, float f2)
{ //be careful to the order
	return (float)f2 / (float)f1 ;
}

float __cmodel_fpx_fmul(float f1, float f2)
{ 
	return (float)f2 * (float)f1 ;
}

float __cmodel_fpx_fmuln(float f1, float f2)
{ 
	return - ( (float)f2 * (float)f1 ) ;
}

float __cmodel_fpx_fabs(float f1)
{
  return (float) fabsf((float)f1) ;		
}

float __cmodel_fpx_fneg(float f1)
{
	return (- (float) f1)  ;
}

float __cmodel_fpx_fsqrt(float f1)
{
  return (float) sqrtf((float)f1) ;		
}

float __cmodel_fpx_fadd(float f1 , float f2 )
{
	return (float)f1 + (float)f2 ;
}

float __cmodel_fpx_faddn(float f1, float f2)
{
	float t1 = (float)f1 + (float)f2 ;
	return (float) (-t1)  ;
}

float __cmodel_fpx_faddaa(float f1, float f2)
{
   float t1 = fabsf((float)f1) ;
   float t2 = fabsf((float)f2) ;
   return (float)t1 + (float)t2 ;
}

float __cmodel_fpx_fsub(float f1, float f2)
{
	return (float)f1 - (float)f2 ;
}

float __cmodel_fpx_fasub(float f1, float f2)
{
	float t1 = (float)f1 - (float)f2 ;
    return (float) fabsf((float)t1) ;		
}

float __cmodel_fpx_fmax(float f1, float f2)
{
    return (float) fmaxf((float)f1,(float)f2) ;		
}

float __cmodel_fpx_fmin(float f1, float f2)
{
    return (float) fminf((float)f1,(float)f2) ;		
}

/********************************************************************************/

float __cmodel_fpx_fscalb(float f1, int s2)	
{
    int t1 = (int)EXT32(s2, 10)	;	 // we keep only 10 lower bits
	return f1 * (float)(2^t1)	 ;
}
 
float __cmodel_fpx_fcnst(int s1)
{
    int t1 = (int)EXT32(s1, 9)	;	 // we keep only 9 lower bits
	return (float)(2^t1)	 ;
}


/********************************************************************/
/* FLOATING CONVERTION TO/FROM INTEGER */

int __cmodel_fpx_ff2i_r(float f1)
{ /* round to 0 */
	 float t1 = floorf(f1);
	 int t2 = (int) t1;
	 return t2 ;
}

int __cmodel_fpx_ff2in_r(float f1)
{ /* nearest */
	 int t1 =(int)(f1);
	 return t1 ;
}

int __cmodel_fpx_ff2i_f(float f1)	
{ /* round to 0 */
	 int t1 = __cmodel_fpx_ff2i_r (f1) ;
	 return t1 ;
}

int __cmodel_fpx_ff2in_f(float f1)	   
{ /* nearest */
	 int t1 = __cmodel_fpx_ff2in_r (f1) ;
	 return t1 ;
}

unsigned int __cmodel_fpx_ff2u_r(float f1) 
{ /* round to 0 */
	 float t1 = floorf(f1);
	 unsigned int t2 = (unsigned int) t1;
	 return t2 ;
}

unsigned int __cmodel_fpx_ff2un_r(float f1) 
{ /* nearest */
	 unsigned int t1 = (unsigned int) f1;
	 return t1 ;
}

unsigned int __cmodel_fpx_ff2u_f(float f1)	
{ /* round to 0 */
	 unsigned int t1 = __cmodel_fpx_ff2u_r (f1) ;
	 return t1 ;
}

unsigned int __cmodel_fpx_ff2un_f(float f1)
{ /* nearest */
	 unsigned int t1 = __cmodel_fpx_ff2un_r (f1) ;
	 return t1 ;
}

float __cmodel_fpx_fi2f_r(int s1)
{
	 float t1 = (float) (s1) ;
	 return t1 ;
}

float __cmodel_fpx_fi2f_f(int f1)
{
	 float t1 =__cmodel_fpx_fi2f_r(f1);
	 return t1 ;
}

float __cmodel_fpx_fu2f_r(unsigned int s1)
{
	 float t1 = (float) (s1) ;
	 return t1 ;
}

float __cmodel_fpx_fu2f_f(unsigned int f1)
{
	 float t1 =__cmodel_fpx_fu2f_r(f1);
	 return t1 ;
}
/***************************************************************/
/* FLOATING COMPARISON */

int __cmodel_fpx_fcmpeq(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1==f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpeqn(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1==f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpge(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1>=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpgen(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1>=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpgt(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1>f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpgtn(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1>f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmple(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1<=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmplen(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1<=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmplt(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1<f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpltn(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1<f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpne(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if (f1!=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpnen(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if (f1!=f2) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpun(float f1, float f2)
{
	 int result_true  = 1 ;
	 int result_false = 0 ;
	 if ( isnan((float)f1)  || isnan((float)f2) ) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpunn(float f1, float f2)
{
	 int result_true  = 0 ;
	 int result_false = 1 ;
	 if ( isnan((float)f1)  || isnan((float)f2) ) { return result_true ; } else { return result_false ; }
}

int __cmodel_fpx_fcmpueq(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpeq( f1, f2) ;
}

int __cmodel_fpx_fcmpueqn(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpeqn( f1, f2) ;
}

int __cmodel_fpx_fcmpuge(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpge( f1, f2) ;
}

int __cmodel_fpx_fcmpugen(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpgen( f1, f2) ;
}

int __cmodel_fpx_fcmpugt(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpgt( f1, f2) ;
}

int __cmodel_fpx_fcmpugtn(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpgtn( f1, f2) ;
}

int __cmodel_fpx_fcmpule(float f1, float f2)
{
	 return  __cmodel_fpx_fcmple( f1, f2) ;
}

int __cmodel_fpx_fcmpulen(float f1, float f2)
{
	 return  __cmodel_fpx_fcmplen( f1, f2) ;
}

int __cmodel_fpx_fcmpult(float f1, float f2)
{
	 return  __cmodel_fpx_fcmplt( f1, f2) ;
}

int __cmodel_fpx_fcmpultn(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpltn( f1, f2) ;
}

int __cmodel_fpx_fcmpune(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpne( f1, f2) ;
}

int __cmodel_fpx_fcmpunen(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpnen( f1, f2) ;
}

int __cmodel_fpx_fcmpuun(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpun( f1, f2) ;
}

int __cmodel_fpx_fcmpuunn(float f1, float f2)
{
	 return  __cmodel_fpx_fcmpunn( f1, f2) ;
}

/************************************************************************/
/* FLOATING MAC  */
float __cmodel_fpx_fmac(float f1, float f2, float f3)
{
	 float t1 = f1 + (  f2 * f3 ) ;
	 return t1 ;
}

float __cmodel_fpx_fmacn(float f1, float f2, float f3)
{
	 float t1 = f1 - (  f2 * f3 ) ;
	 return t1 ;
}




/********************************************************************************/
/* INTEGER DIVISION/MODULO (same as X3) */

int  __cmodel_fpx_div(int s1, int s2)
{
	 return (int) ((int)s1 / (int)s2) ;					 
}

unsigned __cmodel_fpx_divu(unsigned s1, unsigned s2)
{
	 return (unsigned)((unsigned)s1 / (unsigned)s2) ;		 
}

int __cmodel_fpx_mod(int s1, int s2)
{
	 return (int) ((int)s1 % (int)s2) ;					 
}

unsigned __cmodel_fpx_modu(unsigned s1, unsigned s2)
{
	 return (unsigned)((unsigned)s1 % (unsigned)s2) ;		 
}

/********************************************************************************/
/* INTEGER MULTIPLICATION (same as X3)*/

int __cmodel_fpx_mp(int s1, int s2)
{
	int t0 = s1*s2 ;
    return t0;	 
}

int __cmodel_fpx_mpn(int s1, int s2)
{
	int t0 = -(s1*s2) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpssll(int s1, int s2)
{
    int t0 = EXT32(s1,16);
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_fpx_mpsslh(int s1, int s2)
{
    int t0 = EXT32(s1,16);
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_fpx_mpsshl(int s1, int s2)
{
    int t0 = s1>>16;
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return t2;	 
}
int __cmodel_fpx_mpsshh(int s1, int s2)
{
    int t0 = s1>>16;
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return t2;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpuull(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    unsigned int t1 = EXTU32(s2,16);
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpuulh(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    unsigned int t1 = s2>>16;
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpuuhl(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = s1>>16;
    unsigned int t1 = EXTU32(s2,16);
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpuuhh(unsigned int s1, unsigned int s2)
{
    unsigned int t0 = s1>>16;
    unsigned int t1 = s2>>16;
	unsigned int t2 = t0*t1 ;
    return (int) t2;	 
}
//------------------------------------------------------
int __cmodel_fpx_mpusll(unsigned int s1, int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpuslh(unsigned int s1, int s2)
{
    unsigned int t0 = EXTU32(s1,16);
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpushl(unsigned int s1, int s2)
{
    unsigned int t0 = s1>>16;
    int t1 = EXT32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpushh(unsigned int s1, int s2)
{
    unsigned int t0 = s1>>16;
    int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}

//------------------------------------------------------
int __cmodel_fpx_mpsull(int s1, unsigned int s2)
{
    int t0 = EXT32(s1,16);
    unsigned int t1 = EXTU32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpsulh(int s1, unsigned int s2)
{
    int t0 = EXT32(s1,16);
    unsigned int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpsuhl(int s1, unsigned int s2)
{
    int t0 = s1>>16;
    unsigned int t1 = EXTU32(s2,16);
	int t2 = t0*t1 ;
    return (int) t2;	 
}
int __cmodel_fpx_mpsuhh(int s1, unsigned int s2)
{
    int t0 = s1>>16;
    unsigned int t1 = s2>>16;
	int t2 = t0*t1 ;
    return (int) t2;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpnsshh(int s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpsshh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnsshl(int s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpsshl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnsslh(int s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpsslh( s1,  s2)) ;
    return t0;	 
}						  =

int __cmodel_fpx_mpnssll(int s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpssll( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpnsuhh(int s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpsuhh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnsuhl(int s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpsuhl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnsulh(int s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpsulh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnsull(int s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpsull( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpnushh(unsigned s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpushh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnushl(unsigned s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpushl( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnuslh(unsigned s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpuslh( s1,  s2)) ;
    return t0;	 
}

int __cmodel_fpx_mpnusll(unsigned s1, int s2)
{
	int t0 = -(__cmodel_fpx_mpusll( s1,  s2)) ;
    return t0;	 
}
//------------------------------------------------------

int __cmodel_fpx_mpnuuhh(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpuuhh( s1,  s2)) ;
    return t0;	 
}
int __cmodel_fpx_mpnuuhl(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpuuhl( s1,  s2)) ;
    return t0;	 
}
int __cmodel_fpx_mpnuulh(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpuulh( s1,  s2)) ;
    return t0;	 
}
int __cmodel_fpx_mpnuull(unsigned s1, unsigned s2)
{
	int t0 = -(__cmodel_fpx_mpuull( s1,  s2)) ;
    return t0;	 
}

/********************************************************************************/
/* INTEGER MAC ADD */

int __cmodel_fpx_mac(int s1, int s2, int s3)
{
	 int t1 = s1 + (  s2 * s3 ) ;
	 return t1 ;
}

int __cmodel_fpx_macssll(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpssll ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsslh(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsslh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsshl(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsshl ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsshh(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsshh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macuull(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuull ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macuulh(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuulh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macuuhl(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuuhl ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macuuhh(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuuhh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macusll(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpusll ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macuslh(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpuslh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macushl(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpushl ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macushh(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpushh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}


int __cmodel_fpx_macsull(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsull ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsulh(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsulh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsuhl(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsuhl ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

int __cmodel_fpx_macsuhh(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsuhh ( s2 , s3 ) ;
	 int t2 = s1 + t1
	 return t2 ;
}

/********************************************************************************/
/* INTEGER MAC SUB */

int __cmodel_fpx_macn(int s1, int s2, int s3)
{
	 int t1 = s1 - (  s2 * s3 ) ;
	 return t1 ;
}


int __cmodel_fpx_macnssll(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpssll ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsslh(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsslh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsshl(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsshl ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsshh(int s1, int s2, int s3)
{
	 int t1 = __cmodel_fpx_mpsshh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnuull(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuull ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnuulh(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuulh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnuuhl(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuuhl ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnuuhh(int s1, unsigned s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpuuhh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnusll(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpusll ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnuslh(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpuslh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnushl(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpushl ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnushh(int s1, unsigned s2, int s3)
{
	 int t1 = __cmodel_fpx_mpushh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}


int __cmodel_fpx_macnsull(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsull ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsulh(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsulh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsuhl(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsuhl ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}

int __cmodel_fpx_macnsuhh(int s1, int s2, unsigned s3)
{
	 int t1 = __cmodel_fpx_mpsuhh ( s2 , s3 ) ;
	 int t2 = s1 - t1
	 return t2 ;
}



#endif

