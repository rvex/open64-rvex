
		 ###INPUT_DATA_1_MAKE###  ;	  ###INPUT_DATA_1_IS_NOT_IMMEDIATE###
		 ###INPUT_DATA_2_MAKE###  ;	  ###INPUT_DATA_2_IS_NOT_IMMEDIATE###
		 ###INPUT_DATA_3_MAKE###  ;	  ###INPUT_DATA_3_IS_NOT_IMMEDIATE###
		 ###INPUT_DATA_4_MAKE###  ;	  ###INPUT_DATA_4_IS_NOT_IMMEDIATE###
		 
	     begin_builtin()	;
		 
		 ###BUILTIN_NAME###(
   	       ###OUTPUT_DATA_1
   	       ###OUTPUT_COMMA### ,
		   ###INPUT_DATA_1_IS_NOT_IMMEDIATE###	 
		   ###INPUT_DATA_1_IS_IMMEDIATE###
		 , ###INPUT_DATA_2_IS_NOT_IMMEDIATE###
		 , ###INPUT_DATA_2_IS_IMMEDIATE###
		 , ###INPUT_DATA_3_IS_NOT_IMMEDIATE###
		 , ###INPUT_DATA_3_IS_IMMEDIATE###
		 , ###INPUT_DATA_4_IS_NOT_IMMEDIATE###
		 , ###INPUT_DATA_4_IS_IMMEDIATE###
		 ) ;

		 #ifdef __CHECK_CARRY__ 
		 if ( (getSR() >> 12) & 0x1  ) 
		 {
		 	printf("builtin_model: WARNING CARRY detected \n") ;
		 }
		 #endif

		 ###INPUT_DATA_1_PRINTF### 	; 
		 ###INPUT_DATA_2_PRINTF### 	; 
		 ###INPUT_DATA_3_PRINTF### 	; 
		 ###INPUT_DATA_4_PRINTF### 	; 
		 ###OUTPUT_DATA_1_PRINTF###	; 
		 printf ("\n----------\n")	;

		 index ++ ; 
		 if (index < nb_data_tab) 
		 {
		 	goto loop ;  _IS_NOT_IMMEDIATE###
		 }
		 else
		 {
		 	exit(0) ; 	 _IS_IMMEDIATE###

		 }
