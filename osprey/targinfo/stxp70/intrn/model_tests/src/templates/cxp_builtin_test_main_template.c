/*
 *      Copyright 2005-2008, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */

/*=======================================================================
 * Project component : 	ST - SX builtin model
 * File name : 	 		test_###BUILTIN_NAME###.c
 * Author : 		 	T.TORRET									   
 * Organization : 		ST/STS/CEC
 *-----------------------------------------------------------------------
 * Purpose : 	###BUILTIN_NAME### Immediate Builtin model test file for ###INSTRUCTION_NAME###
 *-----------------------------------------------------------------------
 * HISTORY: Creation by T.TORRET ST/HPC/STS (2005/Apr/19)
 * HISTORY: ------------------------------------------------------------------------
 * HISTORY: v2.0 T.TORRET (2005/May/26)
 * HISTORY:    new StxP70 arch 1_3 toolset : 2 header files  "builtin_core.h","builtin_AX.h"
 * HISTORY: ------------------------------------------------------------------------
 * HISTORY: v3.0 T.TORRET (2005/Oct/17)
 * HISTORY:    New Compiler STCXP70cc => new builtin files
 * HISTORY: ------------------------------------------------------------------------
 * HISTORY: v4.0 T.TORRET (2006/Dec/05)
 * HISTORY:   CORXPERT extension  => new builtin files
 * HISTORY: ------------------------------------------------------------------------
 * HISTORY: v4.1 T.TORRET (2008/Sept/26)
 * HISTORY:   add  INSTRUCTION NAME for build only=> new builtin files
 * HISTORY: ------------------------------------------------------------------------
 * HISTORY: v4.2 T.TORRET (2008/Oct/15)
 * HISTORY:   add  begin_builtin() to find start of builtin in disasm file
 *-----------------------------------------------------------------------
 * Date of builtin test file : ###GENERATION_DATE###
 *=======================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifdef  __###EXTENSION_NAME###_BUILTIN_TEST
#include "###EXTENSION_NAME###.h"
#ifdef __SX
#define __CHECK_CARRY__	  1
#endif
#endif


#ifdef __SX
extern int getSR();
#endif

#define NB_PARAM_IN      ###INPUT_PARAM_NB###
#define NB_PARAM_OUT     ###OUTPUT_PARAM_NB###
#define NB_PARAM_IN_OUT  ###INOUT_PARAM_NB###

void begin_builtin() {};

###INPUT_DATA_1_BLOCK### ###INPUT_DATA_1_IS_NOT_IMMEDIATE###  
###INPUT_DATA_2_BLOCK### ###INPUT_DATA_2_IS_NOT_IMMEDIATE###  
###INPUT_DATA_3_BLOCK### ###INPUT_DATA_3_IS_NOT_IMMEDIATE###  
###INPUT_DATA_4_BLOCK### ###INPUT_DATA_4_IS_NOT_IMMEDIATE###  

###OUTPUT_DATA_1_TYPE###   out1  ;

int nb_data_tab	 =	###INPUT_DATA_TAB_SIZE### ;

main ()
{
	 int EOF_Data= 0  ;
	 int index;

	 printf ( "builtin: ###BUILTIN_NAME###\n")	;
	 printf ( "###INSTRUCTION_NAME###\n")	;
	 printf ( "\t Nb of Input  Parameter: %d\n",NB_PARAM_IN)		;
	 printf ( "\t\t Type of Input  Parameter 1: ###INPUT_DATA_1_TYPE###\n")		;
	 printf ( "\t\t Type of Input  Parameter 2: ###INPUT_DATA_2_TYPE###\n")		;
	 printf ( "\t\t Type of Input  Parameter 3: ###INPUT_DATA_3_TYPE###\n")		;
	 printf ( "\t\t Type of Input  Parameter 4: ###INPUT_DATA_4_TYPE###\n")		;
	 printf ( "\t Nb of Output  Parameter: %d\n",NB_PARAM_OUT)		;
	 printf ( "\t\t Type of Output  Parameter 1: ###OUTPUT_DATA_1_TYPE###\n")		;
	 printf ("----------\n")	;

	 index=0 ;
	 loop :											  
	 ###TAB_BUILTIN###									  

}


