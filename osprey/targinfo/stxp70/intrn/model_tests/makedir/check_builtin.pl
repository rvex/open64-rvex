#!/apa/gnu/SunOS5.7/bin/perl
#=======================================================================
# Check output files of  builtin between SX reference and C models
#=======================================================================
# HISTORY:
#---------
# Creation by T.TORRET (2005/Apr/12)
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2005/Apr/15)
#		add CARRY check
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2006/Nov/22)
#		change builtin name check
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2007/Jan/12)
#		make a result file even when missing SX reference or  C models files
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2008/Sept/26)
#		make a result file  when built only on stxp70
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2008/Oct/15)
#		fix issue (add begin_builtin) when built only (difficult to see where builtins begin in .dis) 
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2008/Nov/12)
#		fix issue (regexp with space \s in CheckDisasm) when built only (new stxp70v4 xxx.dis) 
#--------------------------------------------------------------------------------
# Modif.  by T.TORRET (2008/Nov/24)
#		fix 2nd issue (add begin_builtin()) when built only (due to disasm and global reference omitted) 
#--------------------------------------------------------------------------------

use diagnostics;
use strict;
use DirHandle();

use Cwd; 						 
use File::Basename;

use Text::Diff;
require Text::Diff;

# Hack to avoid to repeat home directory of this script.
my $base_root=dirname($0);


my $DirMain=cwd ;

#-------------------------------------------------------------------------------
# Script identification strings
#-------------------------------------------------------------------------------
my $SName = "check_builtin.pl";
my $SVersion = "3.3";
my $SBuildDate = "Nov '08";
my $ScriptIdentification = "$SName Version $SVersion - $SBuildDate";
my $Platform = "-";



my $Ref_File="" ;
my $Tocheck_File="" ;
my $Result_File="" ;

my $BUILD_ONLY=0 ;
my $fileText="xxxx" ;


#-------------------------------------------------------------------------------
# Usage: describes PERL script parameters.
#-------------------------------------------------------------------------------
# Parameter:
#   None.
#-------------------------------------------------------------------------------
# Result:
#   None.
#-------------------------------------------------------------------------------
# Prerequisites:
#   None.
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub Usage {
	print "\nUsage1: $0 -ref=file1 -tocheck=file2 -out=fileresult\n";
	print "\n   -ref= 	    ( Mandatory )  : SX reference output file with path\n";
	print "\n   -tocheck= 	( Mandatory )  : Cmodel output file to check with path\n";
	print "\n   -out= 	    ( Mandatory )  : diff result file name\n";
	print "\n";   
	print "\n or Usage2: $0 -buildonly -dis=file1 -out=fileresult\n";
	print "\n   -buildonly ( Mandatory in first position)   : we check only SX disasm output \n";
	print "\n   -tocheck= ( Mandatory )   : SX disasm output file with path\n";
	print "\n   -out= 	   ( Mandatory )   : diff result file name\n";
	print "\n";   
}
my $NbArgMin=3	;
my $NbArgMax=$NbArgMin+0	;

#-------------------------------------------------------------------------------
# AnalyzeCmdLine: analyzes command line to retrieve flags.
#-------------------------------------------------------------------------------
# Parameter:
#   reg_ARGV: reference to ARGV global parameter.
#-------------------------------------------------------------------------------
# Result:
#-------------------------------------------------------------------------------
# Prerequisites:
#   None.
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub AnalyzeCmdLine {
	my $ref_ARGV = shift;
	my @OptionList=();
	
	#read all command line option and put it in a list
	while (scalar(@$ref_ARGV)) 
	{
		 my $OneLineOption = shift @$ref_ARGV;
		 push @OptionList,$OneLineOption ;
	} # end while argv

	print "Option list: @OptionList	\n";
	print "Nb Option=".scalar(@OptionList)."\n";


	# process all command  option from list
	while (scalar(@OptionList)) 
	{ 
		my $OneOption = shift @OptionList;
		if ($OneOption =~ s/^-buildonly// )  
		{
			$BUILD_ONLY = 1;
		} 
		elsif ( ($BUILD_ONLY == 0) and ($OneOption =~ s/^-ref=// )  )
		{
			$Ref_File = $OneOption;
		}  
		elsif ($OneOption =~ s/^-tocheck=// )
		{
			$Tocheck_File = $OneOption;
		} 
		elsif ($OneOption =~ s/^-out=// )  
		{
			$Result_File = $OneOption;
		} 
		else 
		{
			Usage();
			print "Unknown command line switch: [$OneOption]\n";
			exit(1);
		}
   
	}

if ($BUILD_ONLY == 0)
{
  $fileText="Cmodel" ;
}
else
{
  $fileText="STXP70 disasm" ;
}


if ( $Result_File eq "" )
	{
		Usage();
		print "You shoud give a result file name\n";
		exit(1);
	}

if ( ($BUILD_ONLY == 0) and ($Ref_File eq "" ) )
	{
		Usage();
		print "You shoud give a  SX Reference file ($Ref_File) \n";
		exit(1);
	}

if ( $Tocheck_File eq "" )
	{
		Usage();
		print "You shoud give a $fileText file to check ($Tocheck_File) \n";
		exit(1);
	}

if ( ($BUILD_ONLY == 0) and (!-f "$Ref_File") )
	{
		Usage();
		print "SX Reference file ($Ref_File) doesn't exist\n";
		
		open (RESULTFILE,">$Result_File")  || die "ERROR WRITE RESULT $Result_File file couldn't be open ";
		print RESULTFILE "--------------------------\n" ;
		print RESULTFILE "*** Name : ($Ref_File) SX Reference file doesn't exist\n" ;
		print RESULTFILE "*** Status FAIL\n" ;
		print RESULTFILE "--------------------------\n" ;
		close(RESULTFILE);

		exit(0);
	}

if (!-f "$Tocheck_File") 
	{
		Usage();
		print "$fileText  file to check ($Tocheck_File) doesn't exist\n";
		
		open (RESULTFILE,">$Result_File")  || die "ERROR WRITE RESULT $Result_File file couldn't be open ";
		print RESULTFILE "--------------------------\n" ;
		print RESULTFILE "*** Name : ($Tocheck_File) $fileText file to check doesn't exist\n" ;
		print RESULTFILE "*** Status FAIL\n" ;
		print RESULTFILE "--------------------------\n" ;
		close(RESULTFILE);
		
		exit(0);
	}

} # End of AnalyzeCmdLine

#-------------------------------------------------------------------------------
# FilterResult: Format Result File
#-------------------------------------------------------------------------------
# Parameter : Result file    (with access path)
#-------------------------------------------------------------------------------
# Result:  formatted String 
#   None.
#-------------------------------------------------------------------------------
# Prerequisites:
#	$ThisFile exists
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub FilterResult
{
	my $ThisFile  	= shift ;

 	my $FilterStr = ""	 ; 
	my $BuiltinName=""   ;
	my $BuiltinHeader="" ;
	my $InstrName="" ;

	if (open(FILEDATA,"<$ThisFile") )  
	{
		my 	$OneLine ;

			while ($OneLine=<FILEDATA>) 
			{
				if ( $OneLine =~ /------/ ) 
				{
					next ;
				}
				elsif ( $OneLine =~ /INSTRUCTION_(\w+)/ ) 
				{
					$InstrName = $1 ;
					# print "FilterResult InstrName=$InstrName \n" ; 
					next ;
				}
				elsif ( $OneLine =~ /CARRY/ ) 
				{
					$FilterStr  .= $OneLine ;
				}
				elsif ( $OneLine =~ /WARNING/ ) 
				{
					next ;
				}
				elsif ( $OneLine =~ /built/ ) 
				{
					$FilterStr  .= $OneLine ;
					chomp($OneLine)  ;
				  	$BuiltinName = $OneLine ;
				}
				elsif ( $OneLine =~ /Parameter/ ) 
				{
					$FilterStr  .= $OneLine ;
				  	$BuiltinHeader .= $OneLine ;
				}
				else
				{
					$FilterStr  .= $OneLine ;
				}
			}
	}
	else
	{
		print "ERROR Cannot OPEN File : $ThisFile\n" ;
		exit (1) ;
	}
	close(FILEDATA);

	if 	( ($BuiltinName ne "") and ($InstrName ne "") )
	{
	 $BuiltinName .= " ".$InstrName ;
	}

 	return ($BuiltinName,$BuiltinHeader,$FilterStr) ;
} #end of FilterResult

#-------------------------------------------------------------------------------
# CheckDisasm: Check Disasm File when BUILD_ONLY
#-------------------------------------------------------------------------------
# Parameter : DISASM Result file with C source inside   (with access path)
#-------------------------------------------------------------------------------
# Result:  formatted String 
#   None.
#-------------------------------------------------------------------------------
# Prerequisites:
#	$ThisFile exists
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub CheckDisasm
{
	my $ThisFile  	= shift ;

	my $RefInstrName=""   ;
	my $BuiltinName = "not a real name for the instant"   ;
	my $BuiltinHeader="" ;
	my $Search=0;
	my $ResultStr="FAILED" ;


	if (open(FILEDATA,"<$ThisFile") )  
	{
		my 	$OneLine ;

		while ($OneLine=<FILEDATA>) 
		{
			if ( $OneLine =~ /INSTRUCTION_(\w+)/ ) 
			{
				$RefInstrName = $1 ;
				# print "CheckDisasm RefInstrName=$RefInstrName \n" ; 
				next ;
			}
			elsif ( ($Search ==0) and ( $OneLine =~ /builtin:\s(\w+)/ )  )
			{
				$BuiltinName = $1;
				# print "CheckDisasm BuiltinName=$BuiltinName \n" ; 
				next ;
			}
			elsif ( ($Search ==0) and ( $OneLine =~ /$BuiltinName\(/ ) 	)
			{
				# print "CheckDisasm Search begin1 \n" ; 
				$Search = 1 ;
				next ;
			}
			elsif ( ($Search ==0) and ( $OneLine =~ /\scallr\s+begin_builtin/i ) 	)
			{
				# print "CheckDisasm Search begin2 \n" ; 
				$Search = 1 ;
				next ;
			}
			elsif ( ($Search ==0) and ( $OneLine =~ /^\s+begin_builtin\(\)/ ) 	)
			{
				# print "CheckDisasm Search begin3 \n" ; 
				$Search = 1 ;
				next ;
			}
			elsif ( ($Search ==1) and ( $OneLine =~ /\s$RefInstrName\s/i ) 	)
			{
				$ResultStr="SUCCESSFULL" ;
				last;
			}
			elsif ( $OneLine =~ /getSR/ ) 
			{
				# print "CheckDisasm Search end \n" ; 
				last;
			}
		}
	}
	else
	{
		print "ERROR Cannot OPEN File : $ThisFile\n" ;
		exit (1) ;
	}
	close(FILEDATA);

	$BuiltinName  = "builtin: ".$BuiltinName." ".$RefInstrName." BUILT_ONLY";
    
 	return ($BuiltinName,$BuiltinHeader,$ResultStr) ;
} #end of CheckDisasm

#-------------------------------------------------------------------------------
# Main program
#-------------------------------------------------------------------------------
# Parameter:
#-------------------------------------------------------------------------------
# Result:
#-------------------------------------------------------------------------------
# Prerequisites:
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------

# Set umask for accessibility of created files by group
umask 0007;

# Ensure correct parameter number.
if ( (scalar(@ARGV) < $NbArgMin) or 	(scalar(@ARGV) > $NbArgMax)  )
{
	Usage();
	exit(1);
}

# Other command line parameters?
if (scalar(@ARGV) >= 1) 
{
	AnalyzeCmdLine(\@ARGV);
}

my $BuiltinNameRef		 = "";
my $BuiltinHeaderRef	 = "";
my $BuiltinNameTocheck	 = "";
my $BuiltinHeaderTocheck = "";
my $RefString     = "";
my $ToCheckString = "";
my $diff= "not_checked_yet";

if ($BUILD_ONLY == 0)
{

	($BuiltinNameRef,    $BuiltinHeaderRef,     $RefString)     = FilterResult($Ref_File);
	($BuiltinNameTocheck,$BuiltinHeaderTocheck, $ToCheckString) = FilterResult($Tocheck_File);

 	$diff = diff \$RefString, \$ToCheckString ;
}
else
{
	($BuiltinNameTocheck, $BuiltinHeaderTocheck,  $ToCheckString) = CheckDisasm($Tocheck_File);
	if ($ToCheckString eq "SUCCESSFULL")	{ $diff="";}
}


open (RESULTFILE,">$Result_File")  || die "ERROR WRITE RESULT $Result_File file couldn't be open ";

print RESULTFILE "--------------------------\n" ;
if 	($BuiltinNameRef ne "")
{
print RESULTFILE "*** Name $BuiltinNameRef\n" ;
print RESULTFILE "*** Header \n$BuiltinHeaderRef\n" ;
}
else
{
print RESULTFILE "*** Name $BuiltinNameTocheck\n" ;
print RESULTFILE "*** Header \n$BuiltinHeaderTocheck\n" ;
}

if 	($diff eq "")
{
	print RESULTFILE "*** Status SUCCESS\n" ;
}
else
{
	print RESULTFILE "*** Status FAIL\n" ;
}
print RESULTFILE "--------------------------\n" ;
print RESULTFILE "1st(ref) versus 2nd (model) \n" ;
print RESULTFILE "*** Differences:\n" ;
print RESULTFILE $diff ;
print RESULTFILE "--------------------------\n" ;

close(RESULTFILE);
