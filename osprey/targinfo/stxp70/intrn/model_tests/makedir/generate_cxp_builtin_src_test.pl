#!/apa/gnu/SunOS5.7/bin/perl
#=======================================================================
# Generate  Src files compatible with Corxpert builtin test
#=======================================================================
# HISTORY:
#---------
# Creation by T.TORRET (2006/Jan/07)
#---------
# Modif. by T.TORRET (2008/Sept/27)
# add  BUILD_ONLY/INSTRUCTION_NAME/InstName/---force/---addname
#--------------------------------------------------------------------------------

use diagnostics;
use strict;
use DirHandle();

use Cwd; 
use File::Basename;

# Hack to avoid to repeat home directory of this script.
my $base_root=dirname($0);
require "$base_root/DateUs.pm";

my $DirMain=cwd ;

my $Date = DateUs->now();

#-------------------------------------------------------------------------------
# Script identification strings
#-------------------------------------------------------------------------------
my $SName = "generate_cxp_builtin_src_test.pl";
my $SVersion = "2.0";
my $SBuildDate = "Sept '08";
my $ScriptIdentification = "$SName Version $SVersion - $SBuildDate";
my $Platform = "-";

my $SrcDir="src" ;
my $SrcTemplatesDir="templates" ;
my $SrcVobDir="$base_root/../$SrcDir" ;

my $SrcGenerateDir  = "$DirMain/$SrcDir"  ;
my $SrcTemplate     = "$SrcVobDir/$SrcTemplatesDir/cxp_builtin_test_main_template.c"	;
my $SrcBuiltinTemplate  = "$SrcVobDir/$SrcTemplatesDir/cxp_basic_builtin.c"	;
my $SrcBuiltinFile  = "$DirMain/$SrcDir/main_###BUILTIN_NAME###.c" ;

my $DataDir="$DirMain/data_in" ;
my $DataFileExt=".in"		   ;
my $DataFileNb 	= 1 ;
my $NUMBER_OF_DATA_FILE	= 4 ;

my $BuiltinListFile="$SrcVobDir/$SrcTemplatesDir/builtin" ;

my $Extension="";
my $ExtensionType="";

my $NB_MAX_IN	= 4 ;	# number max of input  parameter for builtin
my $NB_MAX_OUT	= 1 ;	# number max of output parameter for builtin

my $System = "Unix";
my $Copy   = "cp";
#-------------------------------------------------------------------------------

my $VobRoot="/vob";



#-------------------------------------------------------------------------------
# Global patterns used in C templates
#-------------------------------------------------------------------------------
my $EXTENSION_NAME			= "###EXTENSION_NAME###"	  	;
my $BUILTIN_NAME			= "###BUILTIN_NAME###"			;
my $INSTRUCTION_NAME		= "###INSTRUCTION_NAME###"	  	;
my $GENERATION_DATE 		= "###GENERATION_DATE###"		;
my $INPUT_DATA_TAB_SIZE    	= "###INPUT_DATA_TAB_SIZE###" 	;
my $INPUT_PARAM_NB 			= "###INPUT_PARAM_NB###"		;
my $OUTPUT_PARAM_NB 		= "###OUTPUT_PARAM_NB###"		;
my $INOUT_PARAM_NB 			= "###INOUT_PARAM_NB###"		;
 
my $INPUT_DATA  	= 	"###INPUT_DATA_"   	;	
my $OUTPUT_DATA 	= 	"###OUTPUT_DATA_"   ;	
my $OUTPUT_COMMA 	= 	"###OUTPUT_COMMA###";	
my $DATATYPE 		= 	"_TYPE###"			;
my $DATALIST 		= 	"_LIST###"  		;
my $DATABLOCK 		= 	"_BLOCK###"  		;
my $DATAMAKE 		= 	"_MAKE###"  		;
my $DATAPRINTF		=	"_PRINTF###"		;
my $IS_NOT_IMMEDIATE=	"_IS_NOT_IMMEDIATE###" 	; 
my $IS_IMMEDIATE	=	"_IS_IMMEDIATE###" 		; 
my $TAB_BUILTIN     =   "###TAB_BUILTIN###"  	;

 
my $VOID = "void"	;
 
my $INT_HEX_PRINTF_FORMAT		= "0x"		;
my $LONG_HEX_PRINTF_FORMAT		= "0lx"		;
my $LONGLONG_HEX_PRINTF_FORMAT	= "0llx"		;  # C librarie currently doesn't support printf 64 bits


my $InOutParamNb = 0   	;
my @InOutParamList 	= () ; # list of parameter number  that are twice
my @InParamList  	= () ;
my @OutParamList 	= () ;
my @InDataFile	 	= () ;
my @ImmedParamNb  	= () ;	# list of  parameter number that are immediate
my @InParamMaskList	= () ;  # list of mask (one for each input parameter)
my @InParamForceList = () ;  # list of force (one for each input parameter)

# Extension Data Type Structure
my @TypeStruct = (
	{ 
	 Name 		=> 'int' , 
  	 Make 		=> ''	,
	 Size 		=> 32	,
	 NbSubSize 	=> 1	,
	 SubSize 	=> 0	,
	 SubSizeType => ''	,
  	 fprintf 	=> 'fprintf(stdout,"0x%08X",###TOPRINT_1###)'	,
	},
	{ 
	 Name 		=> 'long' , 
  	 Make 		=> ''	,
	 Size 		=> 32	,
	 NbSubSize 	=> 1	,
	 SubSize 	=> 0	,
	 SubSizeType => ''	,
  	 fprintf 	=> 'fprintf(stdout,"0x%08X",###TOPRINT_1###)'	,
	},
	{ 
	 Name 		=> 'long long' , 
  	 Make 		=> ''	,
	 Size 		=> 64	,
	 NbSubSize 	=> 2	,
	 SubSize 	=> 32	,
	 SubSizeType => 'unsigned int'	,
  	 fprintf 	=> 'fprintf(stdout,"0x%08X%08X",###TOPRINT_1###,###TOPRINT_2###)'	,
	},
	{ 
	 Name 		=> 'short' , 
  	 Make 		=> ''	,
	 Size 		=> 16	,
	 NbSubSize 	=> 1	,
	 SubSize 	=> 0	,
	 SubSizeType => ''	,
  	 fprintf 	=> 'fprintf(stdout,"0x%04X",###TOPRINT_1###)'	,
	},
	{ 
	 Name 		=> 'char' , 
  	 Make 		=> ''	,
	 Size 		=> 8	,
	 NbSubSize 	=> 1	,
	 SubSize 	=> 0	,
	 SubSizeType => ''	,
  	 fprintf 	=> 'fprintf(stdout,"0x%02X",###TOPRINT_1###)'	,
	}

) ;

my $DataTypeIndex= scalar(@TypeStruct) - 1 ; 

#=============================================================================
# How to use this script
#
my $NbArgMin=1	;
my $NbArgMax=$NbArgMin+0	;

sub Usage {
	print "Usage: $0  -ext=EXTENSION\n";
	print "		EXTENSION	: aX2 or ...\n";
} # end of Usage

#=============================================================================
# Command line analyzer
#
sub AnalyzeCmdLine 
{
	my $ref_ARGV = shift;

	while (scalar(@$ref_ARGV)) 
	{
		my $OneOption = shift @$ref_ARGV;
		if ($OneOption =~ s/^-ext=//) 
		{
			$Extension     = $OneOption ;
			$BuiltinListFile.="_".$Extension.".lst"	;
		} else 
		{
			print "Unknown command line switch: [$OneOption]\n";
			exit(1);
		}
	}
	if ($Extension eq "")
	{
		Usage();
		exit(1);
	}
}  # END of	 AnalyzeCmdLine

#=============================================================================
# IsAnInOut 
#-------------------------------------------------------------------------------
# Parameter :
#  parameter number to check
#-------------------------------------------------------------------------------
# Result:
#   Is this parameter used as input and output :  yes (1) or no (0).
#-------------------------------------------------------------------------------
# Prerequisites:
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub IsAnInOut 
{
	my $Entry = shift;

	my $IsInOut = 0 ;
	my $Index = scalar(@InOutParamList) ;

	while ( ($IsInOut != 1  ) and ( $Index != 0 )    )
	{
	 	if ( $Entry == $InOutParamList[$Index-1])
	 	{
			$IsInOut = 1 ;
	 	}
		$Index -- ;
	}

   return $IsInOut	;

}  # END of	 IsAnInOut

#=============================================================================
# IsAnImmediate 
#-------------------------------------------------------------------------------
# Parameter :
#  parameter number to check
#-------------------------------------------------------------------------------
# Result:
#   Is an Immediate parameter yes (1) or no (0).
#-------------------------------------------------------------------------------
# Prerequisites:
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub IsAnImmediate 
{
	my $Entry = shift;

	my $IsImmediate = 0 ;
	my $Index = scalar(@ImmedParamNb) ;

	while ( ($IsImmediate != 1  ) and ( $Index != 0 )    )
	{
	 	if ( $Entry == $ImmedParamNb[$Index-1])
	 	{
			$IsImmediate = 1 ;
	 	}
		$Index -- ;
	}

   return $IsImmediate	;

}  # END of	 IsAnImmediate


#-------------------------------------------------------------------------------
# GetDataTypeIndex: Get index for all values of Type .
#-------------------------------------------------------------------------------
# Parameter : 
# 	- Type  of variables/data
#-------------------------------------------------------------------------------
# Result:
#   -  index in tab of struct of Type .
#-------------------------------------------------------------------------------
# Prerequisites:
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub GetDataTypeIndex
{
	 my $Type = shift  ;

	 my $Name ="" ;
	 my $Size = 0 ;

	 if   ( $Type =~ /char/ )
	 {
	 	$Name = "char" ;
	 } 
	 elsif   ( $Type =~ /short/ )
	 {
	 	$Name = "short" ;
	 } 
	 elsif   ( $Type =~ /long.*long/ )
	 {
	 	$Name = "long long" ;
	 } 
	 elsif ( $Type =~ /long/ )
	 {
	 	$Name = "long" ;
	 }
	 elsif   ( $Type =~ /int/ )
	 {
	 	$Name = "int" ;
	 } 
	 else
	 {
	 	$Name =	$Type ;
	 }

	my $Index ;

	for ($Index=0; $Index<scalar(@TypeStruct); $Index++) {
		if 	($TypeStruct[$Index]{Name} eq $Name) {
			return ( $Index	)
		}
	}
	return -1 ;

} # end  GetDataTypeIndex



#-------------------------------------------------------------------------------
# GetDataStr: Read Data File
#-------------------------------------------------------------------------------
#   - Type size in bits: 8 ,16, 32  
#-------------------------------------------------------------------------------
# Result: Data Str	 
#   	  Number of Data In file
#-------------------------------------------------------------------------------
# Prerequisites:
#	$DataDir  exists
#	$DataFile exists
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub GetDataStr
{
	my $TypeSize 	= shift ;
	
	my $DataFile  	= "builtin_" ;
	
	$DataFile  	.= $TypeSize."b_".$DataFileNb.$DataFileExt  ;

	my $DataFileLoc  	= "$DataDir/$DataFile" ;

	my $NbData = 0 ;
 	my $DataStr = ""	; 

	if (open(FILEDATA,"<$DataFileLoc") )  
	{
		my 	$OneLine ;

			while ($OneLine=<FILEDATA>) 
			{
				$DataStr .=  $OneLine ;
				$NbData ++ ;
			}
	}
	else
	{
		print "ERROR Cannot OPEN Data File : $DataFileLoc\n" ;
		exit (1) ;
	}
	close(FILEDATA);

	$DataFileNb ++ ;
	if ($DataFileNb > $NUMBER_OF_DATA_FILE)
	{
		$DataFileNb	= 1 ;
	}

 	return ($NbData,$DataStr) ;
} #end of GetDataStr


#-------------------------------------------------------------------------------
# GetOneData: Read One Data in One Data File
#-------------------------------------------------------------------------------
# Parameter : 
#	- Parameter number
#   - Type size in bits: 8 ,16, 32  
#	- Index of data in Data File   ;
#-------------------------------------------------------------------------------
# Result: One Data Str	 (string is null when file is all done)
#   	  Number of Data In file
#-------------------------------------------------------------------------------
# Prerequisites:
#	$DataDir  exists
#	$DataFile exists
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub GetOneData
{
	my $Param		= shift ;
	my $TypeSize 	= shift ;
	my $NbToFind 	= shift ;
	
	my $DataFile  	= "builtin_" ;
	# to be sure to have the same file for input and printf	we use the parameter number
	# we calculate a special number in  [$NUMBER_OF_DATA_FILE , ..., 1]
	my $OneDataFileNb =	 $NUMBER_OF_DATA_FILE + 1 -	$Param  ;  
	
	$DataFile  	.= $TypeSize."b_".$OneDataFileNb.$DataFileExt  ;

	my $DataFileLoc  	= "$DataDir/$DataFile" ;

	my $NbData = 0 ;
 	my $DataStr = ""	; 

	if (open(FILEDATA,"<$DataFileLoc") )  
	{
		my 	$OneLine ;

			while ($OneLine=<FILEDATA>) 
			{
				chomp  $OneLine ;
				if ($NbToFind == $NbData )
				{
					$OneLine =~ s/\,// ;
					$DataStr =  $OneLine ;
				}
				$NbData ++ ;
			}
	}
	else
	{
		print "ERROR Cannot OPEN Data File : $DataFileLoc\n" ;
		exit (1) ;
	}
	close(FILEDATA);


 	return ($NbData,$DataStr) ;
} #end of GetOneData




#-------------------------------------------------------------------------------
# GenerateSrc: generates builtin Src file .
#-------------------------------------------------------------------------------
# Parameter :
#	  BuiltinName  	
#	  BuiltImmediate : is there any builtin parameter immediate
#	  Instruction Name  	
#	  AddName : name to be add to file because we can have several files for same builtin 	
#	  BUILD_ONLY : 1 if we cannot simulate and modelize	/ else 0
#-------------------------------------------------------------------------------
# Result:
#   None.
#-------------------------------------------------------------------------------
# Prerequisites:
#   $SrcTemplatesDir shall exist.
#   $SrcGenerateDir  shall exist.
#   $SrcTemplate     shall exist.
#   $SrcBuiltinTemplate     shall exist.
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------
sub GenerateSrc 
{
	my $BuiltinName  	= shift ;
	my $BuiltImmediate  = shift ;
	my $InstName  		= shift ;
	my $AddName  		= shift ;
	my $BUILD_ONLY      = shift ;
	
	my @DataSize  		= () ;

  	my $temp = $BuiltinName.$AddName ;

	my $OneSrcBuiltinFile = $SrcBuiltinFile ;
  #	if ($BUILD_ONLY == 1)
  #	{
  #		$temp = $temp."_BUILD_ONLY" ;
  #	}
  	$OneSrcBuiltinFile =~  s/$BUILTIN_NAME/$temp/  ;


	my $GenDate = $Date->formatLong();

	my $NbInParam  = scalar(@InParamList )  ;
	my $NbOutParam = scalar(@OutParamList ) ;
	my $NbInOutParam  = scalar(@InOutParamList )  ;
	my $NbInDataFile  = scalar(@InDataFile )  ;
	my $StrBuiltin    = "" ;

	if ( $NbInParam  > $NB_MAX_IN  )
	{
		print $BuiltinName." Too much Input Parameters:$NbInParam , MAX is $NB_MAX_IN \n" ;
		exit (1) ;
	}
	if ( $NbOutParam  > $NB_MAX_OUT )
	{
		print $BuiltinName." Too much Output Parameters:$NbOutParam , MAX is $NB_MAX_OUT \n" ;
		exit (1) ;
	}

	if ( ( $NbOutParam ==  1 ) && ($OutParamList[0] =~ /void/) )
   {
      $NbOutParam = 0;
   }
   
	# ----------------- CONSTRUCT the BUILTIN  BLOCK -----------
	my   $LastData = 0  ;
	my @DataIndex   = (0,0,0,0) ; # index of data in input file	 for each input
	do
	{
		if (open(SRCTEMPLATE,"<$SrcBuiltinTemplate")) 
		{
			my 	$OneLine ;
			while ($OneLine=<SRCTEMPLATE>) 
			{

				$OneLine =~ s/$BUILTIN_NAME/$BuiltinName/	;
				$OneLine =~ s/$INSTRUCTION_NAME/$InstName/	;

				
				if ( $OneLine =~ /$INPUT_DATA(\d)/ ) 
				{
				 	my $nb = $1 ; 
					if ( $nb >	$NbInParam )
					{
						$OneLine = ""	;
					}
					elsif ( ($BuiltImmediate) and IsAnImmediate($nb) )
					{ # immediate parameter
						if ($OneLine =~ /$INPUT_DATA$nb$IS_NOT_IMMEDIATE/) 
						{
							$OneLine = ""	;
						}
						else 
						#($OneLine =~ /$INPUT_DATA$nb$IS_IMMEDIATE/) 
						{
							my $InDataStr = "";
							my $DataNbInFile = 0 ;
							my $TypeIndex = -1 ;
							my $TypeSize = 0 ;

							$TypeIndex= GetDataTypeIndex($InParamList[$nb-1]) ;
							if ($TypeIndex <0 ) 
							{
								print $BuiltinName."TYPE:".$InParamList[$nb-1]." NOT FOUND \n" ;
								exit (1) ;
							}

							($DataNbInFile,$InDataStr)  = GetOneData($nb,$TypeStruct[$TypeIndex]{Size},$DataIndex[$nb-1] ) ;
							
							if ($InParamForceList[$nb-1] ne "")
							{ # we introduce the mask
								$InDataStr = $InParamForceList[$nb-1] ;
							}
							elsif ($InParamMaskList[$nb-1] ne "")
							{ # we introduce the mask
								$InDataStr = "(".$InParamMaskList[$nb-1]." & ".$InDataStr.")" ;
							}
							if ($OneLine =~ /$INPUT_DATA$nb$DATAPRINTF/)
							{  # we increment only after we don't need this value again
								$DataIndex[$nb-1] = $DataIndex[$nb-1] + 1 ;

								# if 'number of data already get' is equal to 'number of Data in File'
								if ($DataIndex[$nb-1] == $DataNbInFile)
								{	# end of generation of call to builtin
									$LastData = 1 ;
									# number of data for test
									push @DataSize, $DataNbInFile ;
								}
							}
							my $PrintStr="" ;
							$PrintStr=$TypeStruct[$TypeIndex]{fprintf} ;
							

						   if ( $TypeStruct[$TypeIndex]{NbSubSize} < 2 )
							{
							 	$PrintStr =~ s/###TOPRINT_1###/$InDataStr/ ;
							}
							else
							{
							 	$PrintStr =~ s/###TOPRINT_1###/(unsigned int)( $InDataStr >> 32 ) & 0x0FFFFFFFFu/ ;
							 	$PrintStr =~ s/###TOPRINT_2###/(unsigned int) $InDataStr & 0x0FFFFFFFFu/ ;
							}
							$PrintStr.=";   fprintf(stdout,\"  \")" ;
						   	
						   	$OneLine =~ s/$INPUT_DATA$nb$DATAPRINTF/$PrintStr/  ;
							$OneLine =~ s/$INPUT_DATA$nb$IS_IMMEDIATE/$InDataStr/ ;
						}
					}
					else
					{ # normal parameter
						if ($OneLine =~ /$INPUT_DATA$nb$IS_IMMEDIATE/) 
						{
							$OneLine = ""	;
						}
					  	else
					  	{
							my $TypeIndex = -1 ;
							
							$TypeIndex= GetDataTypeIndex($InParamList[$nb-1]) ;
							if ($TypeIndex <0 ) 
							{
								print $BuiltinName."TYPE:".$InParamList[$nb-1]." NOT FOUND \n" ;
								exit (1) ;
							}

							my $Make=$TypeStruct[$TypeIndex]{Make} ;
							my $PrintStr =$TypeStruct[$TypeIndex]{fprintf} ;
							my $NbOfSubSize = $TypeStruct[$TypeIndex]{NbSubSize} ;

					  		if ($OneLine =~ /$INPUT_DATA$nb$DATAMAKE/ )
							{
					  			if ($Make eq "")  
								{
									$OneLine = ""	;
									 if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
									 { 
									 	 $OneLine  = "\t\t in_out".$nb." = inp".$nb."[index] ;\n"	;
									 }
								}
								else
								{
									my $ind=0  ;
									$Make .= "( in".$nb ;
									for	($ind=0; $ind<$NbOfSubSize; $ind++) {
									$Make .= ", inp".$nb."_".$ind."[index]"	  ;
									}
									$Make .= " )" ;
									
									$OneLine =~ s/$INPUT_DATA$nb$DATAMAKE/$Make/	;
									$OneLine =~ s/$INPUT_DATA$nb$IS_NOT_IMMEDIATE// ;
									if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
									{ 
									 	my $InOutStr = $OneLine  ;
										$InOutStr =~ s/in$nb/in_out$nb/ ;
										$OneLine .= $InOutStr ;
									} 
								}

							}
							else
							{
							 my $In ="";
							 my $InPrt ="";
					  			if ($Make eq "")  
								{
									if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
									{ 
										$In = "in_out".$nb	;
									}
									else
									{
								  		$In = "inp".$nb."[index]";
									}
								   	
								   	$InPrt = "inp".$nb."[index]"     ;
								   	if ( $TypeStruct[$TypeIndex]{NbSubSize} < 2 )
									{
									 	$PrintStr =~ s/###TOPRINT_1###/$InPrt/ ;
									}
									else
									{
									 	$PrintStr =~ s/###TOPRINT_1###/(unsigned int)( $InPrt >> 32 ) & 0x0FFFFFFFFu/ ;
									 	$PrintStr =~ s/###TOPRINT_2###/(unsigned int) $InPrt & 0x0FFFFFFFFu/ ;
									}
								}
								else
								{
								 	if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
								 	{
								 	 	$In = "in_out".$nb	;
								 	}
									else
									{
								  		$In = "in".$nb	;
									}

									 $PrintStr .= "(stdout, in".$nb.")" ;
								}
								$OneLine =~ s/$INPUT_DATA$nb$IS_NOT_IMMEDIATE/$In/	;
							
							}
							$PrintStr.=";   fprintf(stdout,\"  \")" ;

					  	   	$OneLine =~ s/$INPUT_DATA$nb$DATAPRINTF/$PrintStr/  ;
					  	   	$OneLine =~ s/$INPUT_DATA$nb//  ;
					    }
					}
				}
			   	elsif ( $OneLine =~ /$OUTPUT_DATA(\d)/ ) 
			   	{
			   	 	my $nb = $1 ; 
			   		if ( $nb >	$NbOutParam )
			   		{
			   			$OneLine = ""	;
			  		}
			  		else 
			  		{
						my $TypeIndex = -1 ;
						
						$TypeIndex= GetDataTypeIndex($OutParamList[$nb-1]) ;
						if ($TypeIndex <0 ) 
						{
							print $BuiltinName."TYPE:".$OutParamList[$nb-1]." NOT FOUND \n" ;
							exit (1) ;
						}

						my $Make=$TypeStruct[$TypeIndex]{Make} ;
						my $Out = "out".$nb	;
						if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
						{	  
						  	$Out = "in_out".$nb	;
						}
						
						my $PrintStr="fprintf(stdout,\"$Out=\") ;  " ;
						$PrintStr.=$TypeStruct[$TypeIndex]{fprintf} ;

					  	if ($Make eq "")  
						{
							if ( $TypeStruct[$TypeIndex]{NbSubSize} < 2 )
							{
							 	$PrintStr =~ s/###TOPRINT_1###/$Out/ ;
							}
							else
							{
							 	$PrintStr =~ s/###TOPRINT_1###/(unsigned int)( $Out >> 32 ) & 0x0FFFFFFFFu/ ;
							 	$PrintStr =~ s/###TOPRINT_2###/(unsigned int) $Out & 0x0FFFFFFFFu/ ;
							}
						}
						else
						{
							$PrintStr .= "(stdout, ".$Out.")" ;
						}
					   	$OneLine =~ s/$OUTPUT_DATA$nb$DATAPRINTF/$PrintStr/  ;
						if ( ($InOutParamNb!=0) and (IsAnInOut($nb) ) )
						{	  
			  	   			$OneLine =~ s/$OUTPUT_DATA$nb.*//  ;
						}
						else
						{
							$OneLine =~ s/$OUTPUT_DATA$nb/$Out/	;	  #be careful to be done after xxxxDATAPRINTF 
						}

					}
			  	}
				elsif ( ($BuiltImmediate) and ($OneLine =~ /$IS_NOT_IMMEDIATE/)  )
				{ #remove loop
						$OneLine = ""	;
				}
				elsif ( !($BuiltImmediate) and ($OneLine =~ /$IS_IMMEDIATE/)  )
				{ #remove return
						$OneLine = ""	;
				}
				elsif ( $OneLine =~ /$OUTPUT_COMMA/ ) 
				{ #remove COMMA after output  when not output or input parameter
						if ( ($NbInParam==0) or ($NbOutParam==0 ) or ($InOutParamNb!=0) )
						{	  
							$OneLine = ""	;
						}
						else
						{	  
							$OneLine =~ s/$OUTPUT_COMMA//	; 
						}
				}
				else
				{
				 $OneLine =~ s/$IS_IMMEDIATE//		;
				 $OneLine =~ s/$IS_NOT_IMMEDIATE//	;
				}
				 $StrBuiltin.="$OneLine";
			}

		}
		else
		{
			print "ERROR Cannot OPEN Builtin Template Src  File\n";
		}
	  
	  $StrBuiltin.="//---------------------------------------\n";

	 } while  ( ($BuiltImmediate) and	($LastData == 0) )	 ;



	# ----------------- CONSTRUCT the MAIN  CODE -----------

	if (open(SRCTEMPLATE,"<$SrcTemplate")) 
	{
		if (open(SRCFILE,">$OneSrcBuiltinFile"))  
		{
		my 	$OneLine ;
		my $DataNb   = 0 ;
			while ($OneLine=<SRCTEMPLATE>) 
			{

				$OneLine =~ s/$EXTENSION_NAME/$Extension/	;
				$OneLine =~ s/$BUILTIN_NAME/$BuiltinName/	;
				$OneLine =~ s/$INSTRUCTION_NAME/$InstName/	;
				$OneLine =~ s/$GENERATION_DATE/$GenDate/	;
				
				$OneLine =~ s/$INPUT_PARAM_NB/$NbInParam/ 		; 	
				$OneLine =~ s/$OUTPUT_PARAM_NB/$NbOutParam/ 	; 	
				$OneLine =~ s/$INOUT_PARAM_NB/$NbInOutParam/ 	; 	

				if ( $OneLine =~ /HISTORY/ ) 
				{
					$OneLine = ""	;
				}
				elsif ( $OneLine =~ /$INPUT_DATA(\d)$DATABLOCK/ ) 
				{
				 	my $nb = $1 ; 
					if ( $nb >	$NbInParam )
					{
						$OneLine = ""	;
					}
					else
					{
						if ( ($BuiltImmediate) and IsAnImmediate($nb) )
						{ # immediate parameter
							if ($OneLine =~ /$INPUT_DATA$nb$IS_NOT_IMMEDIATE/) 
							{
								$OneLine = ""	;
							}
						}
						else 
						{
							my $InDataStr = "";
							my $InDataBlock = "";
							my $TypeIndex = -1 ;
							my $TypeSize = 0 ;
							my $DataNbInFile = 0 ;
				
							$TypeIndex= GetDataTypeIndex($InParamList[$nb-1]) ;
							if ($TypeIndex <0 ) 
							{
								print $BuiltinName."TYPE:".$InParamList[$nb-1]." NOT FOUND \n" ;
								exit (1) ;
							}
							my $Make=$TypeStruct[$TypeIndex]{Make} ;

					  		if ($Make eq "")  
							{
								($DataNbInFile,$InDataStr)  = GetDataStr($TypeStruct[$TypeIndex]{Size} ) ;
								push @DataSize, $DataNbInFile ;
							 	$InDataBlock = $InParamList[$nb-1]." inp".$nb."[".$DataNbInFile."] = {\n".$InDataStr."};\n"
							}
							else
							{ 
							
							my $SubSizeType = $TypeStruct[$TypeIndex]{SubSizeType} ;
							my $NbOfSubSize = $TypeStruct[$TypeIndex]{NbSubSize}   ;
							my $ind=0  ;

							for	($ind=0; $ind<$NbOfSubSize; $ind++) {
								($DataNbInFile,$InDataStr)  = GetDataStr($TypeStruct[$TypeIndex]{SubSize} ) ;
								push @DataSize, $DataNbInFile ;
							 	$InDataBlock .= $SubSizeType." inp".$nb."_".$ind."[".$DataNbInFile."] = {\n".$InDataStr."};\n"
								}
							 $InDataBlock .="\n".$InParamList[$nb-1]." in".$nb." ; \n" ;
							}
							$OneLine =~ s/$INPUT_DATA$nb$IS_NOT_IMMEDIATE//	;
							$OneLine =~ s/$INPUT_DATA_TAB_SIZE/$DataNbInFile/	;
							$OneLine =~ s/$INPUT_DATA$nb$DATABLOCK/$InDataBlock/ ;
						}
					}
				}
				elsif ( $OneLine =~ /$INPUT_DATA(\d)/ ) 
				{
				 	my $nb = $1 ;
					if ( $nb >	$NbInParam )
					{
						$OneLine = ""	;
					}
					else 
					{
						$OneLine =~ s/$INPUT_DATA$nb$IS_NOT_IMMEDIATE//	;
						$OneLine =~ s/$INPUT_DATA$nb$DATATYPE/$InParamList[$nb-1]/  ;
						$OneLine =~ s/$INPUT_DATA$nb//  ;
					}
				} 
				elsif ( $OneLine =~ /$OUTPUT_DATA(\d)/ ) 
				{
				 	my $nb = $1 ; 
					
					if ( $nb >	$NbOutParam )
					{
						$OneLine = ""	;
					}
					else 
					{
						my $TypeIndex = -1 ;
						
						$TypeIndex= GetDataTypeIndex($OutParamList[$nb-1]) ;
						if ($TypeIndex <0 ) 
						{
							print $BuiltinName."TYPE:".$OutParamList[$nb-1]." NOT FOUND \n" ;
							exit (1) ;
						}
					   	$OneLine =~ s/$OUTPUT_DATA$nb$DATATYPE/$OutParamList[$nb-1]/  ;
				   		if ( ($InOutParamNb!=0) and (IsAnInOut($nb) )	)
				   		{
				   			$OneLine =~ s/out$nb/in_out$nb/ ;
				   		}

					}
				}
				
				my $TempTabSize = 0	 ;
				if (scalar(@DataSize) != 0) { $TempTabSize =$DataSize[0]; }
				
				$OneLine =~ s/$INPUT_DATA_TAB_SIZE/$TempTabSize/;

				$OneLine =~ s/$TAB_BUILTIN/$StrBuiltin/	;

				print SRCFILE "$OneLine";
 			}
			close(SRCFILE);
		}
		else
		{
			print "ERROR Cannot OPEN generated Src File\n";
		}
		close(SRCTEMPLATE);
	}
	else
	{
		print "ERROR Cannot OPEN Template Src  File\n";
	}

	my $NbSize  = scalar(@DataSize )  ;
	for (my $Index=0; $Index<$NbSize; $Index++) 
	{
		if ( $DataSize[$Index] != $DataSize[0]  )
		{
			print $BuiltinName." Data size (".@DataSize.") shall be the same for all input  \n" ;
			exit (1) ;								  
			}
	}
	
	
} #end sub GenerateSrc


#-------------------------------------------------------------------------------
# Main program
#-------------------------------------------------------------------------------
# Result:
#   All builtin C files generated in $SrcGenerateDir 
#   directory.
#-------------------------------------------------------------------------------
# Prerequisites:
#-------------------------------------------------------------------------------
# Comments:
#-------------------------------------------------------------------------------

# Set umask for accessibility of created files by group
umask 007;

print "$ScriptIdentification\n" ;

if ( (scalar(@ARGV) < $NbArgMin) or 	(scalar(@ARGV) > $NbArgMax)  )
{
	Usage();
	exit(1);
}

# Analyze Command Line
AnalyzeCmdLine(\@ARGV);


# ------- Create test Source Dir
if (!-d $SrcDir) 
{
	my $Command = "mkdir $SrcDir";

	if (system("$Command") != 0) 
	{
		print "$Command\n";
		print "Make Test Source directory failed\n";
		exit(1);
	}
}


# -------- Read  Builtin list File -------------------

open(LISTFILE,"<$BuiltinListFile") || die "ERROR $BuiltinListFile File couldn't be open " ;
my $OneLine;
my $BuiltinName  = "";
my $AddName  = "";
my $InstName  = "";
my $OnWork = 0 ;
my $BuiltImmediate = 0	;
my $BUILD_ONLY = 0  ;
my $ParamNb = 0	   		;
my $BuiltinCounter = 0 	;

my $NewDataType=0 ;
$InOutParamNb = 0   	;
				
 
while ($OneLine=<LISTFILE>)
{
	chomp($OneLine);
	if ($OneLine =~ /^#/) 
	{
		# Comments in this file begin with a #
		next;
	}
	elsif ($OneLine =~ /^STOP_FILE/)
	{ # we want to stop before the end of builtins list file
	 last ;
	}
	elsif ($OneLine =~ /^MADE_BY_CORXPERT/)
	{
		$ExtensionType = "MADE_BY_CORXPERT" ;
		$SrcTemplate     	 = "$SrcVobDir/$SrcTemplatesDir/cxp_builtin_test_main_template.c"	;
		$SrcBuiltinTemplate  = "$SrcVobDir/$SrcTemplatesDir/cxp_basic_builtin.c"	;
		next;
	}
	elsif ($OneLine =~ /^MADE_BY_RTL/)
	{
		$ExtensionType = "MADE_BY_RTL" ;
		$SrcTemplate     	 = "$SrcVobDir/$SrcTemplatesDir/cxp_builtin_test_main_template.c"	;
		$SrcBuiltinTemplate  = "$SrcVobDir/$SrcTemplatesDir/cxp_basic_builtin.c"	;
		next;
	}
	elsif ($OneLine =~ /^TYPE_DATA=(\w+)/)
	{  
		if ( $NewDataType != 0 )
		{
			print "ERROR LAST DATATYPE STRUCTURE of $TypeStruct[$DataTypeIndex]{Name} is NOT FINISH \n";
			exit(1);
		}
		$TypeStruct[$DataTypeIndex]{Name}= $1;

		my $MakeData= $TypeStruct[$DataTypeIndex]{Name} ;
		$MakeData=~ s/_/_make_/		  ;
 		$TypeStruct[$DataTypeIndex]{Make}		= $MakeData	 ;
		
		my $PrintData= $TypeStruct[$DataTypeIndex]{Name} ;
		$PrintData=~ s/_/_fprintf_/	 ;
 		$TypeStruct[$DataTypeIndex]{fprintf} = $PrintData	 ;

		$TypeStruct[$DataTypeIndex]{Size} 		= 0 ;
 		$TypeStruct[$DataTypeIndex]{SubSize} 	= 0 ;
		$TypeStruct[$DataTypeIndex]{SubSizeType} ='';

		$NewDataType=1 ;
	}
	elsif ( ($NewDataType==1) and ($OneLine =~ /^END_THIS_DATA_TYPE/) )
	{  
		if ( $TypeStruct[$DataTypeIndex]{Size} == 0 )
		{
			print "ERROR DATA SIZE of $TypeStruct[$DataTypeIndex]{Name} is Missing \n";
			exit(1);
		}
		if ( ($TypeStruct[$DataTypeIndex]{SubSize} != 0) and ($TypeStruct[$DataTypeIndex]{SubSizeType} eq '')  )
		{
			print "ERROR TYPE of SUB SIZE of $TypeStruct[$DataTypeIndex]{Name} is Missing \n";
			exit(1);
		}
	   	$DataTypeIndex ++ ;
		$NewDataType=0 ;
	}
 	elsif ( ($NewDataType==1) and ($OneLine =~ /SIZE_DATA=(\d+)/) )
	{
 		$TypeStruct[$DataTypeIndex]{Size}=	$1	 ;
	}
 	elsif ( ($NewDataType==1) and ($OneLine =~ /SIZE_SUB_DATA=(\d+)/) )
	{
 		$TypeStruct[$DataTypeIndex]{SubSize}=  $1		 ;

		if ( $TypeStruct[$DataTypeIndex]{SubSize} == 0 )
		{
			print "ERROR DATA SUB SIZE = 0 of $TypeStruct[$DataTypeIndex]{Name} is FORBIDDEN \n";
			exit(1);
		}

		my $NbSubSize= $TypeStruct[$DataTypeIndex]{Size} / $TypeStruct[$DataTypeIndex]{SubSize} ;

		if ( $NbSubSize != int($NbSubSize) )
		{
			print "ERROR DATA SUB SIZE of $TypeStruct[$DataTypeIndex]{Size} \n";
			print "MUST BE A MULTIPLE of $TypeStruct[$DataTypeIndex]{SubSize} \n";
			exit(1);
		}
		$TypeStruct[$DataTypeIndex]{NbSubSize}=$NbSubSize ;
  	}
 	elsif ( ($NewDataType==1) and ($OneLine =~ /SIZE_SUB_TYPE=(.*)/) )		 
	{
 		$TypeStruct[$DataTypeIndex]{SubSizeType}=  $1		 ;
	}
	elsif ($OneLine =~ /^__builtin_(\w+)/)
	{
		if ($NewDataType==1)
		{
			print "ERROR: DATA TYPE definition not FINISH \n";
			exit (1) ;
		}
		if ($OnWork == 1 )
		{
			print "ERROR end missing for $BuiltinName in $BuiltinListFile  \n";
			exit (1) ;
		}
		$BuiltinName = $1 ;
		print  "Reading $BuiltinName in builtin list file\n";
		$AddName ="";
		$InstName ="";
		@InOutParamList  = () ;
		@InParamList  = () ;
		@ImmedParamNb  = () ;
		@OutParamList = () ;
		@InDataFile   = () ;
		@InParamMaskList= ("","","","") ;
		@InParamForceList= ("","","","") ;
		$OnWork = 1	   ;
		$BuiltImmediate = 0	   ;
		$ParamNb = 0	   		;
		$BuiltinCounter ++ ;
	}
	elsif ($OnWork == 1 )
	{
	 	if ($OneLine =~ s/^immediate//)
		{
			$BuiltImmediate = 1	   ;
		}
	 	elsif ($OneLine =~ s/^---addname=(\w+)//)
		{  # name to be add to file because we can have several files for same builtin
		 	$AddName = $1 ;
		}
		elsif ($OneLine =~ /^(INSTRUCTION_\w+)/)
		{
			$InstName = $1	   ;
		}
	 	elsif ($OneLine =~ s/^BUILD_ONLY//)
		{
			$BUILD_ONLY = 1	   ;
		}
	 	elsif ( ($BuiltImmediate == 1) and ($OneLine =~ s/^ImmedparamIn\s+//) )
		{
		 	push @InParamList , $OneLine ;
		 	$ParamNb ++ ;
		 	push @ImmedParamNb , $ParamNb ;
		}
	 	elsif ($OneLine =~ s/^paramIn\s+//)
		{
		 	push @InParamList , $OneLine ;
		 	$ParamNb ++ ;
		}
	 	elsif ($OneLine =~ s/^---mask=(\w+)//)
		{
		 	print "mask= $1 \n";
		 	$InParamMaskList[$ParamNb-1] = $1 ;
		}
	 	elsif ($OneLine =~ s/^---force=(\w+)//)
		{
		 	print "force= $1 \n";
		 	$InParamForceList[$ParamNb-1] = $1 ;
		}
	 	elsif ($OneLine =~ s/^paramOut\s+//)
		{
		 	push @OutParamList , $OneLine ;
		}
	 	elsif ($OneLine =~ s/^paramInOut\s+//)
		{ # parameter is used as an input and an output 
			if ( scalar(@OutParamList) != scalar(@InParamList) )
			{
				print "ERROR in_out parameter must be before anyelse parameter for $BuiltinName in $BuiltinListFile  \n";
				exit (1) ;
			}
			
			push @OutParamList 	, $OneLine ;
			push @InParamList 	, $OneLine ;
			$ParamNb ++ ;
			push @InOutParamList, $ParamNb ;  # enter  Input_Output parameter number 
			$InOutParamNb ++ ;
		}
	 	elsif ($OneLine =~ /^end/)
		{
			if ( ($BuiltImmediate == 1) and (scalar(@ImmedParamNb) == 0)	)
			{
				print "ERROR BuiltImmediate but no Immediate parameter for $BuiltinName in $BuiltinListFile  \n";
				exit (1) ;
			}
			
			print  "Generation of $BuiltinName C file \n";
  			GenerateSrc(
					$BuiltinName, 
					$BuiltImmediate,
					$InstName,
					$AddName,
					$BUILD_ONLY
					)	 ;
		 	$OnWork = 0 ;
			$BuiltImmediate = 0	;
			$BUILD_ONLY = 0  ;
			$ParamNb = 0	   	;
			$InOutParamNb  = 0 	;
			$DataFileNb = 1 ;
		}
	}
}
close(LISTFILE);
print "$BuiltinCounter builtins src files \n" ;

