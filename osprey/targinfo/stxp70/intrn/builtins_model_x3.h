/*
 *      Copyright 2006, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 * Version  ST/HPC/STS (2006/Nov/23)
 */

#ifdef __X3

#error "builtins_model_x3 must not be used on X3 extension"

#else
#ifndef __builtins_model_x3_h
#define __builtins_model_x3_h


extern void __cmodel_x3_clrcc(void)	 	;
extern void __cmodel_x3_startcc(void)		;
extern void __cmodel_x3_stopcc(void)		;
extern void __cmodel_x3_clrcc1(void)	   	;
extern void __cmodel_x3_startcc1(void)   	;
extern void __cmodel_x3_stopcc1(void)		;
extern void __cmodel_x3_clrcca(void)	  	;
extern void __cmodel_x3_startcca(void)   	;
extern void __cmodel_x3_stopcca(void)		;

extern void __cmodel_x3_terminate(void)  	;
extern void __cmodel_x3_ptouch(int)   	;
extern void __cmodel_x3_pinval(int)   	;
extern void __cmodel_x3_pinvalr(int)	;
extern void __cmodel_x3_plock(int)	   	;
extern void __cmodel_x3_plockr(int)	;
extern void __cmodel_x3_pflushw(void) 	;
extern void __cmodel_x3_dflush(int)   	;
extern void __cmodel_x3_dflushw(void)	;
extern void __cmodel_x3_dtouch(int)   	;
extern void __cmodel_x3_dlock(int)	 	;
extern void __cmodel_x3_dinval(int)   	;
extern void __cmodel_x3_pstata(int)   	;
extern void __cmodel_x3_pstatwi(int)	;
extern void __cmodel_x3_dstata(int)	;
extern void __cmodel_x3_dstatwi(int)	;

extern void __cmodel_x3_cancelg_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_cancelg_r_g(int);
extern void __cmodel_x3_cancell_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_cancell_r_g(int);
extern void __cmodel_x3_clric(unsigned, int);
extern int  __cmodel_x3_clrie(void);
extern void __cmodel_x3_fork_g(int, int, unsigned short);
extern void __cmodel_x3_fork_i3_g(unsigned short, int, int, unsigned short);
extern int  __cmodel_x3_movee2r(unsigned);
extern int  __cmodel_x3_moveic2r(unsigned);
extern int  __cmodel_x3_moveic2ri(int);
extern int  __cmodel_x3_moveie2r(void);
extern void __cmodel_x3_mover2ic(unsigned, int);
extern void __cmodel_x3_mover2ici(int, int);
extern void __cmodel_x3_mover2ie(int);
extern void __cmodel_x3_mover2e(unsigned, int);
extern void __cmodel_x3_notifyg_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_notifyg_r_g(int);
extern void __cmodel_x3_notifyl_i8_i2_i3_g(unsigned short, unsigned short, unsigned short);
extern void __cmodel_x3_notifyl_r_i3_g(unsigned short, int);
extern void __cmodel_x3_notifyla_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_notifyla_r_g(int);
extern void __cmodel_x3_setic(unsigned, int);
extern void __cmodel_x3_setie(int);
extern void __cmodel_x3_terminate_i3(unsigned short);
extern void __cmodel_x3_wait(int, int);
extern void __cmodel_x3_waitg_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_waitg_r_g(int);
extern void __cmodel_x3_waitl_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_waitl_r_g(int);
extern void __cmodel_x3_waitnb(int, int);
extern void __cmodel_x3_waitnbg_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_waitnbg_r_g(int);
extern void __cmodel_x3_waitnbl_i8_i2_g(unsigned short, unsigned short);
extern void __cmodel_x3_waitnbl_r_g(int);

extern int __cmodel_x3_mp(int, int);
extern int __cmodel_x3_mpn(int, int);

extern int __cmodel_x3_mpssll(int, int);
extern int __cmodel_x3_mpsslh(int, int);
extern int __cmodel_x3_mpsshl(int, int);
extern int __cmodel_x3_mpsshh(int, int);

extern int __cmodel_x3_mpuull(unsigned, unsigned);
extern int __cmodel_x3_mpuulh(unsigned, unsigned);
extern int __cmodel_x3_mpuuhl(unsigned, unsigned);
extern int __cmodel_x3_mpuuhh(unsigned, unsigned);

extern int __cmodel_x3_mpusll(unsigned, int);
extern int __cmodel_x3_mpuslh(unsigned, int);
extern int __cmodel_x3_mpushl(unsigned, int);
extern int __cmodel_x3_mpushh(unsigned, int);

extern int __cmodel_x3_mpsull(int, unsigned);
extern int __cmodel_x3_mpsulh(int, unsigned);
extern int __cmodel_x3_mpsuhl(int, unsigned);
extern int __cmodel_x3_mpsuhh(int, unsigned);


extern int __cmodel_x3_mpnsshh(int, int);
extern int __cmodel_x3_mpnsshl(int, int);
extern int __cmodel_x3_mpnsslh(int, int);
extern int __cmodel_x3_mpnssll(int, int);

extern int __cmodel_x3_mpnsuhh(int, unsigned);
extern int __cmodel_x3_mpnsuhl(int, unsigned);
extern int __cmodel_x3_mpnsulh(int, unsigned);
extern int __cmodel_x3_mpnsull(int, unsigned);

extern int __cmodel_x3_mpnushh(unsigned, int);
extern int __cmodel_x3_mpnushl(unsigned, int);
extern int __cmodel_x3_mpnuslh(unsigned, int);
extern int __cmodel_x3_mpnusll(unsigned, int);

extern int __cmodel_x3_mpnuuhh(unsigned, unsigned);
extern int __cmodel_x3_mpnuuhl(unsigned, unsigned);
extern int __cmodel_x3_mpnuulh(unsigned, unsigned);
extern int __cmodel_x3_mpnuull(unsigned, unsigned);


#endif  /* __builtins_model_x3_h */

#endif	/* __X3	*/



