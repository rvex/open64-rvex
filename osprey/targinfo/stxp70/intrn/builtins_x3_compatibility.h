/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */

#ifdef __X3

/* V4 compatiblity layer mp[n]us intructions are not available on v4
   but may be replaced by mp[n]su intructions by exchanging their
   paremeters
*/
#ifdef __STxP70_V4__
#define  __builtin_x3_mpnushh(a, b) __builtin_x3_mpnsuhh(b, a)
#define  __builtin_x3_mpnuslh(a, b) __builtin_x3_mpnsuhl(b, a)
#define  __builtin_x3_mpnushl(a, b) __builtin_x3_mpnsulh(b, a)
#define  __builtin_x3_mpnusll(a, b) __builtin_x3_mpnsull(b, a)

#define  __builtin_x3_mpushh(a, b) __builtin_x3_mpsuhh(b, a)
#define  __builtin_x3_mpuslh(a, b) __builtin_x3_mpsuhl(b, a)
#define  __builtin_x3_mpushl(a, b) __builtin_x3_mpsulh(b, a)
#define  __builtin_x3_mpusll(a, b) __builtin_x3_mpsull(b, a)

#endif

#endif
