/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */

/* Builtins for class sx */
#ifdef __SX

#ifndef _BUILTIN_SX_COMPATIBILITY_H_
#define _BUILTIN_SX_COMPATIBILITY_H_


/* loopena lw
 * lw loopena( u[0..1]);
 */
#define __builtin_sx_loopena(l) {           \
  asm(\
      " loopena L%0"\
      : \
      : "i" (l)\
      );       \
  }

/* loopdis lw
 * lw loopdis( u[0..1]);
 */
#define __builtin_sx_loopdis(l) {           \
  asm(\
      " loopdis L%0"\
      : \
      : "i" (l)\
      );       \
  }

/*  mover2sfr sfr, gpr
 * sfr mover2sfr( u[0..31], gpr);
 */
#define __builtin_sx_mover2sfr(i,r) {           \
  asm(\
      " mover2sfr S%0, %1"\
      : \
      : "i" (i), "r" (r)\
      );\
    }

/*  movesfr2r gpr, sfr
 * gpr movesfr2r( u[0..31]);
 */
#define __builtin_sx_movesfr2r(i)( {           \
    int res;\
  asm(\
      " movesfr2r %0, S%1"\
      : "=r" (res)\
      : "i" (i)\
      ); res;\
    })

#endif
#endif
