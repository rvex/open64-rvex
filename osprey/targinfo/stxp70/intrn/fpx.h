/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */



#ifndef _FPX_H_
#define _FPX_H_

#if defined(__FPX) && !defined(__FPX_C_MODEL)

/* Macros mapped on builtins for class fpx */
#include <builtins_fpx.h>

#ifdef __STxP70_V3__
/*  divu fpr , gpr , gpr
 * fpr divu( gpr, gpr);
 */
#define fpx_divu_f  __builtin_fpx_divu
#define fpx_divu(a, b, c) { \
  a = __builtin_fpx_divu(b, c); \
}

/*  div fpr , gpr , gpr
 * fpr div( gpr, gpr);
 */
#define fpx_div_f  __builtin_fpx_div
#define fpx_div(a, b, c) { \
  a = __builtin_fpx_div(b, c); \
}

/*  fabs fpr , fpr
 * fpr fabs( fpr);
 */
#define fpx_fabs_f  __builtin_fpx_fabs
#define fpx_fabs(a, b) { \
  a = __builtin_fpx_fabs(b); \
}

/*  faddaa fpr , fpr , fpr
 * fpr faddaa( fpr, fpr);
 */
#define fpx_faddaa_f  __builtin_fpx_faddaa
#define fpx_faddaa(a, b, c) { \
  a = __builtin_fpx_faddaa(b, c); \
}

/*  faddn fpr , fpr , fpr
 * fpr faddn( fpr, fpr);
 */
#define fpx_faddn_f  __builtin_fpx_faddn
#define fpx_faddn(a, b, c) { \
  a = __builtin_fpx_faddn(b, c); \
}

/*  fadd fpr , fpr , fpr
 * fpr fadd( fpr, fpr);
 */
#define fpx_fadd_f  __builtin_fpx_fadd
#define fpx_fadd(a, b, c) { \
  a = __builtin_fpx_fadd(b, c); \
}

/*  fasub fpr , fpr , fpr
 * fpr fasub( fpr, fpr);
 */
#define fpx_fasub_f  __builtin_fpx_fasub
#define fpx_fasub(a, b, c) { \
  a = __builtin_fpx_fasub(b, c); \
}

/*  fclrs <u6>
 * fclrs( <u6>);
 */
#define fpx_fclrs_f  __builtin_fpx_fclrs
#define fpx_fclrs(a) { \
  __builtin_fpx_fclrs(a); \
}

/*  fcmpuISA_EC_cp gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpueq_f  __builtin_fpx_fcmpueq
#define fpx_fcmpueq(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_eq, c, d); \
}

#define fpx_fcmpune_f  __builtin_fpx_fcmpune
#define fpx_fcmpune(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_ne, c, d); \
}

#define fpx_fcmpuge_f  __builtin_fpx_fcmpuge
#define fpx_fcmpuge(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_ge, c, d); \
}

#define fpx_fcmpult_f  __builtin_fpx_fcmpult
#define fpx_fcmpult(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_lt, c, d); \
}

#define fpx_fcmpule_f  __builtin_fpx_fcmpule
#define fpx_fcmpule(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_le, c, d); \
}

#define fpx_fcmpugt_f  __builtin_fpx_fcmpugt
#define fpx_fcmpugt(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_gt, c, d); \
}

#define fpx_fcmpuun_f  __builtin_fpx_fcmpuun
#define fpx_fcmpuun(a, c, d) { \
  a = __builtin_fpx_fcmpu_cp_g(intrn_cp_un, c, d); \
}

/*  fcmpuISA_EC_cpN gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpu_neq_f  __builtin_fpx_fcmpu_neq
#define fpx_fcmpu_neq(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_eq, c, d); \
}

#define fpx_fcmpu_nne_f  __builtin_fpx_fcmpu_nne
#define fpx_fcmpu_nne(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_ne, c, d); \
}

#define fpx_fcmpu_nge_f  __builtin_fpx_fcmpu_nge
#define fpx_fcmpu_nge(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_ge, c, d); \
}

#define fpx_fcmpu_nlt_f  __builtin_fpx_fcmpu_nlt
#define fpx_fcmpu_nlt(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_lt, c, d); \
}

#define fpx_fcmpu_nle_f  __builtin_fpx_fcmpu_nle
#define fpx_fcmpu_nle(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_le, c, d); \
}

#define fpx_fcmpu_ngt_f  __builtin_fpx_fcmpu_ngt
#define fpx_fcmpu_ngt(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_gt, c, d); \
}

#define fpx_fcmpu_nun_f  __builtin_fpx_fcmpu_nun
#define fpx_fcmpu_nun(a, c, d) { \
  a = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_un, c, d); \
}

/*  fcmpISA_EC_cp gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpeq_f  __builtin_fpx_fcmpeq
#define fpx_fcmpeq(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_eq, c, d); \
}

#define fpx_fcmpne_f  __builtin_fpx_fcmpne
#define fpx_fcmpne(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_ne, c, d); \
}

#define fpx_fcmpge_f  __builtin_fpx_fcmpge
#define fpx_fcmpge(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_ge, c, d); \
}

#define fpx_fcmplt_f  __builtin_fpx_fcmplt
#define fpx_fcmplt(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_lt, c, d); \
}

#define fpx_fcmple_f  __builtin_fpx_fcmple
#define fpx_fcmple(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_le, c, d); \
}

#define fpx_fcmpgt_f  __builtin_fpx_fcmpgt
#define fpx_fcmpgt(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_gt, c, d); \
}

#define fpx_fcmpun_f  __builtin_fpx_fcmpun
#define fpx_fcmpun(a, c, d) { \
  a = __builtin_fpx_fcmp_cp_g(intrn_cp_un, c, d); \
}

/*  fcmpISA_EC_cpN gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmp_neq_f  __builtin_fpx_fcmp_neq
#define fpx_fcmp_neq(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_eq, c, d); \
}

#define fpx_fcmp_nne_f  __builtin_fpx_fcmp_nne
#define fpx_fcmp_nne(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_ne, c, d); \
}

#define fpx_fcmp_nge_f  __builtin_fpx_fcmp_nge
#define fpx_fcmp_nge(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_ge, c, d); \
}

#define fpx_fcmp_nlt_f  __builtin_fpx_fcmp_nlt
#define fpx_fcmp_nlt(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_lt, c, d); \
}

#define fpx_fcmp_nle_f  __builtin_fpx_fcmp_nle
#define fpx_fcmp_nle(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_le, c, d); \
}

#define fpx_fcmp_ngt_f  __builtin_fpx_fcmp_ngt
#define fpx_fcmp_ngt(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_gt, c, d); \
}

#define fpx_fcmp_nun_f  __builtin_fpx_fcmp_nun
#define fpx_fcmp_nun(a, c, d) { \
  a = __builtin_fpx_fcmp_n_cp_g(intrn_cp_un, c, d); \
}

/*  fcnst fpr , <u9>
 * fpr fcnst( <u9>);
 */
#define fpx_fcnst_f  __builtin_fpx_fcnst
#define fpx_fcnst(a, b) { \
  a = __builtin_fpx_fcnst(b); \
}

/*  fdiv fpr , fpr , fpr
 * fpr fdiv( fpr, fpr);
 */
#define fpx_fdiv_f  __builtin_fpx_fdiv
#define fpx_fdiv(a, b, c) { \
  a = __builtin_fpx_fdiv(b, c); \
}

/*  ff2in gpr , fpr
 * gpr ff2in( fpr);
 */
#define fpx_ff2in_r_f  __builtin_fpx_ff2in_r
#define fpx_ff2in_r(a, b) { \
  a = __builtin_fpx_ff2in_r(b); \
}

/*  ff2in fpr , fpr
 * fpr ff2in( fpr);
 */
#define fpx_ff2in_f_f  __builtin_fpx_ff2in_f
#define fpx_ff2in_f(a, b) { \
  a = __builtin_fpx_ff2in_f(b); \
}

/*  ff2i gpr , fpr
 * gpr ff2i( fpr);
 */
#define fpx_ff2i_r_f  __builtin_fpx_ff2i_r
#define fpx_ff2i_r(a, b) { \
  a = __builtin_fpx_ff2i_r(b); \
}

/*  ff2i fpr , fpr
 * fpr ff2i( fpr);
 */
#define fpx_ff2i_f_f  __builtin_fpx_ff2i_f
#define fpx_ff2i_f(a, b) { \
  a = __builtin_fpx_ff2i_f(b); \
}

/*  ff2un gpr , fpr
 * gpr ff2un( fpr);
 */
#define fpx_ff2un_r_f  __builtin_fpx_ff2un_r
#define fpx_ff2un_r(a, b) { \
  a = __builtin_fpx_ff2un_r(b); \
}

/*  ff2un fpr , fpr
 * fpr ff2un( fpr);
 */
#define fpx_ff2un_f_f  __builtin_fpx_ff2un_f
#define fpx_ff2un_f(a, b) { \
  a = __builtin_fpx_ff2un_f(b); \
}

/*  ff2u gpr , fpr
 * gpr ff2u( fpr);
 */
#define fpx_ff2u_r_f  __builtin_fpx_ff2u_r
#define fpx_ff2u_r(a, b) { \
  a = __builtin_fpx_ff2u_r(b); \
}

/*  ff2u fpr , fpr
 * fpr ff2u( fpr);
 */
#define fpx_ff2u_f_f  __builtin_fpx_ff2u_f
#define fpx_ff2u_f(a, b) { \
  a = __builtin_fpx_ff2u_f(b); \
}

/*  fi2f fpr , gpr
 * fpr fi2f( gpr);
 */
#define fpx_fi2f_r_f  __builtin_fpx_fi2f_r
#define fpx_fi2f_r(a, b) { \
  a = __builtin_fpx_fi2f_r(b); \
}

/*  fi2f fpr , fpr
 * fpr fi2f( fpr);
 */
#define fpx_fi2f_f_f  __builtin_fpx_fi2f_f
#define fpx_fi2f_f(a, b) { \
  a = __builtin_fpx_fi2f_f(b); \
}

/*  fmacn fpr , fpr , fpr
 * fpr fmacn( fpr, fpr, fpr);
 */
#define fpx_fmacn_f  __builtin_fpx_fmacn
#define fpx_fmacn(a, b, c) { \
  a = __builtin_fpx_fmacn(a, b, c); \
}

/*  fmac fpr , fpr , fpr
 * fpr fmac( fpr, fpr, fpr);
 */
#define fpx_fmac_f  __builtin_fpx_fmac
#define fpx_fmac(a, b, c) { \
  a = __builtin_fpx_fmac(a, b, c); \
}

/*  fmax fpr , fpr , fpr
 * fpr fmax( fpr, fpr);
 */
#define fpx_fmax_f  __builtin_fpx_fmax
#define fpx_fmax(a, b, c) { \
  a = __builtin_fpx_fmax(b, c); \
}

/*  fmin fpr , fpr , fpr
 * fpr fmin( fpr, fpr);
 */
#define fpx_fmin_f  __builtin_fpx_fmin
#define fpx_fmin(a, b, c) { \
  a = __builtin_fpx_fmin(b, c); \
}

/*  fmuln fpr , fpr , fpr
 * fpr fmuln( fpr, fpr);
 */
#define fpx_fmuln_f  __builtin_fpx_fmuln
#define fpx_fmuln(a, b, c) { \
  a = __builtin_fpx_fmuln(b, c); \
}

/*  fmul fpr , fpr , fpr
 * fpr fmul( fpr, fpr);
 */
#define fpx_fmul_f  __builtin_fpx_fmul
#define fpx_fmul(a, b, c) { \
  a = __builtin_fpx_fmul(b, c); \
}

/*  fmvf2f fpr , fpr
 * fpr fmvf2f( fpr);
 */
#define fpx_fmvf2f_f  __builtin_fpx_fmvf2f
#define fpx_fmvf2f(a, b) { \
  a = __builtin_fpx_fmvf2f(b); \
}

/*  fmvf2r gpr , fpr
 * gpr fmvf2r( fpr);
 */
#define fpx_fmvf2r_f  __builtin_fpx_fmvf2r
#define fpx_fmvf2r(a, b) { \
  a = __builtin_fpx_fmvf2r(b); \
}

/*  fmvr2f fpr , gpr
 * fpr fmvr2f( gpr);
 */
#define fpx_fmvr2f_f  __builtin_fpx_fmvr2f
#define fpx_fmvr2f(a, b) { \
  a = __builtin_fpx_fmvr2f(b); \
}

/*  fmvr2st gpr
 * fmvr2st( gpr);
 */
#define fpx_fmvr2st_f  __builtin_fpx_fmvr2st
#define fpx_fmvr2st(a) { \
  __builtin_fpx_fmvr2st(a); \
}

/*  fmvr2s gpr
 * fmvr2s( gpr);
 */
#define fpx_fmvr2s_f  __builtin_fpx_fmvr2s
#define fpx_fmvr2s(a) { \
  __builtin_fpx_fmvr2s(a); \
}

/*  fmvs2r gpr
 * gpr fmvs2r( );
 */
#define fpx_fmvs2r_f  __builtin_fpx_fmvs2r
#define fpx_fmvs2r(a) { \
  a = __builtin_fpx_fmvs2r(); \
}

/*  fneg fpr , fpr
 * fpr fneg( fpr);
 */
#define fpx_fneg_f  __builtin_fpx_fneg
#define fpx_fneg(a, b) { \
  a = __builtin_fpx_fneg(b); \
}

/*  fscalb fpr , fpr , gpr
 * fpr fscalb( fpr, gpr);
 */
#define fpx_fscalb_f  __builtin_fpx_fscalb
#define fpx_fscalb(a, b, c) { \
  a = __builtin_fpx_fscalb(b, c); \
}

/*  fsqrt fpr , fpr
 * fpr fsqrt( fpr);
 */
#define fpx_fsqrt_f  __builtin_fpx_fsqrt
#define fpx_fsqrt(a, b) { \
  a = __builtin_fpx_fsqrt(b); \
}

/*  fsub fpr , fpr , fpr
 * fpr fsub( fpr, fpr);
 */
#define fpx_fsub_f  __builtin_fpx_fsub
#define fpx_fsub(a, b, c) { \
  a = __builtin_fpx_fsub(b, c); \
}

/*  fu2f fpr , gpr
 * fpr fu2f( gpr);
 */
#define fpx_fu2f_r_f  __builtin_fpx_fu2f_r
#define fpx_fu2f_r(a, b) { \
  a = __builtin_fpx_fu2f_r(b); \
}

/*  fu2f fpr , fpr
 * fpr fu2f( fpr);
 */
#define fpx_fu2f_f_f  __builtin_fpx_fu2f_f
#define fpx_fu2f_f(a, b) { \
  a = __builtin_fpx_fu2f_f(b); \
}

/*  macn fpr , gpr , gpr
 * fpr macn( fpr, gpr, gpr);
 */
#define fpx_macn_f  __builtin_fpx_macn
#define fpx_macn(a, b, c) { \
  a = __builtin_fpx_macn(a, b, c); \
}

/*  macnISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr macn( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
#define fpx_macnssll_f  __builtin_fpx_macnssll
#define fpx_macnssll(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_ll, c, d, e); \
}

#define fpx_macnsslh_f  __builtin_fpx_macnsslh
#define fpx_macnsslh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_lh, c, d, e); \
}

#define fpx_macnsshl_f  __builtin_fpx_macnsshl
#define fpx_macnsshl(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_hl, c, d, e); \
}

#define fpx_macnsshh_f  __builtin_fpx_macnsshh
#define fpx_macnsshh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_hh, c, d, e); \
}

#define fpx_macnsull_f  __builtin_fpx_macnsull
#define fpx_macnsull(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_ll, c, d, e); \
}

#define fpx_macnsulh_f  __builtin_fpx_macnsulh
#define fpx_macnsulh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_lh, c, d, e); \
}

#define fpx_macnsuhl_f  __builtin_fpx_macnsuhl
#define fpx_macnsuhl(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_hl, c, d, e); \
}

#define fpx_macnsuhh_f  __builtin_fpx_macnsuhh
#define fpx_macnsuhh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_hh, c, d, e); \
}

#define fpx_macnusll_f  __builtin_fpx_macnusll
#define fpx_macnusll(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_ll, c, d, e); \
}

#define fpx_macnuslh_f  __builtin_fpx_macnuslh
#define fpx_macnuslh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_lh, c, d, e); \
}

#define fpx_macnushl_f  __builtin_fpx_macnushl
#define fpx_macnushl(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_hl, c, d, e); \
}

#define fpx_macnushh_f  __builtin_fpx_macnushh
#define fpx_macnushh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_hh, c, d, e); \
}

#define fpx_macnuull_f  __builtin_fpx_macnuull
#define fpx_macnuull(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_ll, c, d, e); \
}

#define fpx_macnuulh_f  __builtin_fpx_macnuulh
#define fpx_macnuulh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_lh, c, d, e); \
}

#define fpx_macnuuhl_f  __builtin_fpx_macnuuhl
#define fpx_macnuuhl(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_hl, c, d, e); \
}

#define fpx_macnuuhh_f  __builtin_fpx_macnuuhh
#define fpx_macnuuhh(a, d, e) { \
  a = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_hh, c, d, e); \
}

/*  mac fpr , gpr , gpr
 * fpr mac( fpr, gpr, gpr);
 */
#define fpx_mac_f  __builtin_fpx_mac
#define fpx_mac(a, b, c) { \
  a = __builtin_fpx_mac(a, b, c); \
}

/*  macISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr mac( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
#define fpx_macssll_f  __builtin_fpx_macssll
#define fpx_macssll(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_ll, c, d, e); \
}

#define fpx_macsslh_f  __builtin_fpx_macsslh
#define fpx_macsslh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_lh, c, d, e); \
}

#define fpx_macsshl_f  __builtin_fpx_macsshl
#define fpx_macsshl(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_hl, c, d, e); \
}

#define fpx_macsshh_f  __builtin_fpx_macsshh
#define fpx_macsshh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_hh, c, d, e); \
}

#define fpx_macsull_f  __builtin_fpx_macsull
#define fpx_macsull(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_ll, c, d, e); \
}

#define fpx_macsulh_f  __builtin_fpx_macsulh
#define fpx_macsulh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_lh, c, d, e); \
}

#define fpx_macsuhl_f  __builtin_fpx_macsuhl
#define fpx_macsuhl(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_hl, c, d, e); \
}

#define fpx_macsuhh_f  __builtin_fpx_macsuhh
#define fpx_macsuhh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_hh, c, d, e); \
}

#define fpx_macusll_f  __builtin_fpx_macusll
#define fpx_macusll(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_ll, c, d, e); \
}

#define fpx_macuslh_f  __builtin_fpx_macuslh
#define fpx_macuslh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_lh, c, d, e); \
}

#define fpx_macushl_f  __builtin_fpx_macushl
#define fpx_macushl(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_hl, c, d, e); \
}

#define fpx_macushh_f  __builtin_fpx_macushh
#define fpx_macushh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_hh, c, d, e); \
}

#define fpx_macuull_f  __builtin_fpx_macuull
#define fpx_macuull(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_ll, c, d, e); \
}

#define fpx_macuulh_f  __builtin_fpx_macuulh
#define fpx_macuulh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_lh, c, d, e); \
}

#define fpx_macuuhl_f  __builtin_fpx_macuuhl
#define fpx_macuuhl(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_hl, c, d, e); \
}

#define fpx_macuuhh_f  __builtin_fpx_macuuhh
#define fpx_macuuhh(a, d, e) { \
  a = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_hh, c, d, e); \
}

/*  modu fpr , gpr , gpr
 * fpr modu( gpr, gpr);
 */
#define fpx_modu_f  __builtin_fpx_modu
#define fpx_modu(a, b, c) { \
  a = __builtin_fpx_modu(b, c); \
}

/*  mod fpr , gpr , gpr
 * fpr mod( gpr, gpr);
 */
#define fpx_mod_f  __builtin_fpx_mod
#define fpx_mod(a, b, c) { \
  a = __builtin_fpx_mod(b, c); \
}

/*  mpnsshh gpr , gpr , gpr
 * gpr mpnsshh( gpr, gpr);
 */
#define fpx_mpnsshh_f  __builtin_fpx_mpnsshh
#define fpx_mpnsshh(a, b, c) { \
  a = __builtin_fpx_mpnsshh(b, c); \
}

/*  mpnsshl gpr , gpr , gpr
 * gpr mpnsshl( gpr, gpr);
 */
#define fpx_mpnsshl_f  __builtin_fpx_mpnsshl
#define fpx_mpnsshl(a, b, c) { \
  a = __builtin_fpx_mpnsshl(b, c); \
}

/*  mpnsslh gpr , gpr , gpr
 * gpr mpnsslh( gpr, gpr);
 */
#define fpx_mpnsslh_f  __builtin_fpx_mpnsslh
#define fpx_mpnsslh(a, b, c) { \
  a = __builtin_fpx_mpnsslh(b, c); \
}

/*  mpnssll gpr , gpr , gpr
 * gpr mpnssll( gpr, gpr);
 */
#define fpx_mpnssll_f  __builtin_fpx_mpnssll
#define fpx_mpnssll(a, b, c) { \
  a = __builtin_fpx_mpnssll(b, c); \
}

/*  mpnsuhh gpr , gpr , gpr
 * gpr mpnsuhh( gpr, gpr);
 */
#define fpx_mpnsuhh_f  __builtin_fpx_mpnsuhh
#define fpx_mpnsuhh(a, b, c) { \
  a = __builtin_fpx_mpnsuhh(b, c); \
}

/*  mpnsuhl gpr , gpr , gpr
 * gpr mpnsuhl( gpr, gpr);
 */
#define fpx_mpnsuhl_f  __builtin_fpx_mpnsuhl
#define fpx_mpnsuhl(a, b, c) { \
  a = __builtin_fpx_mpnsuhl(b, c); \
}

/*  mpnsulh gpr , gpr , gpr
 * gpr mpnsulh( gpr, gpr);
 */
#define fpx_mpnsulh_f  __builtin_fpx_mpnsulh
#define fpx_mpnsulh(a, b, c) { \
  a = __builtin_fpx_mpnsulh(b, c); \
}

/*  mpnsull gpr , gpr , gpr
 * gpr mpnsull( gpr, gpr);
 */
#define fpx_mpnsull_f  __builtin_fpx_mpnsull
#define fpx_mpnsull(a, b, c) { \
  a = __builtin_fpx_mpnsull(b, c); \
}

/*  mpnushh gpr , gpr , gpr
 * gpr mpnushh( gpr, gpr);
 */
#define fpx_mpnushh_f  __builtin_fpx_mpnushh
#define fpx_mpnushh(a, b, c) { \
  a = __builtin_fpx_mpnushh(b, c); \
}

/*  mpnushl gpr , gpr , gpr
 * gpr mpnushl( gpr, gpr);
 */
#define fpx_mpnushl_f  __builtin_fpx_mpnushl
#define fpx_mpnushl(a, b, c) { \
  a = __builtin_fpx_mpnushl(b, c); \
}

/*  mpnuslh gpr , gpr , gpr
 * gpr mpnuslh( gpr, gpr);
 */
#define fpx_mpnuslh_f  __builtin_fpx_mpnuslh
#define fpx_mpnuslh(a, b, c) { \
  a = __builtin_fpx_mpnuslh(b, c); \
}

/*  mpnusll gpr , gpr , gpr
 * gpr mpnusll( gpr, gpr);
 */
#define fpx_mpnusll_f  __builtin_fpx_mpnusll
#define fpx_mpnusll(a, b, c) { \
  a = __builtin_fpx_mpnusll(b, c); \
}

/*  mpnuuhh gpr , gpr , gpr
 * gpr mpnuuhh( gpr, gpr);
 */
#define fpx_mpnuuhh_f  __builtin_fpx_mpnuuhh
#define fpx_mpnuuhh(a, b, c) { \
  a = __builtin_fpx_mpnuuhh(b, c); \
}

/*  mpnuuhl gpr , gpr , gpr
 * gpr mpnuuhl( gpr, gpr);
 */
#define fpx_mpnuuhl_f  __builtin_fpx_mpnuuhl
#define fpx_mpnuuhl(a, b, c) { \
  a = __builtin_fpx_mpnuuhl(b, c); \
}

/*  mpnuulh gpr , gpr , gpr
 * gpr mpnuulh( gpr, gpr);
 */
#define fpx_mpnuulh_f  __builtin_fpx_mpnuulh
#define fpx_mpnuulh(a, b, c) { \
  a = __builtin_fpx_mpnuulh(b, c); \
}

/*  mpnuull gpr , gpr , gpr
 * gpr mpnuull( gpr, gpr);
 */
#define fpx_mpnuull_f  __builtin_fpx_mpnuull
#define fpx_mpnuull(a, b, c) { \
  a = __builtin_fpx_mpnuull(b, c); \
}

/*  mpn gpr , gpr , gpr
 * gpr mpn( gpr, gpr);
 */
#define fpx_mpn_f  __builtin_fpx_mpn
#define fpx_mpn(a, b, c) { \
  a = __builtin_fpx_mpn(b, c); \
}

/*  mpsshh gpr , gpr , gpr
 * gpr mpsshh( gpr, gpr);
 */
#define fpx_mpsshh_f  __builtin_fpx_mpsshh
#define fpx_mpsshh(a, b, c) { \
  a = __builtin_fpx_mpsshh(b, c); \
}

/*  mpsshl gpr , gpr , gpr
 * gpr mpsshl( gpr, gpr);
 */
#define fpx_mpsshl_f  __builtin_fpx_mpsshl
#define fpx_mpsshl(a, b, c) { \
  a = __builtin_fpx_mpsshl(b, c); \
}

/*  mpsslh gpr , gpr , gpr
 * gpr mpsslh( gpr, gpr);
 */
#define fpx_mpsslh_f  __builtin_fpx_mpsslh
#define fpx_mpsslh(a, b, c) { \
  a = __builtin_fpx_mpsslh(b, c); \
}

/*  mpssll gpr , gpr , gpr
 * gpr mpssll( gpr, gpr);
 */
#define fpx_mpssll_f  __builtin_fpx_mpssll
#define fpx_mpssll(a, b, c) { \
  a = __builtin_fpx_mpssll(b, c); \
}

/*  mpsuhh gpr , gpr , gpr
 * gpr mpsuhh( gpr, gpr);
 */
#define fpx_mpsuhh_f  __builtin_fpx_mpsuhh
#define fpx_mpsuhh(a, b, c) { \
  a = __builtin_fpx_mpsuhh(b, c); \
}

/*  mpsuhl gpr , gpr , gpr
 * gpr mpsuhl( gpr, gpr);
 */
#define fpx_mpsuhl_f  __builtin_fpx_mpsuhl
#define fpx_mpsuhl(a, b, c) { \
  a = __builtin_fpx_mpsuhl(b, c); \
}

/*  mpsulh gpr , gpr , gpr
 * gpr mpsulh( gpr, gpr);
 */
#define fpx_mpsulh_f  __builtin_fpx_mpsulh
#define fpx_mpsulh(a, b, c) { \
  a = __builtin_fpx_mpsulh(b, c); \
}

/*  mpsull gpr , gpr , gpr
 * gpr mpsull( gpr, gpr);
 */
#define fpx_mpsull_f  __builtin_fpx_mpsull
#define fpx_mpsull(a, b, c) { \
  a = __builtin_fpx_mpsull(b, c); \
}

/*  mpushh gpr , gpr , gpr
 * gpr mpushh( gpr, gpr);
 */
#define fpx_mpushh_f  __builtin_fpx_mpushh
#define fpx_mpushh(a, b, c) { \
  a = __builtin_fpx_mpushh(b, c); \
}

/*  mpushl gpr , gpr , gpr
 * gpr mpushl( gpr, gpr);
 */
#define fpx_mpushl_f  __builtin_fpx_mpushl
#define fpx_mpushl(a, b, c) { \
  a = __builtin_fpx_mpushl(b, c); \
}

/*  mpuslh gpr , gpr , gpr
 * gpr mpuslh( gpr, gpr);
 */
#define fpx_mpuslh_f  __builtin_fpx_mpuslh
#define fpx_mpuslh(a, b, c) { \
  a = __builtin_fpx_mpuslh(b, c); \
}

/*  mpusll gpr , gpr , gpr
 * gpr mpusll( gpr, gpr);
 */
#define fpx_mpusll_f  __builtin_fpx_mpusll
#define fpx_mpusll(a, b, c) { \
  a = __builtin_fpx_mpusll(b, c); \
}

/*  mpuuhh gpr , gpr , gpr
 * gpr mpuuhh( gpr, gpr);
 */
#define fpx_mpuuhh_f  __builtin_fpx_mpuuhh
#define fpx_mpuuhh(a, b, c) { \
  a = __builtin_fpx_mpuuhh(b, c); \
}

/*  mpuuhl gpr , gpr , gpr
 * gpr mpuuhl( gpr, gpr);
 */
#define fpx_mpuuhl_f  __builtin_fpx_mpuuhl
#define fpx_mpuuhl(a, b, c) { \
  a = __builtin_fpx_mpuuhl(b, c); \
}

/*  mpuulh gpr , gpr , gpr
 * gpr mpuulh( gpr, gpr);
 */
#define fpx_mpuulh_f  __builtin_fpx_mpuulh
#define fpx_mpuulh(a, b, c) { \
  a = __builtin_fpx_mpuulh(b, c); \
}

/*  mpuull gpr , gpr , gpr
 * gpr mpuull( gpr, gpr);
 */
#define fpx_mpuull_f  __builtin_fpx_mpuull
#define fpx_mpuull(a, b, c) { \
  a = __builtin_fpx_mpuull(b, c); \
}

/*  mp gpr , gpr , gpr
 * gpr mp( gpr, gpr);
 */
#define fpx_mp_f  __builtin_fpx_mp
#define fpx_mp(a, b, c) { \
  a = __builtin_fpx_mp(b, c); \
}

#endif

#else /* !defined(__FPX) || defined(__FPX_C_MODEL) */

/* Macros mapped on C models for class fpx */
#error "FPX models are not yet available"

#if 0

#include <builtins_model_fpx.h>

/*  divu fpr , gpr , gpr
 * fpr divu( gpr, gpr);
 */
#define fpx_divu_f __cmodel_fpx_divu
#define fpx_divu(a, b, c) { \
  a = __cmodel_fpx_divu(b, c); \
}

/*  div fpr , gpr , gpr
 * fpr div( gpr, gpr);
 */
#define fpx_div_f __cmodel_fpx_div
#define fpx_div(a, b, c) { \
  a = __cmodel_fpx_div(b, c); \
}

/*  fabs fpr , fpr
 * fpr fabs( fpr);
 */
#define fpx_fabs_f __cmodel_fpx_fabs
#define fpx_fabs(a, b) { \
  a = __cmodel_fpx_fabs(b); \
}

/*  faddaa fpr , fpr , fpr
 * fpr faddaa( fpr, fpr);
 */
#define fpx_faddaa_f __cmodel_fpx_faddaa
#define fpx_faddaa(a, b, c) { \
  a = __cmodel_fpx_faddaa(b, c); \
}

/*  faddn fpr , fpr , fpr
 * fpr faddn( fpr, fpr);
 */
#define fpx_faddn_f __cmodel_fpx_faddn
#define fpx_faddn(a, b, c) { \
  a = __cmodel_fpx_faddn(b, c); \
}

/*  fadd fpr , fpr , fpr
 * fpr fadd( fpr, fpr);
 */
#define fpx_fadd_f __cmodel_fpx_fadd
#define fpx_fadd(a, b, c) { \
  a = __cmodel_fpx_fadd(b, c); \
}

/*  fasub fpr , fpr , fpr
 * fpr fasub( fpr, fpr);
 */
#define fpx_fasub_f __cmodel_fpx_fasub
#define fpx_fasub(a, b, c) { \
  a = __cmodel_fpx_fasub(b, c); \
}

/*  fclrs <u6>
 * fclrs( <u6>);
 */
#define fpx_fclrs_f __cmodel_fpx_fclrs
#define fpx_fclrs(a) { \
  __cmodel_fpx_fclrs(a); \
}

/*  fcmpuISA_EC_cp gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpueq_f __cmodel_fpx_fcmpueq
#define fpx_fcmpueq(a, c, d) { \
  a = __cmodel_fpx_fcmpueq(c, d); \
}

#define fpx_fcmpune_f __cmodel_fpx_fcmpune
#define fpx_fcmpune(a, c, d) { \
  a = __cmodel_fpx_fcmpune(c, d); \
}

#define fpx_fcmpuge_f __cmodel_fpx_fcmpuge
#define fpx_fcmpuge(a, c, d) { \
  a = __cmodel_fpx_fcmpuge(c, d); \
}

#define fpx_fcmpult_f __cmodel_fpx_fcmpult
#define fpx_fcmpult(a, c, d) { \
  a = __cmodel_fpx_fcmpult(c, d); \
}

#define fpx_fcmpule_f __cmodel_fpx_fcmpule
#define fpx_fcmpule(a, c, d) { \
  a = __cmodel_fpx_fcmpule(c, d); \
}

#define fpx_fcmpugt_f __cmodel_fpx_fcmpugt
#define fpx_fcmpugt(a, c, d) { \
  a = __cmodel_fpx_fcmpugt(c, d); \
}

#define fpx_fcmpuun_f __cmodel_fpx_fcmpuun
#define fpx_fcmpuun(a, c, d) { \
  a = __cmodel_fpx_fcmpuun(c, d); \
}

/*  fcmpuISA_EC_cpN gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpu_neq_f __cmodel_fpx_fcmpu_neq
#define fpx_fcmpu_neq(a, c, d) { \
  a = __cmodel_fpx_fcmpu_neq(c, d); \
}

#define fpx_fcmpu_nne_f __cmodel_fpx_fcmpu_nne
#define fpx_fcmpu_nne(a, c, d) { \
  a = __cmodel_fpx_fcmpu_nne(c, d); \
}

#define fpx_fcmpu_nge_f __cmodel_fpx_fcmpu_nge
#define fpx_fcmpu_nge(a, c, d) { \
  a = __cmodel_fpx_fcmpu_nge(c, d); \
}

#define fpx_fcmpu_nlt_f __cmodel_fpx_fcmpu_nlt
#define fpx_fcmpu_nlt(a, c, d) { \
  a = __cmodel_fpx_fcmpu_nlt(c, d); \
}

#define fpx_fcmpu_nle_f __cmodel_fpx_fcmpu_nle
#define fpx_fcmpu_nle(a, c, d) { \
  a = __cmodel_fpx_fcmpu_nle(c, d); \
}

#define fpx_fcmpu_ngt_f __cmodel_fpx_fcmpu_ngt
#define fpx_fcmpu_ngt(a, c, d) { \
  a = __cmodel_fpx_fcmpu_ngt(c, d); \
}

#define fpx_fcmpu_nun_f __cmodel_fpx_fcmpu_nun
#define fpx_fcmpu_nun(a, c, d) { \
  a = __cmodel_fpx_fcmpu_nun(c, d); \
}

/*  fcmpISA_EC_cp gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmpeq_f __cmodel_fpx_fcmpeq
#define fpx_fcmpeq(a, c, d) { \
  a = __cmodel_fpx_fcmpeq(c, d); \
}

#define fpx_fcmpne_f __cmodel_fpx_fcmpne
#define fpx_fcmpne(a, c, d) { \
  a = __cmodel_fpx_fcmpne(c, d); \
}

#define fpx_fcmpge_f __cmodel_fpx_fcmpge
#define fpx_fcmpge(a, c, d) { \
  a = __cmodel_fpx_fcmpge(c, d); \
}

#define fpx_fcmplt_f __cmodel_fpx_fcmplt
#define fpx_fcmplt(a, c, d) { \
  a = __cmodel_fpx_fcmplt(c, d); \
}

#define fpx_fcmple_f __cmodel_fpx_fcmple
#define fpx_fcmple(a, c, d) { \
  a = __cmodel_fpx_fcmple(c, d); \
}

#define fpx_fcmpgt_f __cmodel_fpx_fcmpgt
#define fpx_fcmpgt(a, c, d) { \
  a = __cmodel_fpx_fcmpgt(c, d); \
}

#define fpx_fcmpun_f __cmodel_fpx_fcmpun
#define fpx_fcmpun(a, c, d) { \
  a = __cmodel_fpx_fcmpun(c, d); \
}

/*  fcmpISA_EC_cpN gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
#define fpx_fcmp_neq_f __cmodel_fpx_fcmp_neq
#define fpx_fcmp_neq(a, c, d) { \
  a = __cmodel_fpx_fcmp_neq(c, d); \
}

#define fpx_fcmp_nne_f __cmodel_fpx_fcmp_nne
#define fpx_fcmp_nne(a, c, d) { \
  a = __cmodel_fpx_fcmp_nne(c, d); \
}

#define fpx_fcmp_nge_f __cmodel_fpx_fcmp_nge
#define fpx_fcmp_nge(a, c, d) { \
  a = __cmodel_fpx_fcmp_nge(c, d); \
}

#define fpx_fcmp_nlt_f __cmodel_fpx_fcmp_nlt
#define fpx_fcmp_nlt(a, c, d) { \
  a = __cmodel_fpx_fcmp_nlt(c, d); \
}

#define fpx_fcmp_nle_f __cmodel_fpx_fcmp_nle
#define fpx_fcmp_nle(a, c, d) { \
  a = __cmodel_fpx_fcmp_nle(c, d); \
}

#define fpx_fcmp_ngt_f __cmodel_fpx_fcmp_ngt
#define fpx_fcmp_ngt(a, c, d) { \
  a = __cmodel_fpx_fcmp_ngt(c, d); \
}

#define fpx_fcmp_nun_f __cmodel_fpx_fcmp_nun
#define fpx_fcmp_nun(a, c, d) { \
  a = __cmodel_fpx_fcmp_nun(c, d); \
}

/*  fcnst fpr , <u9>
 * fpr fcnst( <u9>);
 */
#define fpx_fcnst_f __cmodel_fpx_fcnst
#define fpx_fcnst(a, b) { \
  a = __cmodel_fpx_fcnst(b); \
}

/*  fdiv fpr , fpr , fpr
 * fpr fdiv( fpr, fpr);
 */
#define fpx_fdiv_f __cmodel_fpx_fdiv
#define fpx_fdiv(a, b, c) { \
  a = __cmodel_fpx_fdiv(b, c); \
}

/*  ff2in gpr , fpr
 * gpr ff2in( fpr);
 */
#define fpx_ff2in_r_f __cmodel_fpx_ff2in_r
#define fpx_ff2in_r(a, b) { \
  a = __cmodel_fpx_ff2in_r(b); \
}

/*  ff2in fpr , fpr
 * fpr ff2in( fpr);
 */
#define fpx_ff2in_f_f __cmodel_fpx_ff2in_f
#define fpx_ff2in_f(a, b) { \
  a = __cmodel_fpx_ff2in_f(b); \
}

/*  ff2i gpr , fpr
 * gpr ff2i( fpr);
 */
#define fpx_ff2i_r_f __cmodel_fpx_ff2i_r
#define fpx_ff2i_r(a, b) { \
  a = __cmodel_fpx_ff2i_r(b); \
}

/*  ff2i fpr , fpr
 * fpr ff2i( fpr);
 */
#define fpx_ff2i_f_f __cmodel_fpx_ff2i_f
#define fpx_ff2i_f(a, b) { \
  a = __cmodel_fpx_ff2i_f(b); \
}

/*  ff2un gpr , fpr
 * gpr ff2un( fpr);
 */
#define fpx_ff2un_r_f __cmodel_fpx_ff2un_r
#define fpx_ff2un_r(a, b) { \
  a = __cmodel_fpx_ff2un_r(b); \
}

/*  ff2un fpr , fpr
 * fpr ff2un( fpr);
 */
#define fpx_ff2un_f_f __cmodel_fpx_ff2un_f
#define fpx_ff2un_f(a, b) { \
  a = __cmodel_fpx_ff2un_f(b); \
}

/*  ff2u gpr , fpr
 * gpr ff2u( fpr);
 */
#define fpx_ff2u_r_f __cmodel_fpx_ff2u_r
#define fpx_ff2u_r(a, b) { \
  a = __cmodel_fpx_ff2u_r(b); \
}

/*  ff2u fpr , fpr
 * fpr ff2u( fpr);
 */
#define fpx_ff2u_f_f __cmodel_fpx_ff2u_f
#define fpx_ff2u_f(a, b) { \
  a = __cmodel_fpx_ff2u_f(b); \
}

/*  fi2f fpr , gpr
 * fpr fi2f( gpr);
 */
#define fpx_fi2f_r_f __cmodel_fpx_fi2f_r
#define fpx_fi2f_r(a, b) { \
  a = __cmodel_fpx_fi2f_r(b); \
}

/*  fi2f fpr , fpr
 * fpr fi2f( fpr);
 */
#define fpx_fi2f_f_f __cmodel_fpx_fi2f_f
#define fpx_fi2f_f(a, b) { \
  a = __cmodel_fpx_fi2f_f(b); \
}

/*  fmacn fpr , fpr , fpr
 * fpr fmacn( fpr, fpr, fpr);
 */
#define fpx_fmacn_f __cmodel_fpx_fmacn
#define fpx_fmacn(a, b, c) { \
  a = __cmodel_fpx_fmacn(a, b, c); \
}

/*  fmac fpr , fpr , fpr
 * fpr fmac( fpr, fpr, fpr);
 */
#define fpx_fmac_f __cmodel_fpx_fmac
#define fpx_fmac(a, b, c) { \
  a = __cmodel_fpx_fmac(a, b, c); \
}

/*  fmax fpr , fpr , fpr
 * fpr fmax( fpr, fpr);
 */
#define fpx_fmax_f __cmodel_fpx_fmax
#define fpx_fmax(a, b, c) { \
  a = __cmodel_fpx_fmax(b, c); \
}

/*  fmin fpr , fpr , fpr
 * fpr fmin( fpr, fpr);
 */
#define fpx_fmin_f __cmodel_fpx_fmin
#define fpx_fmin(a, b, c) { \
  a = __cmodel_fpx_fmin(b, c); \
}

/*  fmuln fpr , fpr , fpr
 * fpr fmuln( fpr, fpr);
 */
#define fpx_fmuln_f __cmodel_fpx_fmuln
#define fpx_fmuln(a, b, c) { \
  a = __cmodel_fpx_fmuln(b, c); \
}

/*  fmul fpr , fpr , fpr
 * fpr fmul( fpr, fpr);
 */
#define fpx_fmul_f __cmodel_fpx_fmul
#define fpx_fmul(a, b, c) { \
  a = __cmodel_fpx_fmul(b, c); \
}

/*  fmvf2f fpr , fpr
 * fpr fmvf2f( fpr);
 */
#define fpx_fmvf2f_f __cmodel_fpx_fmvf2f
#define fpx_fmvf2f(a, b) { \
  a = __cmodel_fpx_fmvf2f(b); \
}

/*  fmvf2r gpr , fpr
 * gpr fmvf2r( fpr);
 */
#define fpx_fmvf2r_f __cmodel_fpx_fmvf2r
#define fpx_fmvf2r(a, b) { \
  a = __cmodel_fpx_fmvf2r(b); \
}

/*  fmvr2f fpr , gpr
 * fpr fmvr2f( gpr);
 */
#define fpx_fmvr2f_f __cmodel_fpx_fmvr2f
#define fpx_fmvr2f(a, b) { \
  a = __cmodel_fpx_fmvr2f(b); \
}

/*  fmvr2st gpr
 * fmvr2st( gpr);
 */
#define fpx_fmvr2st_f __cmodel_fpx_fmvr2st
#define fpx_fmvr2st(a) { \
  __cmodel_fpx_fmvr2st(a); \
}

/*  fmvr2s gpr
 * fmvr2s( gpr);
 */
#define fpx_fmvr2s_f __cmodel_fpx_fmvr2s
#define fpx_fmvr2s(a) { \
  __cmodel_fpx_fmvr2s(a); \
}

/*  fmvs2r gpr
 * gpr fmvs2r( );
 */
#define fpx_fmvs2r_f __cmodel_fpx_fmvs2r
#define fpx_fmvs2r(a) { \
  a = __cmodel_fpx_fmvs2r(); \
}

/*  fneg fpr , fpr
 * fpr fneg( fpr);
 */
#define fpx_fneg_f __cmodel_fpx_fneg
#define fpx_fneg(a, b) { \
  a = __cmodel_fpx_fneg(b); \
}

/*  fscalb fpr , fpr , gpr
 * fpr fscalb( fpr, gpr);
 */
#define fpx_fscalb_f __cmodel_fpx_fscalb
#define fpx_fscalb(a, b, c) { \
  a = __cmodel_fpx_fscalb(b, c); \
}

/*  fsqrt fpr , fpr
 * fpr fsqrt( fpr);
 */
#define fpx_fsqrt_f __cmodel_fpx_fsqrt
#define fpx_fsqrt(a, b) { \
  a = __cmodel_fpx_fsqrt(b); \
}

/*  fsub fpr , fpr , fpr
 * fpr fsub( fpr, fpr);
 */
#define fpx_fsub_f __cmodel_fpx_fsub
#define fpx_fsub(a, b, c) { \
  a = __cmodel_fpx_fsub(b, c); \
}

/*  fu2f fpr , gpr
 * fpr fu2f( gpr);
 */
#define fpx_fu2f_r_f __cmodel_fpx_fu2f_r
#define fpx_fu2f_r(a, b) { \
  a = __cmodel_fpx_fu2f_r(b); \
}

/*  fu2f fpr , fpr
 * fpr fu2f( fpr);
 */
#define fpx_fu2f_f_f __cmodel_fpx_fu2f_f
#define fpx_fu2f_f(a, b) { \
  a = __cmodel_fpx_fu2f_f(b); \
}

/*  macn fpr , gpr , gpr
 * fpr macn( fpr, gpr, gpr);
 */
#define fpx_macn_f __cmodel_fpx_macn
#define fpx_macn(a, b, c) { \
  a = __cmodel_fpx_macn(a, b, c); \
}

/*  macnISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr macn( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
#define fpx_macnssll_f __cmodel_fpx_macnssll
#define fpx_macnssll(a, d, e) { \
  a = __cmodel_fpx_macnssll(c, d, e); \
}

#define fpx_macnsslh_f __cmodel_fpx_macnsslh
#define fpx_macnsslh(a, d, e) { \
  a = __cmodel_fpx_macnsslh(c, d, e); \
}

#define fpx_macnsshl_f __cmodel_fpx_macnsshl
#define fpx_macnsshl(a, d, e) { \
  a = __cmodel_fpx_macnsshl(c, d, e); \
}

#define fpx_macnsshh_f __cmodel_fpx_macnsshh
#define fpx_macnsshh(a, d, e) { \
  a = __cmodel_fpx_macnsshh(c, d, e); \
}

#define fpx_macnsull_f __cmodel_fpx_macnsull
#define fpx_macnsull(a, d, e) { \
  a = __cmodel_fpx_macnsull(c, d, e); \
}

#define fpx_macnsulh_f __cmodel_fpx_macnsulh
#define fpx_macnsulh(a, d, e) { \
  a = __cmodel_fpx_macnsulh(c, d, e); \
}

#define fpx_macnsuhl_f __cmodel_fpx_macnsuhl
#define fpx_macnsuhl(a, d, e) { \
  a = __cmodel_fpx_macnsuhl(c, d, e); \
}

#define fpx_macnsuhh_f __cmodel_fpx_macnsuhh
#define fpx_macnsuhh(a, d, e) { \
  a = __cmodel_fpx_macnsuhh(c, d, e); \
}

#define fpx_macnusll_f __cmodel_fpx_macnusll
#define fpx_macnusll(a, d, e) { \
  a = __cmodel_fpx_macnusll(c, d, e); \
}

#define fpx_macnuslh_f __cmodel_fpx_macnuslh
#define fpx_macnuslh(a, d, e) { \
  a = __cmodel_fpx_macnuslh(c, d, e); \
}

#define fpx_macnushl_f __cmodel_fpx_macnushl
#define fpx_macnushl(a, d, e) { \
  a = __cmodel_fpx_macnushl(c, d, e); \
}

#define fpx_macnushh_f __cmodel_fpx_macnushh
#define fpx_macnushh(a, d, e) { \
  a = __cmodel_fpx_macnushh(c, d, e); \
}

#define fpx_macnuull_f __cmodel_fpx_macnuull
#define fpx_macnuull(a, d, e) { \
  a = __cmodel_fpx_macnuull(c, d, e); \
}

#define fpx_macnuulh_f __cmodel_fpx_macnuulh
#define fpx_macnuulh(a, d, e) { \
  a = __cmodel_fpx_macnuulh(c, d, e); \
}

#define fpx_macnuuhl_f __cmodel_fpx_macnuuhl
#define fpx_macnuuhl(a, d, e) { \
  a = __cmodel_fpx_macnuuhl(c, d, e); \
}

#define fpx_macnuuhh_f __cmodel_fpx_macnuuhh
#define fpx_macnuuhh(a, d, e) { \
  a = __cmodel_fpx_macnuuhh(c, d, e); \
}

/*  mac fpr , gpr , gpr
 * fpr mac( fpr, gpr, gpr);
 */
#define fpx_mac_f __cmodel_fpx_mac
#define fpx_mac(a, b, c) { \
  a = __cmodel_fpx_mac(a, b, c); \
}

/*  macISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr mac( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
#define fpx_macssll_f __cmodel_fpx_macssll
#define fpx_macssll(a, d, e) { \
  a = __cmodel_fpx_macssll(c, d, e); \
}

#define fpx_macsslh_f __cmodel_fpx_macsslh
#define fpx_macsslh(a, d, e) { \
  a = __cmodel_fpx_macsslh(c, d, e); \
}

#define fpx_macsshl_f __cmodel_fpx_macsshl
#define fpx_macsshl(a, d, e) { \
  a = __cmodel_fpx_macsshl(c, d, e); \
}

#define fpx_macsshh_f __cmodel_fpx_macsshh
#define fpx_macsshh(a, d, e) { \
  a = __cmodel_fpx_macsshh(c, d, e); \
}

#define fpx_macsull_f __cmodel_fpx_macsull
#define fpx_macsull(a, d, e) { \
  a = __cmodel_fpx_macsull(c, d, e); \
}

#define fpx_macsulh_f __cmodel_fpx_macsulh
#define fpx_macsulh(a, d, e) { \
  a = __cmodel_fpx_macsulh(c, d, e); \
}

#define fpx_macsuhl_f __cmodel_fpx_macsuhl
#define fpx_macsuhl(a, d, e) { \
  a = __cmodel_fpx_macsuhl(c, d, e); \
}

#define fpx_macsuhh_f __cmodel_fpx_macsuhh
#define fpx_macsuhh(a, d, e) { \
  a = __cmodel_fpx_macsuhh(c, d, e); \
}

#define fpx_macusll_f __cmodel_fpx_macusll
#define fpx_macusll(a, d, e) { \
  a = __cmodel_fpx_macusll(c, d, e); \
}

#define fpx_macuslh_f __cmodel_fpx_macuslh
#define fpx_macuslh(a, d, e) { \
  a = __cmodel_fpx_macuslh(c, d, e); \
}

#define fpx_macushl_f __cmodel_fpx_macushl
#define fpx_macushl(a, d, e) { \
  a = __cmodel_fpx_macushl(c, d, e); \
}

#define fpx_macushh_f __cmodel_fpx_macushh
#define fpx_macushh(a, d, e) { \
  a = __cmodel_fpx_macushh(c, d, e); \
}

#define fpx_macuull_f __cmodel_fpx_macuull
#define fpx_macuull(a, d, e) { \
  a = __cmodel_fpx_macuull(c, d, e); \
}

#define fpx_macuulh_f __cmodel_fpx_macuulh
#define fpx_macuulh(a, d, e) { \
  a = __cmodel_fpx_macuulh(c, d, e); \
}

#define fpx_macuuhl_f __cmodel_fpx_macuuhl
#define fpx_macuuhl(a, d, e) { \
  a = __cmodel_fpx_macuuhl(c, d, e); \
}

#define fpx_macuuhh_f __cmodel_fpx_macuuhh
#define fpx_macuuhh(a, d, e) { \
  a = __cmodel_fpx_macuuhh(c, d, e); \
}

/*  modu fpr , gpr , gpr
 * fpr modu( gpr, gpr);
 */
#define fpx_modu_f __cmodel_fpx_modu
#define fpx_modu(a, b, c) { \
  a = __cmodel_fpx_modu(b, c); \
}

/*  mod fpr , gpr , gpr
 * fpr mod( gpr, gpr);
 */
#define fpx_mod_f __cmodel_fpx_mod
#define fpx_mod(a, b, c) { \
  a = __cmodel_fpx_mod(b, c); \
}

/*  mpnsshh gpr , gpr , gpr
 * gpr mpnsshh( gpr, gpr);
 */
#define fpx_mpnsshh_f __cmodel_fpx_mpnsshh
#define fpx_mpnsshh(a, b, c) { \
  a = __cmodel_fpx_mpnsshh(b, c); \
}

/*  mpnsshl gpr , gpr , gpr
 * gpr mpnsshl( gpr, gpr);
 */
#define fpx_mpnsshl_f __cmodel_fpx_mpnsshl
#define fpx_mpnsshl(a, b, c) { \
  a = __cmodel_fpx_mpnsshl(b, c); \
}

/*  mpnsslh gpr , gpr , gpr
 * gpr mpnsslh( gpr, gpr);
 */
#define fpx_mpnsslh_f __cmodel_fpx_mpnsslh
#define fpx_mpnsslh(a, b, c) { \
  a = __cmodel_fpx_mpnsslh(b, c); \
}

/*  mpnssll gpr , gpr , gpr
 * gpr mpnssll( gpr, gpr);
 */
#define fpx_mpnssll_f __cmodel_fpx_mpnssll
#define fpx_mpnssll(a, b, c) { \
  a = __cmodel_fpx_mpnssll(b, c); \
}

/*  mpnsuhh gpr , gpr , gpr
 * gpr mpnsuhh( gpr, gpr);
 */
#define fpx_mpnsuhh_f __cmodel_fpx_mpnsuhh
#define fpx_mpnsuhh(a, b, c) { \
  a = __cmodel_fpx_mpnsuhh(b, c); \
}

/*  mpnsuhl gpr , gpr , gpr
 * gpr mpnsuhl( gpr, gpr);
 */
#define fpx_mpnsuhl_f __cmodel_fpx_mpnsuhl
#define fpx_mpnsuhl(a, b, c) { \
  a = __cmodel_fpx_mpnsuhl(b, c); \
}

/*  mpnsulh gpr , gpr , gpr
 * gpr mpnsulh( gpr, gpr);
 */
#define fpx_mpnsulh_f __cmodel_fpx_mpnsulh
#define fpx_mpnsulh(a, b, c) { \
  a = __cmodel_fpx_mpnsulh(b, c); \
}

/*  mpnsull gpr , gpr , gpr
 * gpr mpnsull( gpr, gpr);
 */
#define fpx_mpnsull_f __cmodel_fpx_mpnsull
#define fpx_mpnsull(a, b, c) { \
  a = __cmodel_fpx_mpnsull(b, c); \
}

/*  mpnushh gpr , gpr , gpr
 * gpr mpnushh( gpr, gpr);
 */
#define fpx_mpnushh_f __cmodel_fpx_mpnushh
#define fpx_mpnushh(a, b, c) { \
  a = __cmodel_fpx_mpnushh(b, c); \
}

/*  mpnushl gpr , gpr , gpr
 * gpr mpnushl( gpr, gpr);
 */
#define fpx_mpnushl_f __cmodel_fpx_mpnushl
#define fpx_mpnushl(a, b, c) { \
  a = __cmodel_fpx_mpnushl(b, c); \
}

/*  mpnuslh gpr , gpr , gpr
 * gpr mpnuslh( gpr, gpr);
 */
#define fpx_mpnuslh_f __cmodel_fpx_mpnuslh
#define fpx_mpnuslh(a, b, c) { \
  a = __cmodel_fpx_mpnuslh(b, c); \
}

/*  mpnusll gpr , gpr , gpr
 * gpr mpnusll( gpr, gpr);
 */
#define fpx_mpnusll_f __cmodel_fpx_mpnusll
#define fpx_mpnusll(a, b, c) { \
  a = __cmodel_fpx_mpnusll(b, c); \
}

/*  mpnuuhh gpr , gpr , gpr
 * gpr mpnuuhh( gpr, gpr);
 */
#define fpx_mpnuuhh_f __cmodel_fpx_mpnuuhh
#define fpx_mpnuuhh(a, b, c) { \
  a = __cmodel_fpx_mpnuuhh(b, c); \
}

/*  mpnuuhl gpr , gpr , gpr
 * gpr mpnuuhl( gpr, gpr);
 */
#define fpx_mpnuuhl_f __cmodel_fpx_mpnuuhl
#define fpx_mpnuuhl(a, b, c) { \
  a = __cmodel_fpx_mpnuuhl(b, c); \
}

/*  mpnuulh gpr , gpr , gpr
 * gpr mpnuulh( gpr, gpr);
 */
#define fpx_mpnuulh_f __cmodel_fpx_mpnuulh
#define fpx_mpnuulh(a, b, c) { \
  a = __cmodel_fpx_mpnuulh(b, c); \
}

/*  mpnuull gpr , gpr , gpr
 * gpr mpnuull( gpr, gpr);
 */
#define fpx_mpnuull_f __cmodel_fpx_mpnuull
#define fpx_mpnuull(a, b, c) { \
  a = __cmodel_fpx_mpnuull(b, c); \
}

/*  mpn gpr , gpr , gpr
 * gpr mpn( gpr, gpr);
 */
#define fpx_mpn_f __cmodel_fpx_mpn
#define fpx_mpn(a, b, c) { \
  a = __cmodel_fpx_mpn(b, c); \
}

/*  mpsshh gpr , gpr , gpr
 * gpr mpsshh( gpr, gpr);
 */
#define fpx_mpsshh_f __cmodel_fpx_mpsshh
#define fpx_mpsshh(a, b, c) { \
  a = __cmodel_fpx_mpsshh(b, c); \
}

/*  mpsshl gpr , gpr , gpr
 * gpr mpsshl( gpr, gpr);
 */
#define fpx_mpsshl_f __cmodel_fpx_mpsshl
#define fpx_mpsshl(a, b, c) { \
  a = __cmodel_fpx_mpsshl(b, c); \
}

/*  mpsslh gpr , gpr , gpr
 * gpr mpsslh( gpr, gpr);
 */
#define fpx_mpsslh_f __cmodel_fpx_mpsslh
#define fpx_mpsslh(a, b, c) { \
  a = __cmodel_fpx_mpsslh(b, c); \
}

/*  mpssll gpr , gpr , gpr
 * gpr mpssll( gpr, gpr);
 */
#define fpx_mpssll_f __cmodel_fpx_mpssll
#define fpx_mpssll(a, b, c) { \
  a = __cmodel_fpx_mpssll(b, c); \
}

/*  mpsuhh gpr , gpr , gpr
 * gpr mpsuhh( gpr, gpr);
 */
#define fpx_mpsuhh_f __cmodel_fpx_mpsuhh
#define fpx_mpsuhh(a, b, c) { \
  a = __cmodel_fpx_mpsuhh(b, c); \
}

/*  mpsuhl gpr , gpr , gpr
 * gpr mpsuhl( gpr, gpr);
 */
#define fpx_mpsuhl_f __cmodel_fpx_mpsuhl
#define fpx_mpsuhl(a, b, c) { \
  a = __cmodel_fpx_mpsuhl(b, c); \
}

/*  mpsulh gpr , gpr , gpr
 * gpr mpsulh( gpr, gpr);
 */
#define fpx_mpsulh_f __cmodel_fpx_mpsulh
#define fpx_mpsulh(a, b, c) { \
  a = __cmodel_fpx_mpsulh(b, c); \
}

/*  mpsull gpr , gpr , gpr
 * gpr mpsull( gpr, gpr);
 */
#define fpx_mpsull_f __cmodel_fpx_mpsull
#define fpx_mpsull(a, b, c) { \
  a = __cmodel_fpx_mpsull(b, c); \
}

/*  mpushh gpr , gpr , gpr
 * gpr mpushh( gpr, gpr);
 */
#define fpx_mpushh_f __cmodel_fpx_mpushh
#define fpx_mpushh(a, b, c) { \
  a = __cmodel_fpx_mpushh(b, c); \
}

/*  mpushl gpr , gpr , gpr
 * gpr mpushl( gpr, gpr);
 */
#define fpx_mpushl_f __cmodel_fpx_mpushl
#define fpx_mpushl(a, b, c) { \
  a = __cmodel_fpx_mpushl(b, c); \
}

/*  mpuslh gpr , gpr , gpr
 * gpr mpuslh( gpr, gpr);
 */
#define fpx_mpuslh_f __cmodel_fpx_mpuslh
#define fpx_mpuslh(a, b, c) { \
  a = __cmodel_fpx_mpuslh(b, c); \
}

/*  mpusll gpr , gpr , gpr
 * gpr mpusll( gpr, gpr);
 */
#define fpx_mpusll_f __cmodel_fpx_mpusll
#define fpx_mpusll(a, b, c) { \
  a = __cmodel_fpx_mpusll(b, c); \
}

/*  mpuuhh gpr , gpr , gpr
 * gpr mpuuhh( gpr, gpr);
 */
#define fpx_mpuuhh_f __cmodel_fpx_mpuuhh
#define fpx_mpuuhh(a, b, c) { \
  a = __cmodel_fpx_mpuuhh(b, c); \
}

/*  mpuuhl gpr , gpr , gpr
 * gpr mpuuhl( gpr, gpr);
 */
#define fpx_mpuuhl_f __cmodel_fpx_mpuuhl
#define fpx_mpuuhl(a, b, c) { \
  a = __cmodel_fpx_mpuuhl(b, c); \
}

/*  mpuulh gpr , gpr , gpr
 * gpr mpuulh( gpr, gpr);
 */
#define fpx_mpuulh_f __cmodel_fpx_mpuulh
#define fpx_mpuulh(a, b, c) { \
  a = __cmodel_fpx_mpuulh(b, c); \
}

/*  mpuull gpr , gpr , gpr
 * gpr mpuull( gpr, gpr);
 */
#define fpx_mpuull_f __cmodel_fpx_mpuull
#define fpx_mpuull(a, b, c) { \
  a = __cmodel_fpx_mpuull(b, c); \
}

/*  mp gpr , gpr , gpr
 * gpr mp( gpr, gpr);
 */
#define fpx_mp_f __cmodel_fpx_mp
#define fpx_mp(a, b, c) { \
  a = __cmodel_fpx_mp(b, c); \
}

#endif /* 0 */


#endif /* defined(__FPX) && !defined(__FPX_C_MODEL) */
#endif /* _FPX_H_ */
