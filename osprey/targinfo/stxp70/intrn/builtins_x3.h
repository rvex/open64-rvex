/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */


/* Note on dedicated registers: as these registers are not allocated by the compiler,
 * it is necessary to pass the index of such register. When it is passed as builtin
 * operand, an immediate of the form 'u[0..n]' is expected in place of dedicated register.
 * 'n' is the size of the register file minus one.
 * When dedicated register is the result, one additional operand of the same form 'u[0..n]'
 * is placed as first parameter to select the result register index.
 *
 * Note on immediates: some builtins may accept constant value as parameter. Programmer must
 * respect the immediate range given by the bits number and signess.
 * Immediates are given under the form '[su][0-9]+'
 * - [su] for signed and unsigned respectively.
 * - [0-9]+ the bits number associated to the immediate.
 */


/* Builtins for class x3 */
#ifdef __X3

#ifndef _BUILTIN_X3_H_
#define _BUILTIN_X3_H_


#include "builtins_x3_compatibility.h"

#ifndef _BUILTIN_INTRN_ENUM_H_
#define _BUILTIN_INTRN_ENUM_H_

enum intrn_bb { 	intrn_bb_b0 = 0 ,intrn_bb_b1 = 1 ,intrn_bb_b2 = 2 ,intrn_bb_b3 = 3};

enum intrn_sg { 	intrn_sg_ss = 0 ,intrn_sg_su = 1 ,intrn_sg_us = 2 ,intrn_sg_uu = 3};

enum intrn_mo { 	intrn_mo_ll = 0 ,intrn_mo_lh = 1 ,intrn_mo_hl = 2 ,intrn_mo_hh = 3};

enum intrn_cmp { 	intrn_cmp_eq = 0 ,intrn_cmp_ne = 1 ,intrn_cmp_ge = 2 ,intrn_cmp_lt = 3 ,intrn_cmp_le = 4 ,intrn_cmp_gt = 5};

enum intrn_btest { 	intrn_btest_f = 0 ,intrn_btest_t = 1};

enum intrn_bitop { 	intrn_bitop_clr = 0 ,intrn_bitop_set = 1 ,intrn_bitop_not = 2};

enum intrn_cp { 	intrn_cp_eq = 0 ,intrn_cp_ne = 1 ,intrn_cp_ge = 2 ,intrn_cp_lt = 3 ,intrn_cp_le = 4 ,intrn_cp_gt = 5 ,intrn_cp_un = 6};

enum intrn_sg_v4 { 	intrn_sg_v4_uu = 1 ,intrn_sg_v4_su = 2 ,intrn_sg_v4_ss = 3};

#endif // _BUILTIN_INTRN_ENUM_H_

#ifdef __STxP70_V4__
/* dflushm
 * dflushm( );
 */
extern void __builtin_x3_dflushm(void);

/* dflush @(gpr -! <u12>shl<scf>)
 * gpr dflush( gpr, <u12>, <scf>);
 */
extern int __builtin_x3_dflush_i3_i12_pre_dec_r(int, unsigned short, unsigned short);

/* dflush @(gpr !- <u13>shl<scf>)
 * gpr dflush( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dflush_i3_i13_post_dec_r(int, unsigned short, unsigned short);

/* dflush @(gpr - <u12>shl<scf>)
 * dflush( gpr, <u12>, <scf>);
 */
extern void __builtin_x3_dflush_i3_i12_dec_r(int, unsigned short, unsigned short);

/* dflush @(gpr !+ gpr)
 * gpr dflush( gpr, gpr);
 */
extern int __builtin_x3_dflush_r_post_inc_r(int, int);

/* dflush @(gpr !+ <u13>shl<scf>)
 * gpr dflush( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dflush_i3_i13_post_inc_r(int, unsigned short, unsigned short);

/* dflush @(gpr + gpr)
 * dflush( gpr, gpr);
 */
extern void __builtin_x3_dflush_r_inc_r(int, int);

/* dflush @(gpr + <u15>shl<scf>)
 * dflush( gpr, <u15>, <scf>);
 */
extern void __builtin_x3_dflush_i3_i15_inc_r(int, unsigned short, unsigned short);

/* dinval @(gpr -! <u12>shl<scf>)
 * gpr dinval( gpr, <u12>, <scf>);
 */
extern int __builtin_x3_dinval_i3_i12_pre_dec_r(int, unsigned short, unsigned short);

/* dinval @(gpr !- <u13>shl<scf>)
 * gpr dinval( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dinval_i3_i13_post_dec_r(int, unsigned short, unsigned short);

/* dinval @(gpr - <u12>shl<scf>)
 * dinval( gpr, <u12>, <scf>);
 */
extern void __builtin_x3_dinval_i3_i12_dec_r(int, unsigned short, unsigned short);

/* dinval @(gpr !+ gpr)
 * gpr dinval( gpr, gpr);
 */
extern int __builtin_x3_dinval_r_post_inc_r(int, int);

/* dinval @(gpr !+ <u13>shl<scf>)
 * gpr dinval( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dinval_i3_i13_post_inc_r(int, unsigned short, unsigned short);

/* dinval @(gpr + gpr)
 * dinval( gpr, gpr);
 */
extern void __builtin_x3_dinval_r_inc_r(int, int);

/* dinval @(gpr + <u15>shl<scf>)
 * dinval( gpr, <u15>, <scf>);
 */
extern void __builtin_x3_dinval_i3_i15_inc_r(int, unsigned short, unsigned short);

/* dlock @(gpr -! <u12>shl<scf>)
 * gpr dlock( gpr, <u12>, <scf>);
 */
extern int __builtin_x3_dlock_i3_i12_pre_dec_r(int, unsigned short, unsigned short);

/* dlock @(gpr !- <u13>shl<scf>)
 * gpr dlock( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dlock_i3_i13_post_dec_r(int, unsigned short, unsigned short);

/* dlock @(gpr - <u12>shl<scf>)
 * dlock( gpr, <u12>, <scf>);
 */
extern void __builtin_x3_dlock_i3_i12_dec_r(int, unsigned short, unsigned short);

/* dlock @(gpr !+ gpr)
 * gpr dlock( gpr, gpr);
 */
extern int __builtin_x3_dlock_r_post_inc_r(int, int);

/* dlock @(gpr !+ <u13>shl<scf>)
 * gpr dlock( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dlock_i3_i13_post_inc_r(int, unsigned short, unsigned short);

/* dlock @(gpr + gpr)
 * dlock( gpr, gpr);
 */
extern void __builtin_x3_dlock_r_inc_r(int, int);

/* dlock @(gpr + <u15>shl<scf>)
 * dlock( gpr, <u15>, <scf>);
 */
extern void __builtin_x3_dlock_i3_i15_inc_r(int, unsigned short, unsigned short);

/* dstata @(gpr -! <u12>shl<scf>)
 * gpr dstata( gpr, <u12>, <scf>);
 */
extern int __builtin_x3_dstata_i3_i12_pre_dec_r(int, unsigned short, unsigned short);

/* dstata @(gpr !- <u13>shl<scf>)
 * gpr dstata( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dstata_i3_i13_post_dec_r(int, unsigned short, unsigned short);

/* dstata @(gpr - <u12>shl<scf>)
 * dstata( gpr, <u12>, <scf>);
 */
extern void __builtin_x3_dstata_i3_i12_dec_r(int, unsigned short, unsigned short);

/* dstata @(gpr !+ gpr)
 * gpr dstata( gpr, gpr);
 */
extern int __builtin_x3_dstata_r_post_inc_r(int, int);

/* dstata @(gpr !+ <u13>shl<scf>)
 * gpr dstata( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dstata_i3_i13_post_inc_r(int, unsigned short, unsigned short);

/* dstata @(gpr + gpr)
 * dstata( gpr, gpr);
 */
extern void __builtin_x3_dstata_r_inc_r(int, int);

/* dstata @(gpr + <u15>shl<scf>)
 * dstata( gpr, <u15>, <scf>);
 */
extern void __builtin_x3_dstata_i3_i15_inc_r(int, unsigned short, unsigned short);

/* dtouch @(gpr -! <u12>shl<scf>)
 * gpr dtouch( gpr, <u12>, <scf>);
 */
extern int __builtin_x3_dtouch_i3_i12_pre_dec_r(int, unsigned short, unsigned short);

/* dtouch @(gpr !- <u13>shl<scf>)
 * gpr dtouch( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dtouch_i3_i13_post_dec_r(int, unsigned short, unsigned short);

/* dtouch @(gpr - <u12>shl<scf>)
 * dtouch( gpr, <u12>, <scf>);
 */
extern void __builtin_x3_dtouch_i3_i12_dec_r(int, unsigned short, unsigned short);

/* dtouch @(gpr !+ gpr)
 * gpr dtouch( gpr, gpr);
 */
extern int __builtin_x3_dtouch_r_post_inc_r(int, int);

/* dtouch @(gpr !+ <u13>shl<scf>)
 * gpr dtouch( gpr, <u13>, <scf>);
 */
extern int __builtin_x3_dtouch_i3_i13_post_inc_r(int, unsigned short, unsigned short);

/* dtouch @(gpr + gpr)
 * dtouch( gpr, gpr);
 */
extern void __builtin_x3_dtouch_r_inc_r(int, int);

/* dtouch @(gpr + <u15>shl<scf>)
 * dtouch( gpr, <u15>, <scf>);
 */
extern void __builtin_x3_dtouch_i3_i15_inc_r(int, unsigned short, unsigned short);

#endif

/*  cancelg gpr
 * cancelg( gpr);
 */
extern void __builtin_x3_cancelg_r_g(int);

/*  cancelg ISA_EC_bb, <u8>
 * cancelg( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_cancelg_i8_i2_g(int, unsigned short);

/*  cancell gpr
 * cancell( gpr);
 */
extern void __builtin_x3_cancell_r_g(int);

/*  cancell ISA_EC_bb, <u8>
 * cancell( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_cancell_i8_i2_g(int, unsigned short);

/* clrcc1
 * clrcc1( );
 */
extern void __builtin_x3_clrcc1(void);

/* clrcca
 * clrcca( );
 */
extern void __builtin_x3_clrcca(void);

/* clrcc
 * clrcc( );
 */
extern void __builtin_x3_clrcc(void);

/*  clric itcr, gpr
 * itcr clric( u[0..31], u[0..31], gpr);
 */
extern void __builtin_x3_clric(unsigned, int);

/*  clrie gpr
 * clrie( gpr);
 */
extern void __builtin_x3_clrie(int);

/* dflushw
 * dflushw( );
 */
extern void __builtin_x3_dflushw(void);

#ifdef __STxP70_V3__
/* dflush gpr
 * dflush( gpr);
 */
extern void __builtin_x3_dflush(int);

/* dinval gpr
 * dinval( gpr);
 */
extern void __builtin_x3_dinval(int);

/* dlock gpr
 * dlock( gpr);
 */
extern void __builtin_x3_dlock(int);

/* dstata gpr
 * dstata( gpr);
 */
extern void __builtin_x3_dstata(int);

#endif

/* dstatwi gpr
 * dstatwi( gpr);
 */
extern void __builtin_x3_dstatwi(int);

#ifdef __STxP70_V3__
/* dtouch gpr
 * dtouch( gpr);
 */
extern void __builtin_x3_dtouch(int);

#endif

/*  fork<u3> gpr, gpr, <u1>
 * fork( <u3>, gpr, gpr, <u1>);
 */
extern void __builtin_x3_fork_i3_g(unsigned short, int, int, unsigned short);

/*  fork gpr, gpr, <u1>
 * fork( gpr, gpr, <u1>);
 */
extern void __builtin_x3_fork_g(int, int, unsigned short);

/*  movee2r gpr, gecr
 * gpr movee2r( u[0..31]);
 */
extern int __builtin_x3_movee2r(unsigned);

/*  moveic2ri gpr, gpr
 * gpr moveic2ri( gpr);
 */
extern int __builtin_x3_moveic2ri(int);

/*  moveic2r gpr, itcr
 * gpr moveic2r( u[0..31]);
 */
extern int __builtin_x3_moveic2r(unsigned);

/*  moveie2r gpr
 * gpr moveie2r( );
 */
extern int __builtin_x3_moveie2r(void);

/*  mover2e gecr, gpr
 * gecr mover2e( u[0..31], gpr);
 */
extern void __builtin_x3_mover2e(unsigned, int);

/*  mover2ici gpr, gpr
 * mover2ici( gpr, gpr);
 */
extern void __builtin_x3_mover2ici(int, int);

/*  mover2ic itcr, gpr
 * itcr mover2ic( u[0..31], gpr);
 */
extern void __builtin_x3_mover2ic(unsigned, int);

/*  mover2ie gpr
 * mover2ie( gpr);
 */
extern void __builtin_x3_mover2ie(int);

/*  mpnsshh gpr, gpr, gpr
 * gpr mpnsshh( gpr, gpr);
 */
extern int __builtin_x3_mpnsshh(int, int);

/*  mpnsshl gpr, gpr, gpr
 * gpr mpnsshl( gpr, gpr);
 */
extern int __builtin_x3_mpnsshl(int, int);

/*  mpnsslh gpr, gpr, gpr
 * gpr mpnsslh( gpr, gpr);
 */
extern int __builtin_x3_mpnsslh(int, int);

/*  mpnssll gpr, gpr, gpr
 * gpr mpnssll( gpr, gpr);
 */
extern int __builtin_x3_mpnssll(int, int);

/*  mpnsuhh gpr, gpr, gpr
 * gpr mpnsuhh( gpr, gpr);
 */
extern int __builtin_x3_mpnsuhh(int, unsigned);

/*  mpnsuhl gpr, gpr, gpr
 * gpr mpnsuhl( gpr, gpr);
 */
extern int __builtin_x3_mpnsuhl(int, unsigned);

/*  mpnsulh gpr, gpr, gpr
 * gpr mpnsulh( gpr, gpr);
 */
extern int __builtin_x3_mpnsulh(int, unsigned);

/*  mpnsull gpr, gpr, gpr
 * gpr mpnsull( gpr, gpr);
 */
extern int __builtin_x3_mpnsull(int, unsigned);

#ifdef __STxP70_V3__
/*  mpnushh gpr, gpr, gpr
 * gpr mpnushh( gpr, gpr);
 */
extern int __builtin_x3_mpnushh(unsigned, int);

/*  mpnushl gpr, gpr, gpr
 * gpr mpnushl( gpr, gpr);
 */
extern int __builtin_x3_mpnushl(unsigned, int);

/*  mpnuslh gpr, gpr, gpr
 * gpr mpnuslh( gpr, gpr);
 */
extern int __builtin_x3_mpnuslh(unsigned, int);

/*  mpnusll gpr, gpr, gpr
 * gpr mpnusll( gpr, gpr);
 */
extern int __builtin_x3_mpnusll(unsigned, int);

#endif

/*  mpnuuhh gpr, gpr, gpr
 * gpr mpnuuhh( gpr, gpr);
 */
extern int __builtin_x3_mpnuuhh(unsigned, unsigned);

/*  mpnuuhl gpr, gpr, gpr
 * gpr mpnuuhl( gpr, gpr);
 */
extern int __builtin_x3_mpnuuhl(unsigned, unsigned);

/*  mpnuulh gpr, gpr, gpr
 * gpr mpnuulh( gpr, gpr);
 */
extern int __builtin_x3_mpnuulh(unsigned, unsigned);

/*  mpnuull gpr, gpr, gpr
 * gpr mpnuull( gpr, gpr);
 */
extern int __builtin_x3_mpnuull(unsigned, unsigned);

/*  mpn gpr, gpr, gpr
 * gpr mpn( gpr, gpr);
 */
extern int __builtin_x3_mpn(int, int);

/*  mpsshh gpr, gpr, gpr
 * gpr mpsshh( gpr, gpr);
 */
extern int __builtin_x3_mpsshh(int, int);

/*  mpsshl gpr, gpr, gpr
 * gpr mpsshl( gpr, gpr);
 */
extern int __builtin_x3_mpsshl(int, int);

/*  mpsslh gpr, gpr, gpr
 * gpr mpsslh( gpr, gpr);
 */
extern int __builtin_x3_mpsslh(int, int);

/*  mpssll gpr, gpr, gpr
 * gpr mpssll( gpr, gpr);
 */
extern int __builtin_x3_mpssll(int, int);

/*  mpsuhh gpr, gpr, gpr
 * gpr mpsuhh( gpr, gpr);
 */
extern int __builtin_x3_mpsuhh(int, unsigned);

/*  mpsuhl gpr, gpr, gpr
 * gpr mpsuhl( gpr, gpr);
 */
extern int __builtin_x3_mpsuhl(int, unsigned);

/*  mpsulh gpr, gpr, gpr
 * gpr mpsulh( gpr, gpr);
 */
extern int __builtin_x3_mpsulh(int, unsigned);

/*  mpsull gpr, gpr, gpr
 * gpr mpsull( gpr, gpr);
 */
extern int __builtin_x3_mpsull(int, unsigned);

#ifdef __STxP70_V3__
/*  mpushh gpr, gpr, gpr
 * gpr mpushh( gpr, gpr);
 */
extern int __builtin_x3_mpushh(unsigned, int);

/*  mpushl gpr, gpr, gpr
 * gpr mpushl( gpr, gpr);
 */
extern int __builtin_x3_mpushl(unsigned, int);

/*  mpuslh gpr, gpr, gpr
 * gpr mpuslh( gpr, gpr);
 */
extern int __builtin_x3_mpuslh(unsigned, int);

/*  mpusll gpr, gpr, gpr
 * gpr mpusll( gpr, gpr);
 */
extern int __builtin_x3_mpusll(unsigned, int);

#endif

/*  mpuuhh gpr, gpr, gpr
 * gpr mpuuhh( gpr, gpr);
 */
extern int __builtin_x3_mpuuhh(unsigned, unsigned);

/*  mpuuhl gpr, gpr, gpr
 * gpr mpuuhl( gpr, gpr);
 */
extern int __builtin_x3_mpuuhl(unsigned, unsigned);

/*  mpuulh gpr, gpr, gpr
 * gpr mpuulh( gpr, gpr);
 */
extern int __builtin_x3_mpuulh(unsigned, unsigned);

/*  mpuull gpr, gpr, gpr
 * gpr mpuull( gpr, gpr);
 */
extern int __builtin_x3_mpuull(unsigned, unsigned);

/*  mp gpr, gpr, gpr
 * gpr mp( gpr, gpr);
 */
extern int __builtin_x3_mp(int, int);

/*  notifyg gpr
 * notifyg( gpr);
 */
extern void __builtin_x3_notifyg_r_g(int);

/*  notifyg ISA_EC_bb, <u8>
 * notifyg( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_notifyg_i8_i2_g(int, unsigned short);

/*  notifyla gpr
 * notifyla( gpr);
 */
extern void __builtin_x3_notifyla_r_g(int);

/*  notifyla ISA_EC_bb, <u8>
 * notifyla( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_notifyla_i8_i2_g(int, unsigned short);

/*  notifyl<u3> gpr
 * notifyl( <u3>, gpr);
 */
extern void __builtin_x3_notifyl_r_i3_g(unsigned short, int);

/*  notifyl<u3> ISA_EC_bb, <u8>
 * notifyl( <u3>, ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_notifyl_i8_i2_i3_g(unsigned short, int, unsigned short);

/* pflushw
 * pflushw( );
 */
extern void __builtin_x3_pflushw(void);

/* pinvalr gpr
 * pinvalr( gpr);
 */
extern void __builtin_x3_pinvalr(int);

/* pinval gpr
 * pinval( gpr);
 */
extern void __builtin_x3_pinval(int);

/* plockr gpr
 * plockr( gpr);
 */
extern void __builtin_x3_plockr(int);

/* plock gpr
 * plock( gpr);
 */
extern void __builtin_x3_plock(int);

/* pstata gpr
 * pstata( gpr);
 */
extern void __builtin_x3_pstata(int);

/* pstatwi gpr
 * pstatwi( gpr);
 */
extern void __builtin_x3_pstatwi(int);

/* ptouch gpr
 * ptouch( gpr);
 */
extern void __builtin_x3_ptouch(int);

/*  setic itcr, gpr
 * itcr setic( u[0..31], gpr);
 */
extern void __builtin_x3_setic(unsigned, int);

/*  setie gpr
 * setie( gpr);
 */
extern void __builtin_x3_setie(int);

/* startcc1
 * startcc1( );
 */
extern void __builtin_x3_startcc1(void);

/* startcca
 * startcca( );
 */
extern void __builtin_x3_startcca(void);

/* startcc
 * startcc( );
 */
extern void __builtin_x3_startcc(void);

/* stopcc1
 * stopcc1( );
 */
extern void __builtin_x3_stopcc1(void);

/* stopcca
 * stopcca( );
 */
extern void __builtin_x3_stopcca(void);

/* stopcc
 * stopcc( );
 */
extern void __builtin_x3_stopcc(void);

/* terminate<u3>
 * terminate( <u3>);
 */
extern void __builtin_x3_terminate_i3(unsigned short);

/* terminate
 * terminate( );
 */
extern void __builtin_x3_terminate(void);

/*  waitg gpr
 * waitg( gpr);
 */
extern void __builtin_x3_waitg_r_g(int);

/*  waitg ISA_EC_bb, <u8>
 * waitg( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_waitg_i8_i2_g(int, unsigned short);

/*  waitl gpr
 * waitl( gpr);
 */
extern void __builtin_x3_waitl_r_g(int);

/*  waitl ISA_EC_bb, <u8>
 * waitl( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_waitl_i8_i2_g(int, unsigned short);

/*  waitnbg gpr
 * waitnbg( gpr);
 */
extern void __builtin_x3_waitnbg_r_g(int);

/*  waitnbg ISA_EC_bb, <u8>
 * waitnbg( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_waitnbg_i8_i2_g(int, unsigned short);

/*  waitnbl gpr
 * waitnbl( gpr);
 */
extern void __builtin_x3_waitnbl_r_g(int);

/*  waitnbl ISA_EC_bb, <u8>
 * waitnbl( ISA_EC_bb, <u8>);
 */
extern void __builtin_x3_waitnbl_i8_i2_g(int, unsigned short);

/*  waitnb gpr, gpr
 * waitnb( gpr, gpr);
 */
extern void __builtin_x3_waitnb(int, int);

/*  wait gpr, gpr
 * wait( gpr, gpr);
 */
extern void __builtin_x3_wait(int, int);


#endif /* _BUILTIN_X3_H_ */
#endif /* __X3 */
