/*
 *      Copyright 2005, STMicroelectronics, Incorporated.
 *      All rights reserved.
 *
 * STMICROELECTRONICS, INCORPORATED PROPRIETARY INFORMATION
 * This software is supplied under the terms of a license agreement
 * or nondisclosure agreement with STMicroelectronics and may not be
 * copied or disclosed except in accordance with the terms of that
 * agreement.
 */


/* Note on dedicated registers: as these registers are not allocated by the compiler,
 * it is necessary to pass the index of such register. When it is passed as builtin
 * operand, an immediate of the form 'u[0..n]' is expected in place of dedicated register.
 * 'n' is the size of the register file minus one.
 * When dedicated register is the result, one additional operand of the same form 'u[0..n]'
 * is placed as first parameter to select the result register index.
 *
 * Note on immediates: some builtins may accept constant value as parameter. Programmer must
 * respect the immediate range given by the bits number and signess.
 * Immediates are given under the form '[su][0-9]+'
 * - [su] for signed and unsigned respectively.
 * - [0-9]+ the bits number associated to the immediate.
 */


/* Builtins for class fpx */
#ifdef __FPX

#ifndef _BUILTIN_FPX_H_
#define _BUILTIN_FPX_H_


#ifndef _BUILTIN_INTRN_ENUM_H_
#define _BUILTIN_INTRN_ENUM_H_

enum intrn_bb { 	intrn_bb_b0 = 0 ,intrn_bb_b1 = 1 ,intrn_bb_b2 = 2 ,intrn_bb_b3 = 3};

enum intrn_sg { 	intrn_sg_ss = 0 ,intrn_sg_su = 1 ,intrn_sg_us = 2 ,intrn_sg_uu = 3};

enum intrn_mo { 	intrn_mo_ll = 0 ,intrn_mo_lh = 1 ,intrn_mo_hl = 2 ,intrn_mo_hh = 3};

enum intrn_cmp { 	intrn_cmp_eq = 0 ,intrn_cmp_ne = 1 ,intrn_cmp_ge = 2 ,intrn_cmp_lt = 3 ,intrn_cmp_le = 4 ,intrn_cmp_gt = 5};

enum intrn_btest { 	intrn_btest_f = 0 ,intrn_btest_t = 1};

enum intrn_bitop { 	intrn_bitop_clr = 0 ,intrn_bitop_set = 1 ,intrn_bitop_not = 2};

enum intrn_cp { 	intrn_cp_eq = 0 ,intrn_cp_ne = 1 ,intrn_cp_ge = 2 ,intrn_cp_lt = 3 ,intrn_cp_le = 4 ,intrn_cp_gt = 5 ,intrn_cp_un = 6};

enum intrn_sg_v4 { 	intrn_sg_v4_uu = 1 ,intrn_sg_v4_su = 2 ,intrn_sg_v4_ss = 3};

#endif // _BUILTIN_INTRN_ENUM_H_

#ifdef __STxP70_V3__
/*  divu fpr , gpr , gpr
 * fpr divu( gpr, gpr);
 */
extern unsigned __builtin_fpx_divu(unsigned, unsigned);

/*  div fpr , gpr , gpr
 * fpr div( gpr, gpr);
 */
extern int __builtin_fpx_div(int, int);

/*  fabs fpr , fpr
 * fpr fabs( fpr);
 */
extern float __builtin_fpx_fabs(float);

/*  faddaa fpr , fpr , fpr
 * fpr faddaa( fpr, fpr);
 */
extern float __builtin_fpx_faddaa(float, float);

/*  faddn fpr , fpr , fpr
 * fpr faddn( fpr, fpr);
 */
extern float __builtin_fpx_faddn(float, float);

/*  fadd fpr , fpr , fpr
 * fpr fadd( fpr, fpr);
 */
extern float __builtin_fpx_fadd(float, float);

/*  fasub fpr , fpr , fpr
 * fpr fasub( fpr, fpr);
 */
extern float __builtin_fpx_fasub(float, float);

/*  fclrs <u6>
 * fclrs( <u6>);
 */
extern void __builtin_fpx_fclrs(unsigned short);

/*  fcmpuISA_EC_cp gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
extern int __builtin_fpx_fcmpu_cp_g(int, float, float);

#define __builtin_fpx_fcmpueq(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_eq, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpune(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_ne, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpuge(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_ge, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpult(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_lt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpule(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_le, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpugt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_gt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpuun(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_cp_g(intrn_cp_un, c, d); res; \
	} ) 

/*  fcmpuISA_EC_cpN gpr , fpr , fpr
 * gpr fcmpu( ISA_EC_cp, fpr, fpr);
 */
extern int __builtin_fpx_fcmpu_n_cp_g(int, float, float);

#define __builtin_fpx_fcmpu_neq(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_eq, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_nne(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_ne, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_nge(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_ge, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_nlt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_lt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_nle(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_le, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_ngt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_gt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpu_nun(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmpu_n_cp_g(intrn_cp_un, c, d); res; \
	} ) 

/*  fcmpISA_EC_cp gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
extern int __builtin_fpx_fcmp_cp_g(int, float, float);

#define __builtin_fpx_fcmpeq(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_eq, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpne(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_ne, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpge(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_ge, c, d); res; \
	} ) 

#define __builtin_fpx_fcmplt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_lt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmple(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_le, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpgt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_gt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmpun(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_cp_g(intrn_cp_un, c, d); res; \
	} ) 

/*  fcmpISA_EC_cpN gpr , fpr , fpr
 * gpr fcmp( ISA_EC_cp, fpr, fpr);
 */
extern int __builtin_fpx_fcmp_n_cp_g(int, float, float);

#define __builtin_fpx_fcmp_neq(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_eq, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_nne(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_ne, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_nge(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_ge, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_nlt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_lt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_nle(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_le, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_ngt(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_gt, c, d); res; \
	} ) 

#define __builtin_fpx_fcmp_nun(c, d) ( { \
	int res; \
	 res = __builtin_fpx_fcmp_n_cp_g(intrn_cp_un, c, d); res; \
	} ) 

/*  fcnst fpr , <u9>
 * fpr fcnst( <u9>);
 */
extern float __builtin_fpx_fcnst(unsigned short);

/*  fdiv fpr , fpr , fpr
 * fpr fdiv( fpr, fpr);
 */
extern float __builtin_fpx_fdiv(float, float);

/*  ff2in gpr , fpr
 * gpr ff2in( fpr);
 */
extern int __builtin_fpx_ff2in_r(float);

/*  ff2in fpr , fpr
 * fpr ff2in( fpr);
 */
extern int __builtin_fpx_ff2in_f(float);

/*  ff2i gpr , fpr
 * gpr ff2i( fpr);
 */
extern int __builtin_fpx_ff2i_r(float);

/*  ff2i fpr , fpr
 * fpr ff2i( fpr);
 */
extern int __builtin_fpx_ff2i_f(float);

/*  ff2un gpr , fpr
 * gpr ff2un( fpr);
 */
extern unsigned __builtin_fpx_ff2un_r(float);

/*  ff2un fpr , fpr
 * fpr ff2un( fpr);
 */
extern unsigned __builtin_fpx_ff2un_f(float);

/*  ff2u gpr , fpr
 * gpr ff2u( fpr);
 */
extern unsigned __builtin_fpx_ff2u_r(float);

/*  ff2u fpr , fpr
 * fpr ff2u( fpr);
 */
extern unsigned __builtin_fpx_ff2u_f(float);

/*  fi2f fpr , gpr
 * fpr fi2f( gpr);
 */
extern float __builtin_fpx_fi2f_r(int);

/*  fi2f fpr , fpr
 * fpr fi2f( fpr);
 */
extern float __builtin_fpx_fi2f_f(int);

/*  fmacn fpr , fpr , fpr
 * fpr fmacn( fpr, fpr, fpr);
 */
extern float __builtin_fpx_fmacn(float, float, float);

/*  fmac fpr , fpr , fpr
 * fpr fmac( fpr, fpr, fpr);
 */
extern float __builtin_fpx_fmac(float, float, float);

/*  fmax fpr , fpr , fpr
 * fpr fmax( fpr, fpr);
 */
extern float __builtin_fpx_fmax(float, float);

/*  fmin fpr , fpr , fpr
 * fpr fmin( fpr, fpr);
 */
extern float __builtin_fpx_fmin(float, float);

/*  fmuln fpr , fpr , fpr
 * fpr fmuln( fpr, fpr);
 */
extern float __builtin_fpx_fmuln(float, float);

/*  fmul fpr , fpr , fpr
 * fpr fmul( fpr, fpr);
 */
extern float __builtin_fpx_fmul(float, float);

/*  fmvf2f fpr , fpr
 * fpr fmvf2f( fpr);
 */
extern float __builtin_fpx_fmvf2f(float);

/*  fmvf2r gpr , fpr
 * gpr fmvf2r( fpr);
 */
extern int __builtin_fpx_fmvf2r(float);

/*  fmvr2f fpr , gpr
 * fpr fmvr2f( gpr);
 */
extern float __builtin_fpx_fmvr2f(int);

/*  fmvr2st gpr
 * fmvr2st( gpr);
 */
extern void __builtin_fpx_fmvr2st(int);

/*  fmvr2s gpr
 * fmvr2s( gpr);
 */
extern void __builtin_fpx_fmvr2s(int);

/*  fmvs2r gpr
 * gpr fmvs2r( );
 */
extern int __builtin_fpx_fmvs2r(void);

/*  fneg fpr , fpr
 * fpr fneg( fpr);
 */
extern float __builtin_fpx_fneg(float);

/*  fscalb fpr , fpr , gpr
 * fpr fscalb( fpr, gpr);
 */
extern float __builtin_fpx_fscalb(float, int);

/*  fsqrt fpr , fpr
 * fpr fsqrt( fpr);
 */
extern float __builtin_fpx_fsqrt(float);

/*  fsub fpr , fpr , fpr
 * fpr fsub( fpr, fpr);
 */
extern float __builtin_fpx_fsub(float, float);

/*  fu2f fpr , gpr
 * fpr fu2f( gpr);
 */
extern float __builtin_fpx_fu2f_r(unsigned);

/*  fu2f fpr , fpr
 * fpr fu2f( fpr);
 */
extern float __builtin_fpx_fu2f_f(unsigned);

/*  macn fpr , gpr , gpr
 * fpr macn( fpr, gpr, gpr);
 */
extern int __builtin_fpx_macn(int, int, int);

/*  macnISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr macn( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
extern int __builtin_fpx_macn_mo_sg_g(int, int, int, int, int);

#define __builtin_fpx_macnssll(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsslh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsshl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsshh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_ss, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsull(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsulh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsuhl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnsuhh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_su, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnusll(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnuslh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnushl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnushh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_us, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnuull(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnuulh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnuuhl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macnuuhh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_macn_mo_sg_g(intrn_sg_uu, intrn_mo_hh, c, d, e); res; \
	} ) 

/*  mac fpr , gpr , gpr
 * fpr mac( fpr, gpr, gpr);
 */
extern int __builtin_fpx_mac(int, int, int);

/*  macISA_EC_sgISA_EC_mo fpr , gpr , gpr
 * fpr mac( ISA_EC_sg, ISA_EC_mo, fpr, gpr, gpr);
 */
extern int __builtin_fpx_mac_mo_sg_g(int, int, int, int, int);

#define __builtin_fpx_macssll(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsslh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsshl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsshh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_ss, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsull(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsulh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsuhl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macsuhh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_su, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macusll(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macuslh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macushl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macushh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_us, intrn_mo_hh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macuull(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_ll, c, d, e); res; \
	} ) 

#define __builtin_fpx_macuulh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_lh, c, d, e); res; \
	} ) 

#define __builtin_fpx_macuuhl(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_hl, c, d, e); res; \
	} ) 

#define __builtin_fpx_macuuhh(d, e) ( { \
	int res; \
	 res = __builtin_fpx_mac_mo_sg_g(intrn_sg_uu, intrn_mo_hh, c, d, e); res; \
	} ) 

/*  modu fpr , gpr , gpr
 * fpr modu( gpr, gpr);
 */
extern unsigned __builtin_fpx_modu(unsigned, unsigned);

/*  mod fpr , gpr , gpr
 * fpr mod( gpr, gpr);
 */
extern int __builtin_fpx_mod(int, int);

/*  mpnsshh gpr , gpr , gpr
 * gpr mpnsshh( gpr, gpr);
 */
extern int __builtin_fpx_mpnsshh(int, int);

/*  mpnsshl gpr , gpr , gpr
 * gpr mpnsshl( gpr, gpr);
 */
extern int __builtin_fpx_mpnsshl(int, int);

/*  mpnsslh gpr , gpr , gpr
 * gpr mpnsslh( gpr, gpr);
 */
extern int __builtin_fpx_mpnsslh(int, int);

/*  mpnssll gpr , gpr , gpr
 * gpr mpnssll( gpr, gpr);
 */
extern int __builtin_fpx_mpnssll(int, int);

/*  mpnsuhh gpr , gpr , gpr
 * gpr mpnsuhh( gpr, gpr);
 */
extern int __builtin_fpx_mpnsuhh(int, unsigned);

/*  mpnsuhl gpr , gpr , gpr
 * gpr mpnsuhl( gpr, gpr);
 */
extern int __builtin_fpx_mpnsuhl(int, unsigned);

/*  mpnsulh gpr , gpr , gpr
 * gpr mpnsulh( gpr, gpr);
 */
extern int __builtin_fpx_mpnsulh(int, unsigned);

/*  mpnsull gpr , gpr , gpr
 * gpr mpnsull( gpr, gpr);
 */
extern int __builtin_fpx_mpnsull(int, unsigned);

/*  mpnushh gpr , gpr , gpr
 * gpr mpnushh( gpr, gpr);
 */
extern int __builtin_fpx_mpnushh(unsigned, int);

/*  mpnushl gpr , gpr , gpr
 * gpr mpnushl( gpr, gpr);
 */
extern int __builtin_fpx_mpnushl(unsigned, int);

/*  mpnuslh gpr , gpr , gpr
 * gpr mpnuslh( gpr, gpr);
 */
extern int __builtin_fpx_mpnuslh(unsigned, int);

/*  mpnusll gpr , gpr , gpr
 * gpr mpnusll( gpr, gpr);
 */
extern int __builtin_fpx_mpnusll(unsigned, int);

/*  mpnuuhh gpr , gpr , gpr
 * gpr mpnuuhh( gpr, gpr);
 */
extern int __builtin_fpx_mpnuuhh(unsigned, unsigned);

/*  mpnuuhl gpr , gpr , gpr
 * gpr mpnuuhl( gpr, gpr);
 */
extern int __builtin_fpx_mpnuuhl(unsigned, unsigned);

/*  mpnuulh gpr , gpr , gpr
 * gpr mpnuulh( gpr, gpr);
 */
extern int __builtin_fpx_mpnuulh(unsigned, unsigned);

/*  mpnuull gpr , gpr , gpr
 * gpr mpnuull( gpr, gpr);
 */
extern int __builtin_fpx_mpnuull(unsigned, unsigned);

/*  mpn gpr , gpr , gpr
 * gpr mpn( gpr, gpr);
 */
extern int __builtin_fpx_mpn(int, int);

/*  mpsshh gpr , gpr , gpr
 * gpr mpsshh( gpr, gpr);
 */
extern int __builtin_fpx_mpsshh(int, int);

/*  mpsshl gpr , gpr , gpr
 * gpr mpsshl( gpr, gpr);
 */
extern int __builtin_fpx_mpsshl(int, int);

/*  mpsslh gpr , gpr , gpr
 * gpr mpsslh( gpr, gpr);
 */
extern int __builtin_fpx_mpsslh(int, int);

/*  mpssll gpr , gpr , gpr
 * gpr mpssll( gpr, gpr);
 */
extern int __builtin_fpx_mpssll(int, int);

/*  mpsuhh gpr , gpr , gpr
 * gpr mpsuhh( gpr, gpr);
 */
extern int __builtin_fpx_mpsuhh(int, unsigned);

/*  mpsuhl gpr , gpr , gpr
 * gpr mpsuhl( gpr, gpr);
 */
extern int __builtin_fpx_mpsuhl(int, unsigned);

/*  mpsulh gpr , gpr , gpr
 * gpr mpsulh( gpr, gpr);
 */
extern int __builtin_fpx_mpsulh(int, unsigned);

/*  mpsull gpr , gpr , gpr
 * gpr mpsull( gpr, gpr);
 */
extern int __builtin_fpx_mpsull(int, unsigned);

/*  mpushh gpr , gpr , gpr
 * gpr mpushh( gpr, gpr);
 */
extern int __builtin_fpx_mpushh(unsigned, int);

/*  mpushl gpr , gpr , gpr
 * gpr mpushl( gpr, gpr);
 */
extern int __builtin_fpx_mpushl(unsigned, int);

/*  mpuslh gpr , gpr , gpr
 * gpr mpuslh( gpr, gpr);
 */
extern int __builtin_fpx_mpuslh(unsigned, int);

/*  mpusll gpr , gpr , gpr
 * gpr mpusll( gpr, gpr);
 */
extern int __builtin_fpx_mpusll(unsigned, int);

/*  mpuuhh gpr , gpr , gpr
 * gpr mpuuhh( gpr, gpr);
 */
extern int __builtin_fpx_mpuuhh(unsigned, unsigned);

/*  mpuuhl gpr , gpr , gpr
 * gpr mpuuhl( gpr, gpr);
 */
extern int __builtin_fpx_mpuuhl(unsigned, unsigned);

/*  mpuulh gpr , gpr , gpr
 * gpr mpuulh( gpr, gpr);
 */
extern int __builtin_fpx_mpuulh(unsigned, unsigned);

/*  mpuull gpr , gpr , gpr
 * gpr mpuull( gpr, gpr);
 */
extern int __builtin_fpx_mpuull(unsigned, unsigned);

/*  mp gpr , gpr , gpr
 * gpr mp( gpr, gpr);
 */
extern int __builtin_fpx_mp(int, int);

#endif


#endif /* _BUILTIN_FPX_H_ */
#endif /* __FPX */
