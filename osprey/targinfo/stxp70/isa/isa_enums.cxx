/* -- This file is automatically generated -- */ 
/* 

  Copyright (C) 2002, 2004 ST Microelectronics, Inc.  All Rights Reserved. 

  This program is free software; you can redistribute it and/or modify it 
  under the terms of version 2 of the GNU General Public License as 
  published by the Free Software Foundation. 
  This program is distributed in the hope that it would be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

  Further, this software is distributed without any warranty that it is 
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever. 
  You should have received a copy of the GNU General Public License along 
  with this program; if not, write the Free Software Foundation, Inc., 59 
  Temple Place - Suite 330, Boston MA 02111-1307, USA. 

  Contact information:  ST Microelectronics, Inc., 
  , or: 

  http://www.st.com 

  For further information regarding this notice, see: 

  http: 
*/ 

// AUTOMATICALLY GENERATED FROM MDS DATA BASE !!! 


// 
// Generate a list of enumeration classes and values for the ISA. 
///////////////////////////////////////////////////////// 

#include <stddef.h> 
#include "isa_enums_gen.h" 

main () 
{ 
  ISA_Enums_Begin(); 

ISA_Create_Enum_Class ("bb",ISA_ENUM_Emit_Name,
	"b0", 0x0,
	"b1", 0x1,
	"b2", 0x2,
	"b3", 0x3,
	"",-1);

ISA_Create_Enum_Class ("sg",ISA_ENUM_Emit_Name,
	"ss", 0x0,
	"su", 0x1,
	"us", 0x2,
	"uu", 0x3,
	"",-1);

ISA_Create_Enum_Class ("mo",ISA_ENUM_Emit_Name,
	"ll", 0x0,
	"lh", 0x1,
	"hl", 0x2,
	"hh", 0x3,
	"",-1);

ISA_Create_Enum_Class ("cmp",ISA_ENUM_Emit_Name,
	"eq", 0x0,
	"ne", 0x1,
	"ge", 0x2,
	"lt", 0x3,
	"le", 0x4,
	"gt", 0x5,
	"",-1);

ISA_Create_Enum_Class ("btest",ISA_ENUM_Emit_Name,
	"f", 0x0,
	"t", 0x1,
	"",-1);

ISA_Create_Enum_Class ("bitop",ISA_ENUM_Emit_Name,
	"clr", 0x0,
	"set", 0x1,
	"not", 0x2,
	"",-1);

ISA_Create_Enum_Class ("cp",ISA_ENUM_Emit_Name,
	"eq", 0x0,
	"ne", 0x1,
	"ge", 0x2,
	"lt", 0x3,
	"le", 0x4,
	"gt", 0x5,
	"un", 0x6,
	"",-1);

ISA_Create_Enum_Class ("sg_v4",ISA_ENUM_Emit_Name,
	"uu", 0x1,
	"su", 0x2,
	"ss", 0x3,
	"",-1);


  ISA_Enums_End(); 
  return 0; 
} 
