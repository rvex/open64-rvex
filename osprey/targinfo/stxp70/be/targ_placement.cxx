/*

  Copyright (C) 2009 ST Microelectronics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information: 
  or:

  http://www.st.com

  For further information regarding this notice, see:


*/

#include <stdio.h>
#include "symtab.h"
#include "ipc_symtab_merge.h"		// for AUX_ST
#include "config_ipa.h"			// for IPA_MEM_Placement_Array, etc.


/* ====================================================================
 *    is_struct: return is the base type is structured.
 * ====================================================================
 */
static BOOL is_struct(TY_IDX ty_id) {
  if (TY_kind(ty_id) == KIND_ARRAY) {
    return is_struct(TY_etype(ty_id));
  }
  else if (TY_kind(ty_id) == KIND_STRUCT) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/* ====================================================================
 *   type_align : return the size of the data in bytes
 * ====================================================================
 */
static UINT64 type_align(TY_IDX ty_id) {
  UINT32 res;
  TY_KIND kind = TY_kind(ty_id); 
 
  if ((kind == KIND_SCALAR) ||
      (kind == KIND_POINTER)) {
    res = TY_align(ty_id);
  }
  else if (kind == KIND_ARRAY) {
    res = type_align(TY_etype(ty_id));
  }
  else if (kind == KIND_STRUCT) {
    res = UINT32_MAX;
    /* check that we do not have an empty struct */
    if (TY_size(ty_id) > 0) {
      FLD_ITER fld_iter = Make_fld_iter (TY_fld(ty_id));
      while (!FLD_last_field (fld_iter)) {
        UINT32 tmp = type_align(FLD_type(fld_iter));
        if (tmp < res) {
          res= tmp;
          /* if byte alignment is achieved, stop parsing the structure
             fields */
          if (res==1)
            break;
        }
        ++fld_iter;
      }
    }
  }
  else {
    DevWarn ("Unexpected TY_kind() %d\n", TY_kind(ty_id));
    /* using smallest alignment conservatively. */
    res = 1;
  }
  return res;
}

/* ====================================================================
 *   cmp_glob_var_size : function called by qsort to sort the datas
 * according their size and their mod/ref counts.
 * ====================================================================
 */
static int cmp_glob_var_size(const void *st1, const void *st2) {
  AUX_ST& aux_st1 = Aux_St_Table[ST_st_idx (*(ST **)st1)];
  AUX_ST& aux_st2 = Aux_St_Table[ST_st_idx (*(ST **)st2)];
  UINT64 size1 = TY_size(ST_type(*(ST **)st1));
  UINT64 size2 = TY_size(ST_type(*(ST **)st2));
  if (size1 < size2) {
    return -1;
  }
  else if (size1 > size2) {
    return 1;
  }
  else {
    INT count1 = AUX_ST_refcount(aux_st1) + AUX_ST_modcount(aux_st1);
    INT count2 = AUX_ST_refcount(aux_st2) + AUX_ST_modcount(aux_st2);
    if ( count1 < count2) {
      return 1;
    }
    else if (count1 > count2) {
      return -1;
    }
    else {
      return 0;
    }  
  }
}

/* ====================================================================
 *   Perform_MEM_Placement: main function of automatic data placement.
 * ====================================================================
 */
void Perform_MEM_Placement (void)
{
  Temporary_Error_Phase ephase ("IPA Mem Placement");
  BOOL trace = Get_Trace(TP_IPO, 1);
  INT i, j, k, l, m;
  INT nb_globals_byte = 0;
  INT nb_globals_half = 0;
  INT nb_globals_word = 0;
  INT nb_globals_da = 0;
  UINT64 size_sda_byte, size_sda_half, size_sda_word, size_da;
  ST *st;
  size_sda_byte = 0; /* in bytes */
  size_sda_half = 0; /* in bytes */
  size_sda_word = 0; /* in bytes */
  size_da = 0;
  FOREACH_SYMBOL (GLOBAL_SYMTAB, st, i) {
    if ((ST_class(st) == CLASS_VAR) &&
	(!ST_has_named_section(st)) &&
	(!(ST_sclass(st) == SCLASS_EXTERN))) {
      if (ST_memory_space(st) == ST_MEMORY_SDA) {
	DevWarn("GLOBAL %s has attribute 'ST_MEMORY_SDA'", ST_name(st));
	if ((TY_kind(ST_type(st)) == KIND_ARRAY) ||
	    (TY_kind(ST_type(st)) == KIND_STRUCT)) {
	  UINT64 size = TY_size(ST_type(st));
	  UINT64 elt_align = type_align(ST_type(st));
	  if (elt_align == 1) {
	    size_sda_byte += size;	      
	  }
	  else if (elt_align == 2) {
	    size_sda_half += size;
	  }
	  else {
	    size_sda_word += size;
	  }
	}
	else {
	  UINT64 size = TY_size(ST_type(st));
	  if (size == 1) {
	    size_sda_byte += size;
	  }
	  else if (size == 2) {
	    size_sda_half += size;
	  }
	  else {
	    size_sda_word += size;
	  }
	}
      }
      else if (ST_memory_space(st) == ST_MEMORY_DA) {
	size_da += TY_size(ST_type(st));
	DevWarn("GLOBAL %s has attribute 'ST_MEMORY_DA'", ST_name(st));
      }
      else if (ST_memory_space(st) == ST_MEMORY_TDA) {
	DevWarn("GLOBAL %s has attribute 'ST_MEMORY_TDA'", ST_name(st));
      }
      else if (ST_memory_space(st) == ST_MEMORY_NONE) {
	DevWarn("GLOBAL %s has attribute 'ST_MEMORY_NONE'", ST_name(st));
      }
      else {
	DevWarn("GLOBAL %s has no memory attribute", ST_name(st));
	if (((TY_kind(ST_type(st)) != KIND_ARRAY) || (IPA_MEM_Placement_Array)) &&
	    ((TY_kind(ST_type(st)) != KIND_STRUCT) || (IPA_MEM_Placement_Struct))) {
	  UINT64 align = type_align(ST_type(st));
	  if (is_struct(ST_type(st)) && (align < 4)) {
	    nb_globals_da++;
	  }
	  else if (align == 1) {
	    nb_globals_byte++;
	  }
	  else if (align == 2) {
	    nb_globals_half++;
	  }
	  else {
	      nb_globals_word++;
	  }
	}
      }
    }
  }
  if (trace) {
    fprintf(TFile, "There are %d byte globals to place\n", nb_globals_byte);
    fprintf(TFile, "There are %d half globals to place\n", nb_globals_half);
    fprintf(TFile, "There are %d word globals to place\n", nb_globals_word);
    fprintf(TFile, "There are %d struct globals to place in da\n", nb_globals_da);
    fprintf(TFile, "%lld bytes, %lld half, %lld word already in sda,\n",
	    size_sda_byte, size_sda_half/2, size_sda_word/4);
    fprintf(TFile, "%lld bytes already in da\n", size_da);
  }


  UINT64 sda_avail = IPA_SDAspace;
  if (trace) {
    fprintf(TFile, "sda available before manual placement: %lld\n", sda_avail);
  }
  /* Manual placement in DA is prioritary, if not excedent */
  if (size_da < IPA_DAspace) {
    if (IPA_DAspace - size_da < sda_avail*4) {
      sda_avail = (IPA_DAspace - size_da)/4;
    }
    if (trace) {
      fprintf(TFile, "sda available after da manual placement: %lld\n", sda_avail);
    }
  }
  else {
    if (trace) {
      fprintf(TFile, "manual da placement fills all DA\n");
    }
    return;
  }

  /* Manual placement of words in SDA has to be taken into account */
  /* If too many variables have been put in sda, linker will fail,
     but automatic placement has nothing to do */
  if (size_sda_word/4 < sda_avail ) {
    sda_avail -= (size_sda_word/4);
    if (trace) {
      fprintf(TFile, "sda available after word manual placement: %lld\n", sda_avail);
    }
  }
  else {
    if (trace) {
      fprintf(TFile, "manual sda placement fills all SDA\n");
    }
    return;
  }
  
  ST* array_globals_byte[nb_globals_byte];
  ST* array_globals_half[nb_globals_half];
  ST* array_globals_word[nb_globals_word];
  ST* array_globals_da[nb_globals_da];
  j = 0;
  k = 0;
  l = 0;
  m = 0;
  FOREACH_SYMBOL (GLOBAL_SYMTAB, st, i) {
    if (ST_class(st) == CLASS_VAR) {
      if (!((ST_memory_space(st) == ST_MEMORY_SDA) ||
	    (ST_memory_space(st) == ST_MEMORY_TDA) ||
	    (ST_memory_space(st) == ST_MEMORY_DA) ||
	    (ST_memory_space(st) == ST_MEMORY_NONE) ||
	    (ST_has_named_section(st)) ||
	    (ST_sclass(st) == SCLASS_EXTERN))) {
	if ((!IPA_MEM_Placement_Array) && (TY_kind(ST_type(st)) == KIND_ARRAY)) {
	  continue;
	}
	if ((!IPA_MEM_Placement_Struct) && (TY_kind(ST_type(st)) == KIND_STRUCT)) {
	  continue;
	}
	UINT64 align = type_align(ST_type(st));
	if (is_struct(ST_type(st)) && (align < 4)) {
	  array_globals_da[m] = st;
	  m++;
	}
	else if (align == 1) {
	  array_globals_byte[j] = st;
	  j++;
	}
	else if (align == 2) {
	  array_globals_half[k] = st;
	  k++;
	}
	else {
	  array_globals_word[l] = st;
	  l++;
	}
	AUX_ST& aux_st = Aux_St_Table[ST_st_idx (st)];
// 	fprintf(TFile, "GLOBAL %s (refs=%d, mods=%d), align %lld, ST_SCLASS %d\n",
// 		ST_name(st),
// 		AUX_ST_refcount(aux_st),
// 		AUX_ST_modcount(aux_st),
// 		align,
// 		ST_sclass(st));
	
      }
    }
  }

  if (IPA_MEM_Placement_Size) {
    /* Computation of sda available for bytes */
    UINT64 sda_byte_avail = IPA_SDAspace;
    if (sda_avail*4 < IPA_SDAspace) {
      sda_byte_avail = sda_avail*4;
    }
    sda_byte_avail -= size_sda_half;
    sda_byte_avail -= size_sda_byte;
    if (trace) {
      fprintf(TFile, "sda available for bytes placement: %lld\n", sda_byte_avail);
    }
    /* Computation of sda available for half before automatic placement of bytes*/
    UINT64 sda_half_avail = IPA_SDAspace*2;
    if (sda_avail < IPA_SDAspace) {
      sda_half_avail = sda_avail*2;
    }
    sda_half_avail -= size_sda_half;
    sda_half_avail -= size_sda_byte;
    if (trace) {
      fprintf(TFile, "sda available for half placement: %lld\n", sda_half_avail);
    }
  
    if (trace) {
      fprintf(TFile, "Placement by size\n");
    }
    if (nb_globals_byte != 0) {
      qsort((void *)array_globals_byte, nb_globals_byte, sizeof(ST*), cmp_glob_var_size) ;
    }
    if (nb_globals_half != 0) {
      qsort((void *)array_globals_half, nb_globals_half, sizeof(ST*), cmp_glob_var_size) ;
    }
    if (nb_globals_word != 0) {
      qsort((void *)array_globals_word, nb_globals_word, sizeof(ST*), cmp_glob_var_size) ;
    }
    if (nb_globals_da != 0) {
      qsort((void *)array_globals_da, nb_globals_da, sizeof(ST*), cmp_glob_var_size) ;
    }

    UINT64 automatic_sda_byte = 0;
    for (i = 0; i < nb_globals_byte; i++) {
      UINT64 size = TY_size(ST_type(array_globals_byte[i]));
      if ((automatic_sda_byte+size) <= sda_byte_avail) {
	Set_ST_memory_space(array_globals_byte[i], ST_MEMORY_SDA);
	automatic_sda_byte+=size;
	if (trace) {
	  fprintf(TFile, "GLOBAL %s put in sda/bytes (%lld used (%lld manual + %lld automatic)/%lld available) (size %lld)\n",
		  ST_name(array_globals_byte[i]),
		  automatic_sda_byte + size_sda_byte, size_sda_byte, automatic_sda_byte, sda_byte_avail, size);
	}
      }
    }
    size_sda_byte += automatic_sda_byte;

    UINT64 automatic_sda_half = 0;
    for (i = 0; i < nb_globals_half; i++) {
      UINT64 size = TY_size(ST_type(array_globals_half[i]));
      if ((automatic_sda_half+size) <= (sda_half_avail - ((size_sda_byte+1)/2)*2)) {
	Set_ST_memory_space(array_globals_half[i], ST_MEMORY_SDA);
	automatic_sda_half+=size;
	if (trace) {
	  fprintf(TFile, "GLOBAL %s put in sda/half (%lld used (%lld manual + %lld automatic)/%lld available) (size %lld)\n",
		  ST_name(array_globals_half[i]),
		  automatic_sda_half + size_sda_half, size_sda_half, automatic_sda_half, sda_half_avail - ((size_sda_byte+1)/2)*2, size);
	}
      }
    }
    size_sda_half += automatic_sda_half;

    UINT64 automatic_sda_word = 0;
    for (i = 0 ; i < nb_globals_word; i++) {
      UINT64 size = TY_size(ST_type(array_globals_word[i]));
      if ((automatic_sda_word+size) < ((sda_avail - ((size_sda_byte+size_sda_half+3)/4))*4)) {
	Set_ST_memory_space(array_globals_word[i], ST_MEMORY_SDA);
	automatic_sda_word+=size;
	if (trace) {
	  fprintf(TFile, "GLOBAL %s put in sda/word (%lld used (%lld manual + %lld automatic)/%lld available) (size %lld)\n",
		  ST_name(array_globals_word[i]),
		  automatic_sda_word + size_sda_word, size_sda_word, automatic_sda_word, (sda_avail - ((size_sda_byte+size_sda_half+3)/4))*4, size);
	}
      }
    }
    size_sda_word += automatic_sda_word;

    UINT64 da_avail = IPA_DAspace - (size_da + size_sda_word + ((size_sda_half + size_sda_byte + 3)/4)*4);
    for (i = 0; i < nb_globals_byte; i++) {
      if (ST_memory_space(array_globals_byte[i]) != ST_MEMORY_SDA) {
	UINT64 size = TY_size(ST_type(array_globals_byte[i]));
	if ((size_da+size) <= da_avail ) {
	  Set_ST_memory_space(array_globals_byte[i], ST_MEMORY_DA);
	  size_da+=size;
	  if (trace) {
	    fprintf(TFile, "GLOBAL %s put in da (%lld used/%lld available) (size %lld)\n",
		    ST_name(array_globals_byte[i]), size_da, da_avail, size);
	  }
	}
      }
    }
    for (i = 0; i < nb_globals_half; i++) {
      if (ST_memory_space(array_globals_half[i]) != ST_MEMORY_SDA) {
	UINT64 size = TY_size(ST_type(array_globals_half[i]));
	if ((size_da+size) <= da_avail) {
	  Set_ST_memory_space(array_globals_half[i], ST_MEMORY_DA);
	  size_da+=size;
	  if (trace) {
	    fprintf(TFile, "GLOBAL %s put in da (%lld used/%lld available) (size %lld)\n",
		    ST_name(array_globals_half[i]), size_da, da_avail, size);
	  }
	}
      }
    }
    for (i = 0; i < nb_globals_word; i++) {
      if (ST_memory_space(array_globals_word[i]) != ST_MEMORY_SDA) {
	UINT64 size = TY_size(ST_type(array_globals_word[i]));
	if ((size_da+size) <= da_avail) {
	  Set_ST_memory_space(array_globals_word[i], ST_MEMORY_DA);
	  size_da+=size;
	  if (trace) {
	    fprintf(TFile, "GLOBAL %s put in da (%lld used/%lld available) (size %lld)\n",
		    ST_name(array_globals_word[i]), size_da, da_avail, size);
	  }
	}
      }
    }   

    for (i = 0; i < nb_globals_da; i++) {
      UINT64 size = TY_size(ST_type(array_globals_da[i]));
      /* Optimisation: if size of sda filled + size of struct < 4096 bytes,
	 struct can be put in sda */
      UINT32 align = type_align(ST_type(array_globals_da[i]));
      if (align == 1) {
	UINT64 sda_half_used_by_bytes = ((size + size_sda_byte + 1)/2)*2;
	UINT64 sda_word_used_by_bytes_and_half = ((sda_half_used_by_bytes + size_sda_half +3)/4)*4;
	UINT64 used_sda = size_sda_word + sda_word_used_by_bytes_and_half + sda_half_used_by_bytes;
	if (((size + size_sda_byte) <= sda_byte_avail) &&
	    (used_sda < sda_avail*4 ) &&
	    (used_sda+size_da < da_avail)) {
	  Set_ST_memory_space(array_globals_da[i], ST_MEMORY_SDA);
	  size_sda_byte+=size;
	  da_avail -= size;
	  if (trace) {
	    fprintf(TFile, "GLOBAL %s put in sda/bytes (%lld used/%lld available) (size %lld) (total sda used %lld)\n",
		    ST_name(array_globals_da[i]), size_sda_byte, sda_byte_avail, size, used_sda);
	  }
	}
      }
      else if (align == 2) {
	UINT64 sda_half_used_by_bytes = ((size_sda_byte + 1)/2)*2;
	UINT64 sda_word_used_by_bytes_and_half = ((sda_half_used_by_bytes + size_sda_half +size +3)/4)*4;
	UINT64 used_sda = size_sda_word + sda_word_used_by_bytes_and_half + sda_half_used_by_bytes;
	if (((size + size_sda_half) <= (sda_half_avail - ((size_sda_byte+1)/2)*2)) &&
	    (used_sda < sda_avail*4) &&
	    (used_sda+size_da < da_avail)) {
	  Set_ST_memory_space(array_globals_da[i], ST_MEMORY_SDA);
	  size_sda_half+=size;
	  da_avail -= size;
	  if (trace) {
	    fprintf(TFile, "GLOBAL %s put in sda/half (%lld used/%lld available) (size %lld) (total sda used %lld)\n",
		    ST_name(array_globals_da[i]), size_sda_half, sda_half_avail - ((size_sda_byte+1)/2)*2, size, used_sda);
	  }
	}
      }
      if (((size_da+size) <= da_avail) &&
	  (ST_memory_space(array_globals_da[i]) != ST_MEMORY_SDA)) {
	Set_ST_memory_space(array_globals_da[i], ST_MEMORY_DA);
	size_da+=size;
	if (trace) {
	fprintf(TFile, "GLOBAL %s put in da (%lld used/%lld available) (size %lld)\n",
		ST_name(array_globals_da[i]), size_da, da_avail, size);
	}
      }
    }   
  }
  else {
    DevWarn("Other placement than by size not implemented yet\n");
  }
}
