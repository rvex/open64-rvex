/*

  Copyright (C) 2008, STMicroelectronics Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement
  or the like.  Any license provided herein, whether implied or
  otherwise, applies only to this software file.  Patent licenses, if
  any, provided herein do not apply to combinations of this program with
  other software, or any other product whatsoever.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

*/

#include "wn.h"
#include "extension_intrinsic.h"
#include "intrn_info.h"


/**
 * Variable stores whether pattern_rec has been initialized
 * 
 */
static bool betarg_pattern_rec_initialized = false;

/* Intrinsic used for implicit parameter */
static INTRINSIC mpx_intrn_shlir2x = INTRINSIC_INVALID;


/** 
 * Initialize pattern_rec
 * 
 */
void
BETARG_Init_Pattern_Rec(void) {
  if (!betarg_pattern_rec_initialized) {
    mpx_intrn_shlir2x = EXTENSION_INTRINSIC_From_Name("__builtin_MP1x_SHLIR2X");
    if (mpx_intrn_shlir2x == INTRINSIC_INVALID) {
      mpx_intrn_shlir2x =
        EXTENSION_INTRINSIC_From_Name("__builtin_MP2x_SHLIR2X");
    }
    betarg_pattern_rec_initialized = true;
  }
}

/**
 * Function that generates an intrinsic OP from extension code generation.
 * Return the corresponding whirl node in case of success, return NULL otherwise.
 *
 * @param intrnidx  id of the intrinsic
 * @param nbkids    arguments count of the intrinsics
 * @param kids      arguments of the intrinsics
 * @param dsttype   return type
 * @param new_stmts statement containing the initial code, if any
 * @param modified  will be set in case of successful generation
 */
WN *
BETARG_Create_Intrinsic_from_OP(INTRINSIC intrnidx, int nbkids, WN *kids[],
				TYPE_ID dsttype, WN** new_stmts, bool* modified)
{
  proto_intrn_info_t * proto = INTRN_proto_info(intrnidx);

  if ((intrnidx == mpx_intrn_shlir2x) && (nbkids == 1)) {
    /* Special handling of SHLIR2X that might be used to perform
     * a 32bits to 64bits conversion with sign extension      */
    WN *newkids[2];
    newkids[0] = kids[0];
    newkids[1] = WN_Intconst(MTYPE_I4, 0LL);
    return Create_Intrinsic_from_OP(intrnidx, 2, newkids, 0, NULL,
                                    dsttype, new_stmts, modified);
  }
  else {
    return Create_Intrinsic_from_OP(intrnidx, nbkids, kids, 0, NULL,
                                    dsttype, new_stmts, modified);
  }
}
