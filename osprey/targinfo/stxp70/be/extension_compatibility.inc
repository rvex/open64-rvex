// Backward compatibility file for MP1x extension older than REV_20090813
// As MP1x extensions are delivered with the toolset, this compatibility
// could be removed for next official toolset delivery.

enum { EXTOPT_enablefractgen = 0x1LL };
static const char* mpx_extoption_array [] = {
  "enablefractgen"
};
static unsigned int mpx_extoption_count = 1; 

static pattern_desc* sub_pattern_single_terminal[] = { NULL };
static pattern_desc pattern_cvti8i4 = { pattern_desc_opc, OPC_I8I4CVT,  sub_pattern_single_terminal };

static pattern_desc pattern_cvti4i8 = { pattern_desc_opc, OPC_I4I8CVT,  sub_pattern_single_terminal };

static pattern_desc pattern_cvti8u4 = { pattern_desc_opc, OPC_I8U4CVT,  sub_pattern_single_terminal };
static pattern_desc pattern_cvtu8u4 = { pattern_desc_opc, OPC_U8U4CVT,  sub_pattern_single_terminal };

static pattern_desc* sub_pattern_mpw[] = { &pattern_cvti8i4, &pattern_cvti8i4 };
static pattern_desc pattern_mpw = { pattern_desc_opc, OPC_I8MPY, sub_pattern_mpw};

static pattern_desc* sub_pattern_dual_terminal[] = { NULL, NULL };

static pattern_desc pattern_i8ashr = { pattern_desc_opc, OPC_I8ASHR, sub_pattern_dual_terminal };
static pattern_desc* sub_pattern_shrrx2r_1[] = { &pattern_i8ashr };
static pattern_desc pattern_shrrx2r_1 = { pattern_desc_opc, OPC_I4I8CVT, sub_pattern_shrrx2r_1};
static pattern_desc pattern_shrrx2r_2 = { pattern_desc_opc, OPC_U4I8CVT, sub_pattern_shrrx2r_1 };

static pattern_desc* sub_pattern_shlrr2x_1[] ={ &pattern_cvti8i4, NULL }; 
static pattern_desc pattern_shlrr2x_1 = {  pattern_desc_opc, OPC_I8SHL, sub_pattern_shlrr2x_1 };
static pattern_desc* sub_pattern_shlrr2x_2[] = { &pattern_cvti8u4, NULL };
static pattern_desc pattern_shlrr2x_2 = {  pattern_desc_opc, OPC_I8SHL, sub_pattern_shlrr2x_2};
static pattern_desc* sub_pattern_shlrr2x_3[] = { &pattern_cvtu8u4, NULL };
static pattern_desc pattern_shlrr2x_3 = {  pattern_desc_opc, OPC_I8SHL, sub_pattern_shlrr2x_3};

static pattern_desc pattern_value1 = { pattern_desc_imm_value, 1, NULL };
static pattern_desc* sub_pattern_intconst1[] = { &pattern_value1 };
static pattern_desc pattern_intconst1 = { pattern_desc_opc, OPC_I4INTCONST, sub_pattern_intconst1 };
static pattern_desc* sub_pattern_mpfw[] = { &pattern_mpw, &pattern_intconst1 };
static pattern_desc pattern_mpfw = { pattern_desc_opc, OPC_I8SHL, sub_pattern_mpfw};

static pattern_desc* sub_pattern_maw[] ={ NULL, &pattern_mpw };
static pattern_desc pattern_maw = { pattern_desc_opc, OPC_I8ADD, sub_pattern_maw };
static pattern_desc pattern_msw = { pattern_desc_opc, OPC_I8SUB, sub_pattern_maw };

static pattern_desc* sub_pattern_mafw[] = { NULL, &pattern_mpfw };
static pattern_desc pattern_mafw = { pattern_desc_opc, OPC_I8ADD, sub_pattern_mafw };
static pattern_desc* sub_pattern_msfw[] = { NULL, &pattern_mpfw };
static pattern_desc pattern_msfw = { pattern_desc_opc, OPC_I8SUB, sub_pattern_msfw };


static pattern_desc pattern_value16 = { pattern_desc_imm_value, 16, NULL };
static pattern_desc* sub_pattern_i4cvtl[] = { NULL, &pattern_value16 };
static pattern_desc pattern_i4cvtl = { pattern_desc_opc, OPC_I4CVTL, sub_pattern_i4cvtl };
static pattern_desc* sub_pattern_mp16[] = { &pattern_i4cvtl, &pattern_i4cvtl};
static pattern_desc pattern_mp16 = { pattern_desc_opc, OPC_I4MPY, sub_pattern_mp16 };
static pattern_desc* sub_pattern_mph[] = { &pattern_mp16 };
static pattern_desc pattern_mph = { pattern_desc_opc, OPC_I8I4CVT, sub_pattern_mph }; 


static pattern_desc* sub_pattern_ma16[] = { &pattern_cvti4i8, &pattern_mp16 };
static pattern_desc pattern_ma16 = { pattern_desc_opc, OPC_I4ADD, sub_pattern_ma16 }; 
static pattern_desc* sub_pattern_ms16[] = { &pattern_cvti4i8, &pattern_mp16 };
static pattern_desc pattern_ms16 = { pattern_desc_opc, OPC_I4SUB, sub_pattern_ms16 }; 

static pattern_desc* sub_pattern_mah[] = { &pattern_ma16 };
static pattern_desc pattern_mah = { pattern_desc_opc, OPC_I8I4CVT, sub_pattern_mah }; 
static pattern_desc* sub_pattern_msh[] = { &pattern_ms16 };
static pattern_desc pattern_msh = { pattern_desc_opc, OPC_I8I4CVT, sub_pattern_msh }; 

static pattern_desc pattern_u8bnot = { pattern_desc_opc, OPC_U8BNOT,  sub_pattern_single_terminal};
static pattern_desc pattern_i8bnot = { pattern_desc_opc, OPC_I8BNOT,  sub_pattern_single_terminal};
static pattern_desc* sub_pattern_u8andcd[] = { NULL, &pattern_u8bnot };
static pattern_desc pattern_u8andcd = { pattern_desc_opc, OPC_U8BAND, sub_pattern_u8andcd }; 
static pattern_desc* sub_pattern_i8andcd[] = { NULL, &pattern_i8bnot };
static pattern_desc pattern_i8andcd = { pattern_desc_opc, OPC_I8BAND, sub_pattern_i8andcd }; 


static int path_mpw_k0[] = {0, 0};
static operand_desc mpw_k0 = {operand_desc_path, 2, path_mpw_k0};
static int path_mpw_k1[] = {1, 0};
static operand_desc mpw_k1 = {operand_desc_path, 2, path_mpw_k1};
static operand_desc* mpw_kids[] = { &mpw_k0, &mpw_k1 };
static recog_rule rule_mpw = { INTRINSIC_INVALID, "__builtin_MP1x_MPW",
                               EXTOPT_none, &pattern_mpw, mpw_kids};

static int path_shrrx2r_k0[] = { 0, 0};
static operand_desc shrrx2r_k0 = {operand_desc_path, 2, path_shrrx2r_k0}; 
static int path_shrrx2r_k1[] = { 0, 1};
static operand_desc shrrx2r_k1 = {operand_desc_path, 2, path_shrrx2r_k1}; 
static operand_desc* shrrx2r_kids[] = { &shrrx2r_k0, &shrrx2r_k1 };
static recog_rule rule_shrrx2r_1 = { INTRINSIC_INVALID, "__builtin_MP1x_SHRRX2R",
                                     EXTOPT_none, &pattern_shrrx2r_1, shrrx2r_kids};
static recog_rule rule_shrrx2r_2 = { INTRINSIC_INVALID, "__builtin_MP1x_SHRRX2R",
                                     EXTOPT_none, &pattern_shrrx2r_2, shrrx2r_kids};

static int path_shlrr2x_k0[] = { 0, 0};
static operand_desc shlrr2x_k0 = {operand_desc_path, 2, path_shlrr2x_k0}; 
static int path_shlrr2x_k1[] = { 1};
static operand_desc shlrr2x_k1 = {operand_desc_path, 1, path_shlrr2x_k1}; 
static operand_desc* shlrr2x_kids[] = { &shlrr2x_k0, &shlrr2x_k1 };
static recog_rule rule_shlrr2x_1={ INTRINSIC_INVALID, "__builtin_MP1x_SHLRR2X",
                                   EXTOPT_none, &pattern_shlrr2x_1, shlrr2x_kids};
static recog_rule rule_shlrr2x_2={ INTRINSIC_INVALID, "__builtin_MP1x_SHLRR2X",
                                   EXTOPT_none, &pattern_shlrr2x_2, shlrr2x_kids};
static recog_rule rule_shlrr2x_3={ INTRINSIC_INVALID, "__builtin_MP1x_SHLRR2X",
                                   EXTOPT_none, &pattern_shlrr2x_3, shlrr2x_kids};

static int path_bnot_k0[] = { 0};
static operand_desc bnot_k0 = {operand_desc_path, 1, path_bnot_k0}; 
static int path_bnot_k1[] = { 1, 0};
static operand_desc bnot_k1 = {operand_desc_path, 2, path_bnot_k1}; 
static operand_desc* bnot_kids[] = { &bnot_k0, &bnot_k1 };
static recog_rule rule_u8andcd = { INTRINSIC_INVALID, "__builtin_MP1x_ANDCD",
                                   EXTOPT_none, &pattern_u8andcd, bnot_kids};
static recog_rule rule_i8andcd = { INTRINSIC_INVALID, "__builtin_MP1x_ANDCD",
                                    EXTOPT_none, &pattern_i8andcd, bnot_kids};

static int path_mph_k0[] = { 0, 0, 0};
static operand_desc mph_k0 = {operand_desc_path, 3, path_mph_k0}; 
static int path_mph_k1[] = { 0, 1, 0};
static operand_desc mph_k1 = {operand_desc_path, 3, path_mph_k1}; 
static operand_desc mph_k2 = {operand_desc_imm,  0 }; // 0 means LL version of mph instruction
static operand_desc* mph_kids[] = { &mph_k0, &mph_k1, &mph_k2 };
static recog_rule rule_mph = { INTRINSIC_INVALID, "__builtin_MP1x_MPH",
                               EXTOPT_none, &pattern_mph, mph_kids};

static int path_mpfw_k0[] = { 0, 0, 0};
static operand_desc mpfw_k0 = {operand_desc_path, 3, path_mpfw_k0}; 
static int path_mpfw_k1[] = { 0, 1, 0};
static operand_desc mpfw_k1 = {operand_desc_path, 3, path_mpfw_k1}; 
static operand_desc mpfw_k2 = {operand_desc_sfr,  0 }; // 0 means sfr rank 0 (register VSW)
static operand_desc* mpfw_kids[] = { NULL, &mpfw_k0, &mpfw_k1, &mpfw_k2};
static recog_rule rule_mpfw = { INTRINSIC_INVALID, "__builtin_MP1x_MPFW",
                                EXTOPT_enablefractgen, &pattern_mpfw, mpfw_kids };

static int path_mafw_k0[] = { 0};
static operand_desc mafw_k0 = {operand_desc_path, 1, path_mafw_k0}; 
static int path_mafw_k1[] = { 1, 0, 0, 0};
static operand_desc mafw_k1 = {operand_desc_path, 4, path_mafw_k1}; 
static int path_mafw_k2[] = { 1, 0, 1, 0};
static operand_desc mafw_k2 = {operand_desc_path, 4, path_mafw_k2}; 
static operand_desc mafw_k3 = {operand_desc_sfr,  0 }; // 0 means sfr rank 0 (register VSW)
static operand_desc* mafw_kids[] = { &mafw_k0, &mafw_k1, &mafw_k2, &mafw_k3 };
static recog_rule rule_mafw = { INTRINSIC_INVALID, "__builtin_MP1x_MAFW",
                                EXTOPT_enablefractgen, &pattern_mafw, mafw_kids };
static recog_rule rule_msfw = { INTRINSIC_INVALID, "__builtin_MP1x_MSFW",
                                EXTOPT_enablefractgen, &pattern_msfw, mafw_kids };

static int path_maw_k0[] = { 0};
static operand_desc maw_k0 = {operand_desc_path, 1, path_maw_k0}; 
static int path_maw_k1[] = { 1, 0, 0};
static operand_desc maw_k1 = {operand_desc_path, 3, path_maw_k1}; 
static int path_maw_k2[] = { 1, 1, 0 };
static operand_desc maw_k2 = {operand_desc_path, 3, path_maw_k2}; 
static operand_desc* maw_kids[] = { &maw_k0, &maw_k1, &maw_k2};
static recog_rule rule_maw = { INTRINSIC_INVALID, "__builtin_MP1x_MAW",
                               EXTOPT_none, &pattern_maw, maw_kids };

static recog_rule rule_msw = { INTRINSIC_INVALID, "__builtin_MP1x_MSW",
                               EXTOPT_none, &pattern_msw, maw_kids };


static int path_mah_k0[] = { 0, 0, 0};
static operand_desc mah_k0 = {operand_desc_path, 3, path_mah_k0}; 
static int path_mah_k1[] = { 0, 1, 0, 0};
static operand_desc mah_k1 = {operand_desc_path, 4, path_mah_k1}; 
static int path_mah_k2[] = { 0, 1, 1, 0 };
static operand_desc mah_k2 = {operand_desc_path, 4, path_mah_k2}; 
static operand_desc mah_k3 = {operand_desc_imm,  0 }; // 0 means LL for msh/mah instructions
static operand_desc* mah_kids[] = { &mah_k0, &mah_k1, &mah_k2, &mah_k3};
static recog_rule rule_mah = { INTRINSIC_INVALID, "__builtin_MP1x_MAH",
                               EXTOPT_none, &pattern_mah, mah_kids };
static recog_rule rule_msh = { INTRINSIC_INVALID, "__builtin_MP1x_MSH",
                                 EXTOPT_none, &pattern_msh, mah_kids };

/* to be sorted by decreasing profit, it could be for example the
 * number of nodes in pattern ?
 */
static recog_rule*  mpx_recog_rules [] = {
  &rule_mafw,
  &rule_mah,
  &rule_msh,
  &rule_msfw,
  &rule_mpfw,
  &rule_shlrr2x_1,
  &rule_shlrr2x_2,
  &rule_shlrr2x_3,
  &rule_shrrx2r_1,
  &rule_shrrx2r_2,
  &rule_u8andcd,
  &rule_i8andcd,
  &rule_mph,
  &rule_mpw,
  &rule_maw,
  &rule_msw
};

static unsigned int mpx_recog_rules_count = 16;


