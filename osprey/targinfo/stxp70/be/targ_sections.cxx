/*

  Copyright (C) 2001 ST Microelectronics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information: 
  or:

  http://www.st.com

  For further information regarding this notice, see:


*/

#include <elf.h>
#include "defs.h"
#include "stab.h"
#include "stblock.h"
#include "config_target.h"
#include "config_TARG.h"
#include "data_layout.h"
#include "sections.h"
#include "be_symtab.h"
#include "targ_sections.h"
#include "targ_isa_bundle.h" // for ISA_MAX_INST_BYTES

/* ====================================================================
 *    SEC_is_gprel
 * ====================================================================
 */
BOOL SEC_is_gprel (SECTION_IDX sec)
{
  if (!Gen_GP_Relative)
    return FALSE;

  switch (sec)
    {
    case _SEC_DATA:
    case _SEC_RDATA:
    case _SEC_BSS:
      /* GP Relative when forced will convert any data/rdata/bss access
	 to GP relative. */
      return TRUE;
    default:
      return FALSE;
    }
}

/* ====================================================================
 *    Corresponding_Short_Section
 * ====================================================================
 */
SECTION_IDX
Corresponding_Short_Section (
  ST *st,
  SECTION_IDX sec
)
{
  /* clarkes:
   * No need to shorten the data sections on ST200, because
   * we can access the normal data sections GP-relative.
   * However, _SEC_RDATA is placed in the text segment, which
   * is not accessible GP-relative in the caller-sets-GP scheme,
   * so in that case, we do move data from _SEC_RDATA
   * to _SEC_SRDATA.
   */
   
  SECTION_IDX newsec;

  if (Is_Caller_Save_GP && sec == _SEC_RDATA)
    newsec = _SEC_SRDATA;
  else
    newsec = sec;
    
  if (SEC_is_gprel(newsec))
    Set_ST_gprel(st);
  return newsec;
}

/* ====================================================================
 *    Initv_Contains_Address
 * ====================================================================
 */
static BOOL
Initv_Contains_Address (INITV_IDX initv_idx)
{
  for ( ; initv_idx != INITV_IDX_ZERO; initv_idx = INITV_next (initv_idx)) {
    switch (INITV_kind(Initv_Table[initv_idx])) {
    case INITVKIND_SYMOFF:
    case INITVKIND_LABEL:
      return TRUE;
    case INITVKIND_ZERO:
    case INITVKIND_ONE:
    case INITVKIND_VAL:
    case INITVKIND_PAD:
      break;
    case INITVKIND_BLOCK:
      if (Initv_Contains_Address (INITV_blk (initv_idx)))
	return TRUE;
      else
	break;
    case INITVKIND_SYMDIFF:
    case INITVKIND_SYMDIFF16:
#ifdef TARG_ST
/* (cbr) DDTSst24451. add support for label diffs initializers */
    case INITVKIND_LABDIFF:
#endif
      break;
    default:
      FmtAssert(FALSE, ("Initializer_Contains_Address: unknown initvkind"));
      break;
    }
  }
  return FALSE;
}    

/* ====================================================================
 *    Initializer_Contains_Address
 * ====================================================================
 */
static BOOL
Initializer_Contains_Address (ST *st)
{
  INITV_IDX initv_idx;
  if (!ST_is_initialized (st)
      || ST_init_value_zero (st)
      || ST_class (st) == CLASS_CONST)
    return FALSE;

  initv_idx = ST_has_initv (st);

  if (initv_idx == 0)
    // Cannot find initializer.
    // Be pessimistic: assume it contains an address.  
    // This can happen for jump tables, as the initializer is not created
    // until after we have chosen the section for the jump table.
    // Another possibility here: look at the type of the symbol to see if
    // the type contains pointers.
    // FmtAssert(FALSE,("Initializer_Contains_Address: cannot find initializer"));
    return TRUE;

  return Initv_Contains_Address (initv_idx);
}
  
/* ====================================================================
 *    Assign_Static_Variable
 * ====================================================================
 */
SECTION_IDX 
Assign_Static_Variable (ST *st)
{
  SECTION_IDX sec;

  if (ST_is_thread_private(st)) {
    if ((ST_is_initialized(st) && !ST_init_value_zero (st))
	|| ST_has_named_section(st))
      sec = _SEC_TDATA;
    else
      sec = _SEC_TBSS;
  }
  else if (ST_is_initialized(st)
	   && !ST_init_value_zero (st))
    sec = (ST_is_constant(st)
	   && ! (Gen_GP_Relative && Initializer_Contains_Address (st)))
      ? _SEC_RDATA : _SEC_DATA;
  else
#ifdef TARG_ST
      // (cbr) for named sections are progbits 
    if (ST_has_named_section(st))
        sec = _SEC_DATA;
      else
#endif
    sec = _SEC_BSS;

  return sec;
}

/* ====================================================================
 *    Assign_Global_Variable
 * ====================================================================
 */
SECTION_IDX 
Assign_Global_Variable (
  ST *st, 
  ST *base_st
)
{
  SECTION_IDX sec;

  switch ( ST_sclass(base_st) ) {
  case SCLASS_UGLOBAL:
    if (ST_is_thread_private(st)) {
      if (ST_has_named_section(st))
	sec = _SEC_TDATA;
      else
	sec = _SEC_TBSS;
    } 
    else {
#ifdef TARG_ST
      // (cbr) for named sections are progbits 
      if (ST_has_named_section(st))
        sec = _SEC_DATA;
      else
#endif
        sec = _SEC_BSS;
    }
    break;

  case SCLASS_DGLOBAL:
    if (ST_is_thread_private(st)) {
      sec = _SEC_TDATA;
    }
    else if (ST_is_constant(st)
	     && !(Gen_GP_Relative && Initializer_Contains_Address (st)))
      sec = _SEC_RDATA;
    else sec = _SEC_DATA;
    break;

  default:
    /* Check if base_st is already a section, and if so return it */
    if (ST_class(base_st) == CLASS_BLOCK && STB_section(base_st)) {
      return STB_section_idx(base_st);
    }
    FmtAssert(FALSE,("Assign_Global_Variable: SCLASS %s unexpected for variable %s (base %s)", Sclass_Name(ST_sclass(base_st)), ST_name(st), ST_name(base_st)));
  }

  return sec;
}

// extern void dump_st(ST*); 

// (cbr) returns the memory_space for this symbol.
// either the one provided by the attribute or the one 
// given by the -Mtda/-Msda flags.
ST_MEMORY_SPACE
Get_Memory_Space(ST *st)
{
const int AllAlignment = 1|2|4|8; // 15

  extern int DA_Mem, DA_MinSize, DA_MaxSize;
  extern int SDA_Mem, SDA_MinSize, SDA_MaxSize;
  extern int TDA_Mem, TDA_MinSize, TDA_MaxSize;

  ST_MEMORY_SPACE variable_got_model=ST_MEMORY_NONE;

  TY_IDX ty = ST_type(st);
  ST_SCLASS sclass = ST_sclass (st);

  // VL DynLoad - GOT model takes the precedence over DA space
  variable_got_model=Get_Got_Model(st);
  if (variable_got_model!=ST_MEMORY_NONE) {
    return variable_got_model;
  }

  if (ST_sym_class (st) != CLASS_VAR) 
    return ST_MEMORY_NONE;

  if (sclass != SCLASS_UGLOBAL &&
      sclass != SCLASS_DGLOBAL &&
      sclass != SCLASS_COMMON &&
      sclass != SCLASS_PSTATIC &&
      sclass != SCLASS_FSTATIC &&
      sclass != SCLASS_EXTERN)
    return ST_MEMORY_NONE;    

  // if symbol has the attribute, return it.
  if (ST_memory_space(st) == ST_MEMORY_DEFAULT) {
    if (TY_size(ty)==0)
      {
        if (DA_MaxSize>0 || TDA_MaxSize>0 || SDA_MaxSize>0)
          {
            DevWarn(("Unknown size for extern type (cannot optimize DA/SDA/TDA)"));
          }
      }
    else {
      // Placement based on size (prioritary)
      if ((DA_MinSize<= TY_size(ty)) &&
          (DA_MaxSize>= TY_size(ty)))
        return ST_MEMORY_DA;
      if ((SDA_MinSize<= TY_size(ty)) &&
          (SDA_MaxSize>= TY_size(ty))) 
        return ST_MEMORY_SDA;
      if ((TDA_MinSize<= TY_size(ty)) &&
          (TDA_MaxSize>= TY_size(ty)))
        return ST_MEMORY_TDA;
    }

    // Placement based in alignement.
    if (DA_Mem==AllAlignment || DA_Mem & TY_align(ty))
      return ST_MEMORY_DA;
    if (SDA_Mem==AllAlignment || SDA_Mem & TY_align(ty))
      return ST_MEMORY_SDA;
    if (TDA_Mem==AllAlignment || TDA_Mem & TY_align(ty))
      return ST_MEMORY_TDA;
    
    return ST_MEMORY_NONE;
  }
   
  return ST_memory_space(st);

}

#define NB_MEMORY_SPACE 3
static const char *space_data[NB_MEMORY_SPACE] = {".da_data", ".sda_data", ".tda_data"};
static const char *space_ro[NB_MEMORY_SPACE] = {".da_ro", ".sda_ro", ".tda_ro"};
static const char *space_bss[NB_MEMORY_SPACE] = {".da_bss", ".sda_bss", ".tda_bss"};

/** 
 * Build memory space section name
 * 
 * @param st 
 * @param kind 
 * 
 * @return 
 */
STR_IDX 
Find_Memory_Space_Name (ST *st, ST_MEMORY_SPACE kind)
{
  STR_IDX name;
  const char *buf;
  TY_IDX ty = ST_type(st);
  UINT32 align = TY_smallest_align(ty);

  /* we don't want to create un-standard memory spaces */
  align = align>8?8:align;

  DevAssert((kind == ST_MEMORY_DA ||
             kind == ST_MEMORY_SDA ||
             kind == ST_MEMORY_TDA), ("Find_Memory_Space_Name for wrong kind"));

  switch (ST_sclass(st)) {
    /* [vcdv] section name may be bss even if sclass is not
       SCLASS_UGLOBAL */
  case SCLASS_DGLOBAL:
  case SCLASS_PSTATIC:
  case SCLASS_FSTATIC:
    if(!ST_is_initialized(st) || 
       (Zeroinit_in_bss && ST_is_initialized(st) && ST_init_value_zero(st))) {
      buf = space_bss[(int)kind-(int)ST_MEMORY_DA];
      break;
    }
  case SCLASS_EXTERN:
    if (TY_is_const (ty) || ST_is_const_var(st))
      buf = space_ro[(int)kind-(int)ST_MEMORY_DA];
    else
      buf = space_data[(int)kind-(int)ST_MEMORY_DA]; 
    break;
  case SCLASS_COMMON:
    Set_ST_sclass (st, SCLASS_UGLOBAL);
  case SCLASS_UGLOBAL:
    buf = space_bss[(int)kind-(int)ST_MEMORY_DA];
    break;
  default:
    FmtAssert(FALSE, ("unsupported storage class for memory space")); 
  }
  
  if (kind == ST_MEMORY_TDA) 
    name = Save_Str(buf);
  else
    name = Save_Stri(buf, align);

  return name;
}

/* ====================================================================
 * Returns TRUE if a section is a bss like section according to target
 * ABI or conventional usage.
 *
 * The whole function is a hack, but since memory spaces are only
 * defined for STxP70, it's difficult to think to a more generic
 * solution.
 * ====================================================================
 */
BOOL
Section_Is_Special_Bss_Like_Section(STR_IDX sec_name) 
{
   INT i;
   INT length;

   // Note: work on memory spaces hasn't begun yet for STxP70 v4.
   // But we're not going to define special sections with names
   // beginning with "da_bss" or "tda_bss" if these sections
   // aren't bss like sections!
   for(i=0;i<NB_MEMORY_SPACE;i++) {
      length = strlen(space_bss[i]);
      if(strncmp(Index_To_Str(sec_name), space_bss[i], length)==0) {
           return TRUE;
      }
    }

   return FALSE;
}

/* ====================================================================
 * Returns the got model (as ST_MEMORY_SPACE) for a st type
 * ====================================================================
 */

ST_MEMORY_SPACE
Get_Got_Model(ST *st)
{
  TY_IDX ty = ST_type(st);
  ST_SCLASS sclass = ST_sclass (st);
  ST_CLASS  symclass = ST_sym_class (st);

  if ((symclass != CLASS_VAR) && 
      (symclass != CLASS_CONST) &&  // For constant strings
      (symclass != CLASS_FUNC))     // For function pointers
    return ST_MEMORY_NONE;

  if (sclass != SCLASS_UGLOBAL &&
      sclass != SCLASS_DGLOBAL &&
      sclass != SCLASS_COMMON &&
      sclass != SCLASS_PSTATIC &&
      sclass != SCLASS_FSTATIC &&
      sclass != SCLASS_EXTERN &&
      sclass != SCLASS_TEXT)        // For function pointers
    return ST_MEMORY_NONE;    

  if (GOT_Model==got_small) return ST_MEMORY_GOTSMALL;
  else if (GOT_Model==got_standard) return ST_MEMORY_GOTSTD;
  else if (GOT_Model==got_large) return ST_MEMORY_GOTLARGE;
  else return ST_MEMORY_NONE;
}
