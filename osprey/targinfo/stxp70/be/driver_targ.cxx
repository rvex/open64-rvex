/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


#include "defs.h"
#include "errors.h"
#include "config.h"
#include "config_target.h"
#include "config_TARG.h"	// For Enable_Fpx, Enable_X3
#include "ti_init.h"

/* ====================================================================
 *   Initialize_Targ_Info
 * ====================================================================
 */

BE_EXPORTED void*
Initialize_Targ_Info (void)
{
  ABI_PROPERTIES_ABI abi;
  ISA_SUBSET isa;
  PROCESSOR proc;
  ISA_SUBSET isa_opt_subsets[ISA_SUBSET_COUNT_MAX];
  INT isa_opt_count = 0;

  switch (Target_ABI) {
  case ABI_STxP70_embedded:
    abi = ABI_PROPERTIES_ABI_embedded;
    break;
  case ABI_STxP70_fpx:
    abi = ABI_PROPERTIES_ABI_fpx;
    break;
  default:
    FmtAssert(FALSE, ("targinfo doesn't handle abi: %d\n", Target_ABI));
  }

  switch (Target_ISA) {
  case TARGET_ISA_stxp70_v3:
    isa = ISA_SUBSET_stxp70_v3;
    break;
  case TARGET_ISA_stxp70_v4:
    isa = ISA_SUBSET_stxp70_v4;
    break;
  default:
    FmtAssert(FALSE, ("targinfo doesn't handle isa: %s\n", Isa_Name(Target_ISA)));
  }

  switch (Target) {
  case TARGET_stxp70_v3:
    proc = PROCESSOR_stxp70_v3;
    break;
  case TARGET_stxp70_v4_novliw:
    proc = PROCESSOR_stxp70_v4_novliw;
    break;
  case TARGET_stxp70_v4_single:
    proc = PROCESSOR_stxp70_v4_single;
    break;
  case TARGET_stxp70_v4_dual:
    proc = PROCESSOR_stxp70_v4_dual;
    break;
  default:
    FmtAssert(FALSE, ("targinfo doesn't handle target: %s\n", Targ_Name(Target)));
  }

  /* Prepare native extensions or configuration options. */
  if ( Is_Target_ISA_stxp70_v3() ) {
    // FPX extension
    if (Enable_Fpx) isa_opt_subsets[isa_opt_count++] = ISA_SUBSET_stxp70_v3_ext_fpx;
    // X3 extension
    if (Enable_X3)  isa_opt_subsets[isa_opt_count++] = ISA_SUBSET_stxp70_v3_ext_x3;
  }
  
  /* Prepare native extensions or configuration options. */
  if ( Is_Target_ISA_stxp70_v4() ) {
    // X3 extension
    if (Enable_X3)  isa_opt_subsets[isa_opt_count++] = ISA_SUBSET_stxp70_v4_ext_x3;
  }
  
  return TI_Initialize(abi, isa, proc, isa_opt_count, isa_opt_subsets, Targ_Path);
}

