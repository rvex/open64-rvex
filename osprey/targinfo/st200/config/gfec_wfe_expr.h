#if defined(TARG_ST200)
    case BUILT_IN_ABSCH:
      iopc = INTRN_ABSCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ABSCL:
      iopc = INTRN_ABSCL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ABSCW:
      iopc = INTRN_ABSCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ABSH:
      iopc = INTRN_ABSH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ABSL:
      iopc = INTRN_ABSL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ABSW:
      iopc = INTRN_ABSW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDCH:
      iopc = INTRN_ADDCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDCL:
      iopc = INTRN_ADDCL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDCW:
      iopc = INTRN_ADDCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDD:
      iopc = INTRN_ADDD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDL:
      iopc = INTRN_ADDL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDS:
      iopc = INTRN_ADDS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ADDUL:
      iopc = INTRN_ADDUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITCLRH:
      iopc = INTRN_BITCLRH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITCLRW:
      iopc = INTRN_BITCLRW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITCNTH:
      iopc = INTRN_BITCNTH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITCNTW:
      iopc = INTRN_BITCNTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITNOTH:
      iopc = INTRN_BITNOTH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITNOTW:
      iopc = INTRN_BITNOTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITREVW:
      iopc = INTRN_BITREVW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITSETH:
      iopc = INTRN_BITSETH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITSETW:
      iopc = INTRN_BITSETW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITVALH:
      iopc = INTRN_BITVALH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BITVALW:
      iopc = INTRN_BITVALW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__DIVH:
      iopc = INTRN_BUILTIN__DIVH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__DIVUH:
      iopc = INTRN_BUILTIN__DIVUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__DIVUW:
      iopc = INTRN_BUILTIN__DIVUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__DIVW:
      iopc = INTRN_BUILTIN__DIVW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__MODH:
      iopc = INTRN_BUILTIN__MODH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__MODUH:
      iopc = INTRN_BUILTIN__MODUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__MODUW:
      iopc = INTRN_BUILTIN__MODUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_BUILTIN__MODW:
      iopc = INTRN_BUILTIN__MODW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_CLAMPLW:
      iopc = INTRN_CLAMPLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_CLAMPWH:
      iopc = INTRN_CLAMPWH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DISTH:
      iopc = INTRN_DISTH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DISTUH:
      iopc = INTRN_DISTUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DISTUW:
      iopc = INTRN_DISTUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DISTW:
      iopc = INTRN_DISTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVD:
      iopc = INTRN_DIVD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVFCH:
      iopc = INTRN_DIVFCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVFCM:
      iopc = INTRN_DIVFCM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVFCW:
      iopc = INTRN_DIVFCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVH:
      iopc = INTRN_DIVH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVL:
      iopc = INTRN_DIVL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVM:
      iopc = INTRN_DIVM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVS:
      iopc = INTRN_DIVS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVUH:
      iopc = INTRN_DIVUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVUL:
      iopc = INTRN_DIVUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVUM:
      iopc = INTRN_DIVUM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVUW:
      iopc = INTRN_DIVUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DIVW:
      iopc = INTRN_DIVW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DTOL:
      iopc = INTRN_DTOL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DTOS:
      iopc = INTRN_DTOS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DTOUL:
      iopc = INTRN_DTOUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DTOUW:
      iopc = INTRN_DTOUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_DTOW:
      iopc = INTRN_DTOW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EDGESH:
      iopc = INTRN_EDGESH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EDGESW:
      iopc = INTRN_EDGESW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EQD:
      iopc = INTRN_EQD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EQL:
      iopc = INTRN_EQL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EQS:
      iopc = INTRN_EQS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_EQUL:
      iopc = INTRN_EQUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GED:
      iopc = INTRN_GED ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GEL:
      iopc = INTRN_GEL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GES:
      iopc = INTRN_GES ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GETHH:
      iopc = INTRN_GETHH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GETHW:
      iopc = INTRN_GETHW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GETLH:
      iopc = INTRN_GETLH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GETLW:
      iopc = INTRN_GETLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GEUL:
      iopc = INTRN_GEUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GTD:
      iopc = INTRN_GTD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GTL:
      iopc = INTRN_GTL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GTS:
      iopc = INTRN_GTS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_GTUL:
      iopc = INTRN_GTUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSEQUW:
      iopc = INTRN_INSEQUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSEQW:
      iopc = INTRN_INSEQW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSGEUW:
      iopc = INTRN_INSGEUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSGEW:
      iopc = INTRN_INSGEW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSGTUW:
      iopc = INTRN_INSGTUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSGTW:
      iopc = INTRN_INSGTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSLEUW:
      iopc = INTRN_INSLEUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSLEW:
      iopc = INTRN_INSLEW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSLTUW:
      iopc = INTRN_INSLTUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSLTW:
      iopc = INTRN_INSLTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSNEUW:
      iopc = INTRN_INSNEUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_INSNEW:
      iopc = INTRN_INSNEW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LED:
      iopc = INTRN_LED ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LEL:
      iopc = INTRN_LEL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LES:
      iopc = INTRN_LES ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LEUL:
      iopc = INTRN_LEUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTD:
      iopc = INTRN_LTD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTL:
      iopc = INTRN_LTL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTOD:
      iopc = INTRN_LTOD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTOS:
      iopc = INTRN_LTOS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTS:
      iopc = INTRN_LTS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LTUL:
      iopc = INTRN_LTUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LZCNTH:
      iopc = INTRN_LZCNTH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LZCNTL:
      iopc = INTRN_LZCNTL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_LZCNTW:
      iopc = INTRN_LZCNTW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MADDS:
      iopc = INTRN_MADDS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAFCW:
      iopc = INTRN_MAFCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXD:
      iopc = INTRN_MAXD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXH:
      iopc = INTRN_MAXH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXL:
      iopc = INTRN_MAXL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXS:
      iopc = INTRN_MAXS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXUH:
      iopc = INTRN_MAXUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MAXUL:
      iopc = INTRN_MAXUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MIND:
      iopc = INTRN_MIND ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MINH:
      iopc = INTRN_MINH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MINL:
      iopc = INTRN_MINL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MINS:
      iopc = INTRN_MINS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MINUH:
      iopc = INTRN_MINUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MINUL:
      iopc = INTRN_MINUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODFCH:
      iopc = INTRN_MODFCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODFCM:
      iopc = INTRN_MODFCM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODFCW:
      iopc = INTRN_MODFCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODH:
      iopc = INTRN_MODH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODL:
      iopc = INTRN_MODL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODM:
      iopc = INTRN_MODM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODUH:
      iopc = INTRN_MODUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODUL:
      iopc = INTRN_MODUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODUM:
      iopc = INTRN_MODUM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODUW:
      iopc = INTRN_MODUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MODW:
      iopc = INTRN_MODW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPFCW:
      iopc = INTRN_MPFCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPFCWL:
      iopc = INTRN_MPFCWL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPFML:
      iopc = INTRN_MPFML ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPFRCH:
      iopc = INTRN_MPFRCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPML:
      iopc = INTRN_MPML ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MPUML:
      iopc = INTRN_MPUML ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MSUBS:
      iopc = INTRN_MSUBS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULD:
      iopc = INTRN_MULD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULFCH:
      iopc = INTRN_MULFCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULFCM:
      iopc = INTRN_MULFCM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULFCW:
      iopc = INTRN_MULFCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULH:
      iopc = INTRN_MULH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULHH:
      iopc = INTRN_MULHH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULHUH:
      iopc = INTRN_MULHUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULHUW:
      iopc = INTRN_MULHUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULHW:
      iopc = INTRN_MULHW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULL:
      iopc = INTRN_MULL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULM:
      iopc = INTRN_MULM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULN:
      iopc = INTRN_MULN ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULS:
      iopc = INTRN_MULS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULUH:
      iopc = INTRN_MULUH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULUL:
      iopc = INTRN_MULUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULUM:
      iopc = INTRN_MULUM ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULUN:
      iopc = INTRN_MULUN ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULUW:
      iopc = INTRN_MULUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_MULW:
      iopc = INTRN_MULW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEARCLW:
      iopc = INTRN_NEARCLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEARCWH:
      iopc = INTRN_NEARCWH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEARLW:
      iopc = INTRN_NEARLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEARWH:
      iopc = INTRN_NEARWH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NED:
      iopc = INTRN_NED ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEGCH:
      iopc = INTRN_NEGCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEGCL:
      iopc = INTRN_NEGCL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEGCW:
      iopc = INTRN_NEGCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEGL:
      iopc = INTRN_NEGL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEGUL:
      iopc = INTRN_NEGUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEL:
      iopc = INTRN_NEL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NES:
      iopc = INTRN_NES ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NEUL:
      iopc = INTRN_NEUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NORMH:
      iopc = INTRN_NORMH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NORML:
      iopc = INTRN_NORML ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NORMW:
      iopc = INTRN_NORMW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PRIORH:
      iopc = INTRN_PRIORH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PRIORL:
      iopc = INTRN_PRIORL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PRIORW:
      iopc = INTRN_PRIORW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PUTHL:
      iopc = INTRN_PUTHL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PUTHW:
      iopc = INTRN_PUTHW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PUTLL:
      iopc = INTRN_PUTLL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_PUTLW:
      iopc = INTRN_PUTLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROTLH:
      iopc = INTRN_ROTLH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROTLW:
      iopc = INTRN_ROTLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROUNDCLW:
      iopc = INTRN_ROUNDCLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROUNDCWH:
      iopc = INTRN_ROUNDCWH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROUNDLW:
      iopc = INTRN_ROUNDLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ROUNDWH:
      iopc = INTRN_ROUNDWH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHLCH:
      iopc = INTRN_SHLCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHLCW:
      iopc = INTRN_SHLCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHLL:
      iopc = INTRN_SHLL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHLUL:
      iopc = INTRN_SHLUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHLW:
      iopc = INTRN_SHLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRL:
      iopc = INTRN_SHRL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRRH:
      iopc = INTRN_SHRRH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRRW:
      iopc = INTRN_SHRRW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRUL:
      iopc = INTRN_SHRUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRUW:
      iopc = INTRN_SHRUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SHRW:
      iopc = INTRN_SHRW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SQRTD:
      iopc = INTRN_SQRTD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SQRTS:
      iopc = INTRN_SQRTS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220ADDCG:
      iopc = INTRN_ST220ADDCG ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200ADDCG:
      iopc = INTRN_ST200ADDCG ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220DIVS:
      iopc = INTRN_ST220DIVS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200DIVS:
      iopc = INTRN_ST200DIVS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULH:
      iopc = INTRN_ST200MULH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULHH:
      iopc = INTRN_ST200MULHH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220MULHHS:
      iopc = INTRN_ST220MULHHS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULHHS:
      iopc = INTRN_ST200MULHHS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220MULHS:
      iopc = INTRN_ST220MULHS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULHS:
      iopc = INTRN_ST200MULHS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULHU:
      iopc = INTRN_ST200MULHU ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULLH:
      iopc = INTRN_ST200MULLH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULLHU:
      iopc = INTRN_ST200MULLHU ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220MULLHUS:
      iopc = INTRN_ST220MULLHUS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULLHUS:
      iopc = INTRN_ST200MULLHUS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220MULLU:
      iopc = INTRN_ST220MULLU ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULLU:
      iopc = INTRN_ST200MULLU ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST220PFT:
      iopc = INTRN_ST220PFT ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST200PFT:
      iopc = INTRN_ST200PFT ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST220PRGADD:
      iopc = INTRN_ST220PRGADD ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST200PRGADD:
      iopc = INTRN_ST200PRGADD ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST220PRGSET:
      iopc = INTRN_ST220PRGSET ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST200PRGSET:
      iopc = INTRN_ST200PRGSET ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST220SYSCALL:
      iopc = INTRN_ST220SYSCALL ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_ST200SYSCALL:
      iopc = INTRN_ST200SYSCALL ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_STOD:
      iopc = INTRN_STOD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_STOL:
      iopc = INTRN_STOL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_STOUL:
      iopc = INTRN_STOUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_STOUW:
      iopc = INTRN_STOUW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_STOW:
      iopc = INTRN_STOW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBCH:
      iopc = INTRN_SUBCH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBCL:
      iopc = INTRN_SUBCL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBCW:
      iopc = INTRN_SUBCW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBD:
      iopc = INTRN_SUBD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBL:
      iopc = INTRN_SUBL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBS:
      iopc = INTRN_SUBS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SUBUL:
      iopc = INTRN_SUBUL ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SWAPBH:
      iopc = INTRN_SWAPBH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SWAPBW:
      iopc = INTRN_SWAPBW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SWAPHW:
      iopc = INTRN_SWAPHW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ULTOD:
      iopc = INTRN_ULTOD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ULTOS:
      iopc = INTRN_ULTOS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_UWTOD:
      iopc = INTRN_UWTOD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_UWTOS:
      iopc = INTRN_UWTOS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_WTOD:
      iopc = INTRN_WTOD ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_WTOS:
      iopc = INTRN_WTOS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_XSHLH:
      iopc = INTRN_XSHLH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_XSHLW:
      iopc = INTRN_XSHLW ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_XSHRH:
      iopc = INTRN_XSHRH ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_XSHRW:
      iopc = INTRN_XSHRW ;
      intrinsic_op = TRUE ;
    break;

    case BUILT_IN_ST200MUL32:
      iopc = INTRN_ST200MUL32 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MUL64H:
      iopc = INTRN_ST200MUL64H ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MUL64HU:
      iopc = INTRN_ST200MUL64HU ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ST200MULFRAC:
      iopc = INTRN_ST200MULFRAC ;
      intrinsic_op = TRUE ;
    break;

    case BUILT_IN_ST200PRGINSPG:
      iopc = INTRN_ST200PRGINSPG ;
      intrinsic_op = FALSE ;
    break;
    case BUILT_IN_NMADDS:
      iopc = INTRN_NMADDS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_NMSUBS:
      iopc = INTRN_NMSUBS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_SQUARES:
      iopc = INTRN_SQUARES ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_RECIPS:
      iopc = INTRN_RECIPS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_RSQRTS:
      iopc = INTRN_RSQRTS ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_0:
      iopc = INTRN_ASM_0 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_1:
      iopc = INTRN_ASM_1 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_2:
      iopc = INTRN_ASM_2 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_3:
      iopc = INTRN_ASM_3 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_4:
      iopc = INTRN_ASM_4 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_5:
      iopc = INTRN_ASM_5 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_6:
      iopc = INTRN_ASM_6 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_7:
      iopc = INTRN_ASM_7 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_8:
      iopc = INTRN_ASM_8 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_9:
      iopc = INTRN_ASM_9 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_10:
      iopc = INTRN_ASM_10 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_11:
      iopc = INTRN_ASM_11 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_12:
      iopc = INTRN_ASM_12 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_13:
      iopc = INTRN_ASM_13 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_14:
      iopc = INTRN_ASM_14 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_ASM_15:
      iopc = INTRN_ASM_15 ;
      intrinsic_op = TRUE ;
    break;
    case BUILT_IN_VA_START:
      iopc = INTRN_VA_START;
      break;

      {
	tree arg1, arg2;

        arg1 = TREE_VALUE (arglist);
	arg2 = TREE_VALUE (TREE_CHAIN (arglist));
	WN *arg_wn = WFE_Expand_Expr (arg1);
	TY_IDX va_list_ty_idx = Get_TY (va_list_type_node);
	TYPE_ID va_list_mtype = TY_mtype (arg_ty_idx);
	UINT ap_field_id = (Target_Byte_Sex == BIG_ENDIAN) ? 1 : 0;
	INT64 rounded_size;
	tree arg2_type = TREE_TYPE (arg2);

	while (TREE_CODE (arg2) == NOP_EXPR
	       || TREE_CODE (arg2) == CONVERT_EXPR
	       || TREE_CODE (arg2) == NON_LVALUE_EXPR
	       || TREE_CODE (arg2) == INDIRECT_REF)
	  arg2 = TREE_OPERAND (arg2, 0);
	ST *st2 = Get_ST (arg2);
	// Set rounded_size to be the difference between the address
	// of st2 and the address of the next argument slot.
	rounded_size = ST_size(st2);
	if (! TARGET_BIG_ENDIAN || AGGREGATE_TYPE_P (arg2_type)) {
	  // Big-endian scalars are placed at the end of their argument
	  // slot, so for those no need to pad up to word size.
	  // Everything else must be padded to word size.
	  rounded_size = (((rounded_size + UNITS_PER_WORD - 1) / UNITS_PER_WORD)
			  * UNITS_PER_WORD);
	}
	wn = WN_Lda (Pointer_Mtype, ST_ofst(st2), st2);
	wn = WN_Binary (OPR_ADD, Pointer_Mtype, wn,
			WN_Intconst (Pointer_Mtype, rounded_size));
	
	if (WN_operator (arg_wn) == OPR_LDA) {
	  wn = WN_Stid (Pointer_Mtype, WN_offset (arg_wn),
			WN_st (arg_wn), va_list_ty_idx, wn, ap_field_id);
	} else {
	  wn = WN_Istore (Pointer_Mtype, 0,
			  Make_Pointer_Type (va_list_ty_idx),
			  arg_wn, wn, ap_field_id);
	}
	WFE_Stmt_Append (wn, Get_Srcpos());
	if (TARGET_BIG_ENDIAN) {
	  // [SC] Need to find the address just above where the 8th
	  // (where 8 is MAX_ARGUMENT_SLOTS)
	  // argument register would be saved to the stack.
	  // We know that the fixed arguments occupy current_function_args_info
	  // registers, and that the symbol st2 occupies the last of these.
	  // Therefore the current_function_args_info + 1 register is
	  // saved in the word above st2, and the 8th register is at
	  // 8 - (current_function_args_info + 1) words from that.
	  // However, current_function_args_info saturates at 8.
	  // In this case, there will be no variable arguments passed in
	  // registers, and we can just use the word above st2 as the
	  // register argument limit.
	  // Beware: gcc code would here just use
	  // current_function_args_info to determine the number of
	  // register arguments.  However, current_function_args_info
	  // may not be available during whirl translation because
	  // cfun may have already been freed.
	  // So here attempt to recreate current_function_args_info.
	  const UINT reg_limit_field_id = 2;
	  int cum = 0;
	  tree current_fn_decl = Current_Function_Decl();
	  tree parm;
	  for (parm = DECL_ARGUMENTS (current_fn_decl);
	       parm != NULL;
	       parm = TREE_CHAIN (parm)) {
	    int words;
	    tree passed_type = DECL_ARG_TYPE (parm);
	    if (DECL_TRANSPARENT_UNION (parm)
		|| (TREE_CODE (passed_type) == UNION_TYPE
		    && TYPE_TRANSPARENT_UNION (passed_type))) {
	      passed_type = TREE_TYPE (TYPE_FIELDS (passed_type));
	    }
	    words = ((int_size_in_bytes (passed_type) + UNITS_PER_WORD - 1)
		     / UNITS_PER_WORD);
	    if (words > 1 && (cum & 1)) cum++;
	    cum += words;
	  }
	  if (cum > MAX_ARGUMENT_SLOTS) cum = MAX_ARGUMENT_SLOTS;

	  wn = WN_Binary (OPR_ADD, Pointer_Mtype,
			  WN_Lda (Pointer_Mtype, ST_ofst(st2), st2),
			  WN_Intconst (Pointer_Mtype,
				       UNITS_PER_WORD *
				       (MAX_ARGUMENT_SLOTS - cum)
				       + rounded_size));
	  if (WN_operator (arg_wn) == OPR_LDA) {
	    wn = WN_Stid (Pointer_Mtype,
			  WN_offset (arg_wn) + MTYPE_byte_size(Pointer_Mtype),
			  WN_st (arg_wn), va_list_ty_idx, wn,
			  reg_limit_field_id);
	  } else {
	    wn = WN_Istore (Pointer_Mtype, MTYPE_byte_size (Pointer_Mtype),
			    Make_Pointer_Type (va_list_ty_idx),
			    WN_COPY_Tree (arg_wn), wn, reg_limit_field_id);
	  }
	  WFE_Stmt_Append (wn, Get_Srcpos());
	}
	whirl_generated = TRUE;
	wn = NULL;
      }
    break;

#define INTRN_GFEC_WFE_EXPR
#include "gen_intrinsics.inc"
#undef INTRN_GFEC_WFE_EXPR
#endif /* defined(TARG_ST200) */
