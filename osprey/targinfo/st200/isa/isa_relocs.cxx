/* -- This file is automatically generated -- */ 
/* 

  Copyright (C) 2002, 2004 ST Microelectronics, Inc.  All Rights Reserved. 

  This program is free software; you can redistribute it and/or modify it 
  under the terms of version 2 of the GNU General Public License as 
  published by the Free Software Foundation. 
  This program is distributed in the hope that it would be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

  Further, this software is distributed without any warranty that it is 
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever. 
  You should have received a copy of the GNU General Public License along 
  with this program; if not, write the Free Software Foundation, Inc., 59 
  Temple Place - Suite 330, Boston MA 02111-1307, USA. 

  Contact information:  ST Microelectronics, Inc., 
  , or: 

  http://www.st.com 

  For further information regarding this notice, see: 

  http: 
*/ 

// AUTOMATICALLY GENERATED FROM MDS DATA BASE !!! 
//  
//  Generate relocations information
///////////////////////////////////////

#include <stdio.h>

#include <stddef.h>
#include "isa_relocs_gen.h"

main()
{
  ISA_Relocs_Begin();

  // ISA_Create_Reloc_Type( name, main symbol, second symbol, addend, pcrel,
  //                        gprel, gotrel)
  RELOC_TYPE rel_none = 
    ISA_Create_Reloc_Type("none",R_NO_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_NO_ADDEND,
			  R_NO_PCREL,R_NO_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_s = 
    ISA_Create_Reloc_Type("s",R_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_NO_ADDEND,
			  R_NO_PCREL,R_NO_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_a = 
    ISA_Create_Reloc_Type("a",R_NO_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_ADDEND,
			  R_NO_PCREL,R_NO_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_s_a = 
    ISA_Create_Reloc_Type("s_a",R_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_ADDEND,
			  R_NO_PCREL,R_NO_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_s_a_pc = 
    ISA_Create_Reloc_Type("s_a_pc",R_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_ADDEND,
			  R_PCREL,R_NO_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_s_a_gp = 
    ISA_Create_Reloc_Type("s_a_gp",R_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_ADDEND,
			  R_NO_PCREL,R_GPREL,R_NO_GOTREL);
  RELOC_TYPE rel_s_a_got = 
    ISA_Create_Reloc_Type("s_a_got",R_MAIN_SYMBOL,R_NO_SECOND_SYMBOL,R_ADDEND,
			  R_NO_PCREL,R_NO_GPREL,R_GOTREL);

  BITFIELD bf_12_9 = ISA_Create_BitField("bf_12_9",12,9);
  BITFIELD bf_0_16 = ISA_Create_BitField("bf_0_16",0,16);
  BITFIELD bf_0_23 = ISA_Create_BitField("bf_0_23",0,23);
  BITFIELD bf_0_32 = ISA_Create_BitField("bf_0_32",0,32);

  // ISA_VIRTUAL_RELOC_TYPE = ISA_Create_Reloc(
  //                            name,syntax, lit_class_type,
  //                            overflow, right_shift, underflow,
  //                             RELOC_TYPE, ... bitfields, BIT_FIELD_END) 

  ISA_VIRTUAL_RELOC_TYPE R_ST200_16  =
                   ISA_Create_Reloc(1,"R_ST200_16", "",
 		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_NO_RLLIB, 1, bf_0_16, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_32  =
                   ISA_Create_Reloc(2,"R_ST200_32", "",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_NO_RLLIB, 2, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_32_PCREL  = 
                   ISA_Create_Reloc(3,"R_ST200_32_PCREL","",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_pc, ISA_RELOC_NO_RLLIB, 3, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_23_PCREL  =
                   ISA_Create_Reloc(4,"R_ST200_23_PCREL","",
		   ISA_RELOC_OVERFLOW_SIGNED, ISA_RELOC_UNDERFLOW, 2,
		   rel_s_a_pc, ISA_RELOC_RLLIB_DYNPLT, 4, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_HI23  =
                   ISA_Create_Reloc(5,"R_ST200_HI23","",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_NO_RLLIB, 5, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_LO9  =
                   ISA_Create_Reloc(6,"R_ST200_LO9","",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_NO_RLLIB, 6, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GPREL_HI23  =
                   ISA_Create_Reloc(7,"R_ST200_GPREL_HI23","@gprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_gp, ISA_RELOC_RLLIB_GPREL, 7, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GPREL_LO9  =
                   ISA_Create_Reloc(8,"R_ST200_GPREL_LO9","@gprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_gp, ISA_RELOC_RLLIB_GPREL, 8, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_REL32  =
                   ISA_Create_Reloc(9,"R_ST200_REL32","",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_a, ISA_RELOC_NO_RLLIB, 9, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_HI23 =
                   ISA_Create_Reloc(10,"R_ST200_GOTOFF_HI23","@gotoff",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_got, ISA_RELOC_RLLIB_GOTOFFS, 10, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_LO9 = 
                   ISA_Create_Reloc(11,"R_ST200_GOTOFF_LO9","@gotoff",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_got, ISA_RELOC_RLLIB_GOTOFFS, 11, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFFX_HI23 = 
                   ISA_Create_Reloc(12,"R_ST200_GOTOFFX_HI23","@gotoffx",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_got, ISA_RELOC_RLLIB_GOTOFFS, 12, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFFX_LO9 =
                   ISA_Create_Reloc(13,"R_ST200_GOTOFFX_LO9","@gotoffx",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_got, ISA_RELOC_RLLIB_GOTOFFS, 13, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_LTV32 =
                   ISA_Create_Reloc(14,"R_ST200_LTV32","@ltv",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_NO_RLLIB, 14, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_SEGREL32 = 
                   ISA_Create_Reloc(15,"R_ST200_SEGREL32","@segrel",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 15, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_FPTR32 = 
                   ISA_Create_Reloc(16,"R_ST200_FPTR32","@fptr",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 16, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_PLTOFF_HI23 =
                   ISA_Create_Reloc(17,"R_ST200_PLTOFF_HI23","@pltoff",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 17, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_PLTOFF_LO9 = 
                   ISA_Create_Reloc(18,"R_ST200_PLTOFF_LO9","@pltoff",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 18, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_FPTR_HI23 =
                   ISA_Create_Reloc(19,"R_ST200_GOTOFF_FPTR_HI23",
                   "@gotoff(@fptr",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 17, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_FPTR_LO9 = 
                   ISA_Create_Reloc(20,"R_ST200_GOTOFF_FPTR_LO9",
                   "@gotoff(@fptr",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 18, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_IPLT =
                   ISA_Create_Reloc(21,"R_ST200_IPLT","@iplt",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 19, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_NEGGPREL_HI23 =
                   ISA_Create_Reloc(22,"R_ST200_NEGGPREL_HI23","@neggprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_gp, ISA_RELOC_RLLIB_NEGGPREL, 20, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_NEGGPREL_LO9 =
                   ISA_Create_Reloc(23,"R_ST200_NEGGPREL_LO9","@neggprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_gp, ISA_RELOC_RLLIB_NEGGPREL, 21, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_COPY =
                   ISA_Create_Reloc(24,"R_ST200_COPY","",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_none, ISA_RELOC_RLLIB_UNKNOWN, 22, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_JMP_SLOT =
                   ISA_Create_Reloc(25,"R_ST200_JMP_SLOT","",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s, ISA_RELOC_RLLIB_UNKNOWN, 22, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_TPREL_HI23 =
                   ISA_Create_Reloc(26,"R_ST200_TPREL_HI23","@tprel",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 23, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_TPREL_LO9 =
                   ISA_Create_Reloc(27,"R_ST200_TPREL_LO9","@tprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 24, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_TPREL32 =
                   ISA_Create_Reloc(28,"R_ST200_TPREL32","@tprel",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 25, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_TPREL_HI23 =
                   ISA_Create_Reloc(29,"R_ST200_GOTOFF_TPREL_HI23",
                   "@gotoff(@tprel",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_got, ISA_RELOC_RLLIB_UNKNOWN, 26, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_TPREL_LO9 =
                   ISA_Create_Reloc(30,"R_ST200_GOTOFF_TPREL_LO9",
                   "@gotoff(@tprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 27, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_DTPLDM_HI23 =
                   ISA_Create_Reloc(31,"R_ST200_GOTOFF_DTPLDM_HI23",
                   "@gotoff(@dtpldm",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a_got, ISA_RELOC_RLLIB_UNKNOWN, 28, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_DTPLDM_LO9 =
                   ISA_Create_Reloc(32,"R_ST200_GOTOFF_DTPLDM_LO9",
                   "@gotoff(@dtpldm",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a_got, ISA_RELOC_RLLIB_UNKNOWN, 29, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_DTPREL_HI23 =
                   ISA_Create_Reloc(33,"R_ST200_DTPREL_HI23","@dtprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 30, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_DTPREL_LO9 =
                   ISA_Create_Reloc(34,"R_ST200_DTPREL_LO9","@dtprel",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 31, bf_12_9, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_DTPMOD32 =
                   ISA_Create_Reloc(35,"R_ST200_DTPMOD32","@dtpmod",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 32, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_DTPREL32 =
                   ISA_Create_Reloc(36,"R_ST200_DTPREL32","@dtprel",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 33, bf_0_32, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_DTPNDX_HI23 =
                   ISA_Create_Reloc(37,"R_ST200_GOTOFF_DTPNDX_HI23",
                   "@gotoff(@dtpndx",
		   ISA_RELOC_OVERFLOW_BITFIELD, ISA_RELOC_NO_UNDERFLOW, 9,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 30, bf_0_23, BITFIELD_END);

  ISA_VIRTUAL_RELOC_TYPE R_ST200_GOTOFF_DTPNDX_LO9 = ISA_Create_Reloc(
                   38,"R_ST200_GOTOFF_DTPNDX_LO9",
                   "@gotoff(@dtpndx",
		   ISA_RELOC_NO_OVERFLOW, ISA_RELOC_NO_UNDERFLOW, 0,
		   rel_s_a, ISA_RELOC_RLLIB_UNKNOWN, 31, bf_12_9, BITFIELD_END);

  ///////////////////////////////////////////////////////
  // Defining subset relocations
  ///////////////////////////////////////////////////////

  ISA_Reloc_Subset(
    ISA_SUBSET_rvex,
    R_ST200_16,
    R_ST200_32,
    R_ST200_32_PCREL ,
    R_ST200_23_PCREL,
    R_ST200_HI23,
    R_ST200_LO9,
    R_ST200_GPREL_HI23,
    R_ST200_GPREL_LO9,
    R_ST200_REL32,
    R_ST200_GOTOFF_HI23,
    R_ST200_GOTOFF_LO9,
    R_ST200_GOTOFFX_HI23,
    R_ST200_GOTOFFX_LO9,
    R_ST200_LTV32,
    R_ST200_SEGREL32,
    R_ST200_FPTR32,
    R_ST200_PLTOFF_HI23,
    R_ST200_PLTOFF_LO9,
    R_ST200_GOTOFF_FPTR_HI23,
    R_ST200_GOTOFF_FPTR_LO9,
    R_ST200_IPLT,
    R_ST200_NEGGPREL_HI23,
    R_ST200_NEGGPREL_LO9,
    R_ST200_COPY,
    R_ST200_JMP_SLOT,
    R_ST200_TPREL_HI23,
    R_ST200_TPREL_LO9,
    R_ST200_TPREL32,
    R_ST200_GOTOFF_TPREL_HI23,
    R_ST200_GOTOFF_TPREL_LO9,
    R_ST200_GOTOFF_DTPLDM_HI23,
    R_ST200_GOTOFF_DTPLDM_LO9,
    R_ST200_DTPREL_HI23,
    R_ST200_DTPREL_LO9,
    R_ST200_DTPMOD32,
    R_ST200_DTPREL32,
    R_ST200_GOTOFF_DTPNDX_HI23,
    R_ST200_GOTOFF_DTPNDX_LO9,
    NULL);

  
  ISA_Reloc_Subset(
    ISA_SUBSET_rvex_fp,
    R_ST200_16,
    R_ST200_32,
    R_ST200_32_PCREL,
    R_ST200_23_PCREL,
    R_ST200_HI23,
    R_ST200_LO9,
    R_ST200_GPREL_HI23,
    R_ST200_GPREL_LO9,
    R_ST200_REL32,
    R_ST200_GOTOFF_HI23,
    R_ST200_GOTOFF_LO9,
    R_ST200_GOTOFFX_HI23,
    R_ST200_GOTOFFX_LO9,
    R_ST200_LTV32,
    R_ST200_SEGREL32,
    R_ST200_FPTR32,
    R_ST200_PLTOFF_HI23,
    R_ST200_PLTOFF_LO9,
    R_ST200_GOTOFF_FPTR_HI23,
    R_ST200_GOTOFF_FPTR_LO9,
    R_ST200_IPLT,
    R_ST200_NEGGPREL_HI23,
    R_ST200_NEGGPREL_LO9,
    R_ST200_COPY,
    R_ST200_JMP_SLOT,
    R_ST200_TPREL_HI23,
    R_ST200_TPREL_LO9,
    R_ST200_TPREL32,
    R_ST200_GOTOFF_TPREL_HI23,
    R_ST200_GOTOFF_TPREL_LO9,
    R_ST200_GOTOFF_DTPLDM_HI23,
    R_ST200_GOTOFF_DTPLDM_LO9,
    R_ST200_DTPREL_HI23,
    R_ST200_DTPREL_LO9,
    R_ST200_DTPMOD32,
    R_ST200_DTPREL32,
    R_ST200_GOTOFF_DTPNDX_HI23,
    R_ST200_GOTOFF_DTPNDX_LO9,
    NULL);

  ISA_Relocs_End();
}

