
#ifndef CONFIG_H
#define CONFIG_H

/* whether to use or not the host machine limits.h directly */
#define USE_H_LIMITS_H 1

/* whether to use or not the host machine errno.h directly */
#undef USE_H_ERRNO_H

/* whether to use or not the host machine math.h directly */
#undef USE_H_MATH_H

/* whether to use or not the host machine endian.h directly */
#undef USE_H_ENDIAN_H

/* whether to use or not the host machine stdint.h directly */
#define USE_H_STDINT_H

/* whether to use or not the host machine bstring.h directly */
#define USE_H_BSTRING_H 1

/* whether to use or not the host machine wait.h directly */
#undef USE_H_WAIT_H

/* whether to use or not the host machine sys/wait.h directly */
#undef USE_H_SYS_WAIT_H

/* whether to use or not the host machine alloca.h directly */
#undef USE_H_ALLOCA_H

/* whether to use or not the host machine values.h directly */
#undef USE_H_VALUES_H

#endif /* CONFIG_H */
