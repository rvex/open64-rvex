/* Include builtin types and operators */
#include <whirl2c.h>

/* Types */
/* File-level symbolic constants */
static _STRING anon = "basic.instr0";

static _STRING anon0 = "basic.c";

static _STRING anon1 = "main";

/* File-level vars and routines */
extern _INT32 main();

extern void __profile_invoke();

extern void __profile_init();

extern _UINT32 __profile_pu_init();

extern void __profile_invoke_init();

