#
#
#  Copyright (C) 2000, 2001 Silicon Graphics, Inc.  All Rights Reserved.
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of version 2 of the GNU General Public License as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it would be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
#
#  Further, this software is distributed without any warranty that it is
#  free of the rightful claim of any third person regarding infringement 
#  or the like.  Any license provided herein, whether implied or 
#  otherwise, applies only to this software file.  Patent licenses, if 
#  any, provided herein do not apply to combinations of this program with 
#  other software, or any other product whatsoever.  
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write the Free Software Foundation, Inc., 59
#  Temple Place - Suite 330, Boston MA 02111-1307, USA.
#
#  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
#  Mountain View, CA 94043, or:
#
#  http://www.sgi.com
#
#  For further information regarding this notice, see:
#
#  http://oss.sgi.com/projects/GenInfo/NoticeExplan
#
#

# Makefile for libiberty library

# What we're building, and where to find it.
TARGET      = libiberty.a
SRC_DIRS    = $(BUILD_BASE)

# where to find the proper libiberty config.h file ...
LIBIBERTY_CONFIG_DIR=$(BUILD_AREA)/include/libiberty

# stuff to be removed by clobber
LDIRT += $(TARGET)

# Makefile setup
include $(COMMONDEFS)

VPATH    =  $(SRC_DIRS)

# Compiler options
LCOPTS = $(STD_COMPILE_OPTS)
LCDEFS = $(HOSTDEFS) $(TARGDEFS)

# Since we must pick-up the config.h from a "host-dependant"
# directory, we must prevent cpp from searching in the directory where
# the c-file lives in the case of #include "config.h" ...
LCINCS = -Iquote -I$(LIBIBERTY_CONFIG_DIR)
LCINCS += -I$(BUILD_BASE)/../include -I$(BUILD_BASE)

HOSTDEFS += -DIN_GCC -DHAVE_CONFIG_H

# Defined in winnt.h for x-mingw32-gcc-3.4.2
ifneq ($(HOST_OS),MINGW)
HOSTDEFS += -DLONGLONG
endif

AR_FLAGS = rcs

OBJS =	alloca.o	\
	argv.o		\
	asprintf.o	\
	vasprintf.o	\
	basename.o	\
	choose-temp.o	\
	concat.o	\
	cp-demangle.o 	\
	cplus-dem.o 	\
	dyn-string.o	\
	fdmatch.o	\
	fibheap.o	\
	floatformat.o	\
	fnmatch.o	\
	getopt.o	\
	getopt1.o	\
	getpwd.o	\
	getruntime.o	\
	hex.o		\
        hashtab.o       \
	lbasename.o	\
	lrealpath.o	\
	make-temp-file.o \
	make-relative-prefix.o \
	md5.o		\
	mkstemps.o	\
	objalloc.o	\
	obstack.o	\
	partition.o	\
	pexecute.o	\
	physmem.o 	\
	safe-ctype.o 	\
	spaces.o 	\
	splay-tree.o	\
	strerror.o	\
	strsignal.o	\
	xatexit.o	\
	xexit.o		\
	xmalloc.o 	\
	xmemdup.o	\
	xstrdup.o	\
	xstrerror.o	\
	bcopy.o \
	bzero.o \
	bcmp.o \
	regex.o

install: last
default: local
local: all

all: $(TARGET)

$(TARGET): $(OBJS)
	rm -f $(TARGET)
	$(AR) $(AR_FLAGS) $(TARGET) $(OBJS)

last: local
	$(MAKE) make_deps

include $(COMMONRULES)
