
#ifndef H_SYS_WAIT_H
#define H_SYS_WAIT_H

#ifdef CROSS_COMPILE

#include <sys/wait.h>

#define WCOREFLAG 0x80

#endif /* CROSS_COMPILE */
#endif /* H_SYS_WAIT_H */
