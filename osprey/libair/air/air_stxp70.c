/* 

  Copyright (C) 2007 ST Microelectronics, Inc.  All Rights Reserved. 

  This program is free software; you can redistribute it and/or modify it 
  under the terms of version 2 of the GNU General Public License as 
  published by the Free Software Foundation. 
  This program is distributed in the hope that it would be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

  Further, this software is distributed without any warranty that it is 
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever. 
  You should have received a copy of the GNU General Public License along 
  with this program; if not, write the Free Software Foundation, Inc., 59 
  Temple Place - Suite 330, Boston MA 02111-1307, USA. 

  Contact information:  ST Microelectronics, Inc., 
  , or: 

  http://www.st.com 

  For further information regarding this notice, see: 

  http: 
*/

/* Assembly Intermediate Representation
 * STxP70 target specific header file
 */

/* Standard C header files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

/* Targinfo header files */
#include "topcode.h"
#include "targ_isa_print.h"
#include "targ_abi_properties.h"

/* libair header files */
#include "air.h"
#include "targ_air.h"

#include "targ_isa_selector.h"

/* AIR is strictly in C */
#ifdef __cplusplus
extern "C" {
#endif

#ifdef TARG_STxP70

/* Target specific initialization */
INT
AIR_target_init( void )
{
    return 0;
}

/* Target specific end */
INT
AIR_target_end( void )
{
    return 0;
}

int 
stxp70_pre_print_fct ( AIR_Print * print ) {
  if ( (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp16_jr_i9) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_stxp70_v4_v4_gp16_jr_i9) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp16_jr_i10) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_jr) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_jr) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_addur_i27) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_addur_i13) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_make32) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_make32) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp16_make32) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_cmp_i16_h_cmp_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_cmp_i8_b_cmp_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_cmp_i8_b_cmp_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_jrgtudec) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_jrgtudec) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_setle) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_setle) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_callr) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_callr) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_setls) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_setls) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp32_fork_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp48_fork_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp32_terminate_i3) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp48_terminate_i3) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp32_notifyl_i8_bb_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp48_notifyl_i8_bb_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp32_notifyl_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_x3_gp48_notifyl_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_addbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_maxbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_minbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_subbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_addbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_maxbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_minbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_subbp_i8) || 
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_addhp_i16) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_maxhp_i16) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_minhp_i16) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_subhp_i16) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_i3_i2_pre_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_i3_i3_post_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_i3_i2_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_r_post_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_i3_i3_post_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_r_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_sx_i3_i5_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_i3_i12_pre_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_i3_i13_post_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_i3_i12_dec_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_r_post_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_i3_i13_post_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_r_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_sx_i3_i15_inc_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_i3_i2_pre_dec_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_i3_i3_post_dec_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_i3_i2_dec_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_r_post_inc_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_i3_i3_post_inc_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_r_inc_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_lx_i3_i5_inc_r_i7_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_i3_i12_pre_dec_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_i3_i13_post_dec_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_i3_i12_dec_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_r_post_inc_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_i3_i13_post_inc_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_r_inc_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_lx_i3_i15_inc_r_i11_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_x_i14_w1_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_x_i14_r1_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_x_i8_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_x_i10_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp32_x_i5_r_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_x_i28_w1_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_x_i28_r1_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_x_i22_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_x_i24_r_i3_g) ||
       (Get_AIR_OP_TOP(print->air_inst) == TOP_v4_gp48_x_i19_r_r_i3_g)
       ) {
     print->hexaconstant = 0;
  }
  return 0;
}

#endif             /* TARG_STxP70 */
#ifdef __cplusplus
}
#endif
