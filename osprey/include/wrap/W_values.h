
#ifndef W_VALUES_H
#define W_VALUES_H

#include <host/config.h>

#ifndef USE_H_VALUES_H
/* use values.h provided with the system */
#include <values.h>
#else
/* use H_values.h wrapper */
#include "host/H_values.h"
#endif

#endif /* W_VALUES_H */
