
#ifndef W_UNISTD_H
#define W_UNISTD_H

#include <host/config.h>

#ifndef USE_H_UNISTD_H
#include <unistd.h>
#else
#include "host/H_unistd.h"
#endif

#endif /* W_UNISTD_H */
