
#ifndef W_DLFCN_H
#define W_DLFCN_H

#include <host/config.h>

#ifndef USE_H_DLFCN_H
#include <dlfcn.h>
#else
#include "host/H_dlfcn.h"
#endif

#endif /* W_DLFCN_H */
