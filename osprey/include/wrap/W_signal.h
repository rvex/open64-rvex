
#ifndef W_SIGNAL_H
#define W_SIGNAL_H

#include <host/config.h>

#ifndef USE_H_SIGNAL_H
#include <signal.h>
#else
#include "host/H_signal.h"
#endif

#endif /* W_SIGNAL_H */
