/* Generated automatically by the program `genconstants'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CONSTANTS_H
#define GCC_INSN_CONSTANTS_H

#define CC_REGNUM 24
#define VUNSPEC_BLOCKAGE 0
#define UNSPEC_CHECK_ARCH 7
#define VUNSPEC_POOL_2 5
#define LAST_ARM_REGNUM 15
#define IP_REGNUM 12
#define LR_REGNUM 14
#define UNPSEC_COS 1
#define UNSPEC_SIN 0
#define UNSPEC_PUSH_MULT 2
#define VUNSPEC_POOL_END 3
#define VUNSPEC_POOL_1 4
#define SP_REGNUM 13
#define VUNSPEC_EPILOGUE 1
#define VUNSPEC_ALIGN 2
#define VUNSPEC_POOL_4 6
#define VUNSPEC_POOL_8 7
#define UNSPEC_PIC_SYM 3
#define UNSPEC_PRLG_STK 5
#define UNSPEC_PIC_BASE 4
#define PC_REGNUM 15
#define UNSPEC_CLZ 5
#define UNSPEC_PROLOGUE_USE 6

#endif /* GCC_INSN_CONSTANTS_H */
