/* This file is machine generated.  Do not edit.  */
static const char *srcdir = 
"../../gcc"
;
static const char *lang_files[] = {
"../../gcc/ada/ada-tree.h", 
"../../gcc/ada/gigi.h", 
"../../gcc/ada/decl.c", 
"../../gcc/ada/trans.c", 
"../../gcc/ada/utils.c", 
"../../gcc/cp/mangle.c", 
"../../gcc/cp/cp-tree.h", 
"../../gcc/cp/decl.h", 
"../../gcc/cp/lex.h", 
"../../gcc/cp/call.c", 
"../../gcc/cp/decl.c", 
"../../gcc/cp/decl2.c", 
"../../gcc/cp/parse.y", 
"../../gcc/cp/pt.c", 
"../../gcc/cp/repo.c", 
"../../gcc/cp/spew.c", 
"../../gcc/cp/tree.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/f/com.c", 
"../../gcc/f/com.h", 
"../../gcc/f/ste.c", 
"../../gcc/f/where.h", 
"../../gcc/f/where.c", 
"../../gcc/f/lex.c", 
"../../gcc/java/java-tree.h", 
"../../gcc/java/builtins.c", 
"../../gcc/java/class.c", 
"../../gcc/java/constants.c", 
"../../gcc/java/decl.c", 
"../../gcc/java/expr.c", 
"../../gcc/java/jcf-parse.c", 
"../../gcc/java/jcf-write.c", 
"../../gcc/java/lang.c", 
"../../gcc/java/mangle.c", 
"../../gcc/java/parse.y", 
"../../gcc/objc/objc-act.h", 
"../../gcc/c-parse.in", 
"../../gcc/c-tree.h", 
"../../gcc/c-decl.c", 
"../../gcc/c-objc-common.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/c-parse.in", 
"../../gcc/c-lang.c", 
"../../gcc/c-parse.in", 
"../../gcc/c-tree.h", 
"../../gcc/c-decl.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/c-objc-common.c", 
NULL};
static const char *langs_for_lang_files[] = {
"ada", 
"ada", 
"ada", 
"ada", 
"ada", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"cp", 
"f", 
"f", 
"f", 
"f", 
"f", 
"f", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"java", 
"objc", 
"objc", 
"objc", 
"objc", 
"objc", 
"objc", 
"objc", 
"objc", 
"objc", 
"c", 
"c", 
"c", 
"c", 
"c", 
"c", 
"c", 
"c", 
NULL};
static const char *all_files[] = {
"config.h", 
"auto-host.h", 
"../../gcc/../include/ansidecl.h", 
"../../gcc/config/dbxelf.h", 
"../../gcc/config/elfos.h", 
"../../gcc/config/arm/unknown-elf.h", 
"../../gcc/config/arm/elf.h", 
"../../gcc/config/arm/aout.h", 
"../../gcc/config/arm/arm.h", 
"../../gcc/defaults.h", 
"../../gcc/defaults.h", 
"../../gcc/location.h", 
"../../gcc/../include/hashtab.h", 
"../../gcc/bitmap.h", 
"../../gcc/function.h", 
"../../gcc/rtl.h", 
"../../gcc/optabs.h", 
"../../gcc/tree.h", 
"../../gcc/libfuncs.h", 
"../../gcc/hashtable.h", 
"../../gcc/real.h", 
"../../gcc/varray.h", 
"../../gcc/ssa.h", 
"../../gcc/insn-addr.h", 
"../../gcc/cselib.h", 
"../../gcc/c-common.h", 
"../../gcc/c-tree.h", 
"../../gcc/basic-block.h", 
"../../gcc/alias.c", 
"../../gcc/bitmap.c", 
"../../gcc/cselib.c", 
"../../gcc/dwarf2out.c", 
"../../gcc/emit-rtl.c", 
"../../gcc/except.c", 
"../../gcc/explow.c", 
"../../gcc/expr.c", 
"../../gcc/fold-const.c", 
"../../gcc/function.c", 
"../../gcc/gcse.c", 
"../../gcc/integrate.c", 
"../../gcc/lists.c", 
"../../gcc/optabs.c", 
"../../gcc/profile.c", 
"../../gcc/ra-build.c", 
"../../gcc/regclass.c", 
"../../gcc/reg-stack.c", 
"../../gcc/sdbout.c", 
"../../gcc/stmt.c", 
"../../gcc/stor-layout.c", 
"../../gcc/tree.c", 
"../../gcc/varasm.c", 
"../../gcc/config/arm/arm.c", 
"../../gcc/ada/ada-tree.h", 
"../../gcc/ada/gigi.h", 
"../../gcc/ada/decl.c", 
"../../gcc/ada/trans.c", 
"../../gcc/ada/utils.c", 
"../../gcc/cp/mangle.c", 
"../../gcc/cp/cp-tree.h", 
"../../gcc/cp/decl.h", 
"../../gcc/cp/lex.h", 
"../../gcc/cp/call.c", 
"../../gcc/cp/decl.c", 
"../../gcc/cp/decl2.c", 
"../../gcc/cp/parse.y", 
"../../gcc/cp/pt.c", 
"../../gcc/cp/repo.c", 
"../../gcc/cp/spew.c", 
"../../gcc/cp/tree.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/f/com.c", 
"../../gcc/f/com.h", 
"../../gcc/f/ste.c", 
"../../gcc/f/where.h", 
"../../gcc/f/where.c", 
"../../gcc/f/lex.c", 
"../../gcc/java/java-tree.h", 
"../../gcc/java/builtins.c", 
"../../gcc/java/class.c", 
"../../gcc/java/constants.c", 
"../../gcc/java/decl.c", 
"../../gcc/java/expr.c", 
"../../gcc/java/jcf-parse.c", 
"../../gcc/java/jcf-write.c", 
"../../gcc/java/lang.c", 
"../../gcc/java/mangle.c", 
"../../gcc/java/parse.y", 
"../../gcc/objc/objc-act.h", 
"../../gcc/c-parse.in", 
"../../gcc/c-tree.h", 
"../../gcc/c-decl.c", 
"../../gcc/c-objc-common.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/c-parse.in", 
"../../gcc/c-lang.c", 
"../../gcc/c-parse.in", 
"../../gcc/c-tree.h", 
"../../gcc/c-decl.c", 
"../../gcc/c-common.c", 
"../../gcc/c-common.h", 
"../../gcc/c-pragma.c", 
"../../gcc/c-objc-common.c", 
 NULL};
static const char *lang_dir_names[] = { "c", 
"ada", 
"cp", 
"f", 
"java", 
"objc", 
NULL};
