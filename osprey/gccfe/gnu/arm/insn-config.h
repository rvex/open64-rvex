/* Generated automatically by the program `genconfig'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CONFIG_H
#define GCC_INSN_CONFIG_H

#define MAX_RECOG_OPERANDS 30
#define MAX_DUP_OPERANDS 6
#ifndef MAX_INSNS_PER_SPLIT
#define MAX_INSNS_PER_SPLIT 3
#endif
#define HAVE_conditional_move 1
#define HAVE_conditional_execution 1
#if 0
/* [JV] No peephole when connected to O64. */
#define HAVE_peephole 1
#define HAVE_peephole2 1
#define MAX_INSNS_PER_PEEP2 2
#endif

#endif /* GCC_INSN_CONFIG_H */
