/* auto-host.h.  Generated automatically by configure.  */
/* config.in.  Generated automatically from configure.in by autoheader.  */
/* Define if you can safely include both <string.h> and <strings.h>.  */

/* Define to the name of a file containing a list of extra machine modes
   for this architecture. */
#define EXTRA_MODES_FILE "stxp70/stxp70-modes.def"
