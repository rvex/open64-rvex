/*
 * Copyright (C) 2007 Pathscale, LLC. All Rights Reserved.
 */

/*
 * Copyright (C) 2006, 2007. QLogic Corporation. All Rights Reserved.
 */

/* 
   Copyright 2003, 2004, 2005, 2006 PathScale, Inc.  All Rights Reserved.
   File modified June 20, 2003 by PathScale, Inc. to update Open64 C/C++ 
   front-ends to GNU 3.2.2 release.
 */

/*

  Copyright (C) 2000, 2001 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


/* ====================================================================
 * ====================================================================
 *
 * Module: dst.c
 *
 * Revision history:
 *  01-May-93 - Original Version
 *
 * Description:
 *  Implements generation of dwarf debugging information be means of
 *  ../common/com/dwarf_DST_producer.h.  The information is generated
 *  by starting with the outermost scope (il_header.primary_scope) and
 *  handling nested scopes as they are encountered. 
 *
 *  Note how we mark nodes that have no DST_INFO_IDX associated with
 *  them as visited by means of a static file-scope variable 
 *  (e.g. void_is_visited).  This is necessary when we want to mark
 *  a type of edg node as visited, without a ASSOC_DST field.
 * ====================================================================
 * ====================================================================
 */

static char *source_file = __FILE__;
#ifdef _KEEP_RCS_ID
#endif /* _KEEP_RCS_ID */

extern "C"{
#include "gspin-wgen-interface.h"
}

#if defined(BUILD_OS_DARWIN)
#include <limits.h>
#else /* defined(BUILD_OS_DARWIN) */
#include "W_values.h"
#endif /* defined(BUILD_OS_DARWIN) */

#include "defs.h"
#include "glob.h"
#include "config.h"
#include "dwarf_DST_producer.h"
#include "dwarf_DST_dump.h"
#include "file_util.h"  /* From common/util */
#include "srcpos.h"
#include "symtab.h"

#include "wgen_misc.h"
#include "wgen_dst.h"
#include "wgen_expr.h"
#include "wgen_spin_symbol.h"

#include <sys/types.h>
#include <sys/stat.h>   /* For accessing file statistics (stat()) */
#ifdef TARG_ST
#  ifdef sun
#    include <netdb.h>      /* define MAXHOSTNAMELEN on Solaris */
#  else
#    ifndef __MINGW32__
#      include <sys/param.h>  /* define MAXHOSTNAMELEN elsewhere  */
#    else
#      include <winsock.h>
#      define MAXHOSTNAMELEN 256
#    endif
#  endif
#else
#  include <sys/param.h>  /* For MAXHOSTNAMELEN */
#endif
#include <unistd.h>     /* for gethostname() and getcwd() */
#include <string>
#include <vector>
#include <map>
#ifdef KEY
#include "stamp.h"	/* For INCLUDE_STAMP */
#include "demangle.h"
extern "C" char *cplus_demangle (const char *, int);
#endif
#ifdef TARG_ST
#include "libiberty.h"
#endif

/*
extern FILE *tree_dump_file; //for debugging only
*/

static BOOL dst_initialized = FALSE;


#ifdef TARG_ST
#  if defined(sun)
#    include <sys/systeminfo.h>
#    define getdomainname(x,y) sysinfo(SI_SRPC_DOMAIN,x,y)
#  endif
#  if defined(__CYGWIN__) || defined(__MINGW32__)
#    include <sys/unistd.h>
#    define getdomainname(x,y) gethostname(x,y)
#  endif
#endif

#ifdef KEY
// This buffer may contain host names but also dir names
static char *cwd_buffer = NULL;
static char *current_working_dir = NULL;
static char *current_host_dir = NULL;
#else
#define MAX_CWD_CHARS (256 - (MAXHOSTNAMELEN+1))
static char  cwd_buffer[MAX_CWD_CHARS+MAXHOSTNAMELEN+1];
static char *current_working_dir = &cwd_buffer[0];
static char *current_host_dir = &cwd_buffer[0];
#endif


// A file-global current scope is not useful, as with gcc we see
// declarations out of context and must derive right context.
//static DST_INFO_IDX current_scope_idx = DST_INVALID_INIT;

static DST_INFO_IDX comp_unit_idx = DST_INVALID_INIT;	// Compilation unit

static void DST_enter_file (char *, UINT);

static UINT last_dir_num = 0;
static UINT current_dir = 1;
static UINT last_file_num = 0;
UINT current_file = 1;

static DST_INFO_IDX DST_Create_var(ST *var_st, gs_t decl);
static DST_INFO_IDX DST_Create_Parmvar(ST *var_st, gs_t decl);
static DST_INFO_IDX DST_Create_type(ST *typ_decl, gs_t decl);
static void DST_enter_param_vars(gs_t fndecl,DST_INFO_IDX parent,gs_t parameter_list,int is_abstract_root, int is_declaration_only);


static std::vector< std::pair< char *, UINT > > dir_dst_list;
typedef std::map< std::string, DST_INFO_IDX > DST_Type_Map;
static DST_Type_Map basetypes;

#ifdef KEY
static DST_accessibility
#ifdef TARG_ST
get_dwarf_access (gs_t t, DST_accessibility def = DW_ACCESS_public)
#else
get_dwarf_access (gs_t t)
#endif
{
  unsigned int flag0, flag1, code;
  flag0 = gs_dwarf_access_flag_0(t);
  flag1 = gs_dwarf_access_flag_1(t);
  code = 2 * flag1 + flag0;
  switch (code) {
    case 1: return DW_ACCESS_public;
    case 2: return DW_ACCESS_protected;
    case 3: return DW_ACCESS_private;
  }
#ifdef TARG_ST
  return def ;
#else
  return 0;
#endif
}

// Returns true if type_tree has a DECL_ORIGINAL_TYPE, which implies this
// node is a typedef.
static inline BOOL is_typedef (gs_t type_tree)
{
  gs_t tname = gs_type_name (type_tree);
  return (tname && gs_tree_code (tname) == GS_TYPE_DECL &&
          gs_decl_original_type (tname));
}
#endif

#ifdef TARG_ST
// [CL] support lexical blocks
DST_INFO_IDX DST_Create_Lexical_Block(LEXICAL_BLOCK_INFO* lexical_block)
{
  DST_INFO_IDX dst;

  dst = DST_mk_lexical_block(NULL,
                             &lexical_block->lexical_block_start_idx,
                             &lexical_block->lexical_block_end_idx,
			     DST_INVALID_IDX);
  return dst;
}

void DST_Link_Lexical_Block(LEXICAL_BLOCK_INFO* parent, LEXICAL_BLOCK_INFO* child)
{
  DST_INFO_IDX dst, prev_dst;
  dst = child->dst;
  prev_dst = parent->dst;
  // [SC] Label information is now correct in the child, so
  // transfer it to the DST structure.
  DST_lexical_block_add_low_pc (dst, (void *)child->lexical_block_start_idx);
  DST_lexical_block_add_high_pc (dst, (void *)child->lexical_block_end_idx);
  DST_append_child(prev_dst, dst);
}
#endif

// Use DECL_CONTEXT or TYPE_CONTEXT to get
// the  index of the current applicable scope
// for the thing indicated.
// We already got to TYPE_CONTEXT/DECL_CONTEXT on the input
//   The TYPE_CONTEXT for any sort of type which could have a name or
//    which could have named members (e.g. tagged types in C/C++) will
//    point to the node which represents the scope of the given type, or
//    will be NULL_TREE if the type has "file scope".  For most types, this
//    will point to a BLOCK node or a FUNCTION_DECL node, but it could also
//    point to a FUNCTION_TYPE node (for types whose scope is limited to the
//    formal parameter list of some function type specification) or it
//    could point to a RECORD_TYPE, UNION_TYPE or QUAL_UNION_TYPE node
//    (for C++ "member" types).
//   DECL_CONTEXT points to the node representing the context in which
//    this declaration has its scope.  For FIELD_DECLs, this is the
//    RECORD_TYPE, UNION_TYPE, or QUAL_UNION_TYPE node that the field
//    is a member of.  For VAR_DECL, PARM_DECL, FUNCTION_DECL, LABEL_DECL,
//    and CONST_DECL nodes, this points to either the FUNCTION_DECL for the
//    containing function, the RECORD_TYPE or UNION_TYPE for the containing
//    type, or NULL_TREE if the given decl has "file scope".


static DST_INFO_IDX 
DST_get_context(gs_t intree)
{
    gs_t ltree = intree;
    DST_INFO_IDX l_dst_idx = DST_INVALID_INIT;
    bool continue_looping = true;

    while(ltree && continue_looping) {
	continue_looping = false;
	switch(gs_tree_code(ltree)) {
	case GS_BLOCK:
#ifndef TARG_ST // [CL] handle block tree
	  ltree =  BLOCK_SUPERCONTEXT(ltree);
	  break;
#else
	    // TODO: unclear when this will happen, as yet
	    DevWarn("Unhandled BLOCK scope of decl/var");
	    return comp_unit_idx;
#endif
	case GS_FUNCTION_DECL:
	    // This is a normal case!
	    l_dst_idx = DECL_DST_IDX(ltree);
	    if (DST_IS_NULL(l_dst_idx)) {
		DevWarn("forward reference to subprogram!"
			" assuming global context\n");
		return comp_unit_idx;
	    }
	    return l_dst_idx;
	case GS_RECORD_TYPE:
	case GS_UNION_TYPE:
	case GS_QUAL_UNION_TYPE:
#ifdef TARG_ST // [CL]
	    l_dst_idx = TYPE_DST_IDX(ltree);
	    if (DST_IS_NULL(l_dst_idx)) {
		DevWarn("NO TYPE CONTEXT");
		return comp_unit_idx;
	    }
	    return l_dst_idx;
#else
	    ltree = gs_type_context(ltree);
	    continue;
#endif
	case GS_FUNCTION_TYPE:
	    DevWarn("Unhandled FUNCTION_TYPE scope of decl/var/type");
	    return comp_unit_idx;
	case GS_REFERENCE_TYPE:
	    // cannot find our context from here
	    return comp_unit_idx;
	case GS_NAMESPACE_DECL:
	case GS_TRANSLATION_UNIT_DECL:
	    // I see these a lot: catching here to avoid default DevWarn
	    ltree = gs_decl_context(ltree);
	    continue;
	default:
#ifndef TARG_ST
	    DevWarn("Unhandled scope of tree code %d", gs_tree_code(ltree));
#endif
	    // Best guess for general types and decls
	    if (gs_tree_code_class(ltree) == GS_TCC_DECLARATION) {
		ltree = gs_decl_context(ltree);
		continue_looping = true;
		continue;
	    } else if (gs_tree_code_class(ltree) == GS_TCC_TYPE) {
		ltree = gs_type_context(ltree);
		continue_looping = true;
		continue;
	    }
	    // else: cannot find our context from here
	    return comp_unit_idx;
	}
    }
    // This is the normal case for most things.
    return comp_unit_idx;
}


// get the directory path dst info.
// if already exists, return existing info, else append to list.
static UINT
Get_Dir_Dst_Info (char *name)
{
// [CL] merged from Open64-4.2.1, for bug #55000
#ifdef TARG_SL
	// NOTE! Wenbo/2007-04-26: Refer to kgccfe/wfe_dst.cxx.
        if (name == NULL) return 0;
#endif
        std::vector< std::pair < char*, UINT > >::iterator found;
	// assume linear search is okay cause list will be small?
        for (found = dir_dst_list.begin(); 
		found != dir_dst_list.end(); 
		++found)
        {
		if (strcmp ((*found).first, name) == 0) {
			return (*found).second;
		}
	}
	// not found, so append path to dst list
#ifdef KEY
#ifdef TARG_ST
	// //[CM] (MBTst16964, MBTst16965) Extend name livetime
	name = xstrdup(name);
#else
	// We have to create a new home for name because memory
	// will be freed once the caller exits. 
	char *new_name = (char *)malloc((strlen(name)+1)*sizeof(char));
	strcpy(new_name, name);
	name = new_name;
#endif
#endif
	dir_dst_list.push_back (std::make_pair (name, ++last_dir_num));
	DST_mk_include_dir (name);
	return last_dir_num;
}

#ifdef TARG_ST
// [CL] bug #64842
static std::vector< std::pair< char *, std::pair <UINT, UINT> > > file_dst_list;
#else
static std::vector< std::pair< char *, UINT > > file_dst_list;
#endif

// get the file dst info.
// if already exists, return existing info, else append to list.
static UINT
Get_File_Dst_Info (char *name, UINT dir)
{
#ifdef KEY
        if (name[0] == '\0') // empty file name from g++ 3.4
          return last_file_num;
#endif
#ifdef TARG_ST
// [CL] bug #64842
        std::vector< std::pair < char*, std::pair <UINT, UINT> > >::iterator found;
#else
        std::vector< std::pair < char*, UINT > >::iterator found;
#endif
	// assume linear search is okay cause list will be small?
        for (found = file_dst_list.begin(); 
		found != file_dst_list.end(); 
		++found)
        {
#ifdef TARG_ST
// [CL] bug #64842: don't compare the filename only, check the directory too
	  if ( (strcmp ((*found).first, name) == 0)
	       && ((*found).second.second == dir)) {
	    return (*found).second.first;
	  }
#else
		if (strcmp ((*found).first, name) == 0) {
			return (*found).second;
		}
#endif
	}
	// not found, so append file to dst list
#ifdef KEY
	// We have to create a new home for name because memory
	// will be freed once the caller exits. 
#ifdef TARG_ST
	name = xstrdup(name);
#else
	char *new_name = (char *)malloc((strlen(name)+1)*sizeof(char));
	strcpy(new_name, name);
	name = new_name;
#endif
#endif
#ifdef TARG_ST
	// [CL] bug #64842
	file_dst_list.push_back (std::make_pair (name, std::make_pair (++last_file_num, dir)));
#else
	file_dst_list.push_back (std::make_pair (name, ++last_file_num));
#endif
	DST_enter_file (name, dir);
	return last_file_num;
}


// drops path prefix in string 
static char *
drop_path (char *s)
{
        char *tail;
        tail = strrchr (s, '/');
        if (tail == NULL) {
                return s;       // no path prefix 
        } else {
                tail++;         // skip the slash 
                return tail;    // points inside s, not new string! 
        }
}

static void
DST_enter_file (char *file_name, UINT dir)
{
        UINT64 file_size = 0;
        UINT64 fmod_time = 0;
        struct stat fstat;
        if (stat(file_name, &fstat) == 0) {
                // File was found, so set to non-zero values 
                file_size = (UINT64)fstat.st_size;
                fmod_time = (UINT64)fstat.st_mtime;
        }
        DST_mk_file_name(
                file_name,
                dir,
                file_size,
                fmod_time);
}

/* Given the set of options passed into the front-end, string
 * together the ones of interest for debugging and return
 * the resultant string.  The options of interest depends on 
 * the level of debugging.  The caller should free the malloced
 * string once it is no longer needed.
 */
static char *
DST_get_command_line_options(INT32 num_copts, 
			     char *copts[])
{
  INT32	    i, 
            strlength = 0;
  INT32     num_opts = 0;
  char    **selected_opt;
  INT32    *opt_size;
  char     *rtrn, *cp;
  char      ch;
  INT32     record_option;

#ifdef TARG_ST
  selected_opt = (char **)xmalloc(sizeof(char*) * num_copts);
  opt_size = (INT32 *)xmalloc(sizeof(INT32) * num_copts);
#else
  selected_opt = (char **)malloc(sizeof(char*) * num_copts);
  opt_size = (INT32 *)malloc(sizeof(INT32) * num_copts);
#endif
  
  for (i = 1; i <= num_copts; i++)
  {
     if (copts[i] != NULL && copts[i][0] == '-')
     {
	ch = copts[i][1];  /* Next flag character */
	if (Debug_Level <= 0)
	   /* No debugging */
	   record_option = (ch == 'g' || /* Debugging option */
			    ch == 'O');  /* Optimization level */
	else
	   /* Full debugging */
	   record_option = (ch == 'D' || /* Macro symbol definition */
			    ch == 'g' || /* Debugging option */
			    ch == 'I' || /* Search path for #include files */
			    ch == 'O' || /* Optimization level */
			    ch == 'U');  /* Macro symbol undefined */
	if (record_option)
	{
	   opt_size[num_opts] = strlen(copts[i]) + 1; /* Arg + space/null */
	   selected_opt[num_opts] = copts[i];
	   strlength += opt_size[num_opts];
	   num_opts += 1;
	}
     }
  }
  
  if (strlength == 0)
  {
#ifdef TARG_ST
     rtrn = (char *)xcalloc(1, 1); /* An empty string */
#else
     rtrn = (char *)calloc(1, 1); /* An empty string */
#endif
  }
  else
  {
#ifdef TARG_ST
     rtrn = (char *)xmalloc(strlength);
#else
     rtrn = (char *)malloc(strlength);
#endif
     cp = rtrn;

     /* Append the selected options to the string (rtrn) */
     for (i = 0; i < num_opts; i++)
	if (opt_size[i] > 0)
	{
	   cp = strcpy(cp, selected_opt[i]) + opt_size[i];
	   cp[-1] = ' '; /* Space character */
	}
     cp[-1] = '\0'; /* Terminating null character */
  }
  
  free(selected_opt);
  free(opt_size);
  return rtrn;
} /* DST_get_command_line_options */

static char *
Get_Name (gs_t node)
{
  static char buf[64];


  if (node == NULL) {
		buf[0] = 0;
                return buf;
  }
  char *name = buf;
  buf[0] = 0;

#define DANAME(d) ((gs_tree_code_class(d) == GS_TCC_DECLARATION)? \
((gs_decl_name(d))?gs_identifier_pointer(gs_decl_name(d)):"?"):\
 "?2")


  gs_tree_code_class_t tc_class = gs_tree_code_class(node);

  if (tc_class == GS_TCC_DECLARATION)
  {
      if (gs_decl_name (node)) {
        name = gs_identifier_pointer (gs_decl_name (node));
      }
  }
  else if (tc_class == GS_TCC_TYPE)
  {
      if (gs_type_name (node))
        {
          if (gs_tree_code (gs_type_name (node)) == GS_IDENTIFIER_NODE)
            name =  gs_identifier_pointer (gs_type_name (node));
#ifdef TARG_ST // [CL] Handle anonymous unions
          else if (gs_tree_code (gs_type_name (node)) == GS_TYPE_DECL
		   && ! gs_decl_ignored_p( gs_type_name(node))
                   && gs_decl_name (gs_type_name (node)))
            name = gs_identifier_pointer (gs_decl_name (gs_type_name (node)));
#else
          else if (gs_tree_code (gs_type_name (node)) == GS_TYPE_DECL
                   && gs_decl_name (gs_type_name (node)))
            name = gs_identifier_pointer (gs_decl_name (gs_type_name (node)));
#endif	  
        } 
      else if (gs_tree_code(node) == GS_INTEGER_TYPE) { // bug 11848
	if (gs_type_unsigned(node))
	  strcpy(name, "unsigned ");
	else strcpy(name, "signed ");
	if (strcmp(gs_type_mode(node), "QI") == 0)
	  strcat(name, "char");
	else if (strcmp(gs_type_mode(node), "HI") == 0)
	  strcat(name, "short");
	else if (strcmp(gs_type_mode(node), "SI") == 0)
	  strcat(name, "int");
	else if (strcmp(gs_type_mode(node), "DI") == 0)
	  strcat(name, "long long");
      }else if (gs_tree_code(node) == GS_COMPLEX_TYPE){
            if (strcmp(gs_type_mode(node), "SC") == 0)
               strcpy(name, "complex float");
            else if(strcmp(gs_type_mode(node), "DC") == 0)
               strcpy(name, "complex double");
            //bug 12960: XC(160bits) and TC(256bits) have the same name             
            else if(strcmp(gs_type_mode(node), "XC") == 0 ||
                    strcmp(gs_type_mode(node), "TC") == 0) 
               strcpy(name, "complex long double");            
      }else if(gs_tree_code(node) == GS_REAL_TYPE){
            if (strcmp(gs_type_mode(node), "SF") == 0)
               strcpy(name, "float");
            else if(strcmp(gs_type_mode(node), "DF") == 0)
               strcpy(name, "double");
            //bug 12960: XF(80bits) and TF(128bits) have the same name
            else if(strcmp(gs_type_mode(node), "XF") == 0 ||
                    strcmp(gs_type_mode(node), "TF") == 0 )
               strcpy(name, "long double");
     }
  } else {
  }
  return name;
}

// static data member of class
// Since there will be a global somewhere with this,
// we need to avoid creating ST information. 
// ST information
// the DECL dst record.
//
static void
DST_enter_static_data_mem(gs_t  parent_tree,
                DST_INFO_IDX parent_idx,
                TY_IDX parent_ty_idx,
                gs_t field)
{
	/* cannot be a bit field */
    int isbit = 0;
    DST_INFO_IDX field_idx = DST_INVALID_INIT;

    char * linkage_name = 
  		gs_identifier_pointer(gs_decl_assembler_name (field));
    char * mem_name = Get_Name(field);


    gs_t ftype = gs_tree_type(field);


    // We don't want ST entries created for this class decl.
    ST * st = 0; // Do not do Get here Get_ST(field);
    ST_IDX base =  ST_IDX_ZERO; //  Nothere ST_st_idx(st);


    DST_INFO_IDX fidx = Create_DST_type_For_Tree(ftype,base,parent_ty_idx);

    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef KEY
#ifdef TARG_ST
    // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(field);
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
#else
    USRCPOS_clear(src);
#endif // KEY

#ifdef TARG_ST
    DST_accessibility access = get_dwarf_access(field);
#endif

    field_idx = DST_mk_variable(
        src,         // srcpos
        mem_name,  // user typed name, not mangled
        fidx,        // user typed type name here (typedef type perhaps).
        0,           // offset (fortran uses non zero )
        (void*) base, // underlying type here, not typedef.
        DST_INVALID_IDX,  // abstract origin
        TRUE,          // is_declaration=  decl only
        FALSE,         // is_automatic ?
#ifdef TARG_ST
	// [CL]
	gs_tree_public(field) ? TRUE : FALSE, // is_external ?
        gs_decl_artificial(field),      // is_artificial ?
	access);
#else
        FALSE,         // is_external ?
        FALSE  );      // is_artificial ?
#endif

    DECL_DST_FIELD_IDX(field) = field_idx;
#ifdef KEY
    // Bug 4829 - do not enter variables without a name.
    if (mem_name != NULL && *mem_name != '\0')
#endif
    DST_append_child(parent_idx,field_idx);

    DST_INFO_IDX varidx = DECL_DST_IDX(field);
    DECL_DST_SPECIFICATION_IDX(field) = field_idx;

#ifdef KEY
#ifdef TARG_ST
// [CL] dont output invisible names
    if(mem_name && linkage_name && strcmp(mem_name, linkage_name) &&
       gs_tree_public(field) && !gs_decl_abstract(field) ) {
#else
    if(mem_name && linkage_name && strcmp(mem_name, linkage_name)) {
#endif
       DST_add_linkage_name_to_variable(field_idx, linkage_name);
    }
#else
    // FIXME: need a data version of this.
    //if(mem_name && linkage_name && strcmp(mem_name, linkage_name)) {
    //   DST_add_linkage_name_to_subprogram(field_idx, linkage_name);
    //}
#endif
    DECL_DST_SPECIFICATION_IDX(field) = field_idx;

    return ;
}

// Called for member functions, whether static member funcs
// or non-static member funcs.
// Here we add the member func to the class decl.
// If the function has arguments and the first argument's name
// is "this" it is a non-static member function. Otherwise it
// is a static member function.
// We don't want types added here to be remembered:
// not DST and not ST information. As this is not a definition point.
// (for some functions it can be, but we don't yet handle that)
// 
#ifndef KEY
static void
#else
void
#endif
DST_enter_member_function( gs_t parent_tree,
		DST_INFO_IDX parent_idx,
                TY_IDX parent_ty_idx,
		gs_t fndecl)
{
    USRCPOS src;
#ifdef TARG_ST // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(fndecl);
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
    DST_INFO_IDX dst = DST_INVALID_INIT;
    DST_INFO_IDX ret_dst = DST_INVALID_IDX;
                                     
    DST_INFO_IDX current_scope_idx = parent_idx;


#ifdef TARG_ST
    // [CL] if DST for this function has already been generated, exit now
    if (DECL_DST_SPECIFICATION_IDX(fndecl) != DST_INVALID_IDX) {
      return;
    }

    // [CL] if this is an instance whose abstract_origin has already
    // been emitted, don't emit it again. If no instance has already
    // been emitted, do it now, even though the current one has an
    // abstract_origin (this is because in some case, the
    // abstract_origin itself would not be emitted.
    if (gs_decl_abstract_origin(fndecl)
	&& (DECL_DST_SPECIFICATION_IDX(gs_decl_abstract_origin(fndecl))
	    != DST_INVALID_IDX)
	) {
      return;
    }
#endif
    
#ifndef KEY
    gs_t resdecl = gs_decl_result(fndecl);
    gs_t restype = 0;
    if( resdecl) {
	   restype = gs_tree_type(resdecl);
    }
#else
    // Another case of Bug 1510 - get the result type from the function type 
    // declaration rather than the result declaration type.
    gs_t restype = 0;
    if (gs_tree_type(fndecl))
      restype = gs_tree_type(gs_tree_type(fndecl));    
#endif
    if(restype) {
	 TY_IDX itx = Get_TY(restype);
	 ret_dst = TYPE_DST_IDX(restype);
#ifdef TARG_ST
	 if (DST_IS_NULL(ret_dst)) {
	   ret_dst = Create_DST_type_For_Tree(restype, itx, 0);
	 }
#endif
    }

    BOOL is_prototyped = TRUE;

#ifdef TARG_ST
    // [CL] Use decl_printable_name to get the right name, eg with
    // destructors (otherwise we wouldn't get the '~')
    char *basename = NULL;
    if (gs_operand (fndecl, GS_DECL_PRINTABLE_NAME)) {
      basename = gs_decl_printable_name(fndecl);
    }
#else
#ifdef KEY
    // bug 1736.  Why is the name of the operator omitted?  Let's not do that
    char *basename = gs_identifier_pointer (gs_decl_name (fndecl));
#else
    char * basename = 
	gs_identifier_opname_p(gs_decl_name(fndecl))? 0 :
	gs_identifier_pointer (gs_decl_name (fndecl));
#endif
#endif
    char * linkage_name = 
	gs_identifier_pointer(gs_decl_assembler_name (fndecl));
#ifndef TARG_ST
#ifdef KEY
    // Bug 3846
    if (gs_identifier_opname_p (gs_decl_name(fndecl)) && 
	gs_identifier_typename_p (gs_decl_name(fndecl))) {
      basename = cplus_demangle(linkage_name, DMGL_PARAMS | DMGL_ANSI | 
                                              DMGL_TYPES);
      if (basename) {
	basename = strstr(basename, "operator ");
	FmtAssert(basename, ("NYI"));
      } else {
	// Bug 4788 has a weird mangled name which cannot be demangled using
	// c++filt; g++ also generates the same linkage name (mangled name) for
	// that symbol. However, g++ is able to get back the demangled name 
	// somehow. For now, leave this operator name as such.
	// c++filt version 3.3 has this problem but version 3.4 seems to have
	// fixed this. Since, there are lot of changes to 
	// kgnu_common/libiberty/cp-demangle.c, we will wait for the front-end
	// upgrade to 3.4 which will fix this automatically.
	DevWarn(
	 "encountered a mangled name that can not be demangled using c++filt");
	basename = gs_identifier_pointer (gs_decl_name (fndecl));
      }
    }
#endif
#endif /* TARG_ST */

    gs_t ftype = gs_tree_type(fndecl);

    int is_abstract_root = 0; // Simply a decl here, not abs. root.

    // is_declaration TRUE  as this is function declared in class,
    // not a definition or abstract root.
    TY_IDX base =  Get_TY(ftype);


    BOOL is_external = TRUE;
    DST_virtuality  virtuality = 
		 gs_decl_pure_virtual_p(fndecl)?
			 DW_VIRTUALITY_pure_virtual
		 : gs_decl_virtual_p(fndecl)?
			DW_VIRTUALITY_virtual : DW_VIRTUALITY_none;
    // FIX virtuality.

    DST_inline inlin = 0; 
		//DECL_PENDING_INLINE_P(fndecl)?
		//	DW_INL_inlined: DW_INL_not_inlined;

    
#ifdef TARG_ST
    DST_vtable_elem_location vtable_elem_location;
    /* (cbr) elem loc must be an integer */
    if (gs_decl_vindex(fndecl) && gs_tree_code (gs_decl_vindex(fndecl)) == GS_INTEGER_CST) {
      vtable_elem_location = gs_get_integer_value(gs_decl_vindex(fndecl));
    } else {
      vtable_elem_location = 0;
    }
#else
    //FIX vtable elem loc
    DST_vtable_elem_location vtable_elem_location =  0;
#endif

    dst = DST_mk_subprogram(
        src,			// srcpos
        basename,
        ret_dst,        	// return type
        DST_INVALID_IDX,        // Index to alias for weak is set later
        0,              // index to fe routine for st_idx
        inlin,                  // dwarf inline code.
        virtuality,     	// applies to C++, dwarf virt code
        vtable_elem_location,   // vtable_elem_location (vtable slot #
				// as emitted in MIPS, something else
				// by gcc for ia64 )
        TRUE,         // is_declaration
        is_prototyped,           // always true for C++
#ifdef TARG_ST // [CL] we added the same field at the last position...
        is_external,  // is_external? (has external linkage)
        gs_decl_artificial (fndecl) );
#else
#ifdef KEY
        gs_decl_artificial (fndecl),
#endif
        is_external );  // is_external? (has external linkage)
#endif

    // producer routines thinks we will set pc to fe ptr initially
    DST_RESET_assoc_fe (DST_INFO_flag(DST_INFO_IDX_TO_PTR(dst)));

    DST_append_child (current_scope_idx, dst);



    DECL_DST_FIELD_IDX(fndecl) = dst;

    // Now we create the argument info itself, relying
    // on the is_prototyped flag above to let us know if
    // we really should do this.
    if(is_prototyped) {
       gs_t parms = gs_decl_arguments(fndecl);
       if(!parms) {
          // no arguments: int y(); for example in C++.
       } else {
	   // This kills be with mem func. FIX
	   DST_enter_param_vars(fndecl, 
		dst,
		parms,
		is_abstract_root,
		/* is_declaration_only */ 1);
       }

    }
#ifdef TARG_ST
// [CL] dont output invisible names
    if(basename && linkage_name && strcmp(basename, linkage_name) &&
       gs_tree_public(fndecl) && !gs_decl_abstract(fndecl) ) {
#else
    if(basename && linkage_name && strcmp(basename, linkage_name)) {
#endif
       DST_add_linkage_name_to_subprogram(dst, linkage_name);
    }
    DECL_DST_SPECIFICATION_IDX(fndecl) = dst;
}

        
#ifdef TARG_ST
/* Given a value, round it up to the lowest multiple of `boundary'
   which is not less than the value itself.  */

static inline INT
ceiling (INT value, unsigned int boundary)
{
  return (((value + boundary - 1) / boundary) * boundary);
}
#endif

static void
DST_enter_normal_field(gs_t  parent_tree,
		DST_INFO_IDX parent_idx,
		TY_IDX parent_ty_idx,
		gs_t field)
{
    char isbit = 0; 
#ifdef TARG_ST
    // [CL] Ignore the nameless fields that are used to skip bits but
    // handle C++ anonymous unions.
    if (gs_decl_name(field) == NULL &&
	gs_tree_code(gs_tree_type(field)) != GS_UNION_TYPE) {
      return;
    }
#endif
#ifdef TARG_ST
    isbit = (gs_decl_bit_field_type(field) != NULL);
#else
    if ( ! gs_decl_bit_field(field)
#ifdef KEY // bug 10478, 11590: from kgccfe
           && gs_decl_size(field)
#endif
           && gs_get_integer_value(gs_decl_size(field)) > 0
           && gs_get_integer_value(gs_decl_size(field))
           != (TY_size(Get_TY(gs_tree_type(field)))
                                        * BITSPERBYTE) )
    {
           // for some reason gnu doesn't set bit field
           // when have bit-field of standard size
           // (e.g. int f: 16;).  But we need it set
           // so we know how to pack it, because
           // otherwise the field type is wrong.
	   // already warned
           //DevWarn(field size %d does not match type size %d,
           //                            gs_decl_size(field),
           ////                           TY_size(Get_TY(gs_tree_type(field)))
           //                                   * BITSPERBYTE );

	   isbit = 1;
    }
    if (gs_decl_bit_field(field)) {
	   isbit = 1;
    }
#endif /* TARG_ST */
    DST_INFO_IDX field_idx = DST_INVALID_INIT;
    char *field_name = Get_Name((field));

#ifdef KEY
    // bug 1718
    if ((field_name == NULL || field_name[0] == '\0' ) &&
	/* bug 3847 - fields that are anonymous unions should be emitted */ 
	gs_tree_code(gs_tree_type(field)) != GS_UNION_TYPE) {
        return ;
    }
#endif

#ifdef TARG_ST
    // [SC] Mimic member_declared_type in gcc/dwarf2out.c.
    gs_t ftype = (gs_decl_bit_field_type(field)
		  ? gs_decl_bit_field_type(field)
		  : gs_tree_type(field));
#else
    gs_t ftype = gs_tree_type(field);
#endif


    TY_IDX base = Get_TY(ftype);

    DST_INFO_IDX fidx = Create_DST_type_For_Tree(ftype,base,parent_ty_idx);

    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef TARG_ST  // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(field);
#else
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
    USRCPOS_clear(src);
#endif // KEY
#endif // TARG_ST

#ifdef TARG_ST
	if (gs_tree_this_volatile(field)) {
	  fidx = DST_mk_volatile_type(fidx);
	  DST_append_child(comp_unit_idx,fidx);
	}
	if (gs_tree_readonly(field)) {
	  fidx = DST_mk_const_type(fidx);
	  DST_append_child(comp_unit_idx,fidx);
	}

	/* The following code is inspired from dwarf2out.c:
	   field_byte_offset, add_bit_offset_attribute */
	const unsigned int BITS_PER_UNIT = 8;
	const unsigned int BITS_PER_WORD = TARGET_64BIT ? 64 : 32;
	INT bitoff = (decl_field_bit_offset(field)
		      + (BITS_PER_UNIT
			 * decl_field_offset(field)));

	unsigned int type_align_in_bytes;
	unsigned int type_align_in_bits;
	UINT type_size_in_bits;

 	gs_t type_size = gs_type_size(ftype);

	if (type_size) {
	  if (gs_tree_code(type_size) == GS_INTEGER_CST) {
	    type_size_in_bits = gs_get_integer_value(type_size);
	  } else {
	    type_size_in_bits = gs_type_align(ftype);
	  }
	} else {
	  type_size_in_bits = 0;
	}
	type_align_in_bits = gs_type_align(ftype);
	UINT align = gs_type_align(ftype)/BITSPERBYTE;
	type_align_in_bytes = type_align_in_bits / BITS_PER_UNIT;

	INT object_offset_in_align_units;
	INT object_offset_in_bits;
	INT object_offset_in_bytes;
	INT deepest_bitpos;
	UINT field_size_in_bits;

        /* (cbr) */
        if (!gs_decl_size(field))
          field_size_in_bits = type_size_in_bits;
        else if (gs_tree_code(gs_decl_size(field)) == GS_INTEGER_CST)
          field_size_in_bits = gs_get_integer_value(gs_decl_size(field));
	else
	  field_size_in_bits = BITS_PER_WORD;
	
	/* Figure out the bit-distance from the start of the structure to the
	   "deepest" bit of the bit-field.  */
	deepest_bitpos = bitoff + field_size_in_bits;

	/* This is the tricky part.  Use some fancy footwork to deduce where the
	   lowest addressed bit of the containing object must be.  */
	object_offset_in_bits
	  = ceiling (deepest_bitpos, type_align_in_bits) - type_size_in_bits;

	/* Compute the offset of the containing object in "alignment units".  */
	object_offset_in_align_units = object_offset_in_bits / type_align_in_bits;

	/* Compute the offset of the containing object in bytes.  */
	object_offset_in_bytes = object_offset_in_align_units * type_align_in_bytes;

	INT fld_offset_bytes = object_offset_in_bytes;
#else
    INT bitoff = gs_get_integer_value(gs_decl_field_bit_offset(field));
#ifndef KEY
INT fld_offset_bytes = bitoff / BITSPERBYTE;
#else
// Bug 1271 
INT fld_offset_bytes = gs_get_integer_value(gs_decl_field_offset(field)) + 
  bitoff / BITSPERBYTE; 
#endif 
 gs_t type_size = gs_type_size(ftype);
UINT align = gs_type_align(ftype)/BITSPERBYTE;
#endif
    INT tsize;
    if (type_size == NULL) {
#ifndef KEY //bug 10478, 11590: from kgccfe
          // incomplete structs have 0 size
          Fail_FmtAssertion("DST_enter_normal_field: type_size NULL ");
#endif
          tsize = 0;
    }
    else {
          if (gs_tree_code(type_size) != GS_INTEGER_CST) {
#ifdef TARG_ST
	    /* Copy gcc and represent the size of a variable-sized type as -1. */
	    tsize = -1;
#else
            if (gs_tree_code(type_size) == GS_ARRAY_TYPE)
              Fail_FmtAssertion ("Encountered VLA at line %d", lineno);
            else
              Fail_FmtAssertion ("VLA at line %d not currently implemented", 
		lineno);
            tsize = 0;
#endif
          }
          else
            tsize = gs_get_integer_value(type_size) / BITSPERBYTE;
    }

    if(isbit == 0) {
        //  currentcontainer = -1 ;                    // invalidate bitfield calculation
#ifndef KEY
	  field_idx = DST_mk_member(
		src,
		field_name,
		fidx, // field type	
		fld_offset_bytes, // field offset in bytes
			0, // no container (size zero)
			0, // bit_offset= no offset into container
		        0, // bit_size= no bitfield size
	       FALSE, // is_bitfield= not a bitfield
	       FALSE, // is_static= not a static member
	       FALSE, // is_declaration= 
	       FALSE); // is_artificial = no
#else
	  int accessibility = get_dwarf_access(field);
	  field_idx = DST_mk_member(
		src,
		field_name,
		fidx, // field type	
		fld_offset_bytes, // field offset in bytes
			0, // no container (size zero)
			0, // bit_offset= no offset into container
		        0, // bit_size= no bitfield size
	       FALSE, // is_bitfield= not a bitfield
	       FALSE, // is_static= not a static member
	       FALSE, // is_declaration= 
#ifdef TARG_ST
  // [CL]
	       gs_decl_artificial(field), // is_artificial = no
#else
	       FALSE, // is_artificial = no
#endif
	       accessibility); // accessibility = public/private/protected
#endif
			
    } else {
	  if(tsize == 0) {
	   Fail_FmtAssertion("bit field type size 0!");
	   return;
	  }
	  UINT container_off = fld_offset_bytes - (fld_offset_bytes%align);

//***********************************************************
//Bug 8890 : modify the calculation for DW_AT_bit_offset
//        (1) claculate offset into the container
//        (2) adjust for little endian,
//***********************************************************
#ifndef KEY
          UINT into_cont_off = bitoff - (container_off*BITSPERBYTE);
#else
          UINT into_cont_off = bitoff - ((container_off%16)*BITSPERBYTE);
          // (1) yes, we mod 16 here because bitoff will wrap around at 16 bytes
          //     this is essential set the new base for bitoff

          // (2) adjust for little endian
          if (Target_Byte_Sex != BIG_ENDIAN) {
             into_cont_off =  tsize*BITSPERBYTE - into_cont_off;   // reset current offset
             into_cont_off -= gs_get_integer_value (gs_decl_size(field)); // start at MSB
            }
#endif

#ifndef KEY
	  field_idx = DST_mk_member(
			src,
			field_name,
			fidx	,      // field type	
                        fld_offset_bytes,    // container offset in bytes
			tsize,         // container size, bytes
                        into_cont_off, // offset into 
					// container, bits

                        gs_get_integer_value(gs_decl_size(field)), // bitfield size
                        TRUE, // a bitfield
                        FALSE, // not a static member
                        FALSE, // Only TRUE for C++?
                        FALSE); // artificial (no)
#else
	  int accessibility = get_dwarf_access(field);
	  field_idx = DST_mk_member(
			src,
			field_name,
			fidx	,      // field type	
                        container_off,    // container offset in bytes
#ifdef TARG_ST
			(tsize == -1) ? 0xffffffff : tsize,
#else
			tsize,         // container size, bytes
#endif
                        into_cont_off, // offset into 
					// container, bits

                        gs_get_integer_value(gs_decl_size(field)), // bitfield size
                        TRUE, // a bitfield
                        FALSE, // not a static member
                        FALSE, // Only TRUE for C++?
#ifdef TARG_ST
  // [CL]
			gs_decl_artificial(field), // is_artificial = no
#else
                        FALSE, // artificial (no)
#endif
			accessibility); // accessibility = public/private/protected
#endif
    }
    DST_append_child(parent_idx,field_idx);


	
    return ;
}
static void
DST_enter_struct_union_members(gs_t parent_tree, 
	DST_INFO_IDX parent_idx  )
{ 
    DST_INFO_IDX dst_idx = DST_INVALID_INIT;

    TY_IDX parent_ty_idx = Get_TY(parent_tree);
    
    //if(TREE_CODE_CLASS(TREE_CODE(parent_tree)) != 'd') {
     //   DevWarn("DST_enter_struct_union_members input not 't' but %c",
      //          TREE_CODE_CLASS(TREE_CODE(parent_tree)));
   // }

    gs_t field = gs_type_fields(parent_tree);

//    int currentoffset = 0 ;
//    int currentcontainer = -1 ;
                                                                                                                      
    for( ; field ; field = gs_tree_chain(field) )
    { 
	if(gs_tree_code(field) == GS_FIELD_DECL) {
	   DST_enter_normal_field( parent_tree,parent_idx,
		parent_ty_idx,field);
	} else if(gs_tree_code(field) == GS_VAR_DECL) {
		// Here create static class data mem decls
		// These cannot be definitions.
	   DST_enter_static_data_mem( parent_tree,parent_idx,
		parent_ty_idx,field);
        } else if (gs_tree_code(field) == GS_FUNCTION_DECL) {
		// Cannot happen.
		DevWarn("Impossible FUNCTION DECL member of class!\n");
	}  else {
	  // RECORD_TYPE is class itself, apparently,
	  // and can appear here.
        }
    }

#ifndef TARG_ST
    // [CL] I don't understand what this patch is supposed to do.
    // Removing it does improve our score on the GDB 6.8 testsuite.
#ifdef KEY
    // Bug 3533 - Expand all member functions of classes inside ::std namespace
    // here (I don't know how to get the member functions of the classes
    // contained in ::std namespace from gxx_emitted_decl in wfe_decl.cxx).
    // Bug 10483 and 13050: Check for NULL context.
    // Bug 3533: Use type context, not decl context.
    gs_t context = gs_type_context(parent_tree);
    if (context == NULL ||
	gs_tree_code(context) != GS_TYPE_DECL ||
	!gs_decl_context(context) ||
	gs_tree_code(gs_decl_context(context)) != GS_NAMESPACE_DECL ||
	!gs_decl_name(gs_decl_context(context)))
      return;
#endif
#endif
    // member functions
    gs_t methods = gs_type_methods(parent_tree);
    for  ( ; methods != NULL; methods = gs_tree_chain(methods)) {
      	if(gs_tree_code(methods) == GS_FUNCTION_DECL ) {
        // g++ seems to put the artificial ones in the output too for some reason
        // in particular, operator= is there.  We do want to omit the __base_ctor stuff
        // though

#ifdef TARG_ST
	  // [CL] this filter is different from GCC's because we
	  // handle trees with more/different attributes set. We have
	  // tagged trees in GCC's dwarf2out.c
	  if (!gs_decl_dwarf_info_needed(methods)) {
	    continue;
	  }
#endif
#ifndef TARG_ST
// [CL] although GCC4 uses decl_abstract_origin, the above test
// actually gives better results in the GDB 6.8 testsuite.
#ifndef KEY
	   if ( gs_decl_artificial(methods)) {
#else
           if (gs_identifier_ctor_or_dtor_p(gs_decl_name(methods))) {
#endif
	     // compiler generated methods are not interesting.
	     // We want only ones user coded.
	     continue;	   
	   } else {
#endif
	     DST_enter_member_function( parent_tree,parent_idx,
		parent_ty_idx,methods);
#ifndef TARG_ST
	   }
#endif
	   }
    }

    return;
}


// We have a struct/union. Create a DST record
// and enter it.
static DST_INFO_IDX
DST_enter_struct_union(gs_t type_tree, TY_IDX ttidx  , TY_IDX idx, 
#ifdef TARG_ST
		UINT64 tsize)
#else
		INT tsize)
#endif
{ 
#ifdef KEY
    // Get the unqualified gcc type.  Bug 3969.
    if (lang_cplus) // bug 10483
      type_tree = gs_type_main_variant(type_tree);
#endif

    DST_INFO_IDX dst_idx  = TYPE_DST_IDX(type_tree);

    DST_INFO_IDX current_scope_idx;

#ifdef KEY
    // Want the immediately enclosing scope.  Bug 4168.  (Do this for other
    // types besides records and unions too?  For now, limit to nested
    // records/unions.)
    if (gs_type_context(type_tree) &&
	(gs_tree_code(type_tree) == GS_RECORD_TYPE ||
	 gs_tree_code(type_tree) == GS_UNION_TYPE) &&
	(gs_tree_code(gs_type_context(type_tree)) == GS_RECORD_TYPE ||
	 gs_tree_code(gs_type_context(type_tree)) == GS_UNION_TYPE)) {
      gs_t type_context = gs_type_context(type_tree);
      current_scope_idx = TYPE_DST_IDX(type_context);
      if (DST_IS_NULL(current_scope_idx)) {
	// TODO: Using TY_IDX_ZERO as the 3rd arg is valid only if type is not
	// being forward declared.  Verify if that's correct.
	Create_DST_type_For_Tree(type_context, TYPE_TY_IDX(type_context),
				 TY_IDX_ZERO);
	current_scope_idx = TYPE_DST_IDX(type_context);
      }
      Is_True(!DST_IS_NULL(current_scope_idx),
	      ("DST_enter_struct_union: invalid current scope index\n"));
    } else
#endif
    current_scope_idx =
         DST_get_context(gs_type_context(type_tree));

#ifdef TARG_ST
    // [CL] in case of forward declaration, we may already have built
    // DST for the incomplete type. Check if the type definition has
    // now been completed, and if so, complete DST too.

    // In all cases, return early to avoid infinite recursion in case
    // of self-referencing struct/union.

    if (!DST_IS_NULL(dst_idx) && (idx != 0)) {
      FLD_HANDLE elt_fld = TY_fld(idx);
      if (elt_fld.Is_Null())  {
	// No field.
	return dst_idx;
      } else {

	// Look for DST info for members
	DST_INFO *parent_info = DST_INFO_IDX_TO_PTR(dst_idx);
	DST_INFO_IDX *last_child_field = DST_get_ptr_to_lastChildField(parent_info);
	if (DST_IS_NULL(*last_child_field)) {

	  // Now we have new fields, but no DST. Create it now.
	  if(gs_tree_code(type_tree) == GS_RECORD_TYPE) {
	    if (!DST_is_structure_being_built(dst_idx)) {
	      DST_set_structure_being_built(dst_idx);
	      DST_enter_struct_union_members(type_tree,dst_idx);
	      DST_clear_structure_being_built(dst_idx);
	      DST_clear_structure_declaration(dst_idx);
	    }
	  } else if (gs_tree_code(type_tree) == GS_UNION_TYPE) {
	    if (!DST_is_union_being_built(dst_idx)) {
	      DST_set_union_being_built(dst_idx);
	      DST_enter_struct_union_members(type_tree,dst_idx);
	      DST_clear_union_being_built(dst_idx);
	      DST_clear_union_declaration(dst_idx);
	    }
	  }
	  return dst_idx;

	} else {
	  // Fields are already in DST.
	  return dst_idx;
	}
      }
    }
#endif

    if(DST_IS_NULL(dst_idx)) {

	// not yet created, so create it


	// Deal with scope here (FIX)
        // in case scope of decl is different from scope of ref
	//
        USRCPOS src;
        // For now, the source location appears bogus
        // (or at least odd) for files other than the base
        // file, so lets leave it out. Temporarily.
        //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef KEY
#ifdef TARG_ST
        USRCPOS_clear(src);
	if (gs_type_stub_decl(type_tree)) {
	  USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(gs_type_stub_decl(type_tree));
	}
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
#else
        USRCPOS_clear(src);
#endif // KEY

	char *name = Get_Name(type_tree);

#ifndef TARG_ST
	// [CL] we should emit struct/union type definitions, even if
	// they have no name
#ifdef KEY
        // bug 1718
        if (name == NULL || name[0] == '\0') {
            return dst_idx ;
        }
#endif
#endif

#ifdef TARG_ST
	// [CL] don't compute completeness as in GCC:
	// complete = (TYPE_SIZE (type)
	//             && (! TYPE_STUB_DECL (type)
	//		      || ! TYPE_DECL_SUPPRESS_DEBUG (TYPE_STUB_DECL (type))));
	// See GDB's gdb.base/opaque.exp (6.8).
	// Removing the tests on type_stub_decl and
	// type_decl_suppress_debug enables passing GDB's
	// gdb.cp/bs15503 test.
	int complete = gs_type_size (type_tree) != NULL;
#endif
	if(gs_tree_code(type_tree) == GS_RECORD_TYPE) {
	   dst_idx = DST_mk_structure_type(src,
		  name , // struct tag name
		  tsize,
		  DST_INVALID_IDX, // not inlined
#ifdef TARG_ST // [CL]
		   !complete   // 1 if incomplete
#else
		   gs_type_fields(type_tree)== 0   // 1 if incomplete
#endif
		   );
#ifdef TARG_ST
	   DST_set_structure_being_built(dst_idx);
#endif
	} else if (gs_tree_code(type_tree) == GS_UNION_TYPE) {
	   dst_idx = DST_mk_union_type(src,
		  name  , // union tag name
		  tsize,
		  DST_INVALID_IDX, // not inlined
#ifdef TARG_ST // [CL]
		   !complete   // 1 if incomplete
#else
		   gs_type_fields(type_tree)== 0   // arg 1 if incomplete
#endif
		   );
#ifdef TARG_ST
	   DST_set_union_being_built(dst_idx);
#endif
	} else {
	  // no DST_enter_struct_union_members(type_tree,dst_idx);
          // leave as DST_IS_NULL
          return dst_idx;
	}
#ifdef TARG_ST
	// [CL] handle const and volatile qualifiers now, so that
	// possible self references point to the right type

	// We need an intermediate variable because we can only add
	// members to struct/union, not to const/volatile type.

	DST_INFO_IDX struct_union_dst_idx = dst_idx;

	if (gs_type_volatile(type_tree)) {
	  DST_append_child(current_scope_idx,dst_idx);
	  dst_idx = DST_mk_volatile_type(dst_idx);
	}
	if (gs_type_readonly(type_tree)) {
	  DST_append_child(current_scope_idx,dst_idx);
	  dst_idx = DST_mk_const_type(dst_idx);
	}
#endif

	DST_append_child(current_scope_idx,dst_idx);

	// set this now so we will not infinite loop
	// if this has ptr to itself inside.
        TYPE_DST_IDX(type_tree) = dst_idx;


#ifdef TARG_ST
	// [CL] record what type our vtable lives in
	DST_INFO_IDX vtidx = DST_INVALID_IDX;

	if (gs_type_vfield (type_tree)) {
	  gs_t vtype = gs_decl_fcontext (gs_type_vfield (type_tree));

	  if (vtype != type_tree) {
	    // Generate type if it is not the one we are currently
	    // generating
	    TY_IDX itx =  TYPE_TY_IDX(vtype);
	    vtidx = Create_DST_type_For_Tree (vtype,itx, idx);
	  } else {
	    vtidx = struct_union_dst_idx;
	  }
	}
	if(gs_tree_code(type_tree) == GS_RECORD_TYPE) {
	  DST_add_structure_containing_type(struct_union_dst_idx, vtidx);
	} else if (gs_tree_code(type_tree) == GS_UNION_TYPE) {
	  DST_add_union_containing_type(struct_union_dst_idx, vtidx);
	}
#endif

  	// Do the base classes
        INT32 offset = 0;
        INT32 anonymous_fields = 0;
	gs_t type_binfo, basetypes;
        if ((type_binfo = gs_type_binfo(type_tree)) != NULL &&
	     (basetypes = gs_binfo_base_binfos(type_binfo)) != NULL) {
	  gs_t list;
	  for (list = basetypes; gs_code(list) != EMPTY; 
	       list = gs_operand(list, 1)) {
#ifdef TARG_ST // [CL] update for 3.x frontend version
    	            INT32 virtual_offset;
#endif
                    gs_t binfo = gs_operand(list, 0);

                    gs_t basetype = gs_binfo_type(binfo);

		    int virtuality = DW_VIRTUALITY_none;
		    if(gs_binfo_virtual_p(binfo)) {
			// Don't know how to tell if
			// it is pure_virtual.  FIX
			virtuality = DW_VIRTUALITY_virtual;
#ifdef TARG_ST // [CL]
			virtual_offset = -gs_get_integer_value(gs_binfo_vptr_field(binfo));
#endif
		    }
#ifdef TARG_ST // [CL]
		    else {
		      virtual_offset = 0;
		    }
#endif
#ifdef KEY
#ifdef TARG_ST
		    // For inheritance, accessibility is private unless
		    // otherwise mentioned - bug 3041.
		    int accessibility = get_dwarf_access(binfo, DW_ACCESS_private);
#else
		    // For inheritance, accessibility is private unless
		    // otherwise mentioned - bug 3041.
		    int accessibility = get_dwarf_access(binfo);
		    if (accessibility != DW_ACCESS_public &&
			accessibility != DW_ACCESS_protected) {
		      accessibility = DW_ACCESS_private;
		    }
#endif
#endif
#ifdef TARG_ST // [CL]
		    offset = gs_get_integer_value(gs_binfo_offset(binfo));
#else
                    offset = Roundup (offset,
                                    gs_type_align(basetype) / BITSPERBYTE);
#endif

		    TY_IDX itx =  TYPE_TY_IDX(basetype);
		    DST_INFO_IDX bcidx =
                       Create_DST_type_For_Tree (basetype,itx, idx);

#ifndef KEY
		    // There is no way to pass in DW_ACCESS_* here.
		    // That is am omission.  FIX
		    DST_INFO_IDX inhx = 
		      DST_mk_inheritance(src,
				bcidx,
			        virtuality,
				offset);
#else
#ifdef TARG_ST
		    DST_INFO_IDX inhx = 
		      DST_mk_inheritance(src,
					 bcidx,
					 virtuality,
					 offset,
					 accessibility,
					 virtual_offset);

#else
	 	    DST_INFO_IDX inhx = 
		      DST_mk_inheritance(src,
				bcidx,
			        virtuality,
				// Bug 1737 - handle virtual base classes
				virtuality ?  //bug 11555: gs_tree_int_cst_low -> gs_get_integer_value
				    -((int)gs_get_integer_value(gs_binfo_vptr_field(binfo))):
				    offset, 
				accessibility);
#endif
#endif

		    DST_append_child(dst_idx,inhx);
//---------------------------------------------------------------
//bug 12948: advance "offset" only when non-empty and non-virtual
//---------------------------------------------------------------
#ifdef KEY
#ifdef TARG_ST
		    if (! gs_is_empty_class(basetype) &&
#else
                    if (!is_empty_base_class(basetype) &&
#endif
#else
                    if (!is_empty_base_class(basetype) ||
#endif
                        !gs_binfo_virtual_p(binfo)) {

                      //FLD_Init (fld, Save_Str(Get_Name(0)),
                       //         Get_TY(basetype) , offset);
                      offset += Type_Size_Without_Vbases (basetype);
                    }

          }
        }


	// now can do the members of our type.
#ifdef TARG_ST
	DST_enter_struct_union_members(type_tree,struct_union_dst_idx);

	if(gs_tree_code(type_tree) == GS_RECORD_TYPE) {
	  DST_clear_structure_being_built(struct_union_dst_idx);
	   // [CL] if it has fields, then it's not a declaration (at
	   // least it seems it's what GDB expects)
	  if (complete){
	     DST_clear_structure_declaration(struct_union_dst_idx);
	  }
	} else if (gs_tree_code(type_tree) == GS_UNION_TYPE) {
	  DST_clear_union_being_built(struct_union_dst_idx);
	  if (complete){
	     DST_clear_union_declaration(struct_union_dst_idx);
	   }
	}
#else
	DST_enter_struct_union_members(type_tree,dst_idx);
#endif

    }

    return  dst_idx;
}


// We have a enum. 
// and enter it.
static DST_INFO_IDX
DST_enter_enum(gs_t type_tree, TY_IDX ttidx  , TY_IDX idx, 
		INT tsize)
{ 
   DST_INFO_IDX dst_idx = 
       TYPE_DST_IDX(type_tree);

   if(gs_tree_code_class(type_tree) != GS_TCC_TYPE) {
        DevWarn("DST_enter_enum input not TCC_TYPE but %c",
                gs_tree_code_class(type_tree));
   }
   DST_INFO_IDX current_scope_idx =
        DST_get_context(gs_type_context(type_tree));


   if(DST_IS_NULL(dst_idx)) {
      DST_INFO_IDX t_dst_idx = DST_INVALID_INIT;
      USRCPOS src;
      // For now, the source location appears bogus
      // (or at least odd) for files other than the base
      // file, so lets leave it out. Temporarily.
      //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef KEY
#ifdef TARG_ST // [CL] get source location from GCC
      USRCPOS_clear(src);
      if (gs_type_stub_decl(type_tree)) {
	USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(gs_type_stub_decl(type_tree));
      }
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
#else
      USRCPOS_clear(src);
#endif // KEY
      char *name1 = Get_Name(type_tree);
      gs_t enum_entry = gs_type_values(type_tree);
      DST_size_t e_tsize =  tsize;
      t_dst_idx = DST_mk_enumeration_type( src,
                           name1,
                           e_tsize, // Type size.
                           DST_INVALID_IDX, // Not inlined.
                           (enum_entry==NULL)); // Pass non-zero 
						// if incomplete.
      DST_append_child(current_scope_idx,t_dst_idx);

      TYPE_DST_IDX(type_tree) = t_dst_idx;

#ifdef TARG_ST
      // [CL] do not forget to update the return value
      dst_idx = t_dst_idx;
#endif

      DST_CONST_VALUE enumerator;
      if(tsize == 8) {
	   DST_CONST_VALUE_form(enumerator) =  DST_FORM_DATA8;
      } else if (tsize == 4) {
	   DST_CONST_VALUE_form(enumerator) =  DST_FORM_DATA4;
#ifdef TARG_ST
      } else if (tsize == 2) {
	   DST_CONST_VALUE_form(enumerator) =  DST_FORM_DATA2;
      } else if (tsize == 1) {
	   DST_CONST_VALUE_form(enumerator) =  DST_FORM_DATA1;
#endif
      } else {
	   // ???
	   DST_CONST_VALUE_form(enumerator) =  DST_FORM_DATA4;
	   DevWarn("Unexpected type size  %d in enumerator",
		(int)tsize);
      }

      for( ; enum_entry; enum_entry = gs_tree_chain(enum_entry) ) {
         USRCPOS src;
         // For now, the source location appears bogus
         // (or at least odd) for files other than the base
         // file, so lets leave it out. Temporarily.
         //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
         USRCPOS_clear(src);
#endif // KEY
         char *name2 = 
		  gs_identifier_pointer(gs_tree_purpose(enum_entry));

         if (tsize == 8) {
	   DST_CONST_VALUE_form_data8(enumerator) = 
	      gs_get_integer_value(gs_tree_value(enum_entry));
         } else {
	   DST_CONST_VALUE_form_data4(enumerator) = 
		gs_get_integer_value(gs_tree_value(enum_entry));
         }

	 DST_INFO_IDX ed = DST_mk_enumerator(src,
				name2,
				enumerator);
	 DST_append_child(t_dst_idx,ed);
	
      }


#ifdef KEY
      // Bug 1334 
      // set dst_idx (was NULL) => the reason we do all that we do
      dst_idx = t_dst_idx;
#endif
    }
    return dst_idx;
}

#ifndef TARG_ST
#ifdef KEY
typedef struct type_trans {
	DST_size_t size;
	char *name;
	DST_ATE_encoding encoding;
} type_trans;

static DST_INFO_IDX base_types[MTYPE_LAST+5] =  
{
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,	
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,	
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,	
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,
DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,DST_INVALID_INIT,
DST_INVALID_INIT
} ;
	
static type_trans ate_types[] = {
 4, "BAD",       0,		
 4, "UNK",       0,                     /* bit */
 1, "INTEGER_1", DW_ATE_signed,		/* MTYPE_I1  */
 2, "INTEGER_2", DW_ATE_signed,		/* MTYPE_I2  */
 4, "INTEGER_4", DW_ATE_signed,		/* MTYPE_I4  */
 8, "INTEGER_8", DW_ATE_signed,		/* MTYPE_I8  */
 1, "INTEGER*1", DW_ATE_unsigned,	/* MTYPE_U1  */
 2, "INTEGER*2", DW_ATE_unsigned,	/* MTYPE_U2  */
 4, "INTEGER*4", DW_ATE_unsigned,	/* MTYPE_U4  */
 8, "INTEGER*8", DW_ATE_unsigned,	/* MTYPE_U8  */
 4, "REAL_4",    DW_ATE_float,		/* MTYPE_F4  */
 8, "REAL_8",    DW_ATE_float,		/* MTYPE_F8  */
#ifdef TARG_IA64
 16,"REAL_10",   DW_ATE_float,          /* MTYPE_F10 */
#else
 10,"UNK",       DW_ATE_float,		/* MTYPE_F10 */
#endif
 16,"REAL_16",   DW_ATE_float,		/* MTYPE_F16 */
 1 ,"CHAR" ,     DW_ATE_signed_char,    /* MTYPE_STR */
 16,"REAL_16",   DW_ATE_float,		/* MTYPE_FQ  */
 1, "UNK",       DW_ATE_unsigned_char,	/* MTYPE_M   */		
 8, "COMPLEX_4", DW_ATE_complex_float,	/* MTYPE_C4  */
 16,"COMPLEX_8", DW_ATE_complex_float,	/* MTYPE_C8  */
 32,"COMPLEX_16",DW_ATE_complex_float,	/* MTYPE_CQ  */
 1, "VOID",      0,                     /* MTYPE_V   */
#ifdef TARG_IA64
 1, "UNK",       0,                     /* MTYPE_BS  */
 4, "ADDRESS_4", DW_ATE_unsigned,       /* MTYPE_A4  */
 8, "ADDRESS_8", DW_ATE_unsigned,       /* MTYPE_A8  */
 32,"COMPLEX_16",DW_ATE_complex_float,  /* MTYPE_C10 */
#else
 1, "LOGICAL_1", DW_ATE_boolean,	
 2, "LOGICAL_2", DW_ATE_boolean,	
 4, "LOGICAL_4", DW_ATE_boolean,	
 8, "LOGICAL_8", DW_ATE_boolean,	
#endif
} ;

/*===================================================
 *
 * DST_create_basetype
 *
 * Given a SCALAR ty, returns the corresponding DST
 * basetype for its typeid. Appends it to compilation 
 * unit to avoid duplication.
 *
 *===================================================
*/
static DST_INFO_IDX
DST_create_basetype (TY_IDX ty)
{
  TYPE_ID bt ;
  DST_INFO_IDX i ;

  bt = TY_mtype(ty);

  if (bt == MTYPE_V) return(DST_INVALID_IDX);

  if (TY_is_logical(Ty_Table[ty]))
    bt = bt -MTYPE_I1 + MTYPE_V + 1 ;

  if (!DST_IS_NULL(base_types[bt]))
    return base_types[bt];

  i = DST_mk_basetype(ate_types[bt].name,
		      ate_types[bt].encoding, 
		      ate_types[bt].size);

  base_types[bt] = i;
  DST_append_child(comp_unit_idx,i);
  return i;
}

// We have a subrange
// enter it.
static DST_INFO_IDX 
DST_enter_subrange_type (ARB_HANDLE ar) 
{
  DST_INFO_IDX i ;
  DST_cval_ref lb,ub;
  DST_flag     const_lb,const_ub ;
  BOOL         extent = FALSE ;
  USRCPOS src;
  USRCPOS_clear(src);
  DST_INFO_IDX type;

  const_lb = ARB_const_lbnd(ar) ;
  const_ub = ARB_const_ubnd(ar) ;

  
  if (const_lb)
    lb.cval = ARB_lbnd_val(ar) ;
  else {
    ST* var_st = &St_Table[ARB_lbnd_var(ar)];
    type = DST_create_basetype(ST_type(var_st));
    lb.ref = DST_mk_variable(
			     src,                    // srcpos
			     ST_name(var_st),
			     type,    
			     0,  
			     ST_st_idx(var_st), 
			     DST_INVALID_IDX,        
			     FALSE,                  // is_declaration
			     ST_sclass(var_st) == SCLASS_AUTO,
			     FALSE,  // is_external
			     FALSE  ); // is_artificial
#ifdef KEY
    // Bug 4829 - do not enter variables without a name.
    if (ST_name(var_st) != NULL && *ST_name(var_st) != '\0')
#endif
    DST_append_child(comp_unit_idx,lb.ref);
  } 

  if (const_ub)
    ub.cval = ARB_ubnd_val(ar) ;
  else {
    ST* var_st = &St_Table[ARB_ubnd_var(ar)];
    type = DST_create_basetype(ST_type(var_st));
    ub.ref = DST_mk_variable(
			     src,                    // srcpos
			     ST_name(var_st),
			     type,    
			     0,  
			     ST_st_idx(var_st), 
			     DST_INVALID_IDX,        
			     FALSE,                  // is_declaration
			     ST_sclass(var_st) == SCLASS_AUTO,
			     FALSE,  // is_external
			     FALSE  ); // is_artificial
#ifdef KEY
    // Bug 4829 - do not enter variables without a name.
    if (ST_name(var_st) != NULL && *ST_name(var_st) != '\0')
#endif
    DST_append_child(comp_unit_idx,ub.ref);
  }

  i = DST_mk_subrange_type(const_lb,
			   lb, 
			   const_ub,
			   ub);

  if (extent) 
    DST_SET_count(DST_INFO_flag(DST_INFO_IDX_TO_PTR(i))) ;

  return i;  
}

#endif /* KEY */
#endif /* TARG_ST */

#ifdef TARG_ST
// [CL] Try to mimic gcc's behavior with the hope of supporting
// VLA [but so far, I don't know what info to pass to be]
static void Create_bound_info(gs_t bound, DST_cval_ref *dbound, BOOL *is_cval)
{
  switch (gs_tree_code(bound)) {
  case GS_INTEGER_CST:
    dbound->cval = gs_get_integer_value(bound);
    *is_cval=TRUE;
    break;
  case GS_CONVERT_EXPR:
  case GS_NOP_EXPR:
  case GS_NON_LVALUE_EXPR:
    Create_bound_info(gs_tree_operand(bound, 0), dbound, is_cval);
    break;
  default:
    dbound->cval = 0;
    DevWarn ("Encountered VLA at line %d: debug info for bound ignored", lineno);
    {
      USRCPOS src;
      USRCPOS_clear(src);
      DST_INFO_IDX bound_var = DST_INVALID_IDX; /* Artificial variable containing the bound */

#if 0
      // [CL] I don't know how to generate valid info in such a case,
      // particularly the 'var' field of DST_mk_variable causes assertion
      // failure in be
      std::string names("int");
      DST_INFO_IDX bound_type = basetypes[names];
      bound_var = DST_mk_variable(
				  src,               // srcpos
				  NULL,              // no name
				  bound_type,
				  0,                 // offset
				  (void*) ST_st_idx(DECL_ST(bound)), // var
				  DST_INVALID_IDX,   // abstract origin
				  FALSE,             // is_declaration
				  FALSE,             // is_automatic
				  FALSE,             // is_external
				  TRUE  );           // is_artificial

      //		DST_INFO_IDX current_scope_idx =
      //		  DST_get_context(DECL_CONTEXT(type_tree));
      //		DST_append_child (current_scope_idx, bound_var);
      DST_append_child (comp_unit_idx, bound_var);
#endif

      dbound->ref = bound_var;
    }
  }
}
#endif


// We have an array
// enter it.
static DST_INFO_IDX
#ifdef TARG_ST
DST_enter_array_type(gs_t type_tree, TY_IDX ttidx  , TY_IDX idx,UINT64 tsize)
#else
DST_enter_array_type(gs_t type_tree, TY_IDX ttidx  , TY_IDX idx,INT tsize)
#endif
{ 
   DST_INFO_IDX dst_idx = TYPE_DST_IDX(type_tree);

   if(gs_tree_code_class(type_tree) != GS_TCC_TYPE) {
        DevWarn("DST_enter_array_type input not TCC_TYPE but %c",
                gs_tree_code_class(type_tree));
   }

   if(DST_IS_NULL(dst_idx)) {

      USRCPOS src;
      // For now, the source location appears bogus
      // (or at least odd) for files other than the base
      // file, so lets leave it out. Temporarily.
      //USRCPOS_srcpos(src) = Get_Srcpos();
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
      USRCPOS_clear(src);
#endif // KEY

      //if tsize == 0, is incomplete array

      gs_t elt_tree = gs_tree_type(type_tree);
      TY_IDX itx = TYPE_TY_IDX(elt_tree);

      // not created yet, so create
      DST_INFO_IDX inner_dst  =
                    Create_DST_type_For_Tree (elt_tree,itx, idx);

#if !defined KEY || defined TARG_ST
      dst_idx = DST_mk_array_type( src,
                            0, // name ?. Nope array types not named: no tag
				// in  C/C++
                           inner_dst, // element type DST_INFO_IDX
                           tsize, // type size
                           DST_INVALID_IDX, // not inlined
                           (tsize == 0)); // pass non-zero if incomplete.
#else
      // We follow the Fortran-style DW_TAG_array_type declaration whereby
      // we would have a DW_TAG_subrange_type declaration for each dimension.
      // Without this, the type information seems to be clobbered for arrays.
      dst_idx = DST_mk_array_type( src,
                            0, // name ?. Nope array types not named: no tag
				// in  C/C++
                           inner_dst, // element type DST_INFO_IDX
                           0, 
                           DST_INVALID_IDX, // not inlined
                           TRUE); 
      TY& tt = Ty_Table[ttidx];
      ARB_HANDLE arb = TY_arb(ttidx);
      DST_INFO_IDX d;
      if ( TY_kind (tt) != KIND_INVALID ) {
	for (INT index = TY_AR_ndims(ttidx) - 1; index >= 0; index--) {
	  // For C++, we may have declarations like "extern int a[]"
	  // Fix bug 322
	  if (!ARB_ubnd_var(arb[index]))
	    break;	
	  // Fix bug 383
	  if (!ARB_const_ubnd(arb[index])) { 
	    ST* var_st = &St_Table[ARB_ubnd_var(arb[index])];
	    if (ST_sclass(var_st) == SCLASS_AUTO)
	      break;
	  }
	  d = DST_enter_subrange_type(arb[index]);
	  DST_append_child(dst_idx,d);
	}
      }
#endif
      DST_append_child(comp_unit_idx,dst_idx);

      TYPE_DST_IDX(type_tree) = dst_idx;
#ifdef TARG_ST
      /* CL: Add subrange info */
      {
	gs_t index_type, min, max;
	
	index_type = gs_type_domain (type_tree);
	if (index_type) {
	  min = gs_type_min_value (index_type);
	  max = gs_type_max_value (index_type);
	  
	  if (gs_tree_type(index_type)) {
	    BOOL is_lb_cval=FALSE;
	    BOOL is_ub_cval=FALSE;
	    
	    DST_cval_ref dmin, dmax;
	    
	    DST_INFO_IDX range_idx;
	    DST_INFO_IDX index_type_idx = DST_INVALID_IDX;
	    
	    if(!(gs_tree_code (index_type) == GS_INTEGER_TYPE
		 && gs_type_name (index_type) == NULL
		 && gs_tree_code (gs_tree_type (index_type)) == GS_INTEGER_TYPE
		 && gs_type_name (gs_tree_type (index_type)) == NULL))
	      {
                index_type_idx = TYPE_DST_IDX(gs_tree_type(index_type));
		if (DST_IS_NULL(index_type_idx))
		  {
		    gs_t index_ty_tree = gs_tree_type(index_type);
		    TY_IDX type_idx = TYPE_TY_IDX(index_ty_tree);
		    // not created yet, so create
		    index_type_idx =
		      Create_DST_type_For_Tree (index_ty_tree, type_idx,
						Get_TY(index_ty_tree));
		  }
	      }
	    Create_bound_info(min, &dmin, &is_lb_cval);
	    if (max){
	      Create_bound_info(max, &dmax, &is_ub_cval);
	      
	      range_idx = DST_mk_subrange_type(is_lb_cval, dmin, is_ub_cval, dmax, FALSE, 0LL, index_type_idx);
	    } else {
	      range_idx = DST_mk_subrange_type(is_lb_cval, dmin, is_ub_cval, dmax, TRUE, 0LL, index_type_idx);
	    }
	    
	    DST_append_child(dst_idx, range_idx);
	  }
	}
      }
#endif
    }
    return dst_idx;
}

// Given a tree with
// POINTER_TYPE, type is OFFSET_TYPE
// we know this is a pointer_to_member tree.
// And we know debug_level >= 2 
// And we know we've not constructed it yet
// (see the caller code)
// What we want to construct here is
// DW_TAG_ptr_to_member_type
// DW_AT_type (of the type of the offset_type)
// DW_AT_containing_type ( of the type of the
//    ttree base-type record-type
static  DST_INFO_IDX
DST_construct_pointer_to_member(gs_t type_tree)
{
#ifndef TARG_ST
    gs_t ttree = gs_tree_type(type_tree);
#endif
#ifdef TARG_ST
    FmtAssert(gs_tree_code(type_tree) == GS_OFFSET_TYPE,
                          ("DST_construct_pointer_to_member:"
			   "invalid incoming arguments "));
#else
    FmtAssert(gs_tree_code(type_tree) == GS_POINTER_TYPE
                        && gs_tree_code(ttree) == GS_OFFSET_TYPE,
                          ("DST_construct_pointer_to_member:"
			   "invalid incoming arguments "));
#endif

    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();
    DST_INFO_IDX error_idx = DST_INVALID_INIT;


#ifdef KEY
#ifdef TARG_ST // [CL] get source location from GCC
    USRCPOS_clear(src);
    if (gs_type_stub_decl(type_tree)) {
      USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(gs_type_stub_decl(type_tree));
    }
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
#else
    USRCPOS_clear(src);
#endif // KEY

    char *name1 = 0;

//bug 11554: use is_typedef to check whether or not it is a typedef type
#ifdef KEY
    if(is_typedef(type_tree) == FALSE){
      // not a typedef type.
      name1 =  Get_Name(gs_tree_type(type_tree));
    } else {
      // is a typedef type
#ifdef TARG_ST
      name1 = Get_Name (type_tree);
#else
      gs_t tname = gs_type_name (type_tree);
      name1 = Get_Name(gs_decl_original_type(tname));
#endif
    }
#else
    if(gs_decl_original_type(type_tree)== 0) {
      // not a typedef type.
      name1 =  Get_Name(gs_tree_type(type_tree));
    } else {
      // is a typedef type
      name1 = Get_Name(gs_decl_original_type(type_tree));
    }
#endif

#ifdef TARG_ST
    gs_t member_type = gs_tree_type(type_tree);
#else
    gs_t member_type = gs_tree_type(ttree);
#endif
    if( gs_tree_code(member_type) == GS_ERROR_MARK) {
	return error_idx;
    }
#ifdef TARG_ST
    if(gs_tree_code_class(type_tree) != GS_TCC_TYPE) {
	DevWarn("Unexpected tree shape1: pointer_to_member %c\n",
		gs_tree_code_class(type_tree));
    }
#else
    if(gs_tree_code_class(ttree) != GS_TCC_TYPE) {
	DevWarn("Unexpected tree shape1: pointer_to_member %c\n",
		gs_tree_code_class(ttree));
    }
#endif
    TY_IDX midx = Get_TY(member_type);
    TYPE_TY_IDX(member_type) = midx;
    TY_IDX orig_idx = 0;

    DST_INFO_IDX mdst =
                    Create_DST_type_For_Tree(member_type,
                        midx,
			orig_idx);
#ifdef TARG_ST
    TYPE_DST_IDX(member_type) = mdst;
#else
    TYPE_DST_IDX(type_tree) = mdst;
#endif


#ifdef TARG_ST
    gs_t base_type = gs_type_offset_basetype(type_tree);
#else
    gs_t base_type = gs_type_offset_basetype(ttree);
#endif
    if( gs_tree_code(base_type) == GS_ERROR_MARK) {
	return error_idx;
    }
    if(gs_tree_code_class(base_type) != GS_TCC_TYPE) {
	DevWarn("Unexpected tree shape2: pointer_to_member %c\n",
		gs_tree_code_class(base_type));
    }
    TY_IDX container_idx = Get_TY(base_type);


    DST_INFO_IDX container_dst = Create_DST_type_For_Tree(
			base_type,
                        container_idx,
			orig_idx);
#ifdef TARG_ST
    TYPE_DST_IDX(base_type) = container_dst;
#else
    TYPE_DST_IDX(type_tree) = container_dst;
#endif


    DST_INFO_IDX lidx =
	DST_mk_ptr_to_member_type(src,
		  name1, //name, I expect it to be empty 
		  mdst	, //type of member
		  container_dst); //type of class
    return lidx;

}

      /*--------------------------------------------------
       * Visible routines for creating the DST information
       *--------------------------------------------------*/

// This is in parallel to Create_TY_For_Tree() in 
// tree_symtab.cxx  and must be kept so.
// We cannot use the TY tree as too much information
// is lost that debuggers depend on.
// type_tree_in is void * so 
//
// idx is non-zero only for RECORD and UNION, 
// when there is forward declaration.
//
// ttidx is the type tree Create_TY_For_Tree just created
// for type_tree

DST_INFO_IDX
Create_DST_type_For_Tree (gs_t type_tree, TY_IDX ttidx  , TY_IDX idx, bool ignoreconst, bool ignorevolatile)
{
    
    DST_INFO_IDX dst_idx = DST_INVALID_INIT;

    if(gs_tree_code_class(type_tree) != GS_TCC_TYPE) {
	DevWarn("Create_DST_type_For_Tree input not TCC_TYPE but %c",
		gs_tree_code_class(type_tree));
	return dst_idx;

    }
	 


    // for typedefs get the information from the base type
    if (gs_type_name(type_tree)) {
	    
	if(  idx == 0  &&
	    (gs_tree_code(type_tree) == GS_RECORD_TYPE ||
	     gs_tree_code(type_tree) == GS_UNION_TYPE) &&
	     gs_tree_code(gs_type_name(type_tree)) == GS_TYPE_DECL &&
	     gs_type_main_variant(type_tree) != type_tree) {
		idx = Get_TY (gs_type_main_variant(type_tree));

#ifndef KEY
		// The following code always a return an invalid DST_IDX. This 
		// causes the back-end to skip DW_AT_type for any variable 
		// declared to be of a user-defined type (which is a typedef 
		// of a base type).

		//if (TYPE_READONLY(type_tree))
		//	Set_TY_is_const (idx);
		//if (TYPE_VOLATILE(type_tree))
		//	Set_TY_is_volatile (idx);
		// restrict qualifier not supported by gcc
		//TYPE_TY_IDX(type_tree) = idx;
		//return idx;
		// FIX	      
		//hack so rest of gnu need know nothing of DST 

		return dst_idx;
#endif
       } else {
//
       }
    }
    DST_INFO_IDX current_scope_idx = comp_unit_idx;
    if(gs_type_context(type_tree)) {
	current_scope_idx = DST_get_context(gs_type_context(type_tree));
    }

    char *name1 = Get_Name(type_tree);

    TYPE_ID mtype;
#ifdef TARG_ST
    UINT64 tsize;
#else
    INT tsize;
#endif
    BOOL variable_size = FALSE;
    gs_t type_size = gs_type_size(type_tree);
    UINT align = gs_type_align(type_tree) / BITSPERBYTE;
    if (type_size == NULL) {
		// In a typedef'd type 
		// incomplete structs have 0 size
		FmtAssert( gs_tree_code(type_tree) == GS_ARRAY_TYPE 
			|| gs_tree_code(type_tree) == GS_UNION_TYPE
			|| gs_tree_code(type_tree) == GS_RECORD_TYPE
			|| gs_tree_code(type_tree) == GS_ENUMERAL_TYPE
			|| gs_tree_code(type_tree) == GS_VOID_TYPE
			|| gs_tree_code(type_tree) == GS_LANG_TYPE,
			  ("Create_DST_type_For_Tree: type_size NULL for non ARRAY/RECORD"));
		tsize = 0;
   }
   else {
		if (gs_tree_code(type_size) != GS_INTEGER_CST) {
#ifdef TARG_ST
		        /* Copy gcc and represent the size of a variable-sized type as -1. */
		        tsize = 0xffffffffull;
#else
			if (gs_tree_code(type_tree) == GS_ARRAY_TYPE)
				DevWarn ("Encountered VLA at line %d", lineno);
			else
				Fail_FmtAssertion ("VLA at line %d not currently implemented", lineno);
			variable_size = TRUE;
			tsize = 0;
#endif
		}
		else
			tsize = gs_get_integer_value(type_size) / BITSPERBYTE;
   }

   // why is this simpler than the one in kgccfe/wfe_dst.cxx???
   if (!ignoreconst && gs_type_readonly (type_tree)) {
       dst_idx = TYPE_DST_IDX(type_tree);
       if(DST_IS_NULL(dst_idx)) {
         TY_IDX itx = TYPE_TY_IDX(type_tree);
         DST_INFO_IDX unqual_dst = Create_DST_type_For_Tree (type_tree,itx, idx, true, false);
#ifdef TARG_ST
	 // [CL] for struct/union, const is handled in DST_enter_struct_union()
	 if ( (gs_tree_code(type_tree) != GS_RECORD_TYPE)
	      && (gs_tree_code(type_tree) != GS_UNION_TYPE) ) {
#endif
         dst_idx = DST_mk_const_type (unqual_dst) ;
         DST_append_child(current_scope_idx,dst_idx);
         TYPE_DST_IDX(type_tree) = dst_idx;
#ifdef TARG_ST
	 } else {
	   dst_idx = unqual_dst;
	 }
#endif
       }
       return dst_idx ;
   }

   if (!ignorevolatile && gs_type_volatile (type_tree)) {
       dst_idx = TYPE_DST_IDX(type_tree);
       if(DST_IS_NULL(dst_idx)) {
         TY_IDX itx = TYPE_TY_IDX(type_tree);
         DST_INFO_IDX unqual_dst = Create_DST_type_For_Tree (type_tree,itx, idx, true, true);
#ifdef TARG_ST
	 // [CL] for struct/union, volatile is handled in DST_enter_struct_union()
	 if ( (gs_tree_code(type_tree) != GS_RECORD_TYPE)
	      && (gs_tree_code(type_tree) != GS_UNION_TYPE) ) {
#endif
         dst_idx = DST_mk_volatile_type (unqual_dst) ;
         DST_append_child(current_scope_idx,dst_idx);
         TYPE_DST_IDX(type_tree) = dst_idx;
#ifdef TARG_ST
	 } else {
	   dst_idx = unqual_dst;
	 }
#endif
       }
       return dst_idx ;
   }

   int encoding = 0;
   switch (gs_tree_code(type_tree)) {
   case GS_VOID_TYPE:
   case GS_LANG_TYPE:
		//idx = MTYPE_To_TY (MTYPE_V);	// use predefined type
		break;
   case GS_BOOLEAN_TYPE:
		{
		encoding = DW_ATE_boolean;
		goto common_basetypes;
		}
   case GS_INTEGER_TYPE:
		{
		// enter base type
#ifdef KEY
		// (copied over from fix for bug 3962 in kgccfe/wfe_dst.cxx).
		// Bug 4326 - GNU produces INTEGER_TYPE node
		// even for CHARACTER_TYPE from gnu/stor_layout.c
		// and then fixes it up during Dwarf emission in
		// gnu/dwarf2out.c (base_type_die). We should do
		// the same. 
		if (tsize == 1 && 
		    (strcmp(name1, "char") == 0 ||
		     strcmp(name1, "unsigned char") == 0)) {
		  if (gs_decl_unsigned(type_tree)) {
		    encoding = DW_ATE_unsigned_char;
		  } else {
		    encoding = DW_ATE_signed_char;
		  }
		  goto common_basetypes;		    
		}		  
#endif
		if (gs_decl_unsigned(type_tree)) {
		 encoding = DW_ATE_unsigned;
		} else {
		 encoding = DW_ATE_signed;
		}
		goto common_basetypes;
		}
     case GS_CHAR_TYPE:
		{
		// enter base type
		if (gs_decl_unsigned(type_tree)) {
		 encoding = DW_ATE_unsigned_char;
		} else {
		 encoding = DW_ATE_signed_char;
		}
		goto common_basetypes;
		}
     case GS_ENUMERAL_TYPE:
		{
#ifdef KEY
                // Handle typedefs for struct/union
                if (is_typedef (type_tree))
                  dst_idx = DST_Create_type ((ST*)NULL, gs_type_name (type_tree));
                else
#endif
		dst_idx = DST_enter_enum(type_tree,ttidx,idx,
                        tsize);

		}
		break;
     case GS_REAL_TYPE:
		{
		// enter base type
		encoding = DW_ATE_float;
		goto common_basetypes;
		}
    case GS_COMPLEX_TYPE:
                // enter base type
                encoding = DW_ATE_complex_float;
    //============
    common_basetypes:
		{
		FmtAssert(name1 != 0 && strlen(name1) != 0,
		   ("name of base type empty, cannot make DST entry!"));

                std::string names(name1);
                DST_Type_Map::iterator p =
                        basetypes.find(names);
                if(p != basetypes.end()) {
                        DST_INFO_IDX t = (*p).second;
                        return t;
                } else {
#ifdef KEY
                       // Handle typedefs for common basetypes
                       if (is_typedef (type_tree))
                         dst_idx = DST_Create_type ((ST*)NULL,
                                                    gs_type_name (type_tree));
                       else
#endif
                       {
                       dst_idx = DST_mk_basetype(
                                name1,encoding,tsize);
                       basetypes[names] = dst_idx;
		       DST_append_child(comp_unit_idx,dst_idx);
                       }
                }

                }

		break;
    case GS_REFERENCE_TYPE:
	       {
                dst_idx = TYPE_DST_IDX(type_tree);
		if(DST_IS_NULL(dst_idx)) {

		  gs_t ttree = gs_tree_type(type_tree);
	          TY_IDX itx = TYPE_TY_IDX(ttree);
	          DST_INFO_IDX inner_dst =
		    Create_DST_type_For_Tree (ttree,itx, idx);


#ifdef TARG_ST
		  // [CL]
		  if (gs_type_readonly(ttree)) {
		    inner_dst = DST_mk_const_type(inner_dst);
		    DST_append_child(current_scope_idx,inner_dst);
		  }
		  if (gs_type_volatile(ttree)) {
		    inner_dst = DST_mk_volatile_type(inner_dst);
		    DST_append_child(current_scope_idx,inner_dst);
		  }
#endif
		  // not created yet, so create
		  dst_idx = DST_mk_reference_type(
		       	inner_dst,    // type ptd to
			DW_ADDR_none, // no address class
			tsize);
                                
                  DST_append_child(current_scope_idx,dst_idx);

		  TYPE_DST_IDX(type_tree) = dst_idx;
		}
               }
		break;
    case GS_POINTER_TYPE:
#ifdef KEY
               // Handle typedefs for pointer types
               if (is_typedef (type_tree))
                 dst_idx = DST_Create_type ((ST*)NULL, gs_type_name (type_tree));
               else
#endif
	       {
                dst_idx =  TYPE_DST_IDX(type_tree);
                if(DST_IS_NULL(dst_idx)) {

		  gs_t ttree = gs_tree_type(type_tree);
		  if(gs_tree_code(ttree) == GS_OFFSET_TYPE) {
#ifdef TARG_ST
		    // [CL] bug #85076: use the type pointed to
		     dst_idx = DST_construct_pointer_to_member(ttree);
#else
		     dst_idx = DST_construct_pointer_to_member(type_tree);
#endif
		  } else {

	              TY_IDX itx = TYPE_TY_IDX(ttree);
	              DST_INFO_IDX inner_dst =
		         Create_DST_type_For_Tree (ttree,itx, idx);

                      // not created yet, so create


#ifdef TARG_ST
		      // [CL]
		      if (gs_type_readonly(ttree)) {
			inner_dst = DST_mk_const_type(inner_dst);
			DST_append_child(current_scope_idx,inner_dst);
		      }
		      if (gs_type_volatile(ttree)) {
			inner_dst = DST_mk_volatile_type(inner_dst);
			DST_append_child(current_scope_idx,inner_dst);
		      }
#endif

		      dst_idx = DST_mk_pointer_type(
		       	inner_dst,    // type ptd to
			DW_ADDR_none, // no address class
			tsize);
		  }
                                
                  DST_append_child(current_scope_idx,dst_idx);

		  TYPE_DST_IDX(type_tree) = dst_idx;
                 }
               }
	       break;
    case GS_OFFSET_TYPE:
#ifdef TARG_ST
      // [CL] support pointer to member
      dst_idx = DST_construct_pointer_to_member(type_tree);
      DST_append_child(current_scope_idx,dst_idx);
      TYPE_DST_IDX(type_tree) = dst_idx;
#endif

		//  Do nothing. We should not get here.
		break;

    case GS_ARRAY_TYPE:
#ifdef TARG_ST
      //TB: generate dst for vector type
   case GS_VECTOR_TYPE:
#endif
#ifdef TARG_ST
                // [CL] Handle typedefs for array/vector
                if (is_typedef (type_tree))
                  dst_idx = DST_Create_type ((ST*)NULL, gs_type_name (type_tree));
                else
#endif
	       {
                dst_idx = DST_enter_array_type(type_tree, 
			ttidx, idx, tsize);
	       }
	       break;
    case GS_RECORD_TYPE:
    case GS_UNION_TYPE:
		{
#ifdef KEY
                // Handle typedefs for struct/union
                if (is_typedef (type_tree))
                  dst_idx = DST_Create_type ((ST*)NULL, gs_type_name (type_tree));
                else
#endif
		dst_idx = DST_enter_struct_union(type_tree,ttidx,idx,
			tsize);
		}
		break;
    case GS_METHOD_TYPE:
#ifndef TARG_ST
		// [CL] fallthrough: behave like GCC, where
		// METHOD_TYPE and FUNCTION_TYPE are handled by the
		// same code (duplicated). See gen_type_die() in
		// dwarf2out.c
		{
		//DevWarn ("Encountered METHOD_TYPE at line %d", lineno);
		}
		break;
#endif

    case GS_FUNCTION_TYPE:
		{	// new scope for local vars
#if 0
		tree arg;
		INT32 num_args;
		TY &ty = New_TY (idx);
		TY_Init (ty, 0, KIND_FUNCTION, MTYPE_UNKNOWN, NULL); 
		Set_TY_align (idx, 1);
		TY_IDX ret_ty_idx;
		TY_IDX arg_ty_idx;
		TYLIST tylist_idx;

		// allocate TYs for return as well as parameters
		// this is needed to avoid mixing TYLISTs if one
		// of the parameters is a pointer to a function

		ret_ty_idx = Get_TY(TREE_TYPE(type_tree));
		for (arg = TYPE_ARG_TYPES(type_tree);
		     arg;
		     arg = TREE_CHAIN(arg))
			arg_ty_idx = Get_TY(TREE_VALUE(arg));

		// if return type is pointer to a zero length struct
		// convert it to void
		if (!WFE_Keep_Zero_Length_Structs    &&
		    TY_mtype (ret_ty_idx) == MTYPE_M &&
		    TY_size (ret_ty_idx) == 0) {
			// zero length struct being returned
		  	DevWarn ("function returning zero length struct at line %d", lineno);
			ret_ty_idx = Be_Type_Tbl (MTYPE_V);
		}

		Set_TYLIST_type (New_TYLIST (tylist_idx), ret_ty_idx);
		Set_TY_tylist (ty, tylist_idx);
		for (num_args = 0, arg = TYPE_ARG_TYPES(type_tree);
		     arg;
		     num_args++, arg = TREE_CHAIN(arg))
		{
			arg_ty_idx = Get_TY(TREE_VALUE(arg));
			if (!WFE_Keep_Zero_Length_Structs    &&
			    TY_mtype (arg_ty_idx) == MTYPE_M &&
			    TY_size (arg_ty_idx) == 0) {
				// zero length struct passed as parameter
				DevWarn ("zero length struct encountered in function prototype at line %d", lineno);
			}
			else
				Set_TYLIST_type (New_TYLIST (tylist_idx), arg_ty_idx);
		}
		if (num_args)
		{
			Set_TY_has_prototype(idx);
			if (arg_ty_idx != Be_Type_Tbl(MTYPE_V))
			{
				Set_TYLIST_type (New_TYLIST (tylist_idx), 0);
				Set_TY_is_varargs(idx);
			}
			else
				Set_TYLIST_type (Tylist_Table [tylist_idx], 0);
		}
		else
			Set_TYLIST_type (New_TYLIST (tylist_idx), 0);
#endif
		} // end FUNCTION_TYPE scope
#ifdef TARG_ST  // [CL] generate type info for function prototype
		// useful for record/union fields that are pointers
		// to function. Generate corresponding param types
		{
		  gs_t ttree = gs_tree_type(type_tree);
	          TY_IDX itx = TYPE_TY_IDX(ttree);
		  DST_INFO_IDX ret_idx = Create_DST_type_For_Tree(ttree, idx, idx);

		  USRCPOS src;
		  // For now, the source location appears bogus
		  USRCPOS_clear(src);

		  BOOL isprototyped = FALSE;

		  if (gs_tree_type(type_tree) &&
		      gs_type_arg_types(type_tree)) {
		    isprototyped = TRUE;
		  }
	    
		  dst_idx = DST_mk_subroutine_type(src,
						   NULL,
						   ret_idx,
						   DST_INVALID_IDX,
						   isprototyped);

		  DST_append_child(current_scope_idx,dst_idx);

		  if (isprototyped) {
		    gs_t arg;
		    DST_INFO_IDX param_idx;

		    for (arg = gs_type_arg_types(type_tree);
			 arg;
			 arg = gs_tree_chain(arg)) {

		      // [CL] parameter list finished by a 'void' node
		      // means this is not a vararg function.
		      // should not generate a parameter
		      if ( (gs_tree_chain(arg) == NULL) &&
			   (gs_tree_value(arg) == gs_void_type_node()) ) {
			break;
		      }

		      DST_INFO_IDX arg_idx = TYPE_DST_IDX(gs_tree_value(arg));

		      param_idx = DST_mk_formal_parameter(src,
							  NULL,
							  arg_idx,
							  NULL,
							  DST_INVALID_IDX,
							  DST_INVALID_IDX,
							  FALSE,
							  FALSE,
							  FALSE,
							  TRUE);

		      DST_append_child(dst_idx,param_idx);
		    }

		    // Generate info for elipsis
		    if (!arg) {
		      param_idx = DST_mk_unspecified_parameters(src,
								DST_INVALID_IDX);
		      DST_append_child(dst_idx,param_idx);

		    }
		  }
		}

		TYPE_DST_IDX(type_tree) = dst_idx;
#endif
		break;
#ifdef TARG_X8664
    case GS_VECTOR_TYPE:
		{
		  // GNU's debug representation for vectors.
		  // See gen_array_type_die() for details.
		  type_tree = gs_tree_type (gs_type_fields
		               (gs_type_debug_representation_type (type_tree)));
		  ttidx = Get_TY (type_tree);
		  dst_idx = DST_enter_array_type(type_tree, ttidx, idx, tsize);
		  DST_SET_GNU_vector(DST_INFO_flag(DST_INFO_IDX_TO_PTR(dst_idx)));
		}
		break;
#endif // TARG_X8664
    default:

		FmtAssert(FALSE, ("Create_DST_type_For_Tree: unexpected tree_type"));
    }
    //if (TYPE_READONLY(type_tree))
//		Set_TY_is_const (idx);
 //   if (TYPE_VOLATILE(type_tree))
//		Set_TY_is_volatile (idx);
    // restrict qualifier not supported by gcc
 //   TYPE_TY_IDX(type_tree) = idx;
	
    
    return dst_idx;
}

extern DST_INFO_IDX
Create_DST_decl_For_Tree(
        gs_t decl, ST* var_st)
{



  DST_INFO_IDX cur_idx = DECL_DST_IDX(decl);
  if(!DST_IS_NULL(cur_idx)) {

	// Already processed. Do not redo!
	return cur_idx;
  }

  DST_INFO_IDX dst_idx = DST_INVALID_INIT;

  //if (gs_tree_code(decl) != GS_VAR_DECL) return var_idx;

  if (gs_decl_ignored_p(decl))    {
  	return cur_idx;
  }


  // if tests  for locals and returns if local 
  // if (gs_decl_context(decl) != 0)  return var_idx;


  // If this is simply an extern decl (not an instance)
  // (and the context is not an inner lexical block,
  // where we could be hiding an outer decl with the same name so
  // need a decl here) 
  // then we can just do nothing. 
  // externs get debug info at the point of def.
  if (gs_tree_code(decl) == GS_VAR_DECL && 
		 gs_decl_external(decl) && !gs_decl_common(decl)) {
      return cur_idx;
  }

#ifdef TARG_ST
  // [CL] preliminary support for anonymous unions
  if (gs_tree_code(decl) == GS_VAR_DECL && gs_decl_external(decl)) {
	return cur_idx ;
  }
#endif
#ifndef KEY
  // For now ignore plain declarations?
  // till we get more working
  if (gs_tree_code(decl) == GS_VAR_DECL && (!gs_tree_static(decl)
		 && !gs_decl_common(decl))) {
	return cur_idx ;
  }
#endif // !KEY

  // is something that we want to put in DST
  // (a var defined in this file, or a type).
  // The following eliminates locals 
  //if(!TREE_PERMANENT(decl)) {
  //	// !in permanent obstack 
  //	return cur_idx ;
  // }

  int tcode = gs_tree_code(decl);
  switch(tcode) {
  case GS_VAR_DECL: {
      //Get_ST(decl);
      dst_idx = DST_Create_var(var_st,decl);
#ifdef TARG_ST // [CL] handle anonymous union
    if (gs_anon_aggr_type_p (gs_tree_type(decl))) {
      for (gs_t mydecl = gs_decl_anon_union_elems(decl); mydecl != NULL; mydecl = gs_tree_chain(mydecl)) {
	gs_t field_decl = gs_tree_value(mydecl);
	(void)Create_ST_For_Tree (field_decl);
	// After creation of each field, make the location point to
	// the main ST by fixing what is setup in
	// dwarf_DST_producer.cxx:mk_variable()
	DST_INFO *info = DST_INFO_IDX_TO_PTR(DECL_DST_IDX(field_decl));
	DST_ASSOC_INFO_fe_ptr(
		DST_VARIABLE_def_st(
		    DST_ATTR_IDX_TO_PTR(
		        DST_INFO_attributes(info), DST_VARIABLE)))
	                    = (void*) ST_st_idx(var_st);
	DST_SET_assoc_fe(DST_INFO_flag(info));
      }
    }
#endif
      }
      break;
  case GS_TYPE_DECL: {
      //Get_ST(decl);
      dst_idx = DST_Create_type(var_st,decl);
      }
      break;
  case GS_PARM_DECL: {
      //Get_ST(decl);
      dst_idx = DST_Create_Parmvar(var_st,decl);
      }
      break;
  case GS_FUNCTION_DECL: {
      dst_idx = DST_Create_Subprogram(var_st,decl);
      }
      break;
  default: {
      }
      break;
  }
  return dst_idx;
}



static DST_INFO_IDX
DST_Create_type(ST *typ_decl, gs_t decl)
{
    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();


#ifdef TARG_ST // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(decl);
#else
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
    USRCPOS_clear(src);
#endif // KEY
#endif // TARG_ST
    DST_INFO_IDX dst_idx = DST_INVALID_INIT;
    



    char *name1 = 0; 
    if(gs_decl_original_type(decl)== 0) {
      // not a typedef type.
      name1 =  Get_Name(gs_tree_type(decl));
    } else {
      // is a typedef type
#ifdef KEY
      // Yes, this is a typedef, and so get THAT typename, not the
      // original typename
      name1 = Get_Name(gs_tree_type(decl));
#else
      name1 = Get_Name(gs_decl_original_type(decl));
#endif
    }
  
#ifndef TARG_ST // [CL] we may need to complete a forward declaration,
                // so handle the underlying type in any case.

    // FIX look in various contexts to find known types ?
    // It is not true base types that are the problem, it
    // is typedefs creating 'new types'.
    std::string names(name1);
    DST_Type_Map::iterator p =
                        basetypes.find(names);
    if(p != basetypes.end()) {
                        //Yep, already known.
        DST_INFO_IDX t = (*p).second;
        // hack so rest of gnu need know nothing of DST
        return t;
    } 
#endif

    DST_INFO_IDX current_scope_idx =
         DST_get_context(gs_decl_context(decl));

    // Nope, something new. make a typedef entry.
    // First, ensure underlying type is set up.
#ifdef KEY
    // Same as DECL_RESULT, but this looks to be the right macro
    gs_t undt = gs_decl_original_type(decl);
#else
    gs_t undt = DECL_RESULT(decl);
#endif
    TY_IDX base;

    if(!undt) {
	DevWarn ("DST no result type for typedef decl: impossible");
	return DST_INVALID_IDX;
    } 
    // Do for side effect of creating DST for base type.
    // ie, in typedef int a, ensure int is there.
    base = Get_TY(undt);
    DST_INFO_IDX dst = 
	Create_DST_type_For_Tree(undt,base,
		/* struct/union fwd decl TY_IDX=*/ 0);

#ifdef TARG_ST // [CL] don't redefine to top-level type

    // FIX look in various contexts to find known types ?
    // It is not true base types that are the problem, it
    // is typedefs creating 'new types'.
    std::string names(name1);
    DST_Type_Map::iterator p =
                        basetypes.find(names);
    if(p != basetypes.end()) {
                        //Yep, already known.
        DST_INFO_IDX t = (*p).second;
        // hack so rest of gnu need know nothing of DST
        return t;
    }
#endif

    dst_idx = DST_mk_typedef( src,
                          name1, // new type name we are defining
                          dst, // type of typedef
                          DST_INVALID_IDX);
    DST_append_child(current_scope_idx,dst_idx);
    // and add the new type to our base types map
    basetypes[names] = dst_idx;
#ifdef KEY
    TYPE_DST_IDX(decl) = dst_idx;	// bug 6247
#else
    TYPE_DST_IDX(undt) = dst_idx;
#endif

    return dst_idx;
}

static DST_INFO_IDX
DST_Create_Parmvar(ST *var_st, gs_t param)
{
    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();

#ifdef TARG_ST
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(param);
#else
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
    USRCPOS_clear(src);
#endif // KEY
#endif // TARG_ST

    DST_INFO_IDX type_idx = DST_INVALID_INIT;
#ifdef KEY //************************************************************
           //bug 11589: The var_st is the just created st for this param.
           //           Don't know why we have to get (may create) a new one.
           //           
           //NOTE:      Using Get_ST here will cause an infinite
           //           loop in compiling for VLA because of the DECL_ST
           //           set_DECL_ST does not match.
           //************************************************************
    ST * st = var_st?var_st:Get_ST(param);//As a side effect,
#else
    ST * st = Get_ST(param); // As a side effect,
#endif
                //parameters and the types are set in ST.
		// and in the DST
    gs_t type = gs_tree_type(param);

    TY_IDX ty_idx = Get_TY(type); 

    DST_INFO_IDX dtype = DECL_DST_IDX(param);
    return dtype;
}

// Look thru the members of myrecord, looking for
// static vars. If we find one with that
// linkage name, return the DST_INFO_IDX.
// Return the DST_INVALID_INIT value if such a member
// not found.
//
static DST_INFO_IDX
DST_find_class_member(char * linkage_name_in, gs_t myrecord)
{
    DST_INFO_IDX return_member_dst = DST_INVALID_INIT;
    if(!linkage_name_in)
		return return_member_dst;
    gs_t field = gs_type_fields(myrecord);
    for( ; field ; field = gs_tree_chain(field) )
    {
        if(gs_tree_code(field) == GS_VAR_DECL) {

             char * linkage_name =
                        gs_identifier_pointer(gs_decl_assembler_name (field));
	     if(linkage_name && 
			(strcmp(linkage_name,  linkage_name_in) == 0)) {
		return_member_dst =  DECL_DST_FIELD_IDX(field);
                return return_member_dst;
	     }
        }
    }
    gs_t methods = gs_type_methods(myrecord);
    for  ( ; methods != NULL; methods = gs_tree_chain(methods)) {
        if(gs_tree_code(methods) == GS_FUNCTION_DECL ) {

	   // Skip if function was never translated by GCC into RTL and so was
	   // never needed.
	   if (gs_operand(methods, GS_FLAGS) == NULL)
	     continue;

#ifndef TARG_ST
	  // [CL]
           if ( gs_decl_artificial(methods)) {
             // compiler generated methods are not interesting.
             // We want only ones user coded.
             continue;
           } else {
#endif
	     char * linkage_name = 
		   gs_identifier_pointer(gs_decl_assembler_name (methods));
	     if(linkage_name &&
			(strcmp(linkage_name,  linkage_name_in) == 0)) {
		return_member_dst =  DECL_DST_FIELD_IDX(methods);
                return return_member_dst;
#ifndef TARG_ST
	     }
#endif
           }
        }
    }


    return return_member_dst;
}

static DST_INFO_IDX
DST_Create_var(ST *var_st, gs_t decl)
{
    USRCPOS src;
    // For now, the source location appears bogus
    // (or at least odd) for files other than the base
    // file, so lets leave it out. Temporarily.
    //USRCPOS_srcpos(src) = Get_Srcpos();



    int is_external = gs_tree_public(decl); // true if var has external linkage
    int external_decl = gs_decl_external(decl); // true if var undefined
    gs_t context = gs_decl_context(decl);

    // If it is external, 
    // then unless it hides some local it need not
    // be in the dwarf at all.
    if(external_decl) {
     if( context && gs_tree_code(context) == GS_RECORD_TYPE) {
	// Is C++ class function mention, not a def.
	DST_INFO_IDX no_info = DST_INVALID_INIT;
	return no_info;
     }
     // FIXME: does not remove other externals...

    }
    char *field_name = Get_Name(decl);

    int class_var_found_member = 0;
    DST_INFO_IDX class_var_idx = DST_INVALID_INIT;
#ifdef TARG_ST
    char *linkage_name = NULL;
#endif
    if (lang_cplus) { // wgen
#ifdef TARG_ST
    // [CL] preliminary support for anonymous unions
    if (strlen(field_name) > 0
        && gs_decl_assembler_name_set_p(decl) 
	&& gs_decl_assembler_name(decl) != NULL // bug 12666
	) {
      linkage_name = gs_identifier_pointer(gs_decl_assembler_name (decl));
    }
#else
#ifdef KEY
    char *linkage_name = "";	
    if (!gs_decl_artificial (decl) && gs_decl_name(decl) 
#if 1 // wgen
        && gs_decl_assembler_name_set_p(decl) 
	&& gs_decl_assembler_name(decl) != NULL // bug 12666
#endif
       )
      linkage_name = gs_identifier_pointer(gs_decl_assembler_name (decl));
#else
    char *linkage_name = 	
		gs_identifier_pointer(gs_decl_assembler_name (decl));
#endif // KEY
#endif // TARG_ST

    if(context && gs_tree_code(context) == GS_RECORD_TYPE) {
	/*look for  static data member decl*/
	class_var_idx =
		DST_find_class_member(linkage_name, context);
	if(!DST_IS_NULL(class_var_idx)) {
		class_var_found_member = 1;
		field_name = 0; // we will use DW_AT_specification
		   //to get name in debugger
		   //So do not assign name here.
	}
	// Field name should now have the class name and ::
	// prepended, per dwarf2
	// Save_Str2 can glob 2 pieces together.
	char *classname = Get_Name(context);
	if(classname && !class_var_found_member) {
	   int len = strlen(classname);
#ifdef TARG_ST
           /* (cbr) dummy_new_mempool not yet set */
	   char newname [len+5 
			/* 5 makes room for :: and null char */];
#else
	   char* newname = new char[len+5 
			/* 5 makes room for :: and null char */];
#endif

	   // no 0 check: let it crash to get signal handler msg.

	   strcpy(newname,classname);
	   strcpy(newname+len,"::");
	   if(field_name) {
	    // We do not need the string in a stable storage
	    // area, but this is a convenient way to
	    // concatenate strings and avoid a memory leak.
	    field_name = Index_To_Str(Save_Str2(newname,field_name));
	   }

#ifndef TARG_ST
           /* (cbr) dummy_new_mempool not yet set */
	   delete [] newname;
#endif
	}

    }
    }

#ifdef TARG_ST // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(decl);
#else
#ifdef KEY
    USRCPOS_srcpos(src) = Get_Srcpos();
#else
    USRCPOS_clear(src);
#endif // KEY
#endif // TARG_ST
    DST_INFO_IDX dst = DST_INVALID_INIT;

    DST_INFO_IDX type = TYPE_DST_IDX(gs_tree_type(decl));

#ifdef TARG_ST
    // [CL] Handle const qualifier, which is attached to the
    // decl tree, not always to the type tree
    // If the type is already qualified, don't re-qualify it
    if (gs_tree_readonly(decl) && (!gs_type_readonly(gs_tree_type(decl))) ) {
      type = DST_mk_const_type(type);
      DST_append_child(comp_unit_idx,type);
    }
    // [CL] Handle volatile qualifier
    if (gs_tree_this_volatile(decl) && (!gs_type_volatile(gs_tree_type(decl))) ) {
      type = DST_mk_volatile_type(type);
      DST_append_child(comp_unit_idx,type);
    }
#endif

#ifdef KEY
    // Bug 1717 - for anonymous unions like
    //      union {
    //       int member1;
    //       char member2;
    //      };
    // a variable without a name used to be created.
    // According to the standard, there could be assignment
    // statements like "member1 = 123" and the compiler is
    // supposed to emit the correct locations as if the 
    // union members are all declared as individual variables 
    // with identical address. Here, we catch all these variables
    // and generate new entries for all members in the anonymous union as
    // if they are individual variables.
    if (field_name && strcmp(field_name, "") == 0 &&
	gs_tree_code(gs_tree_type(decl)) == GS_UNION_TYPE &&
	gs_type_fields(gs_tree_type(decl)) &&
	// exposed by bug 3531 - treat types with fields that are
	// template declarations the usual way; because we don't care
	// about them right now and we don't know how to handle them yet.
	gs_tree_code(gs_type_fields(gs_tree_type(decl))) != GS_TEMPLATE_DECL) {
      gs_t field = gs_type_fields(gs_tree_type(decl));
      DST_INFO_IDX current_scope_idx =
	DST_get_context(gs_decl_context(decl));
      for( ; field ; field = gs_tree_chain(field) )
      { 
	field_name = Get_Name(field);
	if (gs_tree_code(field) == GS_FIELD_DECL ||
	    gs_tree_code(field) == GS_VAR_DECL) {
	  dst = DST_mk_variable(src,                    // srcpos
				field_name,
				TYPE_DST_IDX(gs_tree_type(field)),
				0,  // offset (fortran uses non zero )
				(void*)ST_st_idx(var_st), // underlying type here, not typedef.
				DST_INVALID_IDX,        // abstract origin
				external_decl,          // is_declaration
				FALSE,                  // is_automatic
				is_external,  // is_external
				FALSE  ); // is_artificial	
	  // Bug 4829 - do not enter variables without a name.
	  if (field_name != NULL && *field_name != '\0')
	    DST_append_child (current_scope_idx, dst);	
	} 
	// Bug 3889 - CONST_DECL, FUNCTION_DECL, and TYPE_DECL  are skipped..
      }	
      return dst;
    }	
#endif
    dst = DST_mk_variable(
        src,                    // srcpos
        field_name,
        type,    // user typed type name here (typedef type perhaps).
	0,  // offset (fortran uses non zero )
        (void*) ST_st_idx(var_st), // underlying type here, not typedef.
        DST_INVALID_IDX,        // abstract origin
        external_decl,          // is_declaration
        FALSE,                  // is_automatic
        is_external,  // is_external
#ifdef TARG_ST
	// [CL]
	gs_decl_artificial(decl)  ); // is_artificial
#else
	FALSE  ); // is_artificial
#endif


#ifdef TARG_ST
    // [CL] support lexical blocks
    DST_INFO_IDX current_scope_idx;
    extern LEXICAL_BLOCK_INFO *current_lexical_block;

    // [CL] in wgen, debug information is created at the same time
    // scopes are handled, hence we can rely on current_lexical_block
    // But be careful, debug information for global variables is
    // emitted when they are used, not when they are declared. So,
    // check the current lexical block level only for variables with
    // context != NULL
    if (context && (current_lexical_block != 0)
	&& (current_lexical_block->dst != DST_INVALID_IDX)) {
      // [SC] var is in nested scope
      current_scope_idx = current_lexical_block->dst;
    } else {
      // [SC] var is global, or fn scope.  In both these cases,
      // DECL_CONTEXT suffices.
      current_scope_idx = DST_get_context(context);
    }
#else
    DST_INFO_IDX current_scope_idx =
         DST_get_context(gs_decl_context(decl));
#endif

#ifdef KEY
    // because all our variables have a linkage name, we don't want to do this
    //if(linkage_name && linkage_name[0] != '\0' && field_name && strcmp(linkage_name,field_name)) {
       //// we have a genuine linkage name.
      //DST_add_linkage_name_to_variable(dst, linkage_name);
    //}
#else
    // At present, DST producer in common/com does not
    // allow setting linkage name

    //if(linkage_name && basename && strcmp(linkage_name,basename)) {
      // we have a genuine linkage name.
     // DST_add_linkage_name_to_subprogram(dst, linkage_name);
    //}
#endif


#ifdef TARG_ST
// [CL] dont output invisible names
    if(field_name && linkage_name && strcmp(field_name, linkage_name) &&
       gs_tree_public(decl) && !gs_decl_abstract(decl) ) {
      DST_add_linkage_name_to_variable(dst, linkage_name);
    }
#endif

    // If this is a def of a static var member, want DW_AT_specification
    // added to point to class mem
    if(class_var_found_member) {
       DST_add_specification_to_variable(dst, class_var_idx);
    }


#ifdef KEY
    // Bug 4829 - do not enter variables without a name.
    if (field_name != NULL && *field_name != '\0')
#endif
    DST_append_child (current_scope_idx, dst);
    return dst;

}

// For each parameter decl,
// create a record and attach to the function
// it belongs to.
// is_declaration_only stuff does NOT work: it was
// for member functions, which we don't deal with. 
// see DST_enter_member_function() comments
// 
static void
DST_enter_param_vars(gs_t fndecl,
  DST_INFO_IDX parent_idx,
  gs_t parameter_list,
  int is_abstract_root,
  int is_declaration_only)
{
    USRCPOS src;
    USRCPOS_srcpos(src) = Get_Srcpos();
   
    gs_t pdecl = parameter_list;
#ifdef TARG_ST // [CL] add support for elipsis
    gs_t first_ptype = gs_type_arg_types(gs_tree_type(fndecl));
    gs_t ptype = first_ptype;
#endif

    for(  ; pdecl; pdecl = gs_tree_chain(pdecl)) {
#ifdef KEY
      // Bug 4443 - Due to the change in the front-end, PARM_DECL nodes 
      // are converted to INDIRECT_REF node now (pass param by invisible
      // reference in wfe_decl.cxx).
      if(gs_tree_code(pdecl) == GS_INDIRECT_REF) {
	_gs_code(pdecl, GS_PARM_DECL);

        DST_INFO_IDX param_idx = DST_INVALID_INIT;
        DST_INFO_IDX type_idx = DST_INVALID_INIT;
        BOOL is_artificial = gs_decl_artificial(pdecl);
	int decl_to_be_restored = 0;
	int type_to_be_restored = 0;
#ifdef TARG_ST // [CL] fix bug #54665
	// [SC] Beware: on gcc 4.2.0, non-prototyped fns have no
	// type_arg_types set.
	gs_t type = ptype ? gs_tree_value(ptype) : gs_tree_type(pdecl);
#else
	gs_t type = gs_tree_type(pdecl);
#endif

	
	DST_INFO_IDX save_type_idx = TYPE_DST_IDX(type);
	DST_INFO_IDX save_decl_idx = DECL_DST_IDX(pdecl);

	ST *st = 0;
	if(!is_declaration_only) {
	  st = Get_ST(gs_tree_operand(pdecl, 0)); // As a side effect,
		// parameters and the types are set in ST
		// and dst
		// and we do not want ST set up in this case.
	}
	


        TY_IDX ty_idx = Get_TY(type);
	

	type_idx = TYPE_DST_IDX(type);
	
#ifdef TARG_ST
	// [CL] the param type from the TYPE_ARG_TYPES list may differ
	// from the pdecl type, and may not have been emitted yet. In
	// this case, do it now.
	if (DST_IS_NULL(type_idx)) {
	  TY_IDX orig_idx = 0;

	  type_idx = Create_DST_type_For_Tree(type, ty_idx, orig_idx);
	  TYPE_DST_IDX(type) = type_idx;
	}
#endif

	char *name = Get_Name(pdecl);
	
	DST_INFO_IDX initinfo = DST_INVALID_IDX;
        //tree initl = DECL_INITIAL(pdecl);
	//if(initl) {  FIXME: optional params
		//  Get_TY(initl); // As a side effect,
			// set types, etc
	  //initinfo = DECL_DST_IDX(initl);
	//}

	ST_IDX loc = (is_abstract_root || is_declaration_only)? 
		ST_IDX_ZERO: ST_st_idx(st);
	DST_INFO_IDX aroot = DST_INVALID_IDX;
	if(!is_abstract_root && !is_declaration_only) {
	  //. get the abstract root idx if it exists
	  aroot = DECL_DST_ABSTRACT_ROOT_IDX(pdecl);
	}

#ifdef TARG_ST // [CL] get source location from GCC
	USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(pdecl);
#endif

	param_idx = DST_mk_formal_parameter(
		src,
		name,
		type_idx,
		(void*) loc, // So backend can get location.
			// For a formal in abstract root
			// or a plain declaration (no def)
			// there is no location.
		aroot, // If inlined, and this
			// is concrete instance,pass idx of formal
			// in the abstract root
		initinfo, // C++ default value (of optional param)
		0?TRUE:FALSE, // true if C++ optional param // FIXME
	        FALSE, // DW_AT_variable_parameter not set in C++.
		is_artificial,
		is_declaration_only);           // bug 1735: this is totally bogus but is needed because
                                                // the backend be/cg/cgdrawf.cxx tries to put a location
                                                // attribute into the tag if it's not a declaration.

       // producer routines thinks we will set pc to fe ptr initially
       DST_RESET_assoc_fe (DST_INFO_flag(DST_INFO_IDX_TO_PTR(param_idx)));

	// The abstract root and
	// each concrete root have
	// distinct values.
	// The concrete root values need not be recorded, and
	// they will differ (no one unique value anyway) based
        // off the same pdecl node (????). FIXME 
	if(is_abstract_root) {
	   DECL_DST_ABSTRACT_ROOT_IDX(pdecl) = param_idx;
	} 
	if (!is_declaration_only && !is_abstract_root) {
	    DECL_DST_IDX(pdecl) = param_idx;
	}

	DST_append_child(parent_idx,param_idx);	

	DST_SET_deref(DST_INFO_flag( DST_INFO_IDX_TO_PTR(param_idx)));

	_gs_code(pdecl, GS_INDIRECT_REF);
      } else
#endif
      if(gs_tree_code_class(pdecl) != GS_TCC_DECLARATION) {
	DevWarn("parameter node not decl! tree node code %d ",
		gs_tree_code(pdecl));
      } else {
        DST_INFO_IDX param_idx = DST_INVALID_INIT;
        DST_INFO_IDX type_idx = DST_INVALID_INIT;
        BOOL is_artificial = gs_decl_artificial(pdecl);
	int decl_to_be_restored = 0;
	int type_to_be_restored = 0;
#ifdef TARG_ST // [CL] fix bug #54665
	// [SC] Beware: on gcc 4.2.0, non-prototyped fns have no
	// type_arg_types set.
	gs_t type = ptype ? gs_tree_value(ptype) : gs_tree_type(pdecl);
#else
	gs_t type = gs_tree_type(pdecl);
#endif
	
	DST_INFO_IDX save_type_idx = TYPE_DST_IDX(type);
	DST_INFO_IDX save_decl_idx = DECL_DST_IDX(pdecl);

	ST *st = 0;
	if(!is_declaration_only) {
	  st = Get_ST(pdecl); // As a side effect,
		// parameters and the types are set in ST
		// and dst
		// and we do not want ST set up in this case.

	}
	


        TY_IDX ty_idx = Get_TY(type);


	type_idx = TYPE_DST_IDX(type);

#ifdef TARG_ST
	// [CL] the param type from the TYPE_ARG_TYPES list may differ
	// from the pdecl type, and may not have been emitted yet. In
	// this case, do it now.
	if (DST_IS_NULL(type_idx)) {
	  TY_IDX orig_idx = 0;

	  type_idx = Create_DST_type_For_Tree(type, ty_idx, orig_idx);
	  TYPE_DST_IDX(type) = type_idx;
	}
#endif

	char *name = Get_Name(pdecl);

	DST_INFO_IDX initinfo = DST_INVALID_IDX;
        //tree initl = DECL_INITIAL(pdecl);
	//if(initl) {  FIXME: optional params
		//  Get_TY(initl); // As a side effect,
			// set types, etc
	  //initinfo = DECL_DST_IDX(initl);
	//}

	ST_IDX loc = (is_abstract_root || is_declaration_only)? 
		ST_IDX_ZERO: ST_st_idx(st);
	DST_INFO_IDX aroot = DST_INVALID_IDX;
	if(!is_abstract_root && !is_declaration_only) {
	  //. get the abstract root idx if it exists
	  aroot = DECL_DST_ABSTRACT_ROOT_IDX(pdecl);
	}

#ifdef TARG_ST // [CL] get source location from GCC
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(pdecl);
#endif

	param_idx = DST_mk_formal_parameter(
		src,
		name,
		type_idx,
		(void*) loc, // So backend can get location.
			// For a formal in abstract root
			// or a plain declaration (no def)
			// there is no location.
		aroot, // If inlined, and this
			// is concrete instance,pass idx of formal
			// in the abstract root
		initinfo, // C++ default value (of optional param)
		0?TRUE:FALSE, // true if C++ optional param // FIXME
	        FALSE, // DW_AT_variable_parameter not set in C++.
		is_artificial,
		is_declaration_only);           // bug 1735: this is totally bogus but is needed because
                                                // the backend be/cg/cgdrawf.cxx tries to put a location
                                                // attribute into the tag if it's not a declaration.

       // producer routines thinks we will set pc to fe ptr initially
       DST_RESET_assoc_fe (DST_INFO_flag(DST_INFO_IDX_TO_PTR(param_idx)));

	// The abstract root and
	// each concrete root have
	// distinct values.
	// The concrete root values need not be recorded, and
	// they will differ (no one unique value anyway) based
        // off the same pdecl node (????). FIXME 
	if(is_abstract_root) {
	   DECL_DST_ABSTRACT_ROOT_IDX(pdecl) = param_idx;
	} 
	if (!is_declaration_only && !is_abstract_root) {
	    DECL_DST_IDX(pdecl) = param_idx;
	}

	DST_append_child(parent_idx,param_idx);
      }
#ifdef TARG_ST
      // [CL] add support for elipsis
      if (ptype) ptype = gs_tree_chain(ptype);
#endif
    }
#ifdef TARG_ST
    // Generate info for elipsis
    if (first_ptype && !ptype) {
      DST_INFO_IDX param_idx;
      param_idx = DST_mk_unspecified_parameters(src,
						DST_INVALID_IDX);
      DST_append_child(parent_idx,param_idx);
      
    }
#endif
}

#ifdef TARG_ST
// [CL] fndecl can no longer be 0: there is no need to call
// DST_Create_Subprogram() for asm stmt.
#else
//
// fndecl can be 0 if this is an asm stmt treated as a function, 
// rather than being a true function.
#endif
DST_INFO_IDX
DST_Create_Subprogram (ST *func_st, gs_t fndecl)
{
    USRCPOS src;
#ifdef TARG_ST
    // [CL] get source location from GCC
    FmtAssert (fndecl != NULL, 
	     ("DST_Create_Subprogram called with NULL fndecl"));
    USRCPOS_srcpos(src) = Get_Srcpos_From_Tree(fndecl);
#else
    USRCPOS_srcpos(src) = Get_Srcpos();
#endif
    DST_INFO_IDX dst = DST_INVALID_INIT;
    DST_INFO_IDX ret_dst = DST_INVALID_IDX;

#ifndef TARG_ST // [CL]
    DST_INFO_IDX current_scope_idx = fndecl ? 
	(DST_get_context(gs_decl_context(fndecl))): 
	comp_unit_idx;
#else
      DST_INFO_IDX current_scope_idx = DST_get_context(gs_decl_context(fndecl));
#endif

     
    BOOL is_prototyped = FALSE;

    
#ifdef TARG_ST
    if(Debug_Level >= 2) {
#else
    if(Debug_Level >= 2 && fndecl) {
#endif

	// Bug 1510 - get the result type from the function type declaration
	// rather than the result declaration type.
	gs_t restype = 0;
	if (gs_tree_type(fndecl))
	  restype = gs_tree_type(gs_tree_type(fndecl)); //bug 12820: a level of indirect

       if(restype) {
	 TY_IDX itx = Get_TY(restype);
	 ret_dst = TYPE_DST_IDX(restype); //bug 12820: return type should be set
#ifdef TARG_ST
	  if (DST_IS_NULL(ret_dst)) {
	    ret_dst = Create_DST_type_For_Tree(restype, itx, 0);
	  }
#endif
	}

        gs_t type = gs_tree_type(fndecl);
	if(type) {
	//  tree arg_types = TYPE_ARG_TYPES(type);
	//  if(arg_types) {
#ifdef TARG_ST
	  // [CL] do not pretend the function is prototyped if it
	  // isn't. Checked by GDB's gdb.base/callfuncs.exp test.
	  gs_t arg_types = gs_type_arg_types(type);
	  if (arg_types) {
	    is_prototyped = TRUE;
	  }
#endif
	 // }
	}
    }


#ifdef TARG_ST
    // [CL] get the 'true' function name
    char * basename = xstrdup((char*)gs_decl_printable_name(fndecl));
#else
    char * basename;
    if (fndecl)
	basename = gs_identifier_pointer (gs_decl_name (fndecl));
    else
        basename = ST_name(func_st);
#endif
    char * linkage_name = ST_name(func_st);
#ifndef TARG_ST
#ifdef KEY
    // Bug 3846
    if (Debug_Level > 0 && fndecl &&
	gs_identifier_opname_p (gs_decl_name(fndecl)) && 
	gs_identifier_typename_p (gs_decl_name(fndecl))) {
      basename = cplus_demangle(linkage_name, DMGL_PARAMS | DMGL_ANSI | 
                                              DMGL_TYPES);
      if (basename) {
	basename = strstr(basename, "operator ");
	FmtAssert(basename, ("NYI"));
      } else {
	// Bug 4788 has a weird mangled name which cannot be demangled using
	// c++filt; g++ also generates the same linkage name (mangled name) for
	// that symbol. However, g++ is able to get back the demangled name 
	// somehow. For now, leave this operator name as such.
	// c++filt version 3.3 has this problem but version 3.4 seems to have
	// fixed this. Since, there are lot of changes to 
	// kgnu_common/libiberty/cp-demangle.c, we will wait for the front-end
	// upgrade to 3.4 which will fix this automatically.
	DevWarn(
         "encountered a mangled name that can not be demangled using c++filt");
	basename = gs_identifier_pointer (gs_decl_name (fndecl));
      }
    }
#endif
#endif

    char * funcname = basename;
    int is_abstract_root = 0;
    DST_INFO_IDX class_func_idx = DST_INVALID_INIT;
    int class_func_found_member = 0;
#ifdef KEY
    gs_t context = NULL;
    if (fndecl) context = gs_decl_context(fndecl);
#else
    gs_t context = gs_decl_context(fndecl);
#endif
    if(context && gs_tree_code(context) == GS_RECORD_TYPE) {
        /*look for  static data member decl*/
        class_func_idx =
                DST_find_class_member(linkage_name, context);
#ifndef TARG_ST
	// [CL] keep the function name, otherwise GDB complains.
	// Actually, it seems that such info should be linked to the
	// proper scope, but this is not done in cgdwarf.cxx
        if(!DST_IS_NULL(class_func_idx)) {
                class_func_found_member = 1;
                funcname = 0; // we will use DW_AT_specification
                   //to get name in debugger
                   //So do not assign name here.
 		   // and no src position: leave that to member
		 USRCPOS_clear(src);
        }
#else
	// [CL] make sure one instance of the member function is
	// emitted.
        if(!DST_IS_NULL(class_func_idx)) {
	  class_func_found_member = 1;
	}
	else {
	    DST_enter_member_function(context,
				      Create_DST_type_For_Tree(context,
							       Get_TY(context),
							       0, false, false),
				      Get_TY(context), fndecl);
	}
#endif
        // Field name should now have the class name and ::
        // prepended, per dwarf2
        // Save_Str2 can glob 2 pieces together.
        char *classname = Get_Name(context);
        if(classname && !class_func_found_member) {
           int len = strlen(classname);
#ifdef TARG_ST
           /* (cbr) dummy_new_mempool not yet set */
           char newname [len+5
                        /* 5 makes room for :: and null char */];
#else
           char* newname = new char[len+5
                        /* 5 makes room for :: and null char */];
#endif

           // no 0 check: let it crash to get signal handler msg.

           strcpy(newname,classname);
           strcpy(newname+len,"::");
           if(funcname) {
            // We do not need the string in a stable storage
            // area, but this is a convenient way to
            // concatenate strings and avoid a memory leak.
            funcname = Index_To_Str(Save_Str2(newname,funcname));
           }

#ifndef TARG_ST
           /* (cbr) dummy_new_mempool not yet set */
           delete [] newname;
#endif
        }
    }

    linkage_name = ST_name(func_st);
    ST_IDX fstidx = ST_st_idx(func_st);

#ifdef TARG_ST
    // [CL]
    DST_virtuality  virtuality = 
		 gs_decl_pure_virtual_p(fndecl)?
			 DW_VIRTUALITY_pure_virtual
		 : gs_decl_virtual_p(fndecl)?
			DW_VIRTUALITY_virtual : DW_VIRTUALITY_none;

    DST_vtable_elem_location vtable_elem_location;
    /* (cbr) elem loc must be an integer */
    if (gs_decl_vindex(fndecl) && gs_tree_code (gs_decl_vindex(fndecl)) == GS_INTEGER_CST) {
        vtable_elem_location = gs_get_integer_value(gs_decl_vindex(fndecl));
    } else {
        vtable_elem_location = 0;
    }
#endif

    dst = DST_mk_subprogram(
        src,			// srcpos
        funcname,
        ret_dst,        	// return type
        DST_INVALID_IDX,        // Index to alias for weak is set later
        (void*) fstidx,         // index to fe routine for st_idx
        DW_INL_not_inlined,     // applies to C++
#ifdef TARG_ST
	// [CL]
	virtuality,
	vtable_elem_location,
#else
        DW_VIRTUALITY_none,     // applies to C++
        0,                      // vtable_elem_location
#endif
        FALSE,                  // is_declaration
#ifndef TARG_ST // [CL]
#ifdef KEY
        FALSE,                  // is_artificial
#endif
#endif
        is_prototyped,           // 
#ifdef TARG_ST
        ! ST_is_export_local(func_st), // is_external
        gs_decl_artificial (fndecl) );
#else
        ! ST_is_export_local(func_st) );  // is_external
#endif
    // producer routines think we will set pc to fe ptr initially
    DST_RESET_assoc_fe (DST_INFO_flag(DST_INFO_IDX_TO_PTR(dst)));
    DST_append_child (current_scope_idx, dst);

    if(class_func_found_member) {
       DST_add_specification_to_subprogram(dst, class_func_idx);
    }

    if(linkage_name && funcname && strcmp(linkage_name,funcname)) {
      // we have a genuine linkage name.
      DST_add_linkage_name_to_subprogram(dst, linkage_name);
    }
    if( !DST_IS_NULL(DECL_DST_ABSTRACT_ROOT_IDX(fndecl))) {
	// FIXME need to record abstract root!
	DevWarn("Concrete function instance has abstract root! %s\n",
		basename);
    }


#ifndef TARG_ST
    if(fndecl) {
#endif
	DECL_DST_IDX(fndecl) =  dst;
#ifndef TARG_ST
    }
#endif

    // Now we create the argument info itself, relying
    // on the is_prototyped flag above to let us know if
    // we really should do this.
#ifndef KEY
    if(is_prototyped) {
#else
    // For function declarations like
    //    foo (i) int i; { ... }
    // isprotyped will be false but, we still want to generate DST info.
    if (fndecl) {
#endif /* KEY */
       gs_t parms = gs_decl_arguments(fndecl);
       if(!parms) {
	  // Normal function, no paramaters: in C++ this means
          // it has no arguments at all.
	  // And no dwarf should be emitted.
          // DevWarn("impossible arg decls -- is %s empty?",basename?basename:"");
       } else {
	   DST_enter_param_vars(fndecl, 
			dst,
			parms, 
			/* abstract_root = */ 0,
			/* declaration only = */ 0  );
       }

    }

    return dst;
}

DST_INFO_IDX
DST_Get_Comp_Unit (void)
{
   return comp_unit_idx;
}



//***********************************************************************
// Look in "lexical.h" at push_input_stack() to find out about directories
// and search paths.
//***********************************************************************
void
DST_build(int num_copts, /* Number of options passed to fec(c) */
	  char *copts[]) /* The array of option strings passed to fec(c) */
{
   char         *src_path, *comp_info;
#ifdef KEY
   char         *cur_dir = Get_Current_Working_Directory();

   current_working_dir = current_host_dir = cwd_buffer =
#ifdef TARG_ST
                      (char *) xmalloc (strlen(cur_dir) + MAXHOSTNAMELEN + 10);
#else
                      (char *) malloc (strlen(cur_dir) + MAXHOSTNAMELEN + 10);
#endif
#endif

   dst_initialized = TRUE;

   /* Initiate the memory system */
   DST_Init (NULL, 0);
   /* Enter the file-name as the first one in the file_list 
    * (DW_AT_name => src_path).  In the case that src_path should not
    * be an absolute path, we only need to eliminate the call to 
    * Make_Absolute_Path and we will get a path relative to the cwd.
    */

   if (Orig_Src_File_Name != NULL)
   {
     src_path = Orig_Src_File_Name;
   }

   /* Get the DW_AT_comp_dir attribute (current_host_dir) */
   if (Debug_Level > 0)
   {
      int host_name_length = 0;
      
      current_host_dir = &cwd_buffer[0];
      if (gethostname(current_host_dir, MAXHOSTNAMELEN) == 0)
      {
	 /* Host name is ok */
	 host_name_length = strlen(current_host_dir);
	 if(strchr(current_host_dir,'.')) {
	    // If hostname is already a FQDN (fully qualified
	    // domain name) don't add the domain again...
	    // Somehow.
	 } else {
	   current_host_dir[host_name_length] = '.';
	   if (getdomainname(&current_host_dir[host_name_length+1], 
			   MAXHOSTNAMELEN-host_name_length) == 0)
	   {
	    /* Domain name is ok */
	    host_name_length += strlen(&current_host_dir[host_name_length]);
	   }
         }

      }
      current_host_dir[host_name_length++] = ':';  /* Prefix cwd with ':' */
      current_working_dir = &cwd_buffer[host_name_length];
   }
   else /* No debugging */
   {
      current_host_dir = NULL;
      current_working_dir = &cwd_buffer[0];
   }
#ifdef KEY
   strcpy(current_working_dir, cur_dir);
#else
   strcpy(current_working_dir, Get_Current_Working_Directory());
#endif
   if (current_working_dir == NULL) {
      perror("getcwd");
      exit(2);
   }

   /* Get the AT_producer attribute! */
#ifndef KEY
   comp_info = DST_get_command_line_options(num_copts, copts);
#else
#ifdef TARG_ST
   if (INCLUDE_STAMP)
     comp_info = (char *)xmalloc(sizeof(char)*(strlen(INCLUDE_STAMP) + sizeof("openCC ") + 1));
   else
     comp_info = (char *)xmalloc(sizeof(char)*100);
   *comp_info = '\0'; // This string may be used directly for strcat
#else
   comp_info = (char *)malloc(sizeof(char)*100);
#endif
#ifdef PSC_TO_OPEN64
   strcpy(comp_info, "openCC ");
#endif
   if (INCLUDE_STAMP)
     strcat(comp_info, INCLUDE_STAMP);
#endif

   {
      // bug 12576: If available, use the original source file name.
      char * dump_base_name = Orig_Src_File_Name ? Orig_Src_File_Name :
                                                   Src_File_Name;
#ifdef TARG_ST
	    // [CL] the full source path is needed for the debug info
      comp_unit_idx = DST_mk_compile_unit(dump_base_name,
#else
      comp_unit_idx = DST_mk_compile_unit(Last_Pathname_Component(dump_base_name),
#endif
					  current_host_dir,
					  comp_info, 
				lang_cplus ? DW_LANG_C_plus_plus : DW_LANG_C89,
					  DW_ID_case_sensitive);
   }

   free(comp_info);

   WGEN_Set_Line_And_File (0, Orig_Src_File_Name);
}

static const char *current_file_name = NULL;

void
WGEN_Set_Line_And_File (UINT line, const char* f, bool check)
{
#ifdef TARG_ST
        Set_Error_Source (f);
        Set_Error_Line (line);
#endif
	if (!dst_initialized) return;

        //bug 8895, 10632: check whether necessary to update
        //NOTE: we don't check for function entry related caller
        if(f == NULL) return;
        if(check && current_file_name)
         if(strcmp(current_file_name, f)==0)
           return;
        current_file_name = f;

        // We aren't really modifying f, this is just so we can
        // call legacy code.
        char* file = const_cast<char*>(f);

	// split file into directory path and file name
	char *dir;
	char *file_name = drop_path(file);;
	char * buf = (char *) alloca (strlen(file) + 1);
	if (file_name == file) {
		// no path
// [CL] merged from Open64-4.2.1, for bug #55000
#ifdef TARG_SL
		// NOTE! Wenbo/2007-04-26: Refer to kgccfe/wfe_dst.cxx. 
		dir = NULL;
	}
#else
		dir = current_working_dir;
	}
	else if (strncmp(file, "./", 2) == 0) {
		// current dir
		dir = current_working_dir;
	}
#endif
	else {
		// copy specified path
		strcpy (buf, file);
		dir = drop_path(buf);	// points inside string, after slash
		--dir;			// points before slash
		*dir = '\0';		// terminate path string
		dir = buf;
	}

	current_dir = Get_Dir_Dst_Info (dir);
	current_file = Get_File_Dst_Info (file_name, current_dir);
}

#ifdef KEY
void WGEN_Macro_Define (UINT line, const char *buffer)
{
  DST_mk_macr(line, (char *)buffer, 1 /* DW_MACINFO_DEFINE */);
  return;
}

void WGEN_Macro_Undef (UINT line, const char *buffer)
{
  DST_mk_macr(line, (char *)buffer, 2 /* DW_MACINFO_UNDEF */);
  return;
}

void WGEN_Macro_Start_File (UINT line, UINT file)
{
  DST_mk_macr_start_file(line, file);
}

void WGEN_Macro_End_File (void)
{
  DST_mk_macr_end_file();
}
#endif
