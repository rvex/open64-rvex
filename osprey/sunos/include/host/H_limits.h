
#ifndef H_LIMITS_H
#define H_LIMITS_H

#include <limits.h>

#define LONGLONG_MAX LLONG_MAX
#define LONGLONG_MIN LLONG_MIN
#define ULONGLONG_MAX ULLONG_MAX

#endif /* H_LIMITS_H */
