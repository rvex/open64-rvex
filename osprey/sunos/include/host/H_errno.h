
#ifndef H_ERRNO_H
#define H_ERRNO_H

#include <errno.h>

extern char* sys_errlist[];
extern int sys_nerr;

#endif /* H_ERRNO_H */
