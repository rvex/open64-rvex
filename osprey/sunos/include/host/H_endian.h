
#ifndef H_ENDIAN_H
#define H_ENDIAN_H

#include <sys/isa_defs.h>

/* also need BIG_ENDIAN, LITTLE_ENDIAN */
#define BIG_ENDIAN 4321
#define LITTLE_ENDIAN 1234

#endif /* H_ENDIAN_H */
