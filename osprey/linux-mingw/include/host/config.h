
#ifndef CONFIG_H
#define CONFIG_H

/* whether to use or not the host machine limits.h directly */
#define USE_H_LIMITS_H 1

/* whether to use or not the host machine errno.h directly */
/*#define USE_H_ERRNO_H 1*/

/* whether to use or not the host machine math.h directly */
#define USE_H_MATH_H 1

/* whether to use or not the host machine endian.h directly */
#define USE_H_ENDIAN_H 1

/* whether to use or not the host machine stdint.h directly */
#define USE_H_STDINT_H 1

/* whether to use or not the host machine bstring.h directly */
#define USE_H_BSTRING_H 1

/* whether to use or not the host machine wait.h directly */
#define USE_H_WAIT_H 1

/* whether to use or not the host machine sys/wait.h directly */
#define USE_H_SYS_WAIT_H 1

/* whether to use or not the host machine alloca.h directly */
#define USE_H_ALLOCA_H 1

/* whether to use or not the host machine values.h directly */
#define USE_H_VALUES_H 1

/* whether to use or not the host machine values.h directly */
#define USE_H_SIGNAL_H 1

/* whether to use or not the host machine dlfcn.h directly */
#define USE_H_DLFCN_H 1

/* whether to use or not the host machine unistd.h directly */
#define USE_H_UNISTD_H 1

/* whether to use or not the host machine sys/mman.h directly */
#define USE_H_MMAN_H 1

/* whether to use or not the host machine regex.h directly */
#define USE_H_REGEX_H 1

/* whether to use or not the host machine sys/times.h directly */
#define USE_H_TIMES_H 1

/* whether to use or not the host machine ar.h directly */
#define USE_H_AR_H 1

#endif /* CONFIG_H */
