
/*

  Copyright (C) 2000 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/

/* ====================================================================
 * ====================================================================
 *
 * Module: ercg.desc
 *
 *
 * Description:
 *
 * Define the code generator error message descriptors for use with
 * the error message handler errors.c.  The associated error codes are
 * defined in the file ercg.h.
 *
 * ====================================================================
 * ====================================================================
 */

#include "ercg.h"

ERROR_DESC EDESC_CG[] = {
  
  /* Scheduling preparation: */
  { EC_Ill_Cycle,	EM_Compiler | ES_ERRPHASE,	RAG_EN_NONE,
    "Illegal cycle kind (%d) in %s",
    2, ET_INT, ET_STRING, 0,0,0,0 },

  /* Register Allocation: */
  { EC_Ill_Reg_Spill1,	EM_Compiler | ES_ERRPHASE,	RAG_EN_NONE,
    "Attempted to store register %s illegally",
    1, ET_STRING, 0,0,0,0,0 },
  { EC_Ill_Reg_Spill2b,	
    EM_Continuation | EM_Compiler | ES_ERRABORT,	RAG_EN_NONE,
    "Try using -O%d",
    1, ET_INT, 0,0,0,0,0 },

  /* Generic error messages. */
  { EC_CG_Generic_Warning, EM_User | ES_WARNING, RAG_EN_NONE,
    "%s",
    1, ET_STRING, 0, 0, 0, 0, 0 },
  { EC_CG_Generic_Error, EM_User | ES_ERRPHASE, RAG_EN_NONE,
    "%s",
    1, ET_STRING, 0, 0, 0, 0, 0 },
  { EC_CG_Generic_Fatal, EM_User | ES_ERRABORT, RAG_EN_NONE,
    "%s",
    1, ET_STRING, 0, 0, 0, 0, 0 },

  /* Scheduling Prefetch warning */
  { EC_Warn_Prefetch,	EM_Compiler | ES_WARNING,	RAG_EN_NONE,
    "%s%s%d prefetch instruction(s) could not be analyzed for padding",
    3, ET_STRING, ET_STRING, ET_INT, 0, 0, 0 },

#ifdef TARG_ST
  /* Misaligned spill code generated due to insufficient stack alignment */
  { EC_Warn_Misaligned_Spill,	EM_User | ES_WARNING,	RAG_EN_NONE,
    "In function `%s', stack alignment too small for efficient memory access"
    " on type '%s'. This might also result in very long compilation time. "
    "Use aligned_stack(%d) function attribute for more efficient memory access(es) and faster compilation.",
    3, ET_STRING, ET_STRING, ET_INT, 0, 0, 0 },

  /*Warning for illegal option emitted by CG when use it*/
    { EC_Warn_ITStackAlignOpt,  EM_User | ES_WARNING,   RAG_EN_NONE,
    "-Mitstackalign=%d or -CG:it_stackalign_val=%d ignored - Argument must be a power of two in [4, 512]",
    2, ET_INT, ET_INT, 0, 0, 0, 0 },
#endif

  /* All error descriptor lists must end with a -1 error code: */
  { -1,	0, RAG_EN_NONE, "", 0, 0,0,0,0,0,0 }
};
