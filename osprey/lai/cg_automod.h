#ifdef TARG_ST

#ifndef CG_AUTOMODOPT_INCLUDED
#define CG_AUTOMODOPT_INCLUDED

extern BOOL CG_AutoMod;
extern BOOL CG_AutoMod_RelaxPdom;
void Perform_AutoMod_Optimization();

#endif /* CG_AUTOMODOPT_INCLUDED */

#endif /* TARG_ST */
