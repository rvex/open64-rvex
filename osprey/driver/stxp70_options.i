#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <SYS.h>
#include "rcparser.h"
#include "objects.h"

extern void create_implies_item (int key, int index, string name);

/*******************************************************************************
 * command_line_copy creates a copy of original command line for -V[<ver>]
 * management.
*******************************************************************************/
static int saved_argc;
static char **saved_argv;
void command_line_copy(int argc, char **argv) {
   int i;
   saved_argc = argc;
   saved_argv = malloc((saved_argc+1) * sizeof(char *));

   for (i = 0; i < argc; i++) {
      if (argv[i])
         saved_argv[i] = string_copy(argv[i]);
      else
         saved_argv[i] = NULL;
   }
   saved_argv[saved_argc] = NULL;
}


/*
 *  Auxiliary function called in phases.c that dumps all command
 *  line options to a file.
 */
boolean dump_cmdline(char* filename)
{
  FILE* f;
  int i;

  f = fopen(filename, "w");

  if (f==NULL)
    {
      return FALSE;
    }

  for (i = 0; i < saved_argc; i++)
    {
      fprintf(f, "%s ", saved_argv[i]);
    }

  fclose(f);

  return TRUE;
}


/*******************************************************************************
 * STxP70_config_mem_size returns the index of string in ptr_subopt:
 *  no            ->  0
 *  512           ->  1
 *  1k 1K         ->  2
 *  2k 2K         ->  3
 *  4k 4K         ->  4
 *  8k 8K         ->  5
 *  16k 16K       ->  6
 *  32k 32K       ->  7
 *  64k 64K       ->  8
 *  128k 128K     ->  9
 *  256k 256K     -> 10
 *  512k 512K     -> 11
 *  1m 1M         -> 12
 *  2m 2M         -> 13
 *  4m 4M         -> 14
 *  anything else -> -1
 ******************************************************************************/
static int STxP70_config_mem_size ( string ptr_subopt, int opt_512_ok ) {
   if ((ptr_subopt[0]=='n') && (ptr_subopt[1]=='o') && (ptr_subopt[2]==0)) {
      return 0x0; /*no*/
   } else if ((ptr_subopt[0]=='5') && (ptr_subopt[1]=='1') && (ptr_subopt[2]=='2')) {
      if (ptr_subopt[3]==0) {
         if (opt_512_ok==1) {
            return 1; /*512*/
         } else {
            return -1; /*512, not allowed*/
         }
      } else if (((ptr_subopt[3]=='k') || (ptr_subopt[3]=='K')) &&
                  (ptr_subopt[4]==0)) {
         return 11; /*512k or 512K*/
      } else {
         return -1;
      }
   } else if (ptr_subopt[0]=='1') {
      if (((ptr_subopt[1]=='k') || ptr_subopt[1]=='K') &&
           (ptr_subopt[2]==0)) {
         return 2; /*1k or 1K*/
      } else if (((ptr_subopt[1]=='m') || ptr_subopt[1]=='M') &&
                 (ptr_subopt[2]==0)) {
         return 12; /*1m or 1M*/
      } else if ((ptr_subopt[1]=='6') &&
                 ((ptr_subopt[2]=='k') || ptr_subopt[2]=='K') &&
                 (ptr_subopt[3]==0)) {
         return 6; /*16k or 16K*/
      } else if ((ptr_subopt[1]=='2') && (ptr_subopt[2]=='8') &&
                 ((ptr_subopt[3]=='k') || ptr_subopt[3]=='K') &&
                 (ptr_subopt[4]==0)) {
         return 9; /*128k or 128K*/
      } else {
         return -1;
      }
   } else if (ptr_subopt[0]=='2') {
      if (((ptr_subopt[1]=='k') || ptr_subopt[1]=='K') &&
          (ptr_subopt[2]==0)) {
         return 3; /*2k or 2K*/
      } else if (((ptr_subopt[1]=='m') || ptr_subopt[1]=='M') &&
                 (ptr_subopt[2]==0)) {
         return 13; /*2m or 2M*/
      } else if ((ptr_subopt[1]=='5') && (ptr_subopt[2]=='6') &&
                 ((ptr_subopt[3]=='k') || ptr_subopt[3]=='K') &&
                 (ptr_subopt[4]==0)) {
         return 10; /*256k or 256K*/
      } else {
         return -1;
      }
   } else if ((ptr_subopt[0]=='3') && (ptr_subopt[1]=='2') &&
              ((ptr_subopt[2]=='k') || ptr_subopt[2]=='K') &&
              (ptr_subopt[3]==0)) {
      return 7; /*32k or 32K*/
   } else if (ptr_subopt[0]=='4') {
      if (((ptr_subopt[1]=='k') || ptr_subopt[1]=='K') &&
          (ptr_subopt[2]==0)) {
         return 4; /*4k or 4K*/
      } else if (((ptr_subopt[1]=='m') || ptr_subopt[1]=='M') &&
                 (ptr_subopt[2]==0)) {
         return 14; /*4m or 4M*/
      } else {
         return -1;
      }
   } else if ((ptr_subopt[0]=='6') && (ptr_subopt[1]=='4') &&
              ((ptr_subopt[2]=='k') || ptr_subopt[2]=='K') &&
              (ptr_subopt[3]==0)) {
      return 8; /*64k or 64K*/
   } else if ((ptr_subopt[0]=='8') &&
              ((ptr_subopt[1]=='k') || ptr_subopt[1]=='K') &&
              (ptr_subopt[2]==0)) {
      return 5; /*8k or 8K*/
   } else {
      return -1;
   }
   /* cannot be reached */
} /* STxP70_config_mem_size */

/*******************************************************************************
 * STxP70_config_mem_if returns the index of string in ptr_subopt:
 *  no            ->  0
 *  32            ->  1
 *  64            ->  2
 *  128           ->  3
 *  256           ->  4
 *  512           ->  5
 *  anything else -> -1
 ******************************************************************************/
static int STxP70_config_mem_if ( string ptr_subopt ) {
   if ((ptr_subopt[0]=='n') && (ptr_subopt[1]=='o') && (ptr_subopt[2]==0)) {
      return 0x0; /*no*/
   } else if ((ptr_subopt[0]=='3') && (ptr_subopt[1]=='2') && (ptr_subopt[2]==0)) {
      return 1; /*32*/
   } else if ((ptr_subopt[0]=='6') && (ptr_subopt[1]=='4') && (ptr_subopt[2]==0)) {
      return 2; /*64*/
   } else if ((ptr_subopt[0]=='1') && (ptr_subopt[1]=='2') && (ptr_subopt[2]=='8') && (ptr_subopt[3]==0)) {
      return 3; /*128*/
   } else if ((ptr_subopt[0]=='2') && (ptr_subopt[1]=='5') && (ptr_subopt[2]=='6') && (ptr_subopt[3]==0)) {
      return 4; /*256*/
   } else if ((ptr_subopt[0]=='5') && (ptr_subopt[1]=='1') && (ptr_subopt[2]=='2') && (ptr_subopt[3]==0)) {
      return 5; /*512*/
   } else {
      return -1;
   }
   /* cannot be reached */
} /* STxP70_config_mem_if */

/*******************************************************************************
 * STxP70_check_yes_no returns the index of string in ptr_subopt:
 *  no            ->  0
 *  yes           ->  1
 *  anything else -> -1
 ******************************************************************************/
static int STxP70_check_yes_no ( string ptr_subopt ) {
   if ((ptr_subopt[0]=='n') && (ptr_subopt[1]=='o') && (ptr_subopt[2]==0)) {
      return 0x0; /*no*/
   } else if ((ptr_subopt[0]=='y') && (ptr_subopt[1]=='e') && (ptr_subopt[2]=='s') &&(ptr_subopt[3]==0)) {
      return 0x1; /*yes*/
   } else {
      return -1;
   }
   /* cannot be reached */
} /* STxP70_check_yes_no */

/*******************************************************************************
 * parse_corecfg_option used to manage -corecfg option: This switch is used to
 * determine core configuration option.
 ******************************************************************************
 * corecfg is shared with phases.c file which takes the value to pass it to
 * associated phase.
 ******************************************************************************
 * Syntax of -corecfg:
 *    -corecfg=n where n is an hexadecimal value.
 ******************************************************************************/
unsigned int corecfg = 0x73B0149C; /* can be changed in multiple passes */
int STxP70mult = FALSE;
static int CoreCfgDone= 0;
static int
parse_corecfg_option (char **argv, int *argi) {
   string ptr_subopt;
   string ptr_nextsubopt;
   int res = O_corecfg__;

   /* Check that it is the first core configuration */
   if (2==CoreCfgDone) {
      parse_error(option_name, "-corecfg/-corecfg1 and -Mconfig are not compatible");
      res = O_Unrecognized;
      goto corecfg_end;
   }

   /* Go to first sub-option i.e. after -corecfg= */
   ptr_subopt = string_copy(next_string(argv,argi));
   ptr_subopt = strchr(ptr_subopt,'=');
   if (NULL != ptr_subopt) ptr_subopt++;
   while (NULL != ptr_subopt) {
      extern boolean flag_HWLOOP;  /* in phases.c */

      /* sub-options are comma separated */
      ptr_nextsubopt = strchr(ptr_subopt,',');
      if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
      sscanf(ptr_subopt,"%x",&corecfg);
      if ((corecfg & 8) == 8) {
         STxP70mult = TRUE;
      } else {
         STxP70mult = FALSE;
      }
      if ((corecfg & 0xFFCFFFFF) == 0) {
         flag_HWLOOP = FALSE;
      } else {
         flag_HWLOOP = TRUE;
      }
      if ((corecfg & 4) == 0) {
         /* Only ! reg bank of 16 registers */
         toggle(&lib_kind,LIB_STXP70_16);
         create_implies_item (O_corecfg__,O_TENV_,"-TENV:disabled_registers=r16-r31");
      }
      ptr_subopt = ptr_nextsubopt;
      if (NULL!=ptr_subopt) {
         parse_error(option_name, "bad syntax for option");
         res = O_Unrecognized;
         goto corecfg_end;
      }
   }
corecfg_end:
   get_next_arg(argi);
   CoreCfgDone = 1;
   return res;
}

/*******************************************************************************
 * parse_corecfg1_option used to manage -corecfg1 option: This switch is used to
 * determine core configuration option.
 ******************************************************************************
 * corecfg1 is shared with phases.c file which takes the value to pass it to
 * associated phase.
 ******************************************************************************
 * Syntax of -corecfg1:
 *    -corecfg1=n where n is an hexadecimal value.
 ******************************************************************************/
unsigned int corecfg1= 0x00000000; /* can be changed in multiple passes */
static int
parse_corecfg1_option (char **argv, int *argi) {
   string ptr_subopt;
   string ptr_nextsubopt;
   int res = O_corecfg1__;

   /* Check that it is the first core configuration */
   if (2==CoreCfgDone) {
      parse_error(option_name, "-corecfg/-corecfg1 and -Mconfig are not compatible");
      res = O_Unrecognized;
      goto corecfg1_end;
   }

   /* Go to first sub-option i.e. after -corecfg= */
   ptr_subopt = string_copy(next_string(argv,argi));
   ptr_subopt = strchr(ptr_subopt,'=');
   if (NULL != ptr_subopt) ptr_subopt++;
   while (NULL != ptr_subopt) {
      /* sub-options are comma separated */
      ptr_nextsubopt = strchr(ptr_subopt,',');
      if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
      sscanf(ptr_subopt,"%x",&corecfg1);

      ptr_subopt = ptr_nextsubopt;
      if (NULL!=ptr_subopt) {
         parse_error(option_name, "bad syntax for option");
         res = O_Unrecognized;
         goto corecfg1_end;
      }
   }
corecfg1_end:
   get_next_arg(argi);
   CoreCfgDone = 1;
   return res;
}

/*******************************************************************************
 * parse_Mconfig_option used to manage -Mconfig option: This switch is used to
 * determine core configuration option.
 ******************************************************************************
 * corecfg is shared with phases.c file which takes the value to pass it to
 * associated phase.
 ******************************************************************************
 * Syntax of -Mconfig:
 *    -Mconfig[=context:<n>|regbank:<n>|mult:<n>|efuif:<n>|mfuif:<n>|
 *              extmemif:<n>|itcnodes:<n>|noevc|evcglobal:<n>|evclocal:<n>|
 *              hwloop:<n>|dmsize:<n>|dcache:<n>|pmsize:<n>|pcache:<n>]
 *    context:<n>     defines context number. <n> in {1,2,4,8}
 *    regbank:<n>     defines register bank number. <n> in {1,2}
 *    mult:<n>        defines multiplier implementation. <n> in {yes,no}
 *    efuif:<n>       defines extension functional unit interface width.
 *                    <n> in {no,32,64,128,256,512}
 *    mfuif:<n>       defines MFU interface width. <n> in {no,32,64,128,256,512}
 *    extmemif:<n>    defines external memory interface width. <n> in {no,32,64}
 *    itcnodes:<n>    defines ITC number of nodes. <n> in {no,8,16,32}
 *    noevc           defines EVC implementation
 *    evcglobal:<n>   defines EVC number of global events. <n> in {4,8,16,32}
 *    evclocal:<n>    defines EVC number of local events. <n> in {4,8,16,32}
 *    hwloop:<n>      defines hardware loop implementation. <n> in {no,bycxt,
 *                    forall}
 *    dmsize:<n>      defines data memory size. <n> in {no,512,1k,2k,4k,8k,16k,
 *                    32k,64k,128k,256k,512k,1M,2M,4M}
 *    dcache:<n>      defines data cache implementation. <n> in {yes,no}
 *    pmsize:<n>      defines program memory size. <n> in {no,512,1k,2k,4k,8k,
 *                    16k,32k,64k,128k,256k,512k,1M,2M,4M}
 *    pcache:<n>      defines program cache implementation. <n> in {yes,no}
 *
 *    pixel:<n>       defines pixel mode implementation. <n> in {yes,no}
 *    pixelsize:<n>   defines the pixel data size. <n> in {8,10,12,14}
 *    rompatch:<n>    defines the ROM PATCH Controller implementation. <n> in
 *                    {yes,no}.
 *    maxszmis:<n>    defines the size of the largest memory access supporting
 *                    misalignment. <n> in {no,2,4,8,16,32,64}
 *    minadmis:<n>    defines the miniam address alignment at which misaligned
 *                    memory access are supported. <n> in {1,2,4,8,16,32,64}
 *    vliw:<n>        defines the dual instruction issue implementation (v4
 *                    architecture only). <n> in {no,singlecoreALU,dualcoreALU}
 *
 *    -Mconfig        Default enables 4 contexts,
 *                                    2 register banks,
 *                                    no multiplier,
 *                                    32-bit EFU interface,
 *                                    32-bit MFU interface,
 *                                    32-bit external memory interface,
 *                                    8 ITC nodes,
 *                                    EVC with 16 global and 16 local events,
 *                                    2 hardware loops for all contexts,
 *                                    4 k-bytes data memory,
 *                                    no data cache,
 *                                    4 k-bytes program memory,
 *                                    no pcache,
 *                                    no pixel,
 *                                    no ROM PATCH,
 *                                    no misalignment supported,
 *                                    no VLIW support for v4 architecture
 ******************************************************************************/
static int
parse_Mconfig_option (char **argv, int *argi) {
   string ptr_subopt;
   string ptr_nextsubopt;
   int    res = O_Mconfig__;

   /* Check that it is the first core configuration */
   if (1==CoreCfgDone) {
      parse_error(option_name, "-Mconfig and -corecfg/-corecfg1 are not compatible");
      get_next_arg(argi);
      return O_Unrecognized;
   }

   /* Go to first sub-option i.e. after -Mconfig= */
   ptr_subopt = string_copy(next_string(argv,argi));
   if (ptr_subopt != strstr(ptr_subopt,"config=")) {
      get_next_arg(argi);
      return O_Unrecognized;
   }
   ptr_subopt = strchr(ptr_subopt,'=');
   if (NULL != ptr_subopt) ptr_subopt++;
   while (NULL != ptr_subopt) {
      /* sub-options are comma separated */
      ptr_nextsubopt = strchr(ptr_subopt,',');
      if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
      switch (*ptr_subopt) {
         case 'c':
            if ((strstr(ptr_subopt,"context:")==ptr_subopt) &&
                (strlen(ptr_subopt)==9))
            {
               corecfg &= 0xFFFFFFFC;
               switch (ptr_subopt[8]) {
                  case '1':
                     corecfg |= 0x0; break;
                  case '2':
                     corecfg |= 0x1; break;
                  case '4':
                     corecfg |= 0x2; break;
                  case '8':
                     corecfg |= 0x3; break;
                  default :
                     corecfg |= 0x2; /* default is 4 contexts */
                     res = O_Unrecognized;
                     goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'd':
            if (strstr(ptr_subopt,"dmsize:")==ptr_subopt) {
               int val;

               corecfg &= 0xFC3FFFFF;
               val = STxP70_config_mem_size(&ptr_subopt[7],1);
               if (val!=-1) {
                  corecfg |= (val << 22);
               } else {
                  corecfg |= (4 << 22);
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"dcache:")==ptr_subopt) {
               int val;

               corecfg &= 0xFBFFFFFF;
               val = STxP70_check_yes_no(&ptr_subopt[7]);
               if (val!=-1) {
                  corecfg |= (val << 26);
               } else {
                  corecfg |= (0x0 << 26); /* default is no dcache */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'e':
            if (strstr(ptr_subopt,"efuif:")==ptr_subopt) {
               int val;

               corecfg &= 0xFFFFFF8F;
               val = STxP70_config_mem_if(&ptr_subopt[6]);
               if (val!=-1) {
                  corecfg |= (val << 4);
               } else {
                  corecfg |= (1 << 4); /* default is 32 */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"extmemif:")==ptr_subopt) {
               int val;

               corecfg &= 0xFFFFF3FF;
               val = STxP70_config_mem_if(&ptr_subopt[9]);
               if ((val!=-1) && (val<=2)) { /* valid values are 0,32,64 */
                  corecfg |= (val << 10);
               } else {
                  corecfg |= (1 << 10); /* default is 32 */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"evclocal:")==ptr_subopt) {
               corecfg &= 0xFFF3FFFF;
               corecfg |= 0x00008000;
               if ((ptr_subopt[9]=='4') && (ptr_subopt[10]==0)) {
                  corecfg |= (0 << 18); /*4*/
               } else if ((ptr_subopt[9]=='8') && (ptr_subopt[10]==0)) {
                  corecfg |= (1 << 18); /*8*/
               } else if ((ptr_subopt[9]=='1') && (ptr_subopt[10]=='6') && (ptr_subopt[11]==0)) {
                  corecfg |= (2 << 18); /*16*/
               } else if ((ptr_subopt[9]=='3') && (ptr_subopt[10]=='2') && (ptr_subopt[11]==0)) {
                  corecfg |= (3 << 18); /*32*/
               } else {
                  corecfg |= (0 << 18); /* default is 4 local events */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"evcglobal:")==ptr_subopt) {
               corecfg &= 0xFFFCFFFF;
               corecfg |= 0x00008000;
               if ((ptr_subopt[10]=='4') && (ptr_subopt[11]==0)) {
                  corecfg |= (0 << 16); /*4*/
               } else if ((ptr_subopt[10]=='8') && (ptr_subopt[11]==0)) {
                  corecfg |= (1 << 16); /*8*/
               } else if ((ptr_subopt[10]=='1') && (ptr_subopt[11]=='6') && (ptr_subopt[12]==0)) {
                  corecfg |= (2 << 16); /*16*/
               } else if ((ptr_subopt[10]=='3') && (ptr_subopt[11]=='2') && (ptr_subopt[12]==0)) {
                  corecfg |= (3 << 16); /*32*/
               } else {
                  corecfg |= (2 << 16); /* default is 16 global events */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'h':
            if (strstr(ptr_subopt,"hwloop:")==ptr_subopt) {
               extern boolean flag_HWLOOP;  /* in phases.c */
               corecfg &= 0xFFCFFFFF;
               if ((ptr_subopt[7]=='n') && (ptr_subopt[8]=='o') && (ptr_subopt[9]==0)) {
                  corecfg |= (0 << 20); /*no*/
                  flag_HWLOOP = FALSE;
               } else if ((ptr_subopt[7]=='b') && (ptr_subopt[8]=='y') &&
                          (ptr_subopt[9]=='c') && (ptr_subopt[10]=='x') &&
                          (ptr_subopt[11]=='t') && (ptr_subopt[12]==0)) {
                  flag_HWLOOP = TRUE;
                  corecfg |= (1 << 20); /*bycxt*/
               } else if ((ptr_subopt[7]=='f') && (ptr_subopt[8]=='o') &&
                          (ptr_subopt[9]=='r') && (ptr_subopt[10]=='a') &&
                          (ptr_subopt[11]=='l') && (ptr_subopt[12]=='l') &&
                          (ptr_subopt[13]==0)) {
                  flag_HWLOOP = TRUE;
                  corecfg |= (3 << 20); /*forall*/
               } else {
                  flag_HWLOOP = TRUE;
                  corecfg |= (3 << 20); /* default is 2 hwloops for all cxt */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'i':
            if (strstr(ptr_subopt,"itcnodes:")==ptr_subopt) {
               corecfg &= 0xFFFF8FFF;
               if ((ptr_subopt[9]=='n') && (ptr_subopt[10]=='o') && (ptr_subopt[11]==0)) {
                  corecfg |= (0 << 12); /*no*/
               } else if ((ptr_subopt[9]=='8') && (ptr_subopt[10]==0)) {
                  corecfg |= (1 << 12); /*8*/
               } else if ((ptr_subopt[9]=='1') && (ptr_subopt[10]=='6') && (ptr_subopt[11]==0)) {
                  corecfg |= (2 << 12); /*16*/
               } else if ((ptr_subopt[9]=='3') && (ptr_subopt[10]=='2') && (ptr_subopt[11]==0)) {
                  corecfg |= (3 << 12); /*32*/
               } else {
                  corecfg |= (1 << 12); /* defqult is 8 ITC nodes */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'm':
            if (strstr(ptr_subopt,"mult:")==ptr_subopt) {
               int val;

               corecfg &= 0xFFFFFFF7;
               val = STxP70_check_yes_no(&ptr_subopt[5]);
               if (val!=-1) {
                  corecfg |= (val << 3);
                  STxP70mult = (val)?TRUE:FALSE;
               } else {
                  corecfg |= (0x0 << 3); /* default is no mult */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"mfuif:")==ptr_subopt) {
               int val;

               corecfg &= 0xFFFFFC7F;
               val = STxP70_config_mem_if(&ptr_subopt[6]);
               if (val!=-1) {
                  corecfg |= (val << 7);
               } else {
                  corecfg |= (1 << 7); /* default is 32 */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"maxszmis:")==ptr_subopt) {
               corecfg1 &= 0xFFFFFF8F;
               if ((ptr_subopt[9]=='n') && (ptr_subopt[10]=='o') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (0 << 4); /*no*/
               } else if ((ptr_subopt[9]=='2') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (1 << 4); /*2-byte*/
               } else if ((ptr_subopt[9]=='4') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (2 << 4); /*4-byte*/
               } else if ((ptr_subopt[9]=='8') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (3 << 4); /*8-byte*/
               } else if ((ptr_subopt[9]=='1') && (ptr_subopt[10]=='6') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (4 << 4); /*16-byte*/
               } else if ((ptr_subopt[9]=='3') && (ptr_subopt[10]=='2') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (5 << 4); /*32-byte*/
               } else if ((ptr_subopt[9]=='6') && (ptr_subopt[10]=='4') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (6 << 4); /*64-byte*/
               } else {
                  corecfg1 |= (0 << 4); /* default is no misaligned memory access */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"minadmis:")==ptr_subopt) {
               corecfg1 &= 0xFFFFFC7F;
               if ((ptr_subopt[9]=='1') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (0 << 7); /*1-byte*/
               } else if ((ptr_subopt[9]=='2') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (1 << 7); /*2-byte*/
               } else if ((ptr_subopt[9]=='4') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (2 << 7); /*4-byte*/
               } else if ((ptr_subopt[9]=='8') && (ptr_subopt[10]==0)) {
                  corecfg1 |= (3 << 7); /*8-byte*/
               } else if ((ptr_subopt[9]=='1') && (ptr_subopt[10]=='6') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (4 << 7); /*16-byte*/
               } else if ((ptr_subopt[9]=='3') && (ptr_subopt[10]=='2') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (5 << 7); /*32-byte*/
               } else if ((ptr_subopt[9]=='6') && (ptr_subopt[10]=='4') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (6 << 7); /*64-byte*/
               } else {
                  corecfg1 |= (0 << 7); /* default is 1-byte */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'n':
            if (!strcmp(ptr_subopt,"noevc")) {
               corecfg &= 0xFFF07FFF;
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'p':
            if (strstr(ptr_subopt,"pmsize:")==ptr_subopt) {
               int val;

               corecfg &= 0x87FFFFFF;
               val = STxP70_config_mem_size(&ptr_subopt[7],0);
               if (val!=-1) {
                  corecfg |= (val << 27);
               } else {
                  corecfg |= (4 << 27);
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"pcache:")==ptr_subopt) {
               int val;

               corecfg &= 0x7FFFFFFF;
               val = STxP70_check_yes_no(&ptr_subopt[7]);
               if (val!=-1) {
                  corecfg |= (val << 31);
               } else {
                  corecfg |= (0x0 << 31); /* default is no dcache */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"pixel:")==ptr_subopt) {
               int val;

               corecfg1 &= 0xFFFFFFFE;
               val = STxP70_check_yes_no(&ptr_subopt[6]);
               if (val!=-1) {
                  corecfg1 |= (val << 0);
               } else {
                  corecfg1 |= (0x0 << 0); /* default is no pixel mode */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"pixelsize:")==ptr_subopt) {
               corecfg1 &= 0xFFFFFFF9;
               if ((ptr_subopt[10]=='8') && (ptr_subopt[11]==0)) {
                  corecfg1 |= (0 << 1); /*8-bit*/
               } else if ((ptr_subopt[10]=='1') && (ptr_subopt[11]=='0') && (ptr_subopt[12]==0)) {
                  corecfg1 |= (1 << 1); /*10-bit*/
               } else if ((ptr_subopt[10]=='1') && (ptr_subopt[11]=='2') && (ptr_subopt[12]==0)) {
                  corecfg1 |= (2 << 1); /*12-bit*/
               } else if ((ptr_subopt[10]=='1') && (ptr_subopt[11]=='4') && (ptr_subopt[12]==0)) {
                  corecfg1 |= (3 << 1); /*14-bit*/
               } else {
                  corecfg1 |= (0 << 1); /* default is 8-bit pixels */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'r':
            if (strstr(ptr_subopt,"regbank:")==ptr_subopt) {

               corecfg &= 0xFFFFFFFB;
               switch (ptr_subopt[8]) {
                  case '1':
                     corecfg |= (0x0 << 2);
                     /* Only one regbank of 16 registers */
                     toggle(&lib_kind,LIB_STXP70_16);
                     create_implies_item (O_Mconfig__,O_TENV_,"-TENV:disabled_registers=r16-r31");
                     break;
                  case '2':
                     corecfg |= (0x1 << 2); break;
                  default :
                     corecfg |= (0x1 << 2); /* default is 2 regbanks */
                     res = O_Unrecognized;
                     goto Mconfig_end;
               }
            } else if (strstr(ptr_subopt,"rompatch:")==ptr_subopt) {
               int val;

               corecfg1 &= 0xFFFFFFF7;
               val = STxP70_check_yes_no(&ptr_subopt[9]);
               if (val!=-1) {
                  corecfg1 |= (val << 3);
               } else {
                  corecfg1 |= (0x0 << 3); /* default is no rom patch */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         case 'v':
            corecfg1 &= 0xFFFFF3FF;
            if (strstr(ptr_subopt,"vliw:")==ptr_subopt) {
               if ((strstr(&ptr_subopt[5],"no")==&ptr_subopt[5]) && (ptr_subopt[7]==0)) {
                  corecfg1 |= 0 << 10;
               } else if ((strstr(&ptr_subopt[5],"singlecoreALU")==&ptr_subopt[5]) && (ptr_subopt[18]==0)) {
                  corecfg1 |= 1 << 10;
               } else if ((strstr(&ptr_subopt[5],"dualcoreALU")==&ptr_subopt[5]) && (ptr_subopt[16]==0)) {
                  corecfg1 |= 3 << 10;
               } else {
                  corecfg1 |= (0 << 10); /* default is no vliw */
                  res = O_Unrecognized;
                  goto Mconfig_end;
               }
            } else {
               res = O_Unrecognized;
               goto Mconfig_end;
            }
            break;
         default :
            res = O_Unrecognized;
            goto Mconfig_end;
      }
      ptr_subopt = ptr_nextsubopt;
   }
Mconfig_end:
   get_next_arg(argi);
   CoreCfgDone = 2;
   return res;
} /* parse_Mconfig_option */

/******************************************************************************
 * extension_create_implies_item is used to create "implied" for Mextension
 * sub-routines.
 ******************************************************************************
 * Based on extension_implied_list_t type.
 ******************************************************************************/
static void
extension_create_implies_item ( extension_T * ext_opt, int index, string name ) {
   extension_implies_list_T *p;

   assert(NULL!=ext_opt);
   p = (extension_implies_list_T *) malloc(sizeof(extension_implies_list_T));
   p->info_index = index;
   p->name = string_copy(name);
   p->next = ext_opt->implies;
   ext_opt->implies = p;
}

/******************************************************************************
 * transmit_to_implies is used to transmit content of stxp70extrc information
 * into implies item of associated internals.
 ******************************************************************************
 * Based on extension_implied_list_t type.
 ******************************************************************************/
static void transmit_to_implies ( extension_T * ext_opt, string to_transmit,
                                  int section ) {
   string ptr_start = to_transmit;
   string ptr_stop;
   string ptr_end;

   if (NULL==to_transmit) return;
   while (isspace(*ptr_start)) { ptr_start++; } /* removes leading spaces */
   ptr_stop = &to_transmit[strlen(to_transmit)-1];
   while (ptr_start<ptr_stop && 0!=*ptr_start) {
      ptr_end = ptr_start+1;
      while (*ptr_end!=0 && !isspace(*ptr_end)) { ptr_end++; }
      *ptr_end++=0;
      if (*ptr_start=='-') {
         extension_create_implies_item(ext_opt,section,ptr_start);
      }
      if (ptr_end>=ptr_stop) {
         ptr_start = ptr_stop;
      } else {
         ptr_start=strchr(ptr_end,'-');
      }
   }
}

static string sxextensionrcname = NULL;

/*****************************************************************************
 * Called by -Mextrcdir command line switch which sets stxp70extrc file path *
 *                                                                           *
 * [YJ] In previous versions of this routine, we used to accept two forms    *
 *      for optargs, namely [directory] or [=directory]. However, it turned  *
 *      out to be problematic with IPA. Indeed, in *cc file (file used to    *
 *      store input command lines so as to generate correct command lines    *
 *      when ipa_link is called), we had to support [==directory] syntax.    *
 *      To solve this problem, we modified OPTION file so as to support both *
 *      [-Mextrcdir] and [-Mextrcdir=] options. As a result, optargs should  *
 *      never begins with the [=] character.                                 *
 *****************************************************************************/
static int new_sxextensionrcname = 0;
extension_T static_ext_opt[STXP70_ARCH_SIZE][STXP70_EXTHWTYPE_SIZE][STXP70_MAX_EXTENSION];
int static_ext_nr[STXP70_ARCH_SIZE][STXP70_EXTHWTYPE_SIZE] = { {0,0,0},{0,0,0} };

void Process_extrcdir ( string optargs ) {
   /* Checks if optargs is a valid path */
   if (SYS_is_dir(optargs)) {
      sxextensionrcname = string_copy(optargs);
      new_sxextensionrcname = 1;
   } else {
      error("[%s] is not a valid directory for -Mextrcdir",optargs);
   }
}

/******************************************************************************
 * create_ext_opt construct information to deal with user-defined extensions.
 ******************************************************************************
 * allow_replace indicates if extension replacement is allowed in the list (1)
 * or not (0).
 ******************************************************************************
 * Syntax of rcfile is specified by librcparser.
 ******************************************************************************/

static void
create_ext_opt ( char * rcfile_path, int allow_replace ) {
   int extid;
   int i, j;
   int entry_static_ext_nr[STXP70_ARCH_SIZE][STXP70_EXTHWTYPE_SIZE];

   for (i=0;i<STXP70_ARCH_SIZE;i++) {
      for (j=0;j<STXP70_EXTHWTYPE_SIZE;j++) {
         entry_static_ext_nr[i][j] = static_ext_nr[i][j];
      }
   }
   RCparser_Init(RCPARSER_OPEN64);
   RCparser_parse(rcfile_path,"stxp70cc");
   FOREACH_EXTENSION(extid) {
      RCparser_ExtensionInfoT * Extension;
      int ExistingExt[STXP70_ARCH_SIZE][STXP70_EXTHWTYPE_SIZE] = {{-1,-1,-1},{-1,-1,-1}};
      int archi;
      int exthwtype;

      Extension = RCparser_getextinfo(extid,"stxp70cc");
      if (debug) {
         RCparser_printextinfo(Extension);
      }
      if (!strcmp(Extension->Architecture,"stxp70v3")) {
         archi = STXP70_ARCH_V3;
      } else if (!strcmp(Extension->Architecture,"stxp70v4")) {
         archi = STXP70_ARCH_V4;
      } else {
         error("Inconsistent %s file. %s architecture not supported\n",rcfile_path,Extension->Architecture);
      }
      if (!strcmp(Extension->ExtHwType,"novliw")) {
         exthwtype = STXP70_EXTHWTYPE_SINGLE;
      } else if (!strcmp(Extension->ExtHwType,"single")) {
         exthwtype = STXP70_EXTHWTYPE_SINGLE_DUAL;
      } else if (!strcmp(Extension->ExtHwType,"dual")) {
         exthwtype = STXP70_EXTHWTYPE_DUAL_DUAL;
      } else {
         error("Inconsistent %s file. %s extenwion hardware type not supported\n",rcfile_path,Extension->ExtHwType);
      }

      for (i=0;ExistingExt[archi][exthwtype]==-1 && i<static_ext_nr[archi][exthwtype];i++) {
         if (!strcmp(Extension->Name,static_ext_opt[archi][exthwtype][i].name)) {
            ExistingExt[archi][exthwtype] = i;
         }
      }
      if (-1==ExistingExt[archi][exthwtype]) {
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].name         = string_copy(Extension->Name);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].architecture = string_copy(Extension->Architecture);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].exthwtype    = string_copy(Extension->ExtHwType);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].help         = string_copy(Extension->HelpMsg);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].libpath      = string_copy(Extension->CmpLibPath);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].libname      = string_copy(Extension->CmpLibName);
         static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]].implies      = NULL;
         /* Transmit defines into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]],Extension->Defines,O_D);
         /* Transmit includes into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]],Extension->Includes,O_I__);
         /* Transmit libraries into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]],Extension->XLibs,O_L__);
         /* Transmit TARG into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]],Extension->CmpTargOpt,O_TARG_);
         /* Transmit ABI into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][static_ext_nr[archi][exthwtype]],"",O_fpx_abi_variant);
         static_ext_nr[archi][exthwtype]++;
      } else if (allow_replace && ExistingExt[archi][exthwtype]<entry_static_ext_nr[archi][exthwtype]) {
         extension_implies_list_T *p, *to_free;

         /* Free potential implies existing */
         p = static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]].implies;
         while (NULL != p ) {
            to_free = p;
            p = p->next;
            free(to_free);
         }
         static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]].implies = NULL;
         static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]].help    = string_copy(Extension->HelpMsg);
         static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]].libpath = string_copy(Extension->CmpLibPath);
         static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]].libname = string_copy(Extension->CmpLibName);
         /* Transmit defines into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]],Extension->Defines,O_D);
         /* Transmit includes into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]],Extension->Includes,O_I__);
         /* Transmit libraries into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]],Extension->XLibs,O_L__);
         /* Transmit TARG into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]],Extension->CmpTargOpt,O_TARG_);
         /* Transmit ABI into implies list */
         transmit_to_implies(&static_ext_opt[archi][exthwtype][ExistingExt[archi][exthwtype]],"",O_fpx_abi_variant);
      } else {
         error("Inconsistent %s file. %s extension specified twice\n",rcfile_path,Extension->Name);
      }
   }
}

/******************************************************************************
 * get_sxext_basedir_TS_410 used to build directory associated to stxp70
 * extension definitions in the toolset tree structure (new structure at TS
 * 4.1.0).
 ******************************************************************************
 * Extension definitions are supposed to be located at
 * <driver_root>/../../../sxext
 ******************************************************************************
 * This base directory is built only once in sxext_basedir_TS_410.
 ******************************************************************************/
#define STXP70EXTRC_PATH_NEW "sxext"
static string sxext_basedir_TS_410 = NULL;
static char * get_sxext_basedir_TS_410 ( char * driver_path ) {
   if (NULL==sxext_basedir_TS_410) {
      sxext_basedir_TS_410 = concat_path(SYS_idirname(SYS_idirname(SYS_idirname(SYS_adirname(SYS_full_path(driver_path))))),
                                         STXP70EXTRC_PATH_NEW);
   }
   return sxext_basedir_TS_410;
}

/******************************************************************************
 * get_sxext_basedir used to build directory associated to stxp70 extension
 * definitions in the toolset tree structure.
 ******************************************************************************
 * Extension definitions are supposed to be located at
 * <driver_root>/../../../sxext/<core> where <core> is "stxp70v3" or "stxp70v4"
 ******************************************************************************
 * if no -mcore command line switch has been specified, then look for
 * SXARCHITECTURE environment variable. This implies that -mcore must always
 * be specified before any -Mextension switch.
 ******************************************************************************
 * This base directory is built only once in sxext_basedir.
 ******************************************************************************/
#define STXP70EXTRC_PATH "sxext/%s"
static string sxext_basedir = NULL;
static int TS_tree_structure = 0x400; /* Toolset tree structure is evolving in 0x410 */
static char * get_sxext_basedir ( char * driver_path ) {
   if (NULL==sxext_basedir) {
      char *proc_env_name=NULL;
      static char sxext_path[20];

      if (proc == UNDEFINED && (proc_env_name = getenv("SXARCHITECTURE")) != NULL) {
         if (same_string(proc_env_name,"stxp70v3")) {
            snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v3");
         } else if (same_string(proc_env_name,"stxp70v4")) {
            snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v4");
         } else {
            warning("No way to determine core architecture used. Assuming STxP70 v3");
            snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v3");
         }
      } else if (proc == PROC_stxp70_v3) {
         snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v3");
      } else if (proc == PROC_stxp70_v4_novliw || proc == PROC_stxp70_v4_single || proc == PROC_stxp70_v4_dual) {
         snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v4");
      } else {
         warning("No way to determine core architecture used. Assuming STxP70 v3");
         snprintf(sxext_path,sizeof(sxext_path),STXP70EXTRC_PATH,"stxp70v3");
      }
      sxext_basedir = concat_path(SYS_idirname(SYS_idirname(SYS_idirname(SYS_adirname(SYS_full_path(driver_path))))),
                                  sxext_path);
   }
   return sxext_basedir;
}

/******************************************************************************
 * extract_from_sxextensionrc used to get valid sub-commands for -Mextension.
 ******************************************************************************
 * stxp70extrc file is supposed to be located at <sxext_basedir>/bin and
 * named stxp70extrc unless command line switch -Mextrc is specified.
 ******************************************************************************
 * Syntax of stxp70extrc is specified by librcparser.
 ******************************************************************************/
#define STXP70EXTRC_NAME "stxp70extrc"
void
extract_from_sxextensionrc ( char * driver_path ) {
   static int         first_pass = 1;
   extern int         ExtensionSeen;

   if ((first_pass==1) || (new_sxextensionrcname==1)) {
      FILE * sxrc_file;
      struct stat bufstat;
      string stxp70extrc_path;

      if (first_pass==1) {
         /* structure for native extensions is built once */
         //[dt] We need to inform that we use an extension to select arch_vx_ext
         ExtensionSeen=1;

         /* Compiler-known extensions (built-in) */
         static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]].name    = string_copy("fpx");
         static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]].help    = string_copy("IEEE-754 single precision floating point extension on port 1");
         static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]].implies = NULL;
         extension_create_implies_item(&static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]],O_TARG_,"-TARG:enable_fpx=on");
         extension_create_implies_item(&static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]],O_TARG_,"-TARG:enable_mx=on");
         extension_create_implies_item(&static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]],O_TARG_,"-TARG:abi=stxp70-fpx");
         extension_create_implies_item(&static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]],O_fpx_abi_variant,"-fpx-abi-variant");
         extension_create_implies_item(&static_ext_opt[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE][static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]],O_D,"-D__FPX");
         static_ext_nr[STXP70_ARCH_V3][STXP70_EXTHWTYPE_SINGLE]++;

         /* First of all and only once, read a potential stxp70extrc file in <driver_root>/../../../sxext */
         stxp70extrc_path = concat_path(get_sxext_basedir_TS_410(driver_path),STXP70EXTRC_NAME);
         if (debug) {
            fprintf(stderr, "stxp70extrc_path=[%s]\n",stxp70extrc_path);
            }
         if (stat(stxp70extrc_path,&bufstat)==0) {
            TS_tree_structure = 0x410;
            create_ext_opt(stxp70extrc_path,0);
         } else {
            /* Second of all and only once if not found in new structure environment, read a potential *
             * stxp70extrc file in <driver_root>/../../../sxext/stxp70v?/bin                           */
            stxp70extrc_path = concat_path(concat_path(get_sxext_basedir(driver_path),"/bin"),STXP70EXTRC_NAME);
            if (debug) {
               fprintf(stderr, "stxp70extrc_path=[%s]\n",stxp70extrc_path);
            }
            if (stat(stxp70extrc_path,&bufstat)==0) {
               create_ext_opt(stxp70extrc_path,0);
            }
         }
      }

      /* Then, potentially read a local user-defined extensions */
      if (NULL!=sxextensionrcname) {
         if (debug) {
            fprintf(stderr, "sxextensionrcpath=[%s]\n",sxextensionrcname);
         }
         sxextensionrcname = concat_path(sxextensionrcname,STXP70EXTRC_NAME);

         if (stat(sxextensionrcname,&bufstat)==0) {
            create_ext_opt(sxextensionrcname,1);
         }
      }
      first_pass = 0;
      new_sxextensionrcname = 0;
   }
}

/******************************************************************************
 * exist_extension checks if ext_name exists over the stxp70extrc files parsed
 ******************************************************************************/
static int  MextensionFirstPass = 1;
char Mextension_str[256] = { '\0' };
static char * Mextensionext_str_p;
#define MAX_EXTENSION_ENTRIES 32
string ExtensionNameTab[MAX_EXTENSION_ENTRIES];
int ExtensionNameTabSize = 0;


static boolean
exist_extension ( char * ext_name ) {
  int i, j, k;

  char * colon_ptr = strchr(ext_name,':');
  int    start, stop;
  if (NULL!=colon_ptr) {
     *colon_ptr++ = 0;
     if (!strcmp(colon_ptr,"novliw")) {
        start = STXP70_EXTHWTYPE_SINGLE;
     } else if (!strcmp(colon_ptr,"single")) {
        start = STXP70_EXTHWTYPE_SINGLE_DUAL;
     } else if (!strcmp(colon_ptr,"dual")) {
        start = STXP70_EXTHWTYPE_DUAL_DUAL;
     } else {
        error("INTERNAL ERROR: extension [%s] HW type [%s] not supported",ext_name,colon_ptr);
        exit(-1);
     }
     stop  = start+1;
  } else {
     start = 0;
     stop  = STXP70_EXTHWTYPE_SIZE;
  }
  for (i=0;i<STXP70_ARCH_SIZE;i++) {
     for (j=start;j<stop;j++) {
        for (k=0;k<static_ext_nr[i][j];k++) {
           if (!strcmp(ext_name,static_ext_opt[i][j][k].name)) {
              if (MextensionFirstPass) {
                 MextensionFirstPass=0;
              } else {
                 * Mextensionext_str_p ++ = ',';
              }
              strcat(Mextensionext_str_p,ext_name);
              Mextensionext_str_p += strlen(ext_name);
              if (NULL!=colon_ptr) {
                 strcat(Mextensionext_str_p,":");
                 strcat(Mextensionext_str_p,colon_ptr);
                 Mextensionext_str_p += (strlen(colon_ptr)+1);
              }
               if (ExtensionNameTabSize>=MAX_EXTENSION_ENTRIES) {
                 error("INTERNAL ERROR: cannot add %s extension",ext_name);
                 exit(-1);
              }
              ExtensionNameTab[ExtensionNameTabSize++] = string_copy(ext_name);
              return TRUE;
           }
        }
     }
  }
  return FALSE;
}


/******************************************************************************
 * strcat_on_TENVExtension() is used locally in this file append a string
 * at the end of the <TENVExttension_str> string. It handles dynamic
 * reallocation of the buffer.
 ******************************************************************************/
#define TENVEXTENSION_BASE_STR     "-TENV::extension="
#define TENVEXTENSION_BASE_STR_LEN (17)

int UserDefinedExtensionSeen = 0;
char       *TENVExtension_str;
static int  TENVExtension_strlen;
static int  TENVExtension_buffer_size;

static void
strcat_on_TENVExtension( const char *str) {
    
    int new_len = TENVExtension_strlen + strlen(str);

    if (new_len > TENVExtension_buffer_size - 2) {
        TENVExtension_buffer_size = new_len*2;
        TENVExtension_str = realloc(TENVExtension_str, TENVExtension_buffer_size);
        if (TENVExtension_str == NULL) {
            error("INTERNAL ERROR: cannot allocate memory for %s", str);
            exit(-1);
        }
    }
    strcpy(&TENVExtension_str[TENVExtension_strlen], str);
    TENVExtension_strlen += strlen(str);
}

/******************************************************************************
 * connect_extensions is to be used once mcore is selected in order to choose
 * extensions according to core architecture.
 ******************************************************************************/


/* array contains list of extension with extlib disabled
 */
char* Mnoextlibs[MAX_EXTENSION_ENTRIES];
int MnoextlibsTabSize = 0;

void
connect_extensions ( void ) {
   char * ptr_ext, *ptr_nextext;
   int archi;
   int i,j;

   if (Mextension_str[0]==0) return;
   switch (proc) {
      case PROC_stxp70_v3:
        archi = STXP70_ARCH_V3;
        break;
      case PROC_stxp70_v4_novliw:
      case PROC_stxp70_v4_single:
      case PROC_stxp70_v4_dual:
        archi = STXP70_ARCH_V4;
        break;
   }
   ptr_ext = &Mextension_str[12]; /* starts with -Mextension= */
   while (NULL != ptr_ext) {
      int exthwtype_found;
      int nr_found;
      extension_implies_list_T *p;

      /* extensions are comma separated */
      ptr_nextext = strchr(ptr_ext,',');
      if (ptr_nextext) *ptr_nextext = 0;
      /* x3 is not concerned by all this stuff... */
      if (strcmp(ptr_ext,"x3")) {
         char * colon_ptr = strchr(ptr_ext,':');
         int    start, stop;

         if (NULL!=colon_ptr) {
            *colon_ptr++ = 0;
            if (!strcmp(colon_ptr,"novliw")) {
               start = STXP70_EXTHWTYPE_SINGLE;
            } else if (!strcmp(colon_ptr,"single")) {
               start = STXP70_EXTHWTYPE_SINGLE_DUAL;
            } else if (!strcmp(colon_ptr,"dual")) {
               start = STXP70_EXTHWTYPE_DUAL_DUAL;
            } else {
               error("INTERNAL ERROR: extension [%s] HW type [%s] not supported",ptr_ext,colon_ptr);
               exit(-1);
            }
            stop  = start+1;
         } else {
            start = 0;
            stop  = STXP70_EXTHWTYPE_SIZE;
         }
         exthwtype_found = -1;
         nr_found = 0;
         for (i=start;i<stop;i++) {
            for (j=0;j<static_ext_nr[archi][i];j++) {
                  if (!strcmp(ptr_ext,static_ext_opt[archi][i][j].name)) {
                     if (exthwtype_found==-1) {
                        exthwtype_found=i;
                     nr_found = j;
                  } else {
                     error("INTERNAL ERROR: %s extension definition is ambiguous (two or more extension hardware types exist)",ptr_ext);
                     exit(-1);
                  }
               }
            }
         }
         if (exthwtype_found==-1) {
            error("%s extension does not exist for %s architecture",ptr_ext,archi==STXP70_ARCH_V3?"stxp70v3":"stxp70v4");
            exit(-1);
         }
 
         /* [VL] In case we connect the FPx, but short-double option was not set,    */
         /* then we emit a warning to indicate that FPx will not be used efficiently */
         /* Notice that the case of legacy option -mfpx is also implictly covered by */
         /* this code!                                                               */
         if ((!strcmp(ptr_ext,"fpx") && (archi==STXP70_ARCH_V3))) {
            if (!option_was_seen(O_fshort_double)) {
               warning("Option -fshort-double should be set to benefit from FPx.");
            }
         }

         /* In case extension is not fpx for v3 architecture, add it to TENV */
         if (!(!strcmp(ptr_ext,"fpx") &&
               (archi==STXP70_ARCH_V3))
              ||
              ((NULL!=static_ext_opt[archi][exthwtype_found][nr_found].libname) &&
               (0!=static_ext_opt[archi][exthwtype_found][nr_found].libname[0]))) {
            UserDefinedExtensionSeen = 1;
            /* In case an extension is already set, add a comma */
            if (TENVExtension_strlen > TENVEXTENSION_BASE_STR_LEN) {
               strcat_on_TENVExtension(",");
            }
            if (static_ext_opt[archi][exthwtype_found][nr_found].libpath[0]!=0) {
               strcat_on_TENVExtension(static_ext_opt[archi][exthwtype_found][nr_found].libpath);
               if (TENVExtension_str[TENVExtension_strlen-1] != '/') {
                  strcat_on_TENVExtension("/");
               }
            }
            strcat_on_TENVExtension(static_ext_opt[archi][exthwtype_found][nr_found].libname);
         }
         p = static_ext_opt[archi][exthwtype_found][nr_found].implies;
         while (NULL!=p) {
           extern void create_implies_item (int key, int index, string name);

           if (p->info_index == O_L__) {
             int i;
             boolean noextlibsdetected = FALSE; 
             /* verify whether extlib has notbeen disabled */
             for (i=0;i<MnoextlibsTabSize; i++) {
              if (strcmp(ptr_ext, Mnoextlibs[i])==0) {
                noextlibsdetected=TRUE;
                break;
              }
             }
             if (!noextlibsdetected) {
               add_extension_library_dir(p->name);
             }
   	   } else {
              create_implies_item(O_Mextension__,p->info_index,p->name);
	   }
           p = p->next;
         }
         if (colon_ptr) *--colon_ptr = ':';
      }
      if (ptr_nextext) *ptr_nextext++ = ',';
      ptr_ext = ptr_nextext;
   }
   if (debug) {
      fprintf(stderr,"-Mextension new TENV is [%s]\n",TENVExtension_str);
   }
}

/******************************************************************************
 * parse_Mextension_option is used to manage -Mextension option. This switch is
 * used to connect extension to STxP70 core for code generation.
 ******************************************************************************
 * The available sub-switches are taken from a file named sxextensionrc using
 * extract_from_sxextensionrc function.
 * As a starting point, creation of a result from this file without parsing it.
 ******************************************************************************
 * Mextension_str string is used to pass information to phases.c file in order
 * to dispatch extension information to relevant sub-phases of driver.
 ******************************************************************************/
int ExtensionSeen = 0;
static int
parse_Mextension_option (char **argv, int *argi) {
//   extension_T * ext_opt;
//   int ExtNr;
   string ptr_subopt;
   string ptr_nextsubopt;
   int    res = O_Mextension__;
   ExtensionSeen = 1;


   extract_from_sxextensionrc(argv[0]);
   if (MextensionFirstPass) {

      /* first time build base of Mextension_str: "-Mextension=" */
      memset(Mextension_str,0,sizeof(Mextension_str)/sizeof(char));
      strcpy(Mextension_str,"-Mextension=");
      Mextensionext_str_p = &Mextension_str[12];
      /* first time build base of TENVExtension_str: "-TENV::extension=" */
      TENVExtension_buffer_size = 1024; /* Initial buffer size*/
      TENVExtension_str = (char*)malloc(TENVExtension_buffer_size);
      strcpy(TENVExtension_str,TENVEXTENSION_BASE_STR);
      TENVExtension_strlen = TENVEXTENSION_BASE_STR_LEN;
      if (TS_tree_structure < 0x410) { /* Starting at TS 4.1.0, include path in given in stxp70extrc file */
         string sxextinc_path;

         sxextinc_path = concat_path(get_sxext_basedir(argv[0]),"/include");
         if (SYS_is_dir(sxextinc_path)) {
            sxextinc_path = concat_strings("-I",sxextinc_path);
            create_implies_item(O_Mextension__,O_I__,sxextinc_path);
         }
      }
   } else {
      /* all other times, removed ,x3 at end of Mextension_str since
         it is required that x3 be the very last one */
      Mextensionext_str_p -= 3; Mextensionext_str_p[0]=0; Mextensionext_str_p[1]=0; Mextensionext_str_p[2]=0;
   }

   /* Go to first sub-option i.e. after -Mextension= */
   ptr_subopt = string_copy(next_string(argv,argi));
   ptr_nextsubopt = strchr(ptr_subopt,'=');
   if (NULL != ptr_nextsubopt) {
      ptr_subopt = ++ptr_nextsubopt;
      /* Assertion on Mextension_str length */
      assert(strlen(ptr_subopt)<=sizeof(Mextension_str)-(Mextensionext_str_p-Mextension_str)-4);
   } else {
      ptr_subopt = string_copy(next_string_after("-Mextension",argv,argi));
      get_next_arg(argi);
   }
   while (NULL != ptr_subopt) {
      int i, next_opt=0;

      /* sub-options are comma separated */
      ptr_nextsubopt = strchr(ptr_subopt,',');
      if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
      if ((!exist_extension(ptr_subopt)) && (strcmp(ptr_subopt,"x3"))) {
         res = O_Unrecognized;
         goto Mextension_end;
      }
      ptr_subopt = ptr_nextsubopt;
   }
Mextension_end:
   if ('=' != Mextensionext_str_p[-1]) {
      /* Add x3 at the very end of Mextension_str */
      MextensionFirstPass=0;
      strcat(Mextensionext_str_p,",x3");
      Mextensionext_str_p += 3;
      if (debug) {
         fprintf(stderr,"-Mextension new TENV is [%s]\n",TENVExtension_str);
      }
   } else {
      MextensionFirstPass = 1;
      Mextension_str[0]   = 0;
      TENVExtension_str[0]= 0;
   }
   get_next_arg(argi);
   return res;
} /* parse_Mextension_option */

/******************************************************************************
 * parse_Mnoextgen_option is used to manage -Mnoextgen option. This switch is
 * used to disable native code generation for specified STxP70 extensions
 * (All if none specifed) .
 ******************************************************************************
 * The corresponding compiler option is not created here as we want to check
 * that specified extensions have also be referenced in -Mextension flag.
 ******************************************************************************/
int MnoextgenSeen = 0;
string MnoextgenTab[MAX_EXTENSION_ENTRIES];
int MnoextgenTabSize = 0;

static int
parse_Mnoextgen_option (char **argv, int *argi) {
    string ptr_nextsubopt;
    string ptr_subopt;
    int    res = O_Mnoextgen__;

    /* If not first pass and table of extension name is empty,
       it means that native codegen is disabled for all extensions */
    if (!MnoextgenSeen || MnoextgenTabSize>0) {
        MnoextgenSeen = 1;

        /* Go to first sub-option i.e. after -Mnoextgen= */
        ptr_subopt = string_copy(next_string(argv,argi));
        ptr_subopt = strchr(ptr_subopt,'=');
        if (NULL == ptr_subopt) {
            /* Native codegen disabled for all extensions */
            MnoextgenTabSize = 0;
        }
        else {
            ptr_subopt++;
        }
        while (NULL != ptr_subopt) {
            int i, next_opt=0;

            /* sub-options are comma separated */
            ptr_nextsubopt = strchr(ptr_subopt,',');
            if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;

            for (i=0; i<MnoextgenTabSize; i++) {
                if (strcmp(MnoextgenTab[i], ptr_subopt)==0) {
                    break; /* Extension already specified */
                }
            }
            if (i == MnoextgenTabSize) {
                /* Register extension name */
                if (MnoextgenTabSize>=MAX_EXTENSION_ENTRIES) {
                    error("INTERNAL ERROR: maximum number of extensions reached for option -Mnoextgen");
                    exit(-1);
                }
                MnoextgenTab[MnoextgenTabSize++] = string_copy(ptr_subopt);
            }
            ptr_subopt = ptr_nextsubopt;
        }
    }
   get_next_arg(argi);
   return res;
} /* parse_Mnoextgen_option */

/******************************************************************************
 * parse_Mextoption_option is used to manage -Mextoption option. This switch is
 * used to add extension specific option to native code generatio
 * for specified STxP70 extensions
 ******************************************************************************
 * The corresponding compiler option is not created here as we want to check
 * that specified extensions have also be referenced in -Mextension flag.
 ******************************************************************************/
typedef struct { string ext; string opt; } extopt;
extopt Mextoption[MAX_EXTENSION_ENTRIES];
int MextoptionTabSize = 0;

static int
parse_Mextoption_option (char **argv, int *argi) {
    string ptr_nextsubopt;
    string opt;
    string ptr_subopt;
    int    res = O_Mextoption__;

    /* Go to first sub-option i.e. after -Mextoption= */
    opt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(opt,'=');
    if (NULL == ptr_subopt) {
      /* Native codegen disabled for all extensions */
      error("Wrong syntax : -Mextoption=ext:opt required instead of %s\n", opt);
      exit(-1);
    }
    else {
      ptr_subopt++;
    }

    if (MextoptionTabSize >=MAX_EXTENSION_ENTRIES) {
      /* Register extension name */
      error("INTERNAL ERROR: maximum number of extensions reached for option -Mextoption");
      exit(-1);
    }

    /* ext and option are separated by : */
    ptr_nextsubopt = strchr(ptr_subopt,':');
    if (!ptr_nextsubopt) {
      error("Wrong syntax : -Mextoption=ext:opt required instead of %s\n", opt);
      exit(-1);
    }

    *ptr_nextsubopt++ = 0;

    /* Disable extlib for extension EXT upon detection
     * -Mextoption=noexlibs:EXT
     */
    if (strcmp(ptr_nextsubopt, "noextlibs") == 0) {
      if (MnoextlibsTabSize >=MAX_EXTENSION_ENTRIES) {
        /* Register extension name */
        error("INTERNAL ERROR: maximum number of extensions reached for option -Mextoption=EXT:noextlibs");
        exit(-1);
      }
      
      Mnoextlibs[MnoextlibsTabSize]=string_copy(ptr_subopt);
      MnoextlibsTabSize++;
    } else {
      Mextoption[MextoptionTabSize].ext = string_copy(ptr_subopt);
      Mextoption[MextoptionTabSize].opt = string_copy(ptr_nextsubopt);
    
      MextoptionTabSize++;
    }

    get_next_arg(argi);
    return res;
} /* parse_Mextoption_option */

/******************************************************************************
 * parse_Mfractsupport_option is used to manage -Mfractsupport option
 * for backward compatibility.
 * New option is: -Mextoption=MP1x:enablefractgen
 ******************************************************************************/

static int
parse_Mfractsupport_option (char **argv, int *argi) {
    string ptr_nextsubopt;
    string ptr_subopt;
    int    res = O_Mfractsupport__;

    if (MextoptionTabSize >=MAX_EXTENSION_ENTRIES) {
      /* Register extension name */
      error("INTERNAL ERROR: maximum number of extensions reached for option -Mextoption");
      exit(-1);
    }

    Mextoption[MextoptionTabSize].ext = "MP1x";
    Mextoption[MextoptionTabSize].opt = "enablefractgen";
    
    MextoptionTabSize++;

    get_next_arg(argi);
    return res;
} /* parse_Mfractsupport_option */

static int
parse_Menablefractgen_option (char **argv, int *argi) {
    string ptr_nextsubopt;
    string ptr_subopt;
    int    res = O_Mfractsupport__;

    if (MextoptionTabSize >=MAX_EXTENSION_ENTRIES) {
      /* Register extension name */
      error("INTERNAL ERROR: maximum number of extensions reached for option -Mextoption");
      exit(-1);
    }

    Mextoption[MextoptionTabSize].ext = "MP1x";
    Mextoption[MextoptionTabSize].opt = "enablefractgen";
    
    MextoptionTabSize++;

    get_next_arg(argi);
    return res;
} /* parse_Menablefractgen_option */


 /****************************************************************************
  * parse_Mda_option is used to manage -Mda={1,2,4,8,all} option. This switch is
  * used to dispatch variable into GP based area.
  ****************************************************************************/
const int AllAlignment = 1|2|4|8; // 15
int da_mem = 0;
static int
parse_Mda_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Mda__;

    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    if (ptr_subopt == NULL)
      da_mem = AllAlignment;
    else
      ptr_subopt++;
    while (NULL != ptr_subopt) {
       /* sub-options are comma separated */
       ptr_nextsubopt = strchr(ptr_subopt,',');
       if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
       if ((ptr_subopt[0]=='1') && (ptr_subopt[1]==0)) {
          da_mem |= 1;
       } else if ((ptr_subopt[0]=='2') && (ptr_subopt[1]==0)) {
          da_mem |= 2;
       } else if ((ptr_subopt[0]=='4') && (ptr_subopt[1]==0)) {
          da_mem |= 4;
       } else if ((ptr_subopt[0]=='8') && (ptr_subopt[1]==0)) {
          da_mem |= 8;
       } else if ((ptr_subopt[0]=='a') && (ptr_subopt[1]=='l') &&
                  (ptr_subopt[2]=='l') && (ptr_subopt[3]==0)) {
         da_mem = AllAlignment;
       } else {
          res=0;
          goto Mda_end;
       }
       ptr_subopt = ptr_nextsubopt;
    }
 Mda_end:
    get_next_arg(argi);
    return res;
 }
 /****************************************************************************
  * parse_Msda_option is used to manage -Msda={1,2,4,8,all} option. This switch is
  * used to dispatch variable into GP based area.
  ****************************************************************************/
int sda_mem = 0;
static int
parse_Msda_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Msda__;

    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    if (ptr_subopt == NULL)
      sda_mem = AllAlignment;
    else
      ptr_subopt++;
    while (NULL != ptr_subopt) {
       /* sub-options are comma separated */
       ptr_nextsubopt = strchr(ptr_subopt,',');
       if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
       if ((ptr_subopt[0]=='1') && (ptr_subopt[1]==0)) {
          sda_mem |= 1;
       } else if ((ptr_subopt[0]=='2') && (ptr_subopt[1]==0)) {
          sda_mem |= 2;
       } else if ((ptr_subopt[0]=='4') && (ptr_subopt[1]==0)) {
          sda_mem |= 4;
       } else if ((ptr_subopt[0]=='8') && (ptr_subopt[1]==0)) {
          sda_mem |= 8;
       } else if ((ptr_subopt[0]=='a') && (ptr_subopt[1]=='l') &&
                  (ptr_subopt[2]=='l') && (ptr_subopt[3]==0)) {
         sda_mem = AllAlignment;
       } else {
          res=0;
          goto Msda_end;
       }
       ptr_subopt = ptr_nextsubopt;
    }
 Msda_end:
    get_next_arg(argi);
    return res;
 }
 /****************************************************************************
  * parse_Mtda_option is used to manage -Mtda={1,2,4,8,all} option. This switch is
  * used to dispatch variable into Tiny data area
  ****************************************************************************/
int tda_mem = 0;
static int
parse_Mtda_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Mtda__;

    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    if (ptr_subopt == NULL)
      tda_mem = AllAlignment;
    else
      ptr_subopt++;
    while (NULL != ptr_subopt) {
       /* sub-options are comma separated */
       ptr_nextsubopt = strchr(ptr_subopt,',');
       if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
       if ((ptr_subopt[0]=='1') && (ptr_subopt[1]==0)) {
          tda_mem |= 1;
       } else if ((ptr_subopt[0]=='2') && (ptr_subopt[1]==0)) {
          tda_mem |= 2;
       } else if ((ptr_subopt[0]=='4') && (ptr_subopt[1]==0)) {
          tda_mem |= 4;
       } else if ((ptr_subopt[0]=='8') && (ptr_subopt[1]==0)) {
          tda_mem |= 8;
       } else if ((ptr_subopt[0]=='a') && (ptr_subopt[1]=='l') &&
                  (ptr_subopt[2]=='l') && (ptr_subopt[3]==0)) {
         tda_mem = AllAlignment;
       } else {
          res=0;
          goto Mtda_end;
       }
       ptr_subopt = ptr_nextsubopt;
    }
 Mtda_end:
    get_next_arg(argi);
    return res;
 }

 /****************************************************************************
  * parse_Mdarange_option is used to manage -Mda[minSize],maxSize
  * option. This switch is
  * used to dispatch variable into GP based area.
  ****************************************************************************/
int da_minsize = 0;
int da_maxsize = 0;
static int
parse_Mdarange_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Mdarange__;
    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    ptr_subopt++;
   if (ptr_subopt == NULL)
      {
        warning("missing parameter for option -Mdarange\n");
      }
    else
      {
        if (strchr(ptr_subopt,',') != NULL)
          {
            char* x;
            da_minsize = strtol(ptr_subopt, &x, 10);
            da_maxsize = strtol(x+1, NULL, 10);
          }
        else
          {
            da_minsize = 0;
            da_maxsize = strtol(ptr_subopt, NULL, 10);
          }
      }
    get_next_arg(argi);
    return res;
}

 /****************************************************************************
  * parse_Msdarange_option is used to manage -Mssda[minSize],maxSize
  * option. This switch is
  * used to dispatch variable into GP based area.
  ****************************************************************************/
int sda_minsize = 0;
int sda_maxsize = 0;
static int
parse_Msdarange_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Msdarange__;
    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    ptr_subopt++;
   if (ptr_subopt == NULL)
      {
        warning("missing parameter for option -Msdarange\n");
      }
    else
      {
        if (strchr(ptr_subopt,',') != NULL)
          {
            char* x;
            sda_minsize = strtol(ptr_subopt, &x, 10);
            sda_maxsize = strtol(x+1, NULL, 10);
          }
        else
          {
            sda_minsize = 0;
            sda_maxsize = strtol(ptr_subopt, NULL, 10);
          }
      }
    get_next_arg(argi);
    return res;
}

/****************************************************************************
  * parse_Mtdarange_option is used to manage -Mtda[minSize],maxSize
  * option. This switch is
  * used to dispatch variable into GP based area.
  ****************************************************************************/
int tda_minsize = 0;
int tda_maxsize = 0;
static int
parse_Mtdarange_option (char **argv, int *argi) {
  string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Mtdarange__;
    /* Go to first sub-option i.e. after -Mconfig= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    ptr_subopt++;
   if (ptr_subopt == NULL)
      {
        warning("missing parameter for option -Mtdarange\n");
      }
    else
      {
        if (strchr(ptr_subopt,',') != NULL)
          {
            char* x;
            tda_minsize = strtol(ptr_subopt, &x, 10);
            tda_maxsize = strtol(x+1, NULL, 10);
          }
        else
          {
            tda_minsize = 0;
            tda_maxsize = strtol(ptr_subopt, NULL, 10);
          }
      }
    get_next_arg(argi);
    return res;
}

 /****************************************************************************
  * parse_Mgot_option is used to manage -Mgot={small,std,large} option. 
  * This switch is used for PIC code generation.
  ****************************************************************************/
static int
parse_Mgot_option (char **argv, int *argi) {
    string ptr_subopt;
    string ptr_nextsubopt;
    int    res = O_Mgot__;
                                                                                                                 
    /* Go to first sub-option i.e. after -Mgot= */
    ptr_subopt = string_copy(next_string(argv,argi));
    ptr_subopt = strchr(ptr_subopt,'=');
    if (ptr_subopt == NULL)
      got_model_opt = got_none;
    else
      ptr_subopt++;
    while (NULL != ptr_subopt) {
       /* sub-options are comma separated */
       ptr_nextsubopt = strchr(ptr_subopt,',');
       if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
       if ((ptr_subopt[0]=='s') && (ptr_subopt[1]=='m') &&
           (ptr_subopt[2]=='a') && (ptr_subopt[3]=='l') &&
           (ptr_subopt[4]=='l') && (ptr_subopt[5]==0)) {
         got_model_opt = got_small;
       } else if ((ptr_subopt[0]=='s') && (ptr_subopt[1]=='t') &&
           (ptr_subopt[2]=='d') && (ptr_subopt[3]==0)) {
         got_model_opt = got_standard;
       } else if ((ptr_subopt[0]=='l') && (ptr_subopt[1]=='a') &&
           (ptr_subopt[2]=='r') && (ptr_subopt[3]=='g') &&
           (ptr_subopt[4]=='e') && (ptr_subopt[5]==0)) {
         got_model_opt = got_large;
       } else {
          res=0;
          goto Mgot_end;
       }
       ptr_subopt = ptr_nextsubopt;
    }
 Mgot_end:
    get_next_arg(argi);
    return res;
 }

/******************************************************************************
 * parse_Mhwloop_option is used to manage -Mhwloop={all,none,jrgtudeconly,
 * hwlooponly} option. This switch is used to toggle HW loop code generation.
 *****************************************************************************/
enum { hwloop_default = -1, hwloop_none, hwlooponly, jrgtudeconly, hwloop_all}  hwloop_mapping = hwloop_default;
static int
parse_Mhwloop_option (char **argv, int *argi) {
   string ptr_subopt;
   string ptr_nextsubopt;
   int    res = O_Mhwloop__;

   /* Go to first sub-option i.e. after -Mconfig= */
   ptr_subopt = string_copy(next_string(argv,argi));
   ptr_subopt = strchr(ptr_subopt,'=');
   if (NULL != ptr_subopt) ptr_subopt++;
   while (NULL != ptr_subopt) {
      /* sub-options are comma separated */
      ptr_nextsubopt = strchr(ptr_subopt,',');
      if (ptr_nextsubopt) *ptr_nextsubopt++ = 0;
      if (!strcmp(ptr_subopt,"all")) {
         hwloop_mapping = hwloop_all;
      } else if (!strcmp(ptr_subopt,"jrgtudeconly")) {
         hwloop_mapping = jrgtudeconly;
      } else if (!strcmp(ptr_subopt,"hwlooponly")) {
         hwloop_mapping = hwlooponly;
      } else if (!strcmp(ptr_subopt,"none")) {
         hwloop_mapping = hwloop_none;
      } else {
         res=0;
         goto Mhwloop_end;
      }
      ptr_subopt = ptr_nextsubopt;
   }
Mhwloop_end:
   get_next_arg(argi);
   return res;
}


/******************************************************************************
 * parse_V_option is used to manage -V[<ver>] option. This switch is used either
 * to display the version number is used alone, or to switch the compiler
 * version when <ver> is specified.
 ******************************************************************************/
static int
parse_V_option (char **argv, int *argi) {
   extern boolean show_version;
   extern boolean execute_flag;

   if (!strcmp(argv[*argi],"-V")) {
      show_version = TRUE; execute_flag = FALSE;
      get_next_arg(argi);
      return O_V__;
   } else {
      char *version;
      char *alt_driver;

      show_version = TRUE; execute_flag = TRUE;
      version = argv[*argi]+2;
      alt_driver = string_copy(SYS_full_path(argv[0]));
      alt_driver = concat_strings(alt_driver,"/../../");
      alt_driver = concat_strings(alt_driver,version);
      alt_driver = concat_strings(alt_driver,"/bin/stxp70cc");
      if (file_exists(alt_driver)) {
         int i1,i2;

         /* changed program pat and remove -V<ver> from saved_argv */
         saved_argv[0] = alt_driver;
         for (i1=i2=0;i1<saved_argc;i1++,i2++) {
            if (!strcmp(saved_argv[i1],argv[*argi])) {
               i2++;
            }
            saved_argv[i1] = saved_argv[i2];
         }
         if (debug) {
            printf("Switching compiler version to %s\n",version);
         }
         execv(alt_driver,(const char * const *)saved_argv);
         error("cannot exec");
         exit(0);
      } else {
         error("compiler version %s does not exist",version);
         exit(-1);
      }
      get_next_arg(argi);
      return O_V__;
   }
}

/******************************************************************************
 * parse_mfpx_option is used to manage -mfpx option. This switch is used to
 * mimic -Mextension=fpx and for former compatibility.
 ******************************************************************************/
static int
parse_mfpx_option (char **argv, int *argi) {
   int    res = O_Mextension__;
   int i;

   extract_from_sxextensionrc(argv[0]);
   if (MextensionFirstPass) {
      /* first time build base of Mextension_str: "-Mextension=" */
      memset(Mextension_str,0,sizeof(Mextension_str)/sizeof(char));
      strcpy(Mextension_str,"-Mextension=");
      Mextensionext_str_p = &Mextension_str[12];
   } else {
      /* all other times, removed ,x3 at end of Mextension_str since
         it is required that x3 be the very last one */
      Mextensionext_str_p -= 3; Mextensionext_str_p[0]=0; Mextensionext_str_p[1]=0; Mextensionext_str_p[2]=0;
   }

   /* Assertion on Mextension_str length */
   assert(strlen("fpx")<=sizeof(Mextension_str)-(Mextensionext_str_p-Mextension_str)-4);

   if (!exist_extension("fpx")) {
      res = O_Unrecognized;
   }

   /* Add x3 at the very end of Mextension_str */
   MextensionFirstPass=0;
   get_next_arg(argi);
   strcat(Mextensionext_str_p,",x3");
   Mextensionext_str_p += 3;
   return res;
}


enum {EXTOPT_none = 0LL,
      EXTOPT_mult=1LL<<32,
      EXTOPT_nomult=1LL<<33,
      EXTOPT_hwloop=1LL<<34,
      EXTOPT_nohwloop=1LL<<35,
      EXTOPT_mode32=1LL<<36,
      EXTOPT_mode16=1LL<<37,
      EXTOPT_shortdouble = 1LL<<38,
      EXTOPT_noshortdouble = 1LL<<39,
      EXTOPT_dualcoreALU=1LL<<40,
      EXTOPT_singlecoreALU=1LL<<41,
      EXTOPT_noextlibs=1LL<<62,
      EXTOPT_disabled=1LL<<63
};

long long build_option_flags() {
  // generic option mask
  long long flags = 0 ;
  extern int lib_short_double;
  if (lib_short_double == TRUE) {
    flags|=EXTOPT_shortdouble;
  } else {
    flags|=EXTOPT_noshortdouble;
  }
  
  if (STxP70mult == TRUE) {
    flags|=EXTOPT_mult;
  } else {
    flags|=EXTOPT_nomult;
  }
  
  extern boolean flag_HWLOOP;  /* in phases.c */
  if (flag_HWLOOP &&
      ( hwloop_mapping==hwlooponly ||
        hwloop_mapping==hwloop_all)) {
    flags|=EXTOPT_hwloop;
  } else {
    flags|=EXTOPT_nohwloop;
  }

  if (lib_kind==LIB_STXP70_16 ||
      corecfg & 4) {
    flags|=EXTOPT_mode16;
  } else {
    flags|=EXTOPT_mode32;
  }

  if (corecfg1& (1<<10)) {
    flags|=EXTOPT_singlecoreALU;
  }
  if (corecfg1& (3<<10)) {
    flags|=EXTOPT_dualcoreALU;
      }  
  return flags;
}

