#
#
#  Copyright (C) 2000, 2001 Silicon Graphics, Inc.  All Rights Reserved.
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of version 2 of the GNU General Public License as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it would be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
#
#  Further, this software is distributed without any warranty that it is
#  free of the rightful claim of any third person regarding infringement 
#  or the like.  Any license provided herein, whether implied or 
#  otherwise, applies only to this software file.  Patent licenses, if 
#  any, provided herein do not apply to combinations of this program with 
#  other software, or any other product whatsoever.  
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write the Free Software Foundation, Inc., 59
#  Temple Place - Suite 330, Boston MA 02111-1307, USA.
#
#  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
#  Mountain View, CA 94043, or:
#
#  http://www.sgi.com
#
#  For further information regarding this notice, see:
#
#  http://oss.sgi.com/projects/GenInfo/NoticeExplan
#
#

#
#  Makefile.base for gcc-based driver
#

include	$(COMMONDEFS)
 COMMON_DIR 		= $(BUILD_TOT)/common
 COMMON_APPLICONFIG_DIR 		= $(COMMON_DIR)/appliconfig

GNU_COMMON_DIR          = $(BUILD_TOT)/gnu_common

ifeq ($(HOST_OS),MINGW)
# For GetProcessMemoryInfo function
LLDLIBS += -lpsapi
endif

# setup stuff to build shared or non-shared
LCOPTS = $(STD_COMPILE_OPTS)
LLDOPTS = $(STD_LOAD_OPTS)

# Defined DRIVER when building the driver (not the tools)
LCDEFS += -DDRIVER


ifeq ($(BUILD_TARGET), IA64)
LCDEFS += -DIA64
endif
ifeq ($(BUILD_TARGET), IA32)
LCDEFS += -DIA32
endif
ifeq ($(BUILD_TARGET), MIPS)
LCDEFS += -DMIPS
endif
ifeq ($(BUILD_TARGET), ST100)
LCDEFS += -DST100
endif
ifeq ($(BUILD_TARGET), ST200)
LCDEFS += -DST200 $(STD_COMPILE_OPTS) -DGNU_FRONT_END=33
  ifeq ($(BUILD_TARGET_OS), BARE_OS21)
    LCDEFS+= -DTARGET_OS21 -DMUMBLE_ST200_BSP 
  endif
  ifeq ($(BUILD_TARGET_OS), LINUX)
    LCDEFS+= -DTARGET_LINUX
  endif
  BUILD_LCFLAGS += -DGNU_FRONT_END=33
endif
ifeq ($(BUILD_TARGET), STxP70)
LCDEFS += -DSTxP70 $(STD_COMPILE_OPTS) -DMUMBLE_STxP70_BSP -DGNU_FRONT_END=33
endif
ifeq ($(BUILD_TARGET), ARM)
LCDEFS += -DARM $(STD_COMPILE_OPTS) -DMUMBLE_ARM_BSP -DGNU_FRONT_END=33
endif

ifeq ($(BUILD_HOST), IA32)
 ifeq ($(BUILD_VARIANT), NUE)
 # NUE is special environment built on ia32 but looks like ia64
 LCDEFS += -DLITTLE_ENDIAN_HOST -DHOST_IA64
 else
 LCDEFS += -DLITTLE_ENDIAN_HOST -DHOST_IA32
 endif
endif
ifeq ($(BUILD_HOST), IA64)
LCDEFS += -DLITTLE_ENDIAN_HOST -DHOST_IA64
endif
ifeq ($(BUILD_HOST), MIPS)
LCDEFS += -DHOST_MIPS
endif

RUN =

ifeq ($(BUILD_ARCH), IA64)
  ifeq ($(BUILD_HOSTARCH), IA32)
    RUN = medusa -batch
  endif
endif

ifeq ($(BUILD_VARIANT), NOOPT)
# special driver for no optimization
LCDEFS += -DNOOPT
endif
ifeq ($(BUILD_VARIANT), NUE)
# special driver for NUE cross environment
LCDEFS += -DNUE
endif
ifeq ($(BUILD_VARIANT), LICENSING)
# special driver for no optimization
LCDEFS += -DCOMPILER_LICENSING 
LDLIBS += -llmsgi
endif

# don't want to use common/com/defs.h
LCINCS	= -I. -I$(BUILD_BASE) -I$(BUILD_TOT)/common/rcparser -I$(BUILD_TOT)/common/rtkutils -I$(GNU_COMMON_DIR)/include -I$(BUILD_TOT)/common/appliconfig

TARGETS	= driver$(HEXE)

# generated header files (including .i files)
GEN_HFILES = \
	option_names.h \
	init_options.i \
	get_option.i \
	opt_action.i

APPCG_HFILES = \
	appli_config_common.h \
	appli_config_yacc.h

SHARED_HFILES = \
	basic.h \
	errors.h \
	lang_defs.h \
	string_utils.h

SRC_HFILES = \
	config_platform.h \
	options.h \
	option_seen.h \
	opt_actions.h \
	get_options.h \
	file_utils.h \
	file_names.h \
	objects.h \
	phases.h \
	run.h

HFILES= \
	$(SHARED_HFILES) \
	$(APPCG_HFILES) \
	$(SRC_HFILES) \
	$(GEN_HFILES)

GEN_CFILES = \
	check_combos.c \
	implicits.c

APPCG_CFILES = \
	appli_config_yacc.c \
	appli_config_lex.c
	
SHARED_CFILES = \
	errors.c \
	lang_defs.c \
	string_utils.c

SRC_CFILES = \
	main.c \
	options.c \
	option_seen.c \
	config_platform.c \
	get_options.c \
	opt_actions.c \
	file_utils.c \
	file_names.c \
	objects.c \
	phases.c \
	run.c \
	special_options.c

CFILES=	\
	$(SHARED_CFILES) \
	$(GEN_CFILES) \
	$(APPCG_CFILES) \
	$(SRC_CFILES)

OPTIONS_SRC = \
	OPTIONS \
	sort_options.csh \
	sort_options.awk

SRC_OBJECTS = $(SRC_CFILES:.c=.o)
SHARED_OBJECTS = $(SHARED_CFILES:.c=.bo)

LDIRT = OPTIONS.P table$(BEXE) generated $(GEN_HFILES) $(GEN_CFILES) *.bo tmp.*

LANGS = as cc c89 CC f77 fort77 f90

appli_config_yacc.h appli_config_yacc.c : $(COMMON_APPLICONFIG_DIR)/appli_config.yacc 
	$(YACC_NO) -d $(COMMON_APPLICONFIG_DIR)/appli_config.yacc -o appli_config_yacc.c

appli_config_lex.c : $(COMMON_APPLICONFIG_DIR)/appli_config.lex appli_config_yacc.h
	$(LEX) -oappli_config_lex.c  $(COMMON_APPLICONFIG_DIR)/appli_config.lex

default: first
	$(MAKE) local last

first:
ifeq ($(BUILD_OS), LINUX)
	cd $(BUILD_AREA)/include && $(MAKE)
endif

first_local:

local: first_local
	$(MAKE) $(TARGETS)

last: local
	$(MAKE) make_deps

install: last
	if [ ! -d $(STD_MONGOOSE_OS_LOC) ]; then $(STD_INSTALL) -d $(STD_MONGOOSE_OS_LOC); fi
	$(STD_INSTALL) $(STD_INSTALL_EXEC_MASK) driver$(HEXE) $(STD_MONGOOSE_OS_LOC)
#	rm -f $(STD_MONGOOSE_OS_LOC)/sgicc  $(STD_MONGOOSE_OS_LOC)/sgiCC
#	rm -f $(STD_MONGOOSE_OS_LOC)/sgif90
#	ln -s $(STD_MONGOOSE_OS_LOC)/driver $(STD_MONGOOSE_OS_LOC)/sgicc
#	ln -s $(STD_MONGOOSE_OS_LOC)/driver $(STD_MONGOOSE_OS_LOC)/sgiCC
#	ln -s $(STD_MONGOOSE_OS_LOC)/driver $(STD_MONGOOSE_OS_LOC)/sgif90


include $(COMMONRULES)

VPATH = $(BUILD_BASE)

$(GEN_CFILES) $(GEN_HFILES) : generated
	@:

generated : OPTIONS.P table$(BEXE)
	$(RUN) ./table$(BEXE) < OPTIONS.P
	touch generated

%.bo : %.c
	$(BUILD_CC) $(BUILD_CFLAGS) $< -c -o $@

table$(BEXE) : table.bo $(SHARED_OBJECTS)
	$(BUILD_CC) -o $@ $^ $(BUILD_LDFLAGS)

# invoke shell script in source tree, but put output here
CPPCMD="$(BUILD_COMPILER_LOC)cpp -traditional -P"

OPTIONS.P : $(OPTIONS_SRC)
	$(BUILD_BASE)/sort_options.sh $(CPPCMD) $(BUILD_BASE) OPTIONS $(LCDEFS) > OPTIONS.P

DRIVERDEPLIBS += $(BUILD_AREA)/libSYS/libSYS.a $(BUILD_AREA)/libiberty/libiberty.a $(BUILD_AREA)/librcparser/librcparser.a $(BUILD_AREA)/librtkutils/librtkutils.a 

driver$(HEXE): $(OBJECTS)  $(DRIVERDEPLIBS)
	$(CCF) -o $@ $(OBJECTS) $(DRIVERDEPLIBS) $(LDFLAGS)

# need to make generated headers before compiling src files
$(SRC_OBJECTS) : $(GEN_HFILES)

