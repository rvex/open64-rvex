#
#
#  Copyright (C) 2000, 2001 Silicon Graphics, Inc.  All Rights Reserved.
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of version 2 of the GNU General Public License as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it would be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
#
#  Further, this software is distributed without any warranty that it is
#  free of the rightful claim of any third person regarding infringement 
#  or the like.  Any license provided herein, whether implied or 
#  otherwise, applies only to this software file.  Patent licenses, if 
#  any, provided herein do not apply to combinations of this program with 
#  other software, or any other product whatsoever.  
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write the Free Software Foundation, Inc., 59
#  Temple Place - Suite 330, Boston MA 02111-1307, USA.
#
#  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
#  Mountain View, CA 94043, or:
#
#  http://www.sgi.com
#
#  For further information regarding this notice, see:
#
#  http://oss.sgi.com/projects/GenInfo/NoticeExplan
#
#

#
# TARG_ST
# 
# [CG]:
# Added support for visibility specification files
# files: vspec_*.[ch]
#

#
#  Makefile.base for libcomutil
#

include	$(COMMONDEFS)

ifeq ($(DSO_MODE),DLL)
HOSTDEFS += -DBE_EXPORTED= -DCG_EXPORTED= -DDLLIMPORT=$(DLLIMPORT)
endif


ifeq ($(BUILD_OPTIMIZE), DEBUG)
HOSTDEFS += -DIs_True_On
HOSTDEFS += -DInsist_On 
endif

ifeq ($(BUILD_COMPILER), EDG)
CVERSION = -xansi
WOFF = -fullwarn
else
CVERSION =
WOFF =
endif

LASOPTS = $(STD_COMPILE_OPTS)

LCOPTS = $(STD_COMPILE_OPTS)
LCDEFS += $(HOSTDEFS) $(TARGDEFS)
LCXXOPTS = $(STD_COMPILE_OPTS)
LCXXDEFS += $(HOSTDEFS) $(TARGDEFS)

LCINCS = -I$(BUILD_BASE)
LCXXINCS = -I$(BUILD_BASE)
BUILD_CFLAGS += -I$(BUILD_BASE)

LIBRARY = librtkutils.a
# Specific LINUX platform version
BLIBRARY = librtkutils.ba

TARGETS = $(LIBRARY) $(BLIBRARY)

# only build mips3 version and install in default place
ifeq ($(BUILD_TARGET), MIPS)
STD_LIBRARY_LOC=$(DEF_LIBRARY_LOC)
endif

CFILES	= \
	utils_memory.c \
	utils_print.c \
	utils_debug.c \
	utils_option.c \
	utils_string.c \
	utils_version_id.c \
	libUtils.c \
	libUtilsOptions.c \
	libUtilsVersion.c

HFILES	= \
	utils_message.h \
	utils_memory.h \
	utils_print.h \
	utils_debug.h \
	utils_option.h \
	utils_string.h \
	utils_version_id.h \
	libUtils.h \
	libUtilsOptions.h \
	libUtilsVersion.h

CXXFILES = 

ifeq ($(HOST_OS),MINGW)
BOBJECTS = $(OBJECTS:.o=.bo)
else
BOBJECTS = $(OBJECTS)
endif

ifeq ($(RELEASE_MAJOR),)
RELEASE_MAJOR=0
endif
ifeq ($(RELEASE_MINOR),)
RELEASE_MINOR=0
endif
ifeq ($(RELEASE_PATCHLEVEL),)
RELEASE_PATCHLEVEL=0
endif

ifeq ($(BUILD_VARIANT),RELEASE)
CFLAGS += -D__RELEASE__ -DNDEBUG -DV_MAJ=$(RELEASE_MAJOR) -DV_MIN=$(RELEASE_MINOR) -DV_B=$(RELEASE_PATCHLEVEL)
else
CFLAGS += -DV_MAJ=$(RELEASE_MAJOR) -DV_MIN=$(RELEASE_MINOR) -DV_B=$(RELEASE_PATCHLEVEL)
endif

LDIRT += *.bo

default: first
	$(MAKE) local last

include $(COMMONRULES)

VPATH =  $(BUILD_BASE) $(BUILD_BASE)/$(BUILD_TARGET_DIR)

first:

first_local:

local: first_local
	$(MAKE) $(TARGETS)

last: local 
	$(MAKE) make_libdeps

exports: default
#	$(INSTALL) -m 444 -F $(STD_LIBRARY_LOC) $(TARGETS)

install: last
#	$(STD_INSTALL) -m 444 -F $(STD_LIBRARY_LOC) $(TARGETS)

.PRECIOUS : $(LIBRARY) $(BLIBRARY)

$(LIBRARY): $(OBJECTS)
	$(AR) $(ARFLAGS) $@ $^

%.bo: %.c
	$(BUILD_CC) $(BUILD_CFLAGS) -c $< -o $@

$(BLIBRARY): $(BOBJECTS)
	$(AR) $(ARFLAGS) $@ $(BOBJECTS)
