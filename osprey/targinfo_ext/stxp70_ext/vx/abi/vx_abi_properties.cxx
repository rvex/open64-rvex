

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "abi_properties_gen.h"
#include "dyn_isa_registers.h"
#include "gen_util.h"


int 
main(int argc, char *argv[])
{
  // Name of extension is mandatory
  Set_Dynamic("vx" /* extension name */);

  ABI_Properties_Begin("stxp70");

  // Definition of abi properties.
  ABI_PROPERTY allocatable = Create_Reg_Property("allocatable");
  ABI_PROPERTY callee      = Create_Reg_Property("callee");
  ABI_PROPERTY caller      = Create_Reg_Property("caller");


  // Register file ABI status
  Begin_ABI_dynamic("vx");

  Reg_Property(allocatable,
               ISA_REGISTER_CLASS_vx_vr,
               0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
               0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
               -1  );

  Reg_Property(caller,
               ISA_REGISTER_CLASS_vx_vr,
               0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
               0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
               -1  );

  // Specifing extension port.
  EXT_DESCRIPTION vx = DW_DEBUG_Extension("vx",   /* extension name    */
                                          "__DWR4"/* relocation string */);

  DW_DEBUG_Extension_Reg(ISA_REGISTER_CLASS_vx_vr,
                          vx,
                          0 /* compilation unit dwarf base id */);

  

  ABI_Properties_End();
  exit(0);
}

