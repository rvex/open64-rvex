
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "si_gen.h"
#include "targ_isa_subset.h"
#include "dyn_isa_topcode.h"
#include "gen_util.h"

/* Currently made resource description optional.
   Uncomment the defines below to activate
   the correxponding resource description. */
#define RES_RP1 1	/* Register port 1 access (load/mov conflicts). */
#define RES_EX2 1	/* Ex2 stage (throughput conflict with 1/2 and 1 throughput instructions). */

int main(int argc, char *argv[])
{
   Set_Dynamic("vx" /* extension name */);

   /* Core resources      */
   RESOURCE Resource_stxp70_ALL =
   RESOURCE_Create("Resource_stxp70_ALL", 1);

   /* Extension resources
    * For a pipelined extension, we could
    * have to define more resources.
    * Must be defined also in the CORE.
    */
#ifdef RES_RP1
   RESOURCE Resource_stxp70_FPX_RP1 = RESOURCE_Create("Resource_vx_stxp70_FPX_RP1", 1);
#endif
#ifdef RES_EX2
   RESOURCE Resource_stxp70_FPX_EX2 = RESOURCE_Create("Resource_vx_stxp70_FPX_EX2", 1);
#endif
     
   Machine(ISA_SUBSET_stxp70_v3);

   /*
    * For this Vx test case, we use the
    * following timings for pipe stages
    * (deduced from "VX microarch" document march 2006,  table 1):
    * 0: dec (core decode)
    * 1,2,3: resp. ex1, mex2, mex3 for load instructions
    * 1,2,3: resp. ex1, ex2, ex3 for instructions throughput 1
    * 1,2,3,4,5: resp. ex1, p2ex1/ex2, ex2, ex3, ex4 for instructions throughput 1/2
    * With the following general assumptions that must match also the core description (proc_si.cxx):
    * - default core GPR register fetch: dec
    * - default core GR register fetch: ex1
    * - default extension register fetch: ex1
    * - (latency,throughput) == 1  ,1-2 -> result avail in 2(ex2) to 3(ex3)
    * - (latency,throughput) == 1/2,3-4 -> result avail in 4(ex3) to 5(ex4)
    */
   
   /* 
    * The useful resources are modelized as the following.
    * First we describe the following operation attributes
    * from the microarch document:
    * T1 : on operations described as "thoughput 1"
    * T2 : on operations described as "thoughput 1/2"
    * LD : on load operations only
    * MV : on all moves / load / store operations
    * M2X: move to eXtension only
    *
    * Resources are:
    ** RES_fpx_EX2, instruction conflict on ex2.
    * EX2 uses are:
    * - thoughtput == 1 (att. T1) => uses EX2 at time 2
    * - thoughtput == 1/2 (att. T2)=> uses EX2 at time 2 and 3
    * - all move/store/load (att. MV) => no use of EX2
    * Which means for instance in term of stalls:
    * - T1|MV; T1|T2|MV => 0 stall,
    * - T2; T1|T2 => 1 stall,
    * - T2; MV => 0 stall
    *
    ** RES_fpx_RP1, register port 1 conflict
    *  RP1 must be described for:
    *  - load (att. LD)=> uses RP1 at time 3 (mex3)
    *  - move to eXtension (att. M2X) => uses RP1 at time 1 (p1ex1)
    */
   
   /* Defining instruction group     */
   /* We join instructions with the 
    * same number of operands and
    * and the same scheduling date
    * for all required resources
    * in a same group.
    */

   /*=================================*/
   Instruction_Group("vx_group0",
		     TOP_dyn_vx_absd,
		     TOP_dyn_vx_absdhm1,
		     TOP_dyn_vx_absdhp1,
		     TOP_dyn_vx_clp63,
		     TOP_dyn_vx_cplsb,
		     TOP_dyn_vx_cplsbi,
		     TOP_dyn_vx_cpmsb,
		     TOP_dyn_vx_cpmsbi,
		     TOP_dyn_vx_max,
		     TOP_dyn_vx_maxpair,
		     TOP_dyn_vx_mean,
		     TOP_dyn_vx_meanr,
		     TOP_dyn_vx_min,
		     TOP_dyn_vx_select,
		     TOP_dyn_vx_sub128,
		     -1);

   Any_Result_Available_Time(2); //VxR
   Any_Operand_Access_Time (1); //GR, VxR [, VxR]
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group1",
		     TOP_dyn_vx_absdslt,
		     TOP_dyn_vx_MULTI_absdslt,
		     TOP_dyn_vx_adds16,
		     TOP_dyn_vx_MULTI_adds16,
		     TOP_dyn_vx_adds16shr,
		     TOP_dyn_vx_MULTI_adds16shr,
		     TOP_dyn_vx_adds16shrr,
		     TOP_dyn_vx_MULTI_adds16shrr,
		     TOP_dyn_vx_addu16m1,
		     TOP_dyn_vx_MULTI_addu16m1,
		     TOP_dyn_vx_addu16m2,
		     TOP_dyn_vx_MULTI_addu16m2,
		     TOP_dyn_vx_addu16shr6,
		     TOP_dyn_vx_addvu8u8,
		     TOP_dyn_vx_MULTI_addvu8u8,
		     TOP_dyn_vx_absdmax,
		     TOP_dyn_vx_absdmin,
		     TOP_dyn_vx_MULTI_absdmax,
		     TOP_dyn_vx_MULTI_absdmin,
		     TOP_dyn_vx_bshr,
		     TOP_dyn_vx_cntdelta0clr,
		     TOP_dyn_vx_MULTI_cntdelta0clr,
		     TOP_dyn_vx_cntdeltabw,
		     TOP_dyn_vx_MULTI_cntdeltabw,
		     TOP_dyn_vx_cntdeltafw,
		     TOP_dyn_vx_MULTI_cntdeltafw,
                     TOP_dyn_vx_fir3edge,
                     TOP_dyn_vx_MULTI_fir3edge,
		     TOP_dyn_vx_firu8s8p,
		     TOP_dyn_vx_MULTI_firu8s8p,
		     TOP_dyn_vx_firu8s8p1clr,
		     TOP_dyn_vx_MULTI_firu8s8p1clr,
		     TOP_dyn_vx_getsad0,
		     TOP_dyn_vx_MULTI_getsad0,
		     TOP_dyn_vx_maxh3,
		     TOP_dyn_vx_MULTI_maxh3,
		     TOP_dyn_vx_maviu8s8,
		     TOP_dyn_vx_MULTI_maviu8s8,
		     TOP_dyn_vx_maviu8s8shr7,
		     TOP_dyn_vx_MULTI_maviu8s8shr7,
		     TOP_dyn_vx_maviu8s8shr7r,
		     TOP_dyn_vx_MULTI_maviu8s8shr7r,
		     TOP_dyn_vx_maviu8u8,
		     TOP_dyn_vx_MULTI_maviu8u8,
		     TOP_dyn_vx_maviu8u8shr7,
		     TOP_dyn_vx_MULTI_maviu8u8shr7,
		     TOP_dyn_vx_maviu8u8shr7r,
		     TOP_dyn_vx_MULTI_maviu8u8shr7r,
		     TOP_dyn_vx_mavu8s8,
		     TOP_dyn_vx_MULTI_mavu8s8,
		     TOP_dyn_vx_mavu8s8shr7,
		     TOP_dyn_vx_MULTI_mavu8s8shr7,
		     TOP_dyn_vx_mavu8s8shr7r,
		     TOP_dyn_vx_MULTI_mavu8s8shr7r,
		     TOP_dyn_vx_mavu8u8,
		     TOP_dyn_vx_MULTI_mavu8u8,
		     TOP_dyn_vx_mavu8u8shr7,
		     TOP_dyn_vx_MULTI_mavu8u8shr7,
		     TOP_dyn_vx_mavu8u8shr7r,
		     TOP_dyn_vx_MULTI_mavu8u8shr7r,
		     TOP_dyn_vx_median,
		     TOP_dyn_vx_MULTI_median,
		     TOP_dyn_vx_minsad,
		     TOP_dyn_vx_MULTI_minsad,
		     TOP_dyn_vx_mpviu8s8,
		     TOP_dyn_vx_MULTI_mpviu8s8,
		     TOP_dyn_vx_mpviu8u8,
		     TOP_dyn_vx_MULTI_mpviu8u8,
		     TOP_dyn_vx_mpvu8s8,
		     TOP_dyn_vx_MULTI_mpvu8s8,
		     TOP_dyn_vx_mpvu8u8,
		     TOP_dyn_vx_MULTI_mpvu8u8,
                     TOP_dyn_vx_random,
		     TOP_dyn_vx_shrs16s8,
		     TOP_dyn_vx_MULTI_shrs16s8,
		     TOP_dyn_vx_shrs16s8r,
		     TOP_dyn_vx_MULTI_shrs16s8r,
		     TOP_dyn_vx_shr7s16s8rc,
		     TOP_dyn_vx_MULTI_shr7s16s8rc,
		     TOP_dyn_vx_shr7s16s8rs,
		     TOP_dyn_vx_MULTI_shr7s16s8rs,
		     TOP_dyn_vx_subs16,
		     TOP_dyn_vx_MULTI_subs16,
		     TOP_dyn_vx_subs16shr,
		     TOP_dyn_vx_MULTI_subs16shr,
		     TOP_dyn_vx_subs16shrr,
		     TOP_dyn_vx_MULTI_subs16shrr,
		     -1);

   Any_Result_Available_Time(3);  //VxR [, VxR]
   Any_Operand_Access_Time (1); //GR, VxR, [, VxR] [, VxR] [, VxR]
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group2",
		     TOP_dyn_vx_mpu8u8shr,
                     TOP_dyn_vx_scales8s9,
                     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VXR
   Operand_Access_Time (2, 0); //GPR
   Operand_Access_Time (3, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group3",
                     TOP_dyn_vx_ld_i5_post_inc,
                     TOP_dyn_vx_ld_i5_post_dec,
                     TOP_dyn_vx_ld_i5_pre_dec,
                     -1);

   Result_Available_Time(0, 3); //VxR
   Result_Available_Time(1, 1); //GPR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1,3);
#endif

   /*=================================*/
   Instruction_Group("vx_group3b",
                     TOP_dyn_vx_ld_r_post_inc,
                     -1);

   Result_Available_Time(0, 3); //VxR
   Result_Available_Time(1, 1); //GPR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(2, 0); // GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1,3);
#endif
   /*=================================*/
   Instruction_Group("vx_group4",
                     TOP_dyn_vx_sd_i5_post_inc,
                     TOP_dyn_vx_sd_i5_post_dec,
                     TOP_dyn_vx_sd_i5_pre_dec,
                     -1);

   Result_Available_Time(0, 1); //GPR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(3, 1); // VxR
   Resource_Requirement(Resource_stxp70_ALL, 0); //STORE

   /*=================================*/
   Instruction_Group("vx_group4b",
                     TOP_dyn_vx_sd_r_post_inc,
                     -1);

   Result_Available_Time(0, 1); //GPR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(2, 0); // GPR
   Operand_Access_Time(3, 1); // VxR
   Resource_Requirement(Resource_stxp70_ALL, 0); //STORE

   /*=================================*/
   Instruction_Group("vx_group5",
                     TOP_dyn_vx_ld_i12_inc,
                     TOP_dyn_vx_ld_i8_inc,
                     TOP_dyn_vx_ld_i8_dec,
                     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
   Resource_Requirement(Resource_stxp70_FPX_RP1,3);

   /*=================================*/
   Instruction_Group("vx_group5b",
                     TOP_dyn_vx_ld_r_inc,
                     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(2, 0); // GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1,3);
#endif

   /*=================================*/
   Instruction_Group("vx_group6",
                     TOP_dyn_vx_sd_i12_inc,
                     TOP_dyn_vx_sd_i8_inc,
                     TOP_dyn_vx_sd_i8_dec,
                     -1);

   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(3, 1); // VxR
   Resource_Requirement(Resource_stxp70_ALL, 0);

   /*=================================*/
   Instruction_Group("vx_group6b",
                     TOP_dyn_vx_sd_r_inc,
                     -1);

   Operand_Access_Time(0, 1); // GR
   Operand_Access_Time(1, 0); // GPR
   Operand_Access_Time(2, 0); // GPR
   Operand_Access_Time(3, 1); // VxR
   Resource_Requirement(Resource_stxp70_ALL, 0);

   /*=================================*/
   Instruction_Group("vx_group10",
                     TOP_dyn_vx_COMPOSE_V_P2X,
                     TOP_dyn_vx_EXTRACT_V_X2P,
                     TOP_dyn_vx_WIDEMOVE_V_X2X,
                     -1);

   Any_Result_Available_Time(1);//VxR
   Any_Operand_Access_Time(1);  //VxR
   Resource_Requirement(Resource_stxp70_ALL, 0);  // PSEUDO MOVES

   /*=================================*/
   Instruction_Group("vx_group11",
                     TOP_dyn_vx_m2rub,
                     TOP_dyn_vx_m2ruhw,
                     TOP_dyn_vx_m2rw0,
                     TOP_dyn_vx_m2rw1,
                     -1);

   Any_Result_Available_Time(1); //GPR 
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Resource_Requirement(Resource_stxp70_ALL, 0); // MOVE TO CORE

   /*=================================*/
   Instruction_Group("vx_group12",
                     TOP_dyn_vx_modand,
                     -1);

   Any_Result_Available_Time(4); ///VxR
   Any_Operand_Access_Time (1); //GR, VxR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group13",
		     TOP_dyn_vx_addu8clp63,
		     TOP_dyn_vx_clpsym,
		     TOP_dyn_vx_incgt,
		     TOP_dyn_vx_mf,
		     TOP_dyn_vx_mfr,
		     TOP_dyn_vx_mpru8s8,
		     TOP_dyn_vx_MULTI_mpru8s8,
		     TOP_dyn_vx_mpru8u8,
		     TOP_dyn_vx_MULTI_mpru8u8,
                     TOP_dyn_vx_offset,
		     TOP_dyn_vx_shrrs16s8,
		     TOP_dyn_vx_shrrs16s8r,
                     -1);

   Any_Result_Available_Time(3); //VxR 
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group14",
		     TOP_dyn_vx_acc,
                     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 2); //VxR (Accumulator)
   Operand_Access_Time (2, 1); //VxR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group15",
		     TOP_dyn_vx_clr,
		     TOP_dyn_vx_dintlvb,
		     TOP_dyn_vx_MULTI_dintlvb,
		     TOP_dyn_vx_dlupdate,
		     TOP_dyn_vx_MULTI_dlupdate,
                     TOP_dyn_vx_intlvb,
                     TOP_dyn_vx_MULTI_intlvb,
		     TOP_dyn_vx_m2d,
		     TOP_dyn_vx_MULTI_m2d,
                     TOP_dyn_vx_m2v,
		     TOP_dyn_vx_shl1s16,
		     TOP_dyn_vx_MULTI_shl1s16,
		     TOP_dyn_vx_shr1s16,
		     TOP_dyn_vx_MULTI_shr1s16,
                     -1);

   Any_Result_Available_Time(1); //VxR [,VxR]
   Any_Operand_Access_Time(1); //GR, VxR [,VxR]
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1, 1); //att. M2X
#endif

   /*=================================*/
   Instruction_Group("vx_group20",
		     TOP_dyn_vx_m2xb,
		     TOP_dyn_vx_m2xhw,
		     TOP_dyn_vx_m2xw0,
		     TOP_dyn_vx_m2xw1,
                     -1);

   Any_Result_Available_Time(1); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1, 1); //att. M2X
#endif

   /*=================================*/
   Instruction_Group("vx_group21",
		     TOP_dyn_vx_m2xshldb,
                     -1);

   Any_Result_Available_Time(1); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 0); //GPR
   Operand_Access_Time (3, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1, 1); //att. M2X
#endif

   /*=================================*/
   Instruction_Group("vx_group22",
                     TOP_dyn_vx_m2x,
                     -1);

   Any_Result_Available_Time(1); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 0); //GPR
   Operand_Access_Time (2, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_RP1
   Resource_Requirement(Resource_stxp70_FPX_RP1,1); // att. M2X
#endif


   /*=================================*/
   /* TODO: not correctly modelized */
   Instruction_Group("vx_group23",
		     TOP_dyn_vx_fir3,
                     -1);

   Any_Result_Available_Time(3); //VxR  , should be 2??
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 2); //VxR (Accumulator)
   Operand_Access_Time (2, 1); //VxR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2); //att. T1
#endif

   /*=================================*/
   /* TODO: not correctly modelized */
   Instruction_Group("vx_group24",
		     TOP_dyn_vx_MULTI_fir3,
                     -1);

   Any_Result_Available_Time(3); //VxR  , should be 2??
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 2); //VxR (Accumulator)
   Operand_Access_Time (2, 2); //VxR (Accumulator)
   Operand_Access_Time (3, 1); //VxR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group25",
		     TOP_dyn_vx_clpu8,
		     TOP_dyn_vx_cmpeqru8,
		     TOP_dyn_vx_cmpgeru8,
		     TOP_dyn_vx_cmpgtru8,
		     TOP_dyn_vx_cmpleru8,
		     TOP_dyn_vx_cmpltru8,
		     TOP_dyn_vx_cmpneru8,
		     -1);

   Any_Result_Available_Time(2); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group26",
		     TOP_dyn_vx_bshrr,
		     TOP_dyn_vx_MULTI_clpsym,
		     TOP_dyn_vx_incgth3clr,
		     TOP_dyn_vx_maru8s8,
		     TOP_dyn_vx_maru8s8shr7,
		     TOP_dyn_vx_maru8s8shr7r,
		     TOP_dyn_vx_maru8u8,
		     TOP_dyn_vx_maru8u8shr7,
		     TOP_dyn_vx_maru8u8shr7r,
		     TOP_dyn_vx_MULTI_shrrs16s8,
		     TOP_dyn_vx_MULTI_shrrs16s8r,
		     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR (DxR)
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group27",
		     TOP_dyn_vx_incgth3,
		     TOP_dyn_vx_MULTI_incgth3clr,
		     TOP_dyn_vx_MULTI_maru8s8,
		     TOP_dyn_vx_MULTI_maru8s8shr7,
		     TOP_dyn_vx_MULTI_maru8s8shr7r,
		     TOP_dyn_vx_MULTI_maru8u8,
		     TOP_dyn_vx_MULTI_maru8u8shr7,
		     TOP_dyn_vx_MULTI_maru8u8shr7r,
		     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 1); //VxR
   Operand_Access_Time (4, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group28",
		     TOP_dyn_vx_MULTI_incgth3,
		     -1);

   Any_Result_Available_Time(3); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 1); //VxR
   Operand_Access_Time (4, 1); //VxR
   Operand_Access_Time (5, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group29",
		     TOP_dyn_vx_cpmsbr,
		     TOP_dyn_vx_cpmsbir,
		     -1);

   Any_Result_Available_Time(2); //VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL,0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2,2); //att. T1
#endif

   /*=================================*/
   Instruction_Group("vx_group30",
		     TOP_dyn_vx_incinsu16,
                     -1);

   Any_Result_Available_Time(4); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR (DxR)
   Operand_Access_Time (3, 0); //GPR
   Operand_Access_Time (4, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group31",
		     TOP_dyn_vx_MULTI_incinsu16,
                     -1);

   Any_Result_Available_Time(4); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 1); //VxR
   Operand_Access_Time (4, 0); //GPR
   Operand_Access_Time (5, 0); //GPR
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group32",
		     TOP_dyn_vx_insmean,
		     TOP_dyn_vx_MULTI_insmean,
		     TOP_dyn_vx_insmeanr,
		     TOP_dyn_vx_MULTI_insmeanr,
		     TOP_dyn_vx_meanuv,
		     TOP_dyn_vx_MULTI_meanuv,
		     TOP_dyn_vx_meanuvr,
		     TOP_dyn_vx_MULTI_meanuvr,
		     TOP_dyn_vx_meany,
		     TOP_dyn_vx_MULTI_meany,
		     TOP_dyn_vx_meanyr,
		     TOP_dyn_vx_MULTI_meanyr,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Any_Operand_Access_Time (1); //GR, VxR, [, VxR] [, VxR] [, VxR]
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group33",
		     TOP_dyn_vx_absdmpslt,
		     TOP_dyn_vx_sadmin,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR (DxR)
   Operand_Access_Time (2, 1); //VxR (DxR)
   Operand_Access_Time (3, 1); //VxR (DxR)
   Operand_Access_Time (4, 0); //GPR 
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group34",
		     TOP_dyn_vx_MULTI_absdmpslt,
		     TOP_dyn_vx_MULTI_sadmin,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 1); //VxR
   Operand_Access_Time (4, 1); //VxR
   Operand_Access_Time (5, 1); //VxR
   Operand_Access_Time (6, 1); //VxR
   Operand_Access_Time (7, 0); //GPR 
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group35",
		     TOP_dyn_vx_ascmf,
		     TOP_dyn_vx_ascmfr,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR (DxR)
   Operand_Access_Time (2, 0); //GPR 
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group36",
		     TOP_dyn_vx_MULTI_ascmf,
		     TOP_dyn_vx_MULTI_ascmfr,
		     TOP_dyn_vx_sad,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR (DxR)
   Operand_Access_Time (2, 1); //VxR (DxR)
   Operand_Access_Time (3, 0); //GPR 
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   /*=================================*/
   Instruction_Group("vx_group37",
		     TOP_dyn_vx_MULTI_sad,
                     -1);

   Any_Result_Available_Time(5); ///VxR
   Operand_Access_Time (0, 1); //GR
   Operand_Access_Time (1, 1); //VxR
   Operand_Access_Time (2, 1); //VxR
   Operand_Access_Time (3, 1); //VxR
   Operand_Access_Time (4, 1); //VxR
   Operand_Access_Time (5, 0); //GPR 
   Resource_Requirement(Resource_stxp70_ALL, 0);
#ifdef RES_EX2
   Resource_Requirement(Resource_stxp70_FPX_EX2, 2);
   Resource_Requirement(Resource_stxp70_FPX_EX2, 3); //att. T2
#endif

   Extension_Done();
   exit(0);
}

