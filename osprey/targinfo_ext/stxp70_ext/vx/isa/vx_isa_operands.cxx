
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "gen_util.h"
#include "isa_operands_gen.h"
#include "dyn_isa_topcode.h"
#include "dyn_isa_registers.h"
#include "dyn_isa_lits.h"
#include "dyn_isa_relocs.h"

int main( int argc, char *argv )
{
  // Setting extension name.
  Set_Dynamic("vx" /* extension name */);

  ISA_Operands_Begin("stxp70");

  //////////////////////////////////////////
  // Core information
  //////////////////////////////////////////
  // Describing core register operands in extension
  // instructions.
  //////////////////////////////////////////
  OPERAND_VALUE_TYPE Opd_gpr = 
      ISA_Reg_Opnd_Type_Create("Opd_gpr", 
                               ISA_REGISTER_CLASS_gpr, 
                               ISA_REGISTER_SUBCLASS_UNDEFINED, 
                               32, SIGNED, INVALID); 

  OPERAND_VALUE_TYPE Opd_gr = 
      ISA_Reg_Opnd_Type_Create("Opd_gr", 
                               ISA_REGISTER_CLASS_gr, 
                               ISA_REGISTER_SUBCLASS_UNDEFINED, 
                               1, UNSIGNED, INVALID); 


  //////////////////////////////////////////
  // Describing operand types. We assume that
  // extensions do not add any specific
  // in this list.
  //////////////////////////////////////////
  OPERAND_USE_TYPE base = Create_Operand_Use("base"); 
  OPERAND_USE_TYPE condition = Create_Operand_Use("condition"); 
  OPERAND_USE_TYPE multi = Create_Operand_Use("multi"); 
  OPERAND_USE_TYPE negoffset = Create_Operand_Use("negoffset"); 
  OPERAND_USE_TYPE offset = Create_Operand_Use("offset"); 
  OPERAND_USE_TYPE opnd1 = Create_Operand_Use("opnd1"); 
  OPERAND_USE_TYPE opnd2 = Create_Operand_Use("opnd2"); 
  OPERAND_USE_TYPE postincr = Create_Operand_Use("postincr"); 
  OPERAND_USE_TYPE predicate = Create_Operand_Use("predicate"); 
  OPERAND_USE_TYPE preincr = Create_Operand_Use("preincr"); 
  OPERAND_USE_TYPE storeval = Create_Operand_Use("storeval"); 
  OPERAND_USE_TYPE target = Create_Operand_Use("target"); 
  OPERAND_USE_TYPE uniq_res = Create_Operand_Use("uniq_res"); 


  //////////////////////////////////////////
  // Extension information.
  //////////////////////////////////////////
  // Describing extension register operands.
  //////////////////////////////////////////
  OPERAND_VALUE_TYPE Vx_opd_vr =
     ISA_Reg_Opnd_Type_Create("Vx_opd_vr",
                              ISA_REGISTER_CLASS_vx_vr,
                              ISA_REGISTER_SUBCLASS_UNDEFINED,
                              64,UNSIGNED,INVALID);

  OPERAND_VALUE_TYPE Vx_opd_dv =
     ISA_Reg_Opnd_Type_Create("Vx_opd_dv",
                              ISA_REGISTER_CLASS_vx_vr,
                              ISA_REGISTER_SUBCLASS_vx_dv,
                              128,UNSIGNED,INVALID);

  OPERAND_VALUE_TYPE Vx_opd_dv_P0 =
     ISA_Reg_Opnd_Type_Create("Vx_opd_dv_P0",
                              ISA_REGISTER_CLASS_vx_vr,
                              ISA_REGISTER_SUBCLASS_vx_dv_P0,
                              64,UNSIGNED,INVALID);

  OPERAND_VALUE_TYPE Vx_opd_dv_P1 =
     ISA_Reg_Opnd_Type_Create("Vx_opd_dv_P1",
                              ISA_REGISTER_CLASS_vx_vr,
                              ISA_REGISTER_SUBCLASS_vx_dv_P1,
                              64,UNSIGNED,INVALID);

  OPERAND_VALUE_TYPE Vx_opd_imm_u4 =
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_u4", 
                               4, 
                               UNSIGNED, 
                               LC_dyn_vx_imm_u4,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED); 

  OPERAND_VALUE_TYPE Vx_opd_imm_u3 =
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_u3", 
                               3, 
                               UNSIGNED, 
                               LC_dyn_vx_imm_u3,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED); 

  OPERAND_VALUE_TYPE Vx_opd_imm_u2 =
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_u2", 
                               2, 
                               UNSIGNED, 
                               LC_dyn_vx_imm_u2,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED); 

  OPERAND_VALUE_TYPE Vx_opd_imm_u5 = 
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_u5", 
                               5, 
                               UNSIGNED, 
                               LC_dyn_vx_imm_u5,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED); 

  OPERAND_VALUE_TYPE Vx_opd_imm_s9 =
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_s9",
                               9,
                               SIGNED,
                               LC_dyn_vx_imm_s9,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED);

  OPERAND_VALUE_TYPE Vx_opd_imm_u12 = 
      ISA_Lit_Opnd_Type_Create("Vx_opd_imm_u12", 
                               12, 
                               UNSIGNED, 
                               LC_dyn_vx_imm_u12,
			       ISA_RELOC_UNDEFINED,
			       ISA_RELOC_UNDEFINED); 


  /*==================================================*/
  Instruction_Group("vx_0",
                     TOP_dyn_vx_absd,
                     TOP_dyn_vx_addu16shr6,
                     TOP_dyn_vx_absdhm1,
                     TOP_dyn_vx_absdhp1,
                     TOP_dyn_vx_max,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);


  /*==================================================*/
  Instruction_Group("vx_1",
                    TOP_dyn_vx_absdmin,
                    TOP_dyn_vx_absdmax,
                    TOP_dyn_vx_fir3,
                    TOP_dyn_vx_maxh3,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_2",
                    TOP_dyn_vx_bshr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_3",
                    TOP_dyn_vx_cplsb,
                    TOP_dyn_vx_cpmsb,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_imm_u3);

  /*==================================================*/
  Instruction_Group("vx_4",
                    TOP_dyn_vx_cplsbi,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_5",
                     TOP_dyn_vx_fir3edge,
                     TOP_dyn_vx_intlvb,
                     TOP_dyn_vx_m2d,
                     TOP_dyn_vx_addvu8u8,
                     TOP_dyn_vx_mpvu8u8,
                     TOP_dyn_vx_mpviu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_6",
                    TOP_dyn_vx_m2x,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_7",
                    TOP_dyn_vx_scales8s9,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);
  Operand(3, Opd_gpr);  

  /*==================================================*/
  Instruction_Group("vx_8", 
		    TOP_dyn_vx_ld_r_inc,
		    (TOP)-1);
  
  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(2, Opd_gpr, offset);

  /*==================================================*/
  Instruction_Group("vx_9", 
		 TOP_dyn_vx_ld_i12_inc,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(2, Vx_opd_imm_u12, offset);


  /*==================================================*/
  Instruction_Group("vx_10", 
		 TOP_dyn_vx_ld_i8_inc,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
#if 1
  Operand(2, Vx_opd_imm_s9, offset);
#else
  Operand(2, Opd_imm_u8, offset);
#endif

  /*==================================================*/
  Instruction_Group("vx_10b", 
		 TOP_dyn_vx_ld_i8_dec,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
#if 1
  Operand(2, Vx_opd_imm_s9, offset);
  Operand(2, Vx_opd_imm_s9, negoffset);
#else
  Operand(2, Opd_imm_u8, offset);
#endif

  /*==================================================*/
  Instruction_Group("vx_11", 
		 TOP_dyn_vx_ld_r_post_inc,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Result (1, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Opd_gpr, offset);

  /*==================================================*/
  Instruction_Group("vx_12", 
		 TOP_dyn_vx_ld_i5_post_dec,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Result (1, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Vx_opd_imm_u5, negoffset);
  Operand(2, Vx_opd_imm_u5, offset);

  /*==================================================*/
  Instruction_Group("vx_13", 
		 TOP_dyn_vx_ld_i5_post_inc,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Result (1, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Vx_opd_imm_u5, offset);

  /*==================================================*/
  Instruction_Group("vx_14", 
		 TOP_dyn_vx_ld_i5_pre_dec,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Result (1, Opd_gpr, preincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, preincr);
  Operand(2, Vx_opd_imm_u5, negoffset);
  Operand(2, Vx_opd_imm_u5, offset);

  /*==================================================*/
  Instruction_Group("vx_15", 
		 TOP_dyn_vx_sd_r_inc,
		 (TOP)-1);

  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(2, Opd_gpr, offset);
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_16", 
		 TOP_dyn_vx_sd_i12_inc,
		 (TOP)-1);

  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(2, Vx_opd_imm_u12, offset);
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_17", 
		 TOP_dyn_vx_sd_i8_inc,
		 (TOP)-1);

  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
#if 1
  Operand(2, Vx_opd_imm_s9, offset);
#else
  Operand(2, Opd_imm_u8, offset);
#endif
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_17b", 
		 TOP_dyn_vx_sd_i8_dec,
		 (TOP)-1);

  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
#if 1
  Operand(2, Vx_opd_imm_s9, offset);
  Operand(2, Vx_opd_imm_s9, negoffset);
#else
  Operand(2, Opd_imm_u8, offset);
#endif
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_18", 
		 TOP_dyn_vx_sd_r_post_inc,
		 (TOP)-1);

  Result (0, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Opd_gpr, offset);
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_19", 
		 TOP_dyn_vx_sd_i5_post_dec,
		 (TOP)-1);

  Result (0, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Vx_opd_imm_u5, negoffset);
  Operand(2, Vx_opd_imm_u5, offset);
  Operand(3, Vx_opd_vr, storeval);
  /*==================================================*/
  Instruction_Group("vx_20", 
		 TOP_dyn_vx_sd_i5_post_inc,
		 (TOP)-1);

  Result (0, Opd_gpr, postincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, postincr);
  Operand(2, Vx_opd_imm_u5, offset);
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_21", 
		 TOP_dyn_vx_sd_i5_pre_dec,
		 (TOP)-1);

  Result (0, Opd_gpr, preincr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Opd_gpr, base);
  Operand(1, Opd_gpr, preincr);
  Operand(2, Vx_opd_imm_u5, negoffset);
  Operand(2, Vx_opd_imm_u5, offset);
  Operand(3, Vx_opd_vr, storeval);

  /*==================================================*/
  Instruction_Group("vx_22", 
		 TOP_dyn_vx_COMPOSE_V_P2X,
		 (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Vx_opd_vr, opnd1);
  Operand(1, Vx_opd_vr, opnd2);

  /*==================================================*/
  Instruction_Group("vx_23", 
		 TOP_dyn_vx_EXTRACT_V_X2P,
		 (TOP)-1);

  Result (0, Vx_opd_vr);
  Result (1, Vx_opd_vr);
  Operand(0, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_24",
                    TOP_dyn_vx_m2v,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr, opnd1);

  /*==================================================*/
  Instruction_Group("vx_25",
                    TOP_dyn_vx_m2rub,
                    (TOP)-1);

  Result (0, Opd_gpr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_imm_u3);

  /*==================================================*/
  Instruction_Group("vx_26",
                    TOP_dyn_vx_m2ruhw,
                    (TOP)-1);

  Result (0, Opd_gpr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_27",
                    TOP_dyn_vx_m2rw0,
                    TOP_dyn_vx_m2rw1,
                    (TOP)-1);

  Result (0, Opd_gpr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_28",
                    TOP_dyn_vx_offset,
                    TOP_dyn_vx_addu8clp63,
                    TOP_dyn_vx_cmpgtru8,
                    TOP_dyn_vx_cpmsbr,
                    TOP_dyn_vx_cpmsbir,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_29",
                    TOP_dyn_vx_modand,
                    TOP_dyn_vx_random,
                    TOP_dyn_vx_clp63,
                    TOP_dyn_vx_maxpair,
                    TOP_dyn_vx_sub128,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_30",
                    TOP_dyn_vx_WIDEMOVE_V_X2X,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Vx_opd_dv, opnd1);

  /*==================================================*/
  Instruction_Group("vx_31",
                    TOP_dyn_vx_MULTI_absdmax,
                    TOP_dyn_vx_MULTI_absdmin,
                    TOP_dyn_vx_MULTI_fir3,
                    TOP_dyn_vx_MULTI_maxh3,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_32",
                     TOP_dyn_vx_MULTI_fir3edge,
                     TOP_dyn_vx_MULTI_intlvb,
                     TOP_dyn_vx_MULTI_m2d,
                     TOP_dyn_vx_MULTI_addvu8u8,
                     TOP_dyn_vx_MULTI_mpvu8u8,
                     TOP_dyn_vx_MULTI_mpviu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_33",
                     TOP_dyn_vx_acc,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_34",
                     TOP_dyn_vx_m2xb,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);
  Operand(3, Vx_opd_imm_u3);

  /*==================================================*/
  Instruction_Group("vx_35",
                     TOP_dyn_vx_m2xhw,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);
  Operand(3, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_36",
		    TOP_dyn_vx_m2xshldb,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);
  Operand(3, Opd_gpr);  

  /*==================================================*/
  Instruction_Group("vx_37",
		    TOP_dyn_vx_m2xw0,
		    TOP_dyn_vx_m2xw1,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_38",
		    TOP_dyn_vx_addu16m1,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_39",
		    TOP_dyn_vx_MULTI_addu16m1,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_40",
		    TOP_dyn_vx_addu16m2,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_41",
		    TOP_dyn_vx_MULTI_addu16m2,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_42",
		    TOP_dyn_vx_firu8s8p,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);
  Operand(4, Vx_opd_imm_u3);

  /*==================================================*/
  Instruction_Group("vx_43",
		    TOP_dyn_vx_MULTI_firu8s8p,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);
  Operand(7, Vx_opd_imm_u3);

  /*==================================================*/
  Instruction_Group("vx_44",
		    TOP_dyn_vx_incgth3,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_45",
		    TOP_dyn_vx_MULTI_incgth3,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_dv_P0);
  Operand(3, Vx_opd_dv_P1, multi);
  Operand(4, Vx_opd_vr);
  Operand(5, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_46",
		    TOP_dyn_vx_incgth3clr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_47",
		    TOP_dyn_vx_MULTI_incgth3clr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_48",
		    TOP_dyn_vx_incinsu16,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_dv);
  Operand(3, Opd_gpr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_49",
		    TOP_dyn_vx_MULTI_incinsu16,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_dv_P0);
  Operand(3, Vx_opd_dv_P1, multi);
  Operand(4, Opd_gpr);
  Operand(5, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_50",
		    TOP_dyn_vx_maru8s8shr7r,
		    TOP_dyn_vx_maru8u8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_51",
		    TOP_dyn_vx_MULTI_maru8s8shr7r,
		    TOP_dyn_vx_MULTI_maru8u8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_52",
		    TOP_dyn_vx_maviu8u8shr7r,
		    TOP_dyn_vx_mavu8u8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_53",
		    TOP_dyn_vx_MULTI_maviu8u8shr7r,
		    TOP_dyn_vx_MULTI_mavu8u8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_54",
		    TOP_dyn_vx_mpru8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_55",
		    TOP_dyn_vx_MULTI_mpru8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_56",
                     TOP_dyn_vx_select,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_57",
		    TOP_dyn_vx_absdmpslt,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_58",
		    TOP_dyn_vx_MULTI_absdmpslt,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);
  Operand(7, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_59",
		    TOP_dyn_vx_absdslt,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_60",
		    TOP_dyn_vx_MULTI_absdslt,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_61",
		    TOP_dyn_vx_adds16,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_62",
		    TOP_dyn_vx_MULTI_adds16,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_63",
		    TOP_dyn_vx_adds16shr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_64",
		    TOP_dyn_vx_MULTI_adds16shr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_65",
		    TOP_dyn_vx_adds16shrr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_66",
		    TOP_dyn_vx_MULTI_adds16shrr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_67",
		    TOP_dyn_vx_ascmf,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_68",
		    TOP_dyn_vx_MULTI_ascmf,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_69",
		    TOP_dyn_vx_ascmfr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_70",
		    TOP_dyn_vx_MULTI_ascmfr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_71",
		    TOP_dyn_vx_bshrr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_72",
		    TOP_dyn_vx_clpsym,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_73",
		    TOP_dyn_vx_MULTI_clpsym,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_74",
		    TOP_dyn_vx_clpu8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_75",
		    TOP_dyn_vx_clr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);

  /*==================================================*/
  Instruction_Group("vx_76",
		    TOP_dyn_vx_cmpeqru8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_77",
		    TOP_dyn_vx_cmpgeru8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_78",
		    TOP_dyn_vx_cmpleru8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_79",
		    TOP_dyn_vx_cmpltru8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_80",
		    TOP_dyn_vx_cmpneru8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_81",
		    TOP_dyn_vx_cntdelta0clr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_82",
		    TOP_dyn_vx_MULTI_cntdelta0clr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_83",
		    TOP_dyn_vx_cntdeltabw,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);
  Operand(4, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_84",
		    TOP_dyn_vx_MULTI_cntdeltabw,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);
  Operand(7, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_85",
		    TOP_dyn_vx_cntdeltafw,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);
  Operand(4, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_86",
		    TOP_dyn_vx_MULTI_cntdeltafw,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);
  Operand(7, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_87",
		    TOP_dyn_vx_cpmsbi,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_imm_u2);

  /*==================================================*/
  Instruction_Group("vx_88",
		    TOP_dyn_vx_dintlvb,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_89",
		    TOP_dyn_vx_MULTI_dintlvb,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_90",
		    TOP_dyn_vx_dlupdate,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_91",
		    TOP_dyn_vx_MULTI_dlupdate,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_92",
		    TOP_dyn_vx_firu8s8p1clr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_93",
		    TOP_dyn_vx_MULTI_firu8s8p1clr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_94",
		    TOP_dyn_vx_getsad0,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_95",
		    TOP_dyn_vx_MULTI_getsad0,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_96",
		    TOP_dyn_vx_incgt,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_97",
		    TOP_dyn_vx_insmean,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_98",
		    TOP_dyn_vx_MULTI_insmean,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_99",
		    TOP_dyn_vx_insmeanr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_100",
		    TOP_dyn_vx_MULTI_insmeanr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_101",
		    TOP_dyn_vx_maru8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_102",
		    TOP_dyn_vx_MULTI_maru8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_103",
		    TOP_dyn_vx_maru8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_104",
		    TOP_dyn_vx_MULTI_maru8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_105",
		    TOP_dyn_vx_maru8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_106",
		    TOP_dyn_vx_MULTI_maru8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_107",
		    TOP_dyn_vx_maru8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_108",
		    TOP_dyn_vx_MULTI_maru8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_109",
		    TOP_dyn_vx_maviu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_110",
		    TOP_dyn_vx_MULTI_maviu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_111",
		    TOP_dyn_vx_maviu8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_112",
		    TOP_dyn_vx_MULTI_maviu8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_113",
		    TOP_dyn_vx_maviu8s8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_114",
		    TOP_dyn_vx_MULTI_maviu8s8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_115",
		    TOP_dyn_vx_maviu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_116",
		    TOP_dyn_vx_MULTI_maviu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_117",
		    TOP_dyn_vx_maviu8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_118",
		    TOP_dyn_vx_MULTI_maviu8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_119",
		    TOP_dyn_vx_mavu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_120",
		    TOP_dyn_vx_MULTI_mavu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_121",
		    TOP_dyn_vx_mavu8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_122",
		    TOP_dyn_vx_MULTI_mavu8s8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_123",
		    TOP_dyn_vx_mavu8s8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_124",
		    TOP_dyn_vx_MULTI_mavu8s8shr7r,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_125",
		    TOP_dyn_vx_mavu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_126",
		    TOP_dyn_vx_MULTI_mavu8u8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_127",
		    TOP_dyn_vx_mavu8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_128",
		    TOP_dyn_vx_MULTI_mavu8u8shr7,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);
  Operand(4, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_129",
		    TOP_dyn_vx_mean,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_130",
		    TOP_dyn_vx_meanr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_131",
		    TOP_dyn_vx_meanuv,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_132",
		    TOP_dyn_vx_MULTI_meanuv,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_133",
		    TOP_dyn_vx_meanuvr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_134",
		    TOP_dyn_vx_MULTI_meanuvr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_135",
		    TOP_dyn_vx_meany,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_136",
		    TOP_dyn_vx_MULTI_meany,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_137",
		    TOP_dyn_vx_meanyr,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_138",
		    TOP_dyn_vx_MULTI_meanyr,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_139",
		    TOP_dyn_vx_median,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_140",
		    TOP_dyn_vx_MULTI_median,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_141",
		    TOP_dyn_vx_mf,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_142",
		    TOP_dyn_vx_mfr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_143",
		    TOP_dyn_vx_min,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_144",
		    TOP_dyn_vx_minsad,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_145",
		    TOP_dyn_vx_MULTI_minsad,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_146",
		    TOP_dyn_vx_mpru8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_147",
		    TOP_dyn_vx_MULTI_mpru8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_148",
		    TOP_dyn_vx_mpu8u8shr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Opd_gpr);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_149",
		    TOP_dyn_vx_mpviu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_150",
		    TOP_dyn_vx_MULTI_mpviu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_151",
		    TOP_dyn_vx_mpvu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_152",
		    TOP_dyn_vx_MULTI_mpvu8s8,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_vr);
  Operand(2, Vx_opd_vr);

  /*==================================================*/
  Instruction_Group("vx_153",
		    TOP_dyn_vx_sad,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_154",
		    TOP_dyn_vx_MULTI_sad,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_155",
		    TOP_dyn_vx_sadmin,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Same_Res(1);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_dv);
  Operand(4, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_156",
		    TOP_dyn_vx_MULTI_sadmin,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Same_Res(1);
  Result (1, Vx_opd_dv_P1, multi);
  Same_Res(2);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_dv_P0);
  Operand(6, Vx_opd_dv_P1, multi);
  Operand(7, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_157",
		    TOP_dyn_vx_shl1s16,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_158",
		    TOP_dyn_vx_MULTI_shl1s16,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_159",
		    TOP_dyn_vx_shr1s16,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_160",
		    TOP_dyn_vx_MULTI_shr1s16,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_161",
		    TOP_dyn_vx_shr7s16s8rc,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_162",
		    TOP_dyn_vx_MULTI_shr7s16s8rc,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_163",
		    TOP_dyn_vx_shr7s16s8rs,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_164",
		    TOP_dyn_vx_MULTI_shr7s16s8rs,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_165",
		    TOP_dyn_vx_shrrs16s8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_166",
		    TOP_dyn_vx_MULTI_shrrs16s8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_167",
		    TOP_dyn_vx_shrrs16s8r,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_168",
		    TOP_dyn_vx_MULTI_shrrs16s8r,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Opd_gpr);

  /*==================================================*/
  Instruction_Group("vx_169",
		    TOP_dyn_vx_shrs16s8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_170",
		    TOP_dyn_vx_MULTI_shrs16s8,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_171",
		    TOP_dyn_vx_shrs16s8r,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_172",
		    TOP_dyn_vx_MULTI_shrs16s8r,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_173",
		    TOP_dyn_vx_subs16,
                    (TOP)-1);

  Result (0, Vx_opd_dv);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);

  /*==================================================*/
  Instruction_Group("vx_174",
		    TOP_dyn_vx_MULTI_subs16,
                    (TOP)-1);

  Result (0, Vx_opd_dv_P0);
  Result (1, Vx_opd_dv_P1, multi);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);

  /*==================================================*/
  Instruction_Group("vx_175",
		    TOP_dyn_vx_subs16shr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_176",
		    TOP_dyn_vx_MULTI_subs16shr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_177",
		    TOP_dyn_vx_subs16shrr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv);
  Operand(2, Vx_opd_dv);
  Operand(3, Vx_opd_imm_u4);

  /*==================================================*/
  Instruction_Group("vx_178",
		    TOP_dyn_vx_MULTI_subs16shrr,
                    (TOP)-1);

  Result (0, Vx_opd_vr);
  Operand(0, Opd_gr, predicate);
  Operand(1, Vx_opd_dv_P0);
  Operand(2, Vx_opd_dv_P1, multi);
  Operand(3, Vx_opd_dv_P0);
  Operand(4, Vx_opd_dv_P1, multi);
  Operand(5, Vx_opd_imm_u4);

  ISA_Operands_End();
  exit(0);
}
