
#include <stdio.h>
#include <stdlib.h>

#include "gen_util.h"
#include "dyn_isa_topcode.h"
#include "dyn_isa_subset.h"
#include "isa_hazards_gen.h"

int main( int argc, char *argv[] )
{
    Set_Dynamic( "vx" ); 

    ISA_Hazards_Begin("vx");
    ISA_Hazards_End();

    exit(0);
}
