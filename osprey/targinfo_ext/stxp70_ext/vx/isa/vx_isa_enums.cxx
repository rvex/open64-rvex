
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "isa_enums_gen.h"
#include "gen_util.h"

int main( int argc, char *argv[] )
{
    Set_Dynamic("vx" /* Extension name */);

    /*
     * Enumerations are not supported yet!
     */
    ISA_Enums_Begin();

    ISA_Enums_End  ();
    exit(0);
}
