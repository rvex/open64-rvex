

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h> 
#include "gen_util.h"
#include "isa_lits_gen.h" 


int
main( int argc, char *argv[] ) 
{ 
  Set_Dynamic("vx" /* extension name */);

  ISA_Lits_Begin(); 

  ISA_Create_Lit_Class("vx_imm_u12", UNSIGNED, UnsignedBitRange(12), LIT_RANGE_END);
  ISA_Create_Lit_Class("vx_imm_u5", UNSIGNED, UnsignedBitRange(5), LIT_RANGE_END);
  ISA_Create_Lit_Class("vx_imm_u4", UNSIGNED, UnsignedBitRange(4), LIT_RANGE_END);
  ISA_Create_Lit_Class("vx_imm_u3", UNSIGNED, UnsignedBitRange(3), LIT_RANGE_END);
  ISA_Create_Lit_Class("vx_imm_u2", UNSIGNED, UnsignedBitRange(2), LIT_RANGE_END);
  // Post patch to handle addresing modes with a signed 9
  ISA_Create_Lit_Class("vx_imm_s9", SIGNED, SignedBitRange(9), LIT_RANGE_END);

  ISA_Lits_End();
  exit(0);
}

