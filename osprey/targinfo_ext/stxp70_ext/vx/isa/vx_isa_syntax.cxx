

#include <stdio.h> 
#include <stdlib.h>
#include "dyn_isa_topcode.h"
#include "dyn_isa_subset.h"
#include "isa_syntax_gen.h" 
#include "gen_util.h"

static const char *mnemonic_names[] = { 
 "absd"       ,      /* TOP_dyn_vx_absd           */
 "absdmax"    ,      /* TOP_dyn_vx_absdmax        */
 "absdmin"    ,      /* TOP_dyn_vx_absdmin        */
 "addu16shr6" ,      /* TOP_dyn_vx_addu16shr6     */
 "bshr"       ,      /* TOP_dyn_vx_bshr           */
 "cplsb"      ,      /* TOP_dyn_vx_cplsb          */
 "cplsbi"     ,      /* TOP_dyn_vx_cplsbi         */
 "fir3edge"   ,      /* TOP_dyn_vx_fir3edge       */
 "intlvb"     ,      /* TOP_dyn_vx_intlvb         */
 "m2rub"      ,      /* TOP_dyn_vx_m2rub          */
 "m2ruhw"     ,      /* TOP_dyn_vx_m2ruhw         */
 "m2rw0"      ,      /* TOP_dyn_vx_m2rw0          */
 "m2rw1"      ,      /* TOP_dyn_vx_m2rw1          */
 "m2x"        ,      /* TOP_dyn_vx_m2x            */
 "m2v"        ,      /* TOP_dyn_vx_m2v            */
 "modand"     ,      /* TOP_dyn_vx_modand         */
 "offset"     ,      /* TOP_dyn_vx_offset         */
 "random"     ,      /* TOP_dyn_vx_random         */
 "scales8s9"  ,      /* TOP_dyn_vx_scales8s9      */
 "ld"         ,      /* TOP_dyn_vx_ld_r_post_inc  */
 "ld"         ,      /* TOP_dyn_vx_ld_i5_post_inc */
 "ld"         ,      /* TOP_dyn_vx_ld_r_inc       */
 "ld"         ,      /* TOP_dyn_vx_ld_i12_inc     */
 "ld"         ,      /* TOP_dyn_vx_ld_i8_inc      */
 "ld"         ,      /* TOP_dyn_vx_ld_i5_post_dec */
 "ld"         ,      /* TOP_dyn_vx_ld_i5_pre_dec  */
 "ld"         ,      /* TOP_dyn_vx_ld_i8_dec      */
 "sd"         ,      /* TOP_dyn_vx_sd_r_post_inc  */
 "sd"         ,      /* TOP_dyn_vx_sd_i5_post_inc */
 "sd"         ,      /* TOP_dyn_vx_sd_r_inc       */
 "sd"         ,      /* TOP_dyn_vx_sd_i12_inc     */
 "sd"         ,      /* TOP_dyn_vx_sd_i8_inc      */
 "sd"         ,      /* TOP_dyn_vx_sd_i5_post_dec */
 "sd"         ,      /* TOP_dyn_vx_sd_i5_pre_dec  */
 "sd"         ,      /* TOP_dyn_vx_sd_i8_dec      */
 "COMPOSE_V_P2X" ,
 "EXTRACT_V_X2P" ,
 "WIDEMOVE_V_X2X",
 "absdmax",          /* TOP_dyn_vx_MULTI_absdmax  */
 "absdmin",          /* TOP_dyn_vx_MULTI_absdmin  */
 "fir3edge",         /* TOP_dyn_vx_MULTI_fir3edge */
 "intlvb",           /* TOP_dyn_vx_MULTI_intlvb   */
 "acc",	             /* TOP_dyn_vx_acc		  */
 "m2d", 	     /* TOP_dyn_vx_m2d            */
 "m2d",      	     /* TOP_dyn_vx_MULTI_m2d      */
 "m2xb", 	     /* TOP_dyn_vx_m2xb           */
 "m2xhw", 	     /* TOP_dyn_vx_m2xhw */
 "m2xshldb",         /* TOP_dyn_vx_m2xshldb */
 "m2xw0",            /* TOP_dyn_vx_m2xw0 */
 "m2xw1",            /* TOP_dyn_vx_m2xw1 */
 "absdhm1",          /* TOP_dyn_vx_absdhm1 */
 "absdhp1",          /* TOP_dyn_vx_absdhp1 */
 "addu16m1",         /* TOP_dyn_vx_addu16m1 */
 "addu16m2",         /* TOP_dyn_vx_addu16m2 */
 "addu8clp63",       /* TOP_dyn_vx_addu8clp63 */
 "addvu8u8",         /* TOP_dyn_vx_addvu8u8 */
 "clp63",            /* TOP_dyn_vx_clp63 */
 "cmpgtru8",         /* TOP_dyn_vx_cmpgtru8 */
 "cpmsb",            /* TOP_dyn_vx_cpmsb */
 "cpmsbr",           /* TOP_dyn_vx_cpmsbr */
 "cpmsbir",          /* TOP_dyn_vx_cpmsbir */
 "fir3",             /* TOP_dyn_vx_fir3 */
 "firu8s8p",         /* TOP_dyn_vx_firu8s8p */
 "incgth3",          /* TOP_dyn_vx_incgth3 */
 "incgth3clr",       /* TOP_dyn_vx_incgth3clr */
 "incinsu16",        /* TOP_dyn_vx_incinsu16 */
 "maru8s8shr7r",     /* TOP_dyn_vx_maru8s8shr7r */
 "maru8u8shr7r",     /* TOP_dyn_vx_maru8u8shr7r */
 "maviu8u8shr7r",    /* TOP_dyn_vx_maviu8u8shr7r */
 "mavu8u8shr7r",     /* TOP_dyn_vx_mavu8u8shr7r */
 "max",              /* TOP_dyn_vx_max */
 "maxh3",            /* TOP_dyn_vx_maxh3 */
 "maxpair",          /* TOP_dyn_vx_maxpair */
 "mpru8u8",          /* TOP_dyn_vx_mpru8u8 */
 "mpvu8u8",          /* TOP_dyn_vx_mpvu8u8 */
 "mpviu8u8",         /* TOP_dyn_vx_mpviu8u8 */
 "select",           /* TOP_dyn_vx_select */
 "sub128",           /* TOP_dyn_vx_sub128 */
 "addu16m1",         /* TOP_dyn_vx_MULTI_addu16m1 */
 "addu16m2",         /* TOP_dyn_vx_MULTI_addu16m2 */
 "addvu8u8",         /* TOP_dyn_vx_MULTI_addvu8u8 */
 "fir3",             /* TOP_dyn_vx_MULTI_fir3 */
 "firu8s8p",         /* TOP_dyn_vx_MULTI_firu8s8p */
 "incgth3",          /* TOP_dyn_vx_MULTI_incgth3 */
 "incgth3clr",       /* TOP_dyn_vx_MULTI_incgth3clr */
 "incinsu16",        /* TOP_dyn_vx_MULTI_incinsu16 */
 "maru8s8shr7r",     /* TOP_dyn_vx_MULTI_maru8s8shr7r */
 "maru8u8shr7r",     /* TOP_dyn_vx_MULTI_maru8u8shr7r */
 "maviu8u8shr7r",    /* TOP_dyn_vx_MULTI_maviu8u8shr7r */
 "mavu8u8shr7r",     /* TOP_dyn_vx_MULTI_mavu8u8shr7r */
 "maxh3",            /* TOP_dyn_vx_MULTI_maxh3 */
 "mpru8u8",          /* TOP_dyn_vx_MULTI_mpru8u8 */
 "mpvu8u8",          /* TOP_dyn_vx_MULTI_mpvu8u8 */
 "mpviu8u8",         /* TOP_dyn_vx_MULTI_mpviu8u8 */

 "absdmpslt",        /* TOP_dyn_vx_absdmpslt, */
 "absdslt",          /* TOP_dyn_vx_absdslt, */
 "adds16",           /* TOP_dyn_vx_adds16, */
 "adds16shr",        /* TOP_dyn_vx_adds16shr, */
 "adds16shrr",       /* TOP_dyn_vx_adds16shrr, */
 "ascmf",            /* TOP_dyn_vx_ascmf, */
 "ascmfr",           /* TOP_dyn_vx_ascmfr, */
 "bshrr",            /* TOP_dyn_vx_bshrr, */
 "clpsym",           /* TOP_dyn_vx_clpsym, */
 "clpu8",            /* TOP_dyn_vx_clpu8, */
 "clr",              /* TOP_dyn_vx_clr, */
 "cmpeqru8",         /* TOP_dyn_vx_cmpeqru8, */
 "cmpneru8",         /* TOP_dyn_vx_cmpneru8, */
 "cmpgeru8",         /* TOP_dyn_vx_cmpgeru8, */
 "cmpltru8",         /* TOP_dyn_vx_cmpltru8, */
 "cmpleru8",         /* TOP_dyn_vx_cmpleru8, */
 "cntdelta0clr",     /* TOP_dyn_vx_cntdelta0clr, */
 "cntdeltabw",       /* TOP_dyn_vx_cntdeltabw, */
 "cntdeltafw",       /* TOP_dyn_vx_cntdeltafw, */
 "cpmsbi",           /* TOP_dyn_vx_cpmsbi, */
 "dintlvb",          /* TOP_dyn_vx_dintlvb, */
 "dlupdate",         /* TOP_dyn_vx_dlupdate, */
 "firu8s8p1clr",     /* TOP_dyn_vx_firu8s8p1clr, */
 "getsad0",          /* TOP_dyn_vx_getsad0, */
 "incgt",            /* TOP_dyn_vx_incgt, */
 "insmean",          /* TOP_dyn_vx_insmean, */
 "insmeanr",         /* TOP_dyn_vx_insmeanr, */
 "maru8s8",          /* TOP_dyn_vx_maru8s8, */
 "maru8s8shr7",      /* TOP_dyn_vx_maru8s8shr7, */
 "maru8u8",          /* TOP_dyn_vx_maru8u8, */
 "maru8u8shr7",      /* TOP_dyn_vx_maru8u8shr7, */
 "maviu8s8",         /* TOP_dyn_vx_maviu8s8, */
 "maviu8s8shr7",     /* TOP_dyn_vx_maviu8s8shr7, */
 "maviu8s8shr7r",    /* TOP_dyn_vx_maviu8s8shr7r, */
 "maviu8u8",         /* TOP_dyn_vx_maviu8u8, */
 "maviu8u8shr7",     /* TOP_dyn_vx_maviu8u8shr7, */
 "mavu8s8",          /* TOP_dyn_vx_mavu8s8, */
 "mavu8s8shr7",      /* TOP_dyn_vx_mavu8s8shr7, */
 "mavu8s8shr7r",     /* TOP_dyn_vx_mavu8s8shr7r, */
 "mavu8u8",          /* TOP_dyn_vx_mavu8u8, */
 "mavu8u8shr7",      /* TOP_dyn_vx_mavu8u8shr7, */
 "mean",             /* TOP_dyn_vx_mean, */
 "meanr",            /* TOP_dyn_vx_meanr, */
 "meanuv",           /* TOP_dyn_vx_meanuv, */
 "meanuvr",          /* TOP_dyn_vx_meanuvr, */
 "meany",            /* TOP_dyn_vx_meany, */
 "meanyr",           /* TOP_dyn_vx_meanyr, */
 "median",           /* TOP_dyn_vx_median, */
 "mf",               /* TOP_dyn_vx_mf, */
 "mfr",              /* TOP_dyn_vx_mfr, */
 "min",              /* TOP_dyn_vx_min, */
 "minsad",           /* TOP_dyn_vx_minsad, */
 "mpru8s8",          /* TOP_dyn_vx_mpru8s8, */
 "mpu8u8shr",        /* TOP_dyn_vx_mpu8u8shr, */
 "mpviu8s8",         /* TOP_dyn_vx_mpviu8s8, */
 "mpvu8s8",          /* TOP_dyn_vx_mpvu8s8, */
 "sad",              /* TOP_dyn_vx_sad, */
 "sadmin",           /* TOP_dyn_vx_sadmin, */
 "shl1s16",          /* TOP_dyn_vx_shl1s16, */
 "shr1s16",          /* TOP_dyn_vx_shr1s16, */
 "shr7s16s8rc",      /* TOP_dyn_vx_shr7s16s8rc, */
 "shr7s16s8rs",      /* TOP_dyn_vx_shr7s16s8rs, */
 "shrrs16s8",        /* TOP_dyn_vx_shrrs16s8, */
 "shrrs16s8r",       /* TOP_dyn_vx_shrrs16s8r, */
 "shrs16s8",         /* TOP_dyn_vx_shrs16s8, */
 "shrs16s8r",        /* TOP_dyn_vx_shrs16s8r, */
 "subs16",           /* TOP_dyn_vx_subs16, */
 "subs16shr",        /* TOP_dyn_vx_subs16shr, */
 "subs16shrr",       /* TOP_dyn_vx_subs16shrr, */
 "absdmpslt",        /* TOP_dyn_vx_MULTI_absdmpslt, */
 "absdslt",          /* TOP_dyn_vx_MULTI_absdslt, */
 "adds16",           /* TOP_dyn_vx_MULTI_adds16, */
 "adds16shr",        /* TOP_dyn_vx_MULTI_adds16shr, */
 "adds16shrr",       /* TOP_dyn_vx_MULTI_adds16shrr, */
 "ascmf",            /* TOP_dyn_vx_MULTI_ascmf, */
 "ascmfr",           /* TOP_dyn_vx_MULTI_ascmfr, */
 "clpsym",           /* TOP_dyn_vx_MULTI_clpsym, */
 "cntdelta0clr",     /* TOP_dyn_vx_MULTI_cntdelta0clr, */
 "cntdeltabw",       /* TOP_dyn_vx_MULTI_cntdeltabw, */
 "cntdeltafw",       /* TOP_dyn_vx_MULTI_cntdeltafw, */
 "dintlvb",          /* TOP_dyn_vx_MULTI_dintlvb, */
 "dlupdate",         /* TOP_dyn_vx_MULTI_dlupdate, */
 "firu8s8p1clr",     /* TOP_dyn_vx_MULTI_firu8s8p1clr, */
 "getsad0",          /* TOP_dyn_vx_MULTI_getsad0, */
 "insmean",          /* TOP_dyn_vx_MULTI_insmean, */
 "insmeanr",         /* TOP_dyn_vx_MULTI_insmeanr, */
 "maru8s8",          /* TOP_dyn_vx_MULTI_maru8s8, */
 "maru8s8shr7",      /* TOP_dyn_vx_MULTI_maru8s8shr7, */
 "maru8u8",          /* TOP_dyn_vx_MULTI_maru8u8, */
 "maru8u8shr7",      /* TOP_dyn_vx_MULTI_maru8u8shr7, */
 "maviu8s8",         /* TOP_dyn_vx_MULTI_maviu8s8, */
 "maviu8s8shr7",     /* TOP_dyn_vx_MULTI_maviu8s8shr7, */
 "maviu8s8shr7r",    /* TOP_dyn_vx_MULTI_maviu8s8shr7r, */
 "maviu8u8",         /* TOP_dyn_vx_MULTI_maviu8u8, */
 "maviu8u8shr7",     /* TOP_dyn_vx_MULTI_maviu8u8shr7, */
 "mavu8s8",          /* TOP_dyn_vx_MULTI_mavu8s8, */
 "mavu8s8shr7",      /* TOP_dyn_vx_MULTI_mavu8s8shr7, */
 "mavu8s8shr7r",     /* TOP_dyn_vx_MULTI_mavu8s8shr7r, */
 "mavu8u8",          /* TOP_dyn_vx_MULTI_mavu8u8, */
 "mavu8u8shr7",      /* TOP_dyn_vx_MULTI_mavu8u8shr7, */
 "meanuv",           /* TOP_dyn_vx_MULTI_meanuv, */
 "meanuvr",          /* TOP_dyn_vx_MULTI_meanuvr, */
 "meany",            /* TOP_dyn_vx_MULTI_meany, */
 "meanyr",           /* TOP_dyn_vx_MULTI_meanyr, */
 "median",           /* TOP_dyn_vx_MULTI_median, */
 "minsad",           /* TOP_dyn_vx_MULTI_minsad, */
 "mpru8s8",          /* TOP_dyn_vx_MULTI_mpru8s8, */
 "mpviu8s8",         /* TOP_dyn_vx_MULTI_mpviu8s8, */
 "mpvu8s8",          /* TOP_dyn_vx_MULTI_mpvu8s8, */
 "sad",              /* TOP_dyn_vx_MULTI_sad, */
 "sadmin",           /* TOP_dyn_vx_MULTI_sadmin, */
 "shl1s16",          /* TOP_dyn_vx_MULTI_shl1s16, */
 "shr1s16",          /* TOP_dyn_vx_MULTI_shr1s16, */
 "shr7s16s8rc",      /* TOP_dyn_vx_MULTI_shr7s16s8rc, */
 "shr7s16s8rs",      /* TOP_dyn_vx_MULTI_shr7s16s8rs, */
 "shrrs16s8",        /* TOP_dyn_vx_MULTI_shrrs16s8, */
 "shrrs16s8r",       /* TOP_dyn_vx_MULTI_shrrs16s8r, */
 "shrs16s8",         /* TOP_dyn_vx_MULTI_shrs16s8, */
 "shrs16s8r",        /* TOP_dyn_vx_MULTI_shrs16s8r, */
 "subs16",           /* TOP_dyn_vx_MULTI_subs16, */
 "subs16shr",        /* TOP_dyn_vx_MULTI_subs16shr, */
 "subs16shrr"        /* TOP_dyn_vx_MULTI_subs16shrr */
};


static const char *asmname(TOP topcode) 
{ 
  return mnemonic_names[topcode]; 
} 

#define MS "[MS]"
#define OS "[OS]"
#define PS "[PS]"
#define TB "[TAB]"

  /* YJ => pretty printing */
#define WITHOUT_GUARD "     "
#define WITH_G7       "%6s"
#define WITH_QUEST_GUARD    "%5s" OS "?" OS

#define NO_OPND "\"\""

int
main(int argc, char *argv[])
{

  Set_Dynamic("vx" /* extension name */);

  ISA_Syntax_Begin(); 

  Set_AsmName_Func(asmname); 

  Set_Mandatory_Space(MS);
  Set_Optional_Space(OS);
  Set_Optional_Printed_Space(PS);
  Set_Tabulation(TB);

  ISA_Parse_Rules (".Port: {\n"
		   "  /* Array defining extension ports used */\n"
		   "  static unsigned int ExtPort[] = {0,0,0,0,0,1,1,0,0};\n"
		   "  \n"
		   "  /* String defining port used */\n"
		   "  #define DEF_EXT_ID_STR \"4,5\"\n"
		   "}\n",
		   ISA_SUBSET_vx);



  /* ================================= */ 
  /* 1 result, 2 operands */
  ISA_SYNTAX_RULE rule_0 =  ISA_Syntax_Rule_Create("rule_0");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(1) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(1) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_0",rule_0,
                          TOP_dyn_vx_absd,
                          TOP_dyn_vx_absdmin,
                          TOP_dyn_vx_absdmax,
                          TOP_dyn_vx_addu16shr6,
                          TOP_dyn_vx_cplsb,
                          TOP_dyn_vx_cplsbi,
                          TOP_dyn_vx_fir3edge,
                          TOP_dyn_vx_intlvb,
                          TOP_dyn_vx_m2rub,
                          TOP_dyn_vx_m2ruhw,
                          TOP_dyn_vx_m2x,
                          TOP_dyn_vx_offset,
                          TOP_dyn_vx_m2d,
                          TOP_dyn_vx_absdhm1,
                          TOP_dyn_vx_absdhp1,
                          TOP_dyn_vx_addu16m1,
                          TOP_dyn_vx_addu8clp63,
                          TOP_dyn_vx_addvu8u8,
                          TOP_dyn_vx_cmpgtru8,
                          TOP_dyn_vx_cpmsb,
                          TOP_dyn_vx_cpmsbr,
                          TOP_dyn_vx_cpmsbir,
                          TOP_dyn_vx_fir3,
                          TOP_dyn_vx_max,
                          TOP_dyn_vx_maxh3,
                          TOP_dyn_vx_mpru8u8,
                          TOP_dyn_vx_mpvu8u8,
                          TOP_dyn_vx_mpviu8u8,
                          (TOP)-1);

  /* ================================= */ 
  /* 1 result, 3 operands */
  ISA_SYNTAX_RULE rule_1 =  ISA_Syntax_Rule_Create("rule_1");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_1",rule_1,
                          TOP_dyn_vx_bshr,
                          TOP_dyn_vx_scales8s9,
                          (TOP)-1);

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_2 = ISA_Syntax_Rule_Create("rule_2");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s, @(" PS "%s" PS "!+" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s, @(" PS "%s" PS "!+" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax_Group("O_2", rule_2, 
		 TOP_dyn_vx_ld_r_post_inc, 
		 TOP_dyn_vx_ld_i5_post_inc, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_3 = ISA_Syntax_Rule_Create("rule_3");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s, @(" PS "%s" PS "!-" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s, @(" PS "%s" PS "!-" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax_Group("O_3", rule_3, 
		 TOP_dyn_vx_ld_i5_post_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_4 = ISA_Syntax_Rule_Create("rule_4");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s, @(" PS "%s" PS "+" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Operand(1); 
  Operand(2); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s, @(" PS "%s" PS "+" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Operand(1); 
  Operand(2); 

  ISA_Syntax_Group("O_4", rule_4, 
		 TOP_dyn_vx_ld_r_inc, 
		 TOP_dyn_vx_ld_i12_inc, 
		 TOP_dyn_vx_ld_i8_inc, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_5 = ISA_Syntax_Rule_Create("rule_5");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s, @(" PS "%s" PS "-" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Operand(1); 
  Operand(2); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s, @(" PS "%s" PS "-" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Operand(1); 
  Operand(2); 

  ISA_Syntax_Group("O_5", rule_5, 
		 TOP_dyn_vx_ld_i8_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_6 = ISA_Syntax_Rule_Create("rule_6");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s, @(" PS "%s" PS "-!" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s, @(" PS "%s" PS "-!" PS "%s" PS ")"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Result(1); 
  Operand(2); 

  ISA_Syntax_Group("O_6", rule_6, 
		 TOP_dyn_vx_ld_i5_pre_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_7 = ISA_Syntax_Rule_Create("rule_7");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "@(" PS "%s" PS "!+" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "@(" PS "%s" PS "!+" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax_Group("O_7", rule_7, 
		 TOP_dyn_vx_sd_r_post_inc, 
		 TOP_dyn_vx_sd_i5_post_inc, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_9 = ISA_Syntax_Rule_Create("rule_9");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "@(" PS "%s" PS "+" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Operand(1); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "@(" PS "%s" PS "+" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Operand(1); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax_Group("O_9", rule_9, 
		 TOP_dyn_vx_sd_r_inc, 
		 TOP_dyn_vx_sd_i12_inc, 
		 TOP_dyn_vx_sd_i8_inc, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_10 = ISA_Syntax_Rule_Create("rule_10");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "@(" PS "%s" PS "-" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Operand(1); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "@(" PS "%s" PS "-" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Operand(1); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax_Group("O_10", rule_10, 
		 TOP_dyn_vx_sd_i8_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_11 = ISA_Syntax_Rule_Create("rule_11");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "@(" PS "%s" PS "-!" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "@(" PS "%s" PS "-!" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax_Group("O_11", rule_11, 
		 TOP_dyn_vx_sd_i5_pre_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_12 = ISA_Syntax_Rule_Create("rule_12");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "@(" PS "%s" PS "!-" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax(WITH_G7 TB "%s" MS TB "@(" PS "%s" PS "!-" PS "%s" PS ")" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest"); 
  Name(); 
  Result(0); 
  Operand(2); 
  Operand(3); 

  ISA_Syntax_Group("O_12", rule_12, 
		 TOP_dyn_vx_sd_i5_post_dec, 
		 (TOP)-1); 

  /* ================================= */ 
  ISA_SYNTAX_RULE rule_13 =  ISA_Syntax_Rule_Create("rule_13");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_13",rule_13,
                          TOP_dyn_vx_m2rw0,
                          TOP_dyn_vx_m2rw1,
                          TOP_dyn_vx_m2v,
                          TOP_dyn_vx_modand,
                          TOP_dyn_vx_random,
                          TOP_dyn_vx_clp63,
                          TOP_dyn_vx_maxpair,
                          TOP_dyn_vx_sub128,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_14 =  ISA_Syntax_Rule_Create("rule_14",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
//   Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
//   Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_14",rule_14,
                          TOP_dyn_vx_MULTI_absdmax,
                          TOP_dyn_vx_MULTI_absdmin,
                          TOP_dyn_vx_MULTI_addu16m1,
                          TOP_dyn_vx_MULTI_fir3,
                          TOP_dyn_vx_MULTI_maxh3,
                          (TOP)-1);


  /* ================================= */ 

  ISA_SYNTAX_RULE rule_15 =  ISA_Syntax_Rule_Create("rule_15");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);

  ISA_Syntax_Group("O_15",rule_15,
                          TOP_dyn_vx_acc,
                          TOP_dyn_vx_m2xw0,
                          TOP_dyn_vx_m2xw1,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_16 =  ISA_Syntax_Rule_Create("rule_16");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_16",rule_16,
                          TOP_dyn_vx_m2xb,
                          TOP_dyn_vx_m2xhw,
                          TOP_dyn_vx_m2xshldb,
                          TOP_dyn_vx_addu16m2,
                          TOP_dyn_vx_maru8s8shr7r,
                          TOP_dyn_vx_maru8u8shr7r,
                          TOP_dyn_vx_maviu8u8shr7r,
                          TOP_dyn_vx_mavu8u8shr7r,
                          TOP_dyn_vx_select,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_17 =  ISA_Syntax_Rule_Create("rule_17",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_17",rule_17,
                          TOP_dyn_vx_MULTI_maru8s8shr7r,
                          TOP_dyn_vx_MULTI_maru8u8shr7r,
                          TOP_dyn_vx_MULTI_maviu8u8shr7r,
                          TOP_dyn_vx_MULTI_mavu8u8shr7r,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_18 =  ISA_Syntax_Rule_Create("rule_18");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_18",rule_18,
                          TOP_dyn_vx_firu8s8p,
                          TOP_dyn_vx_incgth3,
                          TOP_dyn_vx_incinsu16,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_19 =  ISA_Syntax_Rule_Create("rule_19",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax_Group("O_19",rule_19,
                          TOP_dyn_vx_MULTI_firu8s8p,
                          (TOP)-1);

  /* ================================= */ 

  ISA_SYNTAX_RULE rule_20 =  ISA_Syntax_Rule_Create("rule_20",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(4);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as result 0
  Operand(2);
  Operand(4);
  Operand(5);

  ISA_Syntax_Group("O_20",rule_20,
                          TOP_dyn_vx_MULTI_incgth3,
                          TOP_dyn_vx_MULTI_incinsu16,
                          (TOP)-1);

 /* ================================= */ 

  ISA_SYNTAX_RULE rule_21 =  ISA_Syntax_Rule_Create("rule_21");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_21",rule_21,
                          TOP_dyn_vx_incgth3clr,
                          (TOP)-1);

 /* ================================= */ 

  ISA_SYNTAX_RULE rule_22 =  ISA_Syntax_Rule_Create("rule_22",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_22",rule_22,
                          TOP_dyn_vx_MULTI_incgth3clr,
                          (TOP)-1);

 /* ================================= */ 

  ISA_SYNTAX_RULE rule_23 =  ISA_Syntax_Rule_Create("rule_23",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_23",rule_23,
                          TOP_dyn_vx_MULTI_addu16m2,
                          (TOP)-1);

 /* ================================= */ 

  ISA_SYNTAX_RULE rule_24 =  ISA_Syntax_Rule_Create("rule_24");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_24",rule_24,
			  TOP_dyn_vx_absdmpslt,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_25 =  ISA_Syntax_Rule_Create("rule_25",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax_Group("O_25",rule_25,
			  TOP_dyn_vx_MULTI_absdmpslt,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_26 =  ISA_Syntax_Rule_Create("rule_26");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_26",rule_26,
			  TOP_dyn_vx_absdslt,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_27 =  ISA_Syntax_Rule_Create("rule_27",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_27",rule_27,
			  TOP_dyn_vx_MULTI_absdslt,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_28 =  ISA_Syntax_Rule_Create("rule_28");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_28",rule_28,
			  TOP_dyn_vx_adds16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_29 =  ISA_Syntax_Rule_Create("rule_29",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_29",rule_29,
			  TOP_dyn_vx_MULTI_adds16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_30 =  ISA_Syntax_Rule_Create("rule_30");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_30",rule_30,
			  TOP_dyn_vx_adds16shr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_31 =  ISA_Syntax_Rule_Create("rule_31",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_31",rule_31,
			  TOP_dyn_vx_MULTI_adds16shr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_32 =  ISA_Syntax_Rule_Create("rule_32");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_32",rule_32,
			  TOP_dyn_vx_adds16shrr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_33 =  ISA_Syntax_Rule_Create("rule_33",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_33",rule_33,
			  TOP_dyn_vx_MULTI_adds16shrr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_34 =  ISA_Syntax_Rule_Create("rule_34");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_34",rule_34,
			  TOP_dyn_vx_ascmf,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_35 =  ISA_Syntax_Rule_Create("rule_35",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_35",rule_35,
			  TOP_dyn_vx_MULTI_ascmf,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_36 =  ISA_Syntax_Rule_Create("rule_36");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_36",rule_36,
			  TOP_dyn_vx_ascmfr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_37 =  ISA_Syntax_Rule_Create("rule_37",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_37",rule_37,
			  TOP_dyn_vx_MULTI_ascmfr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_38 =  ISA_Syntax_Rule_Create("rule_38");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_38",rule_38,
			  TOP_dyn_vx_bshrr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_39 =  ISA_Syntax_Rule_Create("rule_39");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_39",rule_39,
			  TOP_dyn_vx_clpsym,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_40 =  ISA_Syntax_Rule_Create("rule_40",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_40",rule_40,
			  TOP_dyn_vx_MULTI_clpsym,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_41 =  ISA_Syntax_Rule_Create("rule_41");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_41",rule_41,
			  TOP_dyn_vx_clpu8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_42 =  ISA_Syntax_Rule_Create("rule_42");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);

  ISA_Syntax_Group("O_42",rule_42,
			  TOP_dyn_vx_clr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_43 =  ISA_Syntax_Rule_Create("rule_43");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_43",rule_43,
			  TOP_dyn_vx_cmpeqru8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_44 =  ISA_Syntax_Rule_Create("rule_44");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_44",rule_44,
			  TOP_dyn_vx_cmpgeru8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_45 =  ISA_Syntax_Rule_Create("rule_45");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_45",rule_45,
			  TOP_dyn_vx_cmpleru8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_46 =  ISA_Syntax_Rule_Create("rule_46");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_46",rule_46,
			  TOP_dyn_vx_cmpltru8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_47 =  ISA_Syntax_Rule_Create("rule_47");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_47",rule_47,
			  TOP_dyn_vx_cmpneru8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_48 =  ISA_Syntax_Rule_Create("rule_48");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_48",rule_48,
			  TOP_dyn_vx_cntdelta0clr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_49 =  ISA_Syntax_Rule_Create("rule_49",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s%s" OS "," PS "%s%s" OS "," PS "%s%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *res0 = GETOPND(0);\n"
			  "  AIR_TN *res1 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(res0));\n"
			  "  AIR_TN *opnd3 = GETOPND(1);\n"
			  "  AIR_TN *opnd4 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(opnd3));\n"
			  "  AIR_TN *opnd5 = GETOPND(2);\n"
			  "  AIR_TN *opnd6 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(opnd5));\n");

  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Result(1,NO_OPND,"res1");
  //  Operand(2); Same as res 1
  Operand(3);
  Operand(4,NO_OPND,"opnd4");
  Operand(5);
  Operand(6,NO_OPND,"opnd6");

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s%s" OS "," PS "%s%s" OS "," PS "%s%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7");
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);\n"
			  "  AIR_TN *res0 = GETOPND(0);\n"
			  "  AIR_TN *res1 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(res0));\n"
			  "  AIR_TN *opnd3 = GETOPND(0);\n"
			  "  AIR_TN *opnd4 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(opnd3));\n"
			  "  AIR_TN *opnd5 = GETOPND(0);\n"
			  "  AIR_TN *opnd6 = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_vx_vr,\n"
			  "                                 ISA_REGISTER_SUBCLASS_vx_dv_P1,\n"
			  "                                 Get_AIR_TN_reg_regnum(opnd5));\n");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Result(1,NO_OPND,"res1");
  //  Operand(2); Same as res 1
  Operand(3);
  Operand(4,NO_OPND,"opnd4");
  Operand(5);
  Operand(6,NO_OPND,"opnd6");

  ISA_Syntax_Group("O_49",rule_49,
			  TOP_dyn_vx_MULTI_cntdelta0clr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_50 =  ISA_Syntax_Rule_Create("rule_50");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_50",rule_50,
			  TOP_dyn_vx_cntdeltabw,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_51 =  ISA_Syntax_Rule_Create("rule_51",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax_Group("O_51",rule_51,
			  TOP_dyn_vx_MULTI_cntdeltabw,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_52 =  ISA_Syntax_Rule_Create("rule_52");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_52",rule_52,
			  TOP_dyn_vx_cntdeltafw,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_53 =  ISA_Syntax_Rule_Create("rule_53",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax_Group("O_53",rule_53,
			  TOP_dyn_vx_MULTI_cntdeltafw,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_54 =  ISA_Syntax_Rule_Create("rule_54");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_54",rule_54,
			  TOP_dyn_vx_cpmsbi,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_55 =  ISA_Syntax_Rule_Create("rule_55");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_55",rule_55,
			  TOP_dyn_vx_dintlvb,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_56 =  ISA_Syntax_Rule_Create("rule_56",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_56",rule_56,
			  TOP_dyn_vx_MULTI_dintlvb,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_57 =  ISA_Syntax_Rule_Create("rule_57");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_57",rule_57,
			  TOP_dyn_vx_dlupdate,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_58 =  ISA_Syntax_Rule_Create("rule_58",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_58",rule_58,
			  TOP_dyn_vx_MULTI_dlupdate,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_59 =  ISA_Syntax_Rule_Create("rule_59");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_59",rule_59,
			  TOP_dyn_vx_firu8s8p1clr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_60 =  ISA_Syntax_Rule_Create("rule_60",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_60",rule_60,
			  TOP_dyn_vx_MULTI_firu8s8p1clr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_61 =  ISA_Syntax_Rule_Create("rule_61");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_61",rule_61,
			  TOP_dyn_vx_getsad0,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_62 =  ISA_Syntax_Rule_Create("rule_62",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_62",rule_62,
			  TOP_dyn_vx_MULTI_getsad0,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_63 =  ISA_Syntax_Rule_Create("rule_63");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_63",rule_63,
			  TOP_dyn_vx_incgt,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_64 =  ISA_Syntax_Rule_Create("rule_64");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_64",rule_64,
			  TOP_dyn_vx_insmean,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_65 =  ISA_Syntax_Rule_Create("rule_65",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_65",rule_65,
			  TOP_dyn_vx_MULTI_insmean,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_66 =  ISA_Syntax_Rule_Create("rule_66");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_66",rule_66,
			  TOP_dyn_vx_insmeanr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_67 =  ISA_Syntax_Rule_Create("rule_67",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_67",rule_67,
			  TOP_dyn_vx_MULTI_insmeanr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_68 =  ISA_Syntax_Rule_Create("rule_68");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_68",rule_68,
			  TOP_dyn_vx_maru8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_69 =  ISA_Syntax_Rule_Create("rule_69",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_69",rule_69,
			  TOP_dyn_vx_MULTI_maru8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_70 =  ISA_Syntax_Rule_Create("rule_70");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_70",rule_70,
			  TOP_dyn_vx_maru8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_71 =  ISA_Syntax_Rule_Create("rule_71",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_71",rule_71,
			  TOP_dyn_vx_MULTI_maru8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_72 =  ISA_Syntax_Rule_Create("rule_72");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_72",rule_72,
			  TOP_dyn_vx_maru8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_73 =  ISA_Syntax_Rule_Create("rule_73",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_73",rule_73,
			  TOP_dyn_vx_MULTI_maru8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_74 =  ISA_Syntax_Rule_Create("rule_74");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_74",rule_74,
			  TOP_dyn_vx_maru8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_75 =  ISA_Syntax_Rule_Create("rule_75",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_75",rule_75,
			  TOP_dyn_vx_MULTI_maru8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_76 =  ISA_Syntax_Rule_Create("rule_76");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_76",rule_76,
			  TOP_dyn_vx_maviu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_77 =  ISA_Syntax_Rule_Create("rule_77",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_77",rule_77,
			  TOP_dyn_vx_MULTI_maviu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_78 =  ISA_Syntax_Rule_Create("rule_78");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_78",rule_78,
			  TOP_dyn_vx_maviu8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_79 =  ISA_Syntax_Rule_Create("rule_79",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_79",rule_79,
			  TOP_dyn_vx_MULTI_maviu8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_80 =  ISA_Syntax_Rule_Create("rule_80");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_80",rule_80,
			  TOP_dyn_vx_maviu8s8shr7r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_81 =  ISA_Syntax_Rule_Create("rule_81",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_81",rule_81,
			  TOP_dyn_vx_MULTI_maviu8s8shr7r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_82 =  ISA_Syntax_Rule_Create("rule_82");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_82",rule_82,
			  TOP_dyn_vx_maviu8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_83 =  ISA_Syntax_Rule_Create("rule_83",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_83",rule_83,
			  TOP_dyn_vx_MULTI_maviu8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_84 =  ISA_Syntax_Rule_Create("rule_84");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_84",rule_84,
			  TOP_dyn_vx_maviu8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_85 =  ISA_Syntax_Rule_Create("rule_85",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_85",rule_85,
			  TOP_dyn_vx_MULTI_maviu8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_86 =  ISA_Syntax_Rule_Create("rule_86");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_86",rule_86,
			  TOP_dyn_vx_mavu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_87 =  ISA_Syntax_Rule_Create("rule_87",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_87",rule_87,
			  TOP_dyn_vx_MULTI_mavu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_88 =  ISA_Syntax_Rule_Create("rule_88");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_88",rule_88,
			  TOP_dyn_vx_mavu8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_89 =  ISA_Syntax_Rule_Create("rule_89",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_89",rule_89,
			  TOP_dyn_vx_MULTI_mavu8s8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_90 =  ISA_Syntax_Rule_Create("rule_90");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_90",rule_90,
			  TOP_dyn_vx_mavu8s8shr7r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_91 =  ISA_Syntax_Rule_Create("rule_91",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_91",rule_91,
			  TOP_dyn_vx_MULTI_mavu8s8shr7r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_92 =  ISA_Syntax_Rule_Create("rule_92");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_92",rule_92,
			  TOP_dyn_vx_mavu8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_93 =  ISA_Syntax_Rule_Create("rule_93",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_93",rule_93,
			  TOP_dyn_vx_MULTI_mavu8u8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_94 =  ISA_Syntax_Rule_Create("rule_94");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_94",rule_94,
			  TOP_dyn_vx_mavu8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_95 =  ISA_Syntax_Rule_Create("rule_95",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_95",rule_95,
			  TOP_dyn_vx_MULTI_mavu8u8shr7,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_96 =  ISA_Syntax_Rule_Create("rule_96");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_96",rule_96,
			  TOP_dyn_vx_mean,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_97 =  ISA_Syntax_Rule_Create("rule_97");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_97",rule_97,
			  TOP_dyn_vx_meanr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_98 =  ISA_Syntax_Rule_Create("rule_98");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_98",rule_98,
			  TOP_dyn_vx_meanuv,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_99 =  ISA_Syntax_Rule_Create("rule_99",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_99",rule_99,
			  TOP_dyn_vx_MULTI_meanuv,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_100 =  ISA_Syntax_Rule_Create("rule_100");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_100",rule_100,
			  TOP_dyn_vx_meanuvr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_101 =  ISA_Syntax_Rule_Create("rule_101",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_101",rule_101,
			  TOP_dyn_vx_MULTI_meanuvr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_102 =  ISA_Syntax_Rule_Create("rule_102");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_102",rule_102,
			  TOP_dyn_vx_meany,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_103 =  ISA_Syntax_Rule_Create("rule_103",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_103",rule_103,
			  TOP_dyn_vx_MULTI_meany,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_104 =  ISA_Syntax_Rule_Create("rule_104");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_104",rule_104,
			  TOP_dyn_vx_meanyr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_105 =  ISA_Syntax_Rule_Create("rule_105",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_105",rule_105,
			  TOP_dyn_vx_MULTI_meanyr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_106 =  ISA_Syntax_Rule_Create("rule_106");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_106",rule_106,
			  TOP_dyn_vx_median,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_107 =  ISA_Syntax_Rule_Create("rule_107",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_107",rule_107,
			  TOP_dyn_vx_MULTI_median,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_108 =  ISA_Syntax_Rule_Create("rule_108");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_108",rule_108,
			  TOP_dyn_vx_mf,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_109 =  ISA_Syntax_Rule_Create("rule_109");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_109",rule_109,
			  TOP_dyn_vx_mfr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_110 =  ISA_Syntax_Rule_Create("rule_110");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_110",rule_110,
			  TOP_dyn_vx_min,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_111 =  ISA_Syntax_Rule_Create("rule_111");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_111",rule_111,
			  TOP_dyn_vx_minsad,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_112 =  ISA_Syntax_Rule_Create("rule_112",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_112",rule_112,
			  TOP_dyn_vx_MULTI_minsad,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_113 =  ISA_Syntax_Rule_Create("rule_113");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_113",rule_113,
			  TOP_dyn_vx_mpru8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_114 =  ISA_Syntax_Rule_Create("rule_114",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_114",rule_114,
			  TOP_dyn_vx_MULTI_mpru8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_115 =  ISA_Syntax_Rule_Create("rule_115");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_115",rule_115,
			  TOP_dyn_vx_mpu8u8shr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_116 =  ISA_Syntax_Rule_Create("rule_116");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_116",rule_116,
			  TOP_dyn_vx_mpviu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_117 =  ISA_Syntax_Rule_Create("rule_117",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_117",rule_117,
			  TOP_dyn_vx_MULTI_mpviu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_118 =  ISA_Syntax_Rule_Create("rule_118");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_118",rule_118,
			  TOP_dyn_vx_mpvu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_119 =  ISA_Syntax_Rule_Create("rule_119",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_119",rule_119,
			  TOP_dyn_vx_MULTI_mpvu8s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_120 =  ISA_Syntax_Rule_Create("rule_120");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_120",rule_120,
			  TOP_dyn_vx_sad,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_121 =  ISA_Syntax_Rule_Create("rule_121",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_121",rule_121,
			  TOP_dyn_vx_MULTI_sad,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_122 =  ISA_Syntax_Rule_Create("rule_122");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(2);
  Operand(3);
  Operand(4);

  ISA_Syntax_Group("O_122",rule_122,
			  TOP_dyn_vx_sadmin,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_123 =  ISA_Syntax_Rule_Create("rule_123",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  //  Operand(1); Same as res 0
  Operand(3);
  Operand(5);
  Operand(7);

  ISA_Syntax_Group("O_123",rule_123,
			  TOP_dyn_vx_MULTI_sadmin,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_124 =  ISA_Syntax_Rule_Create("rule_124");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_124",rule_124,
			  TOP_dyn_vx_shl1s16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_125 =  ISA_Syntax_Rule_Create("rule_125",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_125",rule_125,
			  TOP_dyn_vx_MULTI_shl1s16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_126 =  ISA_Syntax_Rule_Create("rule_126");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_126",rule_126,
			  TOP_dyn_vx_shr1s16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_127 =  ISA_Syntax_Rule_Create("rule_127",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_127",rule_127,
			  TOP_dyn_vx_MULTI_shr1s16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_128 =  ISA_Syntax_Rule_Create("rule_128");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_128",rule_128,
			  TOP_dyn_vx_shr7s16s8rc,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_129 =  ISA_Syntax_Rule_Create("rule_129",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_129",rule_129,
			  TOP_dyn_vx_MULTI_shr7s16s8rc,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_130 =  ISA_Syntax_Rule_Create("rule_130");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_130",rule_130,
			  TOP_dyn_vx_shr7s16s8rs,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_131 =  ISA_Syntax_Rule_Create("rule_131",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);

  ISA_Syntax_Group("O_131",rule_131,
			  TOP_dyn_vx_MULTI_shr7s16s8rs,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_132 =  ISA_Syntax_Rule_Create("rule_132");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_132",rule_132,
			  TOP_dyn_vx_shrrs16s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_133 =  ISA_Syntax_Rule_Create("rule_133",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_133",rule_133,
			  TOP_dyn_vx_MULTI_shrrs16s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_134 =  ISA_Syntax_Rule_Create("rule_134");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_134",rule_134,
			  TOP_dyn_vx_shrrs16s8r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_135 =  ISA_Syntax_Rule_Create("rule_135",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_135",rule_135,
			  TOP_dyn_vx_MULTI_shrrs16s8r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_136 =  ISA_Syntax_Rule_Create("rule_136");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_136",rule_136,
			  TOP_dyn_vx_shrs16s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_137 =  ISA_Syntax_Rule_Create("rule_137",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_137",rule_137,
			  TOP_dyn_vx_MULTI_shrs16s8,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_138 =  ISA_Syntax_Rule_Create("rule_138");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_138",rule_138,
			  TOP_dyn_vx_shrs16s8r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_139 =  ISA_Syntax_Rule_Create("rule_139",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_139",rule_139,
			  TOP_dyn_vx_MULTI_shrs16s8r,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_140 =  ISA_Syntax_Rule_Create("rule_140");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_140",rule_140,
			  TOP_dyn_vx_subs16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_141 =  ISA_Syntax_Rule_Create("rule_141",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);

  ISA_Syntax_Group("O_141",rule_141,
			  TOP_dyn_vx_MULTI_subs16,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_142 =  ISA_Syntax_Rule_Create("rule_142");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_142",rule_142,
			  TOP_dyn_vx_subs16shr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_143 =  ISA_Syntax_Rule_Create("rule_143",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_143",rule_143,
			  TOP_dyn_vx_MULTI_subs16shr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_144 =  ISA_Syntax_Rule_Create("rule_144");

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);
  Operand(3);

  ISA_Syntax_Group("O_144",rule_144,
			  TOP_dyn_vx_subs16shrr,
                          (TOP)-1);
 /* ================================= */ 

  ISA_SYNTAX_RULE rule_145 =  ISA_Syntax_Rule_Create("rule_145",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(0) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(3);
  Operand(5);

  ISA_Syntax_Group("O_145",rule_145,
			  TOP_dyn_vx_MULTI_subs16shrr,
                          (TOP)-1);


  /* ================================= */ 
  ISA_SYNTAX_RULE rule_146 =  ISA_Syntax_Rule_Create("rule_146",false);

  ISA_Syntax(WITH_QUEST_GUARD TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(1) != 7"); 
  Operand(0);
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax(WITH_G7 TB "%s" MS TB "%s" OS "," PS "%s" OS "," PS "%s"); 
  ISA_Syntax_Print_Condition("O_VAL(1) == 7"); 
  ISA_Syntax_Parse_Action("  AIR_TN *gx_quest = AIR_NEW_REG_TN(ISA_REGISTER_CLASS_gr,ISA_REGISTER_SUBCLASS_UNDEFINED,7);");
  Operand(0,NO_OPND,"gx_quest");
  Name();
  Result(0);
  Operand(1);
  Operand(2);

  ISA_Syntax_Group("O_146",rule_146,
                          TOP_dyn_vx_MULTI_fir3edge,
                          TOP_dyn_vx_MULTI_intlvb,
                          TOP_dyn_vx_MULTI_m2d,
                          TOP_dyn_vx_MULTI_mpru8u8,
                          TOP_dyn_vx_MULTI_mpvu8u8,
                          TOP_dyn_vx_MULTI_mpviu8u8,
                          TOP_dyn_vx_MULTI_addvu8u8,
                          (TOP)-1);



  ISA_Syntax_End();
  exit(0);
}
