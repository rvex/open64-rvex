
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "dyn_isa_topcode.h"
#include "isa_bundle_gen.h"
#include "gen_util.h"

int 
main( int argc, char *argv )
{
    unsigned int i;

    // Mandatory name.
    Set_Dynamic("vx" /* extension name */);

    ISA_Bundle_Begin("stxp70", /* Useless name */
                      32       /* Bit witdth   */);

    /* ===== Specification for ALL_Unit Type ===== */ 
    ISA_EXEC_UNIT_TYPE ALL_Unit = ISA_Exec_Unit_Type_Create("ALL_Unit", NULL); 

    /* STxP70 is a scalar processor. Hence the code*/
    for(i=0;i<TOP_dyn_count;i++)
      Instruction_Exec_Unit_Group(ALL_Unit,i,-1);

    ISA_Bundle_End();
    exit(0);
}
