
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "isa_gen.h"
#include "gen_util.h"

// Note: The following load/store instructions are not modelized in targinfo,
//       because they cannot be automatically generated (they do not comply
//       to extension rules): lwh, lwl, swh, swl
//       They can only be generated through assembly statement, without
//       automodified addressing mode.

int 
main( int argc, char *argv[] )
{ 
  // Mandatory name.
  Set_Dynamic("vx" /* extension name */);

  // A small subset of instruction set.
  ISA_Create("stxp70",

             "absd",               /* p 28 */
             "absdmax",            /* p 31 */
             "absdmin",            /* p 32 */
             "addu16shr6",         /* p */
             "bshr",               /* p 51 */
             "cplsb",              /* p 65 */
             "cplsbi",             /* p 66 */
             "fir3edge",           /* p  */
             "intlvb",             /* p  */
             "m2rub",              /* p  */
             "m2ruhw",             /* p  */
             "m2rw0",              /* p  */
             "m2rw1",              /* p  */
             "m2x",                /* p  */
             "m2v",                /* p  */
             "modand",             /* p  */
             "offset",             /* p  */
             "random",             /* p  */
             "scales8s9",          /* p  */
             "ld_r_post_inc",      /* p  */
             "ld_i5_post_inc",     /* p  */
             "ld_r_inc",           /* p  */
             "ld_i12_inc",         /* p  */
             "ld_i8_inc",          /* p  */
             "ld_i5_post_dec",     /* p  */
             "ld_i5_pre_dec",      /* p  */
             "ld_i8_dec",          /* p  */
             "sd_r_post_inc",      /* p  */
             "sd_i5_post_inc",     /* p  */
             "sd_r_inc",           /* p  */
             "sd_i12_inc",         /* p  */
             "sd_i8_inc",          /* p  */
             "sd_i5_post_dec",     /* p  */
             "sd_i5_pre_dec",      /* p  */
             "sd_i8_dec",          /* p  */   
             "COMPOSE_V_P2X",      /* simulated op */   
             "EXTRACT_V_X2P",      /* simulated op */
             "WIDEMOVE_V_X2X",     /* simulated op */
             "MULTI_absdmax",      /* op Multi */   
             "MULTI_absdmin",      /* op Multi */   
             "MULTI_fir3edge",     /* op Multi */   
             "MULTI_intlvb",       /* op Multi */   
	     "acc", 	           /* p 37 */
	     "m2d", 	           /* p 97 */
	     "MULTI_m2d",          /* op MULTI */
	     "m2xb", 	           /* p 103 */
	     "m2xhw", 	           /* p 104 */
	     "m2xshldb",           /* p 105 */	
	     "m2xw0",              /* p 106 */
	     "m2xw1",              /* p 106 */
	     "absdhm1",            /* */
	     "absdhp1",            /* */
	     "addu16m1",           /* */
	     "addu16m2",           /* */
	     "addu8clp63",         /* */
	     "addvu8u8",           /* */
	     "clp63",              /* */
	     "cmpgtru8",           /* */
	     "cpmsb",              /* */
	     "cpmsbr",             /* */
	     "cpmsbir",            /* */
	     "fir3",               /* */
	     "firu8s8p",           /* p 78 */
	     "incgth3",            /* */
	     "incgth3clr",         /* */
	     "incinsu16",          /* */
	     "maru8s8shr7r",       /* p 109 */
	     "maru8u8shr7r",       /* */
	     "maviu8u8shr7r",      /* */
	     "mavu8u8shr7r",       /* */
	     "max",                /* */
	     "maxh3",              /* */
	     "maxpair",            /* */
	     "mpru8u8",            /* */
	     "mpvu8u8",            /* */
	     "mpviu8u8",           /* */
	     "select",             /* */
	     "sub128",             /* */
	     "MULTI_addu16m1",     /* */
	     "MULTI_addu16m2",     /* */
	     "MULTI_addvu8u8",     /* */
	     "MULTI_fir3",         /* */
	     "MULTI_firu8s8p",     /* */
	     "MULTI_incgth3",      /* */
	     "MULTI_incgth3clr",   /* */
	     "MULTI_incinsu16",    /* */
	     "MULTI_maru8s8shr7r", /* */
	     "MULTI_maru8u8shr7r", /* */
	     "MULTI_maviu8u8shr7r",/* */
	     "MULTI_mavu8u8shr7r", /* */
	     "MULTI_maxh3",        /* */
	     "MULTI_mpru8u8",      /* */
	     "MULTI_mpvu8u8",      /* */
	     "MULTI_mpviu8u8",     /* */
	     "absdmpslt",          /* p 33 */
	     "absdslt",            /* p 35 */
	     "adds16",             /* p 38 */
	     "adds16shr",          /* p 40 */
	     "adds16shrr",         /* p 40 */
	     "ascmf",              /* p 49 */
	     "ascmfr",             /* p 49 */
	     "bshrr",              /* p 52 */
	     "clpsym",             /* p 54 */
	     "clpu8",              /* p 56 */
	     "clr",                /* p 57 */
	     "cmpeqru8",           /* p 58 */
	     "cmpneru8",           /* p 58 */
	     "cmpgeru8",           /* p 58 */
	     "cmpltru8",           /* p 58 */
	     "cmpleru8",           /* p 58 */
	     "cntdelta0clr",       /* p 59 */
	     "cntdeltabw",         /* p 61 */
	     "cntdeltafw",         /* p 63 */
	     "cpmsbi",             /* p 68 */
	     "dintlvb",            /* p 71 */
	     "dlupdate",           /* p 72 */
	     "firu8s8p1clr",       /* p 80 */
	     "getsad0",            /* p 82 */
	     "incgt",              /* p 84 */
	     "insmean",            /* p 91 */
	     "insmeanr",           /* p 91 */
	     "maru8s8",            /* p 107 */
	     "maru8s8shr7",        /* p 109 */
	     "maru8u8",            /* p 111 */
	     "maru8u8shr7",        /* p 113 */
	     "maviu8s8",           /* p 115 */
	     "maviu8s8shr7",       /* p 117 */
	     "maviu8s8shr7r",      /* p 117 */
	     "maviu8u8",           /* p 119 */
	     "maviu8u8shr7",       /* p 121 */
	     "mavu8s8",            /* p 123 */
	     "mavu8s8shr7",        /* p 125 */
	     "mavu8s8shr7r",       /* p 125 */
	     "mavu8u8",            /* p 127 */
	     "mavu8u8shr7",        /* p 129 */
	     "mean",               /* p 134 */
	     "meanr",              /* p 134 */
	     "meanuv",             /* p 135 */
	     "meanuvr",            /* p 135 */
	     "meany",              /* p 138 */
	     "meanyr",             /* p 138 */
	     "median",             /* p 141 */
	     "mf",                 /* p 142 */
	     "mfr",                /* p 142 */
	     "min",                /* p 144 */
	     "minsad",             /* p 145 */
	     "mpru8s8",            /* p 148 */
	     "mpu8u8shr",          /* p 150 */
	     "mpviu8s8",           /* p 151 */
	     "mpvu8s8",            /* p 155 */
	     "sad",                /* p 159 */
	     "sadmin",             /* p 163 */
	     "shl1s16",            /* p 171 */
	     "shr1s16",            /* p 172 */
	     "shr7s16s8rc",        /* p 173 */
	     "shr7s16s8rs",        /* p 173 */
	     "shrrs16s8",          /* p 175 */
	     "shrrs16s8r",         /* p 175 */
	     "shrs16s8",           /* p 177 */
	     "shrs16s8r",          /* p 177 */
	     "subs16",             /* p 180 */
	     "subs16shr",          /* p 182 */
	     "subs16shrr",         /* p 182 */
	     "MULTI_absdmpslt",    /* p 33 */
	     "MULTI_absdslt",      /* p 35 */
	     "MULTI_adds16",       /* p 38 */
	     "MULTI_adds16shr",    /* p 40 */
	     "MULTI_adds16shrr",   /* p 40 */
	     "MULTI_ascmf",        /* p 49 */
	     "MULTI_ascmfr",       /* p 49 */
	     "MULTI_clpsym",       /* p 54 */
	     "MULTI_cntdelta0clr", /* p 59 */
	     "MULTI_cntdeltabw",   /* p 61 */
	     "MULTI_cntdeltafw",   /* p 63 */
	     "MULTI_dintlvb",      /* p 71 */
	     "MULTI_dlupdate",     /* p 72 */
	     "MULTI_firu8s8p1clr", /* p 80 */
	     "MULTI_getsad0",      /* p 82 */
	     "MULTI_insmean",      /* p 91 */
	     "MULTI_insmeanr",     /* p 91 */
	     "MULTI_maru8s8",      /* p 107 */
	     "MULTI_maru8s8shr7",  /* p 109 */
	     "MULTI_maru8u8",      /* p 111 */
	     "MULTI_maru8u8shr7",  /* p 113 */
	     "MULTI_maviu8s8",     /* p 115 */
	     "MULTI_maviu8s8shr7", /* p 117 */
	     "MULTI_maviu8s8shr7r",/* p 117 */
	     "MULTI_maviu8u8",     /* p 119 */
	     "MULTI_maviu8u8shr7", /* p 121 */
	     "MULTI_mavu8s8",      /* p 123 */
	     "MULTI_mavu8s8shr7",  /* p 125 */
	     "MULTI_mavu8s8shr7r", /* p 125 */
	     "MULTI_mavu8u8",      /* p 127 */
	     "MULTI_mavu8u8shr7",  /* p 129 */
	     "MULTI_meanuv",       /* p 135 */
	     "MULTI_meanuvr",      /* p 135 */
	     "MULTI_meany",        /* p 138 */
	     "MULTI_meanyr",       /* p 138 */
	     "MULTI_median",       /* p 141 */
	     "MULTI_minsad",       /* p 145 */
	     "MULTI_mpru8s8",      /* p 148 */
	     "MULTI_mpviu8s8",     /* p 151 */
	     "MULTI_mpvu8s8",      /* p 155 */
	     "MULTI_sad",          /* p 159 */
	     "MULTI_sadmin",       /* p 163 */
	     "MULTI_shl1s16",      /* p 171 */
	     "MULTI_shr1s16",      /* p 172 */
	     "MULTI_shr7s16s8rc",  /* p 173 */
	     "MULTI_shr7s16s8rs",  /* p 173 */
	     "MULTI_shrrs16s8",    /* p 175 */
	     "MULTI_shrrs16s8r",   /* p 175 */
	     "MULTI_shrs16s8",     /* p 177 */
	     "MULTI_shrs16s8r",    /* p 177 */
	     "MULTI_subs16",       /* p 180 */
	     "MULTI_subs16shr",    /* p 182 */
	     "MULTI_subs16shrr",   /* p 182 */
	     NULL);
             
  exit(0);
}
