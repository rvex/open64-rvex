#include <stdio.h>
#include <stdlib.h>
#include "dyn_isa_topcode.h" 
#include "gen_util.h"
#include "isa_variants_gen.h"
#include "targ_isa_variants.h"
 


int
main(int argc, char *argv[])
{
 
  Set_Dynamic("vx" /* extension name */);
 
  ISA_Variants_Begin("stxp70");

  /* Include core variant attributes */
  #include "targ_isa_variants.inc.cxx"

  /* ============================================================
     Multi variants.
     ============================================================ */
  /* Define variants for attribute: multi. */
  ISA_Instruction_Variant(TOP_dyn_vx_absdmax, TOP_dyn_vx_MULTI_absdmax, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_absdmin, TOP_dyn_vx_MULTI_absdmin, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_fir3edge, TOP_dyn_vx_MULTI_fir3edge, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_intlvb, TOP_dyn_vx_MULTI_intlvb, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_m2d, TOP_dyn_vx_MULTI_m2d, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_addu16m1, TOP_dyn_vx_MULTI_addu16m1, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_addu16m2, TOP_dyn_vx_MULTI_addu16m2, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_addvu8u8, TOP_dyn_vx_MULTI_addvu8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_fir3, TOP_dyn_vx_MULTI_fir3, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_firu8s8p, TOP_dyn_vx_MULTI_firu8s8p, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_incgth3, TOP_dyn_vx_MULTI_incgth3, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_incgth3clr, TOP_dyn_vx_MULTI_incgth3clr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_incinsu16, TOP_dyn_vx_MULTI_incinsu16, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8s8shr7r, TOP_dyn_vx_MULTI_maru8s8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8u8shr7r, TOP_dyn_vx_MULTI_maru8u8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8u8shr7r, TOP_dyn_vx_MULTI_maviu8u8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8u8shr7r, TOP_dyn_vx_MULTI_mavu8u8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maxh3, TOP_dyn_vx_MULTI_maxh3, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpru8u8, TOP_dyn_vx_MULTI_mpru8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpvu8u8, TOP_dyn_vx_MULTI_mpvu8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpviu8u8, TOP_dyn_vx_MULTI_mpviu8u8, att_multi, 0);

  ISA_Instruction_Variant(TOP_dyn_vx_absdmpslt, TOP_dyn_vx_MULTI_absdmpslt, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_absdslt, TOP_dyn_vx_MULTI_absdslt, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_adds16, TOP_dyn_vx_MULTI_adds16, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_adds16shr, TOP_dyn_vx_MULTI_adds16shr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_adds16shrr, TOP_dyn_vx_MULTI_adds16shrr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ascmf, TOP_dyn_vx_MULTI_ascmf, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ascmfr, TOP_dyn_vx_MULTI_ascmfr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_clpsym, TOP_dyn_vx_MULTI_clpsym, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_cntdelta0clr, TOP_dyn_vx_MULTI_cntdelta0clr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_cntdeltabw, TOP_dyn_vx_MULTI_cntdeltabw, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_cntdeltafw, TOP_dyn_vx_MULTI_cntdeltafw, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_dintlvb, TOP_dyn_vx_MULTI_dintlvb, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_dlupdate, TOP_dyn_vx_MULTI_dlupdate, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_firu8s8p1clr, TOP_dyn_vx_MULTI_firu8s8p1clr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_getsad0, TOP_dyn_vx_MULTI_getsad0, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_insmean, TOP_dyn_vx_MULTI_insmean, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_insmeanr, TOP_dyn_vx_MULTI_insmeanr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8s8, TOP_dyn_vx_MULTI_maru8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8s8shr7, TOP_dyn_vx_MULTI_maru8s8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8u8, TOP_dyn_vx_MULTI_maru8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maru8u8shr7, TOP_dyn_vx_MULTI_maru8u8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8s8, TOP_dyn_vx_MULTI_maviu8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8s8shr7, TOP_dyn_vx_MULTI_maviu8s8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8s8shr7r, TOP_dyn_vx_MULTI_maviu8s8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8u8, TOP_dyn_vx_MULTI_maviu8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_maviu8u8shr7, TOP_dyn_vx_MULTI_maviu8u8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8s8, TOP_dyn_vx_MULTI_mavu8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8s8shr7, TOP_dyn_vx_MULTI_mavu8s8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8s8shr7r, TOP_dyn_vx_MULTI_mavu8s8shr7r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8u8, TOP_dyn_vx_MULTI_mavu8u8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mavu8u8shr7, TOP_dyn_vx_MULTI_mavu8u8shr7, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_meany, TOP_dyn_vx_MULTI_meany, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_meanyr, TOP_dyn_vx_MULTI_meanyr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_median, TOP_dyn_vx_MULTI_median, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_minsad, TOP_dyn_vx_MULTI_minsad, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpru8s8, TOP_dyn_vx_MULTI_mpru8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpviu8s8, TOP_dyn_vx_MULTI_mpviu8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_mpvu8s8, TOP_dyn_vx_MULTI_mpvu8s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sad, TOP_dyn_vx_MULTI_sad, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sadmin, TOP_dyn_vx_MULTI_sadmin, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shl1s16, TOP_dyn_vx_MULTI_shl1s16, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shr1s16, TOP_dyn_vx_MULTI_shr1s16, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shr7s16s8rc, TOP_dyn_vx_MULTI_shr7s16s8rc, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shr7s16s8rs, TOP_dyn_vx_MULTI_shr7s16s8rs, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shrrs16s8, TOP_dyn_vx_MULTI_shrrs16s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shrrs16s8r, TOP_dyn_vx_MULTI_shrrs16s8r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shrs16s8, TOP_dyn_vx_MULTI_shrs16s8, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_shrs16s8r, TOP_dyn_vx_MULTI_shrs16s8r, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_subs16, TOP_dyn_vx_MULTI_subs16, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_subs16shr, TOP_dyn_vx_MULTI_subs16shr, att_multi, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_subs16shrr, TOP_dyn_vx_MULTI_subs16shrr, att_multi, 0);


  /* ============================================================
     Immediate variants.
     ============================================================ */
  ISA_Instruction_Variant(TOP_dyn_vx_ld_r_post_inc, TOP_dyn_vx_ld_i5_post_inc, att_immediate, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_r_post_inc, TOP_dyn_vx_sd_i5_post_inc, att_immediate, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_r_inc, TOP_dyn_vx_ld_i8_inc, att_immediate, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_r_inc, TOP_dyn_vx_sd_i8_inc, att_immediate, 0);


  /* ============================================================
     Addressing Mode variants.
     ============================================================ */
  /* Variants for attribute: negoffset (Rn - imm). */
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i8_inc, TOP_dyn_vx_ld_i8_dec, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i8_inc, TOP_dyn_vx_sd_i8_dec, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i12_inc, TOP_dyn_vx_ld_i8_dec, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i12_inc, TOP_dyn_vx_sd_i8_dec, att_addr_negoffset, 0);

  /* Variants for attribute: postincr (Rn !+ imm and Rn !+ Rp). */
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i8_inc, TOP_dyn_vx_ld_i5_post_inc, att_addr_postincr, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i8_inc, TOP_dyn_vx_sd_i5_post_inc, att_addr_postincr, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i12_inc, TOP_dyn_vx_ld_i5_post_inc, att_addr_postincr, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i12_inc, TOP_dyn_vx_sd_i5_post_inc, att_addr_postincr, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_r_inc, TOP_dyn_vx_ld_r_post_inc, att_addr_postincr, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_r_inc, TOP_dyn_vx_sd_r_post_inc, att_addr_postincr, 0);

  /* Variants for attribute: postincr|negoffset (Rn !- imm). */
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i8_inc, TOP_dyn_vx_ld_i5_post_dec, att_addr_postincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i8_inc, TOP_dyn_vx_sd_i5_post_dec, att_addr_postincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i12_inc, TOP_dyn_vx_ld_i5_post_dec, att_addr_postincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i12_inc, TOP_dyn_vx_sd_i5_post_dec, att_addr_postincr, att_addr_negoffset, 0);

  /* Variants for attribute: preinc (Rn +! imm). */
  /* None. */

  /* Variants for attribute: preinc|negoffset (Rn -! imm). */
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i8_inc, TOP_dyn_vx_ld_i5_pre_dec, att_addr_preincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i8_inc, TOP_dyn_vx_sd_i5_pre_dec, att_addr_preincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_ld_i12_inc, TOP_dyn_vx_ld_i5_pre_dec, att_addr_preincr, att_addr_negoffset, 0);
  ISA_Instruction_Variant(TOP_dyn_vx_sd_i12_inc, TOP_dyn_vx_sd_i5_pre_dec, att_addr_preincr, att_addr_negoffset, 0);

  ISA_Variants_End();
  exit(0);
}

