
#include <stdio.h>
#include <stdlib.h>
#include "targ_isa_subset.h"
#include "isa_registers_gen.h"
#include "gen_util.h"

 /* Describing 64 bits Vx registers and DVx 128 appaired registers */
 static const char *vr_reg_names[] = {
  "V0", "V1", "V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9",
  "V10", "V11", "V12", "V13", "V14", "V15", "V16", "V17", "V18", "V19", "V20",
  "V21", "V22", "V23", "V24", "V25", "V26", "V27", "V28", "V29", "V30", "V31"
  };
 
  static const char *dv_reg_names[] = {
  "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8",
  "D9", "D10", "D11", "D12", "D13", "D14", "D15"
  };

  static const char *dv_P0_reg_names[] = {
  "V0", "V2", "V4", "V6", "V8",
  "V10", "V12", "V14", "V16", "V18", "V20",
  "V22", "V24", "V26", "V28", "V30"
  };

  static const char *dv_P1_reg_names[] = {
  "V1", "V3", "V5", "V7", "V9",
  "V11", "V13", "V15", "V17", "V19", "V21",
  "V23", "V25", "V27", "V29", "V31"
  };
 
  static int vr_dv[] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 
                        18, 20, 22, 24, 26, 28, 30};

  static int vr_dv_P0[] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 
                           18, 20, 22, 24, 26, 28, 30};

  static int vr_dv_P1[] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 
                           19, 21, 23, 25, 27, 29, 31};
 
int main( int arc, char *argv[] )
{
  // Mandatory step. Set dynamic extension mode.
  Set_Dynamic("vx" /* extension name */);

  ISA_Registers_Begin("stxp70");

  ISA_REGISTER_CLASS rc_vr = ISA_Register_Class_Create("vr", 64, false,
                                                       true, false);
  ISA_Register_Set(rc_vr, 0, 31, NULL, vr_reg_names,
                   (1 << (int)ISA_SUBSET_stxp70_v3));

  ISA_Register_Subclass_Create("dv", rc_vr, 16, vr_dv,
                                dv_reg_names,true /* is canonical */);

  // Note: 'dv_reg_names' is intentionally used here instead of
  //       'dv_P0_reg_names' in order to avoid a TOP From Multi
  //       conversion before Emit stage.
  ISA_Register_Subclass_Create("dv_P0", rc_vr, 16, vr_dv_P0,
                                dv_reg_names,false /* is canonical */);

  ISA_Register_Subclass_Create("dv_P1", rc_vr, 16, vr_dv_P1,
                                dv_P1_reg_names, false /* is canonical */);

  ISA_Registers_End(); 
  exit(0);
}
