#include <stdio.h>

#include <stddef.h>
#include "gen_util.h"
#include "isa_binutils_gen.h"
#include "dyn_isa_subset.h"

main()
{
  Set_Dynamic("vx" /* extension name */);

  ISA_Binutils_Begin(9);

  ISA_Binutils_Create(ISA_SUBSET_vx,
		      "Video",
		      "Vx",
		      0,
		      -1,
		      0,
		      48,
		      0,
		      4, "Vx",
		      5, "Vx",
		      -1, NULL
		      );

  ISA_Binutils_End();
}
