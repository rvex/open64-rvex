#include "topcode.h"
#include "vx_topcode.h"
#include "dyn_isa_api.h"


// Register class level API
static TOP VX_RF0_get_load_TOP(INT size, AM_Base_Reg_Type base_reg, BOOL offs_is_imm, BOOL offs_is_incr);
static TOP VX_RF0_get_store_TOP(INT size, AM_Base_Reg_Type base_reg, BOOL offs_is_imm, BOOL offs_is_incr);

static TOP VX_RF0_get_move_X2X_TOP       (INT size);
static TOP VX_RF0_get_move_R2X_TOP       (INT size);
static TOP VX_RF0_get_move_X2R_TOP       (INT size);

static TOP VX_RF0_get_insert_and_zeroext_R2X_TOP (INT size);
static TOP VX_RF0_get_rotate_and_insert_R2X_TOP  (INT size);
static TOP VX_RF0_get_insert_and_zeroext_RR2X_TOP(INT size);
static TOP VX_RF0_get_rotate_and_insert_RR2X_TOP (INT size);
static TOP VX_RF0_get_extract_X2R_TOP            (INT size);
static TOP VX_RF0_get_extract_and_rotate_X2R_TOP (INT size);

static TOP VX_RF0_get_clear_TOP          (INT size);
    
static TOP VX_RF0_get_zeroext_P2X_TOP    (INT size);
static TOP VX_RF0_get_signext_P2X_TOP    (INT size);
static TOP VX_RF0_get_zeroext_X_TOP      (INT size);
static TOP VX_RF0_get_signext_X_TOP      (INT size);

static TOP VX_RF0_get_compose_simulated_TOP(INT from_size, INT to_size);
static TOP VX_RF0_get_extract_simulated_TOP(INT from_size, INT to_size);
static TOP VX_RF0_get_widemove_simulated_TOP(INT size);

static const extension_regclass_t VX_extension_regclass[] = {
    {
	VX_RF0_get_load_TOP,
	VX_RF0_get_store_TOP,

	VX_RF0_get_move_X2X_TOP,
	VX_RF0_get_move_R2X_TOP,
	VX_RF0_get_move_X2R_TOP,

	VX_RF0_get_insert_and_zeroext_R2X_TOP,
	VX_RF0_get_rotate_and_insert_R2X_TOP,
	VX_RF0_get_insert_and_zeroext_RR2X_TOP,
	VX_RF0_get_rotate_and_insert_RR2X_TOP,
	VX_RF0_get_extract_X2R_TOP,
	VX_RF0_get_extract_and_rotate_X2R_TOP,

	VX_RF0_get_clear_TOP,

	VX_RF0_get_zeroext_P2X_TOP,
	VX_RF0_get_signext_P2X_TOP,
	VX_RF0_get_zeroext_X_TOP,
	VX_RF0_get_signext_X_TOP,

	VX_RF0_get_compose_simulated_TOP,
	VX_RF0_get_extract_simulated_TOP,
	VX_RF0_get_widemove_simulated_TOP
    }
};


const extension_regclass_t* dyn_get_ISA_injected_operation_tab() {
    return (VX_extension_regclass);
};


/*
 * For a given load/store TOP, return an equivalent TOP with an auto-modified
 * addressing mode, specified by the post_mod, pre_mod and regclass atttributes.
 * If it does not exist, vx_CORE_TOP_UNDEFINED is returned.
 *
 * top      : initial TOP
 * post_mod : post-modified or pre-modified addressing mode
 * inc_mod  : increment or decrement addressing mode
 * regclass : offset to add given by value or register
 */
TOP dyn_get_TOP_AM_automod_variant(TOP  top,
				   BOOL post_mod,
				   BOOL inc_mod,
				   ISA_REGISTER_CLASS regclass) {
#define TOP_AM_i_r(x) \
    case TOP_local_dyn_##x##_i8_dec: \
    case TOP_local_dyn_##x##_i8_inc: \
    case TOP_local_dyn_##x##_i12_inc: \
      if (regclass == ISA_REGISTER_CLASS_UNDEFINED) { \
	if (post_mod) \
	  top_am_automod = inc_mod ? TOP_local_dyn_##x##_i5_post_inc : TOP_local_dyn_##x##_i5_post_dec; \
        else if (!inc_mod) \
	  top_am_automod = TOP_local_dyn_##x##_i5_pre_dec; \
	break; \
      } \
      /* fall through */ \
    case TOP_local_dyn_##x##_r_inc: \
      if (post_mod && inc_mod && (regclass == ISA_REGISTER_CLASS_gpr)) \
	top_am_automod = TOP_local_dyn_##x##_r_post_inc; \
      break;
    
  TOP top_am_automod = vx_CORE_TOP_UNDEFINED;
  
  // Convert to TOP local numbering (required to support switch)
  TOP local_top = top - dynamic_offset_vx;

  switch (local_top) {
      TOP_AM_i_r(vx_ld);
	  
      TOP_AM_i_r(vx_sd);
  }

  if (top_am_automod != vx_CORE_TOP_UNDEFINED) {
      // Convert to TOP global numbering
      top_am_automod += dynamic_offset_vx;
  }

  return (top_am_automod);
}


/*
 * Return the TOP, related to VX register class, used to load a data of the specified MAU size.
 * The expected addressing mode is specified in parameter.
 * Return vx_CORE_TOP_UNDEFINED if not defined
 */
static TOP VX_RF0_get_load_TOP(INT size, AM_Base_Reg_Type base_reg, BOOL offs_is_imm, BOOL offs_is_incr) {
  TOP top = vx_CORE_TOP_UNDEFINED;

  switch (size) {
  case 8:
    switch(base_reg) {
    case AM_BASE_DEFAULT:
        top = offs_is_imm ? (offs_is_incr ? (TOP)TOP_dyn_vx_ld_i8_inc
                                          : (TOP)TOP_dyn_vx_ld_i8_dec)
                          : (offs_is_incr ? (TOP)TOP_dyn_vx_ld_r_inc
                                          : vx_CORE_TOP_UNDEFINED);
      break;

    case AM_BASE_GP:
    case AM_BASE_SP:
        top = offs_is_imm ? (offs_is_incr ? (TOP)TOP_dyn_vx_ld_i12_inc
                                          : (TOP)TOP_dyn_vx_ld_i8_dec)
                          : (offs_is_incr ? (TOP)TOP_dyn_vx_ld_r_inc
                                          : vx_CORE_TOP_UNDEFINED);
      break;
    }
  case 16: // No load/Store available at this level
  default:
      break;
  }

  return (top);
}


/*
 * Return the TOP, related to VX register class, used to store a data of the specified MAU size.
 * The expected addressing mode is specified in parameter.
 * Return vx_CORE_TOP_UNDEFINED if not defined
 */
static TOP VX_RF0_get_store_TOP(INT size, AM_Base_Reg_Type base_reg, BOOL offs_is_imm, BOOL offs_is_incr) {
  TOP top = vx_CORE_TOP_UNDEFINED;

  switch (size) {
  case 8:
    switch(base_reg) {
    case AM_BASE_DEFAULT:
        top = offs_is_imm ? (offs_is_incr ? (TOP)TOP_dyn_vx_sd_i8_inc
			                  : (TOP)TOP_dyn_vx_sd_i8_dec)
                          : (offs_is_incr ? (TOP)TOP_dyn_vx_sd_r_inc
                                          : vx_CORE_TOP_UNDEFINED);
      break;

    case AM_BASE_GP:
    case AM_BASE_SP:
	top = offs_is_imm ? (offs_is_incr ? (TOP)TOP_dyn_vx_sd_i12_inc
                                          : (TOP)TOP_dyn_vx_sd_i8_dec)
                          : (offs_is_incr ? (TOP)TOP_dyn_vx_sd_r_inc
                                          : vx_CORE_TOP_UNDEFINED);
      break;
    }
  case 16: // No load/Store available at this level
  default:
      break;
  }

  return (top);
}


/*
 * Return the TOP used to move data between VX registers of the same size (given in MAU).
 * Return vx_CORE_TOP_UNDEFINED if not defined
 */
static TOP VX_RF0_get_move_X2X_TOP(int size) {
  switch (size) {
  case 8:
	return ((TOP)TOP_dyn_vx_m2v);
  case 16:
  default:
	return (vx_CORE_TOP_UNDEFINED);
  }
}


/*
 * Return the TOP used to move a 32 bits data from GPR to a 32bits extension register class.
 * Return vx_CORE_TOP_UNDEFINED if not defined
 */
static TOP VX_RF0_get_move_R2X_TOP(INT size) {
  return (vx_CORE_TOP_UNDEFINED);
}


/* 
 * Return the TOP used to move a 32 data from a 32 bits extension register class to a GPR.
 * Return vx_CORE_TOP_UNDEFINED if not defined
 */
static TOP VX_RF0_get_move_X2R_TOP(INT size) {
  return (vx_CORE_TOP_UNDEFINED);
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to move a 32 bits data in lowest part of the extension register and zero extend it.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_insert_and_zeroext_R2X_TOP(INT size) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to shift an extension register 32 bits left then to fill lowest 32 bits with a 32 bits data coming from a GPR.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_rotate_and_insert_R2X_TOP( INT size ) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to move two 32 bits data in lowest part of the extension register and zero extend it.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_insert_and_zeroext_RR2X_TOP( INT size ) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to shift an extension register 64 bits left then to fill lowest 64 bits with two 32 bits data coming from 2 GPRs.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_rotate_and_insert_RR2X_TOP( INT size ) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to extract the lowest 32 bits of an extension register in a GPR.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_extract_X2R_TOP( INT size ) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to extract the lowest 32 bits in a GPR then shift the extension register 32 bits right.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_extract_and_rotate_X2R_TOP( INT size ) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to clear an extension register.
 * The <size> argument specifies the extension register size.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_clear_TOP(INT size) {
  if (size == 8) { return ((TOP)TOP_dyn_vx_clr); }
  return vx_CORE_TOP_UNDEFINED;
}

    
/*----------------------------------------------------------------------------*
 * Return the TOP used to zero extend an extension register from a given level 
 * (Q or P) to the upper one (rspectively P or X).
 * The <xsize> argument specifies the extension register size of the upper level.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_zeroext_P2X_TOP(INT size) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to zero extend an extension register from a given level
 * (Q or P) to the upper one (rspectively P or X).
 * The <xsize> argument specifies the extension register size of the upper level.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_signext_P2X_TOP(INT size) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to zero extend an extension register from #( imm * 32 )
 * bits data upto the register size specified in parameter
 * The <xsize> argument specifies the extension register size of the upper level.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_zeroext_X_TOP(INT size) {
  return vx_CORE_TOP_UNDEFINED;
}


/*----------------------------------------------------------------------------*
 * Return the TOP used to sign extend an extension register from #( imm * 32 - 1)
 * bits data upto the register size specified in parameter
 * The <xsize> argument specifies the extension register size of the upper level.
 * Return VX_CORE_TOP_UNDEFINED if not defined
 *----------------------------------------------------------------------------*/
static TOP VX_RF0_get_signext_X_TOP(INT size) {
  return vx_CORE_TOP_UNDEFINED;
}


/*
 * Return a pseudo TOP used to compose a SIMD register (2X, 4X) from
 * its children. 
 * Return vx_CORE_TOP_UNDEFINED if not defined.
 * The size is specified in bytes.
 */
static TOP VX_RF0_get_compose_simulated_TOP(INT from_size, INT to_size) {
  if ((from_size == 8) && (to_size == 16)) {
	return ((TOP)TOP_dyn_vx_COMPOSE_V_P2X);
  }
  return (vx_CORE_TOP_UNDEFINED);
}


/*
 * Return a pseudo TOP used to extract all sub parts of a SIMD register
 * (2X, 4X).
 * Return vx_CORE_TOP_UNDEFINED if not defined.
 * The size is specified in bytes.
 */
static TOP VX_RF0_get_extract_simulated_TOP(INT from_size, INT to_size) {
  if ((from_size == 16) && (to_size == 8)) {
	return ((TOP)TOP_dyn_vx_EXTRACT_V_X2P);
  }
  return (vx_CORE_TOP_UNDEFINED);
}


/*
 * Return a pseudo TOP used to move data between to SIMD registers (2X, 4X). 
 * Return vx_CORE_TOP_UNDEFINED if not defined.
 * The size is specified in bytes.
 */
static TOP VX_RF0_get_widemove_simulated_TOP(INT size) {
  if (size == 16) {
	return ((TOP)TOP_dyn_vx_WIDEMOVE_V_X2X);
  }
  return (vx_CORE_TOP_UNDEFINED);
}


