
#include <stdio.h>

//  Generate ISA properties information 
/////////////////////////////////////// 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h> 
#include "gen_util.h"
#include "isa_properties_gen.h" 
#include "targ_isa_properties.h"
#include "vx_topcode.h" 

int
main( int argc, char* argv[] ) 
{ 
  Set_Dynamic("vx" /* extension name */);

  ISA_Properties_Begin ("stxp70");

  /* Defining all properties */
  #include "targ_isa_properties.inc.cxx"


  /* ==================================================================== 
   *              Operator attributes descriptors 
   * ==================================================================== 
   */ 

  Instruction_Group (prop_guard_t, 
                     TOP_dyn_vx_absd,
                     TOP_dyn_vx_absdmax,
                     TOP_dyn_vx_absdmin,
                     TOP_dyn_vx_addu16shr6,
                     TOP_dyn_vx_bshr,
                     TOP_dyn_vx_cplsb,
                     TOP_dyn_vx_cplsbi,
                     TOP_dyn_vx_fir3edge,
                     TOP_dyn_vx_intlvb,
                     TOP_dyn_vx_m2rub,
                     TOP_dyn_vx_m2ruhw,
                     TOP_dyn_vx_m2rw0,
                     TOP_dyn_vx_m2rw1,
                     TOP_dyn_vx_m2x,
                     TOP_dyn_vx_m2v,
                     TOP_dyn_vx_modand,
                     TOP_dyn_vx_offset,
                     TOP_dyn_vx_random,
                     TOP_dyn_vx_scales8s9,
                     TOP_dyn_vx_ld_r_post_inc,
                     TOP_dyn_vx_ld_i5_post_inc,
                     TOP_dyn_vx_ld_r_inc,
                     TOP_dyn_vx_ld_i12_inc,
                     TOP_dyn_vx_ld_i8_inc,
                     TOP_dyn_vx_ld_i5_post_dec,
                     TOP_dyn_vx_ld_i5_pre_dec,
                     TOP_dyn_vx_ld_i8_dec,
                     TOP_dyn_vx_sd_r_post_inc,
                     TOP_dyn_vx_sd_i5_post_inc,
                     TOP_dyn_vx_sd_r_inc,
                     TOP_dyn_vx_sd_i12_inc,
                     TOP_dyn_vx_sd_i8_inc,
                     TOP_dyn_vx_sd_i5_post_dec,
                     TOP_dyn_vx_sd_i5_pre_dec,
                     TOP_dyn_vx_sd_i8_dec,
                     TOP_dyn_vx_MULTI_absdmax,
                     TOP_dyn_vx_MULTI_absdmin,
                     TOP_dyn_vx_MULTI_fir3edge,
                     TOP_dyn_vx_MULTI_intlvb,
                     TOP_dyn_vx_acc,
		     TOP_dyn_vx_m2d,
		     TOP_dyn_vx_MULTI_m2d,
		     TOP_dyn_vx_m2xb,
		     TOP_dyn_vx_m2xhw,
		     TOP_dyn_vx_m2xshldb,
		     TOP_dyn_vx_m2xw0,
		     TOP_dyn_vx_m2xw1,
		     TOP_dyn_vx_absdhm1,
		     TOP_dyn_vx_absdhp1,
		     TOP_dyn_vx_addu16m1,
		     TOP_dyn_vx_addu16m2,
		     TOP_dyn_vx_addu8clp63,
		     TOP_dyn_vx_addvu8u8,
		     TOP_dyn_vx_clp63,
		     TOP_dyn_vx_cmpgtru8,
		     TOP_dyn_vx_cpmsb,
		     TOP_dyn_vx_cpmsbr,
		     TOP_dyn_vx_cpmsbir,
		     TOP_dyn_vx_fir3,
		     TOP_dyn_vx_firu8s8p,
		     TOP_dyn_vx_incgth3,
		     TOP_dyn_vx_incgth3clr,
		     TOP_dyn_vx_incinsu16,
		     TOP_dyn_vx_maru8s8shr7r,
		     TOP_dyn_vx_maru8u8shr7r,
		     TOP_dyn_vx_maviu8u8shr7r,
		     TOP_dyn_vx_mavu8u8shr7r,
		     TOP_dyn_vx_max,
		     TOP_dyn_vx_maxh3,
		     TOP_dyn_vx_maxpair,
		     TOP_dyn_vx_mpru8u8,
		     TOP_dyn_vx_mpvu8u8,
		     TOP_dyn_vx_mpviu8u8,
		     TOP_dyn_vx_select,
		     TOP_dyn_vx_sub128,
		     TOP_dyn_vx_MULTI_addu16m1,
		     TOP_dyn_vx_MULTI_addu16m2,
		     TOP_dyn_vx_MULTI_addvu8u8,
		     TOP_dyn_vx_MULTI_fir3,
		     TOP_dyn_vx_MULTI_firu8s8p,
		     TOP_dyn_vx_MULTI_incgth3,
		     TOP_dyn_vx_MULTI_incgth3clr,
		     TOP_dyn_vx_MULTI_incinsu16,
		     TOP_dyn_vx_MULTI_maru8s8shr7r,
		     TOP_dyn_vx_MULTI_maru8u8shr7r,
		     TOP_dyn_vx_MULTI_maviu8u8shr7r,
		     TOP_dyn_vx_MULTI_mavu8u8shr7r,
		     TOP_dyn_vx_MULTI_maxh3,
		     TOP_dyn_vx_MULTI_mpru8u8,
		     TOP_dyn_vx_MULTI_mpvu8u8,
		     TOP_dyn_vx_MULTI_mpviu8u8,
		     TOP_dyn_vx_absdmpslt,
		     TOP_dyn_vx_absdslt,
		     TOP_dyn_vx_adds16,
		     TOP_dyn_vx_adds16shr,
		     TOP_dyn_vx_adds16shrr,
		     TOP_dyn_vx_ascmf,
		     TOP_dyn_vx_ascmfr,
		     TOP_dyn_vx_bshrr,
		     TOP_dyn_vx_clpsym,
		     TOP_dyn_vx_clpu8,
		     TOP_dyn_vx_clr,
		     TOP_dyn_vx_cmpeqru8,
		     TOP_dyn_vx_cmpneru8,
		     TOP_dyn_vx_cmpgeru8,
		     TOP_dyn_vx_cmpltru8,
		     TOP_dyn_vx_cmpleru8,
		     TOP_dyn_vx_cntdelta0clr,
		     TOP_dyn_vx_cntdeltabw,
		     TOP_dyn_vx_cntdeltafw,
		     TOP_dyn_vx_cpmsbi,
		     TOP_dyn_vx_dintlvb,
		     TOP_dyn_vx_dlupdate,
		     TOP_dyn_vx_firu8s8p1clr,
		     TOP_dyn_vx_getsad0,
		     TOP_dyn_vx_incgt,
		     TOP_dyn_vx_insmean,
		     TOP_dyn_vx_insmeanr,
		     TOP_dyn_vx_maru8s8,
		     TOP_dyn_vx_maru8s8shr7,
		     TOP_dyn_vx_maru8u8,
		     TOP_dyn_vx_maru8u8shr7,
		     TOP_dyn_vx_maviu8s8,
		     TOP_dyn_vx_maviu8s8shr7,
		     TOP_dyn_vx_maviu8s8shr7r,
		     TOP_dyn_vx_maviu8u8,
		     TOP_dyn_vx_maviu8u8shr7,
		     TOP_dyn_vx_mavu8s8,
		     TOP_dyn_vx_mavu8s8shr7,
		     TOP_dyn_vx_mavu8s8shr7r,
		     TOP_dyn_vx_mavu8u8,
		     TOP_dyn_vx_mavu8u8shr7,
		     TOP_dyn_vx_mean,
		     TOP_dyn_vx_meanr,
		     TOP_dyn_vx_meanuv,
		     TOP_dyn_vx_meanuvr,
		     TOP_dyn_vx_meany,
		     TOP_dyn_vx_meanyr,
		     TOP_dyn_vx_median,
		     TOP_dyn_vx_mf,
		     TOP_dyn_vx_mfr,
		     TOP_dyn_vx_min,
		     TOP_dyn_vx_minsad,
		     TOP_dyn_vx_mpru8s8,
		     TOP_dyn_vx_mpu8u8shr,
		     TOP_dyn_vx_mpviu8s8,
		     TOP_dyn_vx_mpvu8s8,
		     TOP_dyn_vx_sad,
		     TOP_dyn_vx_sadmin,
		     TOP_dyn_vx_shl1s16,
		     TOP_dyn_vx_shr1s16,
		     TOP_dyn_vx_shr7s16s8rc,
		     TOP_dyn_vx_shr7s16s8rs,
		     TOP_dyn_vx_shrrs16s8,
		     TOP_dyn_vx_shrrs16s8r,
		     TOP_dyn_vx_shrs16s8,
		     TOP_dyn_vx_shrs16s8r,
		     TOP_dyn_vx_subs16,
		     TOP_dyn_vx_subs16shr,
		     TOP_dyn_vx_subs16shrr,
		     TOP_dyn_vx_MULTI_absdmpslt,
		     TOP_dyn_vx_MULTI_absdslt,
		     TOP_dyn_vx_MULTI_adds16,
		     TOP_dyn_vx_MULTI_adds16shr,
		     TOP_dyn_vx_MULTI_adds16shrr,
		     TOP_dyn_vx_MULTI_ascmf,
		     TOP_dyn_vx_MULTI_ascmfr,
		     TOP_dyn_vx_MULTI_clpsym,
		     TOP_dyn_vx_MULTI_cntdelta0clr,
		     TOP_dyn_vx_MULTI_cntdeltabw,
		     TOP_dyn_vx_MULTI_cntdeltafw,
		     TOP_dyn_vx_MULTI_dintlvb,
		     TOP_dyn_vx_MULTI_dlupdate,
		     TOP_dyn_vx_MULTI_firu8s8p1clr,
		     TOP_dyn_vx_MULTI_getsad0,
		     TOP_dyn_vx_MULTI_insmean,
		     TOP_dyn_vx_MULTI_insmeanr,
		     TOP_dyn_vx_MULTI_maru8s8,
		     TOP_dyn_vx_MULTI_maru8s8shr7,
		     TOP_dyn_vx_MULTI_maru8u8,
		     TOP_dyn_vx_MULTI_maru8u8shr7,
		     TOP_dyn_vx_MULTI_maviu8s8,
		     TOP_dyn_vx_MULTI_maviu8s8shr7,
		     TOP_dyn_vx_MULTI_maviu8s8shr7r,
		     TOP_dyn_vx_MULTI_maviu8u8,
		     TOP_dyn_vx_MULTI_maviu8u8shr7,
		     TOP_dyn_vx_MULTI_mavu8s8,
		     TOP_dyn_vx_MULTI_mavu8s8shr7,
		     TOP_dyn_vx_MULTI_mavu8s8shr7r,
		     TOP_dyn_vx_MULTI_mavu8u8,
		     TOP_dyn_vx_MULTI_mavu8u8shr7,
		     TOP_dyn_vx_MULTI_meanuv,
		     TOP_dyn_vx_MULTI_meanuvr,
		     TOP_dyn_vx_MULTI_meany,
		     TOP_dyn_vx_MULTI_meanyr,
		     TOP_dyn_vx_MULTI_median,
		     TOP_dyn_vx_MULTI_minsad,
		     TOP_dyn_vx_MULTI_mpru8s8,
		     TOP_dyn_vx_MULTI_mpviu8s8,
		     TOP_dyn_vx_MULTI_mpvu8s8,
		     TOP_dyn_vx_MULTI_sad,
		     TOP_dyn_vx_MULTI_sadmin,
		     TOP_dyn_vx_MULTI_shl1s16,
		     TOP_dyn_vx_MULTI_shr1s16,
		     TOP_dyn_vx_MULTI_shr7s16s8rc,
		     TOP_dyn_vx_MULTI_shr7s16s8rs,
		     TOP_dyn_vx_MULTI_shrrs16s8,
		     TOP_dyn_vx_MULTI_shrrs16s8r,
		     TOP_dyn_vx_MULTI_shrs16s8,
		     TOP_dyn_vx_MULTI_shrs16s8r,
		     TOP_dyn_vx_MULTI_subs16,
		     TOP_dyn_vx_MULTI_subs16shr,
		     TOP_dyn_vx_MULTI_subs16shrr,
                     (TOP)-1);

  Instruction_Group (prop_move,
                     TOP_dyn_vx_m2v,
                     TOP_dyn_vx_WIDEMOVE_V_X2X, 
                     (TOP)-1);

  Instruction_Group (prop_automod, 
		     TOP_dyn_vx_ld_r_post_inc, 
		     TOP_dyn_vx_ld_i5_post_inc, 
		     TOP_dyn_vx_ld_i5_post_dec, 
		     TOP_dyn_vx_ld_i5_pre_dec, 
		     TOP_dyn_vx_sd_r_post_inc, 
		     TOP_dyn_vx_sd_i5_post_inc, 
		     TOP_dyn_vx_sd_i5_post_dec, 
		     TOP_dyn_vx_sd_i5_pre_dec, 
		     (TOP)-1); 
  
   Instruction_Group(prop_load,
		     TOP_dyn_vx_ld_r_post_inc, 
		     TOP_dyn_vx_ld_i5_post_inc, 
		     TOP_dyn_vx_ld_r_inc, 
		     TOP_dyn_vx_ld_i12_inc, 
		     TOP_dyn_vx_ld_i8_inc, 
		     TOP_dyn_vx_ld_i5_post_dec, 
		     TOP_dyn_vx_ld_i5_pre_dec, 
		     TOP_dyn_vx_ld_i8_dec, 
		     (TOP)-1); 
   
   Instruction_Group(prop_store,
		     TOP_dyn_vx_sd_r_post_inc, 
		     TOP_dyn_vx_sd_i5_post_inc, 
		     TOP_dyn_vx_sd_r_inc, 
		     TOP_dyn_vx_sd_i12_inc, 
		     TOP_dyn_vx_sd_i8_inc, 
		     TOP_dyn_vx_sd_i5_post_dec, 
		     TOP_dyn_vx_sd_i5_pre_dec, 
		     TOP_dyn_vx_sd_i8_dec, 
		     (TOP)-1);

   Instruction_Group(prop_compose,
		     TOP_dyn_vx_COMPOSE_V_P2X, 
		     (TOP)-1);

   Instruction_Group(prop_extract,
		     TOP_dyn_vx_EXTRACT_V_X2P, 
		     (TOP)-1);

   Instruction_Group(prop_widemove,
		     TOP_dyn_vx_WIDEMOVE_V_X2X, 
		     (TOP)-1);

   Instruction_Group(prop_simulated,
		     TOP_dyn_vx_COMPOSE_V_P2X, 
		     TOP_dyn_vx_EXTRACT_V_X2P, 
		     TOP_dyn_vx_WIDEMOVE_V_X2X, 
                     (TOP)-1);

   Instruction_Group(prop_multi,
                     TOP_dyn_vx_MULTI_absdmax,
                     TOP_dyn_vx_MULTI_absdmin,
                     TOP_dyn_vx_MULTI_absdmpslt,
                     TOP_dyn_vx_MULTI_absdslt,
                     TOP_dyn_vx_MULTI_adds16,
                     TOP_dyn_vx_MULTI_adds16shr,
                     TOP_dyn_vx_MULTI_adds16shrr,
                     TOP_dyn_vx_MULTI_addu16m1,
                     TOP_dyn_vx_MULTI_addu16m2,
                     TOP_dyn_vx_MULTI_addvu8u8,
                     TOP_dyn_vx_MULTI_ascmf,
                     TOP_dyn_vx_MULTI_ascmfr,
                     TOP_dyn_vx_MULTI_clpsym,
                     TOP_dyn_vx_MULTI_cntdelta0clr,
                     TOP_dyn_vx_MULTI_cntdeltabw,
                     TOP_dyn_vx_MULTI_cntdeltafw,
                     TOP_dyn_vx_MULTI_dintlvb,
                     TOP_dyn_vx_MULTI_dlupdate,
                     TOP_dyn_vx_MULTI_fir3,
                     TOP_dyn_vx_MULTI_fir3edge,
                     TOP_dyn_vx_MULTI_firu8s8p,
                     TOP_dyn_vx_MULTI_firu8s8p1clr,
                     TOP_dyn_vx_MULTI_getsad0,
                     TOP_dyn_vx_MULTI_incgth3,
                     TOP_dyn_vx_MULTI_incgth3clr,
                     TOP_dyn_vx_MULTI_incinsu16,
                     TOP_dyn_vx_MULTI_insmean,
                     TOP_dyn_vx_MULTI_insmeanr,
                     TOP_dyn_vx_MULTI_intlvb,
                     TOP_dyn_vx_MULTI_m2d,
                     TOP_dyn_vx_MULTI_maru8s8,
                     TOP_dyn_vx_MULTI_maru8s8shr7,
                     TOP_dyn_vx_MULTI_maru8s8shr7r,
                     TOP_dyn_vx_MULTI_maru8u8,
                     TOP_dyn_vx_MULTI_maru8u8shr7,
                     TOP_dyn_vx_MULTI_maru8u8shr7r,
                     TOP_dyn_vx_MULTI_maviu8s8,
                     TOP_dyn_vx_MULTI_maviu8s8shr7,
                     TOP_dyn_vx_MULTI_maviu8s8shr7r,
                     TOP_dyn_vx_MULTI_maviu8u8,
                     TOP_dyn_vx_MULTI_maviu8u8shr7,
                     TOP_dyn_vx_MULTI_maviu8u8shr7r,
                     TOP_dyn_vx_MULTI_mavu8s8,
                     TOP_dyn_vx_MULTI_mavu8s8shr7,
                     TOP_dyn_vx_MULTI_mavu8s8shr7r,
                     TOP_dyn_vx_MULTI_mavu8u8,
                     TOP_dyn_vx_MULTI_mavu8u8shr7,
                     TOP_dyn_vx_MULTI_mavu8u8shr7r,
                     TOP_dyn_vx_MULTI_maxh3,
                     TOP_dyn_vx_MULTI_meanuv,
                     TOP_dyn_vx_MULTI_meanuvr,
                     TOP_dyn_vx_MULTI_meany,
                     TOP_dyn_vx_MULTI_meanyr,
                     TOP_dyn_vx_MULTI_median,
                     TOP_dyn_vx_MULTI_minsad,
                     TOP_dyn_vx_MULTI_mpru8s8,
                     TOP_dyn_vx_MULTI_mpru8u8,
                     TOP_dyn_vx_MULTI_mpviu8s8,
                     TOP_dyn_vx_MULTI_mpviu8u8,
                     TOP_dyn_vx_MULTI_mpvu8s8,
                     TOP_dyn_vx_MULTI_mpvu8u8,
                     TOP_dyn_vx_MULTI_sad,
                     TOP_dyn_vx_MULTI_sadmin,
                     TOP_dyn_vx_MULTI_shl1s16,
                     TOP_dyn_vx_MULTI_shr1s16,
                     TOP_dyn_vx_MULTI_shr7s16s8rc,
                     TOP_dyn_vx_MULTI_shr7s16s8rs,
                     TOP_dyn_vx_MULTI_shrrs16s8,
                     TOP_dyn_vx_MULTI_shrrs16s8r,
                     TOP_dyn_vx_MULTI_shrs16s8,
                     TOP_dyn_vx_MULTI_shrs16s8r,
                     TOP_dyn_vx_MULTI_subs16,
                     TOP_dyn_vx_MULTI_subs16shr,
                     TOP_dyn_vx_MULTI_subs16shrr,
                     (TOP)-1);


  /* ====================================== */ 
  /*         Memory Access Size 8          */ 
  /* ====================================== */ 
  ISA_Memory_Access (8, 
		 TOP_dyn_vx_ld_r_post_inc, 
		 TOP_dyn_vx_ld_i5_post_inc, 
		 TOP_dyn_vx_ld_r_inc, 
		 TOP_dyn_vx_ld_i12_inc, 
		 TOP_dyn_vx_ld_i8_inc, 
		 TOP_dyn_vx_ld_i5_post_dec, 
		 TOP_dyn_vx_ld_i5_pre_dec, 
		 TOP_dyn_vx_ld_i8_dec, 
		 TOP_dyn_vx_sd_r_post_inc, 
		 TOP_dyn_vx_sd_i5_post_inc, 
		 TOP_dyn_vx_sd_r_inc, 
		 TOP_dyn_vx_sd_i12_inc, 
		 TOP_dyn_vx_sd_i8_inc, 
		 TOP_dyn_vx_sd_i5_post_dec, 
		 TOP_dyn_vx_sd_i5_pre_dec, 
		 TOP_dyn_vx_sd_i8_dec, 
		 (TOP)-1); 

  /* ====================================== */ 
  /*         Memory Alignment 8          */ 
  /* ====================================== */ 
  ISA_Memory_Alignment (8, 
		 TOP_dyn_vx_ld_r_post_inc, 
		 TOP_dyn_vx_ld_i5_post_inc, 
		 TOP_dyn_vx_ld_r_inc, 
		 TOP_dyn_vx_ld_i12_inc, 
		 TOP_dyn_vx_ld_i8_inc, 
		 TOP_dyn_vx_ld_i5_post_dec, 
		 TOP_dyn_vx_ld_i5_pre_dec, 
		 TOP_dyn_vx_ld_i8_dec, 
		 TOP_dyn_vx_sd_r_post_inc, 
		 TOP_dyn_vx_sd_i5_post_inc, 
		 TOP_dyn_vx_sd_r_inc, 
		 TOP_dyn_vx_sd_i12_inc, 
		 TOP_dyn_vx_sd_i8_inc, 
		 TOP_dyn_vx_sd_i5_post_dec, 
		 TOP_dyn_vx_sd_i5_pre_dec, 
		 TOP_dyn_vx_sd_i8_dec, 
		 (TOP)-1); 

  /* ==================================================================== 
   *                Empty properties.
   * ==================================================================== 
   */
   Instruction_Group(prop_and        ,(TOP)-1);
   Instruction_Group(prop_or         ,(TOP)-1);
   Instruction_Group(prop_xor        ,(TOP)-1);
   Instruction_Group(prop_add        ,(TOP)-1);
   Instruction_Group(prop_barrier    ,(TOP)-1);
   Instruction_Group(prop_batomicseq ,(TOP)-1);
   Instruction_Group(prop_dummy      ,(TOP)-1);
   Instruction_Group(prop_shl        ,(TOP)-1);
   Instruction_Group(prop_shlu       ,(TOP)-1);
   Instruction_Group(prop_shr        ,(TOP)-1);
   Instruction_Group(prop_call       ,(TOP)-1);
   Instruction_Group(prop_cmp        ,(TOP)-1);
   Instruction_Group(prop_sub        ,(TOP)-1);
   Instruction_Group(prop_cond       ,(TOP)-1);
   Instruction_Group(prop_xfer       ,(TOP)-1);
   Instruction_Group(prop_sext       ,(TOP)-1);
   Instruction_Group(prop_zext       ,(TOP)-1);
   Instruction_Group(prop_ssa        ,(TOP)-1);
   Instruction_Group(prop_shru       ,(TOP)-1);
   Instruction_Group(prop_noop       ,(TOP)-1);
   Instruction_Group(prop_var_opnds  ,(TOP)-1);
   Instruction_Group(prop_unsign     ,(TOP)-1);
   Instruction_Group(prop_ijump      ,(TOP)-1);
   Instruction_Group(prop_intop      ,(TOP)-1);

  

   ISA_Properties_End();
   exit(0);
}
