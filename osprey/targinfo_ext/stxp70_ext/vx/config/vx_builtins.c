
/* This file has been automatically modified
 * in order to add BUILTARG_INOUT_TYPE information. 
 */

/*
 * This file emulates a dynamically generated extension
 */
#ifdef __cplusplus
extern "C" {
#endif

#include "../gccfe/extension_include.h"
#include "dyn_dll_api.h"
#include "vx_mtypes.h"

#define INTRINSIC_ABSD           INTRINSIC_STATIC_LAST + 1
#define INTRINSIC_ABSDMAX        INTRINSIC_STATIC_LAST + 2
#define INTRINSIC_ABSDMIN        INTRINSIC_STATIC_LAST + 3
#define INTRINSIC_ADDU16SHR6     INTRINSIC_STATIC_LAST + 4
#define INTRINSIC_BSHR           INTRINSIC_STATIC_LAST + 5
#define INTRINSIC_CPLSB          INTRINSIC_STATIC_LAST + 6
#define INTRINSIC_CPLSBI         INTRINSIC_STATIC_LAST + 7
#define INTRINSIC_FIR3EDGE       INTRINSIC_STATIC_LAST + 8
#define INTRINSIC_INTLVB         INTRINSIC_STATIC_LAST + 9
#define INTRINSIC_M2RUB          INTRINSIC_STATIC_LAST + 10
#define INTRINSIC_M2RUHW         INTRINSIC_STATIC_LAST + 11
#define INTRINSIC_M2RW0          INTRINSIC_STATIC_LAST + 12
#define INTRINSIC_M2RW1          INTRINSIC_STATIC_LAST + 13
#define INTRINSIC_M2X            INTRINSIC_STATIC_LAST + 14
#define INTRINSIC_M2V            INTRINSIC_STATIC_LAST + 15
#define INTRINSIC_MODAND         INTRINSIC_STATIC_LAST + 16
#define INTRINSIC_OFFSET         INTRINSIC_STATIC_LAST + 17
#define INTRINSIC_RANDOM         INTRINSIC_STATIC_LAST + 18
#define INTRINSIC_SCALES8S9      INTRINSIC_STATIC_LAST + 19

#define INTRINSIC_vx_INSERT_DP_INTO_DX0   INTRINSIC_STATIC_LAST + 20 // Compiler specific intrinsic for subpart access
#define INTRINSIC_vx_INSERT_DP_INTO_DX1   INTRINSIC_STATIC_LAST + 21 // Compiler specific intrinsic for subpart access
#define INTRINSIC_vx_EXTRACT_DP_FROM_DX0  INTRINSIC_STATIC_LAST + 22 // Compiler specific intrinsic for subpart access
#define INTRINSIC_vx_EXTRACT_DP_FROM_DX1  INTRINSIC_STATIC_LAST + 23 // Compiler specific intrinsic for subpart access
#define INTRINSIC_vx_COMPOSE_DX_FROM_2xDP INTRINSIC_STATIC_LAST + 24 // Compiler specific intrinsic for subpart access

#define INTRINSIC_ACC            INTRINSIC_STATIC_LAST + 25
#define INTRINSIC_M2D            INTRINSIC_STATIC_LAST + 26
#define INTRINSIC_M2XB           INTRINSIC_STATIC_LAST + 27
#define INTRINSIC_M2XHW          INTRINSIC_STATIC_LAST + 28
#define INTRINSIC_M2XSHLDB       INTRINSIC_STATIC_LAST + 29
#define INTRINSIC_M2XW0          INTRINSIC_STATIC_LAST + 30
#define INTRINSIC_M2XW1          INTRINSIC_STATIC_LAST + 31
	// TNR
#define INTRINSIC_ABSDHM1        INTRINSIC_STATIC_LAST + 32
#define INTRINSIC_ABSDHP1        INTRINSIC_STATIC_LAST + 33
#define INTRINSIC_ADDU16M1       INTRINSIC_STATIC_LAST + 34
#define INTRINSIC_ADDU16M2       INTRINSIC_STATIC_LAST + 35
#define INTRINSIC_ADDU8CLP63     INTRINSIC_STATIC_LAST + 36
#define INTRINSIC_ADDVU8U8       INTRINSIC_STATIC_LAST + 37
#define INTRINSIC_CLP63          INTRINSIC_STATIC_LAST + 38
#define INTRINSIC_CMPGTRU8       INTRINSIC_STATIC_LAST + 39
#define INTRINSIC_CPMSB          INTRINSIC_STATIC_LAST + 40
#define INTRINSIC_CPMSBR         INTRINSIC_STATIC_LAST + 41
#define INTRINSIC_CPMSBIR        INTRINSIC_STATIC_LAST + 42
#define INTRINSIC_FIR3           INTRINSIC_STATIC_LAST + 43
#define INTRINSIC_FIRU8S8P       INTRINSIC_STATIC_LAST + 44
#define INTRINSIC_INCGTH3        INTRINSIC_STATIC_LAST + 45
#define INTRINSIC_INCGTH3CLR     INTRINSIC_STATIC_LAST + 46
#define INTRINSIC_INCINSU16      INTRINSIC_STATIC_LAST + 47
#define INTRINSIC_MARU8S8SHR7R   INTRINSIC_STATIC_LAST + 48
#define INTRINSIC_MARU8U8SHR7R   INTRINSIC_STATIC_LAST + 49
#define INTRINSIC_MAVIU8U8SHR7R  INTRINSIC_STATIC_LAST + 50
#define INTRINSIC_MAVU8U8SHR7R   INTRINSIC_STATIC_LAST + 51
#define INTRINSIC_MAX            INTRINSIC_STATIC_LAST + 52
#define INTRINSIC_MAXH3          INTRINSIC_STATIC_LAST + 53
#define INTRINSIC_MAXPAIR        INTRINSIC_STATIC_LAST + 54
#define INTRINSIC_MPRU8U8        INTRINSIC_STATIC_LAST + 55
#define INTRINSIC_MPVU8U8        INTRINSIC_STATIC_LAST + 56
#define INTRINSIC_MPVIU8U8       INTRINSIC_STATIC_LAST + 57
#define INTRINSIC_SELECT         INTRINSIC_STATIC_LAST + 58
#define INTRINSIC_SUB128         INTRINSIC_STATIC_LAST + 59
    // Remaining 
#define INTRINSIC_ABSDMPSLT      INTRINSIC_STATIC_LAST + 60
#define INTRINSIC_ABSDSLT        INTRINSIC_STATIC_LAST + 61
#define INTRINSIC_ADDS16         INTRINSIC_STATIC_LAST + 62
#define INTRINSIC_ADDS16SHR      INTRINSIC_STATIC_LAST + 63
#define INTRINSIC_ADDS16SHRR     INTRINSIC_STATIC_LAST + 64
#define INTRINSIC_ASCMF          INTRINSIC_STATIC_LAST + 65
#define INTRINSIC_ASCMFR         INTRINSIC_STATIC_LAST + 66
#define INTRINSIC_BSHRR          INTRINSIC_STATIC_LAST + 67
#define INTRINSIC_CLPSYM         INTRINSIC_STATIC_LAST + 68
#define INTRINSIC_CLPU8          INTRINSIC_STATIC_LAST + 69
#define INTRINSIC_CLR            INTRINSIC_STATIC_LAST + 70
#define INTRINSIC_CMPEQRU8       INTRINSIC_STATIC_LAST + 71
#define INTRINSIC_CMPGERU8       INTRINSIC_STATIC_LAST + 72
#define INTRINSIC_CMPLERU8       INTRINSIC_STATIC_LAST + 73
#define INTRINSIC_CMPLTRU8       INTRINSIC_STATIC_LAST + 74
#define INTRINSIC_CMPNERU8       INTRINSIC_STATIC_LAST + 75
#define INTRINSIC_CNTDELTA0CLR   INTRINSIC_STATIC_LAST + 76
#define INTRINSIC_CNTDELTABW     INTRINSIC_STATIC_LAST + 77
#define INTRINSIC_CNTDELTAFW     INTRINSIC_STATIC_LAST + 78
#define INTRINSIC_CPMSBI         INTRINSIC_STATIC_LAST + 79
#define INTRINSIC_DINTLVB        INTRINSIC_STATIC_LAST + 80
#define INTRINSIC_DLUPDATE       INTRINSIC_STATIC_LAST + 81
#define INTRINSIC_FIRU8S8P1CLR   INTRINSIC_STATIC_LAST + 82
#define INTRINSIC_GETSAD0        INTRINSIC_STATIC_LAST + 83
#define INTRINSIC_INCGT          INTRINSIC_STATIC_LAST + 84
#define INTRINSIC_INSMEAN        INTRINSIC_STATIC_LAST + 85
#define INTRINSIC_INSMEANR       INTRINSIC_STATIC_LAST + 86
#define INTRINSIC_MARU8S8        INTRINSIC_STATIC_LAST + 87
#define INTRINSIC_MARU8S8SHR7    INTRINSIC_STATIC_LAST + 88
#define INTRINSIC_MARU8U8        INTRINSIC_STATIC_LAST + 89
#define INTRINSIC_MARU8U8SHR7    INTRINSIC_STATIC_LAST + 90
#define INTRINSIC_MAVIU8S8       INTRINSIC_STATIC_LAST + 91
#define INTRINSIC_MAVIU8S8SHR7   INTRINSIC_STATIC_LAST + 92
#define INTRINSIC_MAVIU8S8SHR7R  INTRINSIC_STATIC_LAST + 93
#define INTRINSIC_MAVIU8U8       INTRINSIC_STATIC_LAST + 94
#define INTRINSIC_MAVIU8U8SHR7   INTRINSIC_STATIC_LAST + 95
#define INTRINSIC_MAVU8S8        INTRINSIC_STATIC_LAST + 96
#define INTRINSIC_MAVU8S8SHR7    INTRINSIC_STATIC_LAST + 97
#define INTRINSIC_MAVU8S8SHR7R   INTRINSIC_STATIC_LAST + 98
#define INTRINSIC_MAVU8U8        INTRINSIC_STATIC_LAST + 99
#define INTRINSIC_MAVU8U8SHR7    INTRINSIC_STATIC_LAST + 100
#define INTRINSIC_MEAN           INTRINSIC_STATIC_LAST + 101
#define INTRINSIC_MEANR          INTRINSIC_STATIC_LAST + 102
#define INTRINSIC_MEANUV         INTRINSIC_STATIC_LAST + 103
#define INTRINSIC_MEANUVR        INTRINSIC_STATIC_LAST + 104
#define INTRINSIC_MEANY          INTRINSIC_STATIC_LAST + 105
#define INTRINSIC_MEANYR         INTRINSIC_STATIC_LAST + 106
#define INTRINSIC_MEDIAN         INTRINSIC_STATIC_LAST + 107
#define INTRINSIC_MF             INTRINSIC_STATIC_LAST + 108
#define INTRINSIC_MFR            INTRINSIC_STATIC_LAST + 109
#define INTRINSIC_MIN            INTRINSIC_STATIC_LAST + 110
#define INTRINSIC_MINSAD         INTRINSIC_STATIC_LAST + 111
#define INTRINSIC_MPRU8S8        INTRINSIC_STATIC_LAST + 112
#define INTRINSIC_MPU8U8SHR      INTRINSIC_STATIC_LAST + 113
#define INTRINSIC_MPVIU8S8       INTRINSIC_STATIC_LAST + 114
#define INTRINSIC_MPVU8S8        INTRINSIC_STATIC_LAST + 115
#define INTRINSIC_SAD            INTRINSIC_STATIC_LAST + 116
#define INTRINSIC_SADMIN         INTRINSIC_STATIC_LAST + 117
#define INTRINSIC_SHL1S16        INTRINSIC_STATIC_LAST + 118
#define INTRINSIC_SHR1S16        INTRINSIC_STATIC_LAST + 119
#define INTRINSIC_SHR7S16S8RC    INTRINSIC_STATIC_LAST + 120
#define INTRINSIC_SHR7S16S8RS    INTRINSIC_STATIC_LAST + 121
#define INTRINSIC_SHRRS16S8      INTRINSIC_STATIC_LAST + 122
#define INTRINSIC_SHRRS16S8R     INTRINSIC_STATIC_LAST + 123
#define INTRINSIC_SHRS16S8       INTRINSIC_STATIC_LAST + 124
#define INTRINSIC_SHRS16S8R      INTRINSIC_STATIC_LAST + 125
#define INTRINSIC_SUBS16         INTRINSIC_STATIC_LAST + 126
#define INTRINSIC_SUBS16SHR      INTRINSIC_STATIC_LAST + 127
#define INTRINSIC_SUBS16SHRR     INTRINSIC_STATIC_LAST + 128


#define BUILT_IN_ABSD            BUILT_IN_STATIC_LAST + 1
#define BUILT_IN_ABSDMAX         BUILT_IN_STATIC_LAST + 2
#define BUILT_IN_ABSDMIN         BUILT_IN_STATIC_LAST + 3
#define BUILT_IN_ADDU16SHR6      BUILT_IN_STATIC_LAST + 4
#define BUILT_IN_BSHR            BUILT_IN_STATIC_LAST + 5
#define BUILT_IN_CPLSB           BUILT_IN_STATIC_LAST + 6
#define BUILT_IN_CPLSBI          BUILT_IN_STATIC_LAST + 7
#define BUILT_IN_FIR3EDGE        BUILT_IN_STATIC_LAST + 8
#define BUILT_IN_INTLVB          BUILT_IN_STATIC_LAST + 9
#define BUILT_IN_M2RUB           BUILT_IN_STATIC_LAST + 10
#define BUILT_IN_M2RUHW          BUILT_IN_STATIC_LAST + 11
#define BUILT_IN_M2RW0           BUILT_IN_STATIC_LAST + 12
#define BUILT_IN_M2RW1           BUILT_IN_STATIC_LAST + 13
#define BUILT_IN_M2X             BUILT_IN_STATIC_LAST + 14
#define BUILT_IN_M2V             BUILT_IN_STATIC_LAST + 15
#define BUILT_IN_MODAND          BUILT_IN_STATIC_LAST + 16
#define BUILT_IN_OFFSET          BUILT_IN_STATIC_LAST + 17
#define BUILT_IN_RANDOM          BUILT_IN_STATIC_LAST + 18
#define BUILT_IN_SCALES8S9       BUILT_IN_STATIC_LAST + 19

#define BUILT_IN_vx_INSERT_DP_INTO_DX0   BUILT_IN_STATIC_LAST + 20  // Compiler specific intrinsic for subpart access
#define BUILT_IN_vx_INSERT_DP_INTO_DX1   BUILT_IN_STATIC_LAST + 21  // Compiler specific intrinsic for subpart access
#define BUILT_IN_vx_EXTRACT_DP_FROM_DX0  BUILT_IN_STATIC_LAST + 22  // Compiler specific intrinsic for subpart access
#define BUILT_IN_vx_EXTRACT_DP_FROM_DX1  BUILT_IN_STATIC_LAST + 23  // Compiler specific intrinsic for subpart access
#define BUILT_IN_vx_COMPOSE_DX_FROM_2xDP BUILT_IN_STATIC_LAST + 24  // Compiler specific intrinsic for subpart access

#define BUILT_IN_ACC   	         BUILT_IN_STATIC_LAST + 25
#define BUILT_IN_M2D             BUILT_IN_STATIC_LAST + 26
#define BUILT_IN_M2XB            BUILT_IN_STATIC_LAST + 27
#define BUILT_IN_M2XHW           BUILT_IN_STATIC_LAST + 28
#define BUILT_IN_M2XSHLDB        BUILT_IN_STATIC_LAST + 29
#define BUILT_IN_M2XW0           BUILT_IN_STATIC_LAST + 30
#define BUILT_IN_M2XW1           BUILT_IN_STATIC_LAST + 31
	// TNR 
#define BUILT_IN_ABSDHM1         BUILT_IN_STATIC_LAST + 32
#define BUILT_IN_ABSDHP1         BUILT_IN_STATIC_LAST + 33
#define BUILT_IN_ADDU16M1        BUILT_IN_STATIC_LAST + 34
#define BUILT_IN_ADDU16M2        BUILT_IN_STATIC_LAST + 35
#define BUILT_IN_ADDU8CLP63      BUILT_IN_STATIC_LAST + 36
#define BUILT_IN_ADDVU8U8        BUILT_IN_STATIC_LAST + 37
#define BUILT_IN_CLP63           BUILT_IN_STATIC_LAST + 38
#define BUILT_IN_CMPGTRU8        BUILT_IN_STATIC_LAST + 39
#define BUILT_IN_CPMSB           BUILT_IN_STATIC_LAST + 40
#define BUILT_IN_CPMSBR          BUILT_IN_STATIC_LAST + 41
#define BUILT_IN_CPMSBIR         BUILT_IN_STATIC_LAST + 42
#define BUILT_IN_FIR3            BUILT_IN_STATIC_LAST + 43
#define BUILT_IN_FIRU8S8P        BUILT_IN_STATIC_LAST + 44
#define BUILT_IN_INCGTH3         BUILT_IN_STATIC_LAST + 45
#define BUILT_IN_INCGTH3CLR      BUILT_IN_STATIC_LAST + 46
#define BUILT_IN_INCINSU16       BUILT_IN_STATIC_LAST + 47
#define BUILT_IN_MARU8S8SHR7R    BUILT_IN_STATIC_LAST + 48
#define BUILT_IN_MARU8U8SHR7R    BUILT_IN_STATIC_LAST + 49
#define BUILT_IN_MAVIU8U8SHR7R   BUILT_IN_STATIC_LAST + 50
#define BUILT_IN_MAVU8U8SHR7R    BUILT_IN_STATIC_LAST + 51
#define BUILT_IN_MAX             BUILT_IN_STATIC_LAST + 52
#define BUILT_IN_MAXH3           BUILT_IN_STATIC_LAST + 53
#define BUILT_IN_MAXPAIR         BUILT_IN_STATIC_LAST + 54
#define BUILT_IN_MPRU8U8         BUILT_IN_STATIC_LAST + 55
#define BUILT_IN_MPVU8U8         BUILT_IN_STATIC_LAST + 56
#define BUILT_IN_MPVIU8U8        BUILT_IN_STATIC_LAST + 57
#define BUILT_IN_SELECT          BUILT_IN_STATIC_LAST + 58
#define BUILT_IN_SUB128          BUILT_IN_STATIC_LAST + 59
    // Remainings
#define BUILT_IN_ABSDMPSLT       BUILT_IN_STATIC_LAST + 60
#define BUILT_IN_ABSDSLT         BUILT_IN_STATIC_LAST + 61
#define BUILT_IN_ADDS16          BUILT_IN_STATIC_LAST + 62
#define BUILT_IN_ADDS16SHR       BUILT_IN_STATIC_LAST + 63
#define BUILT_IN_ADDS16SHRR      BUILT_IN_STATIC_LAST + 64
#define BUILT_IN_ASCMF           BUILT_IN_STATIC_LAST + 65
#define BUILT_IN_ASCMFR          BUILT_IN_STATIC_LAST + 66
#define BUILT_IN_BSHRR           BUILT_IN_STATIC_LAST + 67
#define BUILT_IN_CLPSYM          BUILT_IN_STATIC_LAST + 68
#define BUILT_IN_CLPU8           BUILT_IN_STATIC_LAST + 69
#define BUILT_IN_CLR             BUILT_IN_STATIC_LAST + 70
#define BUILT_IN_CMPEQRU8        BUILT_IN_STATIC_LAST + 71
#define BUILT_IN_CMPGERU8        BUILT_IN_STATIC_LAST + 72
#define BUILT_IN_CMPLERU8        BUILT_IN_STATIC_LAST + 73
#define BUILT_IN_CMPLTRU8        BUILT_IN_STATIC_LAST + 74
#define BUILT_IN_CMPNERU8        BUILT_IN_STATIC_LAST + 75
#define BUILT_IN_CNTDELTA0CLR    BUILT_IN_STATIC_LAST + 76
#define BUILT_IN_CNTDELTABW      BUILT_IN_STATIC_LAST + 77
#define BUILT_IN_CNTDELTAFW      BUILT_IN_STATIC_LAST + 78
#define BUILT_IN_CPMSBI          BUILT_IN_STATIC_LAST + 79
#define BUILT_IN_DINTLVB         BUILT_IN_STATIC_LAST + 80
#define BUILT_IN_DLUPDATE        BUILT_IN_STATIC_LAST + 81
#define BUILT_IN_FIRU8S8P1CLR    BUILT_IN_STATIC_LAST + 82
#define BUILT_IN_GETSAD0         BUILT_IN_STATIC_LAST + 83
#define BUILT_IN_INCGT           BUILT_IN_STATIC_LAST + 84
#define BUILT_IN_INSMEAN         BUILT_IN_STATIC_LAST + 85
#define BUILT_IN_INSMEANR        BUILT_IN_STATIC_LAST + 86
#define BUILT_IN_MARU8S8         BUILT_IN_STATIC_LAST + 87
#define BUILT_IN_MARU8S8SHR7     BUILT_IN_STATIC_LAST + 88
#define BUILT_IN_MARU8U8         BUILT_IN_STATIC_LAST + 89
#define BUILT_IN_MARU8U8SHR7     BUILT_IN_STATIC_LAST + 90
#define BUILT_IN_MAVIU8S8        BUILT_IN_STATIC_LAST + 91
#define BUILT_IN_MAVIU8S8SHR7    BUILT_IN_STATIC_LAST + 92
#define BUILT_IN_MAVIU8S8SHR7R   BUILT_IN_STATIC_LAST + 93
#define BUILT_IN_MAVIU8U8        BUILT_IN_STATIC_LAST + 94
#define BUILT_IN_MAVIU8U8SHR7    BUILT_IN_STATIC_LAST + 95
#define BUILT_IN_MAVU8S8         BUILT_IN_STATIC_LAST + 96
#define BUILT_IN_MAVU8S8SHR7     BUILT_IN_STATIC_LAST + 97
#define BUILT_IN_MAVU8S8SHR7R    BUILT_IN_STATIC_LAST + 98
#define BUILT_IN_MAVU8U8         BUILT_IN_STATIC_LAST + 99
#define BUILT_IN_MAVU8U8SHR7     BUILT_IN_STATIC_LAST + 100
#define BUILT_IN_MEAN            BUILT_IN_STATIC_LAST + 101
#define BUILT_IN_MEANR           BUILT_IN_STATIC_LAST + 102
#define BUILT_IN_MEANUV          BUILT_IN_STATIC_LAST + 103
#define BUILT_IN_MEANUVR         BUILT_IN_STATIC_LAST + 104
#define BUILT_IN_MEANY           BUILT_IN_STATIC_LAST + 105
#define BUILT_IN_MEANYR          BUILT_IN_STATIC_LAST + 106
#define BUILT_IN_MEDIAN          BUILT_IN_STATIC_LAST + 107
#define BUILT_IN_MF              BUILT_IN_STATIC_LAST + 108
#define BUILT_IN_MFR             BUILT_IN_STATIC_LAST + 109
#define BUILT_IN_MIN             BUILT_IN_STATIC_LAST + 110
#define BUILT_IN_MINSAD          BUILT_IN_STATIC_LAST + 111
#define BUILT_IN_MPRU8S8         BUILT_IN_STATIC_LAST + 112
#define BUILT_IN_MPU8U8SHR       BUILT_IN_STATIC_LAST + 113
#define BUILT_IN_MPVIU8S8        BUILT_IN_STATIC_LAST + 114
#define BUILT_IN_MPVU8S8         BUILT_IN_STATIC_LAST + 115
#define BUILT_IN_SAD             BUILT_IN_STATIC_LAST + 116
#define BUILT_IN_SADMIN          BUILT_IN_STATIC_LAST + 117
#define BUILT_IN_SHL1S16         BUILT_IN_STATIC_LAST + 118
#define BUILT_IN_SHR1S16         BUILT_IN_STATIC_LAST + 119
#define BUILT_IN_SHR7S16S8RC     BUILT_IN_STATIC_LAST + 120
#define BUILT_IN_SHR7S16S8RS     BUILT_IN_STATIC_LAST + 121
#define BUILT_IN_SHRRS16S8       BUILT_IN_STATIC_LAST + 122
#define BUILT_IN_SHRRS16S8R      BUILT_IN_STATIC_LAST + 123
#define BUILT_IN_SHRS16S8        BUILT_IN_STATIC_LAST + 124
#define BUILT_IN_SHRS16S8R       BUILT_IN_STATIC_LAST + 125
#define BUILT_IN_SUBS16          BUILT_IN_STATIC_LAST + 126
#define BUILT_IN_SUBS16SHR       BUILT_IN_STATIC_LAST + 127
#define BUILT_IN_SUBS16SHRR      BUILT_IN_STATIC_LAST + 128


/* builtin argument arrays */
static const machine_mode_t  arg_builtin_absd[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_absdmax[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_absdmin[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_addu16shr6[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_bshr[] = {VxV1DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_cplsb[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_cplsbi[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_fir3edge[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_intlvb[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_m2rub[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_m2ruhw[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_m2rw0[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_m2rw1[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_m2x[] = {SImode, SImode};
static const machine_mode_t  arg_builtin_m2v[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_modand[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_offset[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_random[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_scales8s9[] = {VxV1DImode, SImode, SImode};

/* Compiler specific builtin argument arrays */
static const machine_mode_t  arg_builtin_vx_INSERT_DP_INTO_DX0[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_vx_INSERT_DP_INTO_DX1[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_vx_EXTRACT_DP_FROM_DX0[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_vx_EXTRACT_DP_FROM_DX1[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_vx_COMPOSE_DX_FROM_2xDP[] = {VxV1DImode, VxV1DImode};

static const machine_mode_t  arg_builtin_acc[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_m2d[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_m2xb[] = {VxV1DImode, SImode, SImode};
static const machine_mode_t  arg_builtin_m2xhw[] = {VxV1DImode, SImode, SImode};
static const machine_mode_t  arg_builtin_m2xshldb[] = {VxV1DImode, SImode, SImode};
static const machine_mode_t  arg_builtin_m2xw0[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_m2xw1[] = {VxV1DImode, SImode};

static const machine_mode_t  arg_builtin_ABSDHM1[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_ABSDHP1[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_ADDU16M1[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_ADDU16M2[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_ADDU8CLP63[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_ADDVU8U8[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_CLP63[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_CMPGTRU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CPMSB[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CPMSBR[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CPMSBIR[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_FIR3[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_FIRU8S8P[] = {VxV2DImode, VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_INCGTH3[] = {VxV1DImode, VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_INCGTH3CLR[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_INCINSU16[] = {VxV1DImode, VxV2DImode, SImode, SImode};
static const machine_mode_t  arg_builtin_MARU8S8SHR7R[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MARU8U8SHR7R[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MAVIU8U8SHR7R[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8U8SHR7R[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAX[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAXH3[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAXPAIR[] = {VxV1DImode};
static const machine_mode_t  arg_builtin_MPRU8U8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MPVU8U8[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MPVIU8U8[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_SELECT[] = {VxV1DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_SUB128[] = {VxV1DImode};

static const machine_mode_t  arg_builtin_ABSDMPSLT[] = {VxV2DImode, VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_ABSDSLT[] = {VxV2DImode, VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_ADDS16[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_ADDS16SHR[] = {VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_ADDS16SHRR[] = {VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_ASCMF[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_ASCMFR[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_BSHRR[] = {VxV1DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CLPSYM[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_CLPU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CLR[] = {VOIDmode};
static const machine_mode_t  arg_builtin_CMPEQRU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CMPGERU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CMPLERU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CMPLTRU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CMPNERU8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_CNTDELTA0CLR[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_CNTDELTABW[] = {VxV2DImode, VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_CNTDELTAFW[] = {VxV2DImode, VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_CPMSBI[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_DINTLVB[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_DLUPDATE[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_FIRU8S8P1CLR[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_GETSAD0[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_INCGT[] = {VxV1DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_INSMEAN[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_INSMEANR[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MARU8S8[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MARU8S8SHR7[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MARU8U8[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MARU8U8SHR7[] = {VxV2DImode, VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MAVIU8S8[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVIU8S8SHR7[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVIU8S8SHR7R[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVIU8U8[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVIU8U8SHR7[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8S8[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8S8SHR7[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8S8SHR7R[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8U8[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MAVU8U8SHR7[] = {VxV2DImode, VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MEAN[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MEANR[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MEANUV[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_MEANUVR[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_MEANY[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_MEANYR[] = {VxV2DImode, VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_MEDIAN[] = {VxV2DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MF[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MFR[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MIN[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MINSAD[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_MPRU8S8[] = {VxV1DImode, SImode};
static const machine_mode_t  arg_builtin_MPU8U8SHR[] = {VxV1DImode, SImode, SImode};
static const machine_mode_t  arg_builtin_MPVIU8S8[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_MPVU8S8[] = {VxV1DImode, VxV1DImode};
static const machine_mode_t  arg_builtin_SAD[] = {VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SADMIN[] = {VxV2DImode, VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SHL1S16[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_SHR1S16[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_SHR7S16S8RC[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_SHR7S16S8RS[] = {VxV2DImode};
static const machine_mode_t  arg_builtin_SHRRS16S8[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SHRRS16S8R[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SHRS16S8[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SHRS16S8R[] = {VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SUBS16[] = {VxV2DImode, VxV2DImode};
static const machine_mode_t  arg_builtin_SUBS16SHR[] = {VxV2DImode, VxV2DImode, SImode};
static const machine_mode_t  arg_builtin_SUBS16SHRR[] = {VxV2DImode, VxV2DImode, SImode};



/*INOUT arguments*/
static const BUILTARG_INOUT_TYPE arg_inout_absd[]          = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_absdmax[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_absdmin[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_addu16shr6[]    = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_bshr[]          = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_cplsb[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_cplsbi[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_fir3edge[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_intlvb[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2rub[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2ruhw[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2rw0[]         = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2rw1[]         = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2x[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2v[]           = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_modand[]        = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_offset[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_random[]        = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_scales8s9[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_vx_INSERT_DP_INTO_DX0[] = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_vx_INSERT_DP_INTO_DX1[] = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_vx_EXTRACT_DP_FROM_DX0[] = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_vx_EXTRACT_DP_FROM_DX1[] = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_vx_COMPOSE_DX_FROM_2xDP[] = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_acc[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2d[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2xb[]          = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2xhw[]         = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2xshldb[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2xw0[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_m2xw1[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ABSDHM1[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ABSDHP1[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDU16M1[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDU16M2[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDU8CLP63[]    = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDVU8U8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CLP63[]         = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CMPGTRU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CPMSB[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CPMSBR[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CPMSBIR[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_FIR3[]          = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_FIRU8S8P[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INCGTH3[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INCGTH3CLR[]    = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INCINSU16[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8S8SHR7R[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8U8SHR7R[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8U8SHR7R[] = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8U8SHR7R[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAX[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAXH3[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAXPAIR[]       = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPRU8U8[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPVU8U8[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPVIU8U8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SELECT[]        = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SUB128[]        = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ABSDMPSLT[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ABSDSLT[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDS16[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDS16SHR[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ADDS16SHRR[]    = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ASCMF[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_ASCMFR[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_BSHRR[]         = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CLPSYM[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CLPU8[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CLR[]           = { };
static const BUILTARG_INOUT_TYPE arg_inout_CMPEQRU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CMPGERU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CMPLERU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CMPLTRU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CMPNERU8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CNTDELTA0CLR[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CNTDELTABW[]    = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CNTDELTAFW[]    = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_CPMSBI[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_DINTLVB[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_DLUPDATE[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_FIRU8S8P1CLR[]  = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_GETSAD0[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INCGT[]         = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INSMEAN[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_INSMEANR[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8S8[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8S8SHR7[]   = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8U8[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MARU8U8SHR7[]   = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8S8[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8S8SHR7[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8S8SHR7R[] = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8U8[]      = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVIU8U8SHR7[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8S8[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8S8SHR7[]   = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8S8SHR7R[]  = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8U8[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MAVU8U8SHR7[]   = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEAN[]          = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEANR[]         = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEANUV[]        = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEANUVR[]       = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEANY[]         = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEANYR[]        = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MEDIAN[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MF[]            = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MFR[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MIN[]           = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MINSAD[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPRU8S8[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPU8U8SHR[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPVIU8S8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_MPVU8S8[]       = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SAD[]           = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SADMIN[]        = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHL1S16[]       = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHR1S16[]       = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHR7S16S8RC[]   = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHR7S16S8RS[]   = {BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHRRS16S8[]     = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHRRS16S8R[]    = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHRS16S8[]      = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SHRS16S8R[]     = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SUBS16[]        = {BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SUBS16SHR[]     = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };
static const BUILTARG_INOUT_TYPE arg_inout_SUBS16SHRR[]    = {BUILTARG_IN , BUILTARG_IN , BUILTARG_IN };



/* extension builtin array */
static const extension_builtins_t dyn_builtins[] = {
  {
    BUILT_IN_ABSD,
    INTRINSIC_ABSD,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absd", /* c_name */
    "__builtin_vx_absd", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_absd, /* arg types */
    arg_inout_absd, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {0}, /* local TOP id */
  },
  {
    BUILT_IN_ABSDMAX,
    INTRINSIC_ABSDMAX,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdmax", /* c_name */
    "__builtin_vx_absdmax", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_absdmax, /* arg types */
    arg_inout_absdmax, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {1}, /* local TOP id */
  },
  {
    BUILT_IN_ABSDMIN,
    INTRINSIC_ABSDMIN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdmin", /* c_name */
    "__builtin_vx_absdmin", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_absdmin, /* arg types */
    arg_inout_absdmin, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {2}, /* local TOP id */
  },
  {
    BUILT_IN_ADDU16SHR6,
    INTRINSIC_ADDU16SHR6,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_addu16shr6", /* c_name */
    "__builtin_vx_addu16shr6", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_addu16shr6, /* arg types */
    arg_inout_addu16shr6, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {3}, /* local TOP id */
  },
  {
    BUILT_IN_BSHR,
    INTRINSIC_BSHR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_bshr", /* c_name */
    "__builtin_vx_bshr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_bshr, /* arg types */
    arg_inout_bshr, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {4}, /* local TOP id */
  },
  {
    BUILT_IN_CPLSB,
    INTRINSIC_CPLSB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cplsb", /* c_name */
    "__builtin_vx_cplsb", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_cplsb, /* arg types */
    arg_inout_cplsb, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {5}, /* local TOP id */
  },
  {
    BUILT_IN_CPLSBI,
    INTRINSIC_CPLSBI,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cplsbi", /* c_name */
    "__builtin_vx_cplsbi", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_cplsbi, /* arg types */
    arg_inout_cplsbi, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {6}, /* local TOP id */
  },
  {
    BUILT_IN_FIR3EDGE,
    INTRINSIC_FIR3EDGE,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_fir3edge", /* c_name */
    "__builtin_vx_fir3edge", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_fir3edge, /* arg types */
    arg_inout_fir3edge, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {7}, /* local TOP id */
  },
  {
    BUILT_IN_INTLVB,
    INTRINSIC_INTLVB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_intlvb", /* c_name */
    "__builtin_vx_intlvb", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_intlvb, /* arg types */
    arg_inout_intlvb, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {8}, /* local TOP id */
  },
  {
    BUILT_IN_M2RUB,
    INTRINSIC_M2RUB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2rub", /* c_name */
    "__builtin_vx_m2rub", /* Runtime name */
    SImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2rub, /* arg types */
    arg_inout_m2rub, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {9}, /* local TOP id */
  },
  {
    BUILT_IN_M2RUHW,
    INTRINSIC_M2RUHW,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2ruhw", /* c_name */
    "__builtin_vx_m2ruhw", /* Runtime name */
    SImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2ruhw, /* arg types */
    arg_inout_m2ruhw, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {10}, /* local TOP id */
  },
  {
    BUILT_IN_M2RW0,
    INTRINSIC_M2RW0,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2rw0", /* c_name */
    "__builtin_vx_m2rw0", /* Runtime name */
    SImode, /* return type */
    1, /* arg_count */
    arg_builtin_m2rw0, /* arg types */
    arg_inout_m2rw0, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {11}, /* local TOP id */
  },
  {
    BUILT_IN_M2RW1,
    INTRINSIC_M2RW1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2rw1", /* c_name */
    "__builtin_vx_m2rw1", /* Runtime name */
    SImode, /* return type */
    1, /* arg_count */
    arg_builtin_m2rw1, /* arg types */
    arg_inout_m2rw1, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {12}, /* local TOP id */
  },
  {
    BUILT_IN_M2X,
    INTRINSIC_M2X,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2x", /* c_name */
    "__builtin_vx_m2x", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2x, /* arg types */
    arg_inout_m2x, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {13}, /* local TOP id */
  },
  {
    BUILT_IN_M2V,
    INTRINSIC_M2V,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2v", /* c_name */
    "__builtin_vx_m2v", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_m2v, /* arg types */
    arg_inout_m2v, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {14}, /* local TOP id */
  },
  {
    BUILT_IN_MODAND,
    INTRINSIC_MODAND,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_modand", /* c_name */
    "__builtin_vx_modand", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_modand, /* arg types */
    arg_inout_modand, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {15}, /* local TOP id */
  },
  {
    BUILT_IN_OFFSET,
    INTRINSIC_OFFSET,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_offset", /* c_name */
    "__builtin_vx_offset", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_offset, /* arg types */
    arg_inout_offset, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {16}, /* local TOP id */
  },
  {
    BUILT_IN_RANDOM,
    INTRINSIC_RANDOM,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_random", /* c_name */
    "__builtin_vx_random", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_random, /* arg types */
    arg_inout_random, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {17}, /* local TOP id */
  },
  {
    BUILT_IN_SCALES8S9,
    INTRINSIC_SCALES8S9,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_scales8s9", /* c_name */
    "__builtin_vx_scales8s9", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_scales8s9, /* arg types */
    arg_inout_scales8s9, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {18}, /* local TOP id */
  },
  // Compiler specific builtins
  // --------------------------
  {
    BUILT_IN_vx_INSERT_DP_INTO_DX0,
    INTRINSIC_vx_INSERT_DP_INTO_DX0,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_INSERT_DP_INTO_DX0", /* c_name */
    "__builtin_vx_INSERT_DP_INTO_DX0", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_vx_INSERT_DP_INTO_DX0, /* arg types */
    arg_inout_vx_INSERT_DP_INTO_DX0, /* in/out arg. table */
	DYN_INTRN_PARTIAL_COMPOSE, /* intrinsic type */
	{0} /* compose index */
  },
  {
    BUILT_IN_vx_INSERT_DP_INTO_DX1,
    INTRINSIC_vx_INSERT_DP_INTO_DX1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_INSERT_DP_INTO_DX1", /* c_name */
    "__builtin_vx_INSERT_DP_INTO_DX1", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_vx_INSERT_DP_INTO_DX1, /* arg types */
    arg_inout_vx_INSERT_DP_INTO_DX1, /* in/out arg. table */
	DYN_INTRN_PARTIAL_COMPOSE, /* intrinsic type */
	{1} /* compose index */
  },
  {
    BUILT_IN_vx_EXTRACT_DP_FROM_DX0,
    INTRINSIC_vx_EXTRACT_DP_FROM_DX0,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_EXTRACT_DP_FROM_DX0", /* c_name */
    "__builtin_vx_EXTRACT_DP_FROM_DX0", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_vx_EXTRACT_DP_FROM_DX0, /* arg types */
    arg_inout_vx_EXTRACT_DP_FROM_DX0, /* in/out arg. table */
	DYN_INTRN_PARTIAL_EXTRACT, /* intrinsic_type */
	{0} /* extract index */
  },
  {
    BUILT_IN_vx_EXTRACT_DP_FROM_DX1,
    INTRINSIC_vx_EXTRACT_DP_FROM_DX1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_EXTRACT_DP_FROM_DX1", /* c_name */
    "__builtin_vx_EXTRACT_DP_FROM_DX1", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_vx_EXTRACT_DP_FROM_DX1, /* arg types */
    arg_inout_vx_EXTRACT_DP_FROM_DX1, /* in/out arg. table */
	DYN_INTRN_PARTIAL_EXTRACT, /* intrinsic_type */
	{1} /* extract index */
  },
  {
    BUILT_IN_vx_COMPOSE_DX_FROM_2xDP,
    INTRINSIC_vx_COMPOSE_DX_FROM_2xDP,
    1, /* BYVAL */
    1, /* PURE */ 
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_COMPOSE_DX_FROM_2xDP", /* c_name */
    "__builtin_vx_COMPOSE_DX_FROM_2xDP", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_vx_COMPOSE_DX_FROM_2xDP, /* arg types */
    arg_inout_vx_COMPOSE_DX_FROM_2xDP, /* in/out arg. table */
	DYN_INTRN_COMPOSE, /* intrinsic_type */
	{0} /* useless info for compose */
  },
  {
    BUILT_IN_ACC,
    INTRINSIC_ACC,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_acc", /* c_name */
    "__builtin_vx_acc", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_acc, /* arg types */
    arg_inout_acc, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {42}, /* local TOP id */
  },
  {
    BUILT_IN_M2D,
    INTRINSIC_M2D,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2d", /* c_name */
    "__builtin_vx_m2d", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2d, /* arg types */
    arg_inout_m2d, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {43}, /* local TOP id */
  },
  {
    BUILT_IN_M2XB,
    INTRINSIC_M2XB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2xb", /* c_name */
    "__builtin_vx_m2xb", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_m2xb, /* arg types */
    arg_inout_m2xb, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {45}, /* local TOP id */
  },
  {
    BUILT_IN_M2XHW,
    INTRINSIC_M2XHW,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2xhw", /* c_name */
    "__builtin_vx_m2xhw", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_m2xhw, /* arg types */
    arg_inout_m2xhw, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {46}, /* local TOP id */
  },
  {
    BUILT_IN_M2XSHLDB,
    INTRINSIC_M2XSHLDB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2xshldb", /* c_name */
    "__builtin_vx_m2xshldb", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_m2xshldb, /* arg types */
    arg_inout_m2xshldb, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {47}, /* local TOP id */
  },
  {
    BUILT_IN_M2XW0,
    INTRINSIC_M2XW0,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2xw0", /* c_name */
    "__builtin_vx_m2xw0", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2xw0, /* arg types */
    arg_inout_m2xw0, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {48}, /* local TOP id */
  },
  {
    BUILT_IN_M2XW1,
    INTRINSIC_M2XW1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_m2xw1", /* c_name */
    "__builtin_vx_m2xw1", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_m2xw1, /* arg types */
    arg_inout_m2xw1, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {49}, /* local TOP id */
  },


  {
    BUILT_IN_ABSDHM1,
    INTRINSIC_ABSDHM1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdhm1", /* c_name */
    "__builtin_vx_absdhm1", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ABSDHM1, /* arg types */
    arg_inout_ABSDHM1, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {50}, /* local TOP id */
  },
  {
    BUILT_IN_ABSDHP1,
    INTRINSIC_ABSDHP1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdhp1", /* c_name */
    "__builtin_vx_absdhp1", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ABSDHP1, /* arg types */
    arg_inout_ABSDHP1, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {51}, /* local TOP id */
  },
  {
    BUILT_IN_ADDU16M1,
    INTRINSIC_ADDU16M1,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_addu16m1", /* c_name */
    "__builtin_vx_addu16m1", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ADDU16M1, /* arg types */
    arg_inout_ADDU16M1, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {52}, /* local TOP id */
  },
  {
    BUILT_IN_ADDU16M2,
    INTRINSIC_ADDU16M2,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_addu16m2", /* c_name */
    "__builtin_vx_addu16m2", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_ADDU16M2, /* arg types */
    arg_inout_ADDU16M2, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {53}, /* local TOP id */
  },
  {
    BUILT_IN_ADDU8CLP63,
    INTRINSIC_ADDU8CLP63,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_addu8clp63", /* c_name */
    "__builtin_vx_addu8clp63", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ADDU8CLP63, /* arg types */
    arg_inout_ADDU8CLP63, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {54}, /* local TOP id */
  },
  {
    BUILT_IN_ADDVU8U8,
    INTRINSIC_ADDVU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_addvu8u8", /* c_name */
    "__builtin_vx_addvu8u8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ADDVU8U8, /* arg types */
    arg_inout_ADDVU8U8, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {55}, /* local TOP id */
  },
  {
    BUILT_IN_CLP63,
    INTRINSIC_CLP63,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_clp63", /* c_name */
    "__builtin_vx_clp63", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_CLP63, /* arg types */
    arg_inout_CLP63, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {56}, /* local TOP id */
  },
  {
    BUILT_IN_CMPGTRU8,
    INTRINSIC_CMPGTRU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpgtru8", /* c_name */
    "__builtin_vx_cmpgtru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPGTRU8, /* arg types */
    arg_inout_CMPGTRU8, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {57}, /* local TOP id */
  },
  {
    BUILT_IN_CPMSB,
    INTRINSIC_CPMSB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cpmsb", /* c_name */
    "__builtin_vx_cpmsb", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CPMSB, /* arg types */
    arg_inout_CPMSB, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {58}, /* local TOP id */
  },
  {
    BUILT_IN_CPMSBR,
    INTRINSIC_CPMSBR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cpmsbr", /* c_name */
    "__builtin_vx_cpmsbr", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CPMSBR, /* arg types */
    arg_inout_CPMSBR, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {59}, /* local TOP id */
  },
  {
    BUILT_IN_CPMSBIR,
    INTRINSIC_CPMSBIR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cpmsbir", /* c_name */
    "__builtin_vx_cpmsbir", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CPMSBIR, /* arg types */
    arg_inout_CPMSBIR, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {60}, /* local TOP id */
  },
  {
    BUILT_IN_FIR3,
    INTRINSIC_FIR3,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_fir3", /* c_name */
    "__builtin_vx_fir3", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_FIR3, /* arg types */
    arg_inout_FIR3, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {61}, /* local TOP id */
  },
  {
    BUILT_IN_FIRU8S8P,
    INTRINSIC_FIRU8S8P,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_firu8s8p", /* c_name */
    "__builtin_vx_firu8s8p", /* Runtime name */
    VxV2DImode, /* return type */
    4, /* arg_count */
    arg_builtin_FIRU8S8P, /* arg types */
    arg_inout_FIRU8S8P, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {62}, /* local TOP id */
  },
  {
    BUILT_IN_INCGTH3,
    INTRINSIC_INCGTH3,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_incgth3", /* c_name */
    "__builtin_vx_incgth3", /* Runtime name */
    VxV1DImode, /* return type */
    4, /* arg_count */
    arg_builtin_INCGTH3, /* arg types */
    arg_inout_INCGTH3, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {63}, /* local TOP id */
  },
  {
    BUILT_IN_INCGTH3CLR,
    INTRINSIC_INCGTH3CLR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_incgth3clr", /* c_name */
    "__builtin_vx_incgth3clr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_INCGTH3CLR, /* arg types */
    arg_inout_INCGTH3CLR, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {64}, /* local TOP id */
  },
  {
    BUILT_IN_INCINSU16,
    INTRINSIC_INCINSU16,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_incinsu16", /* c_name */
    "__builtin_vx_incinsu16", /* Runtime name */
    VxV1DImode, /* return type */
    4, /* arg_count */
    arg_builtin_INCINSU16, /* arg types */
    arg_inout_INCINSU16, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {65}, /* local TOP id */
  },
  {
    BUILT_IN_MARU8S8SHR7R,
    INTRINSIC_MARU8S8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8s8shr7r", /* c_name */
    "__builtin_vx_maru8s8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8S8SHR7R, /* arg types */
    arg_inout_MARU8S8SHR7R, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {66}, /* local TOP id */
  },
  {
    BUILT_IN_MARU8U8SHR7R,
    INTRINSIC_MARU8U8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8u8shr7r", /* c_name */
    "__builtin_vx_maru8u8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8U8SHR7R, /* arg types */
    arg_inout_MARU8U8SHR7R, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {67}, /* local TOP id */
  },
  {
    BUILT_IN_MAVIU8U8SHR7R,
    INTRINSIC_MAVIU8U8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8u8shr7r", /* c_name */
    "__builtin_vx_maviu8u8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8U8SHR7R, /* arg types */
    arg_inout_MAVIU8U8SHR7R, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {68}, /* local TOP id */
  },
  {
    BUILT_IN_MAVU8U8SHR7R,
    INTRINSIC_MAVU8U8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8u8shr7r", /* c_name */
    "__builtin_vx_mavu8u8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8U8SHR7R, /* arg types */
    arg_inout_MAVU8U8SHR7R, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {69}, /* local TOP id */
  }, 
  {
    BUILT_IN_MAX,
    INTRINSIC_MAX,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_max", /* c_name */
    "__builtin_vx_max", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MAX, /* arg types */
    arg_inout_MAX, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {70}, /* local TOP id */
  },
  {
    BUILT_IN_MAXH3,
    INTRINSIC_MAXH3,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maxh3", /* c_name */
    "__builtin_vx_maxh3", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MAXH3, /* arg types */
    arg_inout_MAXH3, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {71}, /* local TOP id */
  },
  {
    BUILT_IN_MAXPAIR,
    INTRINSIC_MAXPAIR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maxpair", /* c_name */
    "__builtin_vx_maxpair", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_MAXPAIR, /* arg types */
    arg_inout_MAXPAIR, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {72}, /* local TOP id */
  },
  {
    BUILT_IN_MPRU8U8,
    INTRINSIC_MPRU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpru8u8", /* c_name */
    "__builtin_vx_mpru8u8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPRU8U8, /* arg types */
    arg_inout_MPRU8U8, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {73}, /* local TOP id */
  },
  {
    BUILT_IN_MPVU8U8,
    INTRINSIC_MPVU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpvu8u8", /* c_name */
    "__builtin_vx_mpvu8u8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPVU8U8, /* arg types */
    arg_inout_MPVU8U8, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {74}, /* local TOP id */
  },
  {
    BUILT_IN_MPVIU8U8,
    INTRINSIC_MPVIU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpviu8u8", /* c_name */
    "__builtin_vx_mpviu8u8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPVIU8U8, /* arg types */
    arg_inout_MPVIU8U8, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {75}, /* local TOP id */
  },
  {
    BUILT_IN_SELECT,
    INTRINSIC_SELECT,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_select", /* c_name */
    "__builtin_vx_select", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_SELECT, /* arg types */
    arg_inout_SELECT, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {76}, /* local TOP id */
  },
  {
    BUILT_IN_SUB128,
    INTRINSIC_SUB128,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_sub128", /* c_name */
    "__builtin_vx_sub128", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_SUB128, /* arg types */
    arg_inout_SUB128, /* in/out arg. table */
	DYN_INTRN_TOP, /* intrinsic type */
    {77}, /* local TOP id */
  },

  /* ------------------------------------------------------------------------ */
  {
    BUILT_IN_ABSDMPSLT,
    INTRINSIC_ABSDMPSLT,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdmpslt", /* c_name */
    "__builtin_vx_absdmpslt", /* Runtime name */
    VxV2DImode, /* return type */
    4, /* arg_count */
    arg_builtin_ABSDMPSLT, /* arg types */
    arg_inout_ABSDMPSLT, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {94},
  },
  {
    BUILT_IN_ABSDSLT,
    INTRINSIC_ABSDSLT,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_absdslt", /* c_name */
    "__builtin_vx_absdslt", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_ABSDSLT, /* arg types */
    arg_inout_ABSDSLT, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {95},
  },
  {
    BUILT_IN_ADDS16,
    INTRINSIC_ADDS16,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_adds16", /* c_name */
    "__builtin_vx_adds16", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ADDS16, /* arg types */
    arg_inout_ADDS16, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {96},
  },
  {
    BUILT_IN_ADDS16SHR,
    INTRINSIC_ADDS16SHR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_adds16shr", /* c_name */
    "__builtin_vx_adds16shr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_ADDS16SHR, /* arg types */
    arg_inout_ADDS16SHR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {97},
  },
  {
    BUILT_IN_ADDS16SHRR,
    INTRINSIC_ADDS16SHRR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_adds16shrr", /* c_name */
    "__builtin_vx_adds16shrr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_ADDS16SHRR, /* arg types */
    arg_inout_ADDS16SHRR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {98},
  },
  {
    BUILT_IN_ASCMF,
    INTRINSIC_ASCMF,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_ascmf", /* c_name */
    "__builtin_vx_ascmf", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ASCMF, /* arg types */
    arg_inout_ASCMF, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {99},
  },
  {
    BUILT_IN_ASCMFR,
    INTRINSIC_ASCMFR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_ascmfr", /* c_name */
    "__builtin_vx_ascmfr", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_ASCMFR, /* arg types */
    arg_inout_ASCMFR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {100},
  },
  {
    BUILT_IN_BSHRR,
    INTRINSIC_BSHRR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_bshrr", /* c_name */
    "__builtin_vx_bshrr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_BSHRR, /* arg types */
    arg_inout_BSHRR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {101},
  },
  {
    BUILT_IN_CLPSYM,
    INTRINSIC_CLPSYM,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_clpsym", /* c_name */
    "__builtin_vx_clpsym", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CLPSYM, /* arg types */
    arg_inout_CLPSYM, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {102},
  },
  {
    BUILT_IN_CLPU8,
    INTRINSIC_CLPU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_clpu8", /* c_name */
    "__builtin_vx_clpu8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CLPU8, /* arg types */
    arg_inout_CLPU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {103},
  },
  {
    BUILT_IN_CLR,
    INTRINSIC_CLR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_clr", /* c_name */
    "__builtin_vx_clr", /* Runtime name */
    VxV1DImode, /* return type */
    0, /* arg_count */
    arg_builtin_CLR, /* arg types */
    arg_inout_CLR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {104},
  },
  {
    BUILT_IN_CMPEQRU8,
    INTRINSIC_CMPEQRU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpeqru8", /* c_name */
    "__builtin_vx_cmpeqru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPEQRU8, /* arg types */
    arg_inout_CMPEQRU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {105},
  },
  {
    BUILT_IN_CMPGERU8,
    INTRINSIC_CMPGERU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpgeru8", /* c_name */
    "__builtin_vx_cmpgeru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPGERU8, /* arg types */
    arg_inout_CMPGERU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {106},
  },
  {
    BUILT_IN_CMPLERU8,
    INTRINSIC_CMPLERU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpleru8", /* c_name */
    "__builtin_vx_cmpleru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPLERU8, /* arg types */
    arg_inout_CMPLERU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {107},
  },
  {
    BUILT_IN_CMPLTRU8,
    INTRINSIC_CMPLTRU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpltru8", /* c_name */
    "__builtin_vx_cmpltru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPLTRU8, /* arg types */
    arg_inout_CMPLTRU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {108},
  },
  {
    BUILT_IN_CMPNERU8,
    INTRINSIC_CMPNERU8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cmpneru8", /* c_name */
    "__builtin_vx_cmpneru8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CMPNERU8, /* arg types */
    arg_inout_CMPNERU8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {109},
  },
  {
    BUILT_IN_CNTDELTA0CLR,
    INTRINSIC_CNTDELTA0CLR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cntdelta0clr", /* c_name */
    "__builtin_vx_cntdelta0clr", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_CNTDELTA0CLR, /* arg types */
    arg_inout_CNTDELTA0CLR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {110},
  },
  {
    BUILT_IN_CNTDELTABW,
    INTRINSIC_CNTDELTABW,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cntdeltabw", /* c_name */
    "__builtin_vx_cntdeltabw", /* Runtime name */
    VxV2DImode, /* return type */
    4, /* arg_count */
    arg_builtin_CNTDELTABW, /* arg types */
    arg_inout_CNTDELTABW, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {111},
  },
  {
    BUILT_IN_CNTDELTAFW,
    INTRINSIC_CNTDELTAFW,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cntdeltafw", /* c_name */
    "__builtin_vx_cntdeltafw", /* Runtime name */
    VxV2DImode, /* return type */
    4, /* arg_count */
    arg_builtin_CNTDELTAFW, /* arg types */
    arg_inout_CNTDELTAFW, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {112},
  },
  {
    BUILT_IN_CPMSBI,
    INTRINSIC_CPMSBI,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_cpmsbi", /* c_name */
    "__builtin_vx_cpmsbi", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_CPMSBI, /* arg types */
    arg_inout_CPMSBI, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {113},
  },
  {
    BUILT_IN_DINTLVB,
    INTRINSIC_DINTLVB,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_dintlvb", /* c_name */
    "__builtin_vx_dintlvb", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_DINTLVB, /* arg types */
    arg_inout_DINTLVB, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {114},
  },
  {
    BUILT_IN_DLUPDATE,
    INTRINSIC_DLUPDATE,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_dlupdate", /* c_name */
    "__builtin_vx_dlupdate", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_DLUPDATE, /* arg types */
    arg_inout_DLUPDATE, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {115},
  },
  {
    BUILT_IN_FIRU8S8P1CLR,
    INTRINSIC_FIRU8S8P1CLR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_firu8s8p1clr", /* c_name */
    "__builtin_vx_firu8s8p1clr", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_FIRU8S8P1CLR, /* arg types */
    arg_inout_FIRU8S8P1CLR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {116},
  },
  {
    BUILT_IN_GETSAD0,
    INTRINSIC_GETSAD0,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_getsad0", /* c_name */
    "__builtin_vx_getsad0", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_GETSAD0, /* arg types */
    arg_inout_GETSAD0, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {117},
  },
  {
    BUILT_IN_INCGT,
    INTRINSIC_INCGT,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_incgt", /* c_name */
    "__builtin_vx_incgt", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_INCGT, /* arg types */
    arg_inout_INCGT, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {118},
  },
  {
    BUILT_IN_INSMEAN,
    INTRINSIC_INSMEAN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_insmean", /* c_name */
    "__builtin_vx_insmean", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_INSMEAN, /* arg types */
    arg_inout_INSMEAN, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {119},
  },
  {
    BUILT_IN_INSMEANR,
    INTRINSIC_INSMEANR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_insmeanr", /* c_name */
    "__builtin_vx_insmeanr", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_INSMEANR, /* arg types */
    arg_inout_INSMEANR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {120},
  },
  {
    BUILT_IN_MARU8S8,
    INTRINSIC_MARU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8s8", /* c_name */
    "__builtin_vx_maru8s8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8S8, /* arg types */
    arg_inout_MARU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {121},
  },
  {
    BUILT_IN_MARU8S8SHR7,
    INTRINSIC_MARU8S8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8s8shr7", /* c_name */
    "__builtin_vx_maru8s8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8S8SHR7, /* arg types */
    arg_inout_MARU8S8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {122},
  },
  {
    BUILT_IN_MARU8U8,
    INTRINSIC_MARU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8u8", /* c_name */
    "__builtin_vx_maru8u8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8U8, /* arg types */
    arg_inout_MARU8U8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {123},
  },
  {
    BUILT_IN_MARU8U8SHR7,
    INTRINSIC_MARU8U8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maru8u8shr7", /* c_name */
    "__builtin_vx_maru8u8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MARU8U8SHR7, /* arg types */
    arg_inout_MARU8U8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {124},
  },
  {
    BUILT_IN_MAVIU8S8,
    INTRINSIC_MAVIU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8s8", /* c_name */
    "__builtin_vx_maviu8s8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8S8, /* arg types */
    arg_inout_MAVIU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {125},
  },
  {
    BUILT_IN_MAVIU8S8SHR7,
    INTRINSIC_MAVIU8S8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8s8shr7", /* c_name */
    "__builtin_vx_maviu8s8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8S8SHR7, /* arg types */
    arg_inout_MAVIU8S8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {126},
  },
  {
    BUILT_IN_MAVIU8S8SHR7R,
    INTRINSIC_MAVIU8S8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8s8shr7r", /* c_name */
    "__builtin_vx_maviu8s8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8S8SHR7R, /* arg types */
    arg_inout_MAVIU8S8SHR7R, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {127},
  },
  {
    BUILT_IN_MAVIU8U8,
    INTRINSIC_MAVIU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8u8", /* c_name */
    "__builtin_vx_maviu8u8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8U8, /* arg types */
    arg_inout_MAVIU8U8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {128},
  },
  {
    BUILT_IN_MAVIU8U8SHR7,
    INTRINSIC_MAVIU8U8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_maviu8u8shr7", /* c_name */
    "__builtin_vx_maviu8u8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVIU8U8SHR7, /* arg types */
    arg_inout_MAVIU8U8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {129},
  },
  {
    BUILT_IN_MAVU8S8,
    INTRINSIC_MAVU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8s8", /* c_name */
    "__builtin_vx_mavu8s8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8S8, /* arg types */
    arg_inout_MAVU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {130},
  },
  {
    BUILT_IN_MAVU8S8SHR7,
    INTRINSIC_MAVU8S8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8s8shr7", /* c_name */
    "__builtin_vx_mavu8s8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8S8SHR7, /* arg types */
    arg_inout_MAVU8S8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {131},
  },
  {
    BUILT_IN_MAVU8S8SHR7R,
    INTRINSIC_MAVU8S8SHR7R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8s8shr7r", /* c_name */
    "__builtin_vx_mavu8s8shr7r", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8S8SHR7R, /* arg types */
    arg_inout_MAVU8S8SHR7R, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {132},
  },
  {
    BUILT_IN_MAVU8U8,
    INTRINSIC_MAVU8U8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8u8", /* c_name */
    "__builtin_vx_mavu8u8", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8U8, /* arg types */
    arg_inout_MAVU8U8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {133},
  },
  {
    BUILT_IN_MAVU8U8SHR7,
    INTRINSIC_MAVU8U8SHR7,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mavu8u8shr7", /* c_name */
    "__builtin_vx_mavu8u8shr7", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MAVU8U8SHR7, /* arg types */
    arg_inout_MAVU8U8SHR7, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {134},
  },
  {
    BUILT_IN_MEAN,
    INTRINSIC_MEAN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mean", /* c_name */
    "__builtin_vx_mean", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MEAN, /* arg types */
    arg_inout_MEAN, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {135},
  },
  {
    BUILT_IN_MEANR,
    INTRINSIC_MEANR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_meanr", /* c_name */
    "__builtin_vx_meanr", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MEANR, /* arg types */
    arg_inout_MEANR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {136},
  },
  {
    BUILT_IN_MEANUV,
    INTRINSIC_MEANUV,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_meanuv", /* c_name */
    "__builtin_vx_meanuv", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MEANUV, /* arg types */
    arg_inout_MEANUV, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {137},
  },
  {
    BUILT_IN_MEANUVR,
    INTRINSIC_MEANUVR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_meanuvr", /* c_name */
    "__builtin_vx_meanuvr", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MEANUVR, /* arg types */
    arg_inout_MEANUVR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {138},
  },
  {
    BUILT_IN_MEANY,
    INTRINSIC_MEANY,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_meany", /* c_name */
    "__builtin_vx_meany", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MEANY, /* arg types */
    arg_inout_MEANY, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {139},
  },
  {
    BUILT_IN_MEANYR,
    INTRINSIC_MEANYR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_meanyr", /* c_name */
    "__builtin_vx_meanyr", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MEANYR, /* arg types */
    arg_inout_MEANYR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {140},
  },
  {
    BUILT_IN_MEDIAN,
    INTRINSIC_MEDIAN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_median", /* c_name */
    "__builtin_vx_median", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MEDIAN, /* arg types */
    arg_inout_MEDIAN, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {141},
  },
  {
    BUILT_IN_MF,
    INTRINSIC_MF,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mf", /* c_name */
    "__builtin_vx_mf", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MF, /* arg types */
    arg_inout_MF, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {142},
  },
  {
    BUILT_IN_MFR,
    INTRINSIC_MFR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mfr", /* c_name */
    "__builtin_vx_mfr", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MFR, /* arg types */
    arg_inout_MFR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {143},
  },
  {
    BUILT_IN_MIN,
    INTRINSIC_MIN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_min", /* c_name */
    "__builtin_vx_min", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MIN, /* arg types */
    arg_inout_MIN, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {144},
  },
  {
    BUILT_IN_MINSAD,
    INTRINSIC_MINSAD,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_minsad", /* c_name */
    "__builtin_vx_minsad", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MINSAD, /* arg types */
    arg_inout_MINSAD, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {145},
  },
  {
    BUILT_IN_MPRU8S8,
    INTRINSIC_MPRU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpru8s8", /* c_name */
    "__builtin_vx_mpru8s8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPRU8S8, /* arg types */
    arg_inout_MPRU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {146},
  },
  {
    BUILT_IN_MPU8U8SHR,
    INTRINSIC_MPU8U8SHR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpu8u8shr", /* c_name */
    "__builtin_vx_mpu8u8shr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_MPU8U8SHR, /* arg types */
    arg_inout_MPU8U8SHR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {147},
  },
  {
    BUILT_IN_MPVIU8S8,
    INTRINSIC_MPVIU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpviu8s8", /* c_name */
    "__builtin_vx_mpviu8s8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPVIU8S8, /* arg types */
    arg_inout_MPVIU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {148},
  },
  {
    BUILT_IN_MPVU8S8,
    INTRINSIC_MPVU8S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_mpvu8s8", /* c_name */
    "__builtin_vx_mpvu8s8", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_MPVU8S8, /* arg types */
    arg_inout_MPVU8S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {149},
  },
  {
    BUILT_IN_SAD,
    INTRINSIC_SAD,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_sad", /* c_name */
    "__builtin_vx_sad", /* Runtime name */
    VxV2DImode, /* return type */
    3, /* arg_count */
    arg_builtin_SAD, /* arg types */
    arg_inout_SAD, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {150},
  },
  {
    BUILT_IN_SADMIN,
    INTRINSIC_SADMIN,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_sadmin", /* c_name */
    "__builtin_vx_sadmin", /* Runtime name */
    VxV2DImode, /* return type */
    4, /* arg_count */
    arg_builtin_SADMIN, /* arg types */
    arg_inout_SADMIN, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {151},
  },
  {
    BUILT_IN_SHL1S16,
    INTRINSIC_SHL1S16,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shl1s16", /* c_name */
    "__builtin_vx_shl1s16", /* Runtime name */
    VxV2DImode, /* return type */
    1, /* arg_count */
    arg_builtin_SHL1S16, /* arg types */
    arg_inout_SHL1S16, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {152},
  },
  {
    BUILT_IN_SHR1S16,
    INTRINSIC_SHR1S16,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shr1s16", /* c_name */
    "__builtin_vx_shr1s16", /* Runtime name */
    VxV2DImode, /* return type */
    1, /* arg_count */
    arg_builtin_SHR1S16, /* arg types */
    arg_inout_SHR1S16, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {153},
  },
  {
    BUILT_IN_SHR7S16S8RC,
    INTRINSIC_SHR7S16S8RC,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shr7s16s8rc", /* c_name */
    "__builtin_vx_shr7s16s8rc", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_SHR7S16S8RC, /* arg types */
    arg_inout_SHR7S16S8RC, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {154},
  },
  {
    BUILT_IN_SHR7S16S8RS,
    INTRINSIC_SHR7S16S8RS,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shr7s16s8rs", /* c_name */
    "__builtin_vx_shr7s16s8rs", /* Runtime name */
    VxV1DImode, /* return type */
    1, /* arg_count */
    arg_builtin_SHR7S16S8RS, /* arg types */
    arg_inout_SHR7S16S8RS, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {155},
  },
  {
    BUILT_IN_SHRRS16S8,
    INTRINSIC_SHRRS16S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shrrs16s8", /* c_name */
    "__builtin_vx_shrrs16s8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_SHRRS16S8, /* arg types */
    arg_inout_SHRRS16S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {156},
  },
  {
    BUILT_IN_SHRRS16S8R,
    INTRINSIC_SHRRS16S8R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shrrs16s8r", /* c_name */
    "__builtin_vx_shrrs16s8r", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_SHRRS16S8R, /* arg types */
    arg_inout_SHRRS16S8R, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {157},
  },
  {
    BUILT_IN_SHRS16S8,
    INTRINSIC_SHRS16S8,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shrs16s8", /* c_name */
    "__builtin_vx_shrs16s8", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_SHRS16S8, /* arg types */
    arg_inout_SHRS16S8, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {158},
  },
  {
    BUILT_IN_SHRS16S8R,
    INTRINSIC_SHRS16S8R,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_shrs16s8r", /* c_name */
    "__builtin_vx_shrs16s8r", /* Runtime name */
    VxV1DImode, /* return type */
    2, /* arg_count */
    arg_builtin_SHRS16S8R, /* arg types */
    arg_inout_SHRS16S8R, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {159},
  },
  {
    BUILT_IN_SUBS16,
    INTRINSIC_SUBS16,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_subs16", /* c_name */
    "__builtin_vx_subs16", /* Runtime name */
    VxV2DImode, /* return type */
    2, /* arg_count */
    arg_builtin_SUBS16, /* arg types */
    arg_inout_SUBS16, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {160},
  },
  {
    BUILT_IN_SUBS16SHR,
    INTRINSIC_SUBS16SHR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_subs16shr", /* c_name */
    "__builtin_vx_subs16shr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_SUBS16SHR, /* arg types */
    arg_inout_SUBS16SHR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {161},
  },
  {
    BUILT_IN_SUBS16SHRR,
    INTRINSIC_SUBS16SHRR,
    1, /* BYVAL */
    1, /* PURE */
    1, /* NO_SIDEEFFECTS */
    0, /* DOES_RETURN */
    0, /* NOT_ACTUAL */
    1, /* CGINTRINSIC */
    "__builtin_vx_subs16shrr", /* c_name */
    "__builtin_vx_subs16shrr", /* Runtime name */
    VxV1DImode, /* return type */
    3, /* arg_count */
    arg_builtin_SUBS16SHRR, /* arg types */
    arg_inout_SUBS16SHRR, /* in/out arg. table */
    DYN_INTRN_TOP, /* intrinsic type */
    {162},
  },

};


extern const extension_builtins_t* dyn_get_builtins (void){
    return (dyn_builtins);
}

extern unsigned int dyn_get_builtins_count (void) {
    return (128);
}

extern machine_mode_t dyn_get_builtins_base_count (void) {
    return (BUILT_IN_STATIC_LAST+1);
}

extern INTRINSIC dyn_get_intrinsics_base_count (void) {
    return (INTRINSIC_STATIC_LAST+1);
}

unsigned int dyn_get_recrules_count ( void ) {
  return 0 ;
}

recog_rule** dyn_get_recrules ( void ) {
  return NULL ;
}

unsigned int dyn_get_extoption_count ( void ) {
  return ( 0 );
}

const char** dyn_get_extoption_array ( void ) {
  return ( NULL );
}

#ifdef __cplusplus
}
#endif

