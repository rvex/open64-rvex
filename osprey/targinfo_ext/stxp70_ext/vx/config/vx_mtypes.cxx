/*
 * This file emulates a dynamically generated extension
 */
#include "dyn_isa_registers.h"
// For  ISA_REGISTER_CLASS_LOCAL_vx_vr 0
// and ISA_REGISTER_SUBCLASS_LOCAL_vx_dv        0
#undef inline
#include "../gccfe/extension_include.h"
#include "dyn_dll_api.h"
#include "dyn_type_api.h"
#include "vx_mtypes.h"
#ifdef __cplusplus
extern "C" {
#endif
/* extension machine mode array */
static const extension_machine_types_t machine_modes_tab[] = {
  {
    VxV1DImode, // GCC machine mode
    "vx_DP",
    MODE_VECTOR_INT, // mclass
    BITS_PER_UNIT*8, // mbitsize
    8,// msize
    8, // munitsize
    VOIDmode, // mwidermode
    DImode, // innermode 
    MTYPE_VxV1DI, // open64 mtype
    8, // alignement
    ISA_REGISTER_CLASS_LOCAL_vx_vr, /* REGISTER_CLASS local id */
    -1 /* REGISTER_SUBCLASS local id: -1 mean
	  ISA_REGISTER_SUBCLASS_UNDEFINED for the global subclass */
  },
  {
    VxV2DImode,
    "vx_DX",
    MODE_VECTOR_INT,
    BITS_PER_UNIT*16, 
    16,
    16, 
    VOIDmode,
    DImode,
    MTYPE_VxV2DI,
    8, // alignment
    ISA_REGISTER_CLASS_LOCAL_vx_vr, /* REGISTER_CLASS local id */
    ISA_REGISTER_SUBCLASS_LOCAL_vx_dv /* REGISTER_SUBCLASS local id */
  }
};


const extension_machine_types_t * dyn_get_modes (void) {
    return (machine_modes_tab);
}

unsigned int dyn_get_modes_count (void) {
    return (2);
}

machine_mode_t dyn_get_modes_base_count (void) {
    return (MACHINE_MODE_STATIC_LAST + 1);
}

TYPE_ID dyn_get_mtypes_base_count (void) {
    return (MTYPE_STATIC_LAST + 1);
}
#ifdef __cplusplus
}
#endif
