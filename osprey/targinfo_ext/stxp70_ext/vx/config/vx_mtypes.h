/*
 * This file emulates a dynamically generated extension
 */
#ifndef _VX_MTYPES_H_
#define _VX_MTYPES_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "../gccfe/extension_include.h"
#include "dyn_dll_api.h"

#define VxV1DImode MACHINE_MODE_STATIC_LAST + 1
#define VxV2DImode MACHINE_MODE_STATIC_LAST + 2

#define MTYPE_VxV1DI MTYPE_STATIC_LAST + 1
#define MTYPE_VxV2DI MTYPE_STATIC_LAST + 2

#ifdef __cplusplus
}
#endif
#endif  /*_VX_MTYPES_H_ */
