/******************************************************************************
 *       Copyright C 2001 STMicroelectronics - All rights reserved            *
 *                                                                            *
 *  No part of this document can be copied, or reproduct, or translated, or   *
 *  stored on electronic storage without the written agreement of             *
 *  STMicroeletronics                                                         *
 *                                                                            *
 *                        DMD-TPA / Architecture Team                         *
 *                       12, rue Jules Horowitz  BP217                        *
 *                       38019 GRENOBLE CEDEX - FRANCE                        *
 ******************************************************************************/
//* Instruction X_ArithTriadic_4
instruction X_ArithTriadic_4 {
	composition {
		instr : ( Gxq_ABSD_Vk_Vr_Vs || Gxq_ABSDMIN_Vk_Dh_Vs || Gxq_ABSDMAX_Vk_Dh_Vs || Gxq_BSHR_Vk_Vr_Vs_u4 || Gxq_BSHRR_Vk_Vr_Vs_Rn || Gxq_MEDIAN_Vk_Dh_Vs )
	}
	coding { instr:32 }
	syntax { instr }
	//* Behavior
	behavior  {
		instr ();
	} // End behavior
} // End instruction X_ArithTriadic_4

//* Instruction Gxq_ABSD_Vk_Vr_Vs
instruction Gxq_ABSD_Vk_Vr_Vs {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Vr_reg:Vector_registers,
		Vs_reg:Vector_registers
	}
	coding { Gx:3 Vk_reg:5 Vr_reg:5 Vs_reg:5 0b01100010100100:14 }
	syntax { Gx ~" " "ABSD" " " Vk_reg "," Vr_reg "," Vs_reg }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, Vr_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);

		uint5 k;
		uint5 r;
		uint5 s;

			/* local variables used in the instruction description */

			uint8 j;  /* byte indice */

			GPVR tmpVk, tmpVr, tmpVs;

		// Start decode
		k = Vk_reg.coding;
		r = Vr_reg.coding;
		s = Vs_reg.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute


			/* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
			/* x_guard(0) = GR[x]; */
			/* x_guard(1) = GR[x+8]; */
			/* x_guard(2) = GR[x+16]; */
			/* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVr = GPVR_Read(r);
			tmpVs = GPVR_Read(s);

			if(x_guard(0)) {

				/* ABSDIFF */

				for (j=0; j<8; j++) {
					tmpVk.B(j) = absdiffU8(tmpVr.B(j), tmpVs.B(j));
				}

				/* Write Back Results to Vk */
				GPVR_Write(k, tmpVk);

			}

		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_ABSD_Vk_Vr_Vs

//* Instruction Gxq_ABSDMIN_Vk_Dh_Vs
instruction Gxq_ABSDMIN_Vk_Dh_Vs {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Dh_reg:Vector_register_pairs,
		Vs_reg:Vector_registers
	}
	coding { Gx:3 Vk_reg:5 Dh_reg:4 0b0:1 Vs_reg:5 0b01110000100100:14 }
	syntax { Gx ~" " "ABSDMIN" " " Vk_reg "," Dh_reg "," Vs_reg }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding+1);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);

		uint5 k;
		uint5 he;
		uint5 ho;
		uint5 s;

			/* local variables used in the instruction description */

			uint8 j;  /* byte indice */

			uint8 tmpU8a, tmpU8b;

			GPVR tmpVk, tmpVs;
			GPVR tmpVhe, tmpVho;

		// Start decode
		k = Vk_reg.coding;
		he = 2*Dh_reg.coding;
		ho = 2*Dh_reg.coding+1;
		s = Vs_reg.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute


			/* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
			/* x_guard(0) = GR[x]; */
			/* x_guard(1) = GR[x+8]; */
			/* x_guard(2) = GR[x+16]; */
			/* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVhe = GPVR_Read((he));
			tmpVho = GPVR_Read((ho));
			tmpVs = GPVR_Read(s);

			if (x_guard(0)) {

			  /* ABSDMIN */

			  for (j=0; j<8; j++) {

			    tmpU8a = absdiffU8(tmpVho.B(j), tmpVs.B(j));
			    tmpU8b = absdiffU8(tmpVhe.B(j), tmpVs.B(j));

			    if (tmpU8b < tmpU8a) {
			      tmpU8a = tmpU8b;
			    }

			    tmpVk.B(j) = tmpU8a;
			  }

			  /* Write Back Results to Vk */
			  GPVR_Write(k, tmpVk);

			}

		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_ABSDMIN_Vk_Dh_Vs

//* Instruction Gxq_ABSDMAX_Vk_Dh_Vs
instruction Gxq_ABSDMAX_Vk_Dh_Vs {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Dh_reg:Vector_register_pairs,
		Vs_reg:Vector_registers
	}
	coding { Gx:3 Vk_reg:5 Dh_reg:4 0b0:1 Vs_reg:5 0b01110001100100:14 }
	syntax { Gx ~" " "ABSDMAX" " " Vk_reg "," Dh_reg "," Vs_reg }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding+1);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);

		uint5 k;
		uint5 he;
		uint5 ho;
		uint5 s;

			/* local variables used in the instruction description */

			uint8 j;  /* byte indice */

			uint8 tmpU8a, tmpU8b;

			GPVR tmpVk, tmpVs;
			GPVR tmpVhe, tmpVho;

		// Start decode
		k = Vk_reg.coding;
		he = 2*Dh_reg.coding;
		ho = 2*Dh_reg.coding+1;
		s = Vs_reg.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute

			/* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
			/* x_guard(0) = GR[x]; */
			/* x_guard(1) = GR[x+8]; */
			/* x_guard(2) = GR[x+16]; */
			/* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVhe = GPVR_Read((he));
			tmpVho = GPVR_Read((ho));
			tmpVs = GPVR_Read(s);

			if (x_guard(0)) {

			  /* ABSDMAX */

			  for (j=0; j<8; j++) {

			    tmpU8a = absdiffU8(tmpVho.B(j), tmpVs.B(j));
			    tmpU8b = absdiffU8(tmpVhe.B(j), tmpVs.B(j));

			    if (tmpU8b > tmpU8a) {
			      tmpU8a = tmpU8b;
			    }

			    tmpVk.B(j) = tmpU8a;
			  }

			  /* Write Back Results to Vk */
			  GPVR_Write(k, tmpVk);

			}

		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_ABSDMAX_Vk_Dh_Vs

//* Instruction Gxq_BSHR_Vk_Vr_Vs_u4
instruction Gxq_BSHR_Vk_Vr_Vs_u4 {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Vr_reg:Vector_registers,
		Vs_reg:Vector_registers,
		u4_imm:immediate
	}
	coding { Gx:3 Vk_reg:5 Vr_reg:5 Vs_reg:5 0b1110:4 u4_imm:4 0b100100:6 }
	syntax { Gx ~" " "BSHR" " " Vk_reg "," Vr_reg "," Vs_reg "," u4_imm }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, Vr_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);
		TRC_IMM(u4, 4, u4_imm.coding);

		uint5 k;
		uint5 r;
		uint5 s;
		uint4 u4;
		        /* local variables used in the instruction description */

		        uint8 j;  /* byte indice */

		//        uint4 u4;

		        uint4 tmpU4;

			uint8 tmpU8tab[2*8];

			GPVR tmpVk, tmpVr, tmpVs;

		// Start decode
		k = Vk_reg.coding;
		r = Vr_reg.coding;
		s = Vs_reg.coding;
		u4 = u4_imm.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute

		        /* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
		        /* x_guard(0) = GR[x]; */
		        /* x_guard(1) = GR[x+8]; */
		        /* x_guard(2) = GR[x+16]; */
		        /* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVr = GPVR_Read(r);
			tmpVs = GPVR_Read(s);

		        tmpU4 = BF_range(u4, 3, 0);  /* shift u4 bytes */

			for(j=0; j<8; j++) {  /* delay line */
			  tmpU8tab[j] = tmpVs.B(j);
			  tmpU8tab[j+8] = tmpVr.B(j);
			}

			/* Bytes shift right */

		        if(x_guard(0)) {

			  for (j=0; j<8; j++) {
			    if ((j+tmpU4) < 16) {
			      tmpVk.B(j) = tmpU8tab[j+tmpU4];
			    } else {
			      tmpVk.B(j) = 0;
			    }
			  }

		          /* Write Back Results to GPVR */
			  GPVR_Write(k, tmpVk);
		        }

		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_BSHR_Vk_Vr_Vs_u4

//* Instruction Gxq_BSHRR_Vk_Vr_Vs_Rn
instruction Gxq_BSHRR_Vk_Vr_Vs_Rn {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Vr_reg:Vector_registers,
		Vs_reg:Vector_registers,
		Rn_reg:sxcore_gpr
	}
	coding { Gx:3 Vk_reg:5 Vr_reg:5 Vs_reg:5 0b101:3 Rn_reg:5 0b100100:6 }
	syntax { Gx ~" " "BSHRR" " " Vk_reg "," Vr_reg "," Vs_reg "," Rn_reg }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, Vr_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);

		uint5 k;
		uint5 r;
		uint5 s;
		        /* local variables used in the instruction description */

		        uint8 j;  /* byte indice */

		        uint4 tmpU4;

			uint8 tmpU8tab[2*8];

			GPVR tmpVk, tmpVr, tmpVs;

		// Start decode
		k = Vk_reg.coding;
		r = Vr_reg.coding;
		s = Vs_reg.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute

		        /* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
		        /* x_guard(0) = GR[x]; */
		        /* x_guard(1) = GR[x+8]; */
		        /* x_guard(2) = GR[x+16]; */
		        /* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVr = GPVR_Read(r);
			tmpVs = GPVR_Read(s);

		        tmpU4 = BF_range(x_opa, 3, 0);  /* shift u4 bytes */

			for(j=0; j<8; j++) {  /* delay line */
			  tmpU8tab[j] = tmpVs.B(j);
			  tmpU8tab[j+8] = tmpVr.B(j);
			}

			/* Bytes shift right */

		        if(x_guard(0)) {

			  for (j=0; j<8; j++) {
			    if ((j+tmpU4) < 16) {
			      tmpVk.B(j) = tmpU8tab[j+tmpU4];
			    } else {
			      tmpVk.B(j) = 0;
			    }
			  }

		          /* Write Back Results to GPVR */
			  GPVR_Write(k, tmpVk);
		        }

		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_BSHRR_Vk_Vr_Vs_Rn

//* Instruction Gxq_MEDIAN_Vk_Dh_Vs
instruction Gxq_MEDIAN_Vk_Dh_Vs {
	composition {
		Gx:sxcore_guard_opt,
		Vk_reg:Vector_registers,
		Dh_reg:Vector_register_pairs,
		Vs_reg:Vector_registers
	}
	coding { Gx:3 Vk_reg:5 Dh_reg:4 0b0:1 Vs_reg:5 0b01110010100100:14 }
	syntax { Gx ~" " "MEDIAN" " " Vk_reg "," Dh_reg "," Vs_reg }
	//* Behavior
	behavior  {
		// Trace source operands
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding);
		TRC_SRC(x_no_ctxtid, 0, 2*Dh_reg.coding+1);
		TRC_SRC(x_no_ctxtid, 0, Vs_reg.coding);

		uint5 k;
		uint5 he;
		uint5 ho;
		uint5 s;

			/* local variables used in the instruction description */

			uint8 j;  /* byte indice */

			GPVR tmpVk, tmpVs;
			GPVR tmpVhe, tmpVho;

		// Start decode
		k = Vk_reg.coding;
		he = 2*Dh_reg.coding;
		ho = 2*Dh_reg.coding+1;
		s = Vs_reg.coding;
		// End decode

		// Start fetch


		// End fetch

		// Start execute


			/* x_inst[31:0] = current_inst[31:0]; */ /* The 32 bits of the current instruction */
			/* x_guard(0) = GR[x]; */
			/* x_guard(1) = GR[x+8]; */
			/* x_guard(2) = GR[x+16]; */
			/* x_guard(3) = GR[x+24]; */

			/* Operands Fetch */

			tmpVhe = GPVR_Read((he));
			tmpVho = GPVR_Read((ho));
			tmpVs = GPVR_Read(s);

			if (x_guard(0)) {

				/* Median */
				for (j=0; j<8; j++) {
					tmpVk.B(j) = medianU8(tmpVho.B(j), tmpVhe.B(j), tmpVs.B(j));
				}

				/* Write Back Results to Vk */
				GPVR_Write(k, tmpVk);

			}


		// End execute

		// Start writeback

		// End writeback
		// Trace destination operands
		TRC_DST(x_no_ctxtid, 0, Vk_reg.coding);

		
	} // End behavior
} // End instruction Gxq_MEDIAN_Vk_Dh_Vs

