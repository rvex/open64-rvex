#!/usr/bin/env python3

import sys
import json

if __name__ == '__main__':
    
    # Check command line/print usage.
    if len(sys.argv) < 2:
        print('Usage: %s <cfg.json>' % sys.argv[0], file=sys.stderr)
        print('', file=sys.stderr)
        print('Configures the source tree in the current directory according to the given', file=sys.stderr)
        print('configuration file. Returns 0 if successful, 3 if the tool should not be', file=sys.stderr)
        print('installed, or something else (usually 1) if an error occurred.', file=sys.stderr)
        print('', file=sys.stderr)
        print('You can also run `%s -` to have the script generate default versions of the', file=sys.stderr)
        print('source files. These are the ones that should be checked into the git repos.', file=sys.stderr)
        sys.exit(2)
    
    # Load the configuration file.
    if sys.argv[1].strip() == '-':
        cfg_json = {}
    else:
        with open(sys.argv[1], 'r') as f:
            cfg_json = json.load(f)

class Config(object):
    
    def __init__(self, d, prefix=''):
        self._d = d
        self._p = prefix
        self._s = set()
        for key in d:
            if not key.startswith(prefix):
                continue
            key = key[len(prefix):].split('.')
            if len(key) == 1:
                continue
            self._s.add(key[0])
    
    def __getattr__(self, name):
        val = self._d.get(self._p + name, None)
        if val is not None:
            return val
        
        if name in self._s:
            return Config(self._d, self._p + name + '.')
        
        raise AttributeError(name)
    
    def __str__(self):
        data = ['    %s: %s\n' % (key[len(self._p):], value) for key, value in sorted(self._d.items()) if key.startswith(self._p)]
        return 'Config(\n' + ''.join(data) + ')'

def parse_cfg(cfg_json, defaults):
    """Parses the config.json contents into a Config object that is guaranteed
    to contain the keys in `defaults`. An error is printed if the given cfg_json
    dict contains non-default keys which are not present in the `defaults`
    dict, which implies that the configuration file has a higher version than
    this file and is doing something that isn't backwards compatible."""
    
    # Check that we don't have any "required" keys that we don't know about.
    for key, value in cfg_json.items():
        if value['req'] and key not in defaults:
            print('Error: found non-default key "%s", which this version of rvex-cfg.py doesn\'t know about!' % key, file=sys.stderr)
            sys.exit(1)
    
    # Build our cfg dictionary.
    cfg = {}
    for key, default in defaults.items():
        value = cfg_json.get(key, {'val': default})
        if value is None:
            print('Error: no configuration value specified for key "%s"!' % key, file=sys.stderr)
            sys.exit(1)
        cfg[key] = value['val']
    
    return Config(cfg)

def template(infname, outfname, *args, **kwargs):
    """Template engine simply using Python's str.format()."""
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            outf.write(inf.read().format(*args, **kwargs))

def template2(infname, outfname, *args, **kwargs):
    """Template engine using Python's str.format(), but with different
    characters that are more suitable for C:
     - open:   <|
     - close:  |>
     - escape: <-| to <|
               |-> to |>
    """
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            data = inf.read()
            data = data.replace('{', '{{').replace('}', '}}')
            data = data.replace('<|', '{').replace('|>', '}')
            data = data.format(*args, **kwargs)
            data = data.replace('<-|', '<|').replace('|->', '|>')
            outf.write(data)

def dont_build(s=''):
    if s:
        s = ': ' + s
    print('This tool does not need to be built%s' % s, file=sys.stderr)
    sys.exit(3)

def misconfigured(s):
    if s:
        s = ': ' + s
    print('Configuration error%s' % s, file=sys.stderr)
    sys.exit(1)

def done(s):
    if s:
        s = ': ' + s
    print('Configuration complete%s' % s, file=sys.stderr)
    sys.exit(0)

#===============================================================================
# Tool-specific from here onwards
#===============================================================================

cfg = parse_cfg(cfg_json, {
    
    # Number of lanes.
    'resources.num_lanes': 8,
    
    # Lane resources (list of boolean).
    'resources.int_mul_lanes': [True]*8,
    'resources.float_add_lanes': [False]*8,
    'resources.float_mul_lanes': [False]*8,
    'resources.float_cmp_lanes': [False]*8,
    'resources.float_i2f_lanes': [False]*8,
    'resources.float_f2i_lanes': [False]*8,
    
    # Memory lane configuration.
    'resources.num_groups': 4,
    'resources.mem_one_per_cyc': True,
    
    # Latency configuration.
    'special.disable_forward': False,
    'special.extend_pipeline': False,
    
    # Vexparse configuration.
    'toolchain.vexparse_opt': -1,
    
    # Software configuration.
    'software.runtime': 'bare'
    
})

# Figure out if we need the normal r-VEX or floating point r-VEX.
enable_float = [
   any(cfg.resources.float_add_lanes),
   any(cfg.resources.float_mul_lanes),
   any(cfg.resources.float_cmp_lanes),
   any(cfg.resources.float_i2f_lanes),
   any(cfg.resources.float_f2i_lanes)
]
if any(enable_float) and not all(enable_float):
    misconfigured('need at least one of every floating point resource if floating point extensions are used.')
enable_float = any(enable_float)

if not any(cfg.resources.int_mul_lanes):
    misconfigured('need at least one lane with an integer multiplier.')

# Generate driver.
template2(
    'osprey/driver/lang_defs.c.tpl',
    'osprey/driver/lang_defs.c',
    enable_vexparse = 'TRUE' if cfg.toolchain.vexparse_opt >= 0 else 'FALSE'
)

template2(
    'osprey/driver/opt_actions.c.tpl',
    'osprey/driver/opt_actions.c',
    default_core = 'RVEX_FP' if enable_float else 'RVEX'
)

runtime = {
    'bare':       'BARE',
    'newlib':     'NEWLIB',
    'newlib_lfs': 'NEWLIB_LFS',
    'uclibc':     'UCLIBC',
}.get(cfg.software.runtime, None)
if runtime is None:
    misconfigured('unknown \'software.runtime\': %s' % cfg.software.runtime)

template2(
    'osprey/driver/special_options.c.tpl',
    'osprey/driver/special_options.c',
    runtime = runtime
)

# Generate target info.

# TODO: latencies need to be checked! Also in vexparse/rvex-cfg.py.
short_lat = 4 if cfg.special.disable_forward else 1
long_lat  = 4 if cfg.special.disable_forward else 2
if cfg.special.extend_pipeline:
    short_lat += 2
    long_lat  += 2

targinfo = {
    'num_lanes': cfg.resources.num_lanes,
    'num_pairs': cfg.resources.num_lanes // 2,
    'num_mem': 1 if cfg.resources.mem_one_per_cyc else cfg.resources.num_groups,
    'num_mul':  max(1, sum(cfg.resources.int_mul_lanes)),
    'num_falu': max(1, sum(cfg.resources.float_add_lanes)),
    'num_fmul': max(1, sum(cfg.resources.float_mul_lanes)),
    'num_fcmp': max(1, sum(cfg.resources.float_cmp_lanes)),
    'num_i2f':  max(1, sum(cfg.resources.float_i2f_lanes)),
    'num_f2i':  max(1, sum(cfg.resources.float_f2i_lanes)),
    'short_latency': short_lat,
    'long_latency': long_lat
}
for fname in ['osprey/targinfo/st200/proc/rvex_si.cxx', 'osprey/targinfo/st200/proc/rvex_fp_si.cxx']:
    template2(fname + '.tpl', fname, **targinfo)

done('open64 is enabled')
