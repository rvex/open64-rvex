Open64 compiler for r-VEX
=========================

This port of Open64 for the r-VEX is based upon the STmicro ST200 target,
because the ST200 series uses an instruction set that is very closely
related to r-VEX (both are based on HP VEX). The ST200 (and Open64) are both a
relic of the past by now, but it's the most stable compiler we have.

Unfortunately, it doesn't build right with versions of GCC that are too new,
and only builds properly if GCC thinks it's running on a 32-bit system (no,
passing `-m32` to GCC is not enough to fix this, for whatever reason). We use
`proot` to work around this problem. `proot` works like `chroot` but doesn't
require root access; instead of actually changing the root directory it simply
catches all system calls made by child processes and modifies the paths before
forwarding them to the kernel. The fake root directory can be downloaded from
`http://ftp.tudelft.nl/TUDelft/rvex/releases/4.x/4.2/toolsrc/build-env.tar.bz2`;
this also includes the `proot` program.


Manual building
---------------

It is strongly recommended to use the toolchain builder to handle building this
behemoth for you. However, if you're stubborn, here's *approximately* how to do
it, assuming you're already in an environment/operating system that Open64
approves of.

 - Run `rvex-cfg.py -`. This produces some files that set command-line defaults
   and describe the r-VEX pipeline and instruction availability. Specifically,
   it configures the compiler for making 8-wide binaries, with up to 8 multiply
   instructions per cycle, no floating point support, no special pipeline
   features (that is, multiply and load instructions have one cycle latency,
   everything else is single-cycle), and the `bare` runtime. You can enable
   hardware floating point support with up to one floating point instruction per
   cycle using the compiler command line flag `-mcore=rvex_fp`, and you can
   select the `newlib` runtime using `-mruntime=newlib`. If you want a different
   configuration, you need to pass a json file to `rvex-cfg.py` instead of the
   `-`; you can generate this json file using the `configure` command of the
   toolchain builder.
 - `cd osprey`
 - `mkdir objdir`
 - `cd targia32_st200`
 - `../configure BUILD=gnu-release PLATFORMS=linux-st200-linux TOOLDEFS=default`
 - Run `make install`. To add some more information on the build, you can append
   the following to this command:

```
RELEASE_ID=rVEX-8issue RELEASE_DATE="`date +%F`" RELEASE_MAJOR="6" RELEASE_MINOR="0" RELEASE_PATCHLEVEL="0" RELEASE_PLATFORM="(TUDelft/rVEX)"
```


r-VEX port notes
----------------

The rVEX-specific code is in `targinfo/st200`.

Keep in mind that this compiler does not include runtime libraries such as libm,
libc, or a floating point library. These can be found in the newlib and libgcc
repositories.

Some of the intrinsics that the compiler uses need VEX instruction set
extensions, as the st200 ISA has some instructions that VEX does not. For the
r-VEX, we have added these instructions (count leading zeros, `clz`, and 2
multiplication types, `mullhus` and `mulhhs`). You can also choose to disable
these instructions by removing them from the ISA definition file in the target
directory but then you must modify some multiplication intrinsics and create an
intrinsic to emulate the `clz` instruction. The ST200 floating point library
(version 6 that includes FLIP, also available on the STLinux FTP site) also
relies on these instructions so without them or the intrinsics you will not be
able to compile it.

Register translation table st200 -> VEX:
```
r0        => r0
r1 - r7   => r57 - r62 (callee-saved)
r7        => r62 (stack pointer; not always used)
r8-r11    => r11 - r56 (scratch)
r12       => r1 (stack pointer)
r13       => r55 (thread pointer ^1)
r14       => r56 (global pointer ^2)
r15       => r2 (struct/union return pointer)
r16 - r23 => r3 - r10 (arguments)
r24 - r62 => r11 - r56 (scratch)
r63       => l0.0 (link register)
```

Note 1: the thread pointer does not exist in VEX. At this time, the compiler
reserves this register.

Note 2: the global pointer is not always used, I think this is specifically for
PIC which we do not support natively at this time (only through FLAT binfmt).


More information
----------------

For more information about debugging the compiler and the various compilation
phases, see `osprey/doc/`.
